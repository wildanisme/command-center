<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelompok_model extends CI_Model
{


    public function get($param=null)
    {
        if(isset($param['where']))
        {
          $this->db->where($param['where']);
        }

        if(isset($param['limit']) && isset($param['offset']))
        {
            $this->db->limit($param['limit'],$param['offset']);
        }

        if (isset($param['search'])) {
            $this->db->where("(kelompok.nama_kelompok like '%".$param['search']."%' )");
        }

        $this->db->join("user ketua","ketua.user_id = kelompok.id_ketua","left");
        $this->db->join("desa","desa.id_desa = kelompok.id_desa","left");
        $this->db->join("kecamatan","kecamatan.id_kecamatan = kelompok.id_kecamatan","left");
        $this->db->join("kabupaten","kabupaten.id_kabupaten = kelompok.id_kabupaten","left");
        $this->db->join("ref_jenis_usaha","ref_jenis_usaha.id_jenis_usaha = kelompok.id_jenis_usaha","left");
        $this->db->join("mentor","mentor.id_mentor = kelompok.id_mentor","left");
        $this->db->select("kelompok.*,
        mentor.nama_mentor,
        ref_jenis_usaha.nama_jenis as 'nama_jenis_usaha',
        ketua.full_name as 'nama_ketua_kelompok',
        desa.desa as 'nama_desa',
        kecamatan.kecamatan as 'nama_kecamatan',
        kabupaten.kabupaten as 'nama_kabupaten'

        ");

      

        return $this->db->get('kelompok');
    }

}
