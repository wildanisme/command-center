<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ref_jenisusaha_model extends CI_Model
{

    public $id_jenis_usaha;
    public $nama_jenis;
    public $status;

    public function get_all()
    {
        $this->db->order_by('id_jenis_usaha', 'desc');
        return $this->db->get('ref_jenis_usaha')->result();
    }
    public function get_active()
    {
        $this->db->order_by('id_jenis_usaha', 'desc');
        $this->db->where('status', 'aktif');
        return $this->db->get('ref_jenis_usaha')->result();
    }

    public function get_by_id($id_jenis_usaha)
    {
        return $this->db->get_where('nama_jenis', array('id_jenis_usaha' => $id_jenis_usaha))->row();
    }

    public function insert()
    {
        $this->db->set('nama_jenis', $this->nama_jenis);
        $this->db->set('status', $this->status);
        $this->db->insert('ref_jenis_usaha');
        return $this->db->insert_id();
    }

    public function update()
    {
        $this->db->where('id_jenis_usaha', $this->id_jenis_usaha);
        $this->db->set('nama_jenis', $this->nama_jenis);
        $this->db->set('status', $this->status);
        return $this->db->update('ref_jenis_usaha');
    }

    public function delete()
    {
        return $this->db->delete('ref_jenis_usaha', array('id_jenis_usaha' => $this->id_jenis_usaha));
    }

    


}
