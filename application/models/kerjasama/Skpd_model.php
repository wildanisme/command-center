<?php defined('BASEPATH') or exit('No direct script access allowed');

class Skpd_model extends CI_Model
{
    private $_table = "kerjasama_skpd";

    public $id_skpd;
    public $nama_skpd;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_skpd" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_skpd = uniqid();
        $this->nama_skpd = $post["nama_skpd"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_skpd = $post["id"];
        $this->nama_skpd = $post["nama_skpd"];
        return $this->db->update($this->_table, $this, array('id_skpd' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_skpd" => $id));
    }
}
