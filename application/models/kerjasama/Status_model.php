<?php defined('BASEPATH') or exit('No direct script access allowed');

class Status_model extends CI_Model
{
    private $_table = "kerjasama_status";

    public $id_status;
    public $nama_status;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_status" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_status = uniqid();
        $this->nama_status = $post["nama_status"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_status = $post["id"];
        $this->nama_status = $post["nama_status"];
        return $this->db->update($this->_table, $this, array('id_status' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_status" => $id));
    }
}
