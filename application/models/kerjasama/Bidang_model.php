<?php defined('BASEPATH') or exit('No direct script access allowed');

class Bidang_model extends CI_Model
{
    private $_table = "kerjasama_bidang";

    public $id_bidang;
    public $nama_bidang;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_bidang" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_bidang = uniqid();
        $this->nama_bidang = $post["nama_bidang"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_bidang = $post["id"];
        $this->nama_bidang = $post["nama_bidang"];
        return $this->db->update($this->_table, $this, array('id_bidang' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_bidang" => $id));
    }
}
