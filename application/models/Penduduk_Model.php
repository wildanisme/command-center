<?php

class Penduduk_Model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
      public function getPjkDrh() {
        $sql = "select sum(pjk_drh) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getRetDrh() {
        $sql = "select sum(pjk_drh) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }

      public function getKd() {
        $sql = "select sum(kd) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    
      public function getLlpads() {
        $sql = "select sum(llpads) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
      public function getPadt() {
        $sql = "select sum(llpads) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
      public function getTd() {
        $sql = "select sum(td) as total from card_disduk";
        $query = $this->db->query($sql);
        return $query->result();
      }
    

}