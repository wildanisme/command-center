<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracking_model extends CI_Model

{
	public	$nomer_registrasi	;
	public	$nomer_sk	;
	public	$nama_pemohon	;
	public	$tgl_registrasi_permohonan	;
	public	$id_bidang_izin	;
	public	$id_jenis_izin	;
	public	$id_sub_jenis_izin	;
	public	$id_sektor	;
	public	$kd_provinsi_perusahaan	;
	public	$kd_kabupaten_perusahaan	;
	public	$kd_kecamatan_perusahaan	;
	public	$kd_desa_perusahaan	;


public function select_by_nomer_reg($id = NULL) {
        if(!empty($id)){
            $this->db->where('no_registrasi', $id);
        }        
        //$this->db->select('tbl_perizinan.*, provinsi.*, kabupaten.*, desa.*, ref_jenis_izin.*, ref_bidang_izin.*, ref_jalan.*, ref_jenis_usaha.*, ref_gangguan.*');
        $this->db->select('*');
        $this->db->select('prov.provinsi as provinsi_perusahaan');
		$this->db->select('kab.kabupaten as kabupaten_perusahaan');
		$this->db->select('kec.kecamatan as kecamatan_perusahaan');
		$this->db->select('des.desa as desa_perusahaan');

		$this->db->join('provinsi as prov','tbl_perizinan.kd_provinsi_perusahaan = prov.id_provinsi', 'left');
		$this->db->join('kabupaten as kab','tbl_perizinan.kd_kabupaten_perusahaan = kab.id_kabupaten', 'left');
		$this->db->join('kecamatan as kec','tbl_perizinan.kd_kecamatan_perusahaan = kec.id_kecamatan', 'left');
		$this->db->join('desa as des','tbl_perizinan.kd_desa_perusahaan = des.id_desa', 'left');

		$this->db->join('provinsi','tbl_perizinan.kd_provinsi_pemohon = provinsi.id_provinsi', 'left');
		$this->db->join('kabupaten','tbl_perizinan.kd_kabupaten_pemohon = kabupaten.id_kabupaten', 'left');
		$this->db->join('kecamatan','tbl_perizinan.kd_desa_kecamatan = kecamatan.id_kecamatan', 'left');
		$this->db->join('desa','tbl_perizinan.kd_desa_pemohon = desa.id_desa', 'left');

		$this->db->join('ref_jenis_izin','tbl_perizinan.id_jenis_izin = ref_jenis_izin.id_jenis_izin', 'left');
		$this->db->join('ref_bidang_izin','tbl_perizinan.id_bidang_izin = ref_bidang_izin.id_bidang', 'left');
		$this->db->join('ref_jalan','tbl_perizinan.id_jalan = ref_jalan.id_jalan', 'left');
		$this->db->join('ref_jenis_usaha','tbl_perizinan.id_jenis_usaha = ref_jenis_usaha.id_jenis_usaha', 'left');
		$this->db->join('ref_gangguan','ref_jenis_usaha.id_gangguan = ref_gangguan.id_gangguan', 'left');

		$this->db->join('ref_kbli','tbl_perizinan.id_kbli1 = ref_kbli.id_kbli', 'left');
		$this->db->join('ref_layanan','tbl_perizinan.id_jenis_layanan_izin = ref_layanan.id_layanan', 'left');
		$this->db->join('ref_bentuk_perusahaan','tbl_perizinan.id_bentuk_perusahaan = ref_bentuk_perusahaan.id_bentuk_perusahaan', 'left');
        $query = $this->db->get('tbl_perizinan');
        return $query->result();  
    }

public function num_rows_by_nomer_reg($id = NULL) {
        if(!empty($id)){
            $this->db->where('no_registrasi', $id);
        }        
        $query = $this->db->get('tbl_perizinan');
        return $query->num_rows();   
    }

public function select_by_hash_registrasi($id = NULL) {
        if(!empty($id)){
            $this->db->where('hash_registrasi', $id);
        }        
        //$this->db->select('tbl_perizinan.*, provinsi.*, kabupaten.*, desa.*, ref_jenis_izin.*, ref_bidang_izin.*, ref_jalan.*, ref_jenis_usaha.*, ref_gangguan.*');
        $this->db->select('*');
        $this->db->select('prov.provinsi as provinsi_perusahaan');
		$this->db->select('kab.kabupaten as kabupaten_perusahaan');
		$this->db->select('kec.kecamatan as kecamatan_perusahaan');
		$this->db->select('des.desa as desa_perusahaan');

		$this->db->join('provinsi as prov','tbl_perizinan.kd_provinsi_perusahaan = prov.id_provinsi', 'left');
		$this->db->join('kabupaten as kab','tbl_perizinan.kd_kabupaten_perusahaan = kab.id_kabupaten', 'left');
		$this->db->join('kecamatan as kec','tbl_perizinan.kd_kecamatan_perusahaan = kec.id_kecamatan', 'left');
		$this->db->join('desa as des','tbl_perizinan.kd_desa_perusahaan = des.id_desa', 'left');

		$this->db->join('provinsi','tbl_perizinan.kd_provinsi_pemohon = provinsi.id_provinsi', 'left');
		$this->db->join('kabupaten','tbl_perizinan.kd_kabupaten_pemohon = kabupaten.id_kabupaten', 'left');
		$this->db->join('kecamatan','tbl_perizinan.kd_desa_kecamatan = kecamatan.id_kecamatan', 'left');
		$this->db->join('desa','tbl_perizinan.kd_desa_pemohon = desa.id_desa', 'left');

		$this->db->join('ref_jenis_izin','tbl_perizinan.id_jenis_izin = ref_jenis_izin.id_jenis_izin', 'left');
		$this->db->join('ref_bidang_izin','tbl_perizinan.id_bidang_izin = ref_bidang_izin.id_bidang', 'left');
		$this->db->join('ref_jalan','tbl_perizinan.id_jalan = ref_jalan.id_jalan', 'left');
		$this->db->join('ref_jenis_usaha','tbl_perizinan.id_jenis_usaha = ref_jenis_usaha.id_jenis_usaha', 'left');
		$this->db->join('ref_gangguan','ref_jenis_usaha.id_gangguan = ref_gangguan.id_gangguan', 'left');

		$this->db->join('ref_kbli','tbl_perizinan.id_kbli1 = ref_kbli.id_kbli', 'left');
		$this->db->join('ref_layanan','tbl_perizinan.id_jenis_layanan_izin = ref_layanan.id_layanan', 'left');
		$this->db->join('ref_bentuk_perusahaan','tbl_perizinan.id_bentuk_perusahaan = ref_bentuk_perusahaan.id_bentuk_perusahaan', 'left');

        $query = $this->db->get('tbl_perizinan');
        return $query->result();   
    }

public function num_rows_by_hash_registrasi($id = NULL) {
        if(!empty($id)){
            $this->db->where('hash_registrasi', $id);
        }        
        $query = $this->db->get('tbl_perizinan');
        return $query->num_rows();   
    }

public function select_by_noreg($noreg = NULL) {
        if(!empty($noreg)){
            $this->db->where('id_perizinan', $noreg);
        }        
        //$this->db->select('tbl_perizinan.*, provinsi.*, kabupaten.*, desa.*, ref_jenis_izin.*, ref_bidang_izin.*, ref_jalan.*, ref_jenis_usaha.*, ref_gangguan.*');
        $this->db->select('*');
        $this->db->select('prov.provinsi as provinsi_perusahaan');
		$this->db->select('kab.kabupaten as kabupaten_perusahaan');
		$this->db->select('kec.kecamatan as kecamatan_perusahaan');
		$this->db->select('des.desa as desa_perusahaan');

		$this->db->join('provinsi as prov','tbl_perizinan.kd_provinsi_perusahaan = prov.id_provinsi', 'left');
		$this->db->join('kabupaten as kab','tbl_perizinan.kd_kabupaten_perusahaan = kab.id_kabupaten', 'left');
		$this->db->join('kecamatan as kec','tbl_perizinan.kd_kecamatan_perusahaan = kec.id_kecamatan', 'left');
		$this->db->join('desa as des','tbl_perizinan.kd_desa_perusahaan = des.id_desa', 'left');

		$this->db->join('provinsi','tbl_perizinan.kd_provinsi_pemohon = provinsi.id_provinsi', 'left');
		$this->db->join('kabupaten','tbl_perizinan.kd_kabupaten_pemohon = kabupaten.id_kabupaten', 'left');
		$this->db->join('kecamatan','tbl_perizinan.kd_desa_kecamatan = kecamatan.id_kecamatan', 'left');
		$this->db->join('desa','tbl_perizinan.kd_desa_pemohon = desa.id_desa', 'left');

		$this->db->join('ref_jenis_izin','tbl_perizinan.id_jenis_izin = ref_jenis_izin.id_jenis_izin', 'left');
		$this->db->join('ref_bidang_izin','tbl_perizinan.id_bidang_izin = ref_bidang_izin.id_bidang', 'left');
		$this->db->join('ref_jalan','tbl_perizinan.id_jalan = ref_jalan.id_jalan', 'left');
		$this->db->join('ref_jenis_usaha','tbl_perizinan.id_jenis_usaha = ref_jenis_usaha.id_jenis_usaha', 'left');
		$this->db->join('ref_gangguan','ref_jenis_usaha.id_gangguan = ref_gangguan.id_gangguan', 'left');

		$this->db->join('ref_kbli','tbl_perizinan.id_kbli1 = ref_kbli.id_kbli', 'left');
		$this->db->join('ref_layanan','tbl_perizinan.id_jenis_layanan_izin = ref_layanan.id_layanan', 'left');
		$this->db->join('ref_bentuk_perusahaan','tbl_perizinan.id_bentuk_perusahaan = ref_bentuk_perusahaan.id_bentuk_perusahaan', 'left');

        $query = $this->db->get('tbl_perizinan');
        return $query->result();   
    }



}
?>