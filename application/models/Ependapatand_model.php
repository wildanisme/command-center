<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Ependapatand_model extends CI_model
{
	private $_table = "card_ependapatand";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $pjk_drh;
	public $ret_drh;
	public $kd;
	public $lld;
	public $tahun;

	public function rules()
	{
		return [
			[
				'field' => 'pjk_drh',
				'label' => 'Pajak Daerah',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'ret_drh',
				'label' => 'Retribusi Daerah',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'kd',
				'label' => 'KD',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'lld',
				'label' => 'LLD',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> pjk_drh = $post["pjk_drh"];
		$this -> ret_drh = $post["ret_drh"];
		$this -> kd = $post["kd"];
		$this -> lld = $post["lld"];
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> pjk_drh = $post["pjk_drh"];
		$this -> ret_drh = $post["ret_drh"];
		$this -> kd = $post["kd"];
		$this -> lld = $post["lld"];
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}