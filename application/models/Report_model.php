<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model
{
	public function get_nilai_it($kd_indikator_tambahan,$kd_desa,$bulan,$tahun)
	{
		$this->db->where('kd_indikator_tambahan',$kd_indikator_tambahan);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where('bulan',$bulan);
		$this->db->where('tahun',$tahun);
		$query = $this->db->get('ref_indikator_tambahan');
		$nilai ="";
		foreach ($query->result() as $row) {
			$nilai = $row->nilai;
		}
		return $nilai;
	}


	public function get_tempat_yg_terdata($kd_desa,$kd_kategori,$bulan,$tahun,$kd_puskesmas)
	{
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where('kd_kategori',$kd_kategori);
		$this->db->where('approve','Ya');
		$this->db->where(" month(tanggal)=$bulan AND year(tanggal) = $tahun ");
		$query = $this->db->get('tempat');
		$jml = $query->num_rows();
		if ($jml==0) $jml="";
		return $jml;
	}
	
	public function get_nilai_i($kd_indikator,$kd_desa,$bulan,$tahun,$kd_kategori,$kd_puskesmas)
	{
		$this->db->join('tempat','tempat.kd_tempat = phbs.kd_tempat','left');
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$this->db->where('tempat.kd_desa',$kd_desa);
		$this->db->where('tempat.kd_kategori',$kd_kategori);
		$this->db->where('tempat.approve','Ya');
		$this->db->where('phbs.kd_indikator',$kd_indikator);
		$this->db->where('phbs.nilai','Ya');
		$this->db->where(" month(tempat.tanggal)=$bulan AND year(tempat.tanggal) = $tahun ");
		$query = $this->db->get('phbs');
		$jml = $query->num_rows();
		if ($jml==0) $jml="";
		return $jml;
	}

	public function get_status_kesehatan($status,$kd_desa,$kd_kategori,$bulan,$tahun,$kd_puskesmas)
	{
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where('kd_kategori',$kd_kategori);
		$this->db->where('approve','Ya');
		$this->db->where('status_kesehatan',$status);
		$this->db->where(" month(tanggal)=$bulan AND year(tanggal) = $tahun ");
		$query = $this->db->get('tempat');
		$jml = $query->num_rows();
		if ($jml==0) $jml="";
		return $jml;
	}

	public function get_statistik($tahun,$bulan,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas,$kd_indikator)
	{
		$this->db->join('tempat','tempat.kd_tempat = phbs.kd_tempat','left');
		$this->db->join('desa','desa.kd_desa = tempat.kd_desa','left');
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where(" year(tempat.tanggal) = $tahun ");
		$this->db->where(" month(tempat.tanggal) = $bulan ");
		$this->db->where('tempat.kd_kategori',$kd_kategori);
		$this->db->where('tempat.approve','Ya');
		$this->db->where('phbs.nilai','Ya');
		$this->db->where('phbs.kd_indikator',$kd_indikator);
		if ($kd_kecamatan!="" && $kd_kecamatan !=0) $this->db->where('desa.kd_kecamatan',$kd_kecamatan);
		if ($kd_desa!="" && $kd_desa !=0) $this->db->where('desa.kd_desa',$kd_desa);
		if ($kd_puskesmas!="" && $kd_puskesmas !=0) $this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$query = $this->db->get('phbs');
		return $query->num_rows()	;
	}
	
	public function get_statistik_status($status_kesehatan,$tahun,$bulan,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas)
	{
		$this->db->join('desa','desa.kd_desa = tempat.kd_desa','left');
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where(" year(tempat.tanggal) = $tahun ");
		$this->db->where(" month(tempat.tanggal) = $bulan ");
		$this->db->where('tempat.kd_kategori',$kd_kategori);
		$this->db->where('tempat.approve','Ya');
		$this->db->where('tempat.status_kesehatan',$status_kesehatan);
		if ($kd_kecamatan!="" && $kd_kecamatan !=0) $this->db->where('desa.kd_kecamatan',$kd_kecamatan);
		if ($kd_desa!="" && $kd_desa !=0) $this->db->where('desa.kd_desa',$kd_desa);
		if ($kd_puskesmas!="" && $kd_puskesmas !=0) $this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$query = $this->db->get('tempat');
		return $query->num_rows()	;
	}
	public function get_grafik_phbs($tahun,$bulan,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas,$kd_indikator)
	{
		$this->db->join('tempat','tempat.kd_tempat = phbs.kd_tempat','left');
		$this->db->join('desa','desa.kd_desa = tempat.kd_desa','left');
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->where(" year(tempat.tanggal) = $tahun ");
		$this->db->where(" month(tempat.tanggal) = $bulan ");
		$this->db->where('tempat.kd_kategori',$kd_kategori);
		$this->db->where('tempat.approve','Ya');
		$this->db->where('phbs.nilai','Ya');
		if ($kd_indikator>0) $this->db->where('phbs.kd_indikator',$kd_indikator);
		if ($kd_kecamatan!="" && $kd_kecamatan !=0) $this->db->where('desa.kd_kecamatan',$kd_kecamatan);
		if ($kd_desa!="" && $kd_desa !=0) $this->db->where('desa.kd_desa',$kd_desa);
		if ($kd_puskesmas!="" && $kd_puskesmas !=0) $this->db->where('user.kd_puskesmas',$kd_puskesmas);
		$query = $this->db->get('phbs');
		return $query->num_rows()	;
	}
}
?>