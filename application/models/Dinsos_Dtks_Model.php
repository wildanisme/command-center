<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dinsos_Dtks_Model extends CI_Model
{
    private $_table = "dinsos_dtks";

    public $id_dtks;
    public $id_kec;
    public $id_desa;
    public $ruta_desil1;
    public $ruta_desil2;
    public $art_desil1;
    public $art_desil2;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_dtks" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_dtks = $post["id_dtks"];
        $this->id_kec = $post["id_kec"];
        $this->id_desa = $post["id_desa"];
        $this->ruta_desil1 = $post["ruta_desil1"];
        $this->ruta_desil2 = $post["ruta_desil2"];
        $this->art_desil1 = $post["art_desil1"];
        $this->art_desil2 = $post["art_desil2"];

        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_dtks = $post["id_dtks"];
        $this->id_kec = $post["id_kec"];
        $this->id_desa = $post["id_desa"];
        $this->ruta_desil1 = $post["ruta_desil1"];
        $this->ruta_desil2 = $post["ruta_desil2"];
        $this->art_desil1 = $post["art_desil1"];
        $this->art_desil2 = $post["art_desil2"];
        return $this->db->update($this->_table, $this, array('id_dtks' => $post['id_dtks']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_dtks" => $id));
    }
}
