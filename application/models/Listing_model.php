
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing_model extends CI_Model{

	public $user_id;

	public function get_all(){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
		$this->db->join('katalog', 'katalog.id_listing = listing.id_listing', 'left');
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		$this->db->join('foto_listing', 'foto_listing.id_listing = foto_listing.id_listing', 'left');
		$query = $this->db->get('listing');
		return $query->result();
	}

	public function get_rekomendasi(){
		$this->db->select('listing.slug_listing, kategori_listing.nama_kategori_listing, listing.nama_listing,listing.banner as banner, listing.id_listing, listing.alamat');
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		$this->db->where('listing.rekomendasi','Y');
		$this->db->limit('4','0');
		$query = $this->db->get('listing');
		return $query->result();
	}

	public function get_umkm(){
		$this->db->select('listing.slug_listing, kategori_listing.nama_kategori_listing, listing.nama_listing,listing.banner as banner, listing.id_listing, listing.alamat');
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		$this->db->where('listing.id_kategori_listing','8');
		$query = $this->db->get('listing');
		return $query->result();
	}

	public function get_total_umkm(){
		$this->db->where('listing.id_kategori_listing','8');
		$query = $this->db->get('listing');
		return $query->num_rows();
	}




	public function get_total(){
		if($this->user_id) $this->db->where("listing.id_user",$this->user_id);
		//$this->db->where('status','aktif');

		if(empty($this->front_end)){

			if($this->session->userdata("user_level")==3) //  operator desa
			{
				$this->db->where("listing.id_desa",$this->session->userdata("id_desa"));
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				$this->db->where("listing.id_kecamatan",$this->session->userdata("id_kecamatan"));
			}
		}
		$query = $this->db->get('listing');
		return $query->num_rows();
	}

	public function hitung_by_kategori(){

	$this->db->select('kategori_listing.id_kategori_listing, kategori_listing.nama_kategori_listing, kategori_listing.banner, COUNT(case listing.status when "Terverifikasi" then 1 else null end) as jml');
	$this->db->join('listing','listing.id_kategori_listing = kategori_listing.id_kategori_listing','left');
	$this->db->group_by('kategori_listing.id_kategori_listing');
	
	$query = $this->db->get('kategori_listing');
	return $query->result();
	}

	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();
		
	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null,$kategori=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("listing.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("kategori_listing.nama_kategori_listing like '%".$keyword."%' OR listing.nama_listing like '%".$keyword."%' OR listing.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "listing.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		if($kategori!=null)
		{
			$kategories = array();
			foreach($kategori as $key=>$value)
			{
				$kategories[] = "listing.id_kategori_listing  = '".$value."'";
			}
			$where_kategori = implode(" OR ",$kategories);
			$this->db->where($where_kategori);
		}
		$this->db->select("listing.*,kategori_listing.nama_kategori_listing, desa.desa, kecamatan.kecamatan");
		
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		if($this->user_id) $this->db->where("listing.id_user",$this->user_id);
		$this->db->join("desa","desa.id_desa=listing.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=listing.id_kecamatan","left");
		if ($this->session->userdata("user_level")==2) { // masyarakat
			$this->db->where('listing.status','Terverifikasi');
		}
		
		$this->db->order_by("listing.nama_listing", "ASC");
		$this->db->limit($hal,$mulai);

		if(empty($this->front_end)){

			if($this->session->userdata("user_level")==3) //  operator desa
			{
				$this->db->where("listing.id_desa",$this->session->userdata("id_desa"));
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				$this->db->where("listing.id_kecamatan",$this->session->userdata("id_kecamatan"));
			}

		}
		$query = $this->db->get('listing');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("listing")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter='',$kategori=null) {
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("listing.".$key,$value);
			}
		}


		if($keyword!="")
		{
			$this->db->where("kategori_listing.nama_kategori_listing like '%".$keyword."%' OR listing.nama_listing like '%".$keyword."%' OR listing.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "listing.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		if($kategori!=null)
		{
			$kategories = array();
			foreach($kategori as $key=>$value)
			{
				$kategories[] = "listing.id_kategori_listing  = '".$value."'";
			}
			$where_kategori = implode(" OR ",$kategories);
			$this->db->where($where_kategori);
		}
		
		//$this->db->where('id_layanan_induk','0');
		
		
		
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		
		if(empty($this->front_end)){
			if ($this->session->userdata("user_level")!=1) {
				$this->db->where('listing.status','Terverifikasi');
			}

			if($this->session->userdata("user_level")==3) //  operator desa
			{
				$this->db->where("listing.id_desa",$this->session->userdata("id_desa"));
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				$this->db->where("listing.id_kecamatan",$this->session->userdata("id_kecamatan"));
			}
		}

		$query = $this->db->get('listing');
		return $query->num_rows();
	}


	public function get_by_id($id_listing){
		$this->db->select("listing.*, kategori_listing.nama_kategori_listing, kategori_listing.kategori_slug");
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		$this->db->where('id_listing',$id_listing);
		$query = $this->db->get('listing');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_listing){
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		$this->db->where('listing.id_listing',$id_listing);
		$this->db->where('listing.status','aktif');
		$query = $this->db->get('listing');
		return $query->result();
	}




	public function insert($data){
		return $this->db->insert('listing',$data);
	}

	public function insert_katalog($data){
		return $this->db->insert('katalog',$data);
	}
	public function insert_foto($data){
		return $this->db->insert('foto_listing',$data);
	}
	
	public function update_katalog($data,$id_katalog){
		return $this->db->update('katalog',$data,array("id_katalog" => $id_katalog));
	}


	public function update($data,$id_listing){
		return $this->db->update('listing',$data,array('id_listing'=>$id_listing));
	}
	public function delete($id_listing){

		$delete = true;
		$listing = $this->get_by_id($id_listing);
		if($this->user_id){
			if($listing->id_user == $this->user_id){
				$this->db->where('id_listing',$id_listing);
				$this->db->delete('listing');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_listing',$id_listing);
			$this->db->delete('listing');
		}

		if($delete && $listing->banner!="" && file_exists("./data/listing/".$listing->banner)){
			unlink("./data/listing/".$listing->banner);
		}

		return $delete;

		
	}

	public function hapus_katalog($id_katalog) {
		$rs = $this->db->where("id_katalog",$id_katalog)->get("katalog")->row();
		if(!empty($rs->foto) && $rs->foto!=""){
			if($this->user_id){
				$listing = $this->get_by_id($rs->id_listing);
				if($listing && $listing->id_user == $this->user_id){
					if(file_exists('./data/listing/katalog/'.$rs->foto))
						unlink('./data/listing/katalog/'.$rs->foto);

					$this->db->delete('katalog',array('id_katalog'=>$id_katalog));
					return true;
					
				}
			}
			else{
				if(file_exists('./data/listing/katalog/'.$rs->foto))
					unlink('./data/listing/katalog/'.$rs->foto);

				$this->db->delete('katalog',array('id_katalog'=>$id_katalog));
				return true;
			}
		}
		

		return false;
	}
	public function hapus_foto($id_foto_listing) {
		$rs = $this->db->where("id_foto_listing",$id_foto_listing)->get("foto_listing")->row();
		
		if(!empty($rs->foto) && $rs->foto!=""){
			if($this->user_id){
				$listing = $this->get_by_id($rs->id_listing);
				if($listing && $listing->id_user == $this->user_id){
					if(file_exists('./data/listing/'.$rs->foto))
						unlink('./data/listing/'.$rs->foto);

					$this->db->delete('foto_listing',array('id_foto_listing'=>$id_foto_listing));
					return true;
				}
				
			}
			else{
				if(file_exists('./data/listing/'.$rs->foto))
					unlink('./data/listing/'.$rs->foto);

				$this->db->delete('foto_listing',array('id_foto_listing'=>$id_foto_listing));
				return true;
			}
		}
		return false;
		

	}

	public function get_katalog($id_listing){
		$this->db->where('id_listing',$id_listing);
		$query = $this->db->get('katalog');

		return $query->result();
	}

	public function get_foto($id_listing){
		$this->db->where('id_listing',$id_listing);
		$query = $this->db->get('foto_listing');

		return $query->result();
	}


	
	public function get_katalog_by_id($id_katalog){
		$this->db->where('id_katalog',$id_katalog);
		$query = $this->db->get('katalog');

		return $query->row();
	}


	


	public function hapus_listing($id_listing){
	
		$katalog = $this->get_katalog($id_listing);
		foreach($katalog as $k)
		{
			$this->hapus_katalog($k->id_katalog);	
		}
		
		$foto = $this->get_foto($id_listing);
		foreach($foto as $f)
		{
			$this->hapus_foto($f->id_foto_listing);	
		}

		$rs = $this->get_by_id($id_listing);
		if(!empty($rs->banner) && $rs->banner!=""){
			if(file_exists('./data/listing/'.$rs->banner))
				unlink('./data/listing/'.$rs->banner);
		}
		$this->db->delete('listing',array('id_listing'=>$id_listing));

	}

	public function update_jadwal($data,$id_listing){

		$this->db->where('id_listing', $id_listing);
		$this->db->update('listing',$data);
	}

	public function get($param=null)
	{
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("listing.*,kategori_listing.nama_kategori_listing, desa.desa, kecamatan.kecamatan");
		$this->db->join('kategori_listing', 'kategori_listing.id_kategori_listing = listing.id_kategori_listing', 'left');
		
		$this->db->join("desa","desa.id_desa=listing.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=listing.id_kecamatan","left");

		$this->db->order_by("listing.nama_listing", "ASC");
		
		$query = $this->db->get('listing');
		return $query->result();
	}
	

	public function is_authorize($id)
	{
		
		$data = $this->get_by_id($id);
		if(!$data)
		{
			return false;
		}
		else{
			if($this->session->userdata("user_level")==3) //  operator desa
			{
				if($data->id_desa != $this->session->userdata("id_desa"))
				{
					return false;
				}
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				if($data->id_kecamatan != $this->session->userdata("id_kecamatan"))
				{
					return false;
				}
			}
			return true;
		}
	}


	

}
