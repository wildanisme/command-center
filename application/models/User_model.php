<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	public $user_id;
	public $username;
	public $password;
	public $full_name;
	public $email;
	public $phone;
	public $bio;
	public $user_picture;
	public $user_level;
	public $user_group_menu;
	public $user_privileges;
	public $reg_date;
	public $user_status;

	public $level;
	public $user_hash;
	public $forget_password_token;
	public $email_verification_token;
	public $email_verification;
	public $email_verification_status;

	public $id_kecamatan;
	public $id_desa;

	public $search;

	public $alamat;
	public $birth_date;
	public $gender;
	public $id_mentor;


	public function validasi_login()
	{
		if($this->email!="")
			$this->db->where('email',$this->email);
		else
			$this->db->where('username',$this->username);

		$this->db->where('password',md5($this->password));
		$query = $this->db->get('user');
		if ($query->num_rows() > 0)
		{
			return true;
		}
	}

	public function cek_status_user()
	{

		if($this->email!="")
			$this->db->where('email',$this->email);
		else
			$this->db->where('username',$this->username);

		$this->db->where('user.user_status','Active');
		//$this->db->where("user.user_status not in ('Not Active')");
		$query = $this->db->get('user');
		if ($query->num_rows() > 0)
		{
			$this->set_user_by_username();
			$this->login_sukses();
			return true;
		}
		else
		{
			return false;
		}
	}

	public function login_sukses()
	{
		$CI =& get_instance();
		$CI->session->set_userdata('user_id',$this->user_id);
		$CI->session->set_userdata('username',$this->username);
		$CI->session->set_userdata('user_level',$this->user_level);
		$CI->session->set_userdata('level',$this->level);
		$CI->session->set_userdata('user_group_menu',$this->user_group_menu);
		$CI->session->set_userdata('user_privileges',$this->user_privileges);
		$CI->session->set_userdata('full_name',$this->full_name);
		$CI->session->set_userdata('email',$this->email);
		$CI->session->set_userdata('phone',$this->phone);
		$CI->session->set_userdata('user_picture',$this->user_picture);
		$CI->session->set_userdata('user_hash',$this->user_hash);
		$CI->session->set_userdata('email_verification_status',$this->email_verification_status);

		$CI->session->set_userdata('id_desa',$this->id_desa);
		$CI->session->set_userdata('id_kecamatan',$this->id_kecamatan);
		$CI->session->set_userdata('id_kabupaten',$this->id_kabupaten);
	}

	public function set_user_by_username()
	{

		$this->db->join('user_level','user_level.level_id = user.user_level','left','left');


		if($this->email!="")
			$this->db->where('email',$this->email);
		else
			$this->db->where('username',$this->username);


		$query = $this->db->get('user');
		foreach ($query->result() as $row)
		{
			$this->user_id 		= $row->user_id;
			$this->username		= $row->username;
			$this->user_level	= $row->user_level;
			$this->level 		= $row->level;
			$this->full_name	= $row->full_name;
			$this->email 		= $row->email;
			$this->phone		= $row->phone;
			$this->bio 			= $row->bio;
			$this->user_picture = $row->user_picture;
			$this->user_group_menu	= $row->user_group_menu;
			$this->user_privileges	= $row->user_privileges;
			$this->reg_date		= $row->reg_date;
			$this->user_status	= $row->user_status;
			$this->picture	= $row->user_picture;
			$this->user_hash	= $row->user_hash;
			$this->email_verification_status	= $row->email_verification_status;

			$this->id_desa	= $row->id_desa;
			$this->id_kecamatan	= $row->id_kecamatan;
			$this->id_kabupaten	= $row->id_kabupaten;
		}
	}

	public function set_user_by_user_id()
	{

		$this->db->select('user.*, user_level.*');
		$this->db->join('user_level','user_level.level_id = user.user_level','left');
		$this->db->where('user.user_id',$this->user_id);
		$query = $this->db->get('user');
		foreach ($query->result() as $row)
		{
			$this->user_id 		= $row->user_id;
			$this->username		= $row->username;
			$this->user_level	= $row->user_level;
			$this->level 		= $row->level;
			$this->full_name	= $row->full_name;
			$this->email 		= $row->email;
			$this->phone		= $row->phone;
			$this->bio 			= $row->bio;
			$this->user_picture = $row->user_picture;
			$this->user_group_menu	= $row->user_group_menu;
			$this->user_privileges	= $row->user_privileges;
			$this->reg_date		= $row->reg_date;
			$this->user_status	= $row->user_status;
			$this->picture	= $row->user_picture;
			$this->user_hash	= $row->user_hash;
			$this->email_verification_status	= $row->email_verification_status;

			$this->id_desa	= $row->id_desa;
			$this->id_kecamatan	= $row->id_kecamatan;
			$this->id_kabupaten	= $row->id_kabupaten;

		}

	}




	public function insert()
		{
			if ($this->username!="") $this->db->set('username',$this->username);
			if ($this->password!="") $this->db->set('password',md5($this->password));
			if ($this->full_name!="") $this->db->set('full_name',$this->full_name);
			if ($this->email!="") $this->db->set('email',$this->email);
			if ($this->phone!="") $this->db->set('phone',$this->phone);
			if ($this->bio!="") $this->db->set('bio',$this->bio);
			if ($this->alamat!="") $this->db->set('alamat',$this->alamat);
			if ($this->birth_date!="") $this->db->set('birth_date',$this->birth_date);
			if ($this->id_mentor!="") $this->db->set('id_mentor',$this->id_mentor);
			if ($this->gender!="") $this->db->set('gender',$this->gender);
			if ($this->user_picture!="") $this->db->set('user_picture',$this->user_picture);
			if ($this->user_status!="") $this->db->set('user_status',$this->user_status);
			if ($this->user_level!="") $this->db->set('user_level',$this->user_level);

			if ($this->id_desa) $this->db->set('id_desa',$this->id_desa);
			if ($this->id_kecamatan) $this->db->set('id_kecamatan',$this->id_kecamatan);
			if ($this->id_kabupaten) $this->db->set('id_kabupaten',$this->id_kabupaten);


			$this->db->set('reg_date',date('Y-m-d H:i:s'));
			$this->db->set('api_key',$this->generate_api_key());
			$this->db->set("user_hash",uniqid());
			$this->db->set("email_verification_token",md5(uniqid(rand(), true)));
			$this->db->insert('user');


			return true;
		}
	private function generate_api_key()
	{
		return md5(uniqid(rand(), true));
	}


	public function update()
	{

		if ($this->username!="") $this->db->set('username',$this->username);
		if ($this->password!="") $this->db->set('password',md5($this->password));
		if ($this->full_name!="") $this->db->set('full_name',$this->full_name);
		if ($this->email!="") $this->db->set('email',$this->email);
		if ($this->alamat!="") $this->db->set('alamat',$this->alamat);
		if ($this->birth_date!="") $this->db->set('birth_date',$this->birth_date);
		if ($this->gender!="") $this->db->set('gender',$this->gender);
		if ($this->phone!="") $this->db->set('phone',$this->phone);
		if ($this->bio!="") $this->db->set('bio',$this->bio);
		if ($this->user_picture!="") $this->db->set('user_picture',$this->user_picture);
		if ($this->user_status!="") $this->db->set('user_status',$this->user_status);
		if ($this->user_level!="") $this->db->set('user_level',$this->user_level);
		if ($this->forget_password_token!="") $this->db->set('forget_password_token',$this->forget_password_token);
		if ($this->email_verification!="") $this->db->set('email_verification',$this->email_verification);
		if ($this->email_verification_status!="") $this->db->set('email_verification_status',$this->email_verification_status);
		$this->db->where('user_id',$this->user_id);
		$this->db->update('user');


		return true;
	}

	public function delete()
		{
			$this->db->where('user_id',$this->user_id);
			$this->db->delete('user');
		}

	public function get_all()
	{
		if ($this->search!=""){
			$this->db->where(" username like '%$this->search%' OR full_name like '%$this->search%' ");
		}
		$this->db->join('user_level','user_level.level_id = user.user_level','left');
		$this->db->order_by('user_level','ASC');
		$query = $this->db->get('user');
		return $query->result();
	}

	public function get_by_id()
	{
		$this->db->where('user_id',$this->user_id);
		$query = $this->db->get('user');
		return $query->row();
	}


	public function get_for_page($limit,$offset)
	{


		if(!empty($this->user_level_)) {
			$this->db->where("user.user_level",$this->user_level_);
		}

		if(!empty($this->full_name_)) {
			$this->db->where("user.full_name like '%".$this->full_name_."%' ");
		}

		// $this->db->join('employee','employee.employee_id = user.employee_id','left');
		$this->db->select("user.*, user.email AS email, user_level.level ");

		$this->db->join("user_level","user_level.level_id=user.user_level","left");
		$this->db->limit($limit,$offset);

		$query = $this->db->get('user');
		return $query->result();
	}

	public function cek_username($username)
	{
		$this->db->where('username',$username);
		$query = $this->db->get('user');
		if ($query->num_rows()==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_user_level()
	{
		$query = $this->db->get('user_level');
		return $query->result();
	}


	public function get_total_row()
	{


		if(!empty($this->user_level_)) {
			$this->db->where("user.user_level",$this->user_level_);
		}

		if(!empty($this->full_name_)) {
			$this->db->where("user.full_name like '%".$this->full_name_."%' ");
		}



		$query = $this->db->get('user');
		return $query->num_rows();
	}

	public function check_avaliable($old_username,$username)
	{
		if ($old_username == $username){
			return true;
		}
		else{
			return $this->cek_username($username);
		}
	}

	public function update_privileges()
	{
		if ($this->session->userdata('user_level') == "1") {
			$this->db->set('user_group_menu',implode(";", $this->input->post('user_group_menu')));
			$this->db->set('user_privileges',implode(";", $this->input->post('user_privileges')));
			$this->db->where('user_id',$this->uri->segment(3));
			$this->db->update('user');
		}
	}

	public function update_setting(){
		$this->db->set('username',$this->username);
		if(!empty($this->password)) $this->db->set('password',$this->password);
		$this->db->where('user_id',$this->user_id);
		$this->db->update('user');
	}


	public function save()
		{

			$this->db->set('username',$this->username);
			$this->db->set('full_name',$this->full_name);
			$this->db->set('email',$this->email);
			$this->db->set('phone',$this->phone);
			$this->db->set('user_picture',$this->user_picture);
			$this->db->set('user_level',$this->user_level);
			$this->db->set('user_status',$this->user_status);
			$this->db->set('reg_date',date('Y-m-d'));
			$this->db->set('password',md5($this->password));

			$this->db->set('bio',$this->bio);
			//$this->db->set('api_key',$this->generate_api_key());
			$this->db->insert('user');

			return true;
		}

	public function x_update_profile()
	{
		if ($this->input->post('id') AND $this->input->post('name') AND $this->input->post('value')) {
			$this->db->set($this->input->post('name'),$this->input->post('value'));
			$this->db->where('user_id',$this->input->post('id'));
			$this->db->update('user');
		}
	}

	public function x_update_profile_image($id)
	{
		if ($id AND $this->input->post('userfile')) {
			$this->db->set('user_picture',$this->input->post('userfile'));
			$this->db->where('user_id',$id);
			$this->db->update('user');
			return true;
		}
	}

	public function checkApiKey($api_key)
	{
		$this->db->where("api_key",$api_key);
		$cek = $this->db->get("user")->row();
		if(count($cek)>0 && $api_key!=null){
			return $cek;
		}
		return false;
	}

	public function get($param=array())
	{
		if($param)
		{
			foreach($param as $key => $value)
			{
				$this->db->where($key,$value);
			}

		}
		$rs = $this->db->get("user")->result();
		return $rs;
	}

	public function change_password($user_id,$password)
	{
		$this->db->set("password",md5($password));
		$this->db->where("user_id",$user_id);
		$this->db->update("user");
	}
}
?>
