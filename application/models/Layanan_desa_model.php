<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_desa_model extends CI_Model
{

	public function get_all()
	{
		$this->db->order_by('id_layanan', 'DESC');
		return $this->db->get('layanan')->result();
	}

	public function get_layanan_by_id($id)
	{
		$this->db->where('id_layanan', $id);
		return $this->db->get('layanan')->row();
	}

	public function insert_layanan($data)
	{
		return $this->db->insert('layanan',$data);
	}

	public function update_layanan($data, $id)
	{
		return $this->db->update('layanan',$data, array('id_layanan' => $id));
	}

	public function verifikasi($status, $id, $berkas=null)
	{
		// $data['id_user_camat'] 	= $this->session->userdata('user_id');
		// $data['tanggal_verifikasi_camat'] 	= date('Y-m-d');

		switch ($status) {
			case 'aktif':
				$data['status'] 		= 'Aktif';
				return $this->db->update('layanan', $data, array('id_layanan' => $id));
				break;

			case 'nonaktif':
				$data['status'] 		= 'Nonaktif';
				return $this->db->update('layanan', $data, array('id_layanan' => $id));
				break;

			case 'hapus':
				return $this->db->delete('layanan', array('id_layanan' => $id));
				break;
		}

	}

}
