<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pendapatan_model extends CI_Model
{
    private $_table = "siskudes_pendapatan";

    public $uraian;
    public $anggaran;
    public $realisasi;
    public $sisa;


    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
}
