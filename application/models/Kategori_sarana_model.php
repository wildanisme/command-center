<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_sarana_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_sarana;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_sarana');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_sarana',$this->nama_kategori_sarana);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_sarana');
	}
	public function update()
	{
		$this->db->where('id_kategori_sarana',$this->id_kategori_sarana);
		$this->db->set('nama_kategori_sarana',$this->nama_kategori_sarana);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_sarana');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_sarana',$this->id_kategori_sarana);
		$query = $this->db->get('kategori_sarana');
		foreach ($query->result() as $row) {
			$this->nama_kategori_sarana 	= $row->nama_kategori_sarana;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_sarana',$this->id_kategori_sarana);
		$query = $this->db->delete('kategori_sarana');	
	}
}
?>