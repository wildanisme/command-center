<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk_model extends CI_Model{

	public $user_id;

	public function get_all($param=null, $limit=null,$offset=null){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("helpdesk.*, kategori_helpdesk.nama_kategori_helpdesk");
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		//$this->db->where('helpdesk.status','aktif');
		$this->db->order_by("helpdesk.judul", "ASC");
		if($limit!=null && $offset!=null)
		$this->db->limit($limit,$offset);
		$query = $this->db->get('helpdesk');
		return $query->result();
	}

	public function get_buka(){
		$this->db->where('status', 'Buka');
		return $this->db->get('helpdesk')->num_rows();
	}

	public function get_tutup(){
		$this->db->where('status', 'Tutup');
		return $this->db->get('helpdesk')->num_rows();
	}

	public function get_total(){
		if($this->user_id) $this->db->where("helpdesk.id_user",$this->user_id);
		//$this->db->where('status','aktif');
		$query = $this->db->get('helpdesk');
		return $query->num_rows();
	}


	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();
		
	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("helpdesk.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("kategori_helpdesk.nama_kategori_helpdesk like '%".$keyword."%' OR helpdesk.judul like '%".$keyword."%' OR helpdesk.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "helpdesk.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		$this->db->select("helpdesk.*,kategori_helpdesk.nama_kategori_helpdesk, user.user_picture");
		if($this->user_id) $this->db->where("helpdesk.id_user",$this->user_id);
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		//$this->db->where('helpdesk.status','aktif');
		$this->db->join('user', 'user.user_id = helpdesk.id_user', 'left');
		
		$this->db->order_by("helpdesk.judul", "ASC");
		$this->db->limit($hal,$mulai);
		$query = $this->db->get('helpdesk');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("helpdesk")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter=null) {
		if($keyword!="")
		{
			$this->db->where("kategori_helpdesk.nama_kategori_helpdesk like '%".$keyword."%' OR helpdesk.judul like '%".$keyword."%' OR helpdesk.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "helpdesk.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		
		if($filter!=null){
			foreach($filter as $key => $value){
				$this->db->like("helpdesk.".$key,$value);
			}
		}

		//$this->db->where('id_layanan_induk','0');
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		$query = $this->db->get('helpdesk');
		return $query->num_rows();
	}


	public function get_by_id($id_helpdesk){
		$this->db->select("helpdesk.*,kategori_helpdesk.nama_kategori_helpdesk,user.*");
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		$this->db->join('user', 'user.user_id = helpdesk.id_user', 'left');
		$this->db->where('id_helpdesk',$id_helpdesk);
		$query = $this->db->get('helpdesk');
		return $query->row();
	}

	public function get_by_id_respons($id_respons){
		$this->db->where('id_respon',$id_respons);
		$query = $this->db->get('helpdesk_respon');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_helpdesk){
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		$this->db->where('helpdesk.id_helpdesk',$id_helpdesk);
		//$this->db->where('helpdesk.status','aktif');
		$query = $this->db->get('helpdesk');
		return $query->result();
	}

	public function respons_by_id($id_helpdesk){
		$this->db->join('user', 'user.user_id = helpdesk_respon.id_user');
		$this->db->where('id_helpdesk', $id_helpdesk);
		return $this->db->get('helpdesk_respon')->result();
	}


	public function insert($data){
		return $this->db->insert('helpdesk',$data);
	}
	

	public function insert_respons($id_helpdesk, $id_user){
		$data = array(
			'id_helpdesk' => $id_helpdesk,
			'tgl_respon' => date('Y-m-d H:i:s'),
			'isi' => $this->input->post('isi'),
			'id_user' => $id_user
		);
		return $this->db->insert('helpdesk_respon', $data);
	}

	

	public function update($data,$id_helpdesk){
		return $this->db->update('helpdesk',$data,array('id_helpdesk'=>$id_helpdesk));
	}

	public function tutup_laporan($id_helpdesk){
		$this->db->where('id_helpdesk', $id_helpdesk);
		return $this->db->update('helpdesk', array('status' => 'Tutup'));
	}

	public function update_respons($id_respons){
		$this->db->where('id_respon', $id_respons);
		return $this->db->update('helpdesk_respon', array('isi' => $this->input->post('isi')));
	}

	public function delete($id_helpdesk){
		$this->db->delete('helpdesk',array('id_helpdesk'=>$id_helpdesk));
	}


	public function hapus_helpdesk($id_helpdesk){

		$delete = true;
		$helpdesk = $this->get_by_id($id_helpdesk);
		if($this->user_id){
			if($helpdesk->id_user == $this->user_id){
				$this->db->where('id_helpdesk',$id_helpdesk);
				$this->db->delete('helpdesk');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_helpdesk',$id_helpdesk);
			$this->db->delete('helpdesk');
		}

		if($delete && $helpdesk->file!="" && file_exists("./data/helpdesk/".$helpdesk->file)){
			unlink("./data/helpdesk/".$helpdesk->file);
		}

		return $delete;
		

	}

	public function hapus_helpdeskrespons($id_respons){

		$delete = true;
		$respons = $this->get_by_id_respons($id_respons);
		if($this->user_id){
			if($respons->id_user == $this->user_id){
				$this->db->where('id_respon',$id_respons);
				$this->db->delete('helpdesk_respon');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_respon',$id_respons);
			$this->db->delete('helpdesk_respon');
		}

		return $delete;
		

	}

	

	public function get_kategori()
	{
		$rs = $this->db->get("kategori_helpdesk")->result();
		return $rs;
	}

	public function get($param=null)
	{
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("helpdesk.*,kategori_helpdesk.nama_kategori_helpdesk, user.username, user.user_picture ");
		$this->db->join('kategori_helpdesk', 'kategori_helpdesk.id_kategori_helpdesk = helpdesk.id_kategori_helpdesk', 'left');
		$this->db->join('user', 'user.user_id = helpdesk.id_user', 'left');
		
		$this->db->order_by("helpdesk.judul", "ASC");
		
		$query = $this->db->get('helpdesk');
		return $query->result();
	}


	

}
