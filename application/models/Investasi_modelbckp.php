<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Investasi_model extends CI_model
{
	private $_table = "investasi";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $target_investasi;
	public $realisasi_investasi;
	public $tahun;
	public $apbd_e;
	public $agg_blj;
	public $real_blj;
	


	public function rules()
	{
		return [
			[
				'field' => 'target_investasi',
				'label' => 'Target Investasi',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'realisasi_investasi',
				'label' => 'Realiasi Investasi',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'apbd_e',
				'label' => 'Apbde',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'agg_blj',
				'label' => 'Agg Blj',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'real_blj',
				'label' => 'Real Blj',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> target_investasi = $post["target_investasi"];
		$this -> realisasi_investasi = $post["realisasi_investasi"];
		$this -> agg_blj =$post["agg_blj"];

		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> target_investasi = $post["target_investasi"];
		$this -> realisasi_investasi = $post["realisasi_investasi"];
		$this -> agg_blj =$post["agg_blj"];
		$this -> apbd_e =$post["apbd_e"];
		$this -> real_blj =$post["real_blj"];
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}