<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Isu_model extends CI_Model
{
    private $_table = "isu_berita";
	private $_tablekategori = "isu_kategori";

    public $berita_id;
	public $tanggal;
    public $judul;
    public $deskripsi;
    public $kecamatan;
    public $kategori;
    public $sentimen;
    public $sumber;
    public $asal;
    public $media;
    public $status;

    public function rules()
    {
        return [
            ['field' => 'judul',
            'label' => 'judul',
            'rules' => 'required']

           
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["berita_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->berita_id = uniqid();
		$this->tanggal = $post["tanggal"];
        $this->judul = $post["judul"];
        $this->deskripsi = $post["deskripsi"];
        $this->kecamatan = $post["kecamatan"];
        $this->kategori = $post["kategori"];
        $this->sentimen = $post["sentimen"];
        $this->sumber = $post["sumber"];
        //$this->asal = $post["asal"];
        //$this->media = $post["media"];
       // $this->status = $post["status"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->berita_id = $post["id"];
		$this->tanggal = $post["tanggal"];
        $this->judul = $post["judul"];
        $this->deskripsi = $post["deskripsi"];
        $this->kecamatan = $post["kecamatan"];
        $this->kategori = $post["kategori"];
        $this->sentimen = $post["sentimen"];
        $this->sumber = $post["sumber"];
        $this->asal = $post["asal"];
        $this->media = $post["media"];
        $this->status = $post["status"];
        return $this->db->update($this->_table, $this, array('berita_id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("berita_id" => $id));
    }
	
	public function savekategori()
	{
		$post = $this->input->post();
		$this->kategori = $post["kategori"];
		return $this->db->insert($this->_tablekategori, $this);
	}

}
