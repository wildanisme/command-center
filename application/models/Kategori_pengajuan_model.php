<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_pengajuan_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_pengajuan;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_pengajuan');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_pengajuan',$this->nama_kategori_pengajuan);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_pengajuan');
	}
	public function update()
	{
		$this->db->where('id_kategori_pengajuan',$this->id_kategori_pengajuan);
		$this->db->set('nama_kategori_pengajuan',$this->nama_kategori_pengajuan);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_pengajuan');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_pengajuan',$this->id_kategori_pengajuan);
		$query = $this->db->get('kategori_pengajuan');
		foreach ($query->result() as $row) {
			$this->nama_kategori_pengajuan 	= $row->nama_kategori_pengajuan;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_pengajuan',$this->id_kategori_pengajuan);
		$query = $this->db->delete('kategori_pengajuan');	
	}
}
?>