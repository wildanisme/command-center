<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alamat_model extends CI_Model{
    public function get_desa(){
        //$this->db->where("kecamatan.id_kabupaten","3211"); // sumedang
        $this->db->where("desa.id_kecamatan","3211150"); // paseh
        $this->db->join("kecamatan","kecamatan.id_kecamatan = desa.id_kecamatan","left");
        $this->db->order_by("kecamatan.kecamatan","ASC");
        $this->db->order_by("desa.desa","ASC");

        if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("desa.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("desa.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
        
        $rs = $this->db->get("desa");
        return $rs->result();
    }
    
    public function get_desa_by_id($id_desa){
        $this->db->where("desa.id_desa",$id_desa); // sumedang
        $this->db->join("kecamatan","kecamatan.id_kecamatan = desa.id_kecamatan","left");
        $this->db->join("kabupaten","kabupaten.id_kabupaten = kecamatan.id_kabupaten","left");
        
        $rs = $this->db->get("desa");
        return $rs->row();
    }

    public function get_profil_desa($id_desa)
    {
        $this->db->where("desa.id_desa",$id_desa);
        $this->db->join("desa","desa.id_desa = profil_desa.id_desa","left");
        $rs = $this->db->get("profil_desa");
        return $rs->row();
    }


    public function fetch_profile_desa($param){
        //$this->db->where("kecamatan.id_kabupaten","3211"); // sumedang

        if(!empty($param['where']))
        {
            $this->db->where($param['where']);
        }
        if(!empty($param['limit']) && !empty($param['offset']))
        {
            $this->db->limit($param['limit'],$param['offset']);
        }
        $this->db->where("desa.id_kecamatan","3211150"); // paseh
        $this->db->order_by("desa.desa","ASC");
        $this->db->join("desa","desa.id_desa = profil_desa.id_desa","left");
        $rs = $this->db->get("profil_desa");
        return $rs->result();
    }
}