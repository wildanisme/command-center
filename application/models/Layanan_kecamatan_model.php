<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_kecamatan_model extends CI_Model
{

	public function get_all()
	{
		$this->db->order_by('status', 'DESC');
		$this->db->order_by('id_layanan', 'DESC');
		return $this->db->get('layanan')->result();
	}

	public function get_layanan_by_id($id)
	{
		$this->db->where('id_layanan', $id);
		return $this->db->get('layanan')->row();
	}

	public function get_layanan_by_id_for_word($id)
	{
		$this->db->where('id_layanan', $id);
		return $this->db->get('layanan');
	}

	public function verifikasi($status, $id, $berkas=null)
	{
		$data['id_user_camat'] 	= $this->session->userdata('user_id');
		$data['tanggal_verifikasi_camat'] 	= date('Y-m-d');

		switch ($status) {
			case 'setuju':
				$data['status'] 		= 'Aktif';
				$data['berkas']			= $berkas;
				break;

			case 'tolak':
				$data['status'] 		= 'Ditolak';
				$data['berkas'] 		= NULL;
				break;

			case 'batal':
				$data['status'] 		= 'Proses';
				$data['id_user_camat'] 	= NULL;
				$data['tanggal_verifikasi_camat'] 	= NULL;
				$data['berkas'] 		= NULL;
				break;

			default:
				unset($data);
				$data = array();
				break;
		}

		return $this->db->update('layanan', $data, array('id_layanan' => $id));
	}

}
