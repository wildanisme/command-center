<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mentor_model extends CI_Model
{
    public function get($param=null)
    {
        if(isset($param['where']))
        {
          $this->db->where($param['where']);
        }

        if(isset($param['limit']) && isset($param['offset']))
        {
            $this->db->limit($param['limit'],$param['offset']);
        }

        if (isset($param['search'])) {
            $this->db->where("(mentor.nama_mentor like '%".$param['search']."%' )");
        }

        if(isset($param['id_kelompok']))
        {
          $this->db->where("kelompok.id_kelompok",$param['id_kelompok']);
          $this->db->join("mentor","mentor.id_mentor = kelompok.id_mentor","left");
          $this->db->select("mentor.*");
          return $this->db->get('kelompok');
        }
        else{
          return $this->db->get('mentor');
        }

    }
}
