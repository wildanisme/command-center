<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda_model extends CI_Model{

	public $user_id;

	public function get_all($param=null, $limit=null,$offset=null){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		//$this->db->where('agenda.status','aktif');
		$this->db->order_by("agenda.nama_agenda", "ASC");
		if($limit!=null && $offset!=null)
		$this->db->limit($limit,$offset);
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("agenda.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("agenda.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
		$query = $this->db->get('agenda');
		return $query->result();
	}

	public function get_total(){
		if($this->user_id) $this->db->where("agenda.id_user",$this->user_id);
		//$this->db->where('status','aktif');
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("agenda.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("agenda.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
		$query = $this->db->get('agenda');
		return $query->num_rows();
	}


	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();
		
	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("agenda.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("kategori_agenda.nama_kategori_agenda like '%".$keyword."%' OR agenda.nama_agenda like '%".$keyword."%' OR agenda.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "agenda.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		$this->db->select("agenda.*,kategori_agenda.nama_kategori_agenda, desa.desa, kecamatan.kecamatan");
		if($this->user_id) $this->db->where("agenda.id_user",$this->user_id);
		$this->db->join("desa","desa.id_desa=agenda.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=agenda.id_kecamatan","left");
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		//$this->db->where('agenda.status','aktif');
		$this->db->order_by("agenda.nama_agenda", "ASC");
		$this->db->limit($hal,$mulai);

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("agenda.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("agenda.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$query = $this->db->get('agenda');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("agenda")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter=null) {
		if($keyword!="")
		{
			$this->db->where("kategori_agenda.nama_kategori_agenda like '%".$keyword."%' OR agenda.nama_agenda like '%".$keyword."%' OR agenda.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "agenda.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		
		if($filter!=null){
			foreach($filter as $key => $value){
				$this->db->like("agenda.".$key,$value);
			}
		}

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("agenda.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("agenda.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		//$this->db->where('id_layanan_induk','0');
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		$query = $this->db->get('agenda');
		return $query->num_rows();
	}


	public function get_by_id($id_agenda){
		$this->db->select("agenda.*,kategori_agenda.nama_kategori_agenda");
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		$this->db->where('id_agenda',$id_agenda);
		$query = $this->db->get('agenda');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_agenda){
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		$this->db->where('agenda.id_agenda',$id_agenda);
		//$this->db->where('agenda.status','aktif');
		$query = $this->db->get('agenda');
		return $query->result();
	}




	public function insert($data){
		return $this->db->insert('agenda',$data);
	}

	public function insert_katalog($data){
		return $this->db->insert('agenda',$data);
	}
	
	public function update_katalog($data,$id_katalog){
		return $this->db->update('katalog',$data,array("id_katalog" => $id_katalog));
	}


	public function update($data,$id_agenda){
		return $this->db->update('agenda',$data,array('id_agenda'=>$id_agenda));
	}
	public function delete($id_agenda){
		$this->db->delete('agenda',array('id_agenda'=>$id_agenda));
	}

	public function hapus_katalog($id_katalog) {
		$rs = $this->db->where("id_katalog",$id_katalog)->get("id_katalog")->row();
		if(!empty($rs->file) && $rs->file!=""){
			if(file_exists('./data/agenda/katalog/'.$rs->file))
				unlink('./data/agenda/katalog/'.$rs->file);
		}
		$this->db->delete('katalog',array('id'=>$id_katalog));

	}
	public function get_katalog($id_agenda){
		$this->db->where('id_agenda',$id_agenda);
		$query = $this->db->get('katalog');

		return $query->result();
	}
	
	public function get_katalog_by_id($id_katalog){
		$this->db->where('id_katalog',$id_katalog);
		$query = $this->db->get('katalog');

		return $query->row();
	}


	


	public function hapus_agenda($id_agenda){

		$delete = true;
		$agenda = $this->get_by_id($id_agenda);
		if($this->user_id){
			if($agenda->id_user == $this->user_id){
				$this->db->where('id_agenda',$id_agenda);
				$this->db->delete('agenda');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_agenda',$id_agenda);
			$this->db->delete('agenda');
		}

		if($delete && $agenda->poster!="" && file_exists("./data/agenda/".$listing->poster)){
			unlink("./data/agenda/".$agenda->poster);
		}

		return $delete;
		

	}

	public function update_jadwal($data,$id_agenda){

		$this->db->where('id_agenda', $id_agenda);
		$this->db->update('agenda',$data);
	}

	public function get_kategori()
	{
		$rs = $this->db->get("kategori_agenda")->result();
		return $rs;
	}

	public function get($param=null)
	{
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("agenda.*,kategori_agenda.nama_kategori_agenda, desa.desa, kecamatan.kecamatan");
		$this->db->join('kategori_agenda', 'kategori_agenda.id_kategori_agenda = agenda.id_kategori_agenda', 'left');
		
		$this->db->join("desa","desa.id_desa=agenda.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=agenda.id_kecamatan","left");

		$this->db->order_by("agenda.nama_agenda", "ASC");
		
		$query = $this->db->get('agenda');
		return $query->result();
	}

	public function is_authorize($id)
	{
		
		$data = $this->get_by_id($id);
		if(!$data)
		{
			return false;
		}
		else{
			if($this->session->userdata("user_level")==3) //  operator desa
			{
				if($data->id_desa != $this->session->userdata("id_desa"))
				{
					return false;
				}
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				if($data->id_kecamatan != $this->session->userdata("id_kecamatan"))
				{
					return false;
				}
			}
			return true;
		}
	}
	

}
