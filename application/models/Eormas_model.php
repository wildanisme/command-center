<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Eormas_model extends CI_model
{
	private $_table = "card_eormas";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $nama_orkesmas;
	public $jns_orkesmas;
	public $alamat;
	public $bidang_kgt;
	public $tgl_pencatatan;
	public $no_tlp;
	public $tahun;


	public function rules()
	{
		return [
			[
				'field' => 'nama_orkesmas',
				'label' => 'Nama Orkesmas',
				'rules' => 'trim|required'
			],
			[
				'field' => 'jns_orkesmas',
				'label' => 'Jenis Orkesmas',
				'rules' => 'trim|required'
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'trim|required'
			],
			[
				'field' => 'bidang_kgt',
				'label' => 'Bidang Kgt',
				'rules' => 'trim|required'
			],
			[
				'field' => 'tgl_pencatatan',
				'label' => 'Tgl Pencatatan',
				'rules' => 'trim|required'
			],
			[
				'field' => 'no_tlp',
				'label' => 'No Telp',
				'rules' => 'trim|required'
			],
			
			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

    public function count()
    {
       return $this->db->count_all_results($this->_table);
    }

	public function getAll()
	{
		$this->db->order_by('id', 'ASC');
		return $this->db->get($this->_table)->result();
	}
	

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> nama_orkesmas = $post["nama_orkesmas"];
		$this -> jns_orkesmas = $post["jns_orkesmas"];
		$this -> alamat = $post["alamat"];
		$this -> bidang_kgt = $post["bidang_kgt"];
		$this -> no_tlp = $post["no_tlp"];
		$this -> tgl_pencatatan = $post["tgl_pencatatan"];
		
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> nama_orkesmas = $post["nama_orkesmas"];
		$this -> jns_orkesmas = $post["jns_orkesmas"];
		$this -> alamat = $post["alamat"];
		$this -> bidang_kgt = $post["bidang_kgt"];
		$this -> no_tlp = $post["no_tlp"];
		$this -> tgl_pencatatan = $post["tgl_pencatatan"];
		
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}