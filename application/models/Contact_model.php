<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model
{
	public $id;
	public $name;
	public $email;
	public $subject;
	public $message;
	public $status;

	public function total_unread()
	{
		$this->db->where('status','unread');
		$query = $this->db->get('contact');
		return $query->num_rows();
	}
	public function get_all()
	{
		$this->db->order_by('id','DESC');
		$query = $this->db->get('contact');
		return $query->result();
	}
	public function update()
	{
		$this->db->where('id',$this->id);
		if ($this->name!="") $this->db->set('name',$this->name);
		if ($this->email!="") $this->db->set('email',$this->email);
		if ($this->subject!="") $this->db->set('subject',$this->subject);
		if ($this->message!="") $this->db->set('message',$this->message);
		if ($this->status!="") $this->db->set('status',$this->status);
		$this->db->update('contact');
	}
	public function insert()
	{
		$this->db->set('name',$this->name);
		$this->db->set('email',$this->email);
		$this->db->set('subject',$this->subject);
		$this->db->set('message',$this->message);
		$this->db->set('date', date("Y-m-d"));
		$this->db->set('status','unread');
		$this->db->insert('contact');
		return true;
	}
}
?>