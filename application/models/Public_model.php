<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_model extends CI_Model
{
	public $id_publikasi;
	public $category_id;
	public $judul;
	public $nama_file;
	public $tgl_posting;
	public $publish;

	public function get_all()
	{
		$query = $this->db->get('publikasi');
		return $query->result();
	}
	public function get_some_public()
	{
		$this->db->limit(10,0);
		$query = $this->db->get('publikasi');
		return $query->result();
	}
	public function insert()
	{
		$this->db->set('category_id',$this->category_id);
		$this->db->set('nama_file',$this->nama_file);
		$this->db->set('judul',$this->judul);
		$this->db->set('publish',$this->publish);
		$this->db->set('type',$this->type);
		$this->db->set('url',$this->url);
		$this->db->set('tgl_posting',date('Y-m-d'));
		$this->db->insert('publikasi');
	}
	public function update()
	{
		$this->db->where('id_publikasi',$this->id_publikasi);
		$this->db->set('category_id',$this->category_id);
		$this->db->set('nama_file',$this->nama_file);
		$this->db->set('judul',$this->judul);
		$this->db->set('type',$this->type);
		$this->db->set('url',$this->url);
		$this->db->set('publish',$this->publish);
		if ($this->nama_file!="") $this->db->set('nama_file',$this->nama_file);
		$this->db->update('publikasi');
	}
	public function delete()
	{
		$this->db->where('id_publikasi',$this->id_publikasi);
		$this->db->delete('publikasi');
	}
	public function set_by_id()
	{
		$this->db->where('id_publikasi',$this->id_publikasi);
		$query= $this->db->get('publikasi');
		foreach ($query->result() as $row) {
			$this->judul = $row->judul;
			$this->category_id = $row->category_id;
			$this->nama_file = $row->nama_file;
			$this->publish=$row->publish;
			$this->type=$row->type;
			$this->url=$row->url;
			$this->tgl_posting = $row->tgl_posting;
		}
	}
}
?>