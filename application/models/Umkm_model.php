<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Umkm_model extends CI_model
{
	private $_table = "umkm";

	public $id;
	public $nama_umkm;
	public $nama_pemilik;
	public $alamat;
	public $desa;
	public $kecamatan;
	public $no_tlp;
	public $jenis;
	public $jml_taker;
	public $hasil_usaha;
	public $asset;
	public $jk_pemilik;

	public function rules()

	{
		return [
			['field' => 'nama_umkm',
			'label' => 'Nama UMKM',
			'rules' => 'required'],

			['field' => 'nama_pemilik',
			'label' => 'Nama Pemilik',
			'rules' => 'required'],
		];
	}
public function getAll()
{
	$this->db->order_by('id', 'DESC');
	return $this->db->get($this->_table)->result();
}

public function getById($id)
{
	return $this->db->get_where($this->_table, ["id" =>$id])->row();
}

public function save()
{
	$post = $this->input->post();
	$this -> id = uniqid();
	$this -> nama_umkm = $post["nama_umkm"];
	$this -> nama_pemilik = $post["nama_pemilik"];
	$this -> alamat = $post ["alamat"];
	$this -> desa = $post ["desa"];
	$this -> kecamatan = $post ["kecamatan"];
	$this -> no_tlp = $post ["no_tlp"];
	$this -> jenis = $post ["jenis"];
	$this -> jml_taker = $post ["jml_taker"];
	$this -> hasil_usaha = $post ["hasil_usaha"];
	$this -> asset = $post ["asset"];
	$this -> jk_pemilik = $post["jk_pemilik"];
	return $this->db->insert($this->_table, $this);
}

public function update()
{
	$post = $this->input->post();
	$this -> id = $post["id"];
	$this -> namaumkm = $post["nama_umkm"];
	$this -> namapemilik = $post["nama_pemilik"];
	$this -> alamat = $post ["alamat"];
	$this -> desa = $post ["desa"];
	$this -> kecamatan = $post ["kecamatan"];
	$this -> no_telp = $post ["no_tlp"];
	$this -> jenis = $post ["jenis"];
	$this -> jml_taker = $post ["jml_taker"];
	$this -> hasil_usaha = $post ["hasil_usaha"];
	$this -> asset = $post ["asset"];
	$this -> jk_pemilik = $post["jk_pemilik"];
	return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
}

public function delete ($id)
{
	return $this->db->delete ($this->_table, array ("id" => $id));
}
}