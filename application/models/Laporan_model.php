<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model
{
	
	public function getRataWaktuPelayanan($id_skpd=null, $mulai=null, $selesai=null, $id_layanan=null)
	{
		$this->db->select("avg(((HOUR(timediff(selesai_dilayani,mulai_dilayani)) *3600) + (MINUTE(timediff(selesai_dilayani,mulai_dilayani))*60) + SECOND(timediff(selesai_dilayani,mulai_dilayani)) )) as 'rata_detik'");
		$this->db->where("antrian.selesai_dilayani is not null");
		
		if($mulai!=null && $selesai!=null)
		{
			$this->db->where("antrian.tgl_antrian >= '".$mulai."' AND antrian.tgl_antrian <= '".$selesai."' ");
		}
		if($id_skpd!=null)
		{
			$this->db->where("ref_layanan.id_skpd",$id_skpd);
			$this->db->join("ref_layanan","ref_layanan.id_layanan=antrian.id_layanan","left");
		}
		if($id_layanan!=null)
		{
			$this->db->where("antrian.id_layanan",$id_layanan);
			
		}
		$rs = $this->db->get("antrian")->row();
		
		$waktu = $this->converToTime($rs->rata_detik);
		
		
		return $waktu;
	}
	
	public function getRating($id_skpd=null, $mulai=null, $selesai=null, $id_layanan=null)
	{
		$this->db->select(" avg(antrian.rating) as 'rating'");
		$this->db->where("antrian.selesai_dilayani is not null");
		
		if($mulai!=null && $selesai!=null)
		{
			$this->db->where("antrian.tgl_antrian >= '".$mulai."' AND antrian.tgl_antrian <= '".$selesai."' ");
		}
		if($id_skpd!=null)
		{
			$this->db->where("ref_layanan.id_skpd",$id_skpd);
			$this->db->join("ref_layanan","ref_layanan.id_layanan=antrian.id_layanan","left");
		}
		if($id_layanan!=null)
		{
			$this->db->where("antrian.id_layanan",$id_layanan);
			
		}
		$rs = $this->db->get("antrian")->row();
		
		return number_format($rs->rating,1);
	}
	
	private function converToTime($total=null)
	{
		if($total==null) return "00:00:00";
		$jam = $menit = "00";
		$mod = 0;
		
		if($total>=3600)
		{
			$jam = number_format(floor($total/3600));
			$mod = $total % 3600;
		}
		else{
			$mod = $total;
		}
		
		if($mod>=60){
			$menit = number_format(floor($mod/60));
			$mod = $mod % 60;
		}
		if($jam<10 && $jam >0) $jam = "0".$jam;
		if($menit<10 && $menit>0) $menit = "0".$menit;
		if($mod<10 && $mod >0) $mod = "0".$mod;
		
		
		return $jam.":".$menit.":".$mod;
	}
	
	public function getLayanan($id_skpd=null)
	{
//		$sub = $this->db->where("id_layanan_induk > 0")->group_by("id_layanan_induk")->get("ref_layanan")->result();
//		$subArr = array();
//		foreach($sub as $s)
//		{
//			$subArr[] = $s->id_layanan_induk;
//		}
		
		
//		$this->db->select("layanan.*,  skpd.nama_skpd, induk.nama_layanan as 'induk_layanan'");
		$this->db->select("layanan.*,  skpd.nama_skpd");
		
//		$this->db->where_not_in("layanan.id_layanan",$subArr);
		
		if($id_skpd!=null) $this->db->where("skpd.id_skpd",$id_skpd);
		
		
		$this->db->join("ref_skpd as skpd","skpd.id_skpd=layanan.id_skpd","left");
//		$this->db->join("ref_layanan as induk","induk.id_layanan=layanan.id_layanan_induk","left");
		$this->db->order_by("layanan.id_skpd","ASC");
//		$this->db->order_by("layanan.id_layanan_induk","ASC");
		$layanan = $this->db->get("ref_layanan as layanan")->result();
		return $layanan;
	}
	
	public function getTotal($id_skpd=null, $mulai=null, $selesai=null, $id_layanan=null,$jenis=null,$status_antrian=null)
	{
		$this->db->select("count(id_antrian) as 'total'");
		//$this->db->where("antrian.selesai_dilayani is not null");
		
		if($mulai!=null && $selesai!=null)
		{
			$this->db->where("antrian.tgl_antrian >= '".$mulai."' AND antrian.tgl_antrian <= '".$selesai."' ");
		}
		if($id_skpd!=null)
		{
			$this->db->where("ref_layanan.id_skpd",$id_skpd);
			$this->db->join("ref_layanan","ref_layanan.id_layanan=antrian.id_layanan","left");
		}
		if($id_layanan!=null)
		{
			$this->db->where("antrian.id_layanan",$id_layanan);
			
		}
		if($jenis!=null)
		{
			$this->db->where("antrian.jenis",$jenis);
			
		}
		if($status_antrian!=null)
		{
			$this->db->where("antrian.status_antrian",$status_antrian);
			
		}
		$rs = $this->db->get("antrian")->row();
		
		
		
		
		return $rs->total;
	}

	public function get_total(){
		$this->db->where('status_antrian','Selesai');
		$query = $this->db->get('antrian');
		return $query->num_rows();
	}

	
}
?>