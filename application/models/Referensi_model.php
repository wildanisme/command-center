<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi_model extends CI_Model
{
	public $kd_kecamatan;
	public $kd_kabupaten;
	public $kecamatan;

	public $kd_desa;
	public $desa;
	public $kd_puskesmas;
	public function get_kecamatan()
	{
		if($this->kd_kecamatan!="" && $this->kd_kecamatan != 0) $this->db->where('kd_kecamatan',$this->kd_kecamatan);
		$this->db->order_by('kecamatan','ASC');
		$query = $this->db->get('kecamatan');
		return $query->result();
	}	
	public function get_desa_by_kecamatan()
	{
		$this->db->where('kd_kecamatan',$this->kd_kecamatan);
		$query = $this->db->get('desa');
		return $query->result();
	}
	public function get_puskesmas_by_kecamatan()
	{

		if($this->kd_puskesmas!="" && $this->kd_puskesmas != 0) $this->db->where('kd_puskesmas',$this->kd_puskesmas);
		$this->db->join('desa','desa.kd_desa = puskesmas.kd_desa','left');
		$this->db->where('desa.kd_kecamatan',$this->kd_kecamatan);
		$query = $this->db->get('puskesmas');
		return $query->result();
	}
	public function set_kecamatan()
	{
		$this->db->where('kd_kecamatan',$this->kd_kecamatan);
		$query = $this->db->get('kecamatan');
		foreach ($query->result() as $row) {
			$this->kecamatan = $row->kecamatan;
		}
	}
	public function set_desa()
	{
		$this->db->where('kd_desa',$this->kd_desa);
		$query = $this->db->get('desa');
		foreach ($query->result() as $row) {
			$this->desa= $row->desa;
		}
	}
}
?>