<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_organisasi_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_organisasi;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_organisasi');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_organisasi',$this->nama_kategori_organisasi);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_organisasi');
	}
	public function update()
	{
		$this->db->where('id_kategori_organisasi',$this->id_kategori_organisasi);
		$this->db->set('nama_kategori_organisasi',$this->nama_kategori_organisasi);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_organisasi');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_organisasi',$this->id_kategori_organisasi);
		$query = $this->db->get('kategori_organisasi');
		foreach ($query->result() as $row) {
			$this->nama_kategori_organisasi 	= $row->nama_kategori_organisasi;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_organisasi',$this->id_kategori_organisasi);
		$query = $this->db->delete('kategori_organisasi');	
	}
}
?>