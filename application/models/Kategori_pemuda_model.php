<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_pemuda_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_pemuda;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_pemuda');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_pemuda',$this->nama_kategori_pemuda);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_pemuda');
	}
	public function update()
	{
		$this->db->where('id_kategori_pemuda',$this->id_kategori_pemuda);
		$this->db->set('nama_kategori_pemuda',$this->nama_kategori_pemuda);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_pemuda');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_pemuda',$this->id_kategori_pemuda);
		$query = $this->db->get('kategori_pemuda');
		foreach ($query->result() as $row) {
			$this->nama_kategori_pemuda 	= $row->nama_kategori_pemuda;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_pemuda',$this->id_kategori_pemuda);
		$query = $this->db->delete('kategori_pemuda');	
	}
}
?>