<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_model extends CI_Model{

	public $user_id;

	public function get_all(){
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("pengajuan.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("pengajuan.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		$query = $this->db->get('pengajuan');
		return $query->result();
	}

	public function get_total(){
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("pengajuan.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("pengajuan.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
		if($this->user_id) $this->db->where("pengajuan.id_user",$this->user_id);
		//$this->db->where('status','aktif');
		$query = $this->db->get('pengajuan');
		return $query->num_rows();
	}


	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();
		
	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("pengajuan.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("kategori_pengajuan.nama_kategori_pengajuan like '%".$keyword."%' OR pengajuan.nama_pengajuan like '%".$keyword."%' OR pengajuan.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "pengajuan.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("pengajuan.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("pengajuan.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$this->db->select("pengajuan.*,kategori_pengajuan.nama_kategori_pengajuan");

		if($this->user_id) $this->db->where("pengajuan.id_user",$this->user_id);
		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		//$this->db->where('pengajuan.status','aktif');
		$this->db->order_by("pengajuan.nama_pengajuan", "ASC");
		$this->db->limit($hal,$mulai);
		$query = $this->db->get('pengajuan');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("pengajuan")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter=null) {
		if($keyword!="")
		{
			$this->db->where("kategori_pengajuan.nama_kategori_pengajuan like '%".$keyword."%' OR pengajuan.nama_pengajuan like '%".$keyword."%' OR pengajuan.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "pengajuan.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		if($filter!=null){
			foreach($filter as $key => $value){
				$this->db->like("pengajuan.".$key,$value);
			}
		}

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("pengajuan.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("pengajuan.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
		
		//$this->db->where('id_layanan_induk','0');
		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		$query = $this->db->get('pengajuan');
		return $query->num_rows();
	}


	public function get_by_id($id_pengajuan){
		$this->db->select("pengajuan.*,user.full_name,user.username,user.email,user.phone,organisasi.nama_organisasi,kategori_pengajuan.nama_kategori_pengajuan");
		$this->db->join('user', 'user.user_id = pengajuan.id_user', 'left');
		$this->db->join('organisasi', 'organisasi.id_organisasi = pengajuan.id_organisasi', 'left');
		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		$this->db->where('id_pengajuan',$id_pengajuan);
		$query = $this->db->get('pengajuan');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_pengajuan){
		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		$this->db->where('pengajuan.id_pengajuan',$id_pengajuan);
		$this->db->where('pengajuan.status','aktif');
		$query = $this->db->get('pengajuan');
		return $query->result();
	}




	public function insert($data){
		return $this->db->insert('pengajuan',$data);
	}


	public function update($data,$id_pengajuan){
		return $this->db->update('pengajuan',$data,array('id_pengajuan'=>$id_pengajuan));
	}
	public function delete($id_pengajuan){
		$this->db->delete('pengajuan',array('id_pengajuan'=>$id_pengajuan));
	}

	


	


	public function get_kategori()
	{
		$rs = $this->db->get("kategori_pengajuan")->result();
		return $rs;
	}

	public function get($param=null)
	{
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("pengajuan.*,kategori_pengajuan.nama_kategori_pengajuan, organisasi.nama_organisasi");
		$this->db->join('kategori_pengajuan', 'kategori_pengajuan.id_kategori_pengajuan = pengajuan.id_kategori_pengajuan', 'left');
		$this->db->join('organisasi', 'organisasi.id_organisasi = pengajuan.id_organisasi', 'left');
		$this->db->order_by("pengajuan.nama_pengajuan", "ASC");
		
		$query = $this->db->get('pengajuan');
		return $query->result();
	}


	public function hapus_pengajuan($id_pengajuan){
		$delete = true;
		$pengajuan = $this->get_by_id($id_pengajuan);
		//echo "<pre>";print_r($pengajuan);die;
		if($this->user_id){
			if($pengajuan->id_user == $this->user_id){
				$this->db->where('id_pengajuan',$id_pengajuan);
				$this->db->delete('pengajuan');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_pengajuan',$id_pengajuan);
			$this->db->delete('pengajuan');
		}

		if($delete && $pengajuan->foto!=""){
			unlink("./data/pengajuan/".$pengajuan->foto);
		}

		return $delete;
		

	}

	public function is_authorize($id)
	{
		
		$data = $this->get_by_id($id);
		if(!$data)
		{
			return false;
		}
		else{
			if($this->session->userdata("user_level")==3) //  operator desa
			{
				if($data->id_desa != $this->session->userdata("id_desa"))
				{
					return false;
				}
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				if($data->id_kecamatan != $this->session->userdata("id_kecamatan"))
				{
					return false;
				}
			}
			return true;
		}
	}

	

}
