<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Investasiopd_model extends CI_model
{
	private $_table = "investasio";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $nama_opd;	
	public $apbd;	
	public $target_investasio;
	public $realisasi_investasio;
	public $tahun;
	public $agg_bljo;
	public $real_bljo;
	


	public function rules()
	{
		return [
			[
				'field' => 'target_investasio',
				'label' => 'Target Investasi',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'realisasi_investasio',
				'label' => 'Realiasi Investasi',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'nama_opd',
				'label' => 'Nama Opd',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'apbd',
				'label' => 'Apbd',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'agg_bljo',
				'label' => 'Agg Blj',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'real_bljo',
				'label' => 'Real Blj',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}



	public function getRealisasiPendapatanByTahun($tahun) 
	{
		$this->db->select_sum('agg_bljo', 'total_realisasi_pendapatan');
		$this->db->where('tahun', $tahun);
		$result = $this->db->get($this->_table)->row();
		
		return $result->total_realisasi_pendapatan;
	}
	
	public function getRealisasiBelanjaByTahun($tahun) 
	{
		$this->db->select_sum('real_bljo', 'total_realisasi_belanja');
		$this->db->where('tahun', $tahun);
		$result = $this->db->get($this->_table)->row();
		
		return $result->total_realisasi_belanja;
	}
	public function getTargetInvestasioByTahun($tahun) 
	{
		$this->db->select_sum('target_investasio', 'total_target_investasio');
		$this->db->where('tahun', $tahun);
		$result = $this->db->get($this->_table)->row();
		
		return $result->total_target_investasio;
	}
	public function getRealisasiInvestasioByTahun($tahun) 
	{
		$this->db->select_sum('realisasi_investasio', 'total_realisasi_investasio');
		$this->db->where('tahun', $tahun);
		$result = $this->db->get($this->_table)->row();
		
		return $result->total_realisasi_investasio;
	}
	public function getApbdByTahun($tahun) 
	{
		$this->db->select_sum('apbd', 'total_apbd');
		$this->db->where('tahun', $tahun);
		$result = $this->db->get($this->_table)->row();
		
		return $result->total_apbd;
	}
	

	public function save()
	{
		$post = $this->input->post();
		$this -> target_investasio = $post["target_investasio"];
		$this -> realisasi_investasio = $post["realisasi_investasio"];
		$this -> agg_bljo =$post["agg_bljo"];
		$this -> nama_opd =$post["nama_opd"];
		$this -> apbd =$post["apbd"];
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> target_investasio = $post["target_investasio"];
		$this -> realisasi_investasio = $post["realisasi_investasio"];
		$this -> agg_bljo =$post["agg_bljo"];
		$this -> nama_opd =$post["nama_opd"];
		$this -> apbd =$post["apbd"];
		$this -> real_bljo =$post["real_bljo"];
		$this -> tahun = $post["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}