<?php

class Parkir_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $parkir_db = $this->load->database('parkir', TRUE);

        $this->parkir_db = $parkir_db;
    }
    public function get_all($tanggal = '')
    {
        if ($tanggal !== '') {
            $this->parkir_db->where('DATE(tanggal_bayar)', $tanggal);
        }
        $this->parkir_db->where('status_transaksi', 'sukses');
        $this->parkir_db->join('ref_kendaraan', 'ref_kendaraan.id_kendaraan = data_parkir.id_kendaraan');
        return $this->parkir_db->get('data_parkir')->result();
    }
}
