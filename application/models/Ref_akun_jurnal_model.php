<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ref_akun_jurnal_model extends CI_Model
{


    public function get($param=null)
    {
        if(isset($param['where']))
        {
          $this->db->where($param['where']);
        }

        $this->db->join("ref_akun_jurnal induk","induk.id_akun = ref_akun_jurnal.id_induk","left");
        $this->db->select("ref_akun_jurnal.*, induk.nama_akun as 'nama_akun_induk'");

        return $this->db->get('ref_akun_jurnal');
    }

}
