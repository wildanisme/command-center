<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sarana_model extends CI_Model{

	public function get_all(){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
		
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("sarana.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("sarana.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		$this->db->join('foto_sarana', 'foto_sarana.id_sarana = foto_sarana.id_sarana', 'left');
		$query = $this->db->get('sarana');
		return $query->result();
	}

	public function get_total(){
		$this->db->where('status','aktif');
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("sarana.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("sarana.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}
		$query = $this->db->get('sarana');
		return $query->num_rows();
	}


	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();

	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		//$this->db->select("sarana.*,sarana.id_sarana as id, foto_sarana.*, kategori_sarana.*");
		$this->db->select("sarana.*,sarana.id_sarana as id, kategori_sarana.*, desa.desa");
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("sarana.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("kategori_sarana.nama_kategori_sarana like '%".$keyword."%' OR sarana.nama_sarana like '%".$keyword."%' OR sarana.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "sarana.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}

		$this->db->join("desa","desa.id_desa=sarana.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=sarana.id_kecamatan","left");
		

		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		//$this->db->join('foto_sarana', 'foto_sarana.id_sarana = sarana.id_sarana', 'left');
		$this->db->where('sarana.status','aktif');
		$this->db->order_by("sarana.nama_sarana", "ASC");
		$this->db->group_by("sarana.id_sarana");
		$this->db->limit($hal,$mulai);

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("sarana.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("sarana.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}


		$query = $this->db->get('sarana');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("sarana")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter=null) {
		if($keyword!="")
		{
			$this->db->where("kategori_sarana.nama_kategori_sarana like '%".$keyword."%' OR sarana.nama_sarana like '%".$keyword."%' OR sarana.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "sarana.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}

		if($filter!=null){
			foreach($filter as $key => $value){
				$this->db->like("sarana.".$key,$value);
			}
		}
		

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("sarana.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("sarana.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}


		//$this->db->where('id_layanan_induk','0');
		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		$query = $this->db->get('sarana');
		return $query->num_rows();
	}


	public function get_by_id($id_sarana){
		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		//$this->db->join('foto_sarana', 'foto_sarana.id_sarana = sarana.id_sarana', 'left');
		$this->db->where('sarana.id_sarana',$id_sarana);
		$query = $this->db->get('sarana');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_sarana){
		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		$this->db->join('foto_sarana', 'foto_sarana.id_sarana = sarana.id_sarana', 'left');
		$this->db->where('sarana.id_sarana',$id_sarana);
		$this->db->where('sarana.status','aktif');
		$query = $this->db->get('sarana');
		return $query->result();
	}




	public function insert($data){
		return $this->db->insert('sarana',$data);
	}

	public function insert_foto($data){
		return $this->db->insert('foto_sarana',$data);
	}
	
	public function update_foto($data,$id_sarana){
		return $this->db->update('foto',$data,array("id_sarana" => $id_sarana));
	}


	public function update($data,$id_sarana){
		return $this->db->update('sarana',$data,array('id_sarana'=>$id_sarana));
	}
	public function delete($id_sarana){
		$this->db->delete('sarana',array('id_sarana'=>$id_sarana));
	}

	public function hapus_foto($id_foto_sarana) {
		$rs = $this->db->where("id_foto_sarana",$id_foto_sarana)->get("foto_sarana")->row();
		if(!empty($rs->file) && $rs->file!=""){
			if(file_exists('./data/sarana/'.$rs->file))
				unlink('./data/sarana/'.$rs->file);
		}
		$this->db->delete('foto_sarana',array('id_foto_sarana'=>$id_foto_sarana));

	}
	public function get_foto($id_sarana){
		$this->db->where('id_sarana',$id_sarana);
		$query = $this->db->get('foto_sarana');

		return $query->result();
	}
	
	
	


	public function hapus_sarana($id_sarana){

		$this->db->where('id_sarana',$id_sarana);
		$this->db->delete('sarana');

		$this->db->where('id_sarana',$id_sarana);
		$this->db->delete('foto_sarana');



	}


	public function get_random($id_sarana=null){
		if($id_sarana)
		{
			$this->db->where("sarana.id_sarana != ",$id_sarana);
		}	
		$this->db->select("sarana.*,sarana.id_sarana as id, foto_sarana.*, kategori_sarana.*");
		$this->db->join('kategori_sarana', 'kategori_sarana.id_kategori_sarana = sarana.id_kategori_sarana', 'left');
		$this->db->join('foto_sarana', 'foto_sarana.id_sarana = foto_sarana.id_sarana', 'left');
		$this->db->limit('4');
		$this->db->group_by("sarana.id_sarana");
		$query = $this->db->get('sarana');
		return $query->result();
	}
	


	
	public function is_authorize($id)
	{
		
		$data = $this->get_by_id($id);
		if(!$data)
		{
			return false;
		}
		else{
			if($this->session->userdata("user_level")==3) //  operator desa
			{
				if($data->id_desa != $this->session->userdata("id_desa"))
				{
					return false;
				}
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				if($data->id_kecamatan != $this->session->userdata("id_kecamatan"))
				{
					return false;
				}
			}
			return true;
		}
	}

	

}
