<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Eparpol_model extends CI_model
{
	private $_table = "card_eparpol";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $total_parpol;
	public $tahun;

	public function rules()
	{
		return [
			[
				'field' => 'total_parpol',
				'label' => 'Total parpol',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> total_parpol = $post["total_parpol"];
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> total_parpol = $post["total_parpol"];
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}