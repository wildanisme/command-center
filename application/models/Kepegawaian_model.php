<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * 
 */
class Kepegawaian_model extends CI_Model
{

	// public function create_view()
	// {
	// 	$query = "CREATE OR REPLACE VIEW pegawai_view AS SELECT id_pegawai, nip, nama, jen_kel, CONCAT(golongan,'','/','',ruang) golongan, id_jabatan, jabatan.eselon as eselon, jabatan FROM pegawai JOIN golongan ON golongan.id = pegawai.id_gol JOIN pegawai_jabatan ON pegawai.id = pegawai_jabatan.id_pegawai JOIN jabatan ON jabatan.id = pegawai_jabatan.id_jabatan ";
	// 	return $this->db->query($query);	
	// }

	public function get_pegawai_view()
	{
		//$this->create_view();

		$this->db->order_by('golongan', 'DESC');
		$query = $this->db->get('pegawai_view');
		return $query->result_array();
	}

	// public function get_all_pegawai(){
	// 	$query = "CREATE OR REPLACE VIEW pegawai_all AS SELECT * FROM pegawai";

	// 	$this->db->order_by('id_gol','DESC');
	// 	$query = $this->db->get('pegawai_all');
	// 	return $this->db->query($query);
	// }

	// public function select_by_id_pegawai($nip)
	// {
	// 	$this->create_view();

	// 	$this->db->order_by('nip','ASC');
	// 	$this->db->where('nip', $nip);
	// 	$query = $this->db->get('pegawai_view');
	// 	return $query->row();
	// }

	// public function create_mutasi(){
	// 	$query = "CREATE OR REPLACE VIEW mutasi_view AS SELECT mutasi.id as mutasi_id, tgl_mutasi, nip,nama, jab_lama.jabatan as jabatan_lama, jab_baru.jabatan as jabatan_baru, unit_kerja FROM mutasi JOIN pegawai ON pegawai.id = mutasi.id_pegawai JOIN jabatan AS jab_lama ON jab_lama.id = mutasi.id_jab_lama JOIN jabatan AS jab_baru ON jab_baru.id = mutasi.id_jab_baru JOIN unit_kerja ON unit_kerja.id = mutasi.id_unker" ;
	// 	return $this->db->query($query);
	// }

	// public function get_all_mutasi(){
	// 	$this->create_mutasi();
	// 	$this->db->order_by('mutasi_id','ASC');
	// 	$query = $this->db->get('mutasi_view');
	// 	return $query->result_array();
	// }

	// public function get_one_mutasi(){
	// 	$parameter = array('mutasi_id' => $id);
	//     return $this->db->get_where('mutasi', $parameter);
	// }

	// public function save_mutasi(){
	// 	$data = array('id_pegawai','id_jab_lama','id_jab_baru','tgl_mutasi','id_unker' => $this->input->post('id_pegawai','id_jab_lama','id_jab_baru','tgl_mutasi','id_unker'));
	// 	$this->db->insert('mutasi', $data);
	// }

	// public function edit_mutasi(){
	// 	$data = array('id_pegawai','id_jab_lama','id_jab_baru','tgl_mutasi','id_unker' => $this->input->post('id_pegawai','id_jab_lama','id_jab_baru','tgl_mutasi','id_unker'));
	// 	$this->db->where('mutasi_id', $this->input->post('mutasi_id'));
	// 	 $this->db->update('mutasi', $data);
	// }

	// public function del_mutasi(){
	// 	$this->db->where('mutasi_id', $id);
	//     $this->db->delete('mutasi');
	// }

	// public function create_jabatan(){
	// 	$query = "CREATE OR REPLACE VIEW jabatan_view AS SELECT * FROM jabatan" ;
	// 	return $this->db->query($query);
	// }
	// public function get_all_jabatan(){
	// 	$this->create_jabatan();
	// 	$this->db->order_by('eselon','ASC');
	// 	$query = $this->db->get('jabatan_view');
	// 	return $query->result_array();
	// }

	// public function get_one_jabatan(){
	// 	$parameter = array('id' => $id);
	//     return $this->db->get_where('jabatan', $parameter);
	// }

	// public function save_jabatan(){
	// 	$data = array('id','jabatan','eselon','jen_jab' => $this->input->post('id','jabatan','eselon','jen_jab'));
	// 	$this->db->insert('jabatan', $data);
	// }

	// public function edit_jabatan(){
	// 	$data = array('id','jabatan','eselon','jen_jab' => $this->input->post('id','jabatan','eselon','jen_jab'));
	// 	$this->db->where('id', $this->input->post('id'));
	// 	 $this->db->update('jabatan', $data);
	// }

	// public function del_jabatan(){
	// 	$this->db->where('id', $id);
	//     $this->db->delete('jabatan');
	// }



}
