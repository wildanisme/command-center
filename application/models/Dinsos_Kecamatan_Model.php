<?php defined('BASEPATH') or exit('No direct script access allowed');

class Dinsos_Kecamatan_Model extends CI_Model
{
    private $_table = "dinsos_kec";

    public $id_kec;
    public $nama_kec;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_kec" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_kec = $post["id_kec"];
        $this->nama_kec = $post["nama_kec"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_kec = $post["id_kec"];
        $this->nama_kec = $post["nama_kec"];
        return $this->db->update($this->_table, $this, array('id_kec' => $post['id_kec']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_kec" => $id));
    }
}
