<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model
{

	public function check_api_key($api_key)
	{
		$this->db->where('api_key',$api_key);
		$query = $this->db->get('user');
		return $query->num_rows() > 0 ;
	}
	public function login($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',md5($password));
		$this->db->where('user_status','Active');
		$query = $this->db->get('user');
		return $query->num_rows() > 0 ;
	}
	public function get_user($username)
	{
		$this->db->join('puskesmas','puskesmas.kd_puskesmas = user.kd_puskesmas','left');
		$this->db->join('desa','desa.kd_desa = puskesmas.kd_desa','left');
		$this->db->join('user_level','user_level.level_id = user.user_level','left');
		$this->db->select("user.user_id, user.username, user.full_name, user.email, user.phone, user.bio, user.user_picture, 
			user_level.level_id, user_level.level, puskesmas.kd_puskesmas, puskesmas.nama_puskesmas, 
		 user.reg_date, user.user_status,  user.nip, user.api_key, 
		 desa.kd_desa, desa.kd_kecamatan ");
		$this->db->where('user.username',$username);
		$query = $this->db->get('user');
		return $query->result();
	}
	public function get_kategori_tempat()
	{
		$query = $this->db->get('kategori_tempat');
		return $query->result();
	}
	public function get_pelaksana()
	{
		$query = $this->db->get('pelaksana');
		return $query->result();
	}
	public function get_puskesmas($kd_puskesmas)
	{
		if($kd_puskesmas>0) $this->db->where('kd_puskesmas',$kd_puskesmas);
		$query = $this->db->get('puskesmas');
		return $query->result();
	}
	public function get_kecamatan($kd_kecamatan)
	{
		if($kd_kecamatan!="" && $kd_kecamatan!="null") $this->db->where('kd_kecamatan',$kd_kecamatan);
		$this->db->order_by('kecamatan','ACS');
		$query = $this->db->get('kecamatan');
		return $query->result();
	}
	public function get_indikator($kd_kategori)
	{
		$this->db->where('kd_kategori',$kd_kategori);
		$this->db->order_by('kd_indikator','ACS');
		$query = $this->db->get('indikator');
		return $query->result();
	}
	public function get_desa_by_kec($kd_kecamatan)
	{
		$this->db->where('kd_kecamatan',$kd_kecamatan);
		$this->db->order_by('desa','ACS');
		$query = $this->db->get('desa');
		return $query->result();
	}
	public function update_profile($api_key,$username, $password, $full_name, $email,$phone, $nip)
	{
		$this->db->set('username',$username);
		if ($password!="") $this->db->set('password',md5($password));
		$this->db->set('full_name',$full_name);
		$this->db->set('email',$email);
		$this->db->set('phone',$phone);
		$this->db->set('nip',$nip);
		$this->db->where('api_key',$api_key);
		$this->db->update('user');
	}
	public function insert_penyuluhan($data)
	{
		$this->db->insert('penyuluhan',$data);
	}

	public function update_penyuluhan($kd_penyuluhan,$data)
	{
		$this->db->where('kd_penyuluhan',$kd_penyuluhan);
		$this->db->update('penyuluhan',$data);
	}
	public function delete_penyuluhan($kd_penyuluhan)
	{
		$this->db->where('kd_penyuluhan',$kd_penyuluhan);
		$this->db->delete('penyuluhan');
	}
	public function approve_penyuluhan($kd_penyuluhan)
	{
		$this->db->where('kd_penyuluhan',$kd_penyuluhan);
		$this->db->set('approve','Ya');
		$this->db->update('penyuluhan');
	}
	public function get_data_penyuluhan(
		$kd_puskesmas,		$kd_pelaksana,
		$user_id,			$kategori,
		$bulan,				
		$tahun,				$approve,
		$level
	)
	{
		$this->db->join('user','user.user_id = penyuluhan.user_id','left');
		$this->db->join('pelaksana','pelaksana.kd_pelaksana = penyuluhan.kd_pelaksana','left');
		$this->db->join('puskesmas','puskesmas.kd_puskesmas = penyuluhan.kd_puskesmas','left');
		if ($kd_puskesmas!="" && $kd_puskesmas>0) $this->db->where('puskesmas.kd_puskesmas',$kd_puskesmas);
		if ($kd_pelaksana!="") $this->db->where('penyuluhan.kd_pelaksana',$kd_pelaksana);
		if ($level=="Petugas Promkes") $this->db->where('penyuluhan.user_id',$user_id);
		if ($kategori!="") $this->db->where('kategori',$kategori);
		if ($bulan!="") $this->db->where('bulan',$bulan);
		if ($tahun!="") $this->db->where('tahun',$tahun);
		if ($approve!="") $this->db->where('approve',$approve);
		$query = $this->db->get('penyuluhan');
		return $query->result();
	}

	public function get_data_penyuluhan_by_id($kd_penyuluhan)
	{
		$this->db->where('kd_penyuluhan',$kd_penyuluhan);
		$query = $this->db->get('penyuluhan');
		return $query->result();
	}

	public function get_data_phbs(
		$kd_puskesmas,		
		$user_id,			
		$bulan,				
		$tahun,
		$approve,
		$level,
		$kd_kategori
	)
	{
		$this->db->select("
			user.user_id, user.full_name, 
			puskesmas.kd_puskesmas, puskesmas.nama_puskesmas, 
			kategori_tempat.kd_kategori, kategori_tempat.kategori, 
			desa.kd_desa, desa.desa, 
			kecamatan.kd_kecamatan, kecamatan.kecamatan, 
			tempat.kd_tempat, tempat.nama_tempat, tempat.tanggal, tempat.status_kesehatan, tempat.approve 
		");
		$this->db->join('user','user.user_id = tempat.user_id','left');
		$this->db->join('puskesmas','puskesmas.kd_puskesmas = user.kd_puskesmas','left');
		$this->db->join('kategori_tempat','kategori_tempat.kd_kategori = tempat.kd_kategori','left');
		$this->db->join('desa','desa.kd_desa = tempat.kd_desa','left');
		$this->db->join('kecamatan','kecamatan.kd_kecamatan = desa.kd_kecamatan','left');
		if ($kd_puskesmas!="" && $kd_puskesmas>0) $this->db->where('puskesmas.kd_puskesmas',$kd_puskesmas);
		if ($level=="Petugas Promkes") $this->db->where('tempat.user_id',$user_id);
		if ($kd_kategori!="") $this->db->where('tempat.kd_kategori',$kd_kategori);
		if ($bulan!="") $this->db->where(' month(tanggal) = '.$bulan );
		if ($tahun!="") $this->db->where(' year(tanggal) = '.$tahun );
		if ($approve!="") $this->db->where('tempat.approve',$approve);
		$query = $this->db->get('tempat');
		return $query->result();
	}
	public function get_new_kd_tempat($kd_kategori,$kd_desa,$user_id)
	{
		$num = $this->get_num($kd_kategori,$kd_desa,$user_id);
		$kd_tempat = $kd_kategori.".".$kd_desa.".".$user_id.".".$num;
		return $kd_tempat;
	}
	public function get_num($kd_kategori,$kd_desa,$user_id)
	{
		$this->db->where('kd_kategori',$kd_kategori);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where('user_id',$user_id);
		$query= $this->db->get('tempat');
		return$query->num_rows() + 1;
	}
	public function insert_tempat($data)
	{
		$this->db->insert('tempat',$data);
	}
	public function insert_phbs($data)
	{
		$this->db->insert('phbs',$data);

	}
	public function get_tempat_by_id($kd_tempat)
	{
		$this->db->join('desa','desa.kd_desa = tempat.kd_desa','left');
		$this->db->join('kecamatan','kecamatan.kd_kecamatan = desa.kd_kecamatan','left');
		$this->db->where('tempat.kd_tempat',$kd_tempat);
		$query = $this->db->get('tempat');
		return $query->result();
	}

	public function get_indikator_phbs($kd_tempat)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$query = $this->db->get('phbs');
		return $query->result();
	}

	public function update_tempat($kd_tempat,$data)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$this->db->update('tempat',$data);
	}
	public function update_phbs($kd_tempat,$kd_indikator,$nilai)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$this->db->where('kd_indikator',$kd_indikator);
		$this->db->set('nilai',$nilai);
		$this->db->update('phbs');
	}
	public function approve_phbs($kd_tempat)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$this->db->set('approve','Ya');
		$this->db->update('tempat');
	}
	public function delete_phbs($kd_tempat)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$this->db->delete('phbs');
	}
	public function delete_tempat($kd_tempat)
	{
		$this->db->where('kd_tempat',$kd_tempat);
		$this->db->delete('tempat');
	}
	public function get_indikator_tambahan($kd_kategori)
	{
		$this->db->where('kd_kategori',$kd_kategori);
		$query = $this->db->get('indikator_tambahan');
		return $query->result();
	}
	public function cek_ref_indikator_tambahan($kd_indikator_tambahan,$tanggal,$kd_desa)
	{
		$bulan = date("m",strtotime($tanggal));
		$tahun = date("Y",strtotime($tanggal));
		$this->db->where('kd_indikator_tambahan',$kd_indikator_tambahan);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where(" bulan = $bulan AND tahun = $tahun ");
		$query = $this->db->get('ref_indikator_tambahan');
		if ($query->num_rows()==0)
		{
			$this->set_ref_indikator_tambahan($kd_indikator_tambahan,$kd_desa,$bulan,$tahun);
		}
	}
	public function set_ref_indikator_tambahan($kd_indikator_tambahan,$kd_desa,$bulan,$tahun)
	{
		$this->db->set('kd_indikator_tambahan',$kd_indikator_tambahan);
		$this->db->set('kd_desa',$kd_desa);
		$this->db->set('nilai','');
		$this->db->set('bulan',$bulan);
		$this->db->set('tahun',$tahun);
		$this->db->insert('ref_indikator_tambahan');
	}
	public function get_ref_indikator_tambahan($tanggal,$kd_kategori,$kd_desa)
	{
		$bulan = date('m',strtotime($tanggal));
		$tahun = date('Y',strtotime($tanggal));
		$this->db->join('indikator_tambahan','indikator_tambahan.kd_indikator_tambahan = ref_indikator_tambahan.kd_indikator_tambahan','left');
		$this->db->where('indikator_tambahan.kd_kategori',$kd_kategori);
		$this->db->where('ref_indikator_tambahan.kd_desa',$kd_desa);
		$this->db->where('ref_indikator_tambahan.bulan',$bulan);
		$this->db->where('ref_indikator_tambahan.tahun',$tahun);
		$query = $this->db->get('ref_indikator_tambahan');
		return $query->result();
	}
	public function update_ref_indikator_tambahan($kd_desa,$kd_indikator_tambahan,$nilai_akhir,$tanggal)
	{
		$bulan = date('m',strtotime($tanggal));
		$tahun = date('Y',strtotime($tanggal));		
		if ($this->cek($bulan,$tahun,$kd_indikator_tambahan,$kd_desa)==0)
		{
			$this->db->set('nilai',$nilai_akhir);
			$this->db->set('kd_indikator_tambahan',$kd_indikator_tambahan);
			$this->db->set('kd_desa',$kd_desa);
			$this->db->set('bulan',$bulan);
			$this->db->set('tahun',$tahun);
			$this->db->insert('ref_indikator_tambahan');
		}
		else{
			$this->db->where('kd_indikator_tambahan',$kd_indikator_tambahan);
			$this->db->where('kd_desa',$kd_desa);
			$this->db->where('bulan',$bulan);
			$this->db->where('tahun',$tahun);
			$this->db->set('nilai',$nilai_akhir);
			$this->db->update('ref_indikator_tambahan');
		}
	}
	public function cek($bulan,$tahun,$kd_indikator_tambahan,$kd_desa)
	{
		$this->db->where('kd_indikator_tambahan',$kd_indikator_tambahan);
		$this->db->where('kd_desa',$kd_desa);
		$this->db->where(" bulan = $bulan AND tahun = $tahun ");
		$query = $this->db->get('ref_indikator_tambahan');
		return $query->num_rows();
	}
	public function get_user_picture_by_api($api_key)
	{
		$user_picture="";
		$this->db->where('api_key',$api_key);
		$query = $this->db->get('user');
		foreach ($query->result() as $row) {
			$user_picture = $row->user_picture;
		}
		return $user_picture;
	}
	public function update_user_picture($API_KEY,$filename)
	{
		$this->db->where('api_key',$API_KEY);
		$this->db->set('user_picture',$filename);
		$this->db->update('user');
		return true;
	}
	public function get_data_informasi()
	{
		$query = $this->db->get_where('post',array('category_id' => -1));
		return $query->result();
	}
}
?>