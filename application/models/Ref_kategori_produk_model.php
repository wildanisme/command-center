<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ref_kategori_produk_model extends CI_Model
{


    public function get($param=null)
    {
        if(isset($param['where']))
        {
          $this->db->where($param['where']);
        }

        $this->db->join("ref_kategori_produk induk","induk.id_kategori_produk = ref_kategori_produk.id_induk","left");
        $this->db->select("ref_kategori_produk.*, induk.nama_kategori_produk as 'nama_kategori_induk'");

        return $this->db->get('ref_kategori_produk');
    }

}
