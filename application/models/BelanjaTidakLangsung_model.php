<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class BelanjaTidakLangsung_model extends CI_model
{
	private $_table = "belanja_tidak_langsung";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $target_belanja_tidak_langsung;
	public $realisasi_belanja_tidak_langsung;
	public $tahun;

	public function rules()
	{
		return [
			[
				'field' => 'target_belanja_tidak_langsung',
				'label' => 'Target Belanja Tidak Langsung',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'realisasi_belanja_tidak_langsung',
				'label' => 'Realiasi Belanja Tidak Langsung',
				'rules' => 'trim|required|numeric'
			],

			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> target_belanja_tidak_langsung = $post["target_belanja_tidak_langsung"];
		$this -> realisasi_belanja_tidak_langsung = $post["realisasi_belanja_tidak_langsung"];
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> target_belanja_tidak_langsung = $post["target_belanja_tidak_langsung"];
		$this -> realisasi_belanja_tidak_langsung = $post["realisasi_belanja_tidak_langsung"];
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}