<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota_model extends CI_Model
{


    public function get($param=null)
    {
        if(isset($param['where']))
        {
          $this->db->where($param['where']);
        }

        if(isset($param['limit']) && isset($param['offset']))
        {
            $this->db->limit($param['limit'],$param['offset']);
        }

        if (isset($param['search'])) {
            $this->db->where("(anggota.nama like '%".$param['search']."%' )");
        }

        $this->db->select("*,anggota.status as 'status'");

        $this->db->join("kelompok","kelompok.id_kelompok = anggota.id_kelompok","left");
        return $this->db->get('anggota');
    }

}
