<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Listopd_model extends CI_model
{
	private $_table = "listopd";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $nm_dinas;
	public $jml_icc;
	public $web_list;
	public $tahun;
	


	public function rules()
	{
		return [
			
			[
				'field' => 'nm_dinas',
				'label' => 'Jenis Orkesmas',
				'rules' => 'trim|required'
			],
			[
				'field' => 'jml_icc',
				'label' => 'Alamat',
				'rules' => 'trim|required'
			],
			[
				'field' => 'web_list',
				'label' => 'Bidang Kgt',
				'rules' => 'trim|required'
			],	
			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'ASC');
		return $this->db->get($this->_table)->result();
		
	}
	

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}
	public function getLimitedData($limit, $start) {
		$this->db->order_by('id', 'ASC');
		$this->db->limit($limit, $start);
		return $this->db->get($this->_table)->result();
	}
	
	public function countAllData() {
		return $this->db->count_all($this->_table);
	}
	

	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> nm_dinas = $post["nm_dinas"];
		$this -> jml_icc = $post["jml_icc"];
		$this -> web_list = $post["web_list"];
	
		
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> nm_dinas = $post["nm_dinas"];
		$this -> jml_icc = $post["jml_icc"];
		$this -> web_list = $post["web_list"];
		
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
	
}
