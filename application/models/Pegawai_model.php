<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{
	public	$id_pegawai	;
	public	$nip_lama	;
	public	$nip_baru	;
	public	$id_gelardepan	;
	public	$nama_lengkap	;
	public	$id_gelarbelakang	;
	public	$tgl_lahir	;
	public	$tempat_lahir	;
	public	$id_agama	;
	public	$jenis_kelamin	;
	public	$kedudukan_pegawai	;
	public	$alamat	;
	public	$RT	;
	public	$RW	;
	public	$id_desa	;
	public	$id_kecamatan	;
	public	$id_kabupaten	;
	public	$id_provinsi	;
	public	$kode_pos	;
	public	$telepon	;
	public	$kartu_akses	;
	public	$kartu_taspen	;
	public	$karis_karsu	;
	public	$npwp	;
	public	$id_statusmenikah	;
	public	$jml_tanggungan_anak	;
	public	$jml_seluruh_anak	;
	public	$foto	;
	public 	$status;
	public 	$nip;
	public 	$id_jabatan;
	public 	$id_unit_kerja;
	public 	$eselon;
	
	public $kd_skpd;
	public function check_avaliable($nip_baru){
		$this->db->where('nip_baru',$nip_baru);
		$query = $this->db->get('pegawai');
		if ($query->num_rows()==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	} 	public function cek_nip($nip){
		$this->db->where('nip',$nip);
		$query = $this->db->get('pegawai');
		if ($query->num_rows()==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	} 

	public function get_registered(){
		$this->db->join('user','user.id_pegawai = pegawai.id_pegawai');
		$this->db->join('riwayat_unit_kerja','riwayat_unit_kerja.id_pegawai = pegawai.id_pegawai');
		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = riwayat_unit_kerja.id_unit_kerja');
		$query = $this->db->get('pegawai');
		return $query->result();
	}



	public function get_registered_u(){
		$this->db->join('user','user.id_pegawai = pegawai.id_pegawai');
		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = user.unit_kerja_id');
		$query = $this->db->get('pegawai');
		return $query->result();
	}

	public function insert($data){
		$data['tgl_input'] = date('Y-m-d');
		$this->db->insert('pegawai',$data);
		// $this->updateMaster($data['nip_baru']);
	}
	public function update($data,$id_pegawai){
		$this->db->where('id_pegawai',$id_pegawai);
		$this->db->update('pegawai',$data);
	}
	public function getFromMaster($nip_master)
	{
		$this->db->where('NIP',$nip_master);
		$this->db->where('migrasi','0');
		$query = $this->db->get('pegawai1');
		return $query->result();
	}
	public function get_by_id(){
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = pegawai.id_jabatan');
		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = pegawai.id_unit_kerja');
		$this->db->where('id_pegawai',$this->id_pegawai);
		$query = $this->db->get('pegawai');
		return $query->row();
	}
	public function cek_user($id_pegawai){
		$this->db->where('id_pegawai',$id_pegawai);
		$query = $this->db->get('user');
		if ($query->num_rows()==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public function get_pegawai_for_pengajuan($nip_baru)
	{
		$this->db->select("
			pegawai.id_pegawai, pegawai.nip_lama, pegawai.nip_baru, pegawai.karpeg,pegawai.foto,
			pegawai.id_gelardepan, pegawai.nama_lengkap, pegawai.id_gelarbelakang,
			ref_gelardepan.nama_gelardepan, ref_gelarbelakang.nama_gelarbelakang,pegawai.jenis_kelamin,
			pegawai.tempat_lahir, pegawai.tgl_lahir, pegawai.id_agama, ref_agama.nama_agama,
			pegawai.alamat, pegawai.RT, pegawai.RW, pegawai.kode_pos, pegawai.telepon,
			pegawai.id_desa, desa.desa, pegawai.id_kecamatan, kecamatan.kecamatan,
			pegawai.id_kabupaten, kabupaten.kabupaten, pegawai.id_provinsi, provinsi.provinsi,
			pegawai.kd_skpd, skpd.nama_skpd
		");
		$this->db->join('ref_gelardepan','ref_gelardepan.id_gelardepan=pegawai.id_gelardepan','left');
		$this->db->join('ref_gelarbelakang','ref_gelarbelakang.id_gelarbelakang=pegawai.id_gelarbelakang','left');
		$this->db->join('ref_agama','ref_agama.id_agama=pegawai.id_agama','left');
		$this->db->join('provinsi','provinsi.id_provinsi=pegawai.id_provinsi','left');
		$this->db->join('kabupaten','kabupaten.id_kabupaten=pegawai.id_kabupaten','left');
		$this->db->join('kecamatan','kecamatan.id_kecamatan=pegawai.id_kecamatan','left');
		$this->db->join('desa','desa.id_desa=pegawai.id_desa','left');
		$this->db->join('skpd','skpd.kd_skpd=pegawai.kd_skpd','left');
		$this->db->where('nip_baru',$nip_baru);
		$query = $this->db->get('pegawai');
		return $query->result();
	}
	//public function updateMaster($nip)
	//{
		//$this->db->where('NIP',$nip);
		//$this->db->set('migrasi','1');
		//$this->db->update('pegawai1');
	//}
	public function get_for_page($limit=0,$offset=0)
	{
		// $this->db->select("pegawai.id_pegawai, pegawai.nama_lengkap, pegawai.nip_baru, pegawai.status, skpd.nama_skpd");
		
		// $this->db->join('riwayat_unit_kerja','riwayat_unit_kerja.id=pegawai.id_riwayat_unit_kerja','left');
		// $this->db->join('skpd','skpd.kd_skpd=riwayat_unit_kerja.id_unit_kerja','left');
		
		// if ($this->kd_skpd>0) $this->db->where('riwayat_unit_kerja.id_unit_kerja',$this->kd_skpd);
		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = pegawai.id_unit_kerja');
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = pegawai.id_jabatan');
		if ($this->nip!="") $this->db->where(" pegawai.nip like '%".$this->nip."%' ");
		if ($this->nama_lengkap!="") $this->db->where(" pegawai.nama_lengkap like '%".$this->nama_lengkap."%' ");
		if ($this->id_unit_kerja!="") $this->db->where('pegawai.id_unit_kerja',$this->id_unit_kerja);
		if ($this->id_jabatan!="") $this->db->where('pegawai.id_jabatan',$this->id_jabatan);
		// if ($this->status!="") $this->db->where('pegawai.status',$this->status);
		if ($limit>0) $this->db->limit($limit,$offset);
		$query = $this->db->get('pegawai');
		return $query->result();
	}

	public function get_all($id_unit_kerja='')
	{
		if($id_unit_kerja!==''){
			$this->db->where('id_unit_kerja',$id_unit_kerja);
		}
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = pegawai.id_jabatan');
		$query = $this->db->get('pegawai');
		return $query->result();
	}
	
	public function get($id=0)
	{
		$this->db->select("
			pegawai.*, 
			skpd.nama_skpd,
			ref_gelardepan.nama_gelardepan,
			ref_gelarbelakang.nama_gelarbelakang,
			provinsi.provinsi,
			kabupaten.kabupaten,
			kecamatan.kecamatan,
			desa.desa,
			ref_statusmenikah.nama_statusmenikah,
			ref_agama.nama_agama,

			pegawai.nip,
			ref_unit_kerja.nama_unit_kerja,
			ref_jabatan.nama_jabatan,
			pegawai.eselon AS nama_eselon,
			
");
		$this->db->join('skpd','skpd.kd_skpd=pegawai.kd_skpd','left');
		$this->db->join('ref_gelardepan','ref_gelardepan.id_gelardepan=pegawai.id_gelardepan','left');
		$this->db->join('ref_gelarbelakang','ref_gelarbelakang.id_gelarbelakang=pegawai.id_gelarbelakang','left');
		$this->db->join('provinsi','provinsi.id_provinsi=pegawai.id_provinsi','left');
		$this->db->join('kabupaten','kabupaten.id_kabupaten=pegawai.id_kabupaten','left');
		$this->db->join('kecamatan','kecamatan.id_kecamatan=pegawai.id_kecamatan','left');
		$this->db->join('desa','desa.id_desa=pegawai.id_desa','left');
		$this->db->join('ref_statusmenikah','ref_statusmenikah.id_statusmenikah=pegawai.id_statusmenikah','left');
		$this->db->join('ref_agama','ref_agama.id_agama=pegawai.id_agama','left');

		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja=pegawai.id_unit_kerja','left');
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan=pegawai.id_jabatan','left');
		//$this->db->join('ref_eselon','ref_eselon.id_eselon=pegawai.eselon','left');
		
		if ($id>0) $this->db->where('pegawai.id_pegawai',$id);
		
		$query = $this->db->get('pegawai');
		return  $query->result_array();
	}
	public function get_golongan()
	{
		$query = $this->db->get('ref_golongan');
		return $query->result();
	}
	public function get_jabatan()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jabatan');
		return $query->result();
	}
	public function get_jenjangpendidikan()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jenjangpendidikan');
		return $query->result();
	}
	public function get_tempatpendidikan()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_tempatpendidikan');
		return $query->result();
	}
	public function get_jurusan($id_tempatpendidikan=null)
	{
		if ($id_tempatpendidikan!=null) $this->db->where('id_tempatpendidikan',$id_tempatpendidikan);
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jurusan');
		return $query->result();
	}
	public function get_jenisdiklat()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jenisdiklat');
		return $query->result();
	}
	public function get_skpd()
	{
		$this->db->where('status','Aktif');
		$this->db->order_by('nama_skpd','ASC');
		$query = $this->db->get('skpd');
		return $query->result();
	}
	public function get_jenispenghargaan()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jenispenghargaan');
		return $query->result();
	}
	public function get_jeniscuti()
	{
		$this->db->where('status','Y');
		$query = $this->db->get('ref_jeniscuti');
		return $query->result();
	}
	public function tambah_riwayat_pangkat($data)
	{
		$this->db->insert('riwayat_pangkat',$data);
	}
	public function tambah_riwayat_jabatan($data)
	{
		$this->db->insert('riwayat_jabatan',$data);
	}
	public function tambah_riwayat_pendidikan($data)
	{
		$this->db->insert('riwayat_pendidikan',$data);
	}
	public function tambah_riwayat_diklat($data)
	{
		$this->db->insert('riwayat_diklat',$data);
	}
	public function tambah_riwayat_unit_kerja($data)
	{
		$this->db->insert('riwayat_unit_kerja',$data);
	}
	public function tambah_riwayat_penghargaan($data)
	{
		$this->db->insert('riwayat_penghargaan',$data);
	}
	public function tambah_riwayat_cuti($data)
	{
		$this->db->insert('riwayat_cuti',$data);
	}
	public function set_status($id_pegawai,$status){
		$this->db->where('id_pegawai',$id_pegawai);
		$this->db->set('status',$status);
		$this->db->update('pegawai');
	}
	public function get_riwayat_pangkat($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_pangkat.id, riwayat_pangkat.id_pegawai, riwayat_pangkat.id_golongan, riwayat_pangkat.tmt_berlaku,
			riwayat_pangkat.gaji_pokok, riwayat_pangkat.nama_pejabat, riwayat_pangkat.no_sk, riwayat_pangkat.tgl_sk,
			riwayat_pangkat.status, riwayat_pangkat.berkas,
			ref_golongan.pangkat, ref_golongan.golongan,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_pangkat.id_pegawai','left');
		$this->db->join('ref_golongan','ref_golongan.id_golongan = riwayat_pangkat.id_golongan','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pangkat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pangkat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pangkat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_pangkat.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_pangkat.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);
		$this->db->order_by('riwayat_pangkat.tgl_sk','DESC');
		$query=$this->db->get('riwayat_pangkat');
		return $query->result();
	}
	public function get_riwayat_pangkat_by_id($id)
	{
		$this->db->select("
			riwayat_pangkat.id, riwayat_pangkat.id_pegawai, riwayat_pangkat.id_golongan, riwayat_pangkat.tmt_berlaku,
			riwayat_pangkat.gaji_pokok, riwayat_pangkat.nama_pejabat, riwayat_pangkat.no_sk, riwayat_pangkat.tgl_sk,
			riwayat_pangkat.status, riwayat_pangkat.berkas,
			ref_golongan.pangkat, ref_golongan.golongan,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_pangkat.id_pegawai','left');
		$this->db->join('ref_golongan','ref_golongan.id_golongan = riwayat_pangkat.id_golongan','left');
		$this->db->where('id',$id);
		$this->db->order_by('riwayat_pangkat.tgl_sk','DESC');
		$query=$this->db->get('riwayat_pangkat');
		return $query->result();
	}
	public function get_riwayat_jabatan($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_jabatan.id,
			riwayat_jabatan.id_pegawai,
			riwayat_jabatan.id_jabatan,
			riwayat_jabatan.id_golongan,
			riwayat_jabatan.tgl_mulai,
			riwayat_jabatan.tgl_akhir,
			riwayat_jabatan.berkas,
			
			riwayat_jabatan.gaji_pokok,
			riwayat_jabatan.nama_pejabat,
			riwayat_jabatan.no_sk,
			riwayat_jabatan.tgl_sk,
			riwayat_jabatan.status,
			ref_jabatan.nama_jabatan,
			ref_jabatan.ket_induk,
			ref_golongan.golongan,
			ref_golongan.pangkat,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_jabatan.id_pegawai','left');
		$this->db->join('ref_golongan','ref_golongan.id_golongan = riwayat_jabatan.id_golongan','left');
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = riwayat_jabatan.id_jabatan','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_jabatan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_jabatan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_jabatan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_jabatan.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_jabatan.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);
		$this->db->order_by('riwayat_jabatan.tgl_sk','DESC');
		$query=$this->db->get('riwayat_jabatan');
		return $query->result();
	}
	public function get_riwayat_jabatan_by_id($id)
	{
		$this->db->select("
			riwayat_jabatan.id,
			riwayat_jabatan.id_pegawai,
			riwayat_jabatan.id_jabatan,
			riwayat_jabatan.id_golongan,
			riwayat_jabatan.tgl_mulai,
			riwayat_jabatan.tgl_akhir,
			riwayat_jabatan.berkas,
			
			riwayat_jabatan.gaji_pokok,
			riwayat_jabatan.nama_pejabat,
			riwayat_jabatan.no_sk,
			riwayat_jabatan.tgl_sk,
			riwayat_jabatan.status,
			ref_jabatan.nama_jabatan,
			ref_jabatan.ket_induk,
			ref_golongan.golongan,
			ref_golongan.pangkat,
			pegawai.nip_baru, pegawai.nama_lengkap
			
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_jabatan.id_pegawai','left');
		$this->db->join('ref_golongan','ref_golongan.id_golongan = riwayat_jabatan.id_golongan','left');
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = riwayat_jabatan.id_jabatan','left');
		$this->db->where('id',$id);
		$query = $this->db->get('riwayat_jabatan');
		return $query->result();
	}
	public function get_riwayat_pendidikan($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_pendidikan.id,
			riwayat_pendidikan.id_pegawai,
			riwayat_pendidikan.id_jenjangpendidikan,
			riwayat_pendidikan.id_tempatpendidikan,
			riwayat_pendidikan.id_jurusan,
			riwayat_pendidikan.nama_pejabat,
			riwayat_pendidikan.nomor_sk,
			riwayat_pendidikan.tgl_sk,
			riwayat_pendidikan.status,
			riwayat_pendidikan.berkas,
			ref_jenjangpendidikan.nama_jenjangpendidikan,
			ref_tempatpendidikan.nama_tempatpendidikan,
			ref_jurusan.nama_jurusan,
			
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_pendidikan.id_pegawai','left');
		$this->db->join('ref_jenjangpendidikan','ref_jenjangpendidikan.id_jenjangpendidikan = riwayat_pendidikan.id_jenjangpendidikan','left');
		$this->db->join('ref_tempatpendidikan','ref_tempatpendidikan.id_tempatpendidikan=riwayat_pendidikan.id_tempatpendidikan','left');
		$this->db->join('ref_jurusan','ref_jurusan.id_jurusan=riwayat_pendidikan.id_jurusan','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pendidikan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pendidikan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_pendidikan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_pendidikan.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_pendidikan.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);
		$this->db->order_by('riwayat_pendidikan.tgl_sk','DESC');
		$query=$this->db->get('riwayat_pendidikan');
		return $query->result();
	}
	public function get_riwayat_pendidikan_by_id($id)
	{
		$this->db->select("
			riwayat_pendidikan.id,
			riwayat_pendidikan.id_pegawai,
			riwayat_pendidikan.id_jenjangpendidikan,
			riwayat_pendidikan.id_tempatpendidikan,
			riwayat_pendidikan.id_jurusan,
			riwayat_pendidikan.nama_pejabat,
			riwayat_pendidikan.nomor_sk,
			riwayat_pendidikan.tgl_sk,
			riwayat_pendidikan.status,
			riwayat_pendidikan.berkas,
			ref_jenjangpendidikan.nama_jenjangpendidikan,
			ref_tempatpendidikan.nama_tempatpendidikan,
			ref_jurusan.nama_jurusan,
			
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_pendidikan.id_pegawai','left');
		$this->db->join('ref_jenjangpendidikan','ref_jenjangpendidikan.id_jenjangpendidikan = riwayat_pendidikan.id_jenjangpendidikan','left');
		$this->db->join('ref_tempatpendidikan','ref_tempatpendidikan.id_tempatpendidikan=riwayat_pendidikan.id_tempatpendidikan','left');
		$this->db->join('ref_jurusan','ref_jurusan.id_jurusan=riwayat_pendidikan.id_jurusan','left');
		$this->db->where('id',$id);
		$this->db->order_by('riwayat_pendidikan.tgl_sk','DESC');
		$query=$this->db->get('riwayat_pendidikan');
		return $query->result();
	}
	public function get_riwayat_diklat($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_diklat.id,
			riwayat_diklat.id_pegawai,
			riwayat_diklat.id_jenisdiklat,
			riwayat_diklat.nama_diklat,
			riwayat_diklat.tempat,
			riwayat_diklat.penyelenggara,
			riwayat_diklat.angkatan,
			riwayat_diklat.tgl_mulai,
			riwayat_diklat.tgl_akhir,
			riwayat_diklat.no_sptl,
			riwayat_diklat.tgl_sptl,
			riwayat_diklat.status,
			riwayat_diklat.berkas,
			ref_jenisdiklat.nama_jenisdiklat,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_diklat.id_pegawai','left');
		$this->db->join('ref_jenisdiklat','ref_jenisdiklat.id_jenisdiklat = riwayat_diklat.id_jenisdiklat','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_diklat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_diklat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_diklat.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_diklat.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_diklat.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);
		$this->db->order_by('riwayat_diklat.tgl_mulai','DESC');
		$query=$this->db->get('riwayat_diklat');
		return $query->result();
	}
	public function get_riwayat_diklat_by_id($id)
	{
		$this->db->select("
			riwayat_diklat.id,
			riwayat_diklat.id_pegawai,
			riwayat_diklat.id_jenisdiklat,
			riwayat_diklat.nama_diklat,
			riwayat_diklat.tempat,
			riwayat_diklat.penyelenggara,
			riwayat_diklat.angkatan,
			riwayat_diklat.tgl_mulai,
			riwayat_diklat.tgl_akhir,
			riwayat_diklat.no_sptl,
			riwayat_diklat.tgl_sptl,
			riwayat_diklat.status,
			riwayat_diklat.berkas,
			ref_jenisdiklat.nama_jenisdiklat,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_diklat.id_pegawai','left');
		$this->db->join('ref_jenisdiklat','ref_jenisdiklat.id_jenisdiklat = riwayat_diklat.id_jenisdiklat','left');
		$this->db->where('id',$id);
		$this->db->order_by('riwayat_diklat.tgl_mulai','DESC');
		$query=$this->db->get('riwayat_diklat');
		return $query->result();
	}
	public function get_riwayat_unit_kerja($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_unit_kerja.id,
			riwayat_unit_kerja.id_pegawai,
			riwayat_unit_kerja.id_unit_kerja,
			riwayat_unit_kerja.tmt_awal,
			riwayat_unit_kerja.tmt_akhir,
			riwayat_unit_kerja.no_sk_awal,
			riwayat_unit_kerja.no_sk_akhir,
			riwayat_unit_kerja.status,
			riwayat_unit_kerja.berkas,
			skpd.nama_skpd,
			skpd.alamat,
			skpd.telp,
			skpd.email,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_unit_kerja.id_pegawai','left');
		$this->db->join('skpd','skpd.kd_skpd = riwayat_unit_kerja.id_unit_kerja','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_unit_kerja.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_unit_kerja.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_unit_kerja.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_unit_kerja.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_unit_kerja.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);
		$this->db->order_by('riwayat_unit_kerja.tmt_akhir','DESC');
		$query=$this->db->get('riwayat_unit_kerja');
		return $query->result();
	}
	public function get_riwayat_unit_kerja_by_id($id)
	{
		$this->db->select("
			riwayat_unit_kerja.id,
			riwayat_unit_kerja.id_pegawai,
			riwayat_unit_kerja.id_unit_kerja,
			riwayat_unit_kerja.tmt_awal,
			riwayat_unit_kerja.tmt_akhir,
			riwayat_unit_kerja.no_sk_awal,
			riwayat_unit_kerja.no_sk_akhir,
			riwayat_unit_kerja.status,
			riwayat_unit_kerja.berkas,
			skpd.nama_skpd,
			skpd.alamat,
			skpd.telp,
			skpd.email,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_unit_kerja.id_pegawai','left');
		$this->db->join('skpd','skpd.kd_skpd = riwayat_unit_kerja.id_unit_kerja','left');
		$this->db->where('id',$id);
		$this->db->order_by('riwayat_unit_kerja.tmt_akhir','DESC');
		$query=$this->db->get('riwayat_unit_kerja');
		return $query->result();
	}
	public function get_riwayat_penghargaan($id_pegawai=null,$status=null,$nip=null,$nama=null,$limit=0,$offset=0)
	{
		$this->db->select("
			riwayat_penghargaan.id,
			riwayat_penghargaan.id_pegawai,
			riwayat_penghargaan.id_jenispenghargaan,
			riwayat_penghargaan.nama_penghargaan,
			riwayat_penghargaan.tahun,
			riwayat_penghargaan.asal_perolehan,
			riwayat_penghargaan.penandatangan,
			riwayat_penghargaan.no_penghargaan,
			riwayat_penghargaan.tgl_penghargaan,
			riwayat_penghargaan.status,
			riwayat_penghargaan.berkas,
			ref_jenispenghargaan.nama_jenispenghargaan,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_penghargaan.id_pegawai','left');
		$this->db->join('ref_jenispenghargaan','ref_jenispenghargaan.id_jenispenghargaan = riwayat_penghargaan.id_jenispenghargaan','left');
		if ($nip!=null && $nama!=null){
			$where = " (pegawai.nip_baru like '%$nip%' ";
			$where .= " AND pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_penghargaan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama!=null){
			//$where = " (pegawai.nip_baru like '%$nip%' ";
			$where = " pegawai.nama_lengkap like '%$nama%' ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_penghargaan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip!=null && $nama==null){
			$where = " pegawai.nip_baru like '%$nip%' ";
			//$where .= " OR pegawai.nama_lengkap like '%$nama%' ) ";
			if ($id_pegawai!=null) $where .= "AND pegawai.id_pegawai =$id_pegawai ";
			if ($status!=null) $where .= "AND riwayat_penghargaan.status =$status ";
			$this->db->where($where);
		}
		else if ($nip==null && $nama==null){
			if ($id_pegawai!=null) $this->db->where('riwayat_penghargaan.id_pegawai',$id_pegawai);
			if ($status!=null) $this->db->where('riwayat_penghargaan.status',$status);
		}
		
		if ($limit>0) $this->db->limit($limit,$offset);$this->db->order_by('riwayat_penghargaan.tgl_penghargaan','DESC');
		$query=$this->db->get('riwayat_penghargaan');
		return $query->result();
	}
	public function get_riwayat_penghargaan_by_id($id)
	{
		$this->db->select("
			riwayat_penghargaan.id,
			riwayat_penghargaan.id_pegawai,
			riwayat_penghargaan.id_jenispenghargaan,
			riwayat_penghargaan.nama_penghargaan,
			riwayat_penghargaan.tahun,
			riwayat_penghargaan.asal_perolehan,
			riwayat_penghargaan.penandatangan,
			riwayat_penghargaan.no_penghargaan,
			riwayat_penghargaan.tgl_penghargaan,
			riwayat_penghargaan.status,
			riwayat_penghargaan.berkas,
			ref_jenispenghargaan.nama_jenispenghargaan,
			pegawai.nip_baru, pegawai.nama_lengkap
		");
		$this->db->join('pegawai','pegawai.id_pegawai = riwayat_penghargaan.id_pegawai','left');
		$this->db->join('ref_jenispenghargaan','ref_jenispenghargaan.id_jenispenghargaan = riwayat_penghargaan.id_jenispenghargaan','left');
		
		$this->db->where('id',$id);
		$query=$this->db->get('riwayat_penghargaan');
		return $query->result();
	}
	public function get_riwayat_cuti($id_pegawai,$status=null)
	{
		$this->db->select("
			riwayat_cuti.id,
			riwayat_cuti.id_pegawai,
			riwayat_cuti.id_jeniscuti,
			riwayat_cuti.keterangan,
			riwayat_cuti.pejabat_penetapan,
			riwayat_cuti.no_sk,
			riwayat_cuti.tgl_sk,
			riwayat_cuti.tgl_awal_cuti,
			riwayat_cuti.tgl_akhir_cuti,
			riwayat_cuti.status,
			riwayat_cuti.berkas,
			ref_jeniscuti.nama_jeniscuti
		");
		$this->db->join('ref_jeniscuti','ref_jeniscuti.id_jeniscuti = riwayat_cuti.id_jeniscuti','left');
		$this->db->where('riwayat_cuti.id_pegawai',$id_pegawai);
		if ($status!=null) $this->db->where('riwayat_cuti.status',$status);
		$this->db->order_by('riwayat_cuti.tgl_sk','DESC');
		$query=$this->db->get('riwayat_cuti');
		return $query->result();
	}
	public function ubah_status_riwayat($table,$status,$id)
	{
		$this->db->where('id',$id);
		$this->db->set('status',$status);
		$this->db->update($table);
		$this->update_riwayat_terakhir($table,$id);
	}
	public function update_riwayat_terakhir($table,$id,$delete=null)
	{
		$this->db->where('id',$id);
		$this->db->limit(1);
		$query = $this->db->get($table);
		$data = $query->result();
		$id_pegawai = $data[0]->id_pegawai;
		$id_riwayat = 0;
		if ($table=="riwayat_jabatan"){
			$riwayat = $this->get_riwayat_jabatan($id_pegawai,1);
			$field = "id_riwayat_jabatan";
		}
		else if ($table=="riwayat_pendidikan"){
			$riwayat = $this->get_riwayat_pendidikan($id_pegawai,1);
			$field = "id_riwayat_pendidikan";
		}
		else if ($table=="riwayat_pangkat"){
			$riwayat = $this->get_riwayat_pangkat($id_pegawai,1);
			$field = "id_riwayat_pangkat";
		}
		else if ($table=="riwayat_unit_kerja"){
			$riwayat = $this->get_riwayat_unit_kerja($id_pegawai,1);
			$field = "id_riwayat_unit_kerja";
		}
		else if ($table=="riwayat_diklat"){
			$riwayat = $this->get_riwayat_diklat($id_pegawai,1);
			$field = "id_riwayat_diklat";
		}
		else if ($table=="riwayat_penghargaan"){
			// $riwayat = $this->get_riwayat_penghargaan($id_pegawai,1);
			// $field = "id_riwayat_penghargaan";
		}
		else if ($table=="riwayat_cuti"){
			// $riwayat = $this->get_riwayat_cuti($id_pegawai,1);
			// $field = "id_riwayat_cuti";
		}
		if (!empty($riwayat)) {
			$id_riwayat = $riwayat[0]->id;
			//update pegawai
			$this->db->where('id_pegawai',$id_pegawai);
			$this->db->update("pegawai",array($field=>$id_riwayat));
		}
		if ($delete!=null && $delete==1 && !empty($field)){
			$this->db->where('id_pegawai',$id_pegawai);
			$this->db->where($field,$id);
			$q=$this->db->get('pegawai');
			$rs = $q->result();
			if (!empty($rs)){
				$this->db->where('id_pegawai',$id_pegawai);
				$this->db->update("pegawai",array($field=>0));
			}
		}
	}
	public function delete_riwayat($table,$id,$berkas)
	{
		$this->update_riwayat_terakhir($table,$id,1);
		$this->db->where('id',$id);
		$this->db->delete($table);
		if ($berkas!=""){
			unlink('./data/upload_berkas/'.$berkas);
		}
		
	}
	public function updateBerkasRiwayat($table,$id,$berkas)
	{
		$this->db->set('berkas',$berkas);
		$this->db->where('id',$id);
		$this->db->update("riwayat_".$table);
	}

	public function getPegawaiByJabatan($id_jabatan){
		// $this->db->where('riwayat_jabatan.status',1);
		$this->db->where('pegawai.id_jabatan',$id_jabatan);
		//$this->db->join('riwayat_jabatan','pegawai.id_pegawai = riwayat_jabatan.id_pegawai');
		$query = $this->db->get('pegawai');
		return $query->row();
	}

	public function delete_pegawai($id_pegawai){
		$this->db->where('id_pegawai',$id_pegawai);
		$this->db->delete('pegawai');
		$this->db->where('id_pegawai',$id_pegawai);
		$this->db->delete('user');
	}
	
	public function getData($param=null)
	{
		if($param!=null)
		{
			foreach ($param as $key => $value) {
				$this->db->where($key,$value);
			}
		}
		$this->db->join('ref_jabatan','ref_jabatan.id_jabatan = pegawai.id_jabatan','left');
		$query = $this->db->get("pegawai");
		return $query->result();
	}
}

?>