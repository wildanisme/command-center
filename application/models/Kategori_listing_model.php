<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_listing_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_listing;
	public $status;
	public $banner;

	public function get_all()
	{
		$query = $this->db->get('kategori_listing');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_listing',$this->nama_kategori_listing);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('banner',$this->banner);
		$this->db->insert('kategori_listing');
	}
	public function update()
	{
		$this->db->where('id_kategori_listing',$this->id_kategori_listing);
		$this->db->set('nama_kategori_listing',$this->nama_kategori_listing);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		if ($this->banner) $this->db->set('banner',$this->banner);
		$this->db->update('kategori_listing');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_listing',$this->id_kategori_listing);
		$query = $this->db->get('kategori_listing');
		foreach ($query->result() as $row) {
			$this->nama_kategori_listing 	= $row->nama_kategori_listing;
			$this->kategori_slug	= $row->kategori_slug;
			$this->banner	= $row->banner;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_listing',$this->id_kategori_listing);
		$query = $this->db->delete('kategori_listing');	
	}
}
?>