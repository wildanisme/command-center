<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model
{
	public $id_setting;
	public $header;
	public $tagline;
	public $logo;
	public $footer;
	

	public function set_setting()
	{
		$query = $this->db->get('setting');
		foreach ($query->result() as $row) {
			$this->header = $row->header;
			$this->header2 = $row->header2;
			$this->header3 = $row->header3;
			$this->tagline = $row->tagline;
			$this->footer = $row->footer;
			$this->footer2 = $row->footer2;
			$this->logo= $row->logo;
			
		}
	}
	public function update_setting()
	{
		$this->db->set('header',$this->header);
		$this->db->set('header2',$this->header2);
		$this->db->set('header3',$this->header3);
		$this->db->set('tagline',$this->tagline);
		$this->db->set('footer',$this->footer);
		$this->db->set('footer2',$this->footer2);
		if ($this->logo !="") $this->db->set('logo',$this->logo);
		$this->db->update('setting');
	}
}
?>