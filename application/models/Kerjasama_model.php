<?php defined('BASEPATH') or exit('No direct script access allowed');

class Kerjasama_model extends CI_Model
{
    private $_table = "kerjasama";

    public $id_surat;
    public $no_surat_ket;
    public $judul_kerjasama;
    public $pihak_pertama;
    public $pihak_kedua;
    public $tanggal;
    public $tanggal_selesai;
    public $bidang;
    public $status;
    public $deskripsi;
    public $galeri = "default.jpg";
    public $dokumen = "default.pdf";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_surat" => $id])->row();
    }
    public function getFilter()
    {
        $post = $this->input->post();
        $year = $post["tanggal"];
        return $this->db->query('SELECT * from kerjasama a where YEAR(a.tanggal) = "$year"')->result();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_surat = uniqid();
        $this->no_surat_ket = $post["no_surat_ket"];
        $this->judul_kerjasama = $post["judul_kerjasama"];
        $this->pihak_pertama = $post["pihak_pertama"];
        $this->pihak_kedua = $post["pihak_kedua"];
        $this->tanggal = $post["tanggal"];
        $this->tanggal_selesai = $post["tanggal_selesai"];
        $this->bidang = $post["bidang"];
        $this->status = $post["status"];
        $this->deskripsi = $post["deskripsi"];
        $this->galeri = $this->_uploadImage();
        $this->dokumen = $this->_uploadDokumen();
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id_surat = $post["id"];
        $this->no_surat_ket = $post["no_surat_ket"];
        $this->judul_kerjasama = $post["judul_kerjasama"];
        $this->pihak_pertama = $post["pihak_pertama"];
        $this->pihak_kedua = $post["pihak_kedua"];
        $this->tanggal = $post["tanggal"];
        $this->tanggal_selesai = $post["tanggal_selesai"];
        $this->bidang = $post["bidang"];
        $this->status = $post["status"];
        $this->deskripsi = $post["deskripsi"];
        $this->galeri = $this->_uploadImage();
        $this->dokumen = $this->_uploadDokumen();

        return $this->db->update($this->_table, $this, array('id_surat' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_surat" => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']  = './uploads/kerjasama/';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['file_name']    = $this->id_surat;
        $config['overwrite'] = false;
        $config['max_size'] = 10024;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('galeri')) {
            return $this->upload->data("file_name");
        }

        return "default.jpg";
    }

    private function _uploadDokumen()
    {
        $config['upload_path']  = './uploads/kerjasama/';
        $config['allowed_types'] = 'pdf';
        $config['file_name']    = $this->id_surat;
        $config['overwrite'] = false;
        $config['max_size'] = 20048;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('dokumen')) {
            return $this->upload->data("file_name");
        }

        return "default.pdf";
    }

    function DetailData($id)
    {
        $this->db->where('id_surat', $id);
        $sql = $this->db->get($this->tabel);
        if ($sql->num_rows() == 1) {
            return $sql->row_array();
        }
    }
}
