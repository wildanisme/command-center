<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_skpd_model extends CI_Model{

	public function get_all(){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_skpd',$this->session->userdata('id_skpd'));
		// }
		$query = $this->db->get('ref_skpd');
		return $query->result();
	}

	public function get_total(){
		
		$query = $this->db->get('ref_skpd');
		return $query->num_rows();
	}

	public function get_page($mulai,$hal,$filter=''){
		if($this->session->userdata('level')=='User'){
			$this->db->where('id_skpd',$this->session->userdata('id_skpd'));
		}
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like($key,$value);
			}
		}else{
			$this->db->limit($hal,$mulai);
		}
		$query = $this->db->get('ref_skpd');
		return $query->result();
	}


public function get_for_page($mulai,$hal,$filter=''){
		
		if($filter!=''){
			$this->db->where("nama_skpd like '%$this->search%' ");
		}else{
			$this->db->limit($hal,$mulai);
		}

		$this->db->order_by("nama_skpd", "DESC");
		$this->db->limit($mulai,$hal);
		$query = $this->db->get('ref_skpd');


		return $query->result();
	}

	public function get_total_row() {
	
		if(!empty($this->search) && $this->search !="") {
			$this->db->where("nama_skpd like '%$this->search%' ");
		}
		$query = $this->db->get('ref_skpd');
		return $query->num_rows();
	}


	public function get_by_id($id_skpd){
		$this->db->where('id_skpd',$id_skpd);
		$query = $this->db->get('ref_skpd');
		return $query->row();
	}
	public function insert($data){
		return $this->db->insert('ref_skpd',$data);
	}
	public function update($data,$id_skpd){
		return $this->db->update('ref_skpd',$data,array('id_skpd'=>$id_skpd));
	}
	public function delete($id_skpd){
		$this->db->delete('ref_skpd',array('id_skpd'=>$id_skpd));
		$this->db->delete('ref_unit_kerja',array('id_skpd'=>$id_skpd));
	}
	public function delete_unit_kerja($id_unit_kerja){
		$this->db->delete('ref_unit_kerja',array('id_unit_kerja'=>$id_unit_kerja));
		$this->db->delete('ref_unit_kerja',array('id_induk'=>$id_unit_kerja));
	}

	public function insert_unit_kerja($data,$id_skpd){
		$this->db->set('id_skpd',$id_skpd);
		return $this->db->insert('ref_unit_kerja',$data);

	}

	public function update_unit_kerja($data,$id_unit_kerja){
		unset($data['id_unit_kerja']);
		return $this->db->update('ref_unit_kerja',$data,array('id_unit_kerja'=>$id_unit_kerja));

	}

	public function get_unit_kerja_by_id($id_unit_kerja){
		$this->db->where('id_unit_kerja',$id_unit_kerja);
		$q = $this->db->get('ref_unit_kerja');
		return $q->row();
	}

	public function get_unit_kerja_by_level($id_skpd,$level){
		$this->db->where('id_skpd',$id_skpd);
		$this->db->where('level_unit_kerja',$level);
		$q = $this->db->get('ref_unit_kerja');
		return $q->result();
	}

	public function get_unit_kerja_by_id_induk($id_skpd,$id_induk){
		$this->db->where('id_skpd',$id_skpd);
		$this->db->where('id_induk',$id_induk);
		$q = $this->db->get('ref_unit_kerja');
		return $q->result();
	}

	public function get_skpd_except_this_id_skpd($id_skpd){
		$this->db->where('id_skpd !=',$id_skpd);
		$this->db->order_by('nama_skpd','ASC');
		$q = $this->db->get('ref_skpd');
		return $q->result();
	}

	public function get_unit_kerja_by_id_skpd($id_skpd){
		// GET KEPALA SKPD
		$this->db->where('id_unit_kerja','0');
		$results1 = $this->db->get('ref_unit_kerja');

		$this->db->where('id_skpd',$id_skpd);
		$this->db->order_by('nama_unit_kerja','ASC');
		$results2 = $this->db->get('ref_unit_kerja');

		$results = array();

		if ($results1->num_rows()) 
		{
		    $results = array_merge($results, $results1->result());
		}

		if ($results2->num_rows())
		{
		    $results = array_merge($results, $results2->result());
		}

		return $results;
	}

	public function get_pegawai_by_id_unit_kerja($id_unit_kerja, $id_skpd){
		$this->db->where('pegawai.id_unit_kerja',$id_unit_kerja);
		if ($id_unit_kerja=="0") {
			$this->db->where('pegawai.id_skpd',$id_skpd);
		}
		$this->db->order_by('pegawai.jenis_pegawai','ASC');
		$this->db->order_by('pegawai.nama_lengkap','ASC');
		$this->db->join('ref_jabatan', 'pegawai.id_jabatan = ref_jabatan.id_jabatan', 'left');
		$q = $this->db->get('pegawai');
		return $q->result();
	}

	public function get_kepala_skpd($id_skpd){
		$this->db->where('id_skpd',$id_skpd);
		$this->db->where('jenis_pegawai','kepala');
		$this->db->where('kepala_skpd','Y');
		$q = $this->db->get('pegawai')->row();
		if(empty($q)){
			$q = array('nama_lengkap'=>'Data belum tersedia');
			$q = (object) $q;
		}
		return $q;
	}

	public function get_kepala_unit_kerja($id_unit_kerja){
		$this->db->where('id_unit_kerja',$id_unit_kerja);
		$this->db->where('jenis_pegawai','kepala');
		$q = $this->db->get('pegawai')->row();
		if(empty($q)){
			$q = array('nama_lengkap'=>'Data belum tersedia');
			$q = (object) $q;
		}
		return $q;
	}

	public function get_staff_unit_kerja($id_unit_kerja){
		$this->db->where('id_unit_kerja',$id_unit_kerja);
		$this->db->where('jenis_pegawai','staff');
		$q = $this->db->get('pegawai');
		return $q->result();
	}

	public function delete_jabatan($id_jabatan){
		$this->db->delete('ref_jabatan',array('id_jabatan'=>$id_jabatan));
	}

	public function insert_jabatan($data,$id_skpd){
		$this->db->set('id_skpd',$id_skpd);
		return $this->db->insert('ref_jabatan',$data);

	}

	public function update_jabatan($data,$id_jabatan){
		unset($data['id_jabatan']);
		unset($data['level_unit_kerja']);
		return $this->db->update('ref_jabatan',$data,array('id_jabatan'=>$id_jabatan));

	}
	public function get_jabatan_by_id($id_jabatan){
		$this->db->where('id_jabatan',$id_jabatan);
		$q = $this->db->get('ref_jabatan');
		return $q->row();
	}

	public function get_jabatan_by_id_skpd($id_skpd){
		$this->db->where('ref_jabatan.id_skpd',$id_skpd);
		$this->db->join('ref_unit_kerja','ref_unit_kerja.id_unit_kerja = ref_jabatan.id_unit_kerja');
		$q = $this->db->get('ref_jabatan');
		return $q->result();
	}

	public function get_jabatan_by_unit_kerja($id_unit_kerja){
		$this->db->where('id_unit_kerja',$id_unit_kerja);
		$q = $this->db->get('ref_jabatan');
		return $q->result();
	}

	public function get_perencanaan_by_id_skpd($id_skpd){
		// $this->db->select("iku_sasaran_rpjmd.id_iku_sasaran_rpjmd, iku_sasaran_rpjmd.iku_sasaran_rpjmd, sasaran_rpjmd.id_sasaran_rpjmd, sasaran_rpjmd.sasaran_rpjmd, tujuan.id_tujuan, tujuan.tujuan, misi.id_misi, misi.misi, visi.id_visi, visi.visi");
		$this->db->where('iku_sasaran_rpjmd.id_skpd',$id_skpd);
		$this->db->join('sasaran_rpjmd', 'sasaran_rpjmd.id_sasaran_rpjmd = iku_sasaran_rpjmd.id_sasaran_rpjmd');
		$this->db->join('tujuan', 'tujuan.id_tujuan = sasaran_rpjmd.id_tujuan');
		$this->db->join('misi', 'misi.id_misi = tujuan.id_misi');
		$this->db->join('visi', 'visi.id_visi = misi.id_visi');
		$query = $this->db->get('iku_sasaran_rpjmd');
		return $query->result_array();
	}

}
