<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	public function get_visitor_days()
	{
		$this->db->where('date',date('Y-m-d'));
		$query = $this->db->get('visitor');
		return $query->num_rows();
	}

	public function get_visitor_all()
	{
		$query = $this->db->get('visitor');
		return $query->num_rows();
	}

	


	public function get_user()
	{
		$query = $this->db->get('user');
		return $query->num_rows();
	}
	
	public function get_post()
	{
		$query = $this->db->get('post');
		return $query->num_rows();
	}

	public function get_status(){
		//$this->db->group_by('user_statuses.user_id');
		$this->db->join('user','user.user_id=user_statuses.user_id');
		$query = $this->db->get('user_statuses');
		return $query->result();
	}

	


	public function get_notice_board(){
		$this->db->where('status','Y');
		$query = $this->db->get('notice_board');
		return $query->row();
	}


	

	

	public function get_download()
	{
		$query = $this->db->get('download');
		return $query->num_rows();
	}


	public function get_video()
	{
		$query = $this->db->get('video');
		return $query->num_rows();
	}


	public function get_misi()
	{
		$query = $this->db->get('misi');
		return $query->num_rows();
	}

	



}
?>