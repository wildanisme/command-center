<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_helpdesk_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_helpdesk;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_helpdesk');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_helpdesk',$this->nama_kategori_helpdesk);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_helpdesk');
	}
	public function update()
	{
		$this->db->where('id_kategori_helpdesk',$this->id_kategori_helpdesk);
		$this->db->set('nama_kategori_helpdesk',$this->nama_kategori_helpdesk);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_helpdesk');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_helpdesk',$this->id_kategori_helpdesk);
		$query = $this->db->get('kategori_helpdesk');
		foreach ($query->result() as $row) {
			$this->nama_kategori_helpdesk 	= $row->nama_kategori_helpdesk;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_helpdesk',$this->id_kategori_helpdesk);
		$query = $this->db->delete('kategori_helpdesk');	
	}
}
?>