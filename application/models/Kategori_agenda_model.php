<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_agenda_model extends CI_Model
{
	public $id_kategori;
	public $nama_kategori_agenda;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('kategori_agenda');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_kategori_agenda',$this->nama_kategori_agenda);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_agenda');
	}
	public function update()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$this->db->set('nama_kategori_agenda',$this->nama_kategori_agenda);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_agenda');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$query = $this->db->get('kategori_agenda');
		foreach ($query->result() as $row) {
			$this->nama_kategori_agenda 	= $row->nama_kategori_agenda;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$query = $this->db->delete('kategori_agenda');	
	}
}
?>