<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan_model extends CI_Model
{
	// public $id_kategori;
	// public $nama_kategori_agenda;
	// public $status;

	public function create_view()
	{
		$query = "
			CREATE OR REPLACE VIEW keuangan_view AS
			SELECT keuangan_ref.kode_rekening, keuangan_ref.uraian, 
			keuangan_perencanaan.apbd_murni, SUM(CASE WHEN keuangan_realisasi.jenis_realisasi = 'murni' THEN keuangan_realisasi.anggaran_realisasi END) AS anggaran_realisasi_murni, 
			keuangan_perencanaan.apbd_perubahan, SUM(CASE WHEN keuangan_realisasi.jenis_realisasi = 'perubahan' THEN keuangan_realisasi.anggaran_realisasi END) AS anggaran_realisasi_perubahan 
			FROM `keuangan_perencanaan` 
			LEFT JOIN `keuangan_realisasi` ON `keuangan_perencanaan`.`id_keuangan` = `keuangan_realisasi`.`id_keuangan` 
			JOIN `keuangan_ref` ON `keuangan_perencanaan`.`id_ref_keuangan` = `keuangan_ref`.`id_ref_keuangan`
			-- WHERE keuangan_perencanaan.tahun = YEAR(CURDATE())
			GROUP BY `keuangan_ref`.`kode_rekening` 
			ORDER BY `keuangan_perencanaan`.`tahun` DESC, `keuangan_ref`.`kode_rekening` ASC
		";
		return $this->db->query($query);
	}

	public function get_keuangan_view()
	{
		$this->db->order_by('kode_rekening','ASC');
		$query = $this->db->get('keuangan_view');
		return $query->result_array();
	}

	public function get_all_ref()
	{
		$this->db->order_by('kode_rekening','ASC');
		$query = $this->db->get('keuangan_ref');
		return $query->result();
	}

	public function select_by_id_ref($id)
	{
		$this->db->where('id_ref_keuangan', $id);
		$query = $this->db->get('keuangan_ref');
		return $query->row();
	}

	public function delete_ref($id = null)
	{
		if ($id) {
			$this->db->where('id_ref_keuangan',$id);
			$query = $this->db->delete('keuangan_ref');	
		}
	}

	public function get_all_perencanaan()
	{
		$this->db->order_by('keuangan_perencanaan.tahun','DESC');
		$this->db->order_by('keuangan_ref.kode_rekening','ASC');
		$this->db->join('keuangan_ref', 'keuangan_perencanaan.id_ref_keuangan = keuangan_ref.id_ref_keuangan');
		$query = $this->db->get('keuangan_perencanaan');
		return $query->result();
	}

	public function select_by_id_perencanaan($id)
	{
		$this->db->where('id_keuangan', $id);
		$query = $this->db->get('keuangan_perencanaan');
		return $query->row();
	}

	public function delete_perencanaan($id = null)
	{
		if ($id) {
			$this->db->where('id_keuangan',$id);
			$query = $this->db->delete('keuangan_perencanaan');	
		}
	}

	public function get_all_realisasi()
	{
		$this->db->select('*');
		$this->db->select("SUM(CASE
			WHEN keuangan_realisasi.jenis_realisasi = 'murni'
			THEN keuangan_realisasi.anggaran_realisasi
			END) AS anggaran_realisasi_murni");
		$this->db->select("SUM(CASE
			WHEN keuangan_realisasi.jenis_realisasi = 'perubahan'
			THEN keuangan_realisasi.anggaran_realisasi
			END) AS anggaran_realisasi_perubahan");
		$this->db->join('keuangan_perencanaan', 'keuangan_realisasi.id_keuangan = keuangan_perencanaan.id_keuangan');
		$this->db->join('keuangan_ref', 'keuangan_perencanaan.id_ref_keuangan = keuangan_ref.id_ref_keuangan');
		$this->db->group_by('keuangan_realisasi.id_keuangan');
		$this->db->order_by('keuangan_perencanaan.tahun','DESC');
		$this->db->order_by('keuangan_ref.kode_rekening','ASC');
		$query = $this->db->get('keuangan_realisasi');
		return $query->result();
	}

	public function select_by_id_detail_realisasi($id)
	{
		$this->db->where('id_keuangan', $id);
		$this->db->order_by('tgl_realisasi','DESC');
		$query = $this->db->get('keuangan_realisasi');
		return $query->result();
	}

	public function select_by_id_realisasi($id)
	{
		$this->db->where('id_realisasi', $id);
		$query = $this->db->get('keuangan_realisasi');
		return $query->row();
	}

	public function delete_realisasi($id = null)
	{
		if ($id) {
			$this->db->where('id_realisasi',$id);
			$query = $this->db->delete('keuangan_realisasi');	
		}
	}

	public function insert()
	{
		$this->db->set('nama_kategori_agenda',$this->nama_kategori_agenda);
		$this->db->set('status',$this->status);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->insert('kategori_agenda');
	}
	public function update()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$this->db->set('nama_kategori_agenda',$this->nama_kategori_agenda);
		$this->db->set('kategori_slug',$this->kategori_slug);
		$this->db->set('status',$this->status);
		$this->db->update('kategori_agenda');
	}
	

	public function set_by_id()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$query = $this->db->get('kategori_agenda');
		foreach ($query->result() as $row) {
			$this->nama_kategori_agenda 	= $row->nama_kategori_agenda;
			$this->kategori_slug	= $row->kategori_slug;
			$this->status	= $row->status;
		}
	}
	
	public function delete()
	{
		$this->db->where('id_kategori_agenda',$this->id_kategori_agenda);
		$query = $this->db->delete('kategori_agenda');	
	}
}
?>