<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sop_model extends CI_Model
{
	public $id_sop;
	public $nama_sop;
	public $sop_slug;
	public $keterangan;
	public $gambar;
	public $status;

	public function get_all()
	{
		$query = $this->db->get('sop');
		return $query->result();
	}

	public function insert()
	{
		$this->db->set('nama_sop',$this->nama_sop);
		$this->db->set('keterangan',$this->keterangan);
		$this->db->set('gambar',$this->gambar);
		$this->db->set('sop_slug',$this->sop_slug);
		$this->db->set('status',$this->status);
		$this->db->insert('sop');
	}
	
	public function update()
	{
		$this->db->where('id_sop',$this->id_sop);
		$this->db->set('nama_sop',$this->nama_sop);
		$this->db->set('keterangan',$this->keterangan);
		$this->db->set('gambar',$this->gambar);
		$this->db->set('sop_slug',$this->sop_slug);
		$this->db->set('status',$this->status);
		$this->db->update('sop');
	}
	
	public function check_availability($old_tema,$tema){
		if ($old_tema==$tema){
			return true;
		}
		else{
			$this->db->where('nama_sop',$tema);
			$query = $this->db->get('sop');
			if ($query->num_rows() == 0){
				return true;
			}
			else{
				return false;
			}
		}
	}
	public function set_by_id()
	{
		$this->db->where('id_sop',$this->id_sop);
		$query = $this->db->get('sop');
		foreach ($query->result() as $row) {
			$this->nama_sop 	= $row->nama_sop;
			$this->keterangan	= $row->keterangan;
			$this->gambar	= $row->gambar;
			$this->sop_slug	= $row->sop_slug;
			$this->status	= $row->status;
		}
	}
	public function set_by_slug()
	{
		$this->db->where('sop_slug',$this->tema_slug);
		$query = $this->db->get('sop');
		foreach ($query->result() as $row) {
			$this->nama_sop 	= $row->nama_sop;
			$this->keterangan	= $row->keterangan;
			$this->gambar	= $row->gambar;
			$this->sop_slug	= $row->sop_slug;
			$this->status	= $row->status;
		}
	}
	public function delete()
	{
		$this->db->where('id_sop',$this->id_sop);
		$query = $this->db->delete('sop');	
	}
}
?>