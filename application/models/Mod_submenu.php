<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_submenu extends CI_Model {

    var $table = 'tbl_submenu';
    var $tblakses = 'tbl_akses_submenu';
    var $column_search = array('a.nama_submenu','a.link','b.nama_menu','a.is_active'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $column_order = array('nama_submenu','link','icon','nama_menu','is_active',null);
    var $order = array('id_submenu' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    public function get_by_nama($nama_submenu)
    {
        $this->db->from($this->table);
        $this->db->where('nama_submenu',$nama_submenu);
        $query = $this->db->get();
        return $query->row();
    }
    function getAll()
    {
        $this->db->select('a.*,b.nama_menu');
        $this->db->join('tbl_menu b','a.id_menu=b.id_menu');
       return $this->db->get('tbl_submenu a');
    }
    function view_submenu($id)
    {	
    	$this->db->where('id_submenu',$id);
    	return $this->db->get('tbl_submenu');
    }

    function get_submenu($id)
    {   
        $this->db->where('id_submenu',$id);
        return $this->db->get('tbl_submenu')->row();
    }

    function edit_submenu($id)
    {	
    	$this->db->where('id_submenu',$id);
    	return $this->db->get('tbl_submenu');
    }

    function insertsubmenu($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function insert_akses_submenu($tbl_akses_submenu, $data)
    {
        $insert = $this->db->insert($tbl_akses_submenu, $data);
        return $insert;
    }

    function updatesubmenu($id, $data)
    {
        $this->db->where('id_submenu', $id);
        $this->db->update('tbl_submenu', $data);
    }
    function deletesubmenu($id, $table)
    {
        $this->db->where('id_submenu', $id);
        $this->db->delete($table);
    }

    function deleteakses($id, $tbl_akses_submenu){
        $this->db->where('id_submenu', $id);
        $this->db->delete($tbl_akses_submenu);
    }
}

/* End of file Mod_login.php */
