<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_menu extends CI_Model {

    var $table = 'tbl_menu';
    var $tbl_akses_menu = 'tbl_akses_menu';
    var $column_order = array('nama_menu','link','icon','urutan','is_active');
    var $column_search = array('nama_menu','link','nama_menu','is_active'); 
    var $order = array('id_menu' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
  


    function getAll()
    {
       return $this->db->get('tbl_menu');
    }

    function view_menu($id)
    {	
    	$this->db->where('id_menu',$id);
    	return $this->db->get('tbl_menu');
    }

    function get_nama_menu($nama_menu){
        $this->db->from($this->table);
        $this->db->where('nama_menu',$nama_menu);
        $query = $this->db->get();

        return $query->row();
    }

    function get_menu($id)
    {   
        $this->db->where('id_menu',$id);
        return $this->db->get('tbl_menu')->row();
    }

    function edit_menu($id)
    {	
    	$this->db->where('id_menu',$id);
    	return $this->db->get('tbl_menu');
    }

    function insertMenu($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    //khusus administrator
    function insertaksesmenu($tbl_akses_menu, $data)
    {
        $insert = $this->db->insert($tbl_akses_menu, $data);
        return $insert;
    }

    function updateMenu($id_menu, $data)
    {
        $this->db->where('id_menu', $id_menu);
        $this->db->update('tbl_menu', $data);
    }
    function deleteMenu($id, $table)
    {
        $this->db->where('id_menu', $id);
        $this->db->delete($table);
    }

    function deleteakses($id, $tbl_akses_menu){
        $this->db->where('id_menu', $id);
        $this->db->delete($tbl_akses_menu);
    }
    function deleteakses_submenu($id, $tbl_akses_submenu){
        $this->db->where('id_menu', $id);
        $this->db->delete($tbl_akses_submenu);
    }
}

/* End of file Mod_login.php */
