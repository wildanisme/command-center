<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organisasi_model extends CI_Model{

	public function get_all(){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
	
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("organisasi.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("organisasi.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		$query = $this->db->get('organisasi');
		return $query->result();
	}

	public function get_total(){
		$this->db->where('status','aktif');
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("organisasi.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("organisasi.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		$query = $this->db->get('organisasi');
		return $query->num_rows();
	}


	//public function get_populer(){
	//	$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
	//	$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
	//	$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
	//	$this->db->where('status_antrian','Selesai');
	//	$this->db->group_by("antrian.id_layanan");
	//	$this->db->order_by("total", "DESC");
	//	$this->db->limit(5);
	//	$query = $this->db->get('ref_layanan');
	//	return $query->result();
		
	//}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like("organisasi.".$key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("organisasi.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("organisasi.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}


		if($keyword!="")
		{
			$this->db->where("kategori_organisasi.nama_kategori_organisasi like '%".$keyword."%' OR organisasi.nama_organisasi like '%".$keyword."%' OR organisasi.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "organisasi.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		$this->db->select("organisasi.*,kategori_organisasi.nama_kategori_organisasi, desa.desa, kecamatan.kecamatan");
		if($this->user_id) $this->db->where("organisasi.id_user",$this->user_id);
		
		$this->db->join("desa","desa.id_desa=organisasi.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=organisasi.id_kecamatan","left");
		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		//$this->db->where('organisasi.status','aktif');
		$this->db->order_by("organisasi.nama_organisasi", "ASC");
		$this->db->limit($hal,$mulai);
		$query = $this->db->get('organisasi');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("organisasi")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null,$filter=null) {
		if($keyword!="")
		{
			$this->db->where("kategori_organisasi.nama_kategori_organisasi like '%".$keyword."%' OR organisasi.nama_organisasi like '%".$keyword."%' OR organisasi.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "organisasi.tag like '%".$value."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}

		if($filter!=null){
			foreach($filter as $key => $value){
				$this->db->like("organisasi.".$key,$value);
			}
		}
		
		if($this->session->userdata("user_level")==3) //  operator desa
		{
			$this->db->where("organisasi.id_desa",$this->session->userdata("id_desa"));
		}

		if($this->session->userdata("user_level")==4) //  operator kecamatan
		{
			$this->db->where("organisasi.id_kecamatan",$this->session->userdata("id_kecamatan"));
		}

		
		//$this->db->where('id_layanan_induk','0');
		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		$query = $this->db->get('organisasi');
		return $query->num_rows();
	}


	public function get_by_id($id_organisasi){
		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		$this->db->where('id_organisasi',$id_organisasi);
		$query = $this->db->get('organisasi');
		return $query->row();
	}

	



	public function get_by_kategori($id_kategori_organisasi){
		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		$this->db->where('organisasi.id_organisasi',$id_organisasi);
		$this->db->where('organisasi.status','aktif');
		$query = $this->db->get('organisasi');
		return $query->result();
	}




	public function insert($data){
		return $this->db->insert('organisasi',$data);
	}


	public function update($data,$id_organisasi){
		return $this->db->update('organisasi',$data,array('id_organisasi'=>$id_organisasi));
	}
	public function delete($id_organisasi){
		$this->db->delete('organisasi',array('id_organisasi'=>$id_organisasi));
	}

	


	


	public function get_kategori()
	{
		$rs = $this->db->get("kategori_organisasi")->result();
		return $rs;
	}

	public function get($param=null)
	{
		if($param!=null)
		{
			foreach($param as $key=>$value)
			{
				$this->db->where($key,$value);
			}
		}
		$this->db->select("organisasi.*,kategori_organisasi.nama_kategori_organisasi, desa.desa, kecamatan.kecamatan");
		$this->db->join('kategori_organisasi', 'kategori_organisasi.id_kategori_organisasi = organisasi.id_kategori_organisasi', 'left');
		$this->db->join("desa","desa.id_desa=organisasi.id_desa","left");
		$this->db->join("kecamatan","kecamatan.id_kecamatan=organisasi.id_kecamatan","left");

		$this->db->order_by("organisasi.nama_organisasi", "ASC");
		
		$query = $this->db->get('organisasi');
		return $query->result();
	}


	public function hapus_organisasi($id_organisasi){
		$delete = true;
		$organisasi = $this->get_by_id($id_organisasi);
		//echo "<pre>";print_r($organisasi);die;
		if($this->user_id){
			if($organisasi->id_user == $this->user_id){
				$this->db->where('id_organisasi',$id_organisasi);
				$this->db->delete('organisasi');
			}
			else{
				$delete = false;
			}
		}
		else{
			$this->db->where('id_organisasi',$id_organisasi);
			$this->db->delete('organisasi');
		}

		if($delete && $organisasi->foto!=""){
			unlink("./data/organisasi/".$organisasi->foto);
		}

		return $delete;
		

	}


	public function is_authorize($id)
	{
		
		$data = $this->get_by_id($id);
		if(!$data)
		{
			return false;
		}
		else{
			if($this->session->userdata("user_level")==3) //  operator desa
			{
				if($data->id_desa != $this->session->userdata("id_desa"))
				{
					return false;
				}
			}

			if($this->session->userdata("user_level")==4) //  operator kecamatan
			{
				if($data->id_kecamatan != $this->session->userdata("id_kecamatan"))
				{
					return false;
				}
			}
			return true;
		}
	}

}
