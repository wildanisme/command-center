<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_layanan_model extends CI_Model{

	public function get_all(){
		// if($this->session->userdata('level')=='User'){
			// $this->db->where('id_layanan',$this->session->userdata('id_layanan'));
		// }
		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$query = $this->db->get('ref_layanan');
		return $query->result();
	}

	public function get_total(){
		$this->db->where('id_layanan_induk','0');
		$query = $this->db->get('ref_layanan');
		return $query->num_rows();
	}


	public function get_populer(){
		$this->db->select('antrian.id_layanan, ref_skpd.logo_skpd, ref_layanan.nama_layanan,ref_skpd.nama_skpd, count(antrian.status_antrian) as total');
		$this->db->join('antrian', 'antrian.id_layanan = ref_layanan.id_layanan', 'left');
		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$this->db->where('status_antrian','Selesai');
		$this->db->group_by("antrian.id_layanan");
		$this->db->order_by("total", "DESC");
		$this->db->limit(5);
		$query = $this->db->get('ref_layanan');
		return $query->result();
		
	}




	public function get_for_page($mulai,$hal,$filter='',$keyword='',$tag=null){
		
		// $this->db->offsett(0,6);
		if($filter!=''){
			foreach($filter as $key => $value){
				$this->db->like($key,$value);
			}
		}
//		else{
//			$this->db->limit($hal,$mulai);
//		}

		if($keyword!="")
		{
			$this->db->where("ref_skpd.nama_skpd like '%".$keyword."%' OR ref_layanan.nama_layanan like '%".$keyword."%' OR ref_layanan.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "ref_layanan.tag like '%".$keyword."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}

		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$this->db->where('ref_layanan.id_layanan_induk','0');
		$this->db->order_by("ref_layanan.loket", "ASC");
		$this->db->limit($hal,$mulai);
		$query = $this->db->get('ref_layanan');


		return $query->result();
	}

	public function getTag()
	{
		$rs = $this->db->where('tag is not null')->get("ref_layanan")->result();
		$tag = "";
		foreach($rs as $r)
		{
			$tag .= $r->tag.",";
		}
		$tagArr = explode(",", $tag);
		unset($tagArr[count($tagArr)-1]);
		$tagArr = array_unique($tagArr);
		
		return $tagArr;
	}
	
	public function get_total_row($keyword='',$tag=null) {
		if($keyword!="")
		{
			$this->db->where("ref_skpd.nama_skpd like '%".$keyword."%' OR ref_layanan.nama_layanan like '%".$keyword."%' OR ref_layanan.tag like '%".$keyword."%'");
		}
		
		if($tag!=null)
		{
			$tags = array();
			foreach($tag as $key=>$value)
			{
				$tags[] = "ref_layanan.tag like '%".$keyword."%'";
			}
			$where_tag = implode(" OR ",$tags);
			$this->db->where($where_tag);
		}
		
		//$this->db->where('id_layanan_induk','0');
		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$query = $this->db->get('ref_layanan');
		return $query->num_rows();
	}


	public function get_by_id($id_layanan){
		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$this->db->where('id_layanan',$id_layanan);
		$query = $this->db->get('ref_layanan');
		return $query->row();
	}

	public function status_antrian($hash){
		$this->db->where('hash_antrian',$hash);
		$this->db->join("ref_layanan","ref_layanan.id_layanan=antrian.id_layanan","left");
		$query = $this->db->get('antrian');
		return $query->row();
	}


	public function getSisaAntrian($loket,$id_antrian=null)
	{
		$this->db->where("tgl_antrian",date('Y-m-d'));
		$this->db->where("ref_layanan.loket",$loket);
		if($id_antrian!=null)
			$this->db->where("id_antrian < $id_antrian");
				//->where_in("status_antrian",["Menunggu","Pending"])
		$this->db->where("status_antrian","Menunggu");
		$this->db->join("ref_layanan","ref_layanan.id_layanan = antrian.id_layanan","left");
		$rs =$this->db->get("antrian")->num_rows();
		return $rs;
	}

	public function get_by_skpd($id_skpd){
		$this->db->join('ref_skpd', 'ref_skpd.id_skpd = ref_layanan.id_skpd', 'left');
		$this->db->where('ref_layanan.id_skpd',$id_skpd);
		$this->db->where('id_layanan_induk','0');
		$query = $this->db->get('ref_layanan');
		return $query->result();
	}



	public function insert($data){
		return $this->db->insert('ref_layanan',$data);
	}

	public function insert_persyaratan($data){
		return $this->db->insert('ref_persyaratan',$data);
	}
	
	public function update_persyaratan($data,$id_persyaratan){
		return $this->db->update('ref_persyaratan',$data,array("id_persyaratan" => $id_persyaratan));
	}


	public function update($data,$id_layanan){
		return $this->db->update('ref_layanan',$data,array('id_layanan'=>$id_layanan));
	}
	public function delete($id_layanan){
		$this->db->delete('ref_layanan',array('id_layanan'=>$id_layanan));
	}

	public function hapus_persyaratan($id_persyaratan) {
		$rs = $this->db->where("id_persyaratan",$id_persyaratan)->get("ref_persyaratan")->row();
		if(!empty($rs->file) && $rs->file!=""){
			if(file_exists('./data/file_persyaratan/'.$rs->file))
				unlink('./data/file_persyaratan/'.$rs->file);
		}
		$this->db->delete('ref_persyaratan',array('id_persyaratan'=>$id_persyaratan));

	}
	public function get_persyaratan($id_layanan){
		$this->db->where('id_layanan',$id_layanan);
		$query = $this->db->get('ref_persyaratan');

		return $query->result();
	}
	
	public function get_persyaratan_by_id($id_persyaratan){
		$this->db->where('id_persyaratan',$id_persyaratan);
		$query = $this->db->get('ref_persyaratan');

		return $query->row();
	}


	public function get_sub_layanan($id_layanan){
		$this->db->where('id_layanan_induk',$id_layanan);
		$query = $this->db->get('ref_layanan');
		return $query->result();
	}

	public function get_persyaratan_sub_layanan ($id_layanan_sub){
		$this->db->where('id_layanan',$id_layanan_sub);
		$query = $this->db->get('ref_persyaratan');
		return $query->result();

	}



	public function hapus_layanan($id_layanan){
	
		$this->db->where('id_layanan',$id_layanan);
		$this->db->delete('ref_layanan');

		$this->db->where('id_layanan',$id_layanan);
		$this->db->delete('ref_persyaratan');

		$this->db->where('id_layanan_induk',$id_layanan);
		$this->db->delete('ref_layanan');
		
		$this->db->where('id_layanan',$id_layanan);
		$this->db->delete('antrian');

	}

	public function update_jadwal($data,$id_layanan){

		$this->db->where('id_layanan', $id_layanan);
		$this->db->update('ref_layanan',$data);
	}

	public function insert_sublayanan($data){
		return $this->db->insert('ref_layanan',$data);
	}

	


	

}
