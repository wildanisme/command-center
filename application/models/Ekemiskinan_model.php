<?php defined ('BASEPATH') OR exit ('No direct script access allowed');

class Ekemiskinan_model extends CI_model
{
	private $_table = "card_ekemiskinan";

	public function __construct() {
			parent::__construct();
			$this->load->database();
	}

	public $id;
	public $jml_kemiskinan;
	// public $sasaran;
	// public $program;
	// public $kegiatan;
	public $tahun;

	public function rules()
	{
		return [
			[
				'field' => 'jml_kemiskinan',
				'label' => 'Jumlah Kemiskinan',
				'rules' => 'trim|required|numeric'
			],
			[
				'field' => 'tahun',
				'label' => 'Tahun',
				'rules' => 'trim|required|numeric'
			],
			
		];
	}

	public function getAll()
	{
		$this->db->order_by('id', 'DESC');
		return $this->db->get($this->_table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->_table, ["id" =>$id])->row();
	}


	public function getByTahun($tahun)
	{
		return $this->db->get_where($this->_table, ["tahun" =>$tahun])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this -> jml_kemiskinan = $post["jml_kemiskinan"];
		// $this -> sasaran = $post["sasaran"];
		// $this -> program = $post["program"];
		// $this -> kegiatan = $post["kegiatan"];
		$this -> tahun = $post ["tahun"];
		return $this->db->insert($this->_table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this -> id = $post["id"];
		$this -> jml_kemiskinan = $post["jml_kemiskinan"];
		// $this -> sasaran = $post["sasaran"];
		// $this -> program = $post["program"];
		// $this -> kegiatan = $post["kegiatan"];
		$this -> tahun = $post ["tahun"];
		return $this->db->Update ($this->_table, $this, array ('id' => $post ['id']));
	}

	public function delete ($id)
	{
		return $this->db->delete ($this->_table, array ("id" => $id));
	}
}