<?php $this->load->view('blog/src/header_x');?>


        <div class="clearfix"></div>
        <!-- Header Container / End -->


 <?php if(!empty($foto)){ ?>

<div class="listing-slider mfp-gallery-container margin-bottom-0" > <?php foreach ($foto as $f ) {
    ?>
	<a href="<?php echo base_url().'data/listing/';?><?=$f->foto?>" data-background-image="<?php echo base_url().'data/listing/';?><?=$f->foto?>" class="item mfp-gallery" title="Title 1"></a>
    <?php
  }
  ?>
</div>

<?php } ?>






<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">

			<!-- Titlebar -->
			<div id="titlebar" class="listing-titlebar">
				<div class="listing-titlebar-title">
					<h2><?=$detail->nama_listing;?> <span class="listing-tag"><?=$detail->nama_kategori_listing;?></span></h2>
					<span>
						<a href="#listing-location" class="listing-address">
							<i class="fa fa-map-marker"></i>
							<?=$detail->alamat;?>
						</a>
					</span>
					
				</div>
			</div>

			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#listing-overview" class="active">Deskripsi</a></li>
          <?php if(!empty($katalog)){ ?>
          <li><a href="#listing-pricing-list">Katalog</a></li>
          <?php }?>
          <li><a href="#listing-location">Lokasi</a></li>
				
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="listing-overview" class="listing-section">

				<!-- Description -->

				<p>
        <?=$detail->deskripsi;?>  
      </p>

			

			
			</div>

      <?php if(!empty($katalog)){
					
          ?>
			<!-- Food Menu -->
			<div id="listing-pricing-list" class="listing-section">
				<h3 class="listing-desc-headline margin-top-70 margin-bottom-30">Katalog</h3>

				<div class="show-more">
					<div class="pricing-list-container">
						
						<!-- Food List -->
					
						<ul>
          <?php foreach ($katalog as $k ) {
            ?>
            
							<li>
								<h5><?=$k->nama_katalog?></h5>
								<p><?=$k->deskripsi_katalog?></p>
								<?php
								if ($k->harga) {
									$rupiah = "Rp " . number_format($k->harga,2,',','.');
								}else{
									$rupiah = "-";
								}
								?>
								<span><?php  
								echo $rupiah;?> </span>
              </li>
              
						
              <?php
          }
          ?>
						</ul>

				

					</div>
				</div>
				<a href="#" class="show-more-button" data-more-title="Selebihnya" data-less-title="Show Less"><i class="fa fa-angle-down"></i></a>
			</div>
			<!-- Food Menu / End -->
      <?php }
      ?>
		


			<!-- Location -->
			<div id="listing-location" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Lokasi</h3>

				<div id="singleListingMap-container">
					<div id="singleListingMap" data-latitude="<?=$detail->latitude;?>" data-longitude="<?=$detail->longitude;?>" data-map-icon="im im-icon-Location-2"></div>
					<a href="#" id="streetView">Street View</a>
				</div>
			</div>
				
	

		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-75 sticky">



			<!-- Contact -->
			<div class="boxed-widget margin-top-35">
				<h3><i class="sl sl-icon-pin"></i> Kontak</h3>
				<ul class="listing-details-sidebar">
					<li><i class="sl sl-icon-phone"></i> <?=$detail->telepon?></li>
					<li><i class="sl sl-icon-globe"></i> <a href="<?=$detail->website?>"><?=$detail->website?></a></li>
					<li><i class="fa fa-envelope-o"></i> <a href="#"><?=$detail->email?></a></li>
				</ul>

				<ul class="listing-details-sidebar social-profiles">
					<li><a href="<?=$detail->facebook?>" target="_blank" class="facebook-profile"><i class="fa fa-facebook-square"></i> <?=$detail->facebook?></a></li>
					<li><a href="<?=$detail->twitter?>" target="_blank" class="twitter-profile"><i class="fa fa-twitter"></i> <?=$detail->twitter?></a></li>
					<!-- <li><a href="#" class="gplus-profile"><i class="fa fa-google-plus"></i> Google Plus</a></li> -->
				</ul>

			
			</div>
			<!-- Contact / End-->
			

			<!-- Opening Hours -->
			<div class="boxed-widget opening-hours margin-top-35">
				<div class="listing-badge now-open">Now Open</div>
				<h3><i class="sl sl-icon-clock"></i> Jadwal Buka</h3>
				<ul>
        <li>
								<div class="row">
									<div class="col-md-7">
										Senin
									</div>
									<div class="col-md-5">
										<?php if ($detail->senin == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_senin ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_senin ,"",-3);
										}
										?>
										
									</div>
								</div>
							</li>
							

							<li>
								<div class="row">
									<div class="col-md-7">
										Selasa
									</div>
									<div class="col-md-5">
										<?php if ($detail->selasa == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_selasa ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_selasa ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Rabu
									</div>
									<div class="col-md-5">
										<?php if ($detail->rabu == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_rabu ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_rabu ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Kamis
									</div>
									<div class="col-md-5">
										<?php if ($detail->kamis == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_kamis ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_kamis ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Jumat
									</div>
									<div class="col-md-5">
										<?php if ($detail->jumat == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_jumat ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_jumat ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Sabtu
									</div>
									<div class="col-md-5">
										<?php if ($detail->sabtu == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_sabtu ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_sabtu ,"",-3);
										}
										?>
									</div>
								</div>
              </li>

              <li>
								<div class="row">
									<div class="col-md-7">
										Minggu
									</div>
									<div class="col-md-5">
										<?php if ($detail->minggu == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_minggu ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_minggu ,"",-3);
										}
										?>
									</div>
								</div>
              </li>

              

				</ul>
			</div>
			<!-- Opening Hours / End -->



		</div>
		<!-- Sidebar / End -->

	</div>
</div>

<?php $this->load->view('blog/src/footer_x');?>
<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>

</body>
</html>