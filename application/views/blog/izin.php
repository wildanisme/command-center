<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->


<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>Daftar Perizinan</h2><span>yang dilayani oleh DPMPTP Kabupaten Bogor</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="#">Home</a></li>
            <li>Perizinan</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>


<!-- Content
================================================== -->
<div class="container">
  <div class="row">
<!-- Sidebar
    ================================================== -->
    <div class="col-lg-3 col-md-4">
      <div class="sidebar">

        <!-- Widget -->
        <div class="widget margin-bottom-40">
          <h3 class="margin-top-0 margin-bottom-30">Filter</h3>

          <form action="<?php echo base_url();?>izin" method='get'>
          <!-- Row -->
          <div class="row with-forms">
            <!-- Cities -->
            <div class="col-md-12">
              <input type="text" name="s" placeholder="Nama Perizinan?" value="<?=(!empty($search)) ? $search : ''?>"/>
            </div>
          </div>
          <!-- Row / End -->


          <!-- Row -->
          <div class="row with-forms">
            <!-- Type -->
            <div class="col-md-12">
              <select data-placeholder="Bidang Perizinan" class="chosen-select form-control" name="id_bidang" id="id_bidang" onchange='getIzin()'>
                <option value="" selected>Semua</option> 
                <?php
                  foreach ($bidang_izin as $key) {
                    $selected = $key->id_bidang == $this->input->get_post('id_bidang',FALSE) ? "selected" : "";
                    echo "<option value='{$key->id_bidang}' {$selected}>{$key->nama_bidang}</option>";
                  }
                ?>         
              </select>
            </div>
          </div>
          <!-- Row / End -->

           <!-- Row -->
          <div class="row with-forms">
            <!-- Type -->
            <div class="col-md-12">
              <select data-placeholder="Jenis Perizinan" class="chosen-select form-control" name="id_jenis_izin" id="id_jenis_izin" onchange='getSubizin()'>
                <option value="" selected>Semua</option>  
                <?php
                  foreach ($jenis_izin as $key) {
                    $selected = $key->id_jenis_izin."-".$key->nama_jenisizin == $this->input->get_post('id_jenis_izin',FALSE) ? "selected" : "";
                    echo "<option value='{$key->id_jenis_izin}' {$selected}>{$key->nama_jenisizin}</option>";
                  }
                ?>           
              </select>
            </div>
          </div>
          <!-- Row / End -->

             <!-- Row -->
          <div class="row with-forms">
            <!-- Type -->
            <div class="col-md-12">
              <select data-placeholder="Sub-Jenis Perizinan" class="chosen-select form-control" name="id_sub_jenis_izin" id="id_sub_jenis_izin">
                <option value="" selected>Semua</option>  
                <?php
                  foreach ($sub_jenis_izin as $key) {
                    $selected = $key->id_sub_jenis_izin."-".$key->nama_sub_jenisizin == $this->input->get_post('id_sub_jenis_izin',FALSE) ? "selected" : "";
                    echo "<option value='{$key->id_sub_jenis_izin}' {$selected}>{$key->nama_sub_jenisizin}</option>";
                  }
                ?>           
              </select>
            </div>
          </div>
          <!-- Row / End -->



        


          <!-- More Search Options -->
          <a href="#" class="more-search-options-trigger margin-bottom-5 margin-top-20 active" data-open-title="Filter Lainya" data-close-title="Filter Lainya"></a>

          <div class="more-search-options relative active" style="display: block;">

            <!-- Checkboxes -->
            <div class="checkboxes one-in-row margin-bottom-15">
          
              <input id="check-a" type="checkbox" name="retribusi" <?=($this->input->get_post('retribusi',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-a">retribusi (Berbayar)</label>

              <input id="check-b" type="checkbox" name="nonretribusi" <?=($this->input->get_post('nonretribusi',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-b">Non-Retribusi (Gratis)</label>

              <input id="check-c" type="checkbox" name="survey" <?=($this->input->get_post('survey',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-c">Survey</label>

              <input id="check-d" type="checkbox" name="nonsurvey" <?=($this->input->get_post('nonsurvey',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-d">Non-Survey</label>

              <input id="check-e" type="checkbox" name="rekomendasi" <?=($this->input->get_post('rekomendasi',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-e">Rekom. Dinas Terkait</label>

              <input id="check-f" type="checkbox" name="nonrekomendasi" <?=($this->input->get_post('nonrekomendasi',FALSE) == "on") ? "checked" : "";?>>
              <label for="check-f">Non. Rekom Dinas Terkait</label>

           
          
            </div>
            <!-- Checkboxes / End -->

          </div>
          <!-- More Search Options / End -->

          <button type="submit" class="button fullwidth margin-top-25">Cari</button>
          </form>
        </div>
        <!-- Widget / End -->

      </div>
    </div>
    <!-- Sidebar / End -->

    <div class="col-lg-9 col-md-8 padding-right-30">

      <!-- Sorting / Layout Switcher -->
      

      <div class="row">


      <?php foreach ($query as $izin) : 
      ?>

        <!-- Listing Item -->
        <div class="col-lg-12 col-md-12">
          <div class="listing-item-container list-layout">
            <a href="<?php echo base_url(). "izin/detail/".$izin->id_jenis_izin_real ;?>" class="listing-item">
              
              <!-- Image -->
              <div class="listing-item-image">
                <img src="<?php echo base_url().'data/';?>icon/imb.jpg" alt="" >
                <span class="tag"><?=($izin->retribusi=="Y")? "Retribusi" : "Gratis"?></span>
              </div>
              
              <!-- Content -->
              <div class="listing-item-content">
                
                <div class="listing-item-inner">
                  <h3><?=$izin->nama_jenisizin?></h3>
                  <span><?=$izin->nama_bidang?></span>
                 
                </div>
              </div>
            </a>
          </div>
        </div>
        <!-- Listing Item / End -->
      <?php endforeach; ?>

        
      </div>

      <!-- Pagination -->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- Pagination -->
          <div class="pagination-container margin-top-20 margin-bottom-40">
            <nav class="pagination">

              <?php
                $CI =& get_instance();
                $CI->load->library('pagination');

                $this->config->load('bootstrap_pagination');
                $config = $this->config->item('pagination');

                $config['base_url'] = base_url(). 'izin/index/';
                $config['total_rows'] = $total_rows;
                $config['per_page'] = $per_page;
                // $config['attributes'] = array('class' => 'page-num marginleft2px');
                $config['page_query_string']=TRUE;

                $config['reuse_query_string'] = TRUE;
                $CI->pagination->initialize($config);
                $link = $CI->pagination->create_links();
                echo $link;
              ?>
            </nav>
          </div>
        </div>
      </div>
      <!-- Pagination / End -->

    </div>


    
  </div>
</div>
    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>


       
          
<script type="text/javascript">

function getIzin(){
    var id = $('#id_bidang').val();
    $('#id_jenis_izin').html('<option value="">Semua</option>');
    $('#id_sub_jenis_izin').html('<option value="">Semua</option>');
    $.post("<?php echo base_url();?>izin/get_izin/"+id,{},function(obj){
      $('#id_jenis_izin').html(obj);
      $('#id_jenis_izin').chosen();
      $('#id_jenis_izin').trigger("chosen:updated");
    });
    
  }

function getSubizin(){
    var id = $('#id_jenis_izin').val();
    $('#id_sub_jenis_izin').html('<option value="">Semua</option>');
    $.post("<?php echo base_url();?>izin/get_sub_izin/"+id,{},function(obj){
      $('#id_sub_jenis_izin').html(obj);
      $('#id_sub_jenis_izin').chosen();
      $('#id_sub_jenis_izin').trigger("chosen:updated");
    });
    
  }


</script>