<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container" style='min-height:440px;'>
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Grafik Izin</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href=" <?php echo base_url().'grafikizin'; ?> ">grafik</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="row blog-page">

          <!-- Start Blog Posts -->
          <div class="col-md-3 blog-box">
     
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filter Grafik</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <form id="laporan" class="form-horizontal form-label-left" method=post target="_blank" action="<?php echo base_url()."grafikizin/grafik/";?>">
          
         
           


           <!--
            <div class="form-group">
              <label>Bulan <i>(wajib)</i></label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="bulan" id="bulan">
                
                <option value="" selected>Pilih</option>
                <?php
                  for($bln=1;$bln<=12;$bln++){
                    echo "<option value='$bln'>$bulan[$bln]</option>";
                  }
                ?>
              </select>
            </div>-->
            <div class="form-group">
              <label>Tahun* <i>(wajib)</i></label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="tahun" id="tahun">
                
                <option value="" selected>Pilih</option>
                <?php
                  $tahun_m = date('Y');
                  while($tahun_m > $tahun_minimal){
                    echo "<option value='$tahun_m'>$tahun_m</option>";
                    $tahun_m--;
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label>Jenis Izin</label>
            </div>
            <div class="form-group" id="">
              <select class="form-control" name="id_jenis_izin" id='id_jenis_izin'>
                
                <option value="" selected>Pilih</option>
                <?php
                  foreach($jenisizin as $row){
                    echo "<option value='$row->id_jenis_izin'>$row->nama_jenisizin</option>";
                  }
                ?>
              </select>
            </div>


                       <div class="form-group">
                    <label>Provinsi* (wajib)</label>
                    <select class='form-control' name='kd_provinsi_perusahaan' id='id_provinsi' onchange='getKabupaten()'>
                      <option value='' selected>Pilih</option> 
                      <?php
                        foreach($provinsi as $row){
                          echo "<option value='$row->id_provinsi'>$row->provinsi</option>";
                        }
                        
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kabupaten/Kota* (wajib)</label>
                    <select class='form-control' name='kd_kabupaten_perusahaan' id='id_kabupaten' onchange='getKecamatan()'>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class='form-control' name='kd_kecamatan_perusahaan' id='id_kecamatan' onchange='getDesa()'>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kelurahan/Desa</label>
                    <select class='form-control' name='kd_desa_perusahaan' id='id_desa'>         
                    </select>
                  </div>  

                      <div class="form-group">
                          <a href='<?= base_url()."grafikizin/index/";?>' class="btn btn-default">Reset</a>
                          
              <a onclick="submit_()" class="btn btn-danger" >Tampilkan</a>
                      </div>

                    </form>
            
                </div>
              </div>

<script>
  function submit_(download)
  {
    var tahun = $("#tahun").val();
    var id_provinsi = $("#id_provinsi").val();
    var id_kabupaten = $("#id_kabupaten").val();
    if (tahun!="" && id_provinsi!="" && id_kabupaten!=""){
      $("#laporan").submit();
    }
    else{
      alert("Filter bertanda * wajib diisi");
    }
  }

  function getKabupaten(){
    var id = $('#id_provinsi').val();
    $('#id_desa').html('<option value="">Pilih</option>');
    $('#id_kecamatan').html('<option value="">Pilih</option>');
    $.post("<?php echo base_url();?>grafikizin/get_kabupaten/"+id,{},function(obj){
      $('#id_kabupaten').html(obj);
    });
    
  }
  function getKecamatan(){
    $('#id_desa').html('<option value="">Pilih</option>');
    var id = $('#id_kabupaten').val();
    $.post("<?php echo base_url();?>grafikizin/get_kecamatan/"+id,{},function(obj){
      $('#id_kecamatan').html(obj);
    });
    
  }
  function getDesa(){
    var id = $('#id_kecamatan').val();
    $.post("<?php echo base_url();?>grafikizin/get_desa/"+id,{},function(obj){
      $('#id_desa').html(obj);
    });
  }


</script>

</script>

          </div>


          <div class="col-md-9 blog-box">
          
            
                        <?php
            $color = array(
              "#5E610B",
              "#610B0B",
              "#FF4000",
              "#5F4C0B",  
              "#0B614B",
              "#0B4C5F",
              "#0489B1",
              "#00BFFF",
              "#045FB4",
              "#FA5882",
              "#FF0040",
              "#B40431",
              "#610B21",
              "#4C0B5F",
              "#BF00FF",
              "#8904B1",
              "#210B61",
              "#4000FF",
              "#4000FF",
              "#6E6E6E",
            );
            ?>
                 <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     Grafik Perizinan
                      
                   </h3>

                     
                      <table class="table">
                       <tr>  <td>Kabupaten </td><td>:</td><td><strong><?php echo "$kabupaten[1]";?> </strong></td> </tr>
                        <tr>  <td>Kecamatan </td><td>:</td><td><strong><?php echo "$kecamatan[1]";?> </strong></td> </tr>
                         <tr>  <td>Desa </td><td>:</td><td><strong><?php echo "$desa[1]";?> </strong></td> </tr>
                          <tr>  <td>Tahun </td><td>:</td><td><strong><?php echo "$tahun";?> </strong></td> </tr>
                          <tr>  <td>Jenis Izin </td><td>:</td><td><strong><?php echo "$jenis_izin";?> </strong></td> </tr>

                        
                      </table>
                       
                  
              </div>
<!--
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                              <button class="btn btn-default" type="button">Go!</button>
                          </span>
                  </div>
                </div>
              </div>-->
            </div>
            <div class="clearfix"></div>

            <div class="row">


              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafik perizinan<small>Berdasarkan nilai investasi</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="gInvestasi"></canvas>
                  </div>
                  Total Investasi : <strong>Rp. <?php echo number_format($total_investasi); ?></strong>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafik perizinan<small>Berdasarkan jumlah izin</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="gJumlah0"></canvas>
                  </div>
                  
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafik perizinan<small>Perkecamatan</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="gJumlah"></canvas>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafik perizinan<small>Perkecamatan</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="gBidang"></canvas>
                  </div>
          <div>
          <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama bidang</th>
                          <th>Jumlah Izin</th>
                          <th>Tenaga Kerja</th>
                          <th>Nilai Investasi</th>
                        </tr>
            </thead>
                      <tbody>
            <?php
            $y=0;
            $total = array();
            $total['jumlah_izin'] = $total['tenaga_kerja'] = $total['investasi'] = 0 ;
              for ($u=0;$u<count($bidang);$u++)
              {
                if ($y>19) $y=0;
                echo"
                  <tr style ='color:".$color[$y]."'>
                    <td>".($u+1)."</td>
                    <td>$bidang[$u]</td>
                    <td>".$data_bidang[$u]['jumlah_izin']."</td>
                    <td>".number_format($data_bidang[$u]['tenaga_kerja'])."</td>
                    <td>".number_format($data_bidang[$u]['investasi'])."</td>
                  </tr>
                ";
                $y++;
                $total['jumlah_izin'] += $data_bidang[$u]['jumlah_izin'];
                $total['tenaga_kerja'] += $data_bidang[$u]['tenaga_kerja'];
                $total['investasi'] += $data_bidang[$u]['investasi'];
              }
              echo "
              <thead>
                <tr>
                  <th colspan=2>Total</td>
                  <th>".number_format($total['jumlah_izin'])."</th>
                  <th>".number_format($total['tenaga_kerja'])."</th>
                  <th>".number_format($total['investasi'])."</th>
                </tr>
                </thead>
              ";
            ?>
                      </tbody>
            </table>
          </div>
                </div>
        
                </div>

              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Grafik perizinan<small>Sektor Usaha</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="gSektor"></canvas>
                  </div>
          <div>
          <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama sektor</th>
                          <th>Jumlah Izin</th>
                          <th>Tenaga Kerja</th>
                          <th>Nilai Investasi</th>
                        </tr>
            </thead>
                      <tbody>
            <?php
            $y = 0;
            $total = array();
            $total['jumlah_izin'] = $total['tenaga_kerja'] = $total['investasi'] = 0 ;
              for ($u=0;$u<count($sektor);$u++)
              {
                if ($y>19) $y=0;
                echo"
                  <tr style ='color:".$color[$y]."'>
                    <td>".($u+1)."</td>
                    <td>$sektor[$u]</td>
                    <td>".$data_sektor[$u]['jumlah_izin']."</td>
                    <td>".number_format($data_sektor[$u]['tenaga_kerja'])."</td>
                    <td>".number_format($data_sektor[$u]['investasi'])."</td>
                  </tr>
                ";
                $y++;
                 $total['jumlah_izin'] += $data_bidang[$u]['jumlah_izin'];
                $total['tenaga_kerja'] += $data_bidang[$u]['tenaga_kerja'];
                $total['investasi'] += $data_bidang[$u]['investasi'];
              }
              echo "
              <thead>
                <tr>
                  <th colspan=2>Total</td>
                  <th>".number_format($total['jumlah_izin'])."</th>
                  <th>".number_format($total['tenaga_kerja'])."</th>
                  <th>".number_format($total['investasi'])."</th>
                </tr>
                </thead>
              ";
            ?>
                      </tbody>
            </table>
          </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
           
              </div>
            </div>
          </div>
<?php
  $bln = json_encode($bulan);
  
?>  
<script>
  var BULAN = new Array();
  <?php
    for($i=1;$i<=count($bulan);$i++)
    {
      echo "BULAN[$i] = '$bulan[$i]';";
    }
  ?>
  var KECAMATAN = new Array();
  <?php
    for($i=0;$i<count($KECAMATAN);$i++)
    {
      echo "KECAMATAN[$i] = '$KECAMATAN[$i]';";
    }
  ?>
  //console.log(KECAMATAN);
  var DATA_INVESTASI = new Array();
  <?php
    for($i=1;$i<=count($data_investasi);$i++)
    {
      echo "DATA_INVESTASI[$i] = '$data_investasi[$i]';";
    }
  ?>
  var DATA_IZIN0 = new Array();
  <?php
    for($i=1;$i<=count($data_izin0);$i++)
    {
      echo "DATA_IZIN0[$i] = '$data_izin0[$i]';";
    }
  ?>
  var DATA_IZIN = new Array();
  <?php
    for($i=0;$i<count($data_izin);$i++)
    {
      echo "DATA_IZIN[$i] = '$data_izin[$i]';";
    }
  ?>
 // console.log(DATA_IZIN);
  var BIDANG = new Array();
  <?php
    for($i=0;$i<count($bidang);$i++)
    {
      echo "BIDANG[$i] = '$bidang[$i]';";
    }
  ?>
  var DATA_BIDANG = new Array();
  var color = new Array();
  <?php
    for($i=0;$i<count($data_bidang);$i++)
    {
      echo "DATA_BIDANG[$i] = '".$data_bidang[$i]['jumlah_izin']."';";
    }
    for ($y=0;$y<count($color);$y++){
      echo "color[$y] = '$color[$y]';";
    }
  ?>
  var bidangColor = new Array();
  <?php
    $x = 0;
    for($i=0;$i<count($bidang);$i++)
    {
      if ($x>19) $x=0;
      echo "bidangColor[$i] = color[$x];";
      $x++;
    }
  ?>
  
  var SEKTOR = new Array();
  <?php
    for($i=0;$i<count($sektor);$i++)
    {
      echo "SEKTOR[$i] = '$sektor[$i]';";
    }
  ?>
  var DATA_SEKTOR = new Array();
  <?php
    for($i=0;$i<count($data_sektor);$i++)
    {
      echo "DATA_SEKTOR[$i] = '".$data_sektor[$i]['jumlah_izin']."';";
    }
  ?>
  var sektorColor = new Array();
  <?php
    $x = 0;
    for($i=0;$i<count($sektor);$i++)
    {
      if ($x>19) $x=0;
      echo "sektorColor[$i] = color[$x];";
      $x++;
    }
  ?>
  //console.log(BIDANG);
</script>
        <!-- /page content -->


    <!-- jQuery -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/Chart.js/dist/Chart.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>

    <!-- Chart.js -->
    <script>
    
      Chart.defaults.global.legend = {
        enabled: false
      };

      // Line chart
      var ctx = document.getElementById("gInvestasi");
    
      var gInvestasi = new Chart(ctx, {
        type: 'line',
        data: {
          labels: BULAN,
          datasets: [{
            label: "Nilai investasi",
            backgroundColor: "rgba(38, 185, 154, 0.31)",
            borderColor: "rgba(38, 185, 154, 0.7)",
            pointBorderColor: "rgba(38, 185, 154, 0.7)",
            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointBorderWidth: 1,
            data: DATA_INVESTASI
          }
      /*
      , {
            label: "My Second dataset",
            backgroundColor: "rgba(3, 88, 106, 0.3)",
            borderColor: "rgba(3, 88, 106, 0.70)",
            pointBorderColor: "rgba(3, 88, 106, 0.70)",
            pointBackgroundColor: "rgba(3, 88, 106, 0.70)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(151,187,205,1)",
            pointBorderWidth: 1,
            data: [82, 23, 66, 9, 99, 4, 2]
          }*/
      ]
      
        },
      });

      // Bar chart
      var ctx = document.getElementById("gJumlah0");
      var gJumlah = new Chart(ctx, {
        type: 'line',
        data: {
          labels: BULAN,
          datasets: [{
            label: 'Total perizinan',
            backgroundColor: "#26B99A",
            data: DATA_IZIN0
          },/* {
            label: '# of Votes',
            backgroundColor: "#03586A",
            data: [41, 56, 25, 48, 72, 34, 12]
          }*/
      ]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });


      // Bar chart
      var ctx = document.getElementById("gJumlah");
      var gJumlah = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: KECAMATAN,
          datasets: [{
            label: 'Total perizinan',
            backgroundColor: "#26B99A",
            data: DATA_IZIN
          },/* {
            label: '# of Votes',
            backgroundColor: "#03586A",
            data: [41, 56, 25, 48, 72, 34, 12]
          }*/
      ]
        },

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });

      // Doughnut chart
      var ctx = document.getElementById("gSektor");
      var data = {
        labels: SEKTOR,
        datasets: [{
          data: DATA_SEKTOR,
          backgroundColor: sektorColor,/*
          hoverBackgroundColor: [
            "#34495E",
            "#B370CF",
            "#CFD4D8",
            "#36CAAB",
            "#49A9EA"
          ]*/

        }]
      };

      var gSektor = new Chart(ctx, {
        type: 'doughnut',
        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
        data: data
      });


      // Doughnut chart
      var ctx = document.getElementById("gBidang");
      var data = {
        labels: BIDANG,
        datasets: [{
          data: DATA_BIDANG,
          backgroundColor: bidangColor,/*
          hoverBackgroundColor: [
            "#34495E",
            "#B370CF",
            "#CFD4D8",
            "#36CAAB",
            "#49A9EA"
          ]*/

        }]
      };

      var gBidang = new Chart(ctx, {
        type: 'doughnut',
        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
        data: data
      });


     
    </script>
    <!-- /Chart.js -->
  </body>
</html>






          <!end col kanan-->
          </div>





    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>