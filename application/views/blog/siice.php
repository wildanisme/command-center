<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
    .marginleft2px{
      margin-left: 2px;
    }
  </style>

  <link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>


  <!-- Start Header -->
  <header class="clearfix">
    <?php $this->load->view('blog/src/top_nav');?>
  </header>
  
  <!-- End Page Banner -->


<!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h3>SI ICE Mandiri</h3><span>Sistem Informasi Izin Cetak Mandiri</span>

          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="#">Home</a></li>
              <li>Si Ice Mandiri</li>
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>
<!-- Content
  ================================================== -->

  <div class="container" style="margin-top: 150px;">

    <div class="row sticky-wrapper">

     <div class="special_description_area mt-150">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="special_description_img" style="margin-top:-100px;">
              <img src="http://aplikasiperizinan.com/asset/siice/img/scr-img/mockapple.png" alt="Aplikasi DPMPTSP">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="special_description_content">
              <h2>SI ICE</h2>
              <p>Merupakan sebuah sistem informasi pelayanan perizinan dan non perizinan yang memungkinkan pemohon dapat mengunduh SK perizinan dan mencetaknya secara mandiri. Aplikasi ini berbasis <i>online</i> sehingga masyarakat dapat melakukan pengajuan perizinan dengan memanfaatkan teknologi internet. Sistem informasi ini sangat cocok digunakan di DPMPTSP (Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu) di kabupaten/kota. Sistem informasi ini dilengkapi dengan kemampuan-kemampuan yang dapat memudahkan baik dari sisi pemohon (masyarakat) maupun dari sisi petugas (DPMPTSP).
              </p>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="special_description_area mt-150">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="special_description_content">
              <h2>SK dengan  Digital Signature</h2>

              <p>Sebagai bentuk keabsahan atau legalitas dan keamanan, SK perizinan dilengkapi dengan <i>QRCODE</i> dan tanda tangan Digital
                yang terverifikasi oleh root ca KOMINFO.
              </p>

            </div>

          </div>
          <div class="col-lg-6 col-xl-5 ml-xl-auto">

            <div class="special_description_img" style="margin-top:-20px;">
              <img src="http://aplikasiperizinan.com/asset/siice/img/scr-img/mockup-ttd-digital.png" width="550px" alt="SK Perizinan">
            </div>

          </div>
        </div>
      </div>
    </div>


    <div class="special_description_area mt-150">
      <div class="container">
        <div class="row">
         <div class="col-lg-6 col-xl-5 ml-xl-auto">

          <div class="special_description_img" style="margin-top:100px;">
           <iframe width="420" height="300"
           src="https://www.youtube.com/embed/R-1MT2lf9hQ">
         </iframe>
       </div>

     </div>
     <div class="col-lg-6">
      <div class="special_description_content">



        <h2 class="tittle">Apa Itu <span>Tanda Tangan Digital</span> ?</h2>
        <span class="tittle-line"></span>
        <p>Secara garis besar Tanda Tangan Digital adalah sebuah skema matematis yang memiliki keunikan dalam mengidentifikasikan seorang (subjek hukum) di dunia digital.</p> 

        <p>Canggihnya, skema tersebut mampu membuktikan validitas dari tanda tangan tersebut secara online (real time).</p>

        <p>CA adalah lembaga yang menerbitkan sertifikat digital, menandatangani sertifikat untuk memverifikasi validitasnya dan melacak sertifikat yang telah dicabut atau kedaluwarsa.</p>
        <p>Tanda Tangan Digital digunakan untuk memberikan kekuatan hukum dan akibat hukum yang sah pada dokumen elektronik dan transaksi elektronik. Seperti yang tercantum pada pasal 11 UU ITE.</p>
        <p>Tanda Tangan Digital dapat menandatangani dokumen PDF dengan menggunakan Adobe Reader DC (free). Sehingga kita dapat membuat dokumen legal digital, tanpa harus menggunakan kertas lagi. (Download panduan disini).</p>
        <p>TTD juga dapat digunakan untuk login dan bertransaksi pada aplikasi (eGovernment, eBanking, eCommerce, dan eServices lainnya). Sayangnya belum ada aplikasi yang siap menggunakan sertifikat digital ini. (Lihat video demo cara menggunakan sertifikat pada layanan eBanking, disini). Aplikasi yang dapat menggunakan sertifikat digital sedang dalam proses pembuatan oleh layanan publik. Diharapkan pada tahun 2017 segera digunakan.</p>



      </div>

    </div>

     


        <div class="special_description_area mt-250"  style="margin-top:100px;">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="special_description_content">
              <h2>Bagaimana cara memverifikasi Digital Signature</h2>

              <p>Berikut kami sertakan tutorial verifikasi digital signature SK perizinan DPMPSTP Kab. Bogor.
              </p>

            </div>

          </div>
          <div class="col-lg-6 col-xl-5 ml-xl-auto mt-250"  style="margin-top:100px;">

             <iframe width="420" height="300"
           src="https://www.youtube.com/embed/R-1MT2lf9hQ" >
         </iframe>

          </div>
        </div>
      </div>
    </div>


  </div>
</div>
</div>





</div>
</div>



<!-- End Content -->
<footer>
  <?php $this->load->view('blog/src/footer');?>
</footer>
</div>
</body>
</html>