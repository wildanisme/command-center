
  <?php $this->load->view('blog/src/header');?>
  


<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="images/foto.jpg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-page-header-inner">
          <div class="left-side">
            <div class="header-image"><a href="#"><img src="<?= (!empty($profil) && $profil->foto) ? base_url().'data/desa/'.$profil->foto : base_url().'data/logo/smd-mpp.png';?>" alt="" style="width: 200px"></a></div>
            <div class="header-details">
              <h3><?=$desa->desa;?></h3>
              <h5>Kecamatan Paseh, Kab. Bogor</h5>
              <ul>
                <li><a href="single-company-profile.html"><i class="icon-material-outline-business"></i> <?= !empty($profil) ? $profil->alamat : '' ;?></a></li>
                
              </ul>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
  <div class="row">
    
    <!-- Content -->
    <div class="col-xl-8 col-lg-8 content-right-offset">

      <div class="single-page-section">
        <h3 class="margin-bottom-25">Visi</h3>
        <p><?= !empty($profil) ? $profil->visi :'-';?></p>
      </div>

      <div class="single-page-section">
        <h3 class="margin-bottom-25">Misi</h3>
        <p><?= !empty($profil) ? $profil->misi :'-';?></p>
      </div>

      <div class="single-page-section">
        <h3 class="margin-bottom-25">Program Kerja</h3>
        <p><?= !empty($profil) ? $profil->program_kerja :'-';?></p>
      </div>

       <div class="single-page-section">
        <h3 class="margin-bottom-25">Struktur Organisasi</h3>
        <?= !empty($profil->struktur_organisasi) ? '<img src="'.base_url().'data/desa/'.$profil->struktur_organisasi.'" />' :'-';?>
      </div>

      <div class="single-page-section">
        <h3 class="margin-bottom-30">Alamat</h3>
        <div id="single-job-map-container">
          <div id="singleListingMap" data-latitude="<?= !empty($profil->latitude) ? $profil->latitude:'' ;?>" data-longitude="<?= !empty($profil->longitude) ? $profil->longitude : '' ;?>" ></div>
          <a href="#" id="streetView">Street View</a>
        </div>
      </div>

      <div class="single-page-section">
        <h3 class="margin-bottom-25">Desa Lainya</h3>

        <!-- Listings Container -->
        <div class="listings-container grid-layout">

            <?php foreach($desa_lainya as $row){?>
            <a href="<?=base_url().'desa/detail/'.$row->id_desa;?>" class="job-listing">

              <!-- Job Listing Details -->
              <div class="job-listing-details">
                <!-- Logo -->
                <div class="job-listing-company-logo">
                  <img src="<?= ($row->foto) ? base_url().'data/desa/'.$row->foto : '' ;?>" alt="">
                </div>

                <!-- Details -->
                <div class="job-listing-description">
                  <h3 class="job-listing-title"><?= $row->desa ;?></h3>
                  <h4 class="job-listing-company">Kec. Paseh, Kab. Bogor</h4>
                  
                </div>
              </div>

              <!-- Job Listing Footer -->
              <div class="job-listing-footer">
                <ul>
                  <li><i class="icon-material-outline-location-on"></i> <?=$row->alamat;?></li>
                  
                </ul>
              </div>
            </a>

            <?php } ?>
            
          </div>
          <!-- Listings Container / End -->

        </div>
    </div>
    

    <!-- Sidebar -->
    <div class="col-xl-4 col-lg-4">
      <div class="sidebar-container">

       
        <!-- Sidebar Widget -->
        <div class="sidebar-widget">
          <div class="job-overview">
            <div class="job-overview-headline">Keterangan</div>
            <div class="job-overview-inner">
              <ul>
                <li>
                  <i class="icon-material-outline-location-on"></i>
                  <span>Alamat</span>
                  <h5><?= !empty($profil) ? $profil->alamat : '-' ;?></h5>
                </li>
                <li>
                  <i class="icon-material-outline-account-circle"></i>
                  <span>Kepala Desa</span>
                  <h5><?= !empty($profil) ? $profil->kepala_desa : '-' ;?></h5>
                </li>
                <li>
                  <i class="icon-material-outline-local-atm"></i>
                  <span>Anggaran Desa (Tahun Terakhir)</span>
                  <h5><?= !empty($profil) ? 'Rp.' . number_format($profil->anggaran) : '-' ;?></h5>
                </li>
                <li>
                  <i class="icon-material-outline-book"></i>
                  <span>Kontak</span>
                  <h5><?= !empty($profil) ? $profil->telp : '-' ;?></h5>
                </li>
              </ul>
            </div>
          </div>
        </div>

        
      </div>
    </div>

  </div>
</div>


<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->

<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>