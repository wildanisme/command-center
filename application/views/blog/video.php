
<style type="text/css">
.centered {
    position: absolute;
    top: 48%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 99;
}
.centered img{
  width: 150px;

}
</style>






<!-- Content
	================================================== -->
	<div id="titlebar" class="white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Video </h2>
					<span>Kab. Bogor</span>

					<!-- Breadcrumbs -->
					<nav id="breadcrumbs" class="">
						<ul>
							<li><a href="#">Home</a></li>
							<li>Video</li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>



	<!-- Recent Blog Posts -->
	<div class="section white padding-top-0 padding-bottom-60 ">
		<div class="container">


			<!-- Search Bar -->
			<div class="row">
				<div class="col-md-12">
					<div class="intro-banner-search-form margin-bottom-60">

				
						<!-- Search Field -->
						<div class="intro-search-field">

							<input name="s" type="text" placeholder="Masukan Judul atau Nama Video" value="<?php if (!empty($search)) echo $search;?>"/>
						</div>

						<!-- Search Field -->
						<div class="intro-search-field">
							<select class="selectpicker" multiple title="All Categories" >
								<option value="">Semua Kategori</option>
								<?php
								foreach ($category as $row) {
									$selected = "";
									if ($row->category_video_id==$this->input->get('c')) {
										$selected = "selected";
									}
									echo "<option value='{$row->category_video_id}' {$selected}>{$row->category_video_name}</option>";
								}
								?>
							</select>
						</div>

						<!-- Button -->
						<div class="intro-search-button">
							<button class="button ripple-effect" onclick="window.location.href='#'">Cari</button>
						</div>

					</div>
				</div>
			</div>


			<div class="row">

				<?php foreach ($video as $vd){
					parse_str( parse_url( $vd->link, PHP_URL_QUERY ), $code );
					$url_id =  $code['v']; 
					?>



					<div class="col-xs-12 col-md-4">

						<a href="<?php echo site_url('video/detail/'.$vd->id_video.'') ?>" class="blog-compact-item-container">
							<div class="blog-compact-item">
								<img src="https://img.youtube.com/vi/<?php echo $url_id ?>/0.jpg" alt="">
								<div class="centered"><img src="<?php echo base_url()."data/icon/play.png" ?>"></div>
								<span class="blog-item-tag"><?php echo $vd->category_video_name ?></span>
								<div class="blog-compact-item-content">

									<h3><?php echo $vd->judul ?></h3>

								</div>
							</div>
						</a>
					</div>
				<?php } ?>








			</div>



			<!-- Pagination -->
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<!-- Pagination -->
					<div class="pagination-container margin-top-10 margin-bottom-20">
						<nav class="pagination">
							<ul>
								<?php
								$CI =& get_instance();
								$CI->load->library('pagination');

								$this->config->load('bootstrap_pagination');
								$config = $this->config->item('pagination');  

								$config['base_url'] = base_url(). 'video';
								$config['total_rows'] = $total_rows;
								$config['per_page'] = $per_page;
								$config['page_query_string']=TRUE;

								$config['reuse_query_string'] = TRUE;

								$CI->pagination->initialize($config);
								$link = $CI->pagination->create_links();
								echo $link;
								?>

							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- Pagination / End -->









		</div>
	</div>
	<!-- Recent Blog Posts / End -->


	<!-- Section -->

	<!-- Section / End -->


</div>
</div>
</div>
<div class="clearfix"></div>
<!-- Pagination / End -->
