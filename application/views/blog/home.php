

<?php $this->load->view('blog/src/header');?>
<section class="discover">
    <div class="container">

        <p class="discover__description">
            Mari Berkembang, Berusaha Bersama.
        </p>

        <h1 class="discover__title places-tab">
            Cari UMKM di Indonesia.
        </h1>

        <!-- by default discover__title events tab is hidden -->
        <h1 class="discover__title events-tab">
            Cari Agenda Terkait UMKM.
        </h1>

        <ul class="discover__list">
            <li class="discover__list-item">
                <a class="place active-list" href="#"><i class="la la-compass" aria-hidden="true"></i>UMKM</a>
            </li>
            <li class="discover__list-item">
                <a class="events" href="#"><i class="la la-calendar" aria-hidden="true"></i>Agenda</a>
            </li>
        </ul>

        <form class="discover__form">
            <input class="discover__form-input" type="text" name="place-event" id="place-event" placeholder="Nama UMKM ?" />
            <select class="discover__form-input js-example-basic-multiple">
                <option>Kota? </option>
                <option>Jakarta</option>
                <option>Bandung</option>
                <option>Bogor</option>
                <option>Pangandaran</option>
                <option>Ciamis</option>
                <option>Semarang</option>
                <option>Jogja</option>
                <option>Solo</option>
            </select>
            <select class="discover__form-input js-example-basic-multiple">
                <option>Kategori: </option>
                <option>Art's</option>
                <option>Health</option>
                <option>Hotels</option>
                <option>Real Estate</option>
                <option>Rentals</option>
                <option>Restaurant</option>
                <option>Shopping</option>
                <option>Travel</option>
                <option>Vacation</option>
            </select>
            <button class="btn-default btn-default-red" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Cari</button>
        </form>

        <p class="discover__hashtags">
            <span>Kategori:</span> 
            <a href="explore-category.html">#restaurant,</a>
            <a href="explore-category.html">#hotels,</a>
            <a href="explore-category.html">#vacation,</a>
            <a href="explore-category.html">#rentals,</a>
            <a href="explore-category.html">#nightlive,</a>
            <a href="explore-category.html">#shopping,</a>
            <a href="explore-category.html">... </a>
        </p>

    </div>
</section>
<!-- End discover-module section -->

        <!-- top-experience-block
            ================================================== -->
            <section class="top-experience">
                <div class="container">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title">
                            Daftar Event / Agenda Kegiatan
                        </h1>
                        <p class="section-header__description">
                            Cari daftar kegiatan terkait pengembangan UMKM
                        </p>
                    </div>
                    <!-- end section-header module -->


                    <div class="top-experience__box owl-wrapper">
                        <div class="owl-carousel" data-num="3">

                            <div class="item">

                                <!-- place-gal module -->
                                <div class="place-gal">
                                    <img class="place-gal__image" src="<?php echo base_url().'data/agenda/';?>event1.jpg" alt="place-image">
                                    <div class="place-gal__content">
                                        <h2 class="place-gal__title">
                                            <a href="#">

                                                Event 1
                                            </a>
                                        </h2>
                                        <ul class="place-gal__list">

                                            <li class="place-gal__list-item">
                                                <a href="#">22 Agustus 2020</a>
                                            </li>
                                        </ul>
                                        <a class="btn-default" href="#">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            Lihat
                                        </a>
                                    </div>
                                </div>
                                <!-- end place-gal module -->

                            </div>

                            <div class="item">

                                <!-- place-gal module -->
                                <div class="place-gal">
                                    <img class="place-gal__image" src="<?php echo base_url().'data/agenda/';?>event2.jpg" alt="place-image">
                                    <div class="place-gal__content">
                                        <h2 class="place-gal__title">
                                            <a href="#">

                                                Event 2
                                            </a>
                                        </h2>
                                        <ul class="place-gal__list">

                                            <li class="place-gal__list-item">
                                                <a href="#">22 Agustus 2020</a>
                                            </li>
                                        </ul>
                                        <a class="btn-default" href="#">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            Lihat
                                        </a>
                                    </div>
                                </div>
                                <!-- end place-gal module -->

                            </div>

                            <div class="item">

                                <!-- place-gal module -->
                                <div class="place-gal">
                                    <img class="place-gal__image" src="<?php echo base_url().'data/agenda/';?>event3.jpg" alt="place-image">
                                    <div class="place-gal__content">
                                        <h2 class="place-gal__title">
                                            <a href="#">

                                                Event 3
                                            </a>
                                        </h2>
                                        <ul class="place-gal__list">

                                            <li class="place-gal__list-item">
                                                <a href="#">22 Agustus 2020</a>
                                            </li>
                                        </ul>
                                        <a class="btn-default" href="#">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            Lihat
                                        </a>
                                    </div>
                                </div>
                                <!-- end place-gal module -->

                            </div>

                            <div class="item">

                                <!-- place-gal module -->
                                <div class="place-gal">
                                    <img class="place-gal__image" src="<?php echo base_url().'data/agenda/';?>event5.jpg" alt="place-image">
                                    <div class="place-gal__content">
                                        <h2 class="place-gal__title">
                                            <a href="#">

                                                Jawa Timur
                                            </a>
                                        </h2>
                                        <ul class="place-gal__list">
                                            <li class="place-gal__list-item">
                                                <a href="#">4 Kota / Kab</a>
                                            </li>
                                            <li class="place-gal__list-item">
                                                <a href="#">603 Data</a>
                                            </li>
                                        </ul>
                                        <a class="btn-default" href="#">
                                            <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            Lihat
                                        </a>
                                    </div>
                                </div>
                                <!-- end place-gal module -->

                            </div>

                        </div>
                    </div>

                </div>
            </section>
            <!-- End top-experience-block -->

        <!-- services-block
            ================================================== -->
            <section class="services">
                <div class="container">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title">
                            Cari Berdasarkan Kategori
                        </h1>
                        <p class="section-header__description">
                            Daftar UMKM sesuai dengan klasifikasi Kategori jenis usaha
                        </p>
                    </div>
                    <!-- end section-header module -->


                    <div class="services__box">
                        <div class="row">

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-futbol-o" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Makanan
                                        </h2>
                                        <p class="services-post__location">
                                            12 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-hotel" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Pengerajin
                                        </h2>
                                        <p class="services-post__location">
                                            16 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-cutlery" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Fashion
                                        </h2>
                                        <p class="services-post__location">
                                            22 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-shopping-cart" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Retail
                                        </h2>
                                        <p class="services-post__location">
                                            30 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-ticket" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Seni Budaya
                                        </h2>
                                        <p class="services-post__location">
                                            8 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                            <div class="col-xl-2 col-md-4 col-sm-6">

                                <!-- services-post module -->
                                <a href="#" class="services-post">
                                    <div class="services-post__content">
                                        <i class="la la-glass" aria-hidden="true"></i>
                                        <h2 class="services-post__title">
                                            Jasa
                                        </h2>
                                        <p class="services-post__location">
                                            19 UMKM
                                        </p>
                                    </div>
                                </a>
                                <!-- end services-post module -->

                            </div>

                        </div>
                    </div>

                </div>
            </section>
            <!-- End services-block -->

        <!-- trending-places-block
            ================================================== -->
            <section class="trending-places">
                <div class="container">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title">
                            Daftar UMKM Terbaru
                        </h1>
                        <p class="section-header__description">
                            menampilkan daftar UMKM yang baru bergabung dengan komunitas kami.
                        </p>
                    </div>
                    <!-- end section-header module -->


                    <div class="trending-places__box owl-wrapper">
                        <div class="owl-carousel" data-num="5">

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/1.jpg" alt="place-image">
                                        <span class="place-post__rating">9.3</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="open">Open</span> until 8:00 PM
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Fifteen Restaurant & Cheese bar</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Restaurant
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                54 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            1149 3rd St (Wilshire), Santa Monica
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/2.jpg" alt="place-image">
                                        <span class="place-post__rating">9.0</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="closed">Closed</span> until 11:00 AM
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Frankies 457 Spuntino</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Cafe
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                23 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            457 Court St, Brooklyn
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/3.jpg" alt="place-image">
                                        <span class="place-post__rating average-rat">8.5</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="open">Open</span> 24/7
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Hotel Van Gogh</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Hostel
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                48 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Van de Veldestraat 5, Amsterdam
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/4.jpg" alt="place-image">
                                        <span class="place-post__rating">9.1</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="open">Likely Open</span>
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Caffè Pascucci</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Cafe
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                58 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            Piazza della Repubblica, Florence
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/5.jpg" alt="place-image">
                                        <span class="place-post__rating solid-rat">6.2</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="open">Open</span> 24/7
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Beautiful City Hostel & Hotel</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Hotel
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                11 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            12 rue de l'atlas, Paris
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                            <div class="item">

                                <!-- place-post module -->
                                <div class="place-post">
                                    <div class="place-post__gal-box">
                                        <img class="place-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/1.jpg" alt="place-image">
                                        <span class="place-post__rating">9.3</span>
                                        <a class="place-post__like" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="place-post__content">
                                        <p class="place-post__info">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span class="open">Open</span> until 8:00 PM
                                        </p>
                                        <h2 class="place-post__title">
                                            <a href="listing-detail-large.html">Fifteen Restaurant & Cheese bar</a>
                                        </h2>
                                        <p class="place-post__description">
                                            Restaurant
                                            <span>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd red-col" aria-hidden="true"></i>
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </span>
                                            <span class="place-post__description-review">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                54 Reviews
                                            </span>
                                        </p>
                                        <p class="place-post__address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            1149 3rd St (Wilshire), Santa Monica
                                        </p>
                                    </div>
                                </div>
                                <!-- end place-post module -->

                            </div>

                        </div>
                    </div>
                    <div class="center-button">
                        <a class="text-btn" href="#">
                            Lihat Semua UMKM <span>(482)</span>
                        </a>
                    </div>
                </div>
            </section>
            <!-- End trending-places-block -->


        <!-- banner-block
            ================================================== -->
            <section class="banner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">

                            <!-- section-header module -->
                            <div class="section-header">
                                <h1 class="section-header__title white-style">
                                    Bergabung dengan komunitas Kami ?
                                </h1>
                                <p class="section-header__description white-style">
                                    Mari bergabung dengan komunitas kami untuk mendapatkan banyak manfaat keanggotaan secara gratis. 
                                </p>
                            </div>
                            <!-- end section-header module -->

                        </div>
                        <div class="col-md-4">
                            <a class="btn-default" href="#">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                Dafta Sekarang
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End banner-block -->

        <!-- how-it-work-block
            ================================================== -->
            <section class="how-it-work">
                <div class="container">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title white-style">
                            Apa Manfaat Ketika Berbagung dengan kami ?
                        </h1>
                        <p class="section-header__description white-style">
                            Explore some of the best tips from around the world.
                        </p>
                    </div>
                    <!-- end section-header module -->

                    <div class="how-it-work__box">
                        <div class="row">

                            <div class="col-lg-4 col-md-6">

                                <!-- how-work-post module -->
                                <div class="how-work-post">

                                    <span class="how-work-post__icon">
                                        <i class="la la-compass" aria-hidden="true"></i>
                                    </span>

                                    <h2 class="how-work-post__title how-work-post__title-white">
                                        Akses pasar
                                    </h2>

                                    <p class="how-work-post__description how-work-post__description-white">
                                        mendapatkan kesempatan untuk mempromosikan setiap usaha yang ada didalam komunitas dengan jangkauan yang lebih luas 
                                    </p>

                                </div>
                                <!-- end how-work-post module -->

                            </div>

                            <div class="col-lg-4 col-md-6">

                                <!-- how-work-post module -->
                                <div class="how-work-post">

                                    <span class="how-work-post__icon">
                                        <i class="la la-binoculars" aria-hidden="true"></i>
                                    </span>

                                    <h2 class="how-work-post__title how-work-post__title-white">
                                        Mentoring dan Pelatihan Usaha
                                    </h2>

                                    <p class="how-work-post__description how-work-post__description-white">
                                        Terdapat banyak para mentor yang berpengalaman dan profesional dibidangnya yang siap melakukan pelatihan dan mentoring usaha
                                    </p>

                                </div>
                                <!-- end how-work-post module -->

                            </div>

                            <div class="col-lg-4 col-md-6">

                                <!-- how-work-post module -->
                                <div class="how-work-post">

                                    <span class="how-work-post__icon">
                                        <i class="la la-plus-circle" aria-hidden="true"></i>
                                    </span>

                                    <h2 class="how-work-post__title how-work-post__title-white">
                                     Akses Permodalan
                                 </h2>

                                 <p class="how-work-post__description how-work-post__description-white">
                                    Terdapat banyak kesempatan untuk menemukan akses permodalan usaha untuk mengembangkan usahamu. 
                                </p>

                            </div>
                            <!-- end how-work-post module -->

                        </div>

                    </div>
                </div>
                
            </div>
        </section>
        <!-- End how-it-work-block -->

        <!-- statistic-block
            ================================================== -->
            <section class="statistic">
                <div class="container">

                    <div class="statistic__box">
                        <div class="row">

                            <div class="col-lg-3 col-sm-6">

                                <!-- statistic-post module -->
                                <div class="statistic-post">

                                    <span class="statistic-post__icon">
                                        <i class="la la-fire" aria-hidden="true"></i>
                                    </span>

                                    <div class="statistic-post__content">
                                        <h1 class="statistic-post__title">
                                            <span class="timer" data-from="0" data-to="100">100</span> +
                                        </h1>
                                        <p class="statistic-post__description">
                                            UMKM Tergabung
                                        </p>
                                    </div>

                                </div>
                                <!-- end statistic-post module -->

                            </div>

                            <div class="col-lg-3 col-sm-6">

                                <!-- statistic-post module -->
                                <div class="statistic-post">

                                    <span class="statistic-post__icon">
                                        <i class="la la-thumbs-up" aria-hidden="true"></i>
                                    </span>

                                    <div class="statistic-post__content">
                                        <h1 class="statistic-post__title">
                                            <span class="timer" data-from="0" data-to="9500">8293</span> +
                                        </h1>
                                        <p class="statistic-post__description">
                                            Mentor 
                                        </p>
                                    </div>

                                </div>
                                <!-- end statistic-post module -->

                            </div>

                            <div class="col-lg-3 col-sm-6">

                                <!-- statistic-post module -->
                                <div class="statistic-post">

                                    <span class="statistic-post__icon">
                                        <i class="la la-comments" aria-hidden="true"></i>
                                    </span>

                                    <div class="statistic-post__content">
                                        <h1 class="statistic-post__title">
                                            <span class="timer" data-from="0" data-to="25000">25000</span> +
                                        </h1>
                                        <p class="statistic-post__description">
                                            Agenda Kegiatan
                                        </p>
                                    </div>

                                </div>
                                <!-- end statistic-post module -->

                            </div>

                            <div class="col-lg-3 col-sm-6">

                                <!-- statistic-post module -->
                                <div class="statistic-post">

                                    <span class="statistic-post__icon">
                                        <i class="la la-street-view" aria-hidden="true"></i>
                                    </span>

                                    <div class="statistic-post__content">
                                        <h1 class="statistic-post__title">
                                            <span class="timer" data-from="0" data-to="5500">5500</span> +
                                        </h1>
                                        <p class="statistic-post__description">
                                            Partner
                                        </p>
                                    </div>

                                </div>

                                <!-- end statistic-post module -->

                            </div>

                        </div>
                    </div>

                </div>
            </section>

            <!-- End statistic-block -->
    <!-- team-block
        ================================================== -->
        <section class="team">
            <div class="container">

                <!-- section-header module -->
                <div class="section-header">
                    <h1 class="section-header__title">
                        Beberapa Mentor kami
                    </h1>
                    <p class="section-header__description">
                        Akan banyak sekali para mentor profesional dan berpengalaman dibidangnya yang siap membantu anda
                    </p>
                </div>
                <!-- end section-header module -->


                <div class="team__box">
                    <div class="row">

                        <div class="col-lg-4 col-md-6">

                            <!-- team-post module -->
                            <div class="team-post">

                                <div class="team-post__gal">
                                    <img src="<?=base_url();?>data/mentor/mentor1.jpg" alt="">
                                    <div class="team-post__gal-hover">
                                        <ul class="team-post__social">
                                            <li>
                                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="team-post__content">
                                    <h2 class="team-post__name">
                                        Mark Guetta
                                    </h2>
                                    <span class="team-post__role">
                                        Founder / Lead of marketing
                                    </span>
                                    <p class="team-post__description">
                                        Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien.
                                    </p>
                                </div>

                            </div>
                            <!-- end team-post module -->

                        </div>
                        
                        <div class="col-lg-4 col-md-6">

                            <!-- team-post module -->
                            <div class="team-post">

                                <div class="team-post__gal">
                                    <img src="<?=base_url();?>data/mentor/mentor2.jpg" alt="">
                                    <div class="team-post__gal-hover">
                                        <ul class="team-post__social">
                                            <li>
                                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="team-post__content">
                                    <h2 class="team-post__name">
                                        Mike McGregor
                                    </h2>
                                    <span class="team-post__role">
                                        Tech lead / Web developer
                                    </span>
                                    <p class="team-post__description">
                                        Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.
                                    </p>
                                </div>

                            </div>
                            <!-- end team-post module -->

                        </div>
                        
                        <div class="col-lg-4 col-md-6">

                            <!-- team-post module -->
                            <div class="team-post">

                                <div class="team-post__gal">
                                     <img src="<?=base_url();?>data/mentor/mentor3.jpg" alt="">
                                    <div class="team-post__gal-hover">
                                        <ul class="team-post__social">
                                            <li>
                                                <a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" class="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="team-post__content">
                                    <h2 class="team-post__name">
                                        Sandra Cardel
                                    </h2>
                                    <span class="team-post__role">
                                        Creative director / Web designer
                                    </span>
                                    <p class="team-post__description">
                                        Sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus
                                    </p>
                                </div>

                            </div>
                            <!-- end team-post module -->

                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End team-block -->
        <!-- testimonial-block
            ================================================== -->
            <section class="testimonial">
                <div class="container">

                    <!-- section-header module -->
                    <div class="section-header">
                        <h1 class="section-header__title">
                            Testimoni tentang komunitas 
                        </h1>
                        <p class="section-header__description">
                            Banyak sekali testimoni kebermanfaatan dari komunitas kami
                        </p>
                    </div>
                    <!-- end section-header module -->

                    <div class="testimonial__box owl-wrapper">
                        <div class="owl-carousel" data-num="3">

                            <div class="item">

                                <!-- testimonial-post module -->
                                <div class="testimonial-post">

                                    <div class="testimonial-post__content">

                                        <span class="testimonial-post__quote">"</span>

                                        <p class="testimonial-post__description">
                                            “ Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh, nullam mollis. Ut justo. Suspendisse potenti. ”
                                        </p>

                                    </div>

                                    <img class="testimonial-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/test1.jpg" alt="testimonial-image">

                                    <h2 class="testimonial-post__title">
                                        Michael Osborne
                                    </h2>

                                </div>
                                <!-- end testimonial-post module -->

                            </div>

                            <div class="item">

                                <!-- testimonial-post module -->
                                <div class="testimonial-post">

                                    <div class="testimonial-post__content">

                                        <span class="testimonial-post__quote">"</span>

                                        <p class="testimonial-post__description">
                                            “Ut justo suspendisse potenti. Sed vel lacus mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus.”
                                        </p>

                                    </div>

                                    <img class="testimonial-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/test2.jpg" alt="testimonial-image">

                                    <h2 class="testimonial-post__title">
                                        Lara Joy
                                    </h2>

                                </div>
                                <!-- end testimonial-post module -->

                            </div>

                            <div class="item">

                                <!-- testimonial-post module -->
                                <div class="testimonial-post">

                                    <div class="testimonial-post__content">

                                        <span class="testimonial-post__quote">"</span>

                                        <p class="testimonial-post__description">
                                            “Sed vel lacus mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. Suspendisse ac urna. Etiam pellentesque mauris ut lectus. ” 
                                        </p>

                                    </div>

                                    <img class="testimonial-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/test3.jpg" alt="testimonial-image">

                                    <h2 class="testimonial-post__title">
                                        John Smith
                                    </h2>

                                </div>
                                <!-- end testimonial-post module -->

                            </div>

                            <div class="item">

                                <!-- testimonial-post module -->
                                <div class="testimonial-post">

                                    <div class="testimonial-post__content">

                                        <span class="testimonial-post__quote">"</span>

                                        <p class="testimonial-post__description">
                                            “ Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh, nullam mollis. Ut justo. Suspendisse potenti. ”
                                        </p>

                                    </div>

                                    <img class="testimonial-post__image" src="<?php echo base_url().'asset/umkm/';?>upload/test1.jpg" alt="testimonial-image">

                                    <h2 class="testimonial-post__title">
                                        Michael Osborne
                                    </h2>

                                </div>
                                <!-- end testimonial-post module -->

                            </div>

                        </div>
                    </div>

                </div>
            </section>
            <!-- End testimonial-block -->
           
    <!-- Recent Blog Posts -->
    <div class="section padding-top-65 padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">


                   <!-- section-header module -->
                   <div class="section-header">
                    <h1 class="section-header__title">
                     Artikel
                 </h1>
                 <p class="section-header__description">
                    Banyak sekali testimoni kebermanfaatan dari komunitas kami
                </p>
            </div>
            <!-- end section-header module -->


            <div class="row">


                <div class="blog-page__box">

                    <div class="row">


                      <?php foreach($posts as $post){

                          $tag = "";
                          $tags= $post->tag;
                          if ($tags!=""){
                            $exp = explode(";", $tags);
                            $_tag = array();
                            foreach ($Qtag as $r) {
                              $_tag[$r->tag_name] = $r->tag_slug;
                          }

                          for ($x=0; $x < (count($exp) - 1) ; $x++)
                          {
                              $slug = $_tag[$exp[$x]];
                              $tag .= "<a href='".base_url()."berita/tag/{$slug}' >$exp[$x]</a>";
                              if ($x < (count($exp) - 2)) $tag .=",";
                          }
                      }
                      $content = substr($post->content,0,150);
                      if (strlen($post->content)>150) $content .="...";
                      ?>

                      <div class="col-md-4">

                        <!-- blog-post module -->
                        <div class="blog-post" style="padding: 10px">

                            <img src="<?php echo (!empty($post->picture)) ? base_url()."data/images/featured_image/{$post->picture}" : "data/logo/layanan.jpg";?>" alt="" class="blog-post__image" >

                            <ul class="blog-post__list">
                                <li class="blog-post__list-item">
                                    <i class="la la-calendar-check-o"></i>
                                    <span><?=tanggal($post->date)?></span>
                                </li>

                            </ul>

                            <h2 class="blog-post__title">
                                <a href="<?php echo base_url()."berita/read/{$post->title_slug}";?>">
                                    <?=$post->title?>
                                </a>
                            </h2>

                            <p class="blog-post__description">
                                <?=$content?>
                            </p>

                            <p class="blog-post__tags">
                                <i class="la la-tag"></i>
                                <a href="#"><?=$post->category_name?></a>
                            </p>

                        </div>
                        <!-- End blog-post module -->

                    </div>

                <?php } ?>

            </div>





        </div>






    </div>


</div>
</div>
</div>
</div>
<!-- Recent Blog Posts / End -->


 <section style="margin-top: 20;padding-top: 50px" 
            data-background-color="#ffffff" class="testimonial">
            <!-- Logo Carousel -->
            <div class="container ">

               <!-- section-header module -->
               <div class="section-header">
                <h1 class="section-header__title">
                    Daftar Partner
                </h1>
                <p class="section-header__description">
                    Kami telah bergabung dan di dukung oleh banyak partner
                </p>
            </div>
            <!-- end section-header module -->


            <div class="owl-wrapper">
                <div class="owl-carousel" data-num="4">




                    <?php foreach($banner as $b){
                      ?>
                      <div class="item">
                        <a href="<?=$b->url;?>" target="_blank" title="<?=$b->judul;?>"><img
                            src="<?php echo base_url().'data/images/banner/'.$b->gambar;?>"></a>
                        </div>

                        <?php
                    }
                    ?>







                </div>
            </div>
            <!-- Carousel / End -->

        </div>
        <!-- Logo Carousel / End -->
    </section>



        <!-- footer block module
            ================================================== -->
            <?php $this->load->view('blog/src/footer');?>

        </body>
        </html>


        <script>
            function search()
            {
                var s = $('#keyword').val();
                var modul = $('#kategori').val();
                window.location.href= modul+'?s='+s;
            }
        </script>
    </body>


    </html>
