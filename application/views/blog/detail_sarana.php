<?php $this->load->view('blog/src/header_x');?>


<div class="clearfix"></div>
<!-- Header Container / End -->


     <!-- Slider
     	================================================== -->
     	<div class="listing-slider mfp-gallery-container margin-bottom-0">
     		<?php foreach ($foto_sarana as $f ) {
     			?>
     			<a href="<?php echo base_url().'data/sarana/';?><?=$f->foto?>" data-background-image="<?php echo base_url().'data/sarana/';?><?=$f->foto?>" class="item mfp-gallery" title="Title 1"></a>
     			<?php
     		}
     		?>
     	</div>


<!-- Content
	================================================== -->
	<div class="container">
		<div class="row sticky-wrapper">
			<div class="col-lg-8 col-md-8 padding-right-30">

				<!-- Titlebar -->
				<div id="titlebar" class="listing-titlebar">
					<div class="listing-titlebar-title">
						<h2><?=$detail->nama_sarana;?> <span class="listing-tag"><?=$detail->nama_kategori_sarana;?></span></h2>
						<span>
							<a href="#listing-location" class="listing-address">
								<i class="fa fa-map-marker"></i>
								<?=$detail->alamat;?>
							</a>
						</span>

					</div>
				</div>

				<!-- Listing Nav -->
				<div id="listing-nav" class="listing-nav-container">
					<ul class="listing-nav">
						<li><a href="#listing-overview" class="active">Deskripsi</a></li>

						<li><a href="#listing-location">Lokasi</a></li>

					</ul>
				</div>

				<!-- Overview -->
				<div id="listing-overview" class="listing-section">

					<!-- Description -->

					<p>
						<?=$detail->deskripsi;?>  
					</p>




				</div>

				<!-- Food Menu -->


				<!-- Location -->
				<div id="listing-location" class="listing-section">
					<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Lokasi</h3>

					<div id="singleListingMap-container">
						<div id="singleListingMap" data-latitude="<?=$detail->latitude;?>" data-longitude="<?=$detail->longitude;?>" data-map-icon="im im-icon-Location-2"></div>
						<a href="#" id="streetView">Street View</a>
					</div>
				</div>
				


			</div>


		<!-- Sidebar
			================================================== -->
			<div class="col-lg-4 col-md-4 margin-top-75 sticky">


				<div class="widget margin-top-40">

				<h3>Sarana Lainya</h3>
				<ul class="widget-tabs">

					<?php foreach ($random as $random) {
					?>
					<li>
						<div class="widget-content">
								<div class="widget-thumb">
								<a href="<?php echo base_url();?>sarana/detail/<?=$random->id?>"><img src="<?php echo base_url().'data/sarana/';?><?=$random->foto?>" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="pages-blog-post.html"><?=$random->nama_sarana?></a></h5>
								<span><?=$random->nama_kategori_sarana?></span>
							</div>
							<div class="clearfix"></div>
						</div>
					</li>
					<?php }

						?>
					
					
				</ul>

			</div>
			<!-- Widget / End-->




			</div>
			<!-- Sidebar / End -->

		</div>
	</div>

	<?php $this->load->view('blog/src/footer_x');?>
	!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>