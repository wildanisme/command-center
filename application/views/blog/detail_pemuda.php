
  <?php $this->load->view('blog/src/header');?>
  





  <div class="clearfix"></div>
  <!-- Header Container / End -->


<!-- Titlebar
================================================== -->
<div class="single-page-header freelancer-header" data-background-image="images/single-freelancer.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image freelancer-avatar"><img src="<?php echo base_url().'data/pemuda/'.$detail->foto;?>" alt=""></div>
						<div class="header-details">
							<h3><?=$detail->nama_pemuda;?> <span><?=$detail->nama_kategori_pemuda;?></span></h3>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-12 col-lg-12 content-right-offset">
			
			<!-- Page Content -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">Deskripsi</h3>
				<?=$detail->deskripsi;?>  
			</div>

		
			
			

		</div>
		

		

	</div>
</div>

<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->

<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMvwWO0aCn876VcbHvGHtgzBNRm8HMVEc"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>