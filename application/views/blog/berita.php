  <?php $this->load->view('blog/src/header');?>
  


  <!-- page-title-module
      ================================================== -->
    <section class="page-title">
      <div class="container">
        <h1 class="page-title__title">News &amp; Blog</h1>
        <span class="page-title__description">News and stories from the team at TripTip</span>
      </div>
    </section>
    <!-- End page-title-module -->

    <!-- blog-page block
      ================================================== -->
    <section class="blog-page">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="blog-page__box">
              
              <div class="row">




             <?php
          foreach ($posts as $post) :
            $tag = "";
            $tags= $post->tag;
            if ($tags!=""){
              $exp = explode(";", $tags);
              $_tag = array();
              foreach ($Qtag as $r) {
                $_tag[$r->tag_name] = $r->tag_slug;
              }
              
              for ($x=0; $x < (count($exp) - 1) ; $x++)
              {
                $slug = $_tag[$exp[$x]];
                $tag .= "<a href='".base_url()."berita/tag/{$slug}' >$exp[$x]</a>";
                if ($x < (count($exp) - 2)) $tag .=",";
              }
            }
            $content = substr($post->content,0,255);
            if (strlen($post->content)>255) $content .="...";
        ?>

                <div class="col-md-6">
                  
                  <!-- blog-post module -->
                  <div class="blog-post">

                    <img class="blog-post__image" src="<?php echo (!empty($post->picture)) ? base_url()."data/images/featured_image/{$post->picture}" : "data/logo/layanan.jpg";?>" alt="">

                    <ul class="blog-post__list">
                      <li class="blog-post__list-item">
                        <i class="la la-calendar-check-o"></i>
                        <span><?php echo $post->date;?></span>
                      </li>
                   
                    </ul>

                    <h2 class="blog-post__title">
                      <a href="<?php echo base_url()."berita/read/{$post->title_slug}";?>">
                       <?php echo $post->title;?>
                      </a>
                    </h2>

                    <p class="blog-post__description">
                     <?php echo $content;?>
                    </p>

                    <p class="blog-post__tags">
                      <i class="la la-tag"></i>
                      <a href="#"><?php echo $post->category_name;?></a>
               
                    </p>

                  </div>
                  <!-- End blog-post module -->

                </div>
    <?php endforeach;?>
            

              </div>
              
         
      

            </div>

                    <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-10 margin-bottom-20">
              <nav class="pagination">
                <ul>
                   <?php
                $CI =& get_instance();
                            $CI->load->library('pagination');

                            $config['base_url'] = base_url(). 'berita/index/';
                            $config['total_rows'] = $total_rows;
                            $config['per_page'] = $per_page;
                            $config['attributes'] = array('class' => 'ripple-effect');
                            $config['page_query_string']=TRUE;
                            $CI->pagination->initialize($config);
                            $link = $CI->pagination->create_links();

                      
                            $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                            $link = str_replace("</strong>", "</a></li>", $link);
                            $link = str_replace("&lt;", "Back", $link);
                            $link = str_replace("&gt;", "Next", $link);
                            $link = str_replace("<a ;", "<li><a ", $link);
                             $link = str_replace("</a> ;", "<li></a> ", $link);
                            echo $link;
              ?>

                  
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <!-- Pagination / End -->


          </div>
          <div class="col-lg-4">

            <!-- sidebar -->
            <div class="sidebar">

              <div class="sidebar__widget sidebar__widget-category">
                <h2 class="sidebar__widget-title">
                  Categories
                </h2>
                <ul class="sidebar__category-list">
                  <li><a href="#">Coffee</a></li>
                  <li><a href="#">Travel</a></li>
                  <li><a href="#">Restaurant</a></li>
                  <li><a href="#">Art &amp; Culture</a></li>
                  <li><a href="#">Hotels</a></li>
                  <li><a href="#">Sport</a></li>
                  <li><a href="#">Uncategorized</a></li>
                </ul>
              </div>

              <div class="sidebar__widget sidebar__widget-tags">
                <h2 class="sidebar__widget-title">
                  Popular Tags
                </h2>
                <ul class="sidebar__tags-list">
                  <li><a href="#">Cafe</a></li>
                  <li><a href="#">Restaurant</a></li>
                  <li><a href="#">food</a></li>
                  <li><a href="#">Romantic</a></li>
                  <li><a href="#">Cozy</a></li>
                  <li><a href="#">Sea Food</a></li>
                  <li><a href="#">Healthy Food</a></li>
                  <li><a href="#">Pasta</a></li>
                </ul>
              </div>

              <div class="sidebar__widget sidebar__widget-instagram">
                <h2 class="sidebar__widget-title">
                  Instagram
                </h2>
                <ul class="sidebar__instagram-list">
                  <li><a href="#"><img src="upload/inst1.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst2.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst3.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst4.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst5.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst6.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst7.jpg" alt=""></a></li>
                  <li><a href="#"><img src="upload/inst8.jpg" alt=""></a></li>
                </ul>
              </div>

              <div class="sidebar__widget sidebar__widget-popular">
                <h2 class="sidebar__widget-title">
                  Popular Post
                </h2>
                <ul class="sidebar__popular-list">
                  <li>
                    <img src="upload/th1.jpg" alt="">
                    <div class="sidebar__popular-list-cont">
                      <h2 class="sidebar__popular-list-title">
                        <a href="single-post.html">Nunc dignissim risus id metus.</a>
                      </h2>
                      <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                    </div>
                  </li>
                  <li>
                    <img src="upload/th2.jpg" alt="">
                    <div class="sidebar__popular-list-cont">
                      <h2 class="sidebar__popular-list-title">
                        <a href="single-post.html">Nunc dignissim risus id metus.</a>
                      </h2>
                      <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                    </div>
                  </li>
                  <li>
                    <img src="upload/th3.jpg" alt="">
                    <div class="sidebar__popular-list-cont">
                      <h2 class="sidebar__popular-list-title">
                        <a href="single-post.html">Nunc dignissim risus id metus.</a>
                      </h2>
                      <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                    </div>
                  </li>
                  <li>
                    <img src="upload/th4.jpg" alt="">
                    <div class="sidebar__popular-list-cont">
                      <h2 class="sidebar__popular-list-title">
                        <a href="single-post.html">Nunc dignissim risus id metus.</a>
                      </h2>
                      <span class="sidebar__popular-list-desc">Oct 20, 2018, 0 Comments</span>
                    </div>
                  </li>
                </ul>
              </div>

              <div class="sidebar__widget sidebar__advertise">
                <span class="sidebar__advertise-title">
                  advertising box
                </span>
                <img src="upload/add.jpg" alt="">
              </div>

            </div>
            <!-- End sidebar -->

          </div>
        </div>
      </div>
    </section>
    <!-- End blog-page block -->





<!-- Section -->

<!-- Section / End -->


    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>