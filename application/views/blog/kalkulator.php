<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->


<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>Daftar Perizinan</h2><span>yang dilayani oleh DPMPTP Kabupaten Bogor</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="#">Home</a></li>
            <li>Perizinan</li>
            <li>Kalkulator</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>


<!-- Content
================================================== -->
<div class="container">
  <div class="row">
<!-- Sidebar
    ================================================== -->
    <div class="col-lg-12 col-md-12">
      <?php $this->load->view('blog/kalkulator_bak');?>
    </div>

    
  </div>
</div>
    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>