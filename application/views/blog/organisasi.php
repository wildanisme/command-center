  <?php $this->load->view('blog/src/header');?>
  
  
  <div class="clearfix"></div>
  <!-- Header Container / End -->

 

<!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h2>Organisasi </h2><span>Kab. Bogor</span>

          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="#">Home</a></li>
              <li>Organisasi</li>
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>

<!-- Page Content

<!-- Page Content
================================================== -->
<div class="container">
  <div class="row">
  
    <div class="col-xl-12">
      <div class="companies-list">

        <?php foreach ($organisasi as $a ) {
 ?>
        <a href="<?php echo base_url();?>organisasi/detail/<?=$a->id_organisasi?>" class="company">
          <div class="company-inner-alignment">
            <span class="company-logo"><img src="<?php echo base_url();?>data/organisasi/<?=$a->foto?>" alt=""></span>
            <h4><?=$a->nama_organisasi?></h4>
            <span class="company-not-rated"><?=$a->nama_kategori_organisasi?></span>

          </div>
        </a>
       <?php }
?>

       
     
      </div>
    </div>
  </div>
</div>


      </div>
      <!-- Freelancers Container / End -->


      <!-- Pagination -->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- Pagination -->
         <!-- Pagination -->
            <div class="pagination-container margin-top-15 margin-bottom-40">
              <nav class="pagination">
                <ul>
                <?php
                $CI =& get_instance();
                            $CI->load->library('pagination');

                            $config['base_url'] = base_url(). 'organisasi/index/';
                            $config['total_rows'] = $total_rows;
                            $config['per_page'] = $per_page;
                            $config['attributes'] = array('class' => 'ripple-effect');
                            $config['page_query_string']=TRUE;
                            $CI->pagination->initialize($config);
                            $link = $CI->pagination->create_links();

                      
                            $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                            $link = str_replace("</strong>", "</a></li>", $link);
                            $link = str_replace("&lt;", "Back", $link);
                            $link = str_replace("&gt;", "Next", $link);
                            $link = str_replace("<a ;", "<li><a ", $link);
                             $link = str_replace("</a> ;", "<li></a> ", $link);
                            echo $link;
              ?>


                
                </ul>
              </nav>
            </div>
        </div>
      </div>
      <!-- Pagination / End -->

    </div>
  </div>
</div>


<?php $this->load->view('blog/src/footer');?>


</body>
</html>