<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->


<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h3>Panduan</h3><span>Perizinan</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="#">Home</a></li>
            <li>Panduan</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>


<!-- Content
================================================== -->
<div class="container">
  <div class="row">

<!-- Info Section -->
<div class="container">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h3 class="headline centered margin-top-20">
       Bagaimana cara membuat perizinan ?
      </h3>
    </div>
  </div>

  <div class="row">
  <div class="col-md-6">
      <p><iframe src="http://www.youtube.com/embed/4KLcR8eOFzg" width="525" height="350"></iframe></p>                  
  </div>    

<div class="col-md-6" style="padding-top: 50px;">
  Pendaftaran semua perizinan kini dapat dilakukan dalam satu portal <a href="http://osss.go.id">oss.go.id</a> 
  <br>
  Di portal tersebut pemohon akan mendapatkan NIB (Nomer Izin Berusaha) sebagai salah satu absahan dalam berusaha.
  Peraturan tersebut diberlakukan pada tahun ini. Jadi dimulai tahun ini, setiap pengusaha akan memiliki Nomor Induk Berusaha (NIB). Nomor Induk Berusaha (NIB) berfungsi juga sebagai Tanda Daftar Perusahaan (TDP), Angka Pengenal Importir (API), dan akses kepabeanan.

Selain itu, NIB juga berfungsi untuk mengurus perizinan usaha tanpa harus membawa berkas-berkas persyaratan yang banyak seperti sebelumnya.
<br>
<br>
<br>
<a href="http://oss.go.id" class="button">Daftar OSS</a>
 </div>
  </div>

   <div class="row">
  <div class="col-md-6">
    <h4 class="listing-desc-headline">4 Kategori Perizinan Berusaha/Investasi</h4>
         <ul class="listing-features checkboxes margin-top-0">
          <li>Pendaftaran dan Perizinan Dasar</li>
          <li>Pendaftaran dan Perizinan Dasar</li>
        

        </ul>

  </div>    

<div class="col-md-6" style="padding-top: 50px;">
   <p><iframe src="http://www.youtube.com/embed/4KLcR8eOFzg" width="525" height="350"></iframe></p>         
 </div>
  </div>





  





</div>
<!-- Info Section / End -->






</div>
</div>




    <!-- Popper js -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/popper.min.js"></script>


    <!-- Jquery-2.2.4 JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/jquery-2.2.4.min.js"></script>
     <!-- Bootstrap-4 Beta JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/bootstrap.min.js"></script>

    <!-- All Plugins JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?php echo base_url().'asset/list/';?>scripts/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/active.js"></script>




    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>