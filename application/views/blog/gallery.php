<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">GAllery</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href="# ">Gallery</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="row blog-page">
          <div class="col-md-12 blog-box">

            <div class="row">

              <div class="recent-projects">
                <h4 class="title"><span>Album</span></h4>
              </div>
              <?php
                foreach ($album as $row) {
                  echo "
                    <div class='col-md-4'>
            
                      <div class='portfolio-item item'>
                        <div class='portfolio-border'>
                         
                          <div class='portfolio-thumb'>
                            <a href='".base_url()."gallery/album/$row->album_id'>
                              <div class='thumb-overlay'></div>
                              <img alt='$row->description' src='".base_url()."data/images/album/$row->picture'>
                            </a>
                          </div>
                        
                          <div class='portfolio-details'>
                            <a href='".base_url()."gallery/album/$row->album_id'>
                              <h4>$row->album_title</h4>
                              
                            </a>
                            <a href='".base_url()."gallery/album/$row->album_id' class='like-link'></a>
                          </div>
                      
                        </div>
                      </div>
                    </div>
                  ";
                }
              ?>
            </div>        
          </div>
        </div>
      </div>
    </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>