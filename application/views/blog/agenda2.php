  <?php $this->load->view('blog/src/header');?>
  
  <style type="text/css">

    a{text-decoration:none}
    h4{text-align:center;margin:30px 0;color:#444}
    .main-timeline{position:relative}
    .main-timeline:before{content:"";width:5px;height:100%;border-radius:20px;margin:0 auto;background:#242922;position:absolute;top:0;left:0;right:0}
    .main-timeline .timeline{display:inline-block;margin-bottom:50px;position:relative}
    .main-timeline .timeline:before{content:"";width:20px;height:20px;border-radius:50%;border:4px solid #fff;background:#ec496e;position:absolute;top:50%;left:50%;z-index:1;transform:translate(-50%,-50%)}
    .main-timeline .timeline-icon{display:inline-block;width:130px;height:130px;border-radius:50%;border:3px solid #ec496e;padding:13px;text-align:center;position:absolute;top:50%;left:30%;transform:translateY(-50%)}
    .main-timeline .timeline-icon i{display:block;border-radius:50%;background:#ec496e;font-size:64px;color:#fff;line-height:100px;z-index:1;position:relative}
    .main-timeline .timeline-icon:after,.main-timeline .timeline-icon:before{content:"";width:100px;height:4px;background:#ec496e;position:absolute;top:50%;right:-100px;transform:translateY(-50%)}
    .main-timeline .timeline-icon:after{width:70px;height:50px;background:#fff;top:89px;right:-30px}
    .main-timeline .timeline-content{width:50%;padding:0 50px;margin:52px 0 0;float:right;position:relative}
    .main-timeline .timeline-content:before{content:"";width:70%;height:100%;border:3px solid #ec496e;border-top:none;border-right:none;position:absolute;bottom:-13px;left:35px}
    .main-timeline .timeline-content:after{content:"";width:37px;height:3px;background:#ec496e;position:absolute;top:13px;left:0}
    .main-timeline .title{font-size:20px;font-weight:600;color:#ec496e;text-transform:uppercase;margin:0 0 5px}
    .main-timeline .description{display:inline-block;font-size:16px;color:#404040;line-height:20px;letter-spacing:1px;margin:0}
    .main-timeline .timeline:nth-child(even) .timeline-icon{left:auto;right:30%}
    .main-timeline .timeline:nth-child(even) .timeline-icon:before{right:auto;left:-100px}
    .main-timeline .timeline:nth-child(even) .timeline-icon:after{right:auto;left:-30px}
    .main-timeline .timeline:nth-child(even) .timeline-content{float:left}
    .main-timeline .timeline:nth-child(even) .timeline-content:before{left:auto;right:35px;transform:rotateY(180deg)}
    .main-timeline .timeline:nth-child(even) .timeline-content:after{left:auto;right:0}
    .main-timeline .timeline:nth-child(2n) .timeline-content:after,.main-timeline .timeline:nth-child(2n) .timeline-icon i,.main-timeline .timeline:nth-child(2n) .timeline-icon:before,.main-timeline .timeline:nth-child(2n):before{background:#f9850f}
    .main-timeline .timeline:nth-child(2n) .timeline-icon{border-color:#f9850f}
    .main-timeline .timeline:nth-child(2n) .title{color:#f9850f}
    .main-timeline .timeline:nth-child(2n) .timeline-content:before{border-left-color:#f9850f;border-bottom-color:#f9850f}
    .main-timeline .timeline:nth-child(3n) .timeline-content:after,.main-timeline .timeline:nth-child(3n) .timeline-icon i,.main-timeline .timeline:nth-child(3n) .timeline-icon:before,.main-timeline .timeline:nth-child(3n):before{background:#8fb800}
    .main-timeline .timeline:nth-child(3n) .timeline-icon{border-color:#8fb800}
    .main-timeline .timeline:nth-child(3n) .title{color:#8fb800}
    .main-timeline .timeline:nth-child(3n) .timeline-content:before{border-left-color:#8fb800;border-bottom-color:#8fb800}
    .main-timeline .timeline:nth-child(4n) .timeline-content:after,.main-timeline .timeline:nth-child(4n) .timeline-icon i,.main-timeline .timeline:nth-child(4n) .timeline-icon:before,.main-timeline .timeline:nth-child(4n):before{background:#2fcea5}
    .main-timeline .timeline:nth-child(4n) .timeline-icon{border-color:#2fcea5}
    .main-timeline .timeline:nth-child(4n) .title{color:#2fcea5}
    .main-timeline .timeline:nth-child(4n) .timeline-content:before{border-left-color:#2fcea5;border-bottom-color:#2fcea5}
    @media only screen and (max-width:1200px){.main-timeline .timeline-icon:before{width:50px;right:-50px}
    .main-timeline .timeline:nth-child(even) .timeline-icon:before{right:auto;left:-50px}
    .main-timeline .timeline-content{margin-top:75px}
  }
  @media only screen and (max-width:990px){.main-timeline .timeline{margin:0 0 10px}
  .main-timeline .timeline-icon{left:25%}
  .main-timeline .timeline:nth-child(even) .timeline-icon{right:25%}
  .main-timeline .timeline-content{margin-top:115px}
}
@media only screen and (max-width:767px){.main-timeline{padding-top:50px}
.main-timeline:before{left:80px;right:0;margin:0}
.main-timeline .timeline{margin-bottom:70px}
.main-timeline .timeline:before{top:0;left:83px;right:0;margin:0}
.main-timeline .timeline-icon{width:60px;height:60px;line-height:40px;padding:5px;top:0;left:0}
.main-timeline .timeline:nth-child(even) .timeline-icon{left:0;right:auto}
.main-timeline .timeline-icon:before,.main-timeline .timeline:nth-child(even) .timeline-icon:before{width:25px;left:auto;right:-25px}
.main-timeline .timeline-icon:after,.main-timeline .timeline:nth-child(even) .timeline-icon:after{width:25px;height:30px;top:44px;left:auto;right:-5px}
.main-timeline .timeline-icon i{font-size:30px;line-height:45px}
.main-timeline .timeline-content,.main-timeline .timeline:nth-child(even) .timeline-content{width:100%;margin-top:-15px;padding-left:130px;padding-right:5px}
.main-timeline .timeline:nth-child(even) .timeline-content{float:right}
.main-timeline .timeline-content:before,.main-timeline .timeline:nth-child(even) .timeline-content:before{width:50%;left:120px}
.main-timeline .timeline:nth-child(even) .timeline-content:before{right:auto;transform:rotateY(0)}
.main-timeline .timeline-content:after,.main-timeline .timeline:nth-child(even) .timeline-content:after{left:85px}
}
@media only screen and (max-width:479px){.main-timeline .timeline-content,.main-timeline .timeline:nth-child(2n) .timeline-content{padding-left:110px}
.main-timeline .timeline-content:before,.main-timeline .timeline:nth-child(2n) .timeline-content:before{left:99px}
.main-timeline .timeline-content:after,.main-timeline .timeline:nth-child(2n) .timeline-content:after{left:65px}
}



/******************* Timeline Demo - 6 *****************/

.main-timeline6{overflow:hidden;position:relative}
.main-timeline6 .timeline{width:50%;float:right;position:relative;z-index:1}
.main-timeline6 .timeline:after,.main-timeline6 .timeline:before{position:absolute;top:50%;content:"";display:block;clear:both}
.main-timeline6 .timeline:before{width:40%;height:6px;background:#e8e8e8;left:0;z-index:-1;transform:translateY(-50%)}
.main-timeline6 .timeline:after{width:6px;height:70%;background:#e8e8e8;left:-3px}
.main-timeline6 .timeline-content{width:65%;float:right;padding:0 0 30px 30px;margin-right:15px;background:#fff;border-radius:10px;box-shadow:3px 3px 5px 6px #ccc}
.main-timeline6 .timeline-content:after,.main-timeline6 .timeline-content:before{content:"";width:26px;height:26px;border-radius:50%;background:#e8e8e8;position:absolute;top:50%;left:-13px;z-index:1;transform:translateY(-50%)}
.main-timeline6 .timeline-content:after{left:30%;transform:translate(-50%,-50%)}
.main-timeline6 .year{display:block;font-size:28px;font-weight:700;color:#e8e8e8;text-align:center;padding-left:50px}
.main-timeline6 .content-inner{padding:35px 15px 35px 110px;margin-right:-15px;background:#e8e8e8;border-radius:150px 0 0 150px;position:relative}
.main-timeline6 .content-inner:after,.main-timeline6 .content-inner:before{content:"";border-left:15px solid #640026;border-top:10px solid transparent;position:absolute;top:-10px;right:0}
.main-timeline6 .content-inner:after{border-top:none;border-bottom:10px solid transparent;top:auto;bottom:-10px}
.main-timeline6 .icon{width:110px;height:100%;text-align:center;position:absolute;top:0;left:0}
.main-timeline6 .icon i{font-size:60px;font-weight:700;color:#fff;position:absolute;top:50%;left:50%;transform:translate(-50%,-50%)}
.main-timeline6 .title{font-size:22px;font-weight:700;color:#fff;margin:0 0 5px}
.main-timeline6 .description{font-size:14px;color:#fff;margin:0}
.main-timeline6 .timeline:nth-child(2n) .icon,.main-timeline6 .timeline:nth-child(2n):after,.main-timeline6 .timeline:nth-child(2n):before{left:auto;right:0}
.main-timeline6 .timeline:nth-child(2n):after{right:-3px}
.main-timeline6 .timeline:nth-child(2n) .timeline-content{float:left;padding:0 30px 30px 0;margin:0 0 0 15px}
.main-timeline6 .timeline:nth-child(2n) .timeline-content:after,.main-timeline6 .timeline:nth-child(2n) .timeline-content:before{left:auto;right:-13px}
.main-timeline6 .timeline:nth-child(2n) .timeline-content:after{right:30%;margin-right:-25px}
.main-timeline6 .timeline:nth-child(2n) .year{padding:0 50px 0 0;color:#e8e8e8}
.main-timeline6 .timeline:nth-child(2n) .content-inner{padding:35px 110px 35px 15px;margin:0 0 0 -15px;border-radius:0 150px 150px 0}
.main-timeline6 .timeline:nth-child(2n) .content-inner:after,.main-timeline6 .timeline:nth-child(2n) .content-inner:before{border:none;border-right:15px solid #027dcd;border-top:10px solid transparent;right:auto;left:0}
.main-timeline6 .timeline:nth-child(2n) .content-inner:after{border-top:none;border-bottom:10px solid transparent}
.main-timeline6 .timeline:nth-child(2){margin-top:200px}
.main-timeline6 .timeline:nth-child(odd){margin:-190px 0 0}
.main-timeline6 .timeline:nth-child(even){margin-bottom:70px}
.main-timeline6 .timeline:first-child,.main-timeline6 .timeline:last-child:nth-child(even){margin:0}
.main-timeline6 .timeline:nth-child(2n) .content-inner,.main-timeline6 .timeline:nth-child(2n) .timeline-content:after,.main-timeline6 .timeline:nth-child(2n) .timeline-content:before,.main-timeline6 .timeline:nth-child(2n):after,.main-timeline6 .timeline:nth-child(2n):before{background:#e8e8e8}
.main-timeline6 .timeline:nth-child(3n) .content-inner,.main-timeline6 .timeline:nth-child(3n) .timeline-content:after,.main-timeline6 .timeline:nth-child(3n) .timeline-content:before,.main-timeline6 .timeline:nth-child(3n):after,.main-timeline6 .timeline:nth-child(3n):before{background:#e8e8e8}
.main-timeline6 .timeline:nth-child(3n) .content-inner:after,.main-timeline6 .timeline:nth-child(3n) .content-inner:before{border-left-color:#006662}
.main-timeline6 .timeline:nth-child(3n) .year{color:#00a3a9}
.main-timeline6 .timeline:nth-child(4n) .content-inner,.main-timeline6 .timeline:nth-child(4n) .timeline-content:after,.main-timeline6 .timeline:nth-child(4n) .timeline-content:before,.main-timeline6 .timeline:nth-child(4n):after,.main-timeline6 .timeline:nth-child(4n):before{background:#e8e8e8}
.main-timeline6 .timeline:nth-child(4n) .content-inner:after,.main-timeline6 .timeline:nth-child(4n) .content-inner:before{border-right-color:#92070e}
.main-timeline6 .timeline:nth-child(4n) .year{color:#e8e8e8}
@media only screen and (max-width:990px) and (min-width:768px){.main-timeline6 .timeline:after{height:80%}
}
@media only screen and (max-width:767px){.main-timeline6 .timeline:last-child,.main-timeline6 .timeline:nth-child(even),.main-timeline6 .timeline:nth-child(odd){margin:0}
.main-timeline6 .timeline{width:95%;margin:15px 15px 15px 0!important}
.main-timeline6 .timeline .timeline-content:after,.main-timeline6 .timeline .timeline-content:before,.main-timeline6 .timeline:after,.main-timeline6 .timeline:before{display:none}
.main-timeline6 .timeline-content,.main-timeline6 .timeline:nth-child(2n) .timeline-content{width:100%;float:none;padding:0 0 30px 30px;margin:0}
.main-timeline6 .content-inner,.main-timeline6 .timeline:nth-child(2n) .content-inner{padding:35px 15px 35px 110px;margin:0 -15px 0 0;border-radius:150px 0 0 150px}
.main-timeline6 .timeline:nth-child(2n) .content-inner:after,.main-timeline6 .timeline:nth-child(2n) .content-inner:before{border:none;border-left:15px solid #027dcd;border-top:10px solid transparent;right:0;left:auto}
.main-timeline6 .timeline:nth-child(2n) .content-inner:after{border-top:none;border-bottom:10px solid transparent}
.main-timeline6 .timeline:nth-child(2n) .icon{top:0;left:0}
.main-timeline6 .timeline:nth-child(4n) .content-inner:after,.main-timeline6 .timeline:nth-child(4n) .content-inner:before{border-left-color:#92070e}
}





</style>


<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
  <!-- Header Container / End -->

<!-- Content
  ================================================== -->
  <div id="titlebar" class="white margin-bottom-30">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Agenda - Event Tahunan </h2>
          <span>Disparbupora Kab. Bogor</span>

          <!-- Breadcrumbs -->
          <nav id="breadcrumbs" class="dark">
            <ul>
              <li><a href="#">Home</a></li>
              <li>Agenda</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>



  <div class="demo">
    <div class="container">
      <h4>Timeline Style : Demo-6</h4>
      <div class="row">
        <div class="col-md-12">
          <div class="main-timeline6">


            <div class="timeline" >
              <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto1.jpg";?>) !important ;min-height:300px;";>



              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
               <a href="#" class="button">22 April 2019</a>

             </div>


           </div>
         </div>





         <div class="timeline"  style="">
           <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto2.jpg";?>) !important ;min-height:300px;";>

              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
                <a href="#" class="button">22 April 2019</a>


             </div>


           </div>
        </div>


        <div class="timeline">
            <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto3.jpg";?>) !important ;min-height:300px;";>

              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
                <a href="#" class="button">22 April 2019</a>


             </div>


           </div>
        </div>

         <div class="timeline">
            <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto4.jpg";?>) !important ;min-height:300px;";>

              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
                <a href="#" class="button">22 April 2019</a>


             </div>


           </div>
        </div>

         <div class="timeline">
            <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto5.jpg";?>) !important ;min-height:300px;";>

              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
                <a href="#" class="button">22 April 2019</a>

             </div>


           </div>
        </div>
         <div class="timeline">
            <div class="timeline-content" style="background: 
              /* top, transparent red */ 
              linear-gradient(to top, rgba(35,35,37,0.9) 0%, rgba(35,35,37,0.45) 35%, rgba(22,22,23,0) 60%, rgba(0,0,0,0) 100%),
              /* bottom, image */
              url(<?php echo base_url()."data/sarana/foto6.jpg";?>) !important ;min-height:300px;";>

              <div style="text-align: center; margin-top:40%">
               <h3 class="title">Web Designer</h3>
               <p style="color: #fff">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer malesuada</p>
                <a href="#" class="button">22 April 2019</a>


             </div>


           </div>
        </div>




      </div>
    </div>
  </div>
</div>
</div>









<!-- Pagination -->
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <!-- Pagination -->
    <div class="pagination-container margin-top-10 margin-bottom-20">
      <nav class="pagination">
        <ul>




        </ul>
      </nav>
    </div>
  </div>
</div>
<!-- Pagination / End -->








<!-- Section -->

<!-- Section / End -->


</div>
</div>
</div>
<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>