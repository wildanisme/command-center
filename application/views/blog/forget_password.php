  <?php $this->load->view('blog/src/header');?>
  



<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
	<div id="titlebar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h2><?= $title;?></h2>

					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<li><?= $title;?></li>
						</ul>
					</nav>

				</div>
			</div>
		</div>
	</div>


</div>

<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-md-12">

			<section id="not-found" class="center">
				
				<div class="row col-lg-8 col-lg-offset-2">
					<?php if(!empty($message)){?>
						<div class="notification <?= $type;?> ">
							<span><?=$message;?></span>			
						</div>
					<?php } ?>
					<?= form_error("email","<mark>","</mark>");?>
					
				</div>
				<div class="row">
					<form method="post">
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
						<div class="col-lg-8 col-lg-offset-2">
							<div class="main-search-input gray-style margin-top-50 margin-bottom-10">
								<div class="main-search-input-item">
									<input type="text" name="email" placeholder="Masukan Alamat Email" value=""/>
								</div>
								
								<button type="submit" class="button">Kirim Email</button>
							</div>
						</div>
					</form>
				</div>
				
				

			</section>

		</div>
	</div>

</div>
<!-- Container / End -->


<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>