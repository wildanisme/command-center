<?php $this->load->view('blog/src/header');?>


        <div class="clearfix"></div>
        <!-- Header Container / End -->


     
<div class="clearfix"></div>
<!-- Header Container / End -->
<!-- Page Content
================================================== -->
<div class="full-page-container with-map">

  <div class="full-page-sidebar hidden-sidebar">
    <div class="full-page-sidebar-inner" data-simplebar>
    <form method="get">
      <div class="sidebar-container">
        <!-- Keywords -->
        <div class="sidebar-widget">
          <h3>Kata kunci</h3>
          <div class="keywords-container">
            <div class="keyword-input-container">
              
              <input name="s" type="text" value="<?= !empty($filter['s']) ? $filter['s'] : '' ;?>"  class="keyword-input" placeholder="Cari.."/>
              <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
            </div>
            <div class="keywords-list"><!-- keywords go here --></div>
            <div class="clearfix"></div>
          </div>
        </div>
        
        <!-- Category -->
        <div class="sidebar-widget">
          <h3>Kategori</h3>
          <select name="id_kategori_sarana" class="selectpicker default" data-selected-text-format="count" data-size="7" title="Semua kategori" >
            <option value="">Semua</option>
            <?php foreach($kategori_sarana as $row){
              $selected = (!empty($filter['id_kategori_sarana']) && $row->id_kategori_sarana == $filter['id_kategori_sarana'] ) ? "selected" : "";
              ?>            
            <option <?= $selected ;?> value="<?= $row->id_kategori_sarana;?>" ><?=$row->nama_kategori_sarana;?></option>
            <?php } ?>
          </select>
        </div>
        
        

        <!--
        <div class="sidebar-widget">
          <h3>Harga</h3>
          <div class="margin-top-55"></div>
          <input class="range-slider" name="harga" type="text" value="" data-slider-currency="Rp" data-slider-min="10000" data-slider-max="10000000" data-slider-step="100" data-slider-value="[50000,5000000]"/>
        </div>

        -->
        <div class="sidebar-widget">
          <h3>Tags</h3>

          <div class="tags-container">
          <?php foreach($tags as $key=>$value){
            if($value!=""){

              $checked = (!empty($filter['tag']) && in_array($value,$filter['tag'])) ? "checked" : "";
            ?>
            <div class="tag">
              <input <?= $checked ;?> type="checkbox" name="tag[]" id="tag<?=$key;?>" value="<?=$value;?>"/>
              <label for="tag<?=$key;?>"><?=$value;?></label>
            </div>
          <?php }}?>
          </div>
          <div class="clearfix"></div>
        </div>

      </div>
      <!-- Sidebar Container / End -->

      <!-- Search Button -->
      <div class="sidebar-search-button-container">
        <button type="submit" class="button ripple-effect">Cari</button>
      </div>
      <!-- Search Button / End-->
    </form>
    </div>
  </div>
  <!-- Full Page Sidebar / End -->
  

  <!-- Full Page Content -->
  <div class="full-page-content-container" data-simplebar>
    <div class="full-page-content-inner">

      <h3 class="page-title">Hasil pencarian</h3>

      

      <div class="listings-container grid-layout margin-top-35">
        

        <?php foreach($sarana as $row){?>
        <!-- Job Listing -->
        <a href="<?=base_url();?>sarana/detail/<?=$row->id_sarana;?>" class="job-listing">

          <!-- Job Listing Details -->
          <div class="job-listing-details">
            <!-- Logo -->
            <div class="job-listing-company-logo">
              <img src="<?= base_url();?>data/sarana/<?=$row->banner;?>" alt="">
            </div>

            <!-- Details -->
            <div class="job-listing-description">
              <h4 class="job-listing-company"><?= $row->nama_kategori_sarana;?> <?= ($row->status=="Terverifikasi") ? '<span class="verified-badge" title="Terverifikasi" data-tippy-placement="top"></span>' :'';?></h4>
              <h3 class="job-listing-title"><?= $row->nama_sarana;?></h3>
            </div>
          </div>

          <!-- Job Listing Footer -->
          <div class="job-listing-footer">
            
            <ul>
              <li><i class="icon-material-outline-location-on"></i> <?= $row->alamat ." - ". $row->desa;?></li>
              
            </ul>
          </div>
        </a>  

        <?php } ?>

      </div>

      <!-- Pagination -->
      <div class="clearfix"></div>
      
      <div class="pagination-container margin-top-20 margin-bottom-20">
        <nav class="pagination">
          <ul>
          <?php
                $CI =& get_instance();
                            $CI->load->library('pagination');

                            $config['base_url'] = base_url(). 'sarana/index/?'.http_build_query($filter);
                            $config['total_rows'] = $total_rows;
                            $config['per_page'] = $per_page;
                            $config['attributes'] = array('class' => 'ripple-effect');
                            $config['page_query_string']=TRUE;
                            $CI->pagination->initialize($config);
                            $link = $CI->pagination->create_links();

                            
                            $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                            $link = str_replace("</strong>", "</a></li>", $link);
                            $link = str_replace("&lt;", "Back", $link);
                            $link = str_replace("&gt;", "Next", $link);
                            $link = str_replace("<a ;", "<li><a ", $link);
                             $link = str_replace("</a> ;", "<li></a> ", $link);
                             echo $link;
              ?>
          </ul>
        </nav>
      </div>
      <div class="clearfix"></div>
      <!-- Pagination / End -->

      

    </div>
  </div>
  <!-- Full Page Content / End -->


  <!-- Full Page Map -->
  <div class="full-page-map-container">
    
    <!-- Enable Filters Button -->
    <div class="filter-button-container">
      <button class="enable-filters-button">
        <i class="enable-filters-button-icon"></i>
        <span class="show-text">Tampilkan Filter</span>
        <span class="hide-text">Sembunyikan</span>
      </button>
      <div class="filter-button-tooltip">Klik untuk memfilter</div>
    </div>
    
    <!-- Map -->
      <div id="map" data-map-zoom="12"  data-map-scroll="true"></div>
  </div>
  <!-- Full Page Map / End -->

</div>


</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<script src="<?php echo base_url().'asset/cityzen/';?>js/jquery-3.3.1.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/jquery-migrate-3.0.0.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/mmenu.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/tippy.all.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/simplebar.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/bootstrap-slider.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/snackbar.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/clipboard.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/counterup.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/magnific-popup.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/slick.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/custom.js"></script>

<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
  Snackbar.show({
    text: 'Your status has been changed!',
    pos: 'bottom-center',
    showAction: false,
    actionText: "Dismiss",
    duration: 3000,
    textColor: '#fff',
    backgroundColor: '#383838'
  }); 
}); 
</script>

<!-- Google Autocomplete -->
<script>
  function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
  }
</script>

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw&libraries=places"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/infobox.min.js"></script>
<script src="<?php echo base_url().'asset/cityzen/';?>js/markerclusterer.js"></script>
<script>

(function($){
    "use strict";

    function mainMap() {

      // Locations
      // ----------------------------------------------- //
      var ib = new InfoBox();

      // Infobox Output
      function locationData(jobURL, companyLogo, companyName, jobTitle, verifiedBadge) {
          return(''+
            '<a href="'+jobURL+'" class="job-listing">'+
               '<div class="infoBox-close"><i class="icon-feather-x"></i></div>'+
               '<div class="job-listing-details">'+
                  '<div class="job-listing-company-logo">'+
                    '<div class="'+verifiedBadge+'-badge"></div>'+
                    '<img src="'+companyLogo+'" alt="">'+
                  '</div>'+
                  '<div class="job-listing-description">'+
                    '<h4 class="job-listing-company">'+companyName+'</h4>'+
                    '<h3 class="job-listing-title">'+jobTitle+'</h3>'+
                  '</div>'+
               '</div>'+
            '</a>')
      }

      // Locations
      
      var locations_ = [
        [ locationData('single-job-page.html','images/company-logo-01.png',"Hexagon",'Bilingual Event Support Specialist', 'verified'), 37.788181, -122.461270, 5, ''],
        [ locationData('single-job-page.html','images/company-logo-05.png',"Laxo",'Competition Law Officer', 'not-verified'), 37.750812, -122.471934, 2, ''],
        [ locationData('single-job-page.html','images/company-logo-02.png',"Coffee",'Barista and Cashier', 'not-verified'), 37.735609, -122.458201, 3, ''],
        [ locationData('single-job-page.html','images/company-logo-03.png',"King",'Restaurant General Manager', 'verified'), 37.745382, -122.500773, 4, ''],
        [ locationData('single-job-page.html','images/company-logo-05.png',"Skyist",'International Marketing Coordinator', 'not-verified'), 37.762963, -122.388506, 1, ''],
        [ locationData('single-job-page.html','images/company-logo-05.png',"Podous",'Construction Labourers', 'not-verified'), 37.801745, -122.409085, 6, ''],
        [ locationData('single-job-page.html','images/company-logo-04.png',"Mates",'Administrative Assistant', 'not-verified'), 37.730511, -122.383679, 7, ''],
        [ locationData('single-job-page.html','images/company-logo-06.png',"Trideo",'Human Resources Consultant', 'not-verified'), 37.750457, -122.478779, 8, ''],
        [ locationData('single-job-page.html','images/company-logo-06.png',"Trideo",'International Marketing Specialist', 'not-verified'), 37.732810, -122.415951, 9, ''],
        [ locationData('single-job-page.html','images/company-logo-02.png',"Coffee",'Terrain Cafe Barista', 'not-verified'), 37.733625, -122.378872, 10, ''],
        [ locationData('#','images/company-logo-05.png',"Kinte",'Skilled Labourer', 'not-verified'), 37.723578, -122.457493, 11, ''],
        [ locationData('single-job-page.html','images/company-logo-05.png',"Alilia",'Healthcare Claims Advisor', 'not-verified'), 37.751543, -122.418354, 12, '']
      ];
      
     var locations = <?= $direktori ;?>;

   

      // Map Attributes
      // ----------------------------------------------- //

      var mapZoomAttr = $('#map').attr('data-map-zoom');
      var mapScrollAttr = $('#map').attr('data-map-scroll');

      if (typeof mapZoomAttr !== typeof undefined && mapZoomAttr !== false) {
          var zoomLevel = parseInt(mapZoomAttr);
      } else {
          var zoomLevel = 5;
      }

      if (typeof mapScrollAttr !== typeof undefined && mapScrollAttr !== false) {
         var scrollEnabled = parseInt(mapScrollAttr);
      } else {
        var scrollEnabled = false;
      }


      // Main Map
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoomLevel,
        scrollwheel: scrollEnabled,
        center: new google.maps.LatLng(-6.838118799999999,107.9275324),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        gestureHandling: 'cooperative',

        // Google Map Style
        styles: [{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]

      });


      // Infobox
      // ----------------------------------------------- //

      var boxText = document.createElement("div");
      boxText.className = 'map-box'

      var currentInfobox;

      var boxOptions = {
              content: boxText,
              disableAutoPan: false,
              alignBottom : true,
              maxWidth: 0,
              pixelOffset: new google.maps.Size(-160, 0),
              zIndex: null,
              boxStyle: {
                width: "320px"
              },
              closeBoxMargin: "0",
              closeBoxURL: "",
              infoBoxClearance: new google.maps.Size(25, 25),
              isHidden: false,
              pane: "floatPane",
              enableEventPropagation: false,
      };


      var markerCluster, overlay, i;
      var allMarkers = [];

      var clusterStyles = [
        {
          textColor: 'white',
          url: '',
          height: 50,
          width: 50
        }
      ];


      var markerIco;
      for (i = 0; i < locations.length; i++) {

        markerIco = locations[i][4];

        var overlaypositions = new google.maps.LatLng(locations[i][1], locations[i][2]),

        overlay = new CustomMarker(
         overlaypositions,
          map,
          {
            marker_id: i
          },
          markerIco
        );

        allMarkers.push(overlay);

        google.maps.event.addDomListener(overlay, 'click', (function(overlay, i) {

        return function() {
             ib.setOptions(boxOptions);
             boxText.innerHTML = locations[i][0];
             ib.close();
             ib.open(map, overlay);
             currentInfobox = locations[i][3];

            google.maps.event.addListener(ib,'domready',function(){
              $('.infoBox-close').click(function(e) {
                  e.preventDefault();
                  ib.close();
                  $('.map-marker-container').removeClass('clicked infoBox-opened');
              });

            });

          }
        })(overlay, i));

      }


      // Marker Clusterer Init
      // ----------------------------------------------- //

      var options = {
         imagePath: 'images/',
         styles : clusterStyles,
         minClusterSize : 3
      };

      markerCluster = new MarkerClusterer(map, allMarkers, options);

      google.maps.event.addDomListener(window, "resize", function() {
          var center = map.getCenter();
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
      });


      // Custom User Interface Elements
      // ----------------------------------------------- //

      // Custom Zoom-In and Zoom-Out Buttons
        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        function ZoomControl(controlDiv, map) {

          zoomControlDiv.index = 1;
          map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
          // Creating divs & styles for custom zoom control
          controlDiv.style.padding = '5px';
          controlDiv.className = "zoomControlWrapper";

          // Set CSS for the control wrapper
          var controlWrapper = document.createElement('div');
          controlDiv.appendChild(controlWrapper);

          // Set CSS for the zoomIn
          var zoomInButton = document.createElement('div');
          zoomInButton.className = "custom-zoom-in";
          controlWrapper.appendChild(zoomInButton);

          // Set CSS for the zoomOut
          var zoomOutButton = document.createElement('div');
          zoomOutButton.className = "custom-zoom-out";
          controlWrapper.appendChild(zoomOutButton);

          // Setup the click event listener - zoomIn
          google.maps.event.addDomListener(zoomInButton, 'click', function() {
            map.setZoom(map.getZoom() + 1);
          });

          // Setup the click event listener - zoomOut
          google.maps.event.addDomListener(zoomOutButton, 'click', function() {
            map.setZoom(map.getZoom() - 1);
          });

      }

      // Geo Location Button
      $("#geoLocation, .input-with-icon.location a").click(function(e){
          e.preventDefault();
          geolocate();
      });

      function geolocate() {

          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function (position) {
                  var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                  map.setCenter(pos);
                  map.setZoom(12);
              });
          }
      }

    }


    // Map Init
    var map =  document.getElementById('map');
    if (typeof(map) != 'undefined' && map != null) {
      google.maps.event.addDomListener(window, 'load',  mainMap);
    }


    // ---------------- Main Map / End ---------------- //


    // Single Listing Map
    // ----------------------------------------------- //

    function singleListingMap() {

      var myLatlng = new google.maps.LatLng({lng: $( '#singleListingMap' ).data('longitude'),lat: $( '#singleListingMap' ).data('latitude'), });

      var single_map = new google.maps.Map(document.getElementById('singleListingMap'), {
        zoom: 15,
        center: myLatlng,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        styles:  [{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"23"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#f38eb0"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"color":"#ced7db"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#ffa5a8"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#c7e5c8"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"color":"#d6cbc7"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#c4c9e8"}]},{"featureType":"poi.sports_complex","elementType":"geometry.fill","stylers":[{"color":"#b1eaf1"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":"100"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"},{"lightness":"100"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffd4a5"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffe9d2"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"weight":"3.00"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"weight":"0.30"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#747474"},{"lightness":"36"}]},{"featureType":"road.local","elementType":"labels.text.stroke","stylers":[{"color":"#e9e5dc"},{"lightness":"30"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":"100"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#d2e7f7"}]}]
      });

      // Steet View Button
      $('#streetView').click(function(e){
         e.preventDefault();
         single_map.getStreetView().setOptions({visible:true,position:myLatlng});
         // $(this).css('display', 'none')
      });


      // Custom zoom buttons
      var zoomControlDiv = document.createElement('div');
      var zoomControl = new ZoomControl(zoomControlDiv, single_map);

      function ZoomControl(controlDiv, single_map) {

        zoomControlDiv.index = 1;
        single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);

        controlDiv.style.padding = '5px';

        var controlWrapper = document.createElement('div');
        controlDiv.appendChild(controlWrapper);

        var zoomInButton = document.createElement('div');
        zoomInButton.className = "custom-zoom-in";
        controlWrapper.appendChild(zoomInButton);

        var zoomOutButton = document.createElement('div');
        zoomOutButton.className = "custom-zoom-out";
        controlWrapper.appendChild(zoomOutButton);

        google.maps.event.addDomListener(zoomInButton, 'click', function() {
          single_map.setZoom(single_map.getZoom() + 1);
        });

        google.maps.event.addDomListener(zoomOutButton, 'click', function() {
          single_map.setZoom(single_map.getZoom() - 1);
        });

      }


      // Marker
      var singleMapIco =  "<i class='"+$('#singleListingMap').data('map-icon')+"'></i>";

      new CustomMarker(
        myLatlng,
        single_map,
        {
          marker_id: '1'
        },
        singleMapIco
      );


    }

    // Single Listing Map Init
    var single_map =  document.getElementById('singleListingMap');
    if (typeof(single_map) != 'undefined' && single_map != null) {
      google.maps.event.addDomListener(window, 'load',  singleListingMap);
    }

    // -------------- Single Listing Map / End -------------- //



    // Custom Map Marker
    // ----------------------------------------------- //

    function CustomMarker(latlng, map, args, markerIco) {
      this.latlng = latlng;
      this.args = args;
      this.markerIco = markerIco;
      this.setMap(map);
    }

    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function() {

      var self = this;

      var div = this.div;

      if (!div) {

        div = this.div = document.createElement('div');
        div.className = 'map-marker-container';

        div.innerHTML = '<div class="marker-container">'+
                            '<div class="marker-card">'+
                            '</div>'+
                          '</div>'


        // Clicked marker highlight
        google.maps.event.addDomListener(div, "click", function(event) {
            $('.map-marker-container').removeClass('clicked infoBox-opened');
            google.maps.event.trigger(self, "click");
            $(this).addClass('clicked infoBox-opened');
        });


        if (typeof(self.args.marker_id) !== 'undefined') {
          div.dataset.marker_id = self.args.marker_id;
        }

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
      }

      var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

      if (point) {
        div.style.left = (point.x) + 'px';
        div.style.top = (point.y) + 'px';
      }
    };

    CustomMarker.prototype.remove = function() {
      if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null; $(this).removeClass('clicked');
      }
    };

    CustomMarker.prototype.getPosition = function() { return this.latlng; };

    // -------------- Custom Map Marker / End -------------- //



})(this.jQuery);


</script>


</body>
</html>