<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Daftar Jenis Perizinan dan Non Perizinan</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content" style='min-height:440px;'>
      <div class="container">
        <div class="row blog-page">

          <!-- Start Blog Posts -->
          <div class="col-md-12 blog-box">
          

                    <div class="x_panel">
                  <div class="x_title">
                    <h2>Daftar Jenis Izin </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table id="datatable" class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Jenis Izin</th>
              <th>Jumlah Persyaratan</th>
                          <th>Bidang Izin</th>
                         
                          <th>Opsi</th>
                        </tr>
                      </thead>
                      <tbody>

                         <?php 
                          $no=($offset+1); foreach ($result as $key){
                          if ($key->status=="Y") {
                           $status = "Aktif";
                          }elseif ($key->status=="N") {
                           $status = "Non Aktif";
                          }
                        ?>
                        <tr>
                          <th scope="row"><?php echo $no;?></th>
                          <td><?php echo $key->nama_jenisizin;?></td>
              <td><?php echo $key->jumlah_persyaratan_izin;?></td>
                          <td><?php echo $key->nama_bidang;?></td>
                         
                          <td>

                            <a href="<?php echo base_url(). "persyaratan/detail/".$key->id_jenis_izin ;?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Lihat Persyaratan </a>
                           <!--a   title='Delete' onclick='delete_(<?php echo $key->id_jenis_izin;?>)' class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a-->
                            
                            
                          </td>
                        </tr>
                        <?php $no++;}?>
                        
                      </tbody>
                    </table>
<div class='row'>
            <div class='col-md-12 pager'>
            <?php
      
            
              $CI =& get_instance();
              $CI->load->library('pagination');

              $config['base_url'] = base_url(). 'ref_persyaratan_izin/index/';
              $config['total_rows'] = $total_rows;
              $config['per_page'] = $per_page;
              $config['attributes'] = array('class' => 'btn btn-primary btn-xm marginleft2px');
              $config['page_query_string']=TRUE;
              $CI->pagination->initialize($config);
              $link = $CI->pagination->create_links();
              $link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled marginleft2px' >", $link);
              $link = str_replace("</strong>", "</button>", $link);
              echo $link;
              
            ?>
            </div>

          </div>
                  </div>
                </div>
              </div>
       
          


      </div>
    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>