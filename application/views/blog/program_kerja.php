<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Program Kerja</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href=" <?php echo base_url().'program_kerja'; ?> ">Program Kerja</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content" style="min-height:450px;">
      <div class="container">
        <div class="row blog-page">

          <!-- Start Blog Posts -->
          <div class="col-md-12 blog-box">
		  
		  <!-- Single Testimonial -->

  
                <h2 align="center"> Program Kerja </h2>
            	<div class='classic-testimonials'>
						<div class='testimonial-content'>
						<p><?php echo $isi ?></p>
						</div>

           
				
				</div>
       </div>
       </div>

			<!-- End Testimonial -->
          

      </div>
    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>