<html>
	<head>
		<title>Laporan</title>
	</head>
	<body>
		<style type="text/css">
	table.tableizer-table {
		font-size: 12px;
		border: 1px solid #CCC; 
		font-family: Arial, Helvetica, sans-serif;
	} 
	.tableizer-table td {
		padding: 4px;
		margin: 3px;
		border: 1px solid #CCC;
	}
	.tableizer-table th {
		background-color: #B1B1B1; 
		color: #FFF;
		font-weight: bold;
	}
	.paging{
		padding:5px;
	}
</style>
<?php
	if ($download==1)
		$width = "";
	else
		$width = "100%";
?>
<table class="tableizer-table" width='<?=$width;?>'>
<thead><tr class="tableizer-firstrow"><th>No</th><th>Tanggal SK</th><th>&nbsp;</th><th>&nbsp;</th><th>Tanggal SK</th><th>No. SK</th><th>Bidang </th><th>Herr/ Baru</th><th>Jenis Izin </th><th>Bentuk Persh</th><th>Nama                     Pemohon</th><th>Nama          Perusahaan</th><th>Dusun</th><th>Desa</th><th>Kecamatan</th><th>Uraian</th><th>Sektor</th><th>Luas Tanah</th><th>Luas Bangunan</th><th>Nilai Investasi              (Rp)</th><th>Tahun</th><th>Ket.</th><th>Tahun Berakhir</th></tr></thead><tbody>
 <?php
	if(!empty($result)){
		$no=$offset+1;
		$CI =& get_instance();
		$CI->load->model('laporan_model');
		
		foreach($result as $row){
			$tanggal_sk = $row->tanggal_sk!="0000-00-00"? date('d-m-Y',strtotime($row->tanggal_sk)) : "" ; 
			$tgl_akhir_sk = $row->tanggal_akhir_sk;
			$nama_sektor = $row->nama_sektor;
			if ($row->nama_sektor2!="") $nama_sektor .="; ".$row->nama_sektor2;
			if ($row->nama_sektor3!="") $nama_sektor .="; ".$row->nama_sektor3;
			
			$tgl_skrg = date('Y-m-d');
			$warning_date = date('Y-m-d', strtotime ( '+60 day' , strtotime ( $tgl_skrg ) ) ) ;
			$color="";$ket="";
			if($row->id_jenis_izin!=14){ // 14 = IMB
				if ($tgl_akhir_sk <= $tgl_skrg){
					$color = "red";$ket="Masa berlaku SK habis";
				}
				else if ($tgl_akhir_sk > $tgl_skrg && $tgl_akhir_sk <= $warning_date){
					$color = "yellow";$ket="";
				}
				
			}
			$desa = $CI->laporan_model->get_nama_desa($row->desa);
			$kecamatan = $CI->laporan_model->get_nama_kecamatan($row->kecamatan);
			echo"
				<tr>
					<td bgcolor='$color'>$no</td>
					<td bgcolor='$color'>$row->tgl_sk</td>
					<td bgcolor='$color'>$row->bln_sk</td>
					<td bgcolor='$color'>$row->thn_sk</td>
					<td bgcolor='$color'>$tanggal_sk</td>
					<td bgcolor='$color'>$row->nomer_sk</td>
					<td bgcolor='$color'>$row->nama_bidang</td>
					<td bgcolor='$color'>$row->id_jenis_layanan_izin</td>
					<td bgcolor='$color'>$row->nama_jenisizin</td>
					<td bgcolor='$color'>$row->nama_bentuk_perusahaan</td>
					<td bgcolor='$color'>$row->nama_pemohon</td>
					
					<td bgcolor='$color'>$row->nama_perusahaan</td>
					<td bgcolor='$color'>$row->alamat_perusahaan</td>
					<td bgcolor='$color'>$desa</td>
					<td bgcolor='$color'>$kecamatan</td>
					
					<td bgcolor='$color'>$row->uraian</td>
					<td bgcolor='$color'>$nama_sektor</td>
					<td bgcolor='$color'>$row->luas_tanah</td>
					<td bgcolor='$color'>$row->luas_bangunan_gedung</td>
					<td bgcolor='$color'>$row->nilai_investasi</td>
					<td bgcolor='$color'>$row->thn_sk</td>
					<td bgcolor='$color'>$ket</td>
					<td bgcolor='$color'>$row->thn_akhir_sk</td>
				</tr>
			";
			$no++;
		}
	}
	else{
		echo"<tr><td colspan='24'>Tidak ada data</td></tr>";
	}
 ?>
 </tbody></table>
 <?php
      
            
              $CI =& get_instance();
              $CI->load->library('pagination');
              $arr_sektor = "";
              if(!empty($sektor) && empty($get)){
              	for($i=0;$i<count($sektor);$i++){
              		$arr_sektor .= $sektor[$i].".";
              		//if ($i>0 && $i<(count($sektor)-1)) $sektor .=".";
              	}

              }
              else if(!empty($get)){
              	$arr_sektor = $sektor;
              }
              $config['base_url'] = base_url(). 'rekapdata/paging/?bidang_izin='.$bidang_izin.'&id_jenis_izin='.$id_jenis_izin.
              '&id_provinsi='.$id_provinsi.'&id_kabupaten='.$id_kabupaten.'&id_kecamatan='.$id_kecamatan.'&id_desa='.$id_desa.
              '&bulan='.$bulan.'&tahun='.$tahun.'&bulan_terbit='.$bulan_terbit.'&tahun_terbit='.$tahun_terbit.'&download='.$download.'&masa_berlaku_sk='.$masa_berlaku_sk.'&sektor='.$arr_sektor;
              $config['total_rows'] = $total_rows;
              $config['per_page'] = $limit;
              $config['attributes'] = array('class' => 'paging');
              $config['page_query_string']=TRUE;
              $CI->pagination->initialize($config);
              $link = $CI->pagination->create_links();
             // $link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled marginleft2px' >", $link);
             // $link = str_replace("</strong>", "</button>", $link);
			 if (!$download){
              echo "<br>$link";
              $download_link = base_url(). 'rekapdata/paging/?bidang_izin='.$bidang_izin.'&id_jenis_izin='.$id_jenis_izin.
              '&id_provinsi='.$id_provinsi.'&id_kabupaten='.$id_kabupaten.'&id_kecamatan='.$id_kecamatan.'&id_desa='.$id_desa.
              '&bulan='.$bulan.'&tahun='.$tahun.'&bulan_terbit='.$bulan_terbit.'&tahun_terbit='.$tahun_terbit.'&download=1&masa_berlaku_sk='.$masa_berlaku_sk.'&sektor='.$arr_sektor.
              '&per_page='.$per_page;
			 echo "&nbsp<a href='".$download_link."'>download</a>";
			 }
            ?>
	</body>
</html>