<div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filter laporan</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <form id="laporan" class="form-horizontal form-label-left" method=post target="_blank" action="<?php echo base_url()."rekapdata/laporan/";?>">
          
         
            <div class="form-group">
              <label>Bidang Izin</label>
            </div>
          
            <div class="form-group" id="induk1">
              <select class="form-control" name="bidang_izin" id="id_bidang" onchange='getIzin()'>
                
                <option value="" selected>Pilih</option>
                <?php
                  foreach($bidangizin as $row){
                    echo "<option value='$row->id_bidang'>$row->nama_bidang</option>";
                  }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis Izin</label>
            </div>
          
            <div class="form-group" id="">
              <select class="form-control" name="id_jenis_izin" id='id_jenis_izin'>
                
                <option value="" selected>Pilih</option>
                <?php
                  foreach($jenisizin as $row){
                    echo "<option value='$row->id_jenis_izin'>$row->nama_jenisizin</option>";
                  }
                ?>
              </select>
            </div>

            <div class="form-group" id="induk1">
            <label>
            Jenis Sektor Usaha
            </label>
            <?php
				$i=0;
                  foreach($sektor as $row){
                    echo "
               <div class='checkbox'>
                            <label>
                              <input type='checkbox' name='sektor[]' value='$row->id_sektor' class='flat'> $row->nama_sektor
                            </label>
                </div>";
				$i++;
                 }
                ?>

              <br>
              

              <div class="form-group">
                    <label>Provinsi</label>
                    <select class='form-control' name='id_provinsi' id='id_provinsi' onchange='getKabupaten()'>
                      <option value='' selected>Pilih</option> 
                      <?php
                        foreach($provinsi as $row){
                          $selected = $row->id_provinsi == $id_provinsi ? "selected" : "";
                          echo "<option value='$row->id_provinsi' $selected>$row->provinsi</option>";
                        }
                        
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kabupaten/Kota</label>
                    <select class='form-control' name='id_kabupaten' id='id_kabupaten' onchange='getKecamatan()'>
                      <option value='' selected>Pilih</option>             
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class='form-control' name='id_kecamatan' id='id_kecamatan' onchange='getDesa()'>
                      <option value='' selected>Pilih</option>   
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kelurahan/Desa</label>
                    <select class='form-control' name='id_desa' id='id_desa'>
                      <option value='' selected>Pilih</option>             
                    </select>
                  </div>      


           
            <div class="form-group">
              <label>Bulan Akhir SK</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="bulan" id="bulan">
                
                <option value="" selected>Pilih</option>
                <?php
                  for($bln=1;$bln<=12;$bln++){
                    echo "<option value='$bln'>$bulan[$bln]</option>";
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label>Tahun Akhir SK</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="tahun" id="tahun">
                
                <option value="" selected>Pilih</option>
                <?php
                  $tahun = (date('Y') + 10);
                  while($tahun > $tahun_minimal){
                    echo "<option value='$tahun'>$tahun</option>";
                    $tahun--;
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label>Bulan Terbit SK</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="bulan_terbit" id="bulan">
                
                <option value="" selected>Pilih</option>
                <?php
                  for($bln=1;$bln<=12;$bln++){
                    echo "<option value='$bln'>$bulan[$bln]</option>";
                  }
                ?>
              </select>
            </div>
            <div class="form-group">
              <label>Tahun Terbit SK</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="tahun_terbit" id="tahun">
                
                <option value="" selected>Pilih</option>
                <?php
                  $tahun = (date('Y') + 10);
                  while($tahun > $tahun_minimal){
                    echo "<option value='$tahun'>$tahun</option>";
                    $tahun--;
                  }
                ?>
              </select>
            </div>
			<div class="form-group">
              <label>Masa berlaku SK</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="masa_berlaku_sk" id="masa_berlaku_sk">
                
                <option value="0" selected>Semua</option>
                <?php
                  for($i=1;$i<=count($masa_berlaku_sk);$i++){
                    echo "<option value='$i'>$masa_berlaku_sk[$i]</option>";
                  }
                ?>
                
              </select>
            </div>
                      <div class="form-group">
              <input type="hidden" id="download" name="download"/>
                          <a href='<?= base_url()."rekapdata";?>' class="btn btn-default">Reset</a>
                          <a onclick="submit_()" class="btn btn-success">Tampilkan data</a>
             <!-- <a onclick="submit_(1)" class="btn btn-danger" >Download</a>-->
                      </div>

                    </form>
                  </div>
                </div>
              </div>

<script>
function getIzin(){
    var id = $('#id_bidang').val();
    $('#id_jenis_izin').html('<option value="">Pilih</option>');
    $.post("<?php echo base_url();?>rekapdata/get_izin/"+id,{},function(obj){
      $('#id_jenis_izin').html(obj);
	  //console.log(obj);
    });
    
  }

function getKabupaten(){
    var id = $('#id_provinsi').val();
    $('#id_desa').html('<option value="">Pilih</option>');
    $('#id_kecamatan').html('<option value="">Pilih</option>');
    $.post("<?php echo base_url();?>rekapdata/get_kabupaten/"+id,{},function(obj){
      $('#id_kabupaten').html(obj);
	  //console.log(obj);
    });
    
  }
  function getKecamatan(){
    $('#id_desa').html('<option value="">Pilih</option>');
    var id = $('#id_kabupaten').val();
    $.post("<?php echo base_url();?>rekapdata/get_kecamatan/"+id,{},function(obj){
      $('#id_kecamatan').html(obj);
    });
    
  }
  function getDesa(){
    var id = $('#id_kecamatan').val();
    $.post("<?php echo base_url();?>rekapdata/get_desa/"+id,{},function(obj){
      $('#id_desa').html(obj);
    });
  }
  function submit_(download)
	{
		if (download)
				$("#download").val(1);
			else
				$("#download").val(0);
			$("#laporan").submit();
		/*
		//var bulan = $("#bulan").val();
		var tahun = $("#tahun").val();
		//if (bulan!="" && tahun!=""){
		if (tahun!=""){
			if (download)
				$("#download").val(1);
			else
				$("#download").val(0);
			$("#laporan").submit();
		}
		else{
			//alert("Bulan dan tahun wajib diisi");
			alert("Tahun wajib diisi");
		}*/
	}
</script>
