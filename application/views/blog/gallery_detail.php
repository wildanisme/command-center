<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Gallery</h2>
            <p><?php echo $description;?></p>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href="<?php echo base_url().'gallery'; ?> ">Album</a></li>
              <li><a href="# "><?php echo $album_title;?></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="portfolio-page portfolio-4column">
          <ul id="portfolio-list" data-animated="fadeIn">
            <?php
              foreach ($picture as $row) {
                echo "
                  <li>
                    <img src='".base_url()."data/upload/$row->picture' alt='$row->gallery_title'>
                    <div class='portfolio-item-content'>
                      <span class='header'>$row->gallery_title</span>
                      <p class='body'></p>
                    </div>
                    <a class='lightbox' href='".base_url()."data/upload/$row->picture'><i class='more'>+</i></a>

                  </li>
                ";
              }
            ?>
          </ul>       
          
        </div>
      </div>
    </div>
    <!-- End Content -->
    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>