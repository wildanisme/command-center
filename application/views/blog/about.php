<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
	<?php $this->load->view('blog/src/header');?>
	<style type="text/css">
	.marginleft2px{
		margin-left: 2px;
	}
</style>
</head>

<body>
	<div id="container">
		
		<!-- Start Header -->
		<header class="clearfix">
			<?php $this->load->view('blog/src/top_nav');?>
		</header>

		<!-- Start Page Banner -->
		<div class="page-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>About Me</h2>
					</div>
				</div>
			</div>
		</div>
		<!-- End Page Banner -->

		<div id="content">
			<div class="container">
				<div class="row blog-post-page">
					
					<!-- Start Blog Posts -->
					<div class="col-md-9 blog-box">
						<h4 class="classic-title"><span>Who am I ?</span></h4>
						<p>
							Hello !<br>
							My name is Arif Nurdian. I am a boy who love everything about music. 
							I also like write code, specialy web programming. 
							Thanks for visiting my blog.
						</p>
					</div>
					
					
					<!--Sidebar-->
					<div class="col-md-3 sidebar right-sidebar">
						
						<!-- Search Widget -->
						<div class="widget widget-search">
							<form action="<?php echo base_url();?>blog" method='get'>
								<input type="search" name='s' value='<?php if (!empty($search)) echo $search;?>' placeholder="Enter Keywords..." />
								<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
							</form>
						</div>

						<!-- Categories Widget -->
						<div class="widget widget-categories">
							<h4>Categories <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($categories as $row) {
										echo"
										<li>
											<a href='".base_url()."blog/category/$row->category_slug'>$row->category_name</a>
										</li>";
									}
								?>
							</ul>
						</div>

						<!-- Popular Posts widget -->
						<div class="widget widget-popular-posts">
							<h4>Popular Post <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($popular as $row) {
										echo "
											<li>
											<div class='widget-content'>
												<h5><a href='".base_url()."blog/read/$row->title_slug'>$row->title</a></h5>
												<span>". date('d M Y',strtotime($row->date)) ."</span>
											</div>
											<div class='clearfix'></div>
										</li>
										";
									}
								?>
								
							</ul>
						</div>
						
						
						<!-- Tags Widget -->
						<div class="widget widget-tags">
							<h4>Tags <span class="head-line"></span></h4>
							<div class="tagcloud">
								<?php
									foreach ($tags_ as $row) {
										echo "<a href='".base_url()."blog/tag/$row->tag_slug'>$row->tag_name</a> ";
									}
								?>
							</div>
						</div>

					</div>
					<!--End sidebar-->
					
					
				</div>
			</div>
		</div>
		<!-- End Content -->
		<footer>
			<?php $this->load->view('blog/src/footer');?>
		</footer>
	</div>
</body>
</html>