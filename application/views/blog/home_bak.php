

<?php $this->load->view('blog/src/header');?>

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


        <!-- Banner
            ================================================== -->
            <div class="main-search-container"
            data-background-image="<?php echo base_url().'data/images/header/'.$header[0]->gbr_header;?>">
            <div class="main-search-inner">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Kecamatan Paseh</h2>
                            <h4>Selamat datang, mari jelajahi dan explore Bogor Parijz Van Java</h4>

                            <div class="main-search-input">

                                <div class="main-search-input-item" style="padding-left: 50px">
                                    <input type="text" id="keyword" placeholder="Apa yang ingin anda cari?" value="" />
                                </div>

                                <div class="main-search-input-item">
                                    <select data-placeholder="Pilih Kategori" id="kategori" class="chosen-select" >
                                        <option value="direktori">Wisata</option>
                                        <option value="agenda">Event</option>
                                        <option value="pemuda">Kepemudaan</option>
                                        <option value="organisasi">Organisasi</option>
                                        <option value="sarana">Sarana Prasarana</option>
                                    </select>
                                </div>
                                <button class="button"
                                onclick="search()">Explore</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <!-- Content
            ================================================== -->
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h3 class="headline centered margin-top-75">
                            Apa saja yang ada di Kabupaten Bogor?
                            <span>Mari <i>jelajahi dan explore </i></span>
                        </h3>
                    </div>

                </div>
            </div>


            <!-- Categories Carousel -->
            <div class="fullwidth-carousel-container margin-top-25">
                <div class="fullwidth-slick-carousel category-carousel">

            <?php
            $jumlah_data = count($hitung_by_kategori);
            $bagi_jumlah = floor($jumlah_data / 4);
            $sisa_bagi = $jumlah_data % 4;
            $array_half = array();
            // echo $jumlah_data.'-'.$bagi_jumlah.'-'.$sisa_bagi.'<br>';
            for($i=1; $i<=$bagi_jumlah; $i++){
                $n=($i*4)-3;
                // echo rand($n,$n+2).'<br>';
                $array_half[] = rand($n,$n+2);
            }
            if($sisa_bagi==3){
                $n=($i*4)-3;
                // echo rand($n,$n+1).'<br>';
                $array_half[] = rand($n,$n+1);
            }

            // print_r($array_half);


             foreach ($hitung_by_kategori as $key => $a) {
            ?>
            <?php if (in_array($key+1, $array_half)): ?>
                 <!-- Item -->
                    <div class="fw-carousel-item">

                        <!-- this (first) box will be hidden under 1680px resolution -->
                        <div class="category-box-container half">
                            <a href="<?= base_url()."direktori?id_kategori_listing=".$a->id_kategori_listing;?>" class="category-box"
                            data-background-image="<?php echo base_url().'data/kategori_listing/'.$a->banner;?>">
                            <div class="category-box-content">
                                <h3><?=$a->nama_kategori_listing;?></h3>
                                <span><?=$a->jml;?> Data</span>
                            </div>
                            <span class="category-box-btn">Cari</span>
                        </a>
                    </div>
            <?php elseif(in_array($key, $array_half)): ?>
                    <div class="category-box-container half">
                       <a href="<?= base_url()."direktori?id_kategori_listing=".$a->id_kategori_listing;?>" class="category-box"
                        data-background-image="<?php echo base_url().'data/kategori_listing/'.$a->banner;?>">
                        <div class="category-box-content">
                            <h3><?=$a->nama_kategori_listing;?></h3>
                            <span><?=$a->jml;?> Data</span>
                        </div>
                        <span class="category-box-btn">Cari</span>
                    </a>
                </div>
            </div>
            <?php else: ?>
            <!-- Item -->
            <div class="fw-carousel-item">
            <div class="category-box-container">
                <a href="<?= base_url()."direktori?id_kategori_listing=".$a->id_kategori_listing;?>" class="category-box"
                data-background-image="<?php echo base_url().'data/kategori_listing/'.$a->banner;?>">
                <div class="category-box-content">
                    <h3><?=$a->nama_kategori_listing;?></h3>
                    <span><?=$a->jml;?> Data</span>
                </div>
                <span class="category-box-btn">Cari</span>
            </a>
        </div>
    </div>

            <?php endif ?>
            <?php
        }
        ?>

    



</div>
</div>
<!-- Categories Carousel / End -->



<!-- Fullwidth Section -->
<section class="fullwidth margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f8f8f8">

    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h3 class="headline centered margin-bottom-45">
                    Tempat Rekomendasi ?
                    <span>berbagai tempat yang harus di kunjungi</span>
                </h3>
            </div>

            <div class="col-md-12">
                <div class="simple-slick-carousel dots-nav">

                    <?php foreach ($rekomendasi as $r) {
                        ?>

                        <!-- Listing Item -->
                        <div class="carousel-item">
                            <a href="<?php echo base_url().'direktori/detail/'.$r->slug_listing;?>" class="listing-item-container">
                                <div class="listing-item">
                                    <img src="<?php echo base_url().'data/listing/';?><?=$r->banner?>"
                                    alt="">



                                    <div class="listing-item-content">
                                        <span class="tag"><?=$r->nama_kategori_listing?></span>
                                        <h3><?=$r->nama_listing?></h3>
                                        <span><?=$r->alamat;?></span>
                                    </div>

                                </div>

                            </a>
                        </div>
                        <!-- Listing Item / End -->

                        <?php } ?>






                    </div>

                </div>

            </div>
        </div>

    </section>
    <!-- Fullwidth Section / End -->


    <!-- Testimonials -->
    <div class="section padding-top-65 padding-bottom-55">

        <div class="container">
          <div class="row">
           <div class="col-md-12">
            <h3 class="headline centered margin-bottom-45">
                Selayang Pandang
                <span>Kabupaten Bogor</span>
            </h3>
        </div>

    </div>
</div>

<!-- Categories Carousel -->
<div class="fullwidth-carousel-container margin-top-20">
  <div class="testimonial-carousel testimonials">


    <?php foreach($sambutan as $s){

      ?>

      <!-- Item -->
      <div class="fw-carousel-review">
        <div class="testimonial-box">
          <div class="testimonial-avatar">
            <img src="<?php echo base_url().'data/images/sambutan/'.$s->foto;?>" alt="">
        </div>
        <div class="testimonial-author">
            <h4><?=$s->nama;?></h4>
            <span><?=$s->jabatan;?></span>
        </div>
        <div class="testimonial" style="color: #fff"><?=$s->isi;?></div>
    </div>
</div>
<?php
}
?>



</div>
</div>
<!-- Categories Carousel / End -->

</div>
<!-- Testimonials / End -->


</section>


<!-- Parallax -->
<div class="parallax" data-background="<?php echo base_url();?>data/images/paralayang-bg.jpg" data-color="#36383e" data-color-opacity="0.6"
    data-img-width="800" data-img-height="505">

    <!-- Infobox -->
    <div class="text-content white-font">
        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-sm-8">
                    <h2>Explore Bogor</h2>
                    <p>Banyak juga agenda tahunan yang menarik di Kabupaten Bogor terkait dengan budaya, sosial dan masih banyak agenda lainya</p>
                    <a href="<?php echo base_url();?>agenda" class="button margin-top-25">Explore</a>
                </div>
            </div>

        </div>
    </div>

    <!-- Infobox / End -->

</div>
<!-- Parallax / End -->



<!-- Recent Blog Posts -->
<section class="fullwidth border-top margin-top-70 padding-top-75 padding-bottom-75"
data-background-color="#fff">
<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3 class="headline centered margin-bottom-45">
                Informasi & Berita
            </h3>
        </div>
    </div>

    <div class="row">

        <?php foreach($posts as $post){

          $tag = "";
          $tags= $post->tag;
          if ($tags!=""){
            $exp = explode(";", $tags);
            $_tag = array();
            foreach ($Qtag as $r) {
              $_tag[$r->tag_name] = $r->tag_slug;
          }

          for ($x=0; $x < (count($exp) - 1) ; $x++)
          {
              $slug = $_tag[$exp[$x]];
              $tag .= "<a href='".base_url()."berita/tag/{$slug}' >$exp[$x]</a>";
              if ($x < (count($exp) - 2)) $tag .=",";
          }
      }
      $content = substr($post->content,0,150);
      if (strlen($post->content)>150) $content .="...";
      ?>
      <!-- Blog Post Item -->
      <div class="col-md-4">
        <a href="<?php echo base_url()."berita/read/{$post->title_slug}";?>" class="blog-compact-item-container">
            <div class="blog-compact-item">
                <img src="<?php echo (!empty($post->picture)) ? base_url()."data/images/featured_image/{$post->picture}" : "data/logo/layanan.jpg";?>" alt="">
               
                <span class="blog-item-tag"><?=$post->category_name?></span>
                <div class="blog-compact-item-content">
                    <ul class="blog-post-tags">
                        <li><?=tanggal($post->date)?></li>
                    </ul>
                    <h3><?=$post->title?></h3>
                    <p><?=$content?></p>
                </div>
            </div>
        </a>
    </div>
    <!-- Blog post Item / End -->
    <?php } ?>


    <div class="col-md-12 centered-content">
        <a href="<?php echo base_url();?>berita" class="button border margin-top-10">Berita Lainya</a>
    </div>

</div>

</div>
</section>
<!-- Recent Blog Posts / End -->

<section class="fullwidth border-top margin-top-40 margin-bottom-0 padding-top-60 padding-bottom-65"
data-background-color="#ffffff">
<!-- Logo Carousel -->
<div class="container">
    <div class="row">



        <!-- Carousel -->
        <div class="col-md-12">
            <div class="logo-slick-carousel dot-navigation">
                <?php foreach($banner as $b){
                  ?>
                  <div class="item">
                    <a href="<?=$b->url;?>" target="_blank" title="<?=$b->judul;?>"><img
                        src="<?php echo base_url().'data/images/banner/'.$b->gambar;?>"></a>
                    </div>

                    <?php
                }
                ?>






            </div>
        </div>
        <!-- Carousel / End -->

    </div>
</div>
<!-- Logo Carousel / End -->
</section>

<?php $this->load->view('blog/src/footer');?>
<script>
    function search()
    {
        var s = $('#keyword').val();
        var modul = $('#kategori').val();
		window.location.href= modul+'?s='+s;
    }
</script>
</body>


</html>
