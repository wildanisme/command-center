  <?php $this->load->view('blog/src/header');?>
  



<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
	<div id="titlebar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h2><?= $title;?></h2>

					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<li><?= $title;?></li>
						</ul>
					</nav>

				</div>
			</div>
		</div>
	</div>


</div>

<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-md-12">

			<section id="not-found" class="center">
				<h2>404 <i class="fa fa-question-circle"></i></h2>
				<p>Mohon maaf, halaman yang anda cari tidak ditemukan</p>

				<!-- Search -->
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="main-search-input gray-style margin-top-50 margin-bottom-10">
								<div class="main-search-input-item" style="padding-left: 50px">
                                    <input type="text" id="keyword" placeholder="Apa yang ingin anda cari?" value="" />
                                </div>

                                <div class="main-search-input-item">
                                    <select data-placeholder="Pilih Kategori" id="kategori" class="chosen-select" >
                                        <option value="direktori">Wisata</option>
                                        <option value="agenda">Event</option>
                                        <option value="pemuda">Kepemudaan</option>
                                        <option value="organisasi">Organisasi</option>
                                        <option value="sarana">Sarana Prasarana</option>
                                    </select>
                                </div>

							<button class="button" onclick="search()">Cari</button>
						</div>
					</div>
				</div>
				<!-- Search Section / End -->


			</section>

		</div>
	</div>

</div>
<!-- Container / End -->


<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<script>
    function search()
    {
        var s = $('#keyword').val();
        var modul = $('#kategori').val();
		window.location.href= "<?= base_url();?>"+ modul+'?s='+s;
    }
</script>
</body>
</html>