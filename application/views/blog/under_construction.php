<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
  .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
}
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  <div id="container">

    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Infobox -->
    <div class="text-content white-font">
      <div class="container">

        <div class="row">
          <img src="<?php echo base_url().'asset/logo/under_construction.png';?>" class="center" style="max-width: 720px; width: 100%;">
        </div>

      </div>
    </div>

    <!-- Infobox / End -->

  </div>



  <!-- End Content -->
  <footer>
    <?php $this->load->view('blog/src/footer');?>
  </footer>
</div>
</body>
</html>