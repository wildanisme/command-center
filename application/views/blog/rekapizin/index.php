<div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Filter laporan</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <form id="laporan" class="form-horizontal form-label-left" method=post target="_blank" action="<?php echo base_url()."rekapizin/laporan/";?>">
          
         
           


           <!--
            <div class="form-group">
              <label>Bulan <i>(wajib)</i></label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="bulan" id="bulan">
                
                <option value="" selected>Pilih</option>
                <?php
                  for($bln=1;$bln<=12;$bln++){
                    echo "<option value='$bln'>$bulan[$bln]</option>";
                  }
                ?>
              </select>
            </div>
			-->
            <div class="form-group">
              <label>Tahun <i>(wajib)</i></label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="tahun" id="tahun">
                
                <option value="" selected>Pilih</option>
                <?php
                  $tahun = date('Y');
                  while($tahun > $tahun_minimal){
                    echo "<option value='$tahun'>$tahun</option>";
                    $tahun--;
                  }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label>Bulan</label>
            </div>
            <div class="form-group" >
              <select class="form-control" name="bulan" id="bulan">
                
                <option value="" selected>Pilih</option>
                <?php
                  for($bln=1;$bln<=12;$bln++){
                    echo "<option value='$bln'>$bulan[$bln]</option>";
                  }
                ?>
              </select>
            </div>

            <div class="form-group">
                    <label>Provinsi</label>
                    <select class='form-control' name='id_provinsi' id='id_provinsi' onchange='getKabupaten()'>
                      <option value='' selected>Pilih</option> 
                      <?php
                        foreach($provinsi as $row){
                          echo "<option value='$row->id_provinsi'>$row->provinsi</option>";
                        }
                        
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kabupaten/Kota</label>
                    <select class='form-control' name='id_kabupaten' id='id_kabupaten' onchange='getKecamatan()'>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class='form-control' name='id_kecamatan' id='id_kecamatan' onchange='getDesa()'>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Kelurahan/Desa</label>
                    <select class='form-control' name='id_desa' id='id_desa'>         
                    </select>
                  </div> 


                      <div class="form-group">
              <input type="hidden" id="download" name="download"/>
                          <a href='<?= base_url()."rekapizin/index";?>' class="btn btn-default">Reset</a>
                          <a onclick="submit_()"  class="btn btn-success">Tampilkan data</a>
              <a onclick="submit_(1)" class="btn btn-danger" >Download</a>
                      </div>

                    </form>
                  </div>
                </div>
              </div>

<script>
	function submit_(download)
	{
		//var bulan = $("#bulan").val();
		var tahun = $("#tahun").val();
		//if (bulan!="" && tahun!=""){
		if (tahun!=""){
			if (download)
				$("#download").val(1);
			else
				$("#download").val(0);
			$("#laporan").submit();
		}
		else{
			//alert("Bulan dan tahun wajib diisi");
			alert("Tahun wajib diisi");
		}
	}

   function getKabupaten(){
    var id = $('#id_provinsi').val();
    $('#id_desa').html('<option value="">Pilih</option>');
    $('#id_kecamatan').html('<option value="">Pilih</option>');
    $.post("<?php echo base_url();?>grafikizin/get_kabupaten/"+id,{},function(obj){
      $('#id_kabupaten').html(obj);
    });
    
  }
  function getKecamatan(){
    $('#id_desa').html('<option value="">Pilih</option>');
    var id = $('#id_kabupaten').val();
    $.post("<?php echo base_url();?>grafikizin/get_kecamatan/"+id,{},function(obj){
      $('#id_kecamatan').html(obj);
    });
    
  }
  function getDesa(){
    var id = $('#id_kecamatan').val();
    $.post("<?php echo base_url();?>grafikizin/get_desa/"+id,{},function(obj){
      $('#id_desa').html(obj);
    });
  }

</script>