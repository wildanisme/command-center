<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header_home');?>
  

</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
  ================================================== -->
  <header id="header-container" >

    <?php $this->load->view('blog/src/navigasi_atas');?>

  </header>
  <div class="clearfix"></div>
  <!-- Header Container / End -->



  <!-- Spacer -->
  <div class="margin-top-90"></div>
  <!-- Spacer / End-->

  <div class="container">
    <!-- selesai -->

    <?php if (($status->status_antrian) == 'Menunggu' ) {
    ?>
    <div class="row dashboard-box margin-top-0 margin-bottom-50 margin-right-10 margin-left-10" >
      <div class="col-md-12 dashboard-box margin-top-0 margin-bottom-50" style="height: 50px; background:#FF8000;padding-top: 10px;">
            <h3 style="text-align: center; vertical-align: middle;color: #fff">Status Antrian</h3>      
      </div>
      <div class="col-md-12" style="height: 150px;vertical-align: middle; border-bottom: 1px solid #C9C9C9;padding-top: 20%x; padding-right: 10%;padding-left: 10%" >
            <p align="center">No. Antrian Anda:</p>
            <h3 style="text-align: center; vertical-align: middle;color: #FF8000;font-size: 50px;font-weight: bold;" ><?=$status->nomer_antrian;?></h3>      
      </div>

      <div class="col-md-12" style="height: 120px;vertical-align: middle;  padding-right: 10%;padding-left: 10%" >
            <p align="center" style="margin-top: 10px">Menunggu :</p>
            <h3 style="text-align: center; vertical-align: middle;color: #FF8000;font-size: 50px;font-weight: bold;margin-bottom:10px" ><?= (!empty($sisa_antrian)) ? $sisa_antrian : "0";?></h3>   
            <p align="center">Antrian Lagi</p>   
      </div>


      </div>
      <?php
    }
    else {
    ?>
   <div class="row dashboard-box margin-top-0 margin-bottom-50 margin-right-10 margin-left-10" >
      <div class="col-md-12 dashboard-box margin-top-0 margin-bottom-50" style="height: 50px; background: #6A10C4;padding-top: 10px;">
            <h3 style="text-align: center; vertical-align: middle;color: #fff">Status Antrian</h3>      
      </div>
      <div class="col-md-12" style="height: 150px;vertical-align: middle; border-bottom: 1px solid #C9C9C9;padding-top: 20%x; padding-right: 10%;padding-left: 10%" >
            <p align="center">No. Antrian Anda:</p>
            <h3 style="text-align: center; vertical-align: middle;color: #6A10C4;font-size: 50px;font-weight: bold;" ><?=$status->nomer_antrian;?></h3>      
      </div>

      <div class="col-md-12" style="height: 120px;vertical-align: middle;  padding-right: 10%;padding-left: 10%" >
            <p align="center" style="margin-top: 10px">Status :</p>
            <h3 style="text-align: center; vertical-align: middle;color: #6A10C4;font-size: 30px;font-weight: bold;" ><?=$status->status_antrian;?></h3>   
           
      </div>


      </div>

      <?php
    }
    ?>




    </div>
  </div>




</div>
<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>