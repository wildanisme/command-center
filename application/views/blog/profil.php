<?php
$CI =& get_instance();
$CI->load->model('company_profile_model');
$CI->company_profile_model->set_identity();
$logo = $CI->company_profile_model->logo;
$telepon = $CI->company_profile_model->telepon;
$email = $CI->company_profile_model->email;
$p_nama = $CI->company_profile_model->nama;
$p_alamat = $CI->company_profile_model->alamat;
$p_logo = $CI->company_profile_model->logo;
$p_email = $CI->company_profile_model->email;
$p_facebook = $CI->company_profile_model->facebook;
$p_twitter = $CI->company_profile_model->twitter;
$p_telepon = $CI->company_profile_model->telepon;
$p_youtube = $CI->company_profile_model->youtube;
$p_gmap = $CI->company_profile_model->gmap;
$p_tentang = $CI->company_profile_model->tentang;
$p_instagram = $CI->company_profile_model->instagram;
$p_long = $CI->company_profile_model->longitude;
$p_lat = $CI->company_profile_model->latitude;

?>

  <?php $this->load->view('blog/src/header');?>
  


<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
  <!-- Header Container / End -->






<!-- Container / Start -->
<div class="container">

  <div class="row">

    <!-- Contact Details -->
    <div class="col-md-4">

      <h4 class="headline margin-bottom-30">Kontak</h4>

      <!-- Contact Details -->
      <div class="sidebar-textbox">

        <ul class="contact-details">
           <?php if(!empty($p_alamat)){ ?>
          <li><i class="icon-material-outline-account-balance"></i> <strong>Alamat:</strong> <span><?=$p_alamat?></span></li>
          <?php } ?>

          <?php if(!empty($p_telepon)){ ?>
          <li><i class="icon-feather-phone"></i> <strong>Telepon:</strong> <span><?=$p_telepon?></span></li>
          <?php } ?>

          <?php if(!empty($p_email)){ ?>
          <li><i class="icon-material-outline-email"></i> <strong>Email:</strong> <span><?=$p_email?></span></li>
          <?php } ?>

          <?php if(!empty($p_facebook)){ ?>
          <li><i class="icon-brand-facebook-f"></i> <strong>facebook:</strong> <span><?=$p_facebook?></span></li>
          <?php } ?>

          <?php if(!empty($p_twitter)){ ?>
          <li><i class="icon-feather-twitter"></i> <strong>twitter:</strong> <span><?=$p_twitter?></span></li>
          <?php } ?>        
          
          <?php if(!empty($p_instagram)){ ?>
          <li><i class="icon-brand-instagram"></i> <strong>instagram:</strong> <span><?=$p_instagram?></span></li>
          <?php } ?>

        </ul>
      </div>

    </div>

    <!-- Contact Form -->
    <div class="col-md-8">

      <div id="listing-nav" class="listing-nav-container">
        <ul class="listing-nav">
          <li><a href="#listing-overview" class="">Visi</a></li>
        </ul>
      </div>
      <div id="listing-overview" class="listing-section">

        <!-- Description -->

        <p>
          <?=$visimisi->visi;?>       </p>
     
      </div>

      <div id="listing-nav" class="listing-nav-container">
        <ul class="listing-nav">
          <li><a href="#listing-overview" class="">Misi</a></li>
        </ul>
      </div>
      <div id="listing-overview" class="listing-section">

        <!-- Description -->

        <p>
          <?=$visimisi->misi;?>  
        </p>
     
      </div>

         <div id="listing-nav" class="listing-nav-container">
        <ul class="listing-nav">
          <li><a href="#listing-overview" class="">Tujuan</a></li>
        </ul>
      </div>
      <div id="listing-overview" class="listing-section">

        <!-- Description -->

        <p>
         <?=$visimisi->tujuan;?>  
        </p>
     
      </div>


</div>
</div>



      <div class="row">

    <div class="col-xl-12">
      <div class="contact-location-info margin-bottom-50">
        <div class="contact-address">
          <ul>
            <li class="contact-address-headline">Kecamatan Paseh</li>
           
            <li>
              <div class="freelancer-socials">
                <ul>
                  
                  <li><a href="<?=$p_twitter?>" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                  <li><a href="<?=$p_facebook?>" title="facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                  <li><a href="<?=$p_youtube?>" title="youtube" data-tippy-placement="top"><i class="icon-brand-youtube"></i></a></li>
                
                </ul>
              </div>
            </li>
          </ul>

        </div>
        <div id="single-job-map-container">
          <div id="singleListingMap" data-latitude="<?=$p_lat?>" data-longitude="<?=$p_long?>" data-map-icon="im im-icon-Hamburger"></div>
          <a href="#" id="streetView">Street View</a>
        </div>
      </div>
    </div>

  </div>

    </div>
    <!-- Contact Form / End -->


  </div>



</div>
<!-- Container / End -->


<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->





<div class="clearfix"></div>
<!-- Map Container / End -->




<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMvwWO0aCn876VcbHvGHtgzBNRm8HMVEc"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>