<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Contact</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href=" <?php echo base_url().'agenda'; ?> ">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-8">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Contact Us</span></h4>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post" action="<?php echo base_url('contact/insert_contact');?>" > 
              <?php if ($this->input->get('status') == "sent"):?><div class="alert alert-info">Terima kasih, pesan anda telah terkirim.</div><?php endif;?>
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name" value="<?php if ($this->session->userdata('user_id')) echo $this->session->userdata('full_name');?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email" value="<?php if ($this->session->userdata('user_id')) echo $this->session->userdata('email');?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" class="requiredField" placeholder="Subject" name="subject" value="<?php echo set_value('subject');?>">
                </div>
                <i class="alert-warning"><?php echo form_error('subject');?></i>
              </div>

              <div class="form-group">

                <div class="controls">
                  <textarea rows="7" placeholder="Message" name="message"><?php echo set_value('message');?></textarea>
                </div>
                <i class="alert-warning"><?php echo form_error('message');?></i>
              </div>
              <?php if (!$this->session->userdata('user_id')): ?>
                <a type="button" href="<?php echo base_url('admin/login'); ?>" class="btn-system btn-large">Login</a>
                <i class="text-info">*Anda harus login terlebih dahulu.</i>
              <?php elseif ($this->session->userdata('user_id')): ?>
                <button type="submit" id="submit" class="btn-system btn-large">Send</button>
              <?php endif;?>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->

          </div>

          <div class="col-md-4">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Information</span></h4>

            <!-- Some Info -->
            <h4><?php echo $nama ?></h4>

            <!-- Divider -->
            <div class="hr1" style="margin-bottom:10px;"></div>

            <!-- Info - Icons List -->
            <ul class="icons-list">
              <li><i class="fa fa-globe">  </i> <strong>Alamat:</strong> <?php echo $alamat ?></li>
              <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong><?php echo $email ?></li>
              <li><i class="fa fa-mobile"></i> <strong>Phone:</strong> <?php echo $telepon ?></li>
            </ul>


          </div>

        </div>

      </div>
    </div>
    <!-- End content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>