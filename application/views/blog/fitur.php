<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->
<div class="clearfix"></div>
<!-- Revolution Slider -->
<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

<!-- 5.0.7 auto mode -->
  <div id="rev_slider_4_1" class="rev_slider home fullwidthabanner" style="display:none;" data-version="5.0.7">
    <ul>

      <!-- Slide  -->
      <li data-index="rs-1" data-transition="fade" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="800" data-fsslotamount="7" data-saveperformance="off">

        <!-- Background -->
        <img src="https://ptsp.sumedangkab.go.id/mpp/data/images/bg-smd.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina data-kenburns="on" data-duration="12000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="100" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0">


        <!-- Caption-->
        <div class="tp-caption centered custom-caption-2 tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
          id="slide-1-layer-2" 
         data-x="['center','center','center','center']" data-hoffset="['0']" 
          data-y="['middle','middle','middle','middle']" data-voffset="['0']" 
          data-width="['640','640', 640','420','320']"
          data-height="auto"
          data-whitespace="nowrap"
          data-transform_idle="o:1;"  
          data-transform_in="y:0;opacity:0;s:1000;e:Power2.easeOutExpo;s:400;e:Power2.easeOutExpo" 
          data-transform_out="" 
          data-mask_in="x:0px;y:[20%];s:inherit;e:inherit;" 
          data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
          data-start="1000" 
          data-responsive_offset="on">

          <!-- Caption Content -->
          <div class="R_title margin-bottom-15"
          id="slide-2-layer-3"
          data-x="['center','center','center','center']"
          data-hoffset="['0','0','0','0']"
          data-y="['middle','middle','middle','middle']"
          data-voffset="['-40','-40','-20','-80']"
          data-fontsize="['42','36', '32','36','22']"
          data-lineheight="['70','60','60','45','35']"
          data-width="['640','640', 640','420','320']"
          data-height="none" data-whitespace="normal"
          data-transform_idle="o:1;"
          data-transform_in="y:-50px;sX:2;sY:2;opacity:0;s:1000;e:Power4.easeOut;"
          data-transform_out="opacity:0;s:300;"
          data-start="600"
          data-splitin="none"
          data-splitout="none"
          data-basealign="slide"
          data-responsive_offset="off"
          data-responsive="off"
          style="z-index: 6; color: ##2caab3; letter-spacing: 0px; font-weight: 600; ">Explore Bogor</div>

          <div class="caption-text">Berbagai Fitur dibuat sebagai bentuk optimalisasi pelayanan publik dan media promosi peluang dan investasi di Kab. Bogor</div>
          <a href="#" class="button medium">Selengkapnya</a>
        </div>

      </li>

      <!-- Slide  -->
      <li data-index="rs-2" data-transition="fade" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="800" data-fsslotamount="7" data-saveperformance="off">

        <!-- Background -->
        <img src="https://ptsp.sumedangkab.go.id/mpp/data/images/bg-smd.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina data-kenburns="on" data-duration="12000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="112" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"> 

        <!-- Caption-->
        <div class="tp-caption centered custom-caption-2 tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" 
          id="slide-2-layer-2" 
          data-x="['center','center','center','center']" data-hoffset="['0']" 
          data-y="['middle','middle','middle','middle']" data-voffset="['0']" 
          data-width="['640','640', 640','420','320']"
          data-height="auto"
          data-whitespace="nowrap"
          data-transform_idle="o:1;"  
          data-transform_in="y:0;opacity:0;s:1000;e:Power2.easeOutExpo;s:400;e:Power2.easeOutExpo" 
          data-transform_out="" 
          data-mask_in="x:0px;y:[20%];s:inherit;e:inherit;" 
          data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
          data-start="1000" 
          data-responsive_offset="on">

          <!-- Caption Content -->
          <div class="R_title margin-bottom-15"
          id="slide-2-layer-3"
          data-x="['center','center','center','center']"
          data-hoffset="['0','0','0','0']"
          data-y="['middle','middle','middle','middle']"
          data-voffset="['-40','-40','-20','-80']"
          data-fontsize="['42','36', '32','36','22']"
          data-lineheight="['70','60','60','45','35']"
          data-width="['640','640', 640','420','320']"
          data-height="none" data-whitespace="normal"
          data-transform_idle="o:1;"
          data-transform_in="y:-50px;sX:2;sY:2;opacity:0;s:1000;e:Power4.easeOut;"
          data-transform_out="opacity:0;s:300;"
          data-start="600"
          data-splitin="none"
          data-splitout="none"
          data-basealign="slide"
          data-responsive_offset="off"
          data-responsive="off"
          style="z-index: 6; color: #fff; letter-spacing: 0px; font-weight: 600; ">Explore Bogor</div>

          <div class="caption-text">Berbagai Fitur dibuat sebagai bentuk optimalisasi pelayanan publik dan media promosi peluang dan investasi di Kab. Bogor</div>
          <a href="#" class="button medium">Selengkapnya</a>
        </div>

      </li>

    </ul>
    <div class="tp-static-layers"></div>

  </div>
</div>
<!-- Revolution Slider / End -->
<!-- Titlebar

<!-- Content
================================================== -->
<div class="container">
    <div class="row">
      <div class="col-md-12">
            <h3 class="headline centered margin-top-75">
                Daftar Fitur            </h3>
        </div>
        <div class="col-md-12">
            <div class="categories-boxes-container margin-top-5 margin-bottom-30">
               </a>
                                                        <a href="<?php echo base_url();?>layanan" class="category-small-box">
                        <i class="im im-icon-Post-Office"></i>
                        <h4>Pelayanan Masyarakat</h4>
                    </a>

                                                        <a href="http://xplora.id/pariwisata" class="category-small-box">
                        <i class="im im-icon-Palm-Tree"></i>
                        <h4>Pariwisata</h4>
                    </a>
                                                        <a href="http://xplora.id/hotel" class="category-small-box">
                        <i class="im im-icon-Hotel"></i>
                        <h4>Hotel</h4>
                    </a>
                                                        <a href="http://xplora.id/resto" class="category-small-box">
                        <i class="im im-icon-Plates"></i>
                        <h4>Restoran/Café</h4>
                    </a>
                                                        <a href="http://xplora.id/fasilitas-kesehatan" class="category-small-box">
                        <i class="im im-icon-Hospital"></i>
                        <h4>Fasilitas Kesehatan</h4>
                    </a>
                                                        <a href="http://xplora.id/sekolah" class="category-small-box">
                        <i class="im im-icon-University"></i>
                        <h4>Sekolah</h4>
                    </a>
                                                        <a href="http://xplora.id/kost" class="category-small-box">
                        <i class="im im-icon-Home-5"></i>
                        <h4>Lowongan Pekerjaan</h4>
                   
                                                        <a href="http://xplora.id/fasilitas-umum" class="category-small-box">
                        <i class="im im-icon-City-Hall"></i>
                        <h4>Fasilitas Umum</h4>
                    </a>
                                                        <a href="http://xplora.id/store" class="category-small-box">
                        <i class="im im-icon-Clothing-Store"></i>
                        <h4>Pusat Perbelanjaan</h4>
                    </a>
                                                        <a href="http://xplora.id/jasa" class="category-small-box">
                        <i class="im im-icon-Farmer"></i>
                        <h4>Jasa</h4>
                    </a>
                                                        <a href="http://xplora.id/event" class="category-small-box">
                        <i class="im im-icon-Bulleted-List"></i>
                        <h4>Event</h4>
                    </a>
                                                        <a href="http://xplora.id/komunitas" class="category-small-box">
                        <i class="im im-icon-Cube-Molecule2"></i>
                        <h4>Komunitas</h4>
                    </a>
                            </div>
        </div>
    </div>
</div>  

  </div>
</div>





    <!-- Popper js -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/popper.min.js"></script>


    <!-- Jquery-2.2.4 JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/jquery-2.2.4.min.js"></script>
     <!-- Bootstrap-4 Beta JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/bootstrap.min.js"></script>

    <!-- All Plugins JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?php echo base_url().'asset/list/';?>scripts/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/active.js"></script>




    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>