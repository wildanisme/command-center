<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">

</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->


<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h3><?=$izin->nama_jenisizin?></h3><span><?=$izin->nama_bidang?></span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="#">Home</a></li>
            <li>Perizinan</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>


<!-- Content
================================================== -->
<div class="container">
  <div class="row">

<!-- Info Section -->
<div class="container">

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h3 class="headline centered margin-top-50">
        Tahapan Proses Pengajuan ?
        <span class="margin-top-5">Kini tahapan  mengurus perizinan menjadi lebih mudah </span>
      </h3>
    </div>
  </div>

  <div class="row icons-container">
    <!-- Stage -->
    <div class="col-md-4">
      <div class="icon-box-2 with-line">
        <i class="im im-icon-Map2"></i>
        <h3>Pendaftaran Akun</h3>
        <p>Pemohon melakukan pendaftaran di sistem dan mengupload semua persyaratan </p>
      </div>
    </div>

    <!-- Stage -->
    <div class="col-md-4">
       <div class="icon-box-2">
        <i class="im im-icon-Checked-User"></i>
        <h3>Proses</h3>
        <p>Pendaftaran anda akan segera di proses oleh pihak PTSP, dan proses dapat di monitoring melalui sistem </p>
      </div>
    </div>

    <!-- Stage -->
    <div class="col-md-4">
      <div class="icon-box-2 with-line">
        <i class="im im-icon-Mail-withAtSign"></i>
 
        <h3>Download SK</h3>
        <p>Apabila Proses sudah selasai maka selanjutnya adalah mendownload SK Perizinan yang sudah di tandatangi secara Digital oleh kepala dinas </p>
      </div>
    </div>
  </div>
<?php if(count($sub)>0){?>
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h3 class="headline centered margin-top-50">
        Sub Jenis Izin
        <span class="margin-top-5">Berikut sub jenis izin yang terdapat pada izin <?=$izin->nama_jenisizin?> </span>
      </h3>
    </div>
  </div>

  <div class="row icons-container">
    <div class="col-md-8 col-md-offset-2">
    <ul class="listing-features checkboxes margin-top-0">
      <?php foreach($sub as $s){?>
          <li><?=$s->nama_sub_jenisizin?></li>
          <?php } ?>
    </ul>
</div>
  </div>

<?php } ?>
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h3 class="headline centered margin-top-50">
        Apa Saja Persyaratanya ?
        <span class="margin-top-5">Berikut daftar persyaratan untuk Izin Mendirikan Bangunan </span>
      </h3>
    </div>
  </div>

<div class="row">
  <div class="col-md-12">     <div class="style-1">

        <!-- Tabs Navigation -->
        <ul class="tabs-nav">
          <?php $order=1; foreach($layanan as $l){?>
          <li<?=$order==1?' class="active"':''?>><a href="#layanan<?=$l->id_layanan?>"><?=$l->nama_layanan?></a></li>
          <?php $order++; } ?>
        </ul>

        <!-- Tabs Content -->
        <div class="tabs-container">
          <?php foreach($layanan as $l){
            $file_syarat = $this->ref_persyaratan_izin_model->get_by_izin($l->id_layanan);
            ?>
          <div class="tab-content" id="layanan<?=$l->id_layanan?>">
<?php if(count($file_syarat)==0){
?>
      <div class="notification notice closeable">
        <p>
Izin ini tidak memiliki layanan <?=$l->nama_layanan?></p>
        <a class="close" href="#"></a>
      </div>
<?php
}else{ ?>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>#</th><th>Persyaratan</th><th>Keterangan</th><th>Template File</th>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach($file_syarat as $row){?>
      <tr>
        <td><?=$no?></td>
        <td><?=$row->nama_persyaratan_izin?></td>
        <td><?=$row->keterangan_persyaratan_izin?></td>
        <td><?= $row->file_persyaratan_izin=='' ? '-' : '<a href="http://'.$_SERVER['HTTP_HOST'].'/'.$row->file_persyaratan_izin.'" class="button">Download</a>' ?></td>
      </tr>
      <?php $no++; } ?>
<!-- 
      <tr><td>1</td><td>KTP</td><td>Scan file KTP</td><td>-</td></tr>
      <tr><td>2</td><td>NPWP</td><td>Scan file NPWP</td><td>-</td></tr>
       <tr><td>3</td><td>Surat pernyataan teknis bangunan</td><td>scan surat pernyataan teknis bangunan</td><td><a href="#" class="button">Download</a></td></tr> -->
    </tbody>
  </table>
  <?php } ?>
          </div>
<?php } ?>

      </div>

</div>
</div>

<div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Bagaimana Panduan Pendaftaranya ?
        <span class="margin-top-5">Berikut kami sertakan video panduannya</span>
      </h3>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
<!-- ***** Video Area Start ***** -->
    <div class="video-section">
       
                    <!-- Video Area Start -->
                    <div class="video-area" style="background-image: url(<?php echo base_url().'data/';?>video/video.jpg);">
                        <div class="video-play-btn">
                            <a   target="_blank" href="https://www.youtube.com/watch?v=-mGrfehRdiA" class="venobox_custom video_btn" data-vbtype="iframe" data-gall="gall-video" data-autoplay="true" data-vbtype="video"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>


           
        </div>
    </div>
    <!-- ***** Video Area End ***** -->
  </div>
  </div>

<?php if($izin->retribusi=='Y'){ ?>

  <div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Apakah Izin Ini Ini Kenakan Retribusi ?
        <span class="margin-top-5">Untuk Izin <?=$izin->nama_jenisizin?> di kenakan <strong style="color: #fa5b0f;">Biaya Retribusi</strong> </span>
      </h3>
    </div>
  </div>    


  <div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Kenapa Izin ini berertribusi ?
        <span class="margin-top-5">Untuk Retribusi Perizinan di atur di dalam UU No 2017</span>
        <span class="margin-top-5"><a href="<?=base_url()."data/download/Retribusi Perizinan.pdf"?>" class="button">Download</a></span>
      </h3>
    </div>
  </div>   


  <div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Bagaimana cara menghitung biaya retribusi berertribusi ?
        <span class="margin-top-5">silakan klik tombol di bawah untuk melakukan perhitungan retribusi</span>
        <span class="margin-top-5"><a href="<?=site_url('kalkulator')?>" class="button">Kalkulator Perizinan</a></span>
      </h3>
    </div>
  </div>   
<?php } ?>

  <div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Berapa lama proses pengurusan izin ?
        <span class="margin-top-5">untuk izin <?=$izin->nama_jenisizin?> lama prosesnya maksimal <?=$izin->masa_kerja?> Hari (setelah persyaratan lengkap)</span>
   
      </h3>
    </div>
  </div>   

  <div class="row">
    <div class="col-md-12 ">
      <h3 class="headline centered margin-top-50">
       Berapa lama masa berlaku izin ?
        <span class="margin-top-5">untuk izin <?=$izin->nama_jenisizin?> masa berlaku <?=$izin->masa_berlaku==0 ? 'Selamanya' : $izin->masa_berlaku.' tahun' ?>, sampai dengan ada perubahan status</span>
   
      </h3>
    </div>
  </div>


  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h3 class="headline centered margin-top-50">
         Silakan klik tombol dibawah untuk pendaftaran 
       <span class="margin-top-5"><a href="<?=site_url('pemohon/register')?>" class="button">Daftar</a></span>
      </h3>
    </div>
  </div>   






</div>
<!-- Info Section / End -->

</div>
</div>




    <!-- Popper js -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/popper.min.js"></script>


    <!-- Jquery-2.2.4 JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/jquery-2.2.4.min.js"></script>
     <!-- Bootstrap-4 Beta JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/bootstrap.min.js"></script>

    <!-- All Plugins JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?php echo base_url().'asset/list/';?>scripts/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/active.js"></script>

<script src="<?php echo base_url().'asset/list/';?>scripts/venobox.min.js"></script>




    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>