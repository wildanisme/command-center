<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
	<?php $this->load->view('blog/src/header');?>
	<style type="text/css">
	.marginleft2px{
		margin-left: 2px;
	}
</style>
</head>

<body>
	<div id="container">
		
		<!-- Start Header -->
		<header class="clearfix">
			<?php $this->load->view('blog/src/top_nav');?>
		</header>

		<!-- Start Page Banner -->
		<div class="page-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2>Berita</h2>
					</div>
			          <div class="col-md-6">
			            <ul class="breadcrumbs">
			              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
			              <li><a href=" <?php echo base_url().'berita'; ?> ">Berita</a></li>
			              <li><a href=" <?php echo base_url().'berita/read/'.$this->uri->segment(3); ?> "><?php echo $title;?></a></li>
			            </ul>
			          </div>
				</div>
			</div>
		</div>
		<!-- End Page Banner -->

		<div id="content">
			<div class="container">
				<div class="row blog-post-page">
					
					<!-- Start Blog Posts -->
					<div class="col-md-9 blog-box">
						<?php 
						$tag="";
								if ($tags!=""){
									$exp = explode(";", $tags);
									$_tag = array();
									foreach ($Qtag as $r) {
										$_tag[$r->tag_name] = $r->tag_slug;
									}
									
									for ($x=0; $x < (count($exp) - 1) ; $x++)
									{
										$slug = $_tag[$exp[$x]];
										$tag .= "<a href='".base_url()."berita/tag/$slug' >$exp[$x]</a> ";
										
									}
								}

								if ($picture!=""){
						echo"
						<!-- Start Single Post Area -->
					    <div class='blog-post gallery-post'>  
					    	<div class='post-head'>
								<a class='lightbox' title='$title' href='".base_url()."data/images/featured_image/$picture'>
									<div class='thumb-overlay'><i class='fa fa-arrows-alt'></i></div>
										<img alt='' src='".base_url()."data/images/featured_image/$picture'>
								</a>
							</div>
						</div>
						<!-- End Single Post (Gallery) -->
						";
						}
						echo"
					    
					    
					    <!-- Start Single Post Content -->
					    <div class='post-content'>
					    	<h2>$title</h2>
					    	<ul class='post-meta'>
					       		<li>By <a href='#'>$full_name</a></li>
					       		<li>". date('d M Y',strtotime($date)) ." $time</li>
					       		<li>Category : <a href='".base_url()."berita/category/$category_slug'>$category_name</a></li>
					     	</ul>

						     <div style='text-align:justify'>
						     $content
						     </div>
						    <div class='post-bottom clearfix'>
						      <div class='post-tags-list'>
						        $tag
						      </div>
<!--
						      <div class='post-share'>
						        <span>Share This Post:</span>
						        <a class='facebook' href='#'><i class='fa fa-facebook'></i></a>
						        <a class='twitter' href='#'><i class='fa fa-twitter'></i></a>
						        <a class='gplus' href='#'><i class='fa fa-google-plus'></i></a>
						      </div>
-->
					    	</div>
					    </div>
					    ";?>	
					</div>
					
					
					<!--Sidebar-->
					<div class="col-md-3 sidebar right-sidebar">
						
						<!-- Search Widget -->
						<div class="widget widget-search">
							<form action="<?php echo base_url();?>berita" method='get'>
								<input type="search" name='s' value='<?php if (!empty($search)) echo $search;?>' placeholder="Enter Keywords..." />
								<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
							</form>
						</div>

						<!-- Categories Widget -->
						<div class="widget widget-categories">
							<h4>Categories <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($categories as $row) {
										echo"
										<li>
											<a href='".base_url()."berita/category/$row->category_slug'>$row->category_name</a>
										</li>";
									}
								?>
							</ul>
						</div>

						<!-- Popular Posts widget -->
						<div class="widget widget-popular-posts">
							<h4>Popular Post <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($popular as $row) {
										echo "
											<li>
											<div class='widget-content'>
												<h5><a href='".base_url()."berita/read/$row->title_slug'>$row->title</a></h5>
												<span>". date('d M Y',strtotime($row->date)) ."</span>
											</div>
											<div class='clearfix'></div>
										</li>
										";
									}
								?>
								
							</ul>
						</div>
						
						
						<!-- Tags Widget -->
						<div class="widget widget-tags">
							<h4>Tags <span class="head-line"></span></h4>
							<div class="tagcloud">
								<?php
									foreach ($tags_ as $row) {
										echo "<a href='".base_url()."berita/tag/$row->tag_slug'>$row->tag_name</a> ";
									}
								?>
							</div>
						</div>

					</div>
					<!--End sidebar-->
					
					
				</div>
			</div>
		</div>
		<!-- End Content -->
		<footer>
			<?php $this->load->view('blog/src/footer');?>
		</footer>
	</div>
</body>
</html>