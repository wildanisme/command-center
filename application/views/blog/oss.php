<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->


<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h3>Online Single Submission</h3><span>DPMPTSP Kab. Bogor</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="#">Home</a></li>
            <li>one stop services</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>


<!-- Content
================================================== -->
<div class="container">
  <div class="row">

<!-- Info Section -->
<div class="container">

  <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <h3 class="headline centered margin-top-20">
        Apa itu Online Single Submission ?
<span class="margin-top-5">Pemohon hanya mengisi satu formulir di satu portal PSTP kemudian data akan secara otomatis terdistribusi kepada SKPD atau Tim Teknis terkait dan Lembaga lainya, karena sistem sudah terintegrasi  </span>
        <img src="<?php echo base_url().'data/';?>images/onlinesinglesubmission.png" class="margin-top-25"> 
        
      </h3>
    </div>
  </div>




</div>
<!-- Info Section / End -->

 <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <h3 class="headline centered margin-top-20">
        Selain itu sistem di lengkapi fitur tambahan
<span class="margin-top-5">untuk memudahkan pelayanan, kami sertakan fitur sebagai berikut :  </span>
      
        
      </h3>
    </div>
  </div>

<!-- Category Boxes -->
<div class="container">
  <div class="row">
    <div class="col-md-12 col-md-offset-2">
        <div class="row">
      <div class="categories-boxes-container margin-top-5 margin-bottom-30 centered">
        <div class="col-md-12 ">
        <!-- Box -->
        <a href="<?php echo base_url().'penanamanmodal';?>" class="category-small-box soon">
          <i class="im im-icon-Air-Balloon"></i>
          <h4> Penanaman Modal</h4>
        </a>

        <!-- Box -->
        <a href="<?php echo base_url().'izin';?>" class="category-small-box">
          <i class="im  im-icon-Wallet"></i>
          <h4>Perizinan Online</h4>
        </a>

        <!-- Box -->
        <a href="<?php echo base_url().'eloker';?>" class="category-small-box">
          <i class="im im-icon-User"></i>
          <h4>E-Loker</h4>
        </a>

        <!-- Box -->
        <a href="<?php echo base_url().'epromotion';?>" class="category-small-box">
          <i class="im im-icon-Map"></i>
          <h4>E-Promotion</h4>
        </a>


      </div>
   
  </div>
</div>
</div>
</div>
</div>
<!-- Category Boxes / End -->



</div>
</div>




    <!-- Popper js -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/popper.min.js"></script>


    <!-- Jquery-2.2.4 JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/jquery-2.2.4.min.js"></script>
     <!-- Bootstrap-4 Beta JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/bootstrap.min.js"></script>

    <!-- All Plugins JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?php echo base_url().'asset/list/';?>scripts/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?php echo base_url().'asset/list/';?>scripts/active.js"></script>




    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>