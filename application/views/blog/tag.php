<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
	<?php $this->load->view('blog/src/header');?>
	<style type="text/css">
	.marginleft2px{
		margin-left: 2px;
	}
</style>
</head>

<body>
	<div id="container">
		
		<!-- Start Header -->
		<header class="clearfix">
			<?php $this->load->view('blog/src/top_nav');?>
		</header>

		<!-- Start Page Banner -->
		<div class="page-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Berita</h2>
					</div>
			          <div class="col-md-6">
			            <ul class="breadcrumbs">
			              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
			              <li><a href=" <?php echo base_url().'berita'; ?> ">Berita</a></li>
			              <li><a href=" <?php echo base_url().'berita/tag/'.$this->uri->segment(3); ?> ">Tag - <?php echo $tag_name;?></a></li>
			            </ul>
			          </div>
					
				</div>
			</div>
		</div>
		<!-- End Page Banner -->

		<div id="content">
			<div class="container">
				<div class="row blog-page">
					
					<!-- Start Blog Posts -->
					<div class="col-md-9 blog-box">
						<?php
							foreach ($posts as $post) {
								echo "
										<div class='blog-post standard-post'>
									";
								if ($post->picture!="") {
									
									echo "
										<div class='post-head'>
											<a class='lightbox' title='$post->title' href='".base_url()."data/images/featured_image/$post->picture'>
												<div class='thumb-overlay'><i class='fa fa-arrows-alt'></i></div>
												<img alt='' src='".base_url()."data/images/featured_image/$post->picture'>
											</a>
										</div>
									";

								}
								$tag = "";
								$tags= $post->tag;
								if ($tags!=""){
									$exp = explode(";", $tags);
									$_tag = array();
									foreach ($Qtag as $r) {
										$_tag[$r->tag_name] = $r->tag_slug;
									}
									
									for ($x=0; $x < (count($exp) - 1) ; $x++)
									{
										$slug = $_tag[$exp[$x]];
										$tag .= "<a href='".base_url()."berita/tag/$slug' >$exp[$x]</a>";
										if ($x < (count($exp) - 2)) $tag .=",";
									}
								}
								$content = $post->content;
                                $content = preg_replace("/<img[^>]+\>/i", "(gambar) ", $content);
                                $content = preg_replace("/<table[^>]+\>/i", "(tabel) ", $content);
                                $content = preg_replace("/<div[^>]+\>/i", "", $content);
                                $content = preg_replace("/<ol[^>]+\>/i", "", $content);
                                $content = preg_replace("/<ul[^>]+\>/i", "", $content);
                                $content = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $content);
                                $content = character_limiter(auto_typography($content), 255, ' [&#8230;]');
                                $content = str_ireplace('<p>','',$content); $content=str_ireplace('</p>','',$content);
                                $content = str_ireplace('<ol>','',$content); $content=str_ireplace('</ol>','',$content);
                                $content = str_ireplace('<ul>','',$content); $content=str_ireplace('</ul>','',$content);
                                $content = str_ireplace('<li>','',$content); $content=str_ireplace('</li>','',$content);
								echo "
									<div class='post-content'>
										<div class='post-type'><i class='fa fa-picture-o'></i></div>
										<h2><a href='".base_url()."berita/read/$post->title_slug'>$post->title</a></h2>
										<ul class='post-meta'>
											<li>By <a href='#'>$post->full_name</a></li>
											<li>". date('d M Y',strtotime($post->date)) ." $post->time</li>
											<li><a href='".base_url()."berita/category/$post->category_slug' >Category : $post->category_name</a></li>
											<li>Tags : $tag</li>
										</ul>
										<p>$content</p>
										<a class='main-button' href='".base_url()."berita/read/$post->title_slug'>Read More <i class='fa fa-angle-right'></i></a>
									</div>
								</div>
								";
							}
						?>
						
						
						<!-- Start Pagination -->
						<div id="pagination">
							<?php
								$CI =& get_instance();
		                        $CI->load->library('pagination');

		                        $config['base_url'] = base_url(). 'berita/index/';
		                        $config['total_rows'] = $total_rows;
		                        $config['per_page'] = $per_page;
		                        $config['attributes'] = array('class' => 'page-num marginleft2px');
		                        $config['page_query_string']=TRUE;
		                        $CI->pagination->initialize($config);
		                        $link = $CI->pagination->create_links();
		                        $link = str_replace("<strong>", "<span class='current page-num marginleft2px' >", $link);
		                        $link = str_replace("</strong>", "</span>", $link);
		                        $link = str_replace("&lt;", "Back", $link);
		                        $link = str_replace("&gt;", "Next", $link);
		                        echo $link;
							?>
							
						</div>
						<!-- End Pagination -->

					</div>
					<!-- End Blog Posts -->
					
					
					<!--Sidebar-->
					<div class="col-md-3 sidebar right-sidebar">
						
						<!-- Search Widget -->
						<div class="widget widget-search">
							<form action="" method='get'>
								<input type="search" name='s' value='<?php if (!empty($search)) echo $search;?>' placeholder="Enter Keywords..." />
								<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
							</form>
						</div>

						<!-- Categories Widget -->
						<div class="widget widget-categories">
							<h4>Categories <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($categories as $row) {
										echo"
										<li>
											<a href='".base_url()."berita/category/$row->category_slug'>$row->category_name</a>
										</li>";
									}
								?>
							</ul>
						</div>

						<!-- Popular Posts widget -->
						<div class="widget widget-popular-posts">
							<h4>Popular Post <span class="head-line"></span></h4>
							<ul>
								<?php
									foreach ($popular as $row) {
										echo "
											<li>
											<div class='widget-content'>
												<h5><a href='".base_url()."berita/read/$row->title_slug'>$row->title</a></h5>
												<span>". date('d M Y',strtotime($row->date)) ."</span>
											</div>
											<div class='clearfix'></div>
										</li>
										";
									}
								?>
								
							</ul>
						</div>
						
						
						<!-- Tags Widget -->
						<div class="widget widget-tags">
							<h4>Tags <span class="head-line"></span></h4>
							<div class="tagcloud">
								<?php
									foreach ($tags_ as $row) {
										echo "<a href='".base_url()."berita/tag/$row->tag_slug'>$row->tag_name</a> ";
									}
								?>
							</div>
						</div>

					</div>
					<!--End sidebar-->
					
					
				</div>
			</div>
		</div>
		<!-- End Content -->
		<footer>
			<?php $this->load->view('blog/src/footer');?>
		</footer>
	</div>
</body>
</html>