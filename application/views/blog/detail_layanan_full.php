<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header_home');?>
  

</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
  ================================================== -->
  <header id="header-container" >

    <?php $this->load->view('blog/src/navigasi_atas');?>

  </header>
  <div class="clearfix"></div>
  <!-- Header Container / End -->






<!-- Titlebar
  ================================================== -->
  <div class="single-page-header freelancer-header" data-background-image="<?php echo base_url()."asset/mpp2/";?>images/single-freelancer.jpg">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="single-page-header-inner">
            <div class="left-side">
              <div class="header-image freelancer-avatar"> <div class="circle">
                <div>A</div>
              </div></div>
              <div class="header-details">
                <h3>Layanan Perizinan <span>Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu</span></h3>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- Page Content
  ================================================== -->
  <div class="container">
    <div class="row">

      <!-- Content -->
      <div class="col-xl-8 col-lg-8 content-right-offset">

        <!-- Page Content -->
        <div class="single-page-section">
          <h3 class="margin-bottom-25">Deskripsi Layanan</h3>
          <p>Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>

          <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
        </div>

        <!-- Boxed List -->
        <div class="boxed-list margin-bottom-60">
          <div class="boxed-list-headline">
            <h3><i class="icon-material-outline-business"></i>Persyaratan</h3>
          </div>

          <table class="basic-table" style="padding-top: 20px;">

            <tr>
              <th>No.</th>
              <th>Persyaratan</th>
              <th>Keterangan</th>
              <th>File</th>
            </tr>

            <tr>
              <td data-label="Column 1">1</td>
              <td data-label="Column 2">Persyaratan 1</td>
              <td data-label="Column 3">Keterangan 1</td>
              <td data-label="Column 4"><a href="#" class="button ripple-effect gray ">Download <i class="icon-feather-download "></i></a></td>
            </tr>

            <tr>
              <td data-label="Column 1">1</td>
              <td data-label="Column 2">Persyaratan 1</td>
              <td data-label="Column 3">Keterangan 1</td>
              <td data-label="Column 4"><a href="#" class="button ripple-effect gray">Download <i class="icon-feather-download "></i></a></td>
            </tr>

            <tr>
              <td data-label="Column 1">1</td>
              <td data-label="Column 2">Persyaratan 1</td>
              <td data-label="Column 3">Keterangan 1</td>
              <td data-label="Column 4"><a href="#" class="button ripple-effect gray">Download <i class="icon-feather-download "></i></a></td>
            </tr>

            <tr>
              <td data-label="Column 1">1</td>
              <td data-label="Column 2">Persyaratan 1</td>
              <td data-label="Column 3">Keterangan 1</td>
              <td data-label="Column 4"><a href="#" class="button ripple-effect gray">Download <i class="icon-feather-download "></i></a></td>
            </tr>


          </table>
          <div class="clearfix"></div>
          <!-- Pagination / End -->

        </div>
        <!-- Boxed List / End -->

        <!-- Boxed List -->
        <div class="boxed-list margin-bottom-60">
          <div class="boxed-list-headline">
            <h3><i class="icon-material-outline-business"></i> Prosedur Pelayanan</h3>
          </div>
          <div style="padding-top:20px;padding-left: 20px;padding-right: 20px;">
            <p>Etiam elit est, tincidunt non tincidunt <sup>This is sup element</sup> elit, sed do. Mauris aliquet ultricies <sub>This is sub element</sub> volutpat ipsum hendrerit sed neque sed sapien rutrum laoreet justo ut labolore magna aliqua. <del>This is deleted element</del> enim ad minim veniam. Lorem ipsum elit <mark class="color">mark element with “color” class.</mark>, consectetur adipisicing. Lorem <dfn>dfn element here</dfn> ipsum dosectetur tincidunt sit amet, <strong>strong text</strong>. Aliquam eu id lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia. Lorem ipsum dosectetur <code>Some code text</code> velit sagittis, <abbr title="The abbr tag">abbreviation</abbr> elit est <mark>highlighted text by using “mark” tag</mark>.</p>
          </div>
        </ul>
      </div>
      <!-- Boxed List / End -->

    </div>
    

    <!-- Sidebar -->
    <div class="col-xl-4 col-lg-4">
      <div class="sidebar-container">

        <div class="dashboard-box margin-top-0 margin-bottom-50">
          <div class="headline">
            <h3><i class="icon-material-outline-access-time"></i> Jam Buka Layanan</h3>
          </div>
          <div class="content" style="padding:20px;">
            <ul class="list-1">
              <li>
                <div class="row">
                  <div class="col-md-7">
                    Senin
                  </div>
                  <div class="col-md-5">
                    (08:00 - 15:00)
                  </div>
                </div>
             </li>

             <li>
                <div class="row">
                  <div class="col-md-7">
                    Selasa
                  </div>
                  <div class="col-md-5">
                    (08:00 - 15:00)
                  </div>
                </div>
             </li>

             <li>
                <div class="row">
                  <div class="col-md-7">
                    Rabu
                  </div>
                  <div class="col-md-5">
                    (08:00 - 15:00)
                  </div>
                </div>
             </li>

             <li>
                <div class="row">
                  <div class="col-md-7">
                    Kamis
                  </div>
                  <div class="col-md-5">
                    (08:00 - 15:00)
                  </div>
                </div>
             </li>

             <li>
                <div class="row">
                  <div class="col-md-7">
                    Jumat
                  </div>
                  <div class="col-md-5">
                    (08:00 - 15:00)
                  </div>
                </div>
             </li>

             
             

              
            </ul>
          </div>

        </div>
        
        

        <!-- Button -->
        <a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50" style="text-align: center;vertical-align: middle; font-size: 14px;">Untuk Mendaftar Layanan Secara Online Silakan Install Aplikasi Android MPP Kab. Bogor <br>
        <img src="<?php echo base_url()."data/logo/playstore.png";?>" width="200px" alt=""></a>

     

      </div>
    </div>

  </div>
</div>


<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>