
  <?php $this->load->view('blog/src/header');?>
  





  <div class="clearfix"></div>
  <!-- Header Container / End -->





<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="images/single-job.jpg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="single-page-header-inner">
          <div class="left-side">
            <div class="header-image"><a href="single-company-profile.html"><img src="images/company-logo-03a.png" alt=""></a></div>
            <div class="header-details">
              <h3><?=$detail->nama_organisasi?></h3>
              <h5><?=$detail->nama_kategori_organisasi?></h5>
           
            </div>
          </div>
      
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Page Content
================================================== -->
<div class="container">
  <div class="row">
    
    <!-- Content -->
    <div class="col-xl-8 col-lg-8 content-right-offset">
      
      <!-- Page Content -->
      <div class="single-page-section">
        <h3 class="margin-bottom-25">Deskripsi</h3>
        <?=$detail->deskripsi?>
      </div>

    
      
      

    </div>

    <!-- Sidebar -->
    <div class="col-xl-4 col-lg-4">
      <div class="sidebar-container">

        
          
        <!-- Sidebar Widget -->
        <div class="sidebar-widget">
          <div class="job-overview">
            <div class="job-overview-headline">Informasi</div>
            <div class="job-overview-inner">
              <ul>
                  <?php if(!empty($detail->alamat)){ ?>
                <li>
                  <i class="icon-material-outline-location-on"></i>
                  <span>Alamat</span>
                  <h5><?=$detail->alamat?></h5>
                </li>

                 <?php } ?>

                 <?php if(!empty($detail->telepon)){ ?>

                <li>
                  <i class="icon-material-outline-business-center"></i>
                  <span>Telepon</span>
                  <h5><?=$detail->telepon?></h5>
                </li>

                  <?php } ?>
                   <?php if(!empty($detail->email)){ ?>
                <li>
                  <i class="icon-material-outline-local-atm"></i>
                  <span>Email</span>
                  <h5><?=$detail->email?></h5>
                </li>

                <?php } ?>
                 <?php if(!empty($detail->facebook)){ ?>
                <li>
                  <i class="icon-material-outline-access-time"></i>
                  <span>Facebook</span>
                  <h5><?=$detail->facebook?></h5>
                </li>

                 <?php } ?>

                  <?php if(!empty($detail->twitter)){ ?>
                <li>
                  <i class="icon-material-outline-access-time"></i>
                  <span>Twitter</span>
                  <h5><?=$detail->twitter?></h5>
                </li>

                 <?php } ?>

                   <?php if(!empty($detail->instagram)){ ?>
                <li>
                  <i class="icon-material-outline-access-time"></i>
                  <span>Instagram</span>
                  <h5><?=$detail->instagram?></h5>
                </li>

                 <?php } ?>



              </ul>
            </div>
          </div>
        </div>

  

      </div>
    </div>


    

    

  </div>
</div>

<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->

<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMvwWO0aCn876VcbHvGHtgzBNRm8HMVEc"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>