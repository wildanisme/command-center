
<?php
$CI =& get_instance();
$CI->load->model('company_profile_model');

$CI->company_profile_model->set_identity();
$logo = $CI->company_profile_model->logo;
$telepon = $CI->company_profile_model->telepon;
$email = $CI->company_profile_model->email;
$p_nama = $CI->company_profile_model->nama;
$p_alamat = $CI->company_profile_model->alamat;
$p_logo = $CI->company_profile_model->logo;
$p_email = $CI->company_profile_model->email;
$p_facebook = $CI->company_profile_model->facebook;
$p_twitter = $CI->company_profile_model->twitter;
$p_telepon = $CI->company_profile_model->telepon;
$p_youtube = $CI->company_profile_model->youtube;
$p_gmap = $CI->company_profile_model->gmap;
$p_tentang = $CI->company_profile_model->tentang;
$p_instagram = $CI->company_profile_model->instagram;

$CI->load->model('alamat_model');

$desa = $CI->alamat_model->get_desa();

?>
<!-- Header -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<div class="container">

					<a class="navbar-brand" href="index.html">
						<img src="<?php echo base_url().'asset/umkm/';?>images/logo.png" alt="">
					</a>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
							<li><a class="active"  href="<?php echo base_url();?>home">Beranda</a></li>
							<li><a href="<?php echo base_url();?>umkm">Daftar UMKM</a></li>
							<li><a href="<?php echo base_url();?>event"> Event</a></li>
							<li><a href="<?php echo base_url();?>layanan"> Layanan Kami</a></li>
							<li><a href="<?php echo base_url();?>profil"> Tentang Kami</a></li>
							<li><a href="<?php echo base_url();?>berita"> Artikel</a></li>
						
							
							
						</ul>
						
						<a href="add-listing.html" class="add-list-btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> Daftar</a>
					</div>
				</div>
			</nav>