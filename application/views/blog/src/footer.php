<footer class="footer">
            <div class="container">

                <div class="footer__up-part">
                    <div class="row">
                        <div class="col-md-4">

                            <div class="footer__widget text-widget">
                                <img src="<?php echo base_url().'asset/umkm/';?>images/logo-black.png" alt="triptip-logo">
                                <p class="footer__widget-description">
                                    Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit, sed diam nonu mmy nibh euismod tincidunt ut laoreet dolore magna aliquam erat. 
                                </p>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="footer__widget subscribe-widget">
                                <h2 class="footer__widget-title">
                                    Subscribe
                                </h2>
                                <p class="footer__widget-description">
                                    Be notified about new locations
                                </p>
                                <form class="footer__subscribe-form">
                                    <input class="footer__subscribe-input" type="text" name="email-sub" id="email-sub" placeholder="Enter your Email" />
                                    <button class="footer__subscribe-button" type="submit">
                                        <i class="la la-arrow-circle-o-right" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="footer__widget text-widget">
                                <h2 class="footer__widget-title">
                                    Contact Info 
                                </h2>
                                <p class="footer__widget-description">
                                    1000 5th Ave to Central Park, New York <br>
                                    +1 246-345-0695 <br>
                                    info@example.com
                                </p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="footer__down-part">
                    <div class="row">
                        <div class="col-md-7">
                            <p class="footer__copyright">
                                © Copyright 2020 - All Rights Reserved
                            </p>
                        </div>
                        <div class="col-md-5">
                            <ul class="footer__social-list">
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </footer>
        <!-- End footer -->

    </div>
    <!-- End Container -->
    
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.migrate.js"></script>
<!-- Google API -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw&libraries=places&callback=initAutocomplete"></script>
    <!--build:js js/triptip-plugins.min.js -->
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/select2.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.imagesloaded.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.isotope.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/retina-1.1.0.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.appear.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/infobox.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/markerclusterer.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/maps.js"></script>
    <!-- endbuild -->
    <script src="<?php echo base_url().'asset/umkm/';?>js/popper.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/jquery.countTo.js"></script>
    <script src="<?php echo base_url().'asset/umkm/';?>js/script.js"></script>






<!-- Sweet-Alert  -->
<script src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>


<!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
<link href="<?php echo base_url().'asset/wisata/';?>css/plugins/datedropper.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url().'asset/wisata/';?>scripts/datedropper.js"></script>

<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
<script src="<?php echo base_url().'asset/wisata/';?>scripts/timedropper.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'asset/wisata/';?>css/plugins/timedropper.css">


<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
  Snackbar.show({
    text: 'Your status has been changed!',
    pos: 'bottom-center',
    showAction: false,
    actionText: "Dismiss",
    duration: 3000,
    textColor: '#fff',
    backgroundColor: '#383838'
  }); 
}); 
</script>


<!-- Google Autocomplete -->
<script>
  function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
  }

  // Autocomplete adjustment for homepage
  if ($('.intro-banner-search-form')[0]) {
      setTimeout(function(){ 
          $(".pac-container").prependTo(".intro-search-field.with-autocomplete");
      }, 300);
  }

</script>



<script>

    
  <?php 
  
  if(!empty($message_success)){
    echo '
      swal("Pesan!", "'.$message_success.'", "success");
      ';
  }
  if(!empty($message_error)){
    echo '
      swal("Pesan!", "'.$message_error.'", "error");
      ';
  }
  ?>
</script>



</body>
</html>