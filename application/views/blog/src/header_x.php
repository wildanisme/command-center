<?php
$CI =& get_instance();
$CI->load->model('company_profile_model');
$CI->company_profile_model->set_identity();
$CI->load->model('listing_model');
$hitung_by_kategori= $CI->listing_model->hitung_by_kategori();

$logo = $CI->company_profile_model->logo;


?>
<!-- Basic -->
<!DOCTYPE html>
	<head>

<!-- Basic Page Needs
================================================== -->
<title><?php if (!empty($title)) echo $title;?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



<!-- CSS
================================================== -->


<link rel="stylesheet" href="<?php echo base_url().'asset/wisata/';?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url().'asset/wisata/';?>css/colors/blue.css" id="colors">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>


					<style>
					//
.modal-window {
  position: fixed;
  background-color: rgba(200, 200, 200, 0.75);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}

.modal-window:target {
  opacity: 1;
  pointer-events: auto;
}

.modal-window>div {
  width: 400px;
  position: relative;
  margin: 10% auto;
  padding: 2rem;
  background: #fff;
  color: #444;
}

.modal-window header {
  font-weight: bold;
}

.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
}

.modal-close:hover {
  color: #000;
}

.modal-window h1 {
  font-size: 150%;
  margin: 0 0 15px;
}
 </style>

<link rel="stylesheet" href="<?php echo base_url();?>/asset/pixel/plugins/bower_components/dropify/dist/css/dropify.min.css">

<!--alerts CSS -->
<link href="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
	
<!-- gallery-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/gallery/css/animated-masonry-gallery.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/fancybox/ekko-lightbox.min.css" />

<!-- Popup CSS -->
<link href="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
================================================== -->
<header id="header-container">

	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo" style="margin-top: -10px;">
					<a href="<?php echo base_url();?>home"><img src="<?php echo base_url()."data/logo/smd-mpp.png";?>" alt=""></a>
				</div>

				<!-- Mobile Navigation -->
				<div class="menu-responsive">
					<i class="fa fa-reorder menu-trigger"></i>
				</div>

				<!-- Main Navigation -->
				<nav id="navigation" class="style-1">
					<ul id="responsive">

					
					

					

						<li><a href="<?php echo base_url();?>home"  class="<?= (!empty($active_menu) && $active_menu=="home" ) ? "current" : "" ;?>" >Beranda</a>
            </li>
            <li><a href="#" class="<?= (!empty($active_menu) && $active_menu=="layanan" ) ? "current" : "" ;?>">Layanan </a>
              <ul>
			
			<li><a href="<?php echo base_url();?>layanan"  class="<?= (!empty($active_menu) && $active_menu=="layanan" ) ? "current" : "" ;?>" >Daftar Layanan</a>
			<li><a href="<?php echo base_url();?>instansi"  class="<?= (!empty($active_menu) && $active_menu=="instansi" ) ? "current" : "" ;?>" >Daftar Instansi</a>
            </li>
			
            </ul>
            </li>

            <li><a href="#" class="<?= (!empty($active_menu) && $active_menu=="direktori" ) ? "current" : "" ;?>">Direktori</a>
              <ul>
              <?php foreach ($hitung_by_kategori as $a) {
            ?>
            <li><a href="<?= base_url()."direktori?id_kategori_listing=".$a->id_kategori_listing;?>"><?=$a->nama_kategori_listing;?></a></li>
            <?php }
            ?>
                <li><a href="<?php echo base_url();?>direktori" class="<?= (!empty($active_menu) && $active_menu=="direktori" ) ? "current" : "" ;?>" >Wisata</a></li>
                <li><a href="<?php echo base_url();?>pemuda" class="<?= (!empty($active_menu) && $active_menu=="direktori" ) ? "current" : "" ;?>">Kepemudaan</a></li>
                <li><a href="<?php echo base_url();?>organisasi" class="<?= (!empty($active_menu) && $active_menu=="direktori" ) ? "current" : "" ;?>">Organisasi</a></li>
                <li><a href="<?php echo base_url();?>sarana" class="<?= (!empty($active_menu) && $active_menu=="direktori" ) ? "current" : "" ;?>">Sarana Prasarana</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url();?>agenda"  class="<?= (!empty($active_menu) && $active_menu=="agenda" ) ? "current" : "" ;?>" >Event</a>
			</li>
			
			<li><a href="<?php echo base_url();?>profil"  class="<?= (!empty($active_menu) && $active_menu=="profil" ) ? "current" : "" ;?>" >Profil</a>
			</li>


            <li><a href="#" class="<?= (!empty($active_menu) && $active_menu=="publikasi" ) ? "current" : "" ;?>">Publikasi</a>
              <ul>
			
			<li><a href="<?php echo base_url();?>berita"  class="<?= (!empty($active_menu) && $active_menu=="publikasi" ) ? "current" : "" ;?>" >Berita</a>
			<li><a href="<?php echo base_url();?>video"  class="<?= (!empty($active_menu) && $active_menu=="video" ) ? "current" : "" ;?>" >Video</a>
            </li>
			
            </ul>
            </li>

			

			

				

								

						

					
						
					</ul>
				</nav>
				<div class="clearfix"></div>
				<!-- Main Navigation / End -->
				
			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side">
				<div class="header-widget">	
						<!-- User Menu -->
					<?php if(!empty($this->session->userdata('user_id'))) {?>
					<div class="user-menu">
						<div class="user-name"><span><img src="<?=base_url()."/data/user_picture/".$this->session->userdata("user_picture");?>" alt=""></span><?= $this->session->userdata('full_name') ;?></div>
						<ul>
							<li><a href="<?php echo base_url();?>cityzen"><i class="sl sl-icon-settings"></i> Dashboard</a></li>
							<li><a href="<?php echo base_url();?>cityzen/akun"><i class="sl sl-icon-user"></i> My Profile</a></li>
							<li><a href="<?php echo base_url();?>logout"><i class="sl sl-icon-power"></i> Logout</a></li>
						</ul>
					</div>
					<?php }
					else{ ?>
					<a href="<?php echo base_url();?>cityzen/access" class="button border with-icon"><i class="sl sl-icon-login"></i> Masuk / Daftar</a>
					<?php } ?>

				</div>
			</div>

		

			
		</div>
	</div>
	<!-- Header / End -->

</header>
	<?php if(empty($this->session->userdata('user_id'))) {?>
	<!-- Sign In Popup -->
			<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

				<div class="small-dialog-header">
					<h3>Masuk</h3>
				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">

					<ul class="tabs-nav">
						<li class=""><a href="#tab1">Masuk</a></li>
						<li><a href="#tab2">Daftar</a></li>
					</ul>

					<div class="tabs-container alt">

						<!-- Login -->
						<div class="tab-content" id="tab1" style="display: none;">
							
							
								<div class="notification warning" style='display:none'>
									
								</div>
								
								<p class="form-row form-row-wide">
									<label for="login_username">NIK:
										<i class="im im-icon-ID-Card"></i>
										<input type="text" class="input-text" name="login_username" id="login_username" value="" />
									</label>
									<label style="color:red" class="error_message" id="error_login_username"></label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password">Password:
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" name="login_password" id="login_password"/>
									</label>
									<label style="color:red" class="error_message" id="error_login_password"></label>
									<span class="lost_password">
										<a href="<?php echo base_url();?>forget-password" >Lupa Password?</a>
									</span>
								</p>

								<div class="form-row">
									<input type="submit" class="button border margin-top-5" onclick="login()" name="login" value="Masuk" />
								</div>
							</div>

						

						<!-- Register -->
						<div class="tab-content" id="tab2" style="display: none;">

							
								
							<p class="form-row form-row-wide">
								<label for="reg_username">NIK:
									<i class="im im-icon-ID-Card"></i>
									<input type="text" class="input-text" name="reg_username" id="reg_username" value="" />
								</label>
								<label style="color:red" class="error_message" id="error_reg_username"></label>
							</p>
							
							<p class="form-row form-row-wide">
								<label for="reg_full_name">Nama:
									<i class="im im-icon-Male"></i>
									<input type="text" class="input-text" name="reg_full_name" id="reg_full_name" value="" />
								</label>
								<label style="color:red" class="error_message" id="error_reg_full_name"></label>
							</p>
								
							<p class="form-row form-row-wide">
								<label for="reg_email">Alamat Email:
									<i class="im im-icon-Mail"></i>
									<input type="text" class="input-text" name="reg_email" id="reg_email" value="" />
								</label>
								<label style="color:red" class="error_message" id="error_reg_email"></label>
							</p>

							<p class="form-row form-row-wide">
								<label for="reg_password">Password:
									<i class="im im-icon-Lock-2"></i>
									<input class="input-text" type="password" name="reg_password" id="reg_password"/>
								</label>
								<label style="color:red" class="error_message" id="error_reg_password"></label>
							</p>

							<p class="form-row form-row-wide">
								<label for="reg_password_confirmation">Tulis Ulang Password:
									<i class="im im-icon-Lock-2"></i>
									<input class="input-text" type="password" name="reg_password_confirmation" id="reg_password_confirmation"/>
								</label>
								<label style="color:red" class="error_message" id="error_reg_password_confirmation"></label>
							</p>

							<input type="submit" class="button border fw margin-top-10" name="register" value="Daftar" onclick="register()" />
	
							
						</div>


						

					</div>
				</div>
			</div>
			<!-- Sign In Popup / End -->
			
			<script>
				var csrf_token = "<?= $this->security->get_csrf_hash();?>";
				function register()
				{
					
					var username = $("#reg_username").val();
					var full_name = $("#reg_full_name").val();
					var email = $("#reg_email").val();
					var password = $("#reg_password").val();
					var password_confirmation = $("#reg_password_confirmation").val();
					
					$(".error_message").html("");
					
					
					$.post("<?= base_url();?>/auth/register",
						{
							'username'	: username,
							'full_name'	: full_name,
							'email'		: email,
							'password'	: password,
							'password_confirmation'	: password_confirmation,
							'<?= $this->security->get_csrf_token_name() ;?>' : csrf_token,
						},
						function(response){
							//console.log(response);
							var result = JSON.parse(response);
							var error = result.error;
							if(!error){
								//window.location = "<?php echo base_url().$this->uri->uri_string();?>";
								window.location = "<?php echo base_url()."cityzen";?>";
								
							}
							else{
								var error_message = result.error_message;
								console.log(error_message);
								for(var key in error_message)
								{
									var message = error_message[key];
									$("#error_reg_"+key).html(message);
								}
								csrf_token = result.csrf_token;
								//console.log(result);
							}
							
						}
					);
				}
				
				function login()
				{
					
					
					var username = $("#login_username").val();
					var password = $("#login_password").val();
					
					
					$(".error_message").html("");
					$(".notification").hide();
					
					$.post("<?= base_url();?>/auth/login",
						{
							'username'		: username,
							'password'		: password,
							'<?= $this->security->get_csrf_token_name() ;?>' : csrf_token,
						},
						function(response){
							console.log(response);
							var result = JSON.parse(response);
							var error = result.error;
							if(!error){
								//window.location = "<?php echo base_url().$this->uri->uri_string();?>";
								window.location = "<?php echo base_url();?>cityzen";
							}
							else{
								var error_message = result.error_message;
								for(var key in error_message)
								{
									
									
									var message = error_message[key];
									if(key=="notification"){
										$(".notification").html("<p>"+message+"</p>");
										$(".notification").show();
										console.log(message);
									}
									else{
										$("#error_login_"+key).html(message);
									}
									
								}
								

								csrf_token = result.csrf_token;
								
								
								//console.log(result);
							}
							
						}
					);
				}
			</script>
			
	<?php } ?>