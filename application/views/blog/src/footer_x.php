<?php
$CI =& get_instance();
$CI->load->model('company_profile_model');
$CI->company_profile_model->set_identity();
$logo = $CI->company_profile_model->logo;
$telepon = $CI->company_profile_model->telepon;
$email = $CI->company_profile_model->email;
$p_nama = $CI->company_profile_model->nama;
$p_alamat = $CI->company_profile_model->alamat;
$p_logo = $CI->company_profile_model->logo;
$p_email = $CI->company_profile_model->email;
$p_facebook = $CI->company_profile_model->facebook;
$p_twitter = $CI->company_profile_model->twitter;
$p_telepon = $CI->company_profile_model->telepon;
$p_youtube = $CI->company_profile_model->youtube;
$p_gmap = $CI->company_profile_model->gmap;
$p_tentang = $CI->company_profile_model->tentang;
$p_instagram = $CI->company_profile_model->instagram;

?>

<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer">
  <!-- Main -->
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-sm-6">
        <!-- <img class="footer-logo" src="<?php echo base_url()."data/logo/disparbudparpora.png";?>" alt=""> -->
        <br>
        <p>Website ini bertujuan untuk mempromosikan potensi yang ada di Kab. Bogor</p>
      </div>

      <div class="col-md-4 col-sm-6 ">
        <h4> Links</h4>
        <ul class="footer-links">
           <li><a href="<?php echo base_url();?>">Beranda</a></li>
          <li><a href="<?php echo base_url();?>direktori">Wisata</a></li>
          <li><a href="<?php echo base_url();?>pemuda">Kepemudaan</a></li>
          <li><a href="<?php echo base_url();?>organisasi">Organisasi</a></li>
          <li><a href="<?php echo base_url();?>sarana">Sarana Prasarana</a></li>
        </ul>

        <ul class="footer-links">
        <li><a href="<?php echo base_url();?>agenda">Event </a></li>
        <li><a href="<?php echo base_url();?>berita">Berita </a></li>
        <li><a href="<?php echo base_url();?>profil">Profil </a></li>
        </ul>
        <div class="clearfix"></div>
      </div>    

      <div class="col-md-3  col-sm-12">
        <h4>Kontak Kami</h4>
        <div class="text-widget">
          <span><?=$p_alamat;?></span> <br>
          Telepon: <span><?=$p_telepon;?> </span><br>
          E-Mail:<span> <a href="#"><?=$p_email;?></a> </span><br>
        </div>

        <ul class="social-icons margin-top-20">
          <li><a class="facebook" href="<?=$p_facebook;?>"><i class="icon-facebook"></i></a></li>
          <li><a class="twitter" href="<?=$p_twitter;?>"><i class="icon-twitter"></i></a></li>
          <li><a class="instagram" href="<?=$p_instagram;?>"><i class="icon-instagram"></i></a></li>
         
        </ul>

      </div>

    </div>
    
    <!-- Copyright -->
    <div class="row">
      <div class="col-md-12">
        <div class="copyrights">© 2019 Kab. Bogor</div>
      </div>
    </div>

  </div>

</div>
<!-- Footer / End -->


<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>


</div>
<!-- Wrapper / End -->



<!-- Scripts
================================================== -->
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/jpanelmenu.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/chosen.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/counterup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/tooltips.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/custom.js"></script>






<!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
<link href="<?php echo base_url().'asset/wisata/';?>css/plugins/datedropper.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url().'asset/wisata/';?>scripts/datedropper.js"></script>

<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
<script src="<?php echo base_url().'asset/wisata/';?>scripts/timedropper.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'asset/wisata/';?>css/plugins/timedropper.css">


<!-- Gallery-->
  <script type="text/javascript" src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/gallery/js/animated-masonry-gallery.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/gallery/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/fancybox/ekko-lightbox.min.js"></script>

<script>
  $('.tanggal').dateDropper();
  $('.time').timeDropper({
            format : 'HH:mm',
            setCurrentTime: false,
            meridians: true,
            primaryColor: "#37b6bd",
            borderColor: "#37b6bd",
            minutesInterval: '15'
        });  
</script> 

<!-- Style Switcher
================================================== -->
<script src="<?php echo base_url().'asset/wisata/';?>scripts/switcher.js"></script>
<!-- jQuery file upload -->
<script src="<?php echo base_url();?>/asset/pixel/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
    <script>
        $(document).ready(function() {

          
          

          $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
              tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
              titleSrc: function(item) {
                //return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
              }
            }
          });

	
        

        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });

    
</script>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw&sensor=true&callback=loadMap&libraries=places"></script>
  <script src="<?=base_url();?>/asset/pixel/plugins/bower_components/gmaps/gmaps.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
  
<!-- Sweet-Alert  -->
<script src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()."asset/pixel/" ;?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

<script>
  <?php 
  if(!empty($message_success)){
    echo '
      swal("Pesan!", "'.$message_success.'", "success");
      ';
  }
  if(!empty($message_error)){
    echo '
      swal("Pesan!", "'.$message_error.'", "error");
      ';
  }
  ?>
</script>