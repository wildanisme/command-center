<?php
$CI =& get_instance();
$CI->load->model('company_profile_model');
$CI->company_profile_model->set_identity();
$CI->load->model('listing_model');
$hitung_by_kategori= $CI->listing_model->hitung_by_kategori();

$logo = $CI->company_profile_model->logo;


?>
<header id="header-container">

  <!-- Header -->
  <div id="header">
    <div class="container">
      
      <!-- Left Side Content -->
      <div class="left-side">
        
        <!-- Logo -->
        <div id="logo">
          <a href="<?php echo base_url();?>"><img src="<?php echo base_url()."data/logo/smd-mpp.png";?>"></a>
        </div>

        <!-- Mobile Navigation -->
        <div class="menu-responsive">
          <i class="fa fa-reorder menu-trigger"></i>
        </div>

        <!-- Main Navigation -->
        <nav id="navigation" class="style-1">
          <ul id="responsive">

          <li><a href="<?php echo base_url();?>profil"  class="<?= (!empty($active_menu) && $active_menu=="Berita" ) ? "current" : "" ;?>" >Beranda</a>
            </li>


            <li><a href="#">Direktori</a>
              <ul>
              <?php foreach ($shitung_by_kategori as $a) {
            ?>
            <li><a href="<?= base_url()."direktori?id_kategori_listing=".$a->id_kategori_listing;?>"><?=$a->nama_kategori_listing;?></a></li>
            <?php }
            ?>

                <li><a href="<?php echo base_url();?>direktori">Wisata</a></li>
                <li><a href="<?php echo base_url();?>pemuda">Kepemudaan</a></li>
                <li><a href="<?php echo base_url();?>organisasi">Organisasi</a></li>
                <li><a href="<?php echo base_url();?>sarana">Sarana Prasarana</a></li>
              </ul>
            </li>
            <li><a href="<?php echo base_url();?>profil"  class="<?= (!empty($active_menu) && $active_menu=="agenda" ) ? "current" : "" ;?>" >Agenda</a>
            </li>

            <li><a href="<?php echo base_url();?>profil"  class="<?= (!empty($active_menu) && $active_menu=="Berita" ) ? "current" : "" ;?>" >Profil Kecamatan</a>
            </li>

            <li><a href="<?php echo base_url();?>berita"  class="<?= (!empty($active_menu) && $active_menu=="Berita" ) ? "current" : "" ;?>" >Berita</a>
            </li>

            
            
          </ul>
        </nav>
        <div class="clearfix"></div>
        <!-- Main Navigation / End -->
        
      </div>
      <!-- Left Side Content / End -->


      <!-- Right Side Content / End -->
      <div class="right-side">
        <div class="header-widget">
           <a href="#sign-in-dialog" class="button border with-icon">Sign In<i class="sl sl-icon-plus"></i></a>
        </div>
      </div>
      <!-- Right Side Content / End -->

      <!-- Sign In Popup -->
      <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

        <div class="small-dialog-header">
          <h3>Sign In</h3>
        </div>

        <!--Tabs -->
        <div class="sign-in-form style-1">

          <ul class="tabs-nav">
            <li class=""><a href="#tab1">Log In</a></li>
            <li><a href="#tab2">Register</a></li>
          </ul>
  <div><p>
  <h3>dasdassd</h3>
  </p></div>
          <div class="tabs-container alt">

            <!-- Login -->
            <div class="tab-content" id="tab1" style="display: none;">
              <form method="post" class="login">

                <p class="form-row form-row-wide">
                  <label for="username">Username:
                    <i class="im im-icon-Male"></i>
                    <input type="text" class="input-text" name="username" id="username" value="" />
                  </label>
                </p>

                <p class="form-row form-row-wide">
                  <label for="password">Password:
                    <i class="im im-icon-Lock-2"></i>
                    <input class="input-text" type="password" name="password" id="password"/>
                  </label>
                  <span class="lost_password">
                    <a href="#" >Lost Your Password?</a>
                  </span>
                </p>

                <div class="form-row">
                  <input type="submit" class="button border margin-top-5" name="login" value="Login" />
                  <div class="checkboxes margin-top-10">
                    <input id="remember-me" type="checkbox" name="check">
                    <label for="remember-me">Remember Me</label>
                  </div>
                </div>
                
              </form>
            </div>

            <!-- Register -->
            <div class="tab-content" id="tab2" style="display: none;">

              <form method="post" class="register">
                
              <p class="form-row form-row-wide">
                <label for="username2">Username:
                  <i class="im im-icon-Male"></i>
                  <input type="text" class="input-text" name="username" id="username2" value="" />
                </label>
              </p>
                
              <p class="form-row form-row-wide">
                <label for="email2">Email Address:
                  <i class="im im-icon-Mail"></i>
                  <input type="text" class="input-text" name="email" id="email2" value="" />
                </label>
              </p>

              <p class="form-row form-row-wide">
                <label for="password1">Password:
                  <i class="im im-icon-Lock-2"></i>
                  <input class="input-text" type="password" name="password1" id="password1"/>
                </label>
              </p>

              <p class="form-row form-row-wide">
                <label for="password2">Repeat Password:
                  <i class="im im-icon-Lock-2"></i>
                  <input class="input-text" type="password" name="password2" id="password2"/>
                </label>
              </p>

              <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />
  
              </form>
            </div>

          </div>
        </div>
      </div>
      <!-- Sign In Popup / End -->

    </div>
  </div>
  <!-- Header / End -->

</header>