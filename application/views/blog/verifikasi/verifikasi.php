<?php 
$nomer_sk = (empty($this->input->post_get('nomer_sk', TRUE))) ? $this->uri->segment(3) : $this->input->post_get('nomer_sk', TRUE);
if ($item) {
  foreach ($item as $key) {
    $id = $key->id_perizinan;
    $nomer_sk = $key->nomer_sk;
    $nama_pemohon = $key->nama_pemohon;
    $nama_jenisizin = $key->nama_jenisizin;
    $nama_bidang = $key->nama_bidang;
    $tanggal_sk = $key->tanggal_sk;
    $status_perizinan = $key->status_perizinan;
    $tanggal_akhir_sk = $key->tanggal_akhir_sk;
    $alamat = $key->alamat;
    $desa = $key->desa;
    $kecamatan = $key->kecamatan;
    $kabupaten = $key->kabupaten;
} 
}?>
           



<!DOCTYPE html>
<html lang="en">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title ;?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()."asset/admin/" ;?>css/custom.css" rel="stylesheet">

   <!-- Select2 -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url()."asset/plugins/" ;?>jQuery/jQuery-2.1.3.min.js"></script>
  
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
<div class="row">
<div class="col-md-1"> </div>
<div class="col-md-10"><img src="<?php echo base_url()."asset/logo/" ;?>logo.png" width="300px" height="80px"> </div>
<div class="col-md-1"> </div>
</div>

      <div class="col-md-1"></div>

              <div class="col-md-10">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Verifikasi SK Izin  : <?php echo $nomer_sk ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h1>
                                          
                                          <small class="pull-right"></small>
                                      </h1>
                        </div>
                  <?php if($item): ?>    
                  <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>SK dengan Nomer : <?php echo $nomer_sk ?></strong> Terdaftar di Database Kami.
                  </div>
                  <?php else: ?>  
                  <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>SK dengan Nomer : <?php echo $nomer_sk ?></strong> Tidak Terdaftar di Database Kami.
                  </div>
                  <?php endif;  ?>  
                        <!-- /.col -->
                      </div>
                  <?php if($item): ?>    
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                    
                          <address>
                                          <strong><?php echo $nama_pemohon ?></strong>
                                          <br><?php echo $alamat ?>
                                          <br><?php echo $desa , $kecamatan , $kabupaten ?>
                                          
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                         
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <div class="col-xs-12 table">
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Izin Yang Di ajukan</th>
                                <th>Bidang Izin #</th>
                                <th>Tanggal Berlaku SK</th>
                                 <th>Tanggal Masa Berlaku SK</th>
                                <!-- <th>Status</th> -->
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td><?php echo $nama_jenisizin ?></td>
                                <td><?php echo $nama_bidang ?></td>
                                 <td><?php echo $tanggal_sk ?> </td>
                                 <?php if($tanggal_akhir_sk = "0000-00-00")
                                 {
                                  $tanggal_akhir_sk = "berlaku selamannya";

                                 }

                                 else {
                                  $tanggal_akhir_sk = $tanggal_akhir_sk;
                                 }
                                 ?>

                                <td><?php echo $tanggal_akhir_sk ?> </td>
                               <!-- <td><span class="btn btn-info"><strong> <?php echo $status_perizinan ?></storng></span> </td></strong></span></td> -->
                              </tr>
                             
                              
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                  <?php endif;  ?>

                      <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-xs-6">
                         
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6">
                          <h4>Untuk Informasi Lebih Lanjut Hubungi :</h4>
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%">Telepon:</th>
                                  <td>(0261) 8219319312</td>
                                </tr>
                                <tr>
                                  <th>Email :</th>
                                  <td>invest.sumedangkab.go.id</td>
                                </tr>
                                <tr>
                                  <th>WA:</th>
                                  <td>081243214644</td>
                                </tr>
                               
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                      <!-- this row will not appear when printing -->
                      <div class="row no-print">
                        <div class="col-xs-12">
                        
                          <button class="btn btn-success pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                          <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-envelope"></i> Kirim Pesan</button>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1"> </div>
        
  <!-- jQuery -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
     
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/iCheck/icheck.min.js"></script>
     

    <!-- jQuery Smart Wizard -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/custom.js"></script>
  
   
    
  
    <!-- Select2 -->
    <script src="<?php echo base_url()."asset/admin/" ;?>js/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Select2 -->
    <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
    <!-- /Select2 -->

    <script>
      $(document).ready(function() {
     // alert("ok");
        //$('#wizard').smartWizard();
    $('#wizard').smartWizard({
      // Properties
      /*selected: 0,  // Selected Step, 0 = first step   
      keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
      enableAllSteps: false,  // Enable/Disable all steps on first load
      transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
      contentURL:null, // specifying content url enables ajax content loading
      contentURLData:null, // override ajax query parameters
      contentCache:true, // cache step contents, if false content is fetched always from ajax url
      cycleSteps: false, // cycle step navigation
      enableFinishButton: false, // makes finish button enabled always
      hideButtonsOnDisabled: false, // when the previous/next/finish buttons are disabled, hide them instead
      errorSteps:[],    // array of step numbers to highlighting as error steps
      labelNext:'Selanjutnya', // label for Next button
      labelPrevious:'Sebelumnya', // label for Previous button
      labelFinish:'Selesai',  // label for Finish button        
      noForwardJumping:false,
      // Events
      onLeaveStep: null, // triggers when leaving a step
      onShowStep: null,  // triggers when showing a step
      onFinish: selesaiCallBack  // triggers when Finish button is clicked*/
    }); 
        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
      });
    
    </script>
    <!-- /jQuery Smart Wizard -->
  
   
  
    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_4"
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });
    
      });
    
    
    </script>
    <!-- /bootstrap-daterangepicker -->

  

   
   
  

  </body>
</html>