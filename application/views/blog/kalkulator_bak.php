
<script type="text/javascript" src="<?php echo base_url()."asset/plugins/" ;?>jQuery/jQuery-2.1.3.min.js"></script>

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<script type="text/javascript">
  $biaya_total_prasarana = 0;
  $biaya_total_imb = 0;
  $biaya_total_ho = 0;
  $biaya_total_rab = 0;

    window.onload = function() {
    biaya_total_prasarana("load");
    biaya_total_imb("load");
    biaya_total_ho("load");
    //cek_luas_tanah();
  };

    function add_ps(no) {
      var i = no; i++;
      $("#btn-add-ps").attr("onclick", "add_ps('"+i+"')");
      $("#input-add-ps").attr("onclick", "add_ps('"+i+"')");
      $("#jumlah-prasarana").attr("value", no);

      document.getElementById('add-ps-'+no).innerHTML = '<div class="col-md-2"><select name="nps'+no+'" id="nps-'+no+'" class="select2_single form-control" style="width:100%;" tabindex="-1" onchange="get_harga_prasarana(\''+no+'\');"><option value="">Pilih</option></select></div><div class="col-md-2"><div class="form-group input-group"><input onchange="total_harga_prasarana(\''+no+'\')" placeholder="Unit" type="number" name="jps'+no+'" id="jps-'+no+'" class="form-control" value="" disabled/><span class="input-group-addon">unit</span></div></div><div class="col-md-2"><div class="form-group input-group"><span class="input-group-addon">Rp</span><input onchange="total_harga_prasarana(\''+no+'\')" placeholder="Harga" type="number" name="hps'+no+'" id="hps-'+no+'" class="form-control" value="" disabled/></div></div><div class="col-md-2"><select onchange="total_harga_prasarana(\''+no+'\')" name="kps'+no+'" id="kps-'+no+'" class="form-control" disabled></select></div><div class="col-md-3"><div class="form-group input-group"><span class="input-group-addon">Rp</span><input onclick="edit_harga_prasarana_imb(\''+no+'\')" placeholder="Total Harga" style="direction: rtl;" type="text" id="tps-'+no+'" class="form-control" value="" readonly/><input onfocusout="update_harga_prasarana_imb(\''+no+'\')" type="hidden" name="tps'+no+'" id="htps-'+no+'" class="form-control" value="0"/></div></div><div class="col-md-1"><button onclick="delete_ps(\''+no+'\')" type="button" class="btn btn-default form-control"><i class="fa fa-times"></i> </button></div><div class="col-md-12"><hr style="margin:10px 0;"/></div>';

      var p = document.getElementById('add-ps-'+no);
      var newElement = document.createElement('div');
      newElement.setAttribute('id', 'add-ps-'+i);
      p.parentNode.insertBefore(newElement, p.nextSibling);
      //$("#isi-prasarana-"+no).focus();
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

      jumlah = $("#jumlah-prasarana").val() + 1;

      get_data_prasarana(no);
    };

    function delete_ps(no) {
      $("#add-ps-"+no).hide();
      $("#nps-"+no).val('');
      $("#jps-"+no).val('');
      $("#hps-"+no).val('');
      $("#kps-"+no).val('');

      $("#htps-"+no).val('0');

      biaya_total_prasarana();
    };

    function get_harga_prasarana(no) {
      if (!$("#nps-"+no).val()) {
          $("#jps-"+no).attr('disabled',true);
          $("#hps-"+no).attr('disabled',true);
          $("#kps-"+no).attr('disabled',true);
          $("#hps-"+no).val("0");
          $("#jps-"+no).val("");
          $("#jps-"+no).focus();
          total_harga_prasarana(no);
      } else {
        var id = $("#nps-"+no).val();
        $.post("<?php echo base_url();?>kalkulator/get_harga_prasarana/"+id,{},function(obj){
          $("#jps-"+no).attr('disabled',false);
          $("#hps-"+no).attr('disabled',false);
          $("#kps-"+no).attr('disabled',false);
          $("#hps-"+no).val(obj);
          $("#jps-"+no).val("");
          $("#jps-"+no).focus();
          total_harga_prasarana(no);
        });

        $.post("<?php echo base_url();?>kalkulator/get_data_kerusakan/",{},function(obj){
          $("#kps-"+no).html(obj);
        });
      };
    };

    function get_data_prasarana(no) {
      var id = $("#nps-"+no).val();
      $.post("<?php echo base_url();?>kalkulator/get_data_prasarana/"+id,{},function(obj){
        $("#nps-"+no).html(obj);
      });
    };

    function total_harga_prasarana(no) {
      var nama_prasarana = $("#nps-"+no).val();
      var jumlah_prasarana = $("#jps-"+no).val();
      var harga_prasarana = $("#hps-"+no).val();
      var kerusakan_prasarana = parseFloat(document.getElementById('kps-'+no).options[document.getElementById('kps-'+no).selectedIndex].text);
      var total_prasarana = jumlah_prasarana*harga_prasarana*kerusakan_prasarana;
      $("#tps-"+no).val(total_prasarana.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#htps-"+no).val(total_prasarana);

      biaya_total_prasarana();
    };

    function biaya_total_prasarana(method){
      $biaya_total_prasarana = 0;

      var jumlah = $("#jumlah-prasarana").val();
      for (var i = jumlah; i > 0; i--) {
        $biaya_total_prasarana += parseFloat($("#htps-"+i).val());
      };
      $biaya_total_prasarana = parseInt($biaya_total_prasarana);
      $("#total-harga-prasarana").val($biaya_total_prasarana.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#total-prasarana-imb").val($biaya_total_prasarana);
      if (method == "load") {total_retribusi_imb("load");} else {total_retribusi_imb();};
    };




    function add_bi(no) {
      var indeks_integrasi = document.getElementById('indeks-integrasi').innerHTML;
      var i = no; i++;
      $("#btn-add-bi").attr("onclick", "add_bi('"+i+"')");
      $("#input-add-bi").attr("onclick", "add_bi('"+i+"')");
      $("#jumlah-bi").attr("value", no);

      document.getElementById('add-bi-'+no).innerHTML = '<div id="add-bi-'+no+'"><div class="col-md-6"><input placeholder="Nama Bangunan" type="text" name="nbi'+no+'" id="nbi-'+no+'" class="form-control" value=""/></div><div class="col-md-4"><label><input type="checkbox" class="" name="bbi'+no+'" id="bbi-'+no+'" onclick="toogle_basement(\''+no+'\')"> Basement (IT x 1.30)</label></div><div class="col-md-2"><button onclick="delete_bi(\''+no+'\')" type="button" class="btn btn-default form-control"><i class="fa fa-times"></i> Delete</button></div><div class="col-md-2"><div class="form-group input-group"><input placeholder="Unit" onchange="total_harga_imb(\''+no+'\')" type="number" name="ubi'+no+'" id="ubi-'+no+'" class="form-control" value=""/><span class="input-group-addon">unit</span></div></div><div class="col-md-2"><div class="form-group input-group"><input placeholder="Luas Bangunan" onchange="total_harga_imb(\''+no+'\')" type="text" name="lbi'+no+'" id="lbi-'+no+'" class="form-control" value=""/><span class="input-group-addon">m<sup>2</sup></span></div></div><div class="col-md-2"><input placeholder="Indeks Terintegrasi" type="number" name="ibi'+no+'" id="ibi-'+no+'" class="form-control" value="'+indeks_integrasi+'" readonly/></div><div class="col-md-2"><select onchange="total_harga_imb(\''+no+'\')" name="kbi'+no+'" id="kbi-'+no+'" class="form-control"></select></div><div class="col-md-4"><div class="form-group input-group"><span class="input-group-addon">Rp</span><input onclick="edit_harga_gedung_imb(\''+no+'\')" placeholder="Total Harga" style="direction: rtl;" type="text" id="tbi-'+no+'" class="form-control" value="" readonly/><input onfocusout="update_harga_gedung_imb(\''+no+'\')" type="hidden" name="tbi'+no+'" id="htbi-'+no+'" class="form-control" value="0"/></div></div><div class="col-md-12"><hr style="margin:10px 0;"/></div></div>'
      
      var p = document.getElementById('add-bi-'+no);
      var newElement = document.createElement('div');
      newElement.setAttribute('id', 'add-bi-'+i);
      p.parentNode.insertBefore(newElement, p.nextSibling);
      //$("#isi-prasarana-"+no).focus();

      jumlah = $("#jumlah-bi").val() + 1;

      $.post("<?php echo base_url();?>kalkulator/get_data_kerusakan/",{},function(obj){
        $("#kbi-"+no).html(obj);
      });
      $("#nbi-"+no).focus();
    };

    function delete_bi(no) {
      $("#add-bi-"+no).hide();
      $("#nbi-"+no).val('');
      $("#ubi-"+no).val('');
      $("#lbi-"+no).val('');
      $("#htbi-"+no).val('0');

      biaya_total_imb();
    };

    function toogle_basement(no) {
      var indeks_integrasi = document.getElementById('indeks-integrasi').innerHTML;
      if ($("#bbi-"+no).is(":checked")) {indeks_integrasi = parseFloat(indeks_integrasi)*1.30;};
      $("#ibi-"+no).val(indeks_integrasi);

      total_harga_imb(no);
    };

    function total_harga_imb(no) {
      var unit_imb = $("#ubi-"+no).val();
      var luas_imb = $("#lbi-"+no).val();
      var indeks_imb = $("#ibi-"+no).val();
      var kerusakan_imb = parseFloat(document.getElementById('kbi-'+no).options[document.getElementById('kbi-'+no).selectedIndex].text);
      var total_imb = unit_imb*luas_imb*indeks_imb*kerusakan_imb*10000;

      total_imb = parseInt(total_imb);
      $("#tbi-"+no).val(total_imb.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#htbi-"+no).val(total_imb);

      biaya_total_imb();
    };

    function biaya_total_imb(method){
      $biaya_total_imb = 0;
      var jumlah = $("#jumlah-bi").val();
      for (var i = jumlah; i > 0; i--) {
        $biaya_total_imb += parseFloat($("#htbi-"+i).val());
      };
      
      $biaya_total_imb = parseInt($biaya_total_imb);
      $("#total-harga-imb").val($biaya_total_imb.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#total-gedung-imb").val($biaya_total_imb);
      if (method == "load") {total_retribusi_imb("load");} else{total_retribusi_imb();};
    };

    function edit_harga_gedung_imb(id) {
      $("#tbi-"+id).attr("type", "hidden");
      $("#htbi-"+id).attr("type", "text");
      $("#htbi-"+id).focus();
    };

    function update_harga_gedung_imb(id) {
      $("#tbi-"+id).attr("type", "text");
      $("#htbi-"+id).attr("type", "hidden");
      $biaya_total_imb = parseInt($("#htbi-"+id).val());
      $("#tbi-"+id).val($biaya_total_imb.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      biaya_total_imb();
    };

    function edit_harga_prasarana_imb(id) {
      $("#tps-"+id).attr("type", "hidden");
      $("#htps-"+id).attr("type", "text");
      $("#htps-"+id).focus();
    };

    function update_harga_prasarana_imb(id) {
      $("#tps-"+id).attr("type", "text");
      $("#htps-"+id).attr("type", "hidden");
      $biaya_total_prasarana = parseInt($("#htps-"+id).val());
      $("#tps-"+id).val($biaya_total_prasarana.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      biaya_total_prasarana();
    };

    function edit_total_gedung_imb() {
      $("#total-harga-imb").attr("type", "hidden");
      $("#total-gedung-imb").attr("type", "text");
      $("#total-gedung-imb").focus();
    };

    function update_total_gedung_imb() {
      $("#total-harga-imb").attr("type", "text");
      $("#total-gedung-imb").attr("type", "hidden");
      $biaya_total_imb = parseInt($("#total-gedung-imb").val());
      $("#total-harga-imb").val($biaya_total_imb.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      total_retribusi_imb();
    };

    function edit_total_prasarana_imb() {
      $("#total-harga-prasarana").attr("type", "hidden");
      $("#total-prasarana-imb").attr("type", "text");
      $("#total-prasarana-imb").focus();
    };

    function update_total_prasarana_imb() {
      $("#total-harga-prasarana").attr("type", "text");
      $("#total-prasarana-imb").attr("type", "hidden");
      $biaya_total_prasarana = parseInt($("#total-prasarana-imb").val());
      $("#total-harga-prasarana").val($biaya_total_prasarana.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      total_retribusi_imb();
    };

    function edit_total_retribusi_imb() {
      $("#total-harga-retribusi-imb").attr("type", "hidden");
      $("#total-biaya-imb").attr("type", "text");
      $("#total-biaya-imb").focus();
    };

    function update_total_retribusi_imb() {
      $("#total-harga-retribusi-imb").attr("type", "text");
      $("#total-biaya-imb").attr("type", "hidden");
      total_retribusi_imb("manual");
    };

    function edit_total_rab_imb() {
      $("#total-harga-rab").attr("type", "hidden");
      $("#total-rab-imb").attr("type", "text");
      $("#total-rab-imb").focus();
    };

    function update_total_rab_imb() {
      $("#total-harga-rab").attr("type", "text");
      $("#total-rab-imb").attr("type", "hidden");
      $biaya_total_rab = parseInt($("#total-rab-imb").val());
      $("#total-harga-rab").val($biaya_total_rab.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      total_retribusi_imb("tower");
    };

    function total_retribusi_imb(method){
      var total_retribusi_imb = 0;
      total_retribusi_imb = $biaya_total_prasarana+$biaya_total_imb;
      if (method == "load") {
        total_retribusi_imb = 0;
      } else if (method == "manual") {
        total_retribusi_imb = parseFloat($("#total-biaya-imb").val());
      } else if (method == "tower") {
        total_retribusi_imb = $biaya_total_rab*(1.75/100);
      };
      
      total_retribusi_imb = parseInt(total_retribusi_imb);
      $("#total-harga-retribusi-imb").val(total_retribusi_imb.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#total-biaya-imb").val(total_retribusi_imb);

      terbilang_retribusi_imb(total_retribusi_imb);
    };

    function terbilang_retribusi_imb(total_retribusi_imb){
        var txtTerbilang = total_retribusi_imb;
        if (txtTerbilang>0) {
          $.ajax({
              type: "POST",
              url: "<?php echo base_url().'kalkulator/konversi'; ?>",
              data: {"txtTerbilang":txtTerbilang},
              success: function(resp){
                      $("#terbilang-total-harga-retribusi-imb").html(resp);
                      $("#terbilang-biaya-imb").val(resp);
              },
              error:function(event, textStatus, errorThrown) {
                  $("#terbilang-total-harga-retribusi-imb").html('Error Message: '+ textStatus + ' , HTTP Error: '+errorThrown);
                  $("#terbilang-biaya-imb").val("");
              },
          });
      } else {$("#terbilang-total-harga-retribusi-imb").html("");$("#terbilang-biaya-imb").val("");};
    };




    function add_ho(no) {
      var i = no; i++;
      $("#btn-add-ho").attr("onclick", "add_ho('"+i+"')");
      $("#input-add-ho").attr("onclick", "add_ho('"+i+"')");
      $("#jumlah-ho").attr("value", no);

      var indeks_jalan = 10;

      document.getElementById('add-ho-'+no).innerHTML = '<div class="row"><div class="col-md-12"><div class="col-md-4"><select onchange="get_indeks_gangguan(\''+no+'\')" name="nho'+no+'" id="nho-'+no+'" class="select2_single form-control" style="width:100%;" tabindex="-1"><option value="">Pilih</option></select></div><div class="col-md-2"><input data-original-title="Indeks Gangguan" data-toggle="tooltip" placeholder="Indeks Gangguan" type="text" name="gho'+no+'" id="gho-'+no+'" class="form-control" value="0" readonly/></div><div class="col-md-2"><input data-original-title="Indeks Jalan" data-toggle="tooltip" placeholder="Indeks Jalan" type="text" name="jho'+no+'" id="jho-'+no+'" class="form-control" value="'+indeks_jalan+'" readonly/></div><div class="col-md-2"><label><input type="checkbox" class="" name="hho'+no+'" id="hho-'+no+'" onclick="total_harga_ho(\''+no+'\')"> Herr (x 20%)</label></div><div class="col-md-2"><button onclick="delete_ho(\''+no+'\')" type="button" class="btn btn-default form-control"><i class="fa fa-times"></i> Delete</button></div></div></div><div class="row"><div class="col-md-12"><div id="lgho-'+no+'" class="col-md-8"><div class="form-group input-group"><input data-original-title="Luas Ruang Usaha" data-toggle="tooltip" placeholder="Luas Ruang Usaha" onchange="luas_ho(\''+no+'\')" type="text" name="lho'+no+'" id="lho-'+no+'" class="form-control" value="0" readonly/><span class="input-group-addon">m<sup>2</sup></span></div></div><div id="lgho1-'+no+'" class="hidden"><div class="form-group input-group"><input data-original-title="Rp0.00" data-toggle="tooltip" placeholder="0 - 100" type="text" name="lho1'+no+'" id="lho1-'+no+'" class="form-control" value="" readonly/><input type="hidden" name="tho1'+no+'" id="tho1-'+no+'" value=""/><span data-original-title="0-100" data-toggle="tooltip" data-placement="right" class="input-group-addon">m<sup>2</sup></span></div></div><div id="lgho2-'+no+'" class="hidden"><div class="form-group input-group"><input data-original-title="Rp0.00" data-toggle="tooltip" placeholder="101 - 250" type="text" name="lho2'+no+'" id="lho2-'+no+'" class="form-control" value="" readonly/><input type="hidden" name="tho2'+no+'" id="tho2-'+no+'" value=""/><span data-original-title="101-250" data-toggle="tooltip" data-placement="right" class="input-group-addon">m<sup>2</sup></span></div></div><div id="lgho3-'+no+'" class="hidden"><div class="form-group input-group"><input data-original-title="Rp0.00" data-toggle="tooltip" placeholder="> 250" type="text" name="lho3'+no+'" id="lho3-'+no+'" class="form-control" value="" readonly/><input type="hidden" name="tho3'+no+'" id="tho3-'+no+'" value=""/><span data-original-title=">250" data-toggle="tooltip" data-placement="right" class="input-group-addon">m<sup>2</sup></span></div></div><div class="col-md-4"><div class="form-group input-group"><span class="input-group-addon">Rp</span><input placeholder="Total Harga" style="direction: rtl;" type="text" id="tho-'+no+'" class="form-control" value="" readonly/><input type="hidden" name="tho'+no+'" id="htho-'+no+'" value=""/></div></div></div></div><div class="col-md-12"><hr style="margin:10px 0;"/></div>';

      var p = document.getElementById('add-ho-'+no);
      var newElement = document.createElement('div');
      newElement.setAttribute('id', 'add-ho-'+i);
      p.parentNode.insertBefore(newElement, p.nextSibling);
      //$("#isi-prasarana-"+no).focus();
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

      jumlah = $("#jumlah-ho").val() + 1;

      get_data_jenis_usaha(no);
    };

    function delete_ho(no) {
      $("#add-ho-"+no).hide();
      $("#nho-"+no).val('');
      $("#gho-"+no).val('');
      $("#jho-"+no).val('');
      $("#lho-"+no).val('');

      $("#tho-"+no).val('0');

      total_harga_ho(no);
    };


    function get_data_jenis_usaha(no) {
      $.post("<?php echo base_url();?>kalkulator/get_data_jenis_usaha/",{},function(obj){
        $("#nho-"+no).html(obj);
      });
    };

    function get_indeks_gangguan(no) {
      if (!$("#nho-"+no).val()) {
          $("#lho-"+no).attr('readonly',true);
          $("#gho-"+no).val("0");
          $("#lho-"+no).val("0");
          luas_ho(no);
      } else {
        var id = $("#nho-"+no).val();
        $.post("<?php echo base_url();?>kalkulator/get_indeks_gangguan/"+id,{},function(obj){
          $("#lho-"+no).attr('readonly',false);
          $("#gho-"+no).val(obj);
          $("#lho-"+no).focus();
          total_harga_ho(no);
        });
      };
    };

    function luas_ho(no) {
      var luas_ho = $("#lho-"+no).val();

      $("#lho1-"+no).val('0');
      $("#lho2-"+no).val('0');
      $("#lho3-"+no).val('0');

      $("#lgho-"+no).attr("class", "col-md-8");
      $("#lgho1-"+no).attr("class", "hidden");
      $("#lgho2-"+no).attr("class", "hidden");
      $("#lgho3-"+no).attr("class", "hidden");

      if (luas_ho>0) {
        $("#lho1-"+no).val(luas_ho);

        $("#lgho-"+no).attr("class", "col-md-4");
        $("#lgho1-"+no).attr("class", "col-md-4");
      };
      if (luas_ho>100) {
        $("#lho1-"+no).val('100'); 
        $("#lho2-"+no).val(luas_ho-100);

        $("#lgho1-"+no).attr("class", "col-md-2");
        $("#lgho2-"+no).attr("class", "col-md-2");
      };
      if (luas_ho>250) {
        $("#lho2-"+no).val('150'); 
        $("#lho3-"+no).val(luas_ho-250);

        $("#lgho-"+no).attr("class", "col-md-2");
        $("#lgho3-"+no).attr("class", "col-md-2");
      };

      total_harga_ho(no);
    };

    function total_harga_ho(no) {
      var indeks_gangguan = $("#gho-"+no).val();
      var indeks_jalan = $("#jho-"+no).val();
      var luas_ho = $("#lho-"+no).val();
      var herr_ho = 1;
      if ($("#hho-"+no).is(":checked")) {herr_ho = 0.2;};

      var indeks = indeks_gangguan*indeks_jalan*herr_ho;
      var total_ho_1 = 0;
      var total_ho_2 = 0;
      var total_ho_3 = 0;

      var luas_ho_1 = $("#lho1-"+no).val();
      var luas_ho_2 = $("#lho2-"+no).val();
      var luas_ho_3 = $("#lho3-"+no).val();

      if (luas_ho>0) {total_ho_1 = luas_ho_1*500*indeks;};
      if (luas_ho>100) {total_ho_2 = luas_ho_2*650*indeks;};
      if (luas_ho>250) {total_ho_3 = luas_ho_3*200*indeks;};

      var total_ho = total_ho_1+total_ho_2+total_ho_3;
      $("#tho-"+no).val(total_ho.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#htho-"+no).val(total_ho);
      $("#tho1-"+no).val(total_ho_1);
      $("#tho2-"+no).val(total_ho_2);
      $("#tho3-"+no).val(total_ho_3);

      $("#lho1-"+no).attr("data-original-title", "Rp"+total_ho_1.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#lho2-"+no).attr("data-original-title", "Rp"+total_ho_2.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#lho3-"+no).attr("data-original-title", "Rp"+total_ho_3.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

      biaya_total_ho();
    };

    function biaya_total_ho(method){
      $biaya_total_ho = 0;
      var jumlah = $("#jumlah-ho").val();
      for (var i = jumlah; i > 0; i--) {
        $biaya_total_ho += parseFloat($("#htho-"+i).val());
      };
      if (method == "load") {
        $biaya_total_ho = 0;
      } else if (method == "manual") {
        $biaya_total_ho = parseFloat($("#total-biaya-ho").val());
      };
      
      $("#total-harga-retribusi-ho").val($biaya_total_ho.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#total-biaya-ho").val($biaya_total_ho);
      terbilang_retribusi_ho($biaya_total_ho);
    };

    function edit_total_retribusi_ho() {
      $("#total-harga-retribusi-ho").attr("type", "hidden");
      $("#total-biaya-ho").attr("type", "text");
      $("#total-biaya-ho").focus();
    };

    function update_total_retribusi_ho() {
      $("#total-harga-retribusi-ho").attr("type", "text");
      $("#total-biaya-ho").attr("type", "hidden");
      biaya_total_ho("manual");
    };

    function terbilang_retribusi_ho(biaya_total_ho){
        var txtTerbilang = biaya_total_ho;
        if (txtTerbilang>0) {
          $.ajax({
              type: "POST",
              url: "<?php echo base_url().'kalkulator/konversi'; ?>",
              data: {"txtTerbilang":txtTerbilang},
              success: function(resp){
                      $("#terbilang-total-harga-retribusi-ho").html(resp);
                      $("#terbilang-biaya-ho").val(resp);
              },
              error:function(event, textStatus, errorThrown) {
                  $("#terbilang-total-harga-retribusi-ho").html('Error Message: '+ textStatus + ' , HTTP Error: '+errorThrown);
                  $("#terbilang-biaya-ho").val("");
              },
          });
      } else {$("#terbilang-total-harga-retribusi-ho").html("");$("#terbilang-biaya-ho").val("");};
    };


    function edit_total_retribusi(id_jenis_izin,id_sub_jenis_izin) {
      $("#total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).attr("type", "hidden");
      $("#total-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).attr("type", "text");
      $("#total-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).focus();
    };

    function update_total_retribusi(id_jenis_izin,id_sub_jenis_izin) {
      biaya_total_retribusi = parseFloat($("#total-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).val());
      $("#total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).val(biaya_total_retribusi.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      $("#total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).attr("type", "text");
      $("#total-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).attr("type", "hidden");
      terbilang_retribusi(id_jenis_izin,id_sub_jenis_izin);
    };

    function terbilang_retribusi(id_jenis_izin,id_sub_jenis_izin){
        var txtTerbilang = $("#total-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).val();
        if (txtTerbilang>0) {
          $.ajax({
              type: "POST",
              url: "<?php echo base_url().'kalkulator/konversi'; ?>",
              data: {"txtTerbilang":txtTerbilang},
              success: function(resp){
                      $("#terbilang-total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).html(resp);
                      $("#terbilang-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).val(resp);
              },
              error:function(event, textStatus, errorThrown) {
                  $("#terbilang-total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).html('Error Message: '+ textStatus + ' , HTTP Error: '+errorThrown);
                  $("#terbilang-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).val("");
              },
          });
      } else {$("#terbilang-total-harga-retribusi-"+id_jenis_izin+"-"+id_sub_jenis_izin).html("");$("#terbilang-biaya-"+id_jenis_izin+"-"+id_sub_jenis_izin).val("");};
    };
</script>

<div class="clearfix">
  <div class="page-title">
    <div class="title_left">
      <h3>Retribusi</h3>
    </div>
  </div>
  <div class="clearfix"/>
  <div class="row">
    <div class="x_panel">
      <form method='post' enctype="multipart/form-data" action="">
      <div class="x_content">
        <div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
          <button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <label id='status'></label>
         </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <form method='post' enctype="multipart/form-data" action="">
              <div class="col-md-12">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                              <li role="presentation" class=""><a href="#tab_content5" role="tab" data-toggle="tab" aria-expanded="false">IMB</a>
                              </li>
                              <li role="presentation" class=""><a href="#tab_content8" role="tab" data-toggle="tab" aria-expanded="false">IMB Tower</a>
                              </li>
                              <li role="presentation" class=""><a href="#tab_content6" role="tab" data-toggle="tab" aria-expanded="false">HO</a>
                              </li>
                              <li role="presentation" class="active"><a href="#tab_content7" role="tab" data-toggle="tab" aria-expanded="true">Lainnya</a>
                              </li>
                          </ul>
                          <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                                  <div class="col-md-12">
                                    <div id="retribusiimb">
                                      <p class="lead">Indeks Perhitungan Besarnya Retribusi Bangunan Gedung</p>
                                      <p>
                                        <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <td colspan="2" align="center"><b>FUNGSI BANGUNAN</b></td>
                                          <td colspan="5" align="center"><b>KLASIFIKASI</b></td>
                                          <td colspan="2" align="center"><b>WAKTU PENGGUNAAN</b></td>
                                        </tr>
                                        <tr>
                                          <td align="center"><b>Parameter</b></td>
                                          <td align="center"><b>Indeks</b></td>
                                          <td align="center"><b>Parameter</b></td>
                                          <td align="center"><b>Bobot</b></td>
                                          <td align="center"><b>Parameter</b></td>
                                          <td align="center"><b>Indeks</b></td>
                                          <td align="center"><b>Hasil</b></td>
                                          <td align="center"><b>Parameter</b></td>
                                          <td align="center"><b>Indeks</b></td>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td rowspan="8" valign="middle"></td>
                                          <td rowspan="8" valign="middle"></td>
                                          <td>Kompleksitas</td>
                                          <td>0.25</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td rowspan="8" valign="middle"></td>
                                          <td rowspan="8" valign="middle"></td>
                                        </tr>
                                        <tr>
                                          <td>Permanensi</td>
                                          <td>0.20</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Risiko kebakaran</td>
                                          <td>0.15</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Zonasi gempa</td>
                                          <td>0.15</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Lokasi (kepadatan bangunan gedung)</td>
                                          <td>0.10</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Ketinggian bangunan gedung</td>
                                          <td>0.10</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Kepemilikan</td>
                                          <td>0.05</td>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td scope="row" colspan="4" align="center"><b>TOTAL</b></td>
                                          <td scope="row"><b></b></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <div class="text-muted well well-sm no-shadow">
                                      <p>Indeks Terintegrasi = Indeks Fungsi Bangunan x Indeks Total Klasifikasi x Indeks Waktu Penggunaan</p>
                                      <p>Indeks Terintegrasi =  x  x </p>
                                      <p>Indeks Terintegrasi = <b id="indeks-integrasi"></b></p>
                                    </div>
                                    
                                    <hr style="margin:10px 0;"/>
                                    <p class="lead">Biaya IMB Gedung</p>

                                    <div class="col-md-12">
                                        <div class="row">
                                          <div class="alert alert-danger">
                                              Indeks Terintegrasi belum lengkap. <a href=">" class="alert-link">Kelengkapan Data</a>.
                                          </div>
                                        <?php $no_bi=0;?>
                                          <div id="add-bi-<?php $add_bi = $no_bi + 1; echo $add_bi;?>"></div>
                                          <div class="col-md-6">
                                            <button id="btn-add-bi" onclick="add_bi('<?php echo $add_bi;?>')" type="button" class="btn btn-info form-control"><i class="fa fa-plus"></i> Add</button>
                                            <input type="hidden" name="jumlah_bi" id="jumlah-bi" value="<?php echo $jumlah_bi=0;?>"/>
                                          </div>
                                          <div class="col-md-2" style="text-align:right; padding:1% 0;">
                                            <p><b>TOTAL = </b></p>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group input-group">
                                              <span class="input-group-addon">Rp</span>
                                              <input onclick="edit_total_gedung_imb();" id="total-harga-imb" style="direction: rtl;" type="text" placeholder="Total Harga IMB" class="form-control" readonly/>
                                              <input onfocusout="update_total_gedung_imb();" type="hidden" name="total_gedung_imb" id="total-gedung-imb" class="form-control" value="<?php echo $total_gedung_imb=0;?>"/>
                                            </div>
                                          </div>
                                        </div>
                                        <hr style="margin:10px 0;"/>
                                      </div>

                                    <p class="lead">Biaya Prasarana</p>

                          <div class="col-md-12">
                                        <div class="row">
                                        <?php $no_ps=0;?>
                                          <div id="add-ps-<?php $add_ps = $no_ps + 1; echo $add_ps;?>"></div>
                                          <div class="col-md-6">
                                            <button id="btn-add-ps" onclick="add_ps('<?php echo $add_ps;?>')" type="button" class="btn btn-info form-control"><i class="fa fa-plus"></i> Add</button>
                                            <input type="hidden" name="jumlah_prasarana" id="jumlah-prasarana" value="<?php echo $jumlah_ps=0;?>"/>
                                          </div>
                                          <div class="col-md-2" style="text-align:right; padding:1% 0;">
                                            <p><b>TOTAL = </b></p>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group input-group">
                                              <span class="input-group-addon">Rp</span>
                                              <input onclick="edit_total_prasarana_imb();" id="total-harga-prasarana" style="direction: rtl;" type="text" placeholder="Total Harga Prasarana" class="form-control" readonly/>
                                              <input onfocusout="update_total_prasarana_imb();" type="hidden" name="total_prasarana_imb" id="total-prasarana-imb" class="form-control" value="<?php echo $total_prasarana_imb=0;?>"/>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12"><hr style="margin:10px 0;"/></div>
                                      <div class="col-md-3">
                                        <h4>
                                          <strong>Rekening = </strong>
                                          <select name="rekening_imb" style="width: 45px !important;">
                                            <option value="01">01 - IMB TIDAK BERTINGKAT</option>
                                            <option value="02">02 - IMB BERTINGKAT</option>
                                            <option value="03">03 - IMB KHUSUS</option>
                                            <option value="05">05 - IMB PEMUTIHAN</option>
                                            <option value="06">06 - IMB PERUBAHAN DAN PENAMBAHAN</option>
                                          </select>
                                        </h4>
                                      </div>
                                      <div class="col-md-3" style="text-align:right;">
                                        <h4><strong>Total biaya retribusi IMB = </strong></h4>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group input-group">
                                          <span class="input-group-addon">Rp</span>
                                          <input onclick="edit_total_retribusi_imb();" id="total-harga-retribusi-imb" style="direction: rtl;" type="text" placeholder="Total Harga Retribusi IMB" class="form-control" readonly/>
                                          <input onfocusout="update_total_retribusi_imb();" name="total_biaya_imb" id="total-biaya-imb" type="hidden" placeholder="Total Harga Retribusi IMB" class="form-control" value="<?php echo $total_biaya_imb=0;?>"/>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                    <div class="col-md-3 row">
                                      <a href="" class="btn btn-success form-control"><i class="fa fa-print"></i> Cetak</a>
                                    </div>
                                        <p align="right"><b id="terbilang-total-harga-retribusi-imb"></b></p>
                                        <input type="hidden" name="terbilang_biaya_imb" id="terbilang-biaya-imb" value=""/>
                                      </div>
                                  </p>
                                    </div>
                                  </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="tab_content8" aria-labelledby="profile-tab">
                                  <div class="col-md-12">
                                    <div id="retribusiimb">
                                      <p class="lead">Indeks Perhitungan Besarnya Retribusi Bangunan Tower</p>

                                        <div class="col-md-3">
                                          <p class="text-muted well well-sm no-shadow">Biaya Retribusi = Total Rencana Anggaran Biaya x 1.75%</p>
                                        </div>

                                      <div class="col-md-3" style="text-align:right;">
                                        <h4><strong>Rencana Anggaran Biaya : </strong></h4>
                                      </div>

                                        <div class="col-md-6">
                                          <div class="row">
                                            <div class="col-md-7">
                                            <div class="form-group input-group">
                                              <span class="input-group-addon">Rp</span>
                                              <input onclick="edit_total_rab_imb();" id="total-harga-rab" style="direction: rtl;" type="text" placeholder="Rencana Anggaran Biaya" class="form-control" value="0" readonly/>
                                              <input onfocusout="update_total_rab_imb();" type="hidden" name="total_rab_imb" id="total-rab-imb" class="form-control" value="0"/>
                                            </div>
                                          </div>
                                            <div class="col-md-1">
                                              <center><h4><strong>X</strong></h4></center>
                                          </div>
                                            <div class="col-md-4">
                                              <input placeholder="Indeks RAB" type="text" class="form-control" value="1.75%" readonly/>
                                          </div>
                                          </div>
                                        </div>

                                      <div class="col-md-12"><hr style="margin:10px 0;"/></div>
                                      <div class="col-md-3">
                                        <h4>
                                          <strong>Rekening = </strong>
                                          <select name="rekening_imb" style="width: 45px !important;">
                                            <option value="01">01 - IMB TIDAK BERTINGKAT</option>
                                            <option value="02">02 - IMB BERTINGKAT</option>
                                            <option value="03">03 - IMB KHUSUS</option>
                                            <option value="05">05 - IMB PEMUTIHAN</option>
                                            <option value="06">06 - IMB PERUBAHAN DAN PENAMBAHAN</option>
                                          </select>
                                        </h4>
                                      </div>
                                      <div class="col-md-3" style="text-align:right;">
                                        <h4><strong>Total biaya retribusi IMB = </strong></h4>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group input-group">
                                          <span class="input-group-addon">Rp</span>
                                          <input onclick="edit_total_retribusi_imb();" id="total-harga-retribusi-imb" style="direction: rtl;" type="text" placeholder="Total Harga Retribusi IMB" class="form-control" readonly/>
                                          <input onfocusout="update_total_retribusi_imb();" name="total_biaya_imb" id="total-biaya-imb" type="hidden" placeholder="Total Harga Retribusi IMB" class="form-control" value="<?php echo $total_biaya_imb;?>"/>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                    <div class="col-md-3 row">
                                      <a href="" class="btn btn-success form-control"><i class="fa fa-print"></i> Cetak</a>
                                    </div>
                                        <p align="right"><b id="terbilang-total-harga-retribusi-imb"></b></p>
                                        <input type="hidden" name="terbilang_biaya_imb" id="terbilang-biaya-imb" value=""/>
                                      </div>
                                  </p>
                                    </div>
                                  </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
                                  <div class="col-md-12">
                                    <div id="retribusiho">
                                      <p class="lead">Biaya HO</p>

                                        <div class="col-md-12">
                                        <div class="row">
                                        <?php $no_ho=0;?>
                                          <div id="add-ho-<?php $add_ho = $no_ho + 1; echo $add_ho;?>"></div>
                                          <div class="col-md-6">
                                            <button id="btn-add-ho" onclick="add_ho('<?php echo $add_ho;?>')" type="button" class="btn btn-info form-control"><i class="fa fa-plus"></i> Add</button>
                                            <input type="hidden" name="jumlah_ho" id="jumlah-ho" value="<?php echo $jumlah_ho=0;?>"/>
                                          </div>
                                          <div class="col-md-2" style="text-align:right;">
                                            <p class="lead">Total = </p>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="form-group input-group">
                                              <span class="input-group-addon">Rp</span>
                                              <input onclick="edit_total_retribusi_ho();" id="total-harga-retribusi-ho" style="direction: rtl;" type="text" placeholder="Total Harga" class="form-control" readonly/>
                                              <input onfocusout="update_total_retribusi_ho();" type="hidden" name="total_biaya_ho" id="total-biaya-ho" placeholder="Total Harga" class="form-control" value="<?php echo $total_biaya_ho=0;?>"/>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12">
                                    <div class="col-md-3 row">
                                      <a href="" class="btn btn-success form-control"><i class="fa fa-print"></i> Cetak</a>
                                    </div>
                                        <p align="right"><b id="terbilang-total-harga-retribusi-ho"></b></p>
                                        <input type="hidden" name="terbilang_biaya_ho" id="terbilang-biaya-ho" value=""/>
                                      </div>
                                  </p>
                                    </div>   
                                  </div>
                              </div>
                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content7" aria-labelledby="profile-tab">
                                <div class="col-md-12">
                                  <?php

                                  class ret
                                  {
                                    public $id_jenis_izin=999;
                                    public $id_sub_jenis_izin=999;
                                  }

                                   $ret = new ret ?>
                                      <div id="retribusi_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <hr style="margin:10px 0;"/>
                                            <div class="col-md-9">
                                              <p class="lead">Biaya </p>
                                            </div>
                                            <div class="col-md-3">
                                                <label class="pull-right lead">
                                                  <input type="hidden" name="beretribusi_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>" value="Y">
                                                    <input type="checkbox" class="flat" value="" name="beretribusi_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>"> Beretribusi
                                                </label>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-3">
                                          <h4>
                                            <strong>Kode Rekening = </strong>
                                            <input type="text" name="rekening_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>" placeholder="Masukkan Kode Rekening" class="" data-original-title="contoh:1.16.02.4.1.2.03.01.XX" data-toggle="tooltip" data-placement="top" value="1.16.02.4.1.2.03.01.XX"/>
                                          </h4>
                                        </div>
                                        <div class="col-md-3" style="text-align:right;">
                                          <h4><strong>Total biaya retribusi = </strong></h4>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group input-group">
                                            <span class="input-group-addon">Rp</span>
                                            <input onclick="edit_total_retribusi('<?=$ret->id_jenis_izin?>','<?=$ret->id_sub_jenis_izin?>');" id="total-harga-retribusi-<?=$ret->id_jenis_izin?>-<?=$ret->id_sub_jenis_izin?>" style="direction: rtl;" type="text" placeholder="Total Harga Retribusi" class="form-control" value="" readonly/>
                                            <input onfocusout="update_total_retribusi('<?=$ret->id_jenis_izin?>','<?=$ret->id_sub_jenis_izin?>');" name="total_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>" id="total-biaya-<?=$ret->id_jenis_izin?>-<?=$ret->id_sub_jenis_izin?>" type="hidden" placeholder="Total Harga Retribusi" class="form-control" value=">"/>
                                          </div>
                                        </div>
                                        <div class="col-md-12">
                                      <div class="col-md-3 row">
                                        <a href="" class="btn btn-success form-control"><i class="fa fa-print"></i> Cetak</a>
                                      </div>
                                          <p align="right"><b id="terbilang-total-harga-retribusi-<?=$ret->id_jenis_izin?>-<?=$ret->id_sub_jenis_izin?>"></b></p>
                                          <input type="hidden" name="terbilang_<?=$ret->id_jenis_izin?>_<?=$ret->id_sub_jenis_izin?>" id="terbilang-biaya-<?=$ret->id_jenis_izin?>-<?=$ret->id_sub_jenis_izin?>" value=""/>
                                        </div>
                                    </p>
                                      </div>
                                </div>
                            </div>
                          </div>
                        </div>
                <div class="col-md-12"><hr style="margin:10px 0;"/></div>
                        <div class="col-md-6 col-md-offset-6">
                          <div class="row">
                            <div class="col-md-4">
                              <a href="" class="btn btn-default form-control"><i class="fa fa-reply"></i> Kembali</a>
                            </div>
                            <div class="col-md-4">
                              <a href="" class="btn btn-warning form-control"><i class="fa fa-undo"></i> Reset</a>
                            </div>
                            <div class="col-md-4">
                              <button type="submit" class="btn btn-primary form-control"><i class="fa fa-save"></i> Simpan</button>
                            </div>
                          </div>
                        </div>
                          </div>
                          </form>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>