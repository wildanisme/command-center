
  <?php $this->load->view('blog/src/header');?>
  



  <div class="clearfix"></div>
  <!-- Header Container / End -->


<!-- Content
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Berita & Informasi</h2>
        <span>Kab. Bogor</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="">
          <ul>
            <li><a href="<?php echo base_url();?>home">Home</a></li>
            <li><a href="<?php echo base_url();?>berita">Berita</a></li>
            <li>Detail</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<!-- Post Content -->
<div class="container">
  <div class="row">
    
    <!-- Inner Content -->
    <div class="col-xl-8 col-lg-8">
      <!-- Blog Post -->

      
      <!-- Blog Post -->
      <div class="blog-post single-post">

        <!-- Blog Post Thumbnail -->
        <div class="blog-post-thumbnail">
          <div class="blog-post-thumbnail-inner">
            
             <?php if ($picture!=""){ 
             echo "

            <img alt='' src='".base_url()."data/images/featured_image/$picture' class='post-img'>";
            }?>
          </div>
        </div>


        
    

        
        <!-- Content -->
          <div class="blog-post-content">

          <h3><?=$title;?></h3>

            <div class="blog-post-info-list margin-bottom-20">
            <a href="#" class="blog-post-info"><?=date('d M Y',strtotime($date));?></a>
            <a href="#"  class="blog-post-info"><?=$category_name?></a>
          </div>


       

         

          <p><?=$content?></p>


        
          <div class="clearfix"></div>

        </div>
      </div>
      <!-- Blog Post / End -->


     
            

      
      

    </div>
    <!-- Inner Content / End -->


    <div class="col-xl-4 col-lg-4 content-left-offset">
      <div class="sidebar-container">
        
       

        <!-- Widget -->
        <div class="sidebar-widget">

        <h4 class="headline margin-top-25">Related Posts</h4>
          <ul class="widget-tabs">
          	<?php
				foreach ($popular as $row) {
				?>
        
            <li>
              <a href="<?php echo base_url()."berita/read/"; echo $row->title_slug;?>" class="widget-content ">
                <img src="images/blog-02a.jpg" alt="">
                <div class="widget-text">
                  <h5> <?=$row->title?></h5>
                  <span><?php echo date('d M Y',strtotime($row->date));?></span>
                </div>
              </a>
            </li>
            <?php
            }
			?>

        
          </ul>

        </div>
        <!-- Widget / End-->



        <!-- Widget -->
        <div class="sidebar-widget">
          <h3>Kategori</h3>
          <div class="task-tags">
          	<?php
									foreach ($categories as $row) {
										echo"
										
											<a href='".base_url()."berita/category/$row->category_slug'><span> $row->category_name </span></a>
										";
									}
								?>
           
          </div>
        </div>

      </div>
    </div>

  </div>
</div>

<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->

<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>