<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->

<!-- Content
================================================== -->

<div class="container" style="margin-top: -40px;">

  <div class="row sticky-wrapper">

    <div class="col-lg-8 col-md-8 padding-right-30">

      <!-- Titlebar -->
      <div id="titlebar" class="listing-titlebar">
        <div class="listing-titlebar-title">
          <h2><?=$detail->nama_perusahaan?> <span class="listing-tag"><?=ucwords($detail->posisi)?></span></h2>
          <span>
            <a href="#listing-location" class="listing-address">
              <i class="fa fa-map-marker"></i>
              <?php echo $kecamatan ?>, <?php echo $kabupaten ?>
            </a>
          </span>
         
        </div>
      </div>

      <!-- Listing Nav -->
      <div id="listing-nav" class="listing-nav-container">
        <ul class="listing-nav">
          <li><a href="#listing-overview" class="active">Tentang Perusahaan</a></li>
        </ul>
      </div>
      
      <!-- Overview -->
      <div id="listing-overview" class="listing-section">

        <!-- Description -->

          <p><?=$detail->tentang_perusahaan?></p>


<h4 class="listing-desc-headline">Posisi :</h4>
<ul class="listing-features checkboxes margin-top-0">
          <li><?=$detail->posisi?></li>
        </ul>

        <h3 class="listing-desc-headline">Deskripsi Pekerjaan :</h3>

        <p><?=$detail->deskripsi?></p>


        <!-- Amenities -->
        <h3 class="listing-desc-headline">Persyaratan</h3>
        <ul class="listing-features checkboxes margin-top-0">
          <?php foreach($syarat as $s) {
                echo'<li>'.$s->nama_persyaratan.' - '.$s->keterangan_persyaratan.'</li>';
              }
            ?>
        </ul>

         <!-- Amenities -->
        <h3 class="listing-desc-headline">Tingkat Pendidikan</h3>
        <ul class="listing-features checkboxes margin-top-0">
          <li><?=$detail->pendidikan?></li>
        </ul>

       <h3 class="listing-desc-headline"> Kisaran Gaji</h3>
        <ul class="listing-features checkboxes margin-top-0">
          <li><?=rupiah($detail->kisaran_dari)?> - <?=rupiah($detail->kisaran_sampai)?></li>

        </ul>



      </div>

<?php if(!empty($other)){?>
      <!-- Food Menu -->
      <div id="listing-pricing-list" class="listing-section" style="margin-top:50px">
    

        <div class="show-more">
          <div class="pricing-list-container">
            <h4>Posisi Pekerjaan Lainya </h4>
            <!-- Food List -->
            <ul>
              <?php foreach($other as $o){
                ?>
              <a href="<?php echo base_url()."eloker/detail/".$o->id_loker ?>">
              <li>
                <h5><?php echo $o->posisi ?></h5>
                <p><?php echo $o->nama_perusahaan ?></p>

              </li>
            </a>
            <?php } ?>             
            </ul>

          </div>
        </div>
      </div>
      <!-- Food Menu / End -->
<?php } ?>

    
    
        
     
    
      <!-- Add Review Box -->
        </div>


    <!-- Sidebar
    ================================================== -->
    <div class="col-lg-4 col-md-4 margin-top-75 sticky">

 


      <!-- Contact -->
      <div class="boxed-widget margin-top-35">
        <h3><i class="sl sl-icon-pin"></i> Kontak</h3>
        <ul class="listing-details-sidebar">
          <li><i class="fa fa-map-marker"></i> <?php echo $detail->alamat_perusahan.'<br>'.$desa.','.$kecamatan.','.$kabupaten.','.$provinsi ?></li>
          <li><i class="sl sl-icon-phone"></i> <?php echo $detail->telepon ?></li>
          <li><i class="sl sl-icon-globe"></i> <a href="<?php echo $detail->website ?>/"><?php echo $detail->website ?></a></li>
          <li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php echo $detail->email ?>"><?php echo $detail->email ?></a></li>
        </ul>

        <ul class="listing-details-sidebar social-profiles">
          <!-- <li><a href="#" class="gplus-profile"><i class="fa fa-google-plus"></i> Google Plus</a></li> -->
        </ul>

        <!-- Reply to review popup -->
<!--         <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
          <div class="small-dialog-header">
            <h3>Send Message</h3>
          </div>
          <div class="message-reply margin-top-0">
            <textarea cols="40" rows="3" placeholder="Your message to Burger House"></textarea>
            <button class="button">Send Message</button>
          </div>
        </div>

        <a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>
      --> </div>
      <!-- Contact / End-->
      

      <!-- Opening Hours -->
      <div class="boxed-widget opening-hours margin-top-35">
        <div class="listing-badge now-<?php echo $detail->availability ?>"><?php echo ucwords($detail->availability) ?></div>
        <h3><i class="sl sl-icon-clock"></i> Jadwal</h3>
        <ul>
          <li><?php echo tanggal($detail->tgl_buka) ?> <span></span></li>
          <li style="font-weight: normal;">Sampai dengan </li>
          <li><?php echo tanggal($detail->tgl_tutup) ?> <span></span></li>
       
        </ul>
      </div>
      <!-- Opening Hours / End -->

             <!-- Book Now -->
      <div class="boxed-widget">
        <h3><i class="fa fa-calendar-check-o "></i> Informasi Loker</h3>
              
        <!-- progress button animation handled via custom.js -->
        <button class="progress-button button fullwidth margin-top-5"><span>Buat E-Loker</span></button>
      </div>
      <!-- Book Now / End -->


      <!-- Share / Like -->
      <div class="listing-share margin-top-40 margin-bottom-40 no-border">
       

          <!-- Share Buttons -->
          <ul class="share-buttons margin-top-40 margin-bottom-0">
            <li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
            <li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
            <li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
            <!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
          </ul>
          <div class="clearfix"></div>
      </div>

    </div>
    <!-- Sidebar / End -->

  </div>
</div>



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>