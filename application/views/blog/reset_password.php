  <?php $this->load->view('blog/src/header');?>
  



<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
	<div id="titlebar">
		<div class="container">
			<div class="row">
				<div class="col-md-12">

					<h2><?= $title;?></h2>

					<!-- Breadcrumbs -->
					<nav id="breadcrumbs">
						<ul>
							<li><a href="<?= base_url();?>">Home</a></li>
							<li><?= $title;?></li>
						</ul>
					</nav>

				</div>
			</div>
		</div>
	</div>


</div>

<!-- Container -->
<div class="container">

	<div class="row">
		<div class="col-md-12">

			<section id="not-found" class="">
				
				<div class="row" >
					<div class="col-lg-6 col-md-12">
						
						<div class="dashboard-list-box margin-top-0">
							<h4 class="gray">Buat Password Baru</h4>
							<div class="dashboard-list-box-static">
								<?php if(!empty($message)){?>
								<div class="notification <?= $type;?> ">
									<span><?=$message;?></span>
									
								</div>
								<?php } ?>
								<!-- Change Password -->
								<div class="my-profile">
								
								<form method="post">
									<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
									
									<label>Password</label>
									<input type="password" name="password">
									<?= form_error("password","<mark>","</mark>");?>

									<label>Konfirmasi Password</label>
									<input type="password" name="password_confirmation">
									<?= form_error("password_confirmation","<mark>","</mark>");?>

									<div style="margin:5px">
										<button type="submit" class="button margin-top-15">Ubah  Password</button>
									</div>
								</div>
								</form>
							</div>
						</div>
			</div>
				</div>
				

			</section>

		</div>
	</div>

</div>
<!-- Container / End -->


<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>