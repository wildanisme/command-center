<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header_home');?>
  

</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
  ================================================== -->
  <header id="header-container" >

    <?php $this->load->view('blog/src/navigasi_atas');?>

  </header>
  <div class="clearfix"></div>
  <!-- Header Container / End -->





<!-- Spacer -->
<div class="margin-top-90"></div>
<!-- Spacer / End-->

<!-- Page Content
================================================== -->
<div class="container">
  <div class="row">
    <div class="col-xl-3 col-lg-4">
      <div class="sidebar-container">
        <form action="<?php echo base_url();?>instansi" method='get'>
        <!-- Location -->
        <div class="sidebar-widget">
          <h3>Nama Instansi</h3>
          <div class="input-with-icon">
            <div id="autocomplete-container">
              
              <input id="keyword_skpd" name="s" type="text" placeholder="Search" name='s' value='<?php if (!empty($search)) echo $search;?>'>
            <i class="icon-material-outline-search"></i>
           
            </div>
            
          </div>
        </div>


      <!-- Search Button -->
      <div class="sidebar-search-button-container">
        <button class="button ripple-effect" >Cari Instansi</button>
      </div>
      <!-- Search Button / End-->

      
       
     
     </form>

      </div>
    </div>
    <div class="col-xl-9 col-lg-8 content-left-offset">

    

      <div class="notify-box margin-top-15">
        <div class="switch-container">
         <span class="switch-text">Daftar Instansi</span></label>
        </div>
      </div>

      <div class="listings-container grid-layout margin-top-35">
        

        <?php
        foreach ($instansi as $i) {
         ?>
        <!-- Job Listing -->
        <a href="<?php echo base_url();?>instansi/detail/<?=$i->id_skpd?>" class="job-listing">

          <!-- Job Listing Details -->
          <div class="job-listing-details">
            <!-- Logo -->
            <div class="job-listing-company-logo">
              <img src="https://mpp.sumedangkab.go.id/data/logo/skpd/<?=($i->logo_skpd=='') ? 'sumedang.png' : $i->logo_skpd  ?>" alt="">
            </div>

            <!-- Details -->
            <div class="job-listing-description">
              <h4 class="job-listing-company"><?=$i->nama_skpd?> </h4>
              
            </div>
          </div>

          <!-- Job Listing Footer -->
          <div class="job-listing-footer">
            
            <ul>
              <li><i class="icon-material-outline-business-center"></i> Full Time</li>
              <li><i class="icon-feather-phone"></i> <?=$i->telepon_skpd?></li>
            
            </ul>
          </div>
        </a>
        <?php
        }
        ?>  

       

        
      </div>




        <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-10 margin-bottom-20">
              <nav class="pagination">
                <ul>
                <?php
                if(count($instansi) > 0){
                  $link = make_pagination($pages,$current);

                  
                  $link = str_replace("btn ", "button ", $link);
                            echo $link;
                }
              ?>
                
                  
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <!-- Pagination / End -->

    </div>
  </div>
</div>



    <?php $this->load->view('blog/src/footer');?>


  </body>
  </html>