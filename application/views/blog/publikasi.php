<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container" style='min-height:440px;'>
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Publikasi</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href=" <?php echo base_url().'publikasi'; ?> ">Publikasi</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="row blog-page">

          <!-- Start kiri -->
          <div class="col-md-12 blog-box">
          
             <!-- Start Image Service Box 1 -->
             <?php foreach ($public as $row ) {
                echo"
              <div class='col-md-3 image-service-box' style='margin-top:20px;'>
              <a href='".base_url()."publikasi/detail/$row->id_publikasi'>
              ";
              //pdf
              if($row->type == 'application/pdf')
              {
                echo "
              <img class='img-thumbnail' src= '".base_url()."data/icon/pdf.png'  />
              ";
               }

               //video
              if($row->type == 'youtube')
              {
                echo "
              <img class='img-thumbnail' src= '".base_url()."data/icon/video.png'  />
              ";
               }

               //zip
               if($row->type == 'application/x-rar')
              {
                echo "
              <img class='img-thumbnail' src= '".base_url()."data/icon/archive.png'  />
              ";
               }

                //zip
               if($row->type == 'application/zip')
              {
                echo "
              <img class='img-thumbnail' src= '".base_url()."data/icon/word.png'  />
              ";
               }


               echo"</a>";

               echo "

              <h4><a href='".base_url()."publikasi/detail/$row->id_publikasi'> $row->judul </a></h4>
              
            </div>
            ";
          }
            ?>

            <!-- End Image Service Box 1 -->

            

         </div>
          <!-- end kiri-->

    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>