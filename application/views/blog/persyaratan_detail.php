<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Persyaratan</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content" style='min-height:440px;'>
      <div class="container">
        <div class="row blog-page">

        <?php foreach ($item as $key) {
                     $id = $key->id_jenis_izin;
                     $id_bidang = $key->id_bidang;
                     $nama_bidang = $key->nama_bidang;
           $masa_berlaku = $key->masa_berlaku;
                     $nama_jenisizin = $key->nama_jenisizin;
                     $status = $key->status;
                   }?>

          <!-- Start Blog Posts -->
          <div class="col-md-12 col-sm-12 col-xs-12">

             <table class="table table-striped">
                      
                      <tbody>

                        <tr>
                          <td>Nama Jenis Izin</td><td>  <?php echo $nama_jenisizin;?></td></tr>
                                                </tr> 
            <tr>
                          <td>Masa Berlaku</td><td> <?php echo $masa_berlaku;?></td></tr>
                                                </tr> 
                        <tr>
                          <td>Nama Bidang</td> <td><?php echo $nama_bidang; ?><td>  
                          
                          </tr>
                                             
                                     
                      </tbody>
                    </table>
          <hr style="margin-bottom:0px;"/>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Persyaratan Izin</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    

          <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          
                          <th>File</th>
                          
                          
                        </tr>
                      </thead>
                      <tbody>
                      <?php $no=1; foreach ($persyaratan as $syarat): ?>
                        
                        <tr>
                          <th scope="row"><?php echo $no;?></th>
                          <td><?php echo $syarat->nama_persyaratan_izin;?></td>
                          <td>
                          <?php if (empty($syarat->file_persyaratan_izin)) 
                          {
                            echo"-"; 
                          }
                          else{
                          	$link = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && ! in_array(strtolower($_SERVER['HTTPS']), array( 'off', 'no' ))) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/global/persyaratan_izin/';
                            echo "
                            <a href='{$link}{$syarat->file_persyaratan_izin}' class='btn btn-success btn-xs' download><i class='fa fa-download'></i> Download</a></td>
   

                            ";
                          
                          }?>

                                                 
                          </tr>
                        
                      <?php $no++; endforeach;?>
                        
                    </table>
                          
                       
             <a href='<?= base_url();?>persyaratan' class='btn btn-success'>Kembali</a> 
                  </div>
                </div>
              </div>
              </div>
       
          


      </div>
    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>