<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
	<?php $this->load->view('blog/src/header_home');?>


</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
	================================================== -->
	<header id="header-container" >

		<?php $this->load->view('blog/src/navigasi_atas');?>

	</header>
	<div class="clearfix"></div>
	<!-- Header Container / End -->






<!-- Titlebar
	================================================== -->
	<div class="single-page-header freelancer-header" data-background-image="<?php echo base_url().'data/images/bg-mpp.jpg';?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="single-page-header-inner">
						<div class="left-side">
							<div class="header-image freelancer-avatar"> <div class="circle">
								<div><?=$detail->loket;?></div>
							</div></div>
							<div class="header-details">
								<h3><?=$detail->nama_layanan;?> <span><?=$detail->nama_skpd;?></span></h3>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<!-- Page Content
	================================================== -->
	<div class="container">
		<div class="row">

			<!-- Content -->
			<div class="col-xl-8 col-lg-8 content-right-offset">

				<!-- Page Content -->
				<div class="single-page-section">
					<h3 class="margin-bottom-25">Deskripsi Layanan</h3>
					<hr>
					<p><?=$detail->deskripsi_layanan;?></p>


				</div>
				<?php if(!empty($detail->persyaratan)){
					
				?>
				<!-- Boxed List -->
				<div class="boxed-list margin-bottom-60">
					<div class="boxed-list-headline">
						<h3><i class="icon-material-outline-business"></i>Persyaratan</h3>
					</div>

					<table class="basic-table" style="padding-top: 20px;">
						
						<tr>
							<th>No.</th>
							<th>Persyaratan</th>
							<th>Keterangan</th>
							<th>File</th>
						</tr>
						<?php
						$no=1;
						foreach($detail->persyaratan as $p){
							$no++;
							?>

							<tr>
								<td data-label="Column 1"><?=$no;?></td>
								<td data-label="Column 2"><?=$p->nama_persyaratan?></td>
								<td data-label="Column 3"><?=$p->keterangan?></td>
								<td data-label="Column 4"><a href="<?php echo base_url();?>data/file_persyaratan/<?=$p->file?>" class="button ripple-effect gray ">Download <i class="icon-feather-download "></i></a></td>
							</tr>

							
							<?php
						}
						?>


					</table>
					<div class="clearfix"></div>
					<!-- Pagination / End -->

				</div>

							<?php
						}
						?>
				<!-- Boxed List / End -->
				<?php if(!empty($detail->sop)){

				?>
				<!-- Boxed List -->
				<div class="boxed-list margin-bottom-60">
					<div class="boxed-list-headline">
						<h3><i class="icon-material-outline-business"></i> Prosedur Pelayanan</h3>
					</div>
					<div style="padding-top:20px;padding-left: 20px;padding-right: 20px;">
						<p><?=$detail->sop;?></p>
					</div>
				</ul>
			</div>
			<?php }
			?>
			<!-- Boxed List / End -->

		</div>


		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				   <!-- Job Listing -->
        <a href="<?php echo base_url();?>instansi/detail/<?=$detail->id_skpd;?>" class="job-listing">

          <!-- Job Listing Details -->
          <div class="job-listing-details">
            <!-- Logo -->
            <div class="job-listing-company-logo">
              <img src="<?=base_url()?>data/logo/skpd/<?=($detail->logo_skpd=='') ? 'sumedang.png' : $detail->logo_skpd  ?>" alt="">
            </div>

            <!-- Details -->
            <div class="job-listing-description">
              <h4 class="job-listing-company"><?=$detail->nama_skpd?> </h4>
              
            </div>
          </div>


        </a>

				<?php
				$v_senin = $detail->senin == 'Y' ? 'checked' : '';
				$v_selasa = $detail->selasa == 'Y' ? 'checked' : '';
				$v_rabu = $detail->rabu == 'Y' ? 'checked' : '';
				$v_kamis = $detail->kamis == 'Y' ? 'checked' : '';
				$v_jumat = $detail->jumat == 'Y' ? 'checked' : '';
				$v_sabtu = $detail->sabtu == 'Y' ? 'checked' : '';
				?>

				<div class="dashboard-box margin-top-0 margin-bottom-50">
					<div class="headline">
						<h3><i class="icon-material-outline-access-time"></i> Jam Buka Layanan</h3>
					</div>
					<div class="content" style="padding:20px;">
						<ul class="list-1">
							
							<li>
								<div class="row">
									<div class="col-md-7">
										Senin
									</div>
									<div class="col-md-5">
										<?php if ($detail->senin == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_senin ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_senin ,"",-3);
										}
										?>
										
									</div>
								</div>
							</li>
							

							<li>
								<div class="row">
									<div class="col-md-7">
										Selasa
									</div>
									<div class="col-md-5">
										<?php if ($detail->selasa == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_selasa ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_selasa ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Rabu
									</div>
									<div class="col-md-5">
										<?php if ($detail->rabu == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_rabu ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_rabu ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Kamis
									</div>
									<div class="col-md-5">
										<?php if ($detail->kamis == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_kamis ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_kamis ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Jumat
									</div>
									<div class="col-md-5">
										<?php if ($detail->jumat == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_jumat ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_jumat ,"",-3);
										}
										?>
									</div>
								</div>
							</li>

							<li>
								<div class="row">
									<div class="col-md-7">
										Sabtu
									</div>
									<div class="col-md-5">
										<?php if ($detail->sabtu == 'N'){
											echo "Tutup";
										}
										else {
											echo substr_replace($detail->jam_buka_sabtu ,"",-3); echo " - "; echo substr_replace($detail->jam_tutup_sabtu ,"",-3);
										}
										?>
									</div>
								</div>
							</li>






						</ul>
					</div>

				</div>



				<!-- Button -->
				<a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50" style="text-align: center;vertical-align: middle; font-size: 14px;">Untuk Mendaftar Layanan Secara Online Silakan Install Aplikasi Android MPP Kab. Bogor <br>
					<img src="<?php echo base_url()."data/logo/playstore.png";?>" width="200px" alt=""></a>



				</div>
			</div>

		</div>
	</div>


	<!-- Spacer -->
	<div class="margin-top-15"></div>
	<!-- Spacer / End-->



	<?php $this->load->view('blog/src/footer');?>


</body>
</html>