  <?php $this->load->view('blog/src/header');?>
  
  
  <div class="clearfix"></div>
  <!-- Header Container / End -->

 

<!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h2>Agenda </h2><span>Kab. Bogor</span>

          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="#">Home</a></li>
              <li>Agenda</li>
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>

<!-- Page Content
  ================================================== -->
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="sidebar-container">

          <!-- Location -->
          <div class="sidebar-widget">
            <h3>Lokasi</h3>
            <div class="input-with-icon">
              <div >
                <input placeholder="Lokasi Agenda" name="alamat" value="<?= !empty($alamat) ? $alamat : ""  ;?>" type="text" placeholder="Anywhere">
              </div>
              <i class="icon-material-outline-location-on"></i>
            </div>
          </div>

          <!-- Category -->
          <div class="sidebar-widget">
            <h3>Kategori</h3>
            <select class="selectpicker default" multiple data-selected-text-format="count" data-size="7" data-placeholder="Pilih Kategori" name="id_kategori_agenda" title="All Categories" >
              <option value="">Semua kategori</option> 
              <?php 
              foreach($kategori_agenda as $row){
                $selected = (!empty($id_kategori_agenda) && $id_kategori_agenda == $row->id_kategori_agenda) ? "selected" :"";
                echo "<option $selected value='$row->id_kategori_agenda'>$row->nama_kategori_agenda</option>";
              }
              ?>
            </select>
          </div>

          <!-- Keywords -->
          <div class="sidebar-widget">
            <h3>Nama Agenda</h3>
            <div class="keywords-container">
              <div class="keyword-input-container">
                <input type="text" placeholder="Cari berdasarkan nama agenda?" value="<?= !empty($nama_agenda) ? $nama_agenda : ""  ;?>" name="nama_agenda"/ class="keyword-input">

              </div>
              <div class="keywords-list"><!-- keywords go here --></div>
              <div class="clearfix"></div>
            </div>
          </div>




          <!-- Tags -->
          <div class="sidebar-widget">
            <h3>Tags</h3>

            <div class="tags-container">

             <?php

             foreach($tags as $key=>$value)
             {
              if($value!=""){
                $checked = "";
                if(!empty($tag) && in_array($value,$tag)){
                  $checked = "checked";
                }
                echo '
                <div class="tag">
                <input '.$checked.' id="check-'.$value.'" type="checkbox" name="tag[]" value="'.$value.'">
                <label for="check-'.$value.'">'.$value.'</label>
                </div>
                ';
              }
            }
            ?>
            <br>

             
            

          </div>


        </div>
        <br><br>
        <br>
         <button type="submit" class="button button-block">Cari</button>
        <div class="clearfix"></div>

      </div>
    </div>
    <div class="col-xl-9 col-lg-8 content-left-offset">




      <!-- Freelancers List Container -->
      <div class="freelancers-container freelancers-grid-layout margin-top-35">

        <?php foreach ($agenda as $a ) {
          $judul = substr($a->nama_agenda,0,40);
          if (strlen($a->nama_agenda)>40) $judul .="...";
         ?>
        <!--Freelancer -->
        <div class="freelancer">

          <!-- Overview -->
          <div class="freelancer-overview" style="display: block;width:100%;height:500px;padding: 0px !important">
            
                    
                <a href="<?php echo base_url();?>agenda/detail/<?=$a->id_agenda?>"><img src="<?php echo base_url();?>data/agenda/<?=$a->poster?>" style="float:left;width:100%;height:100%;object-fit:cover;" alt=""></a>
                     
          </div>
          
          <!-- Details -->
          <div class="freelancer-details">
            <div class="freelancer-details-list">
              <?=$judul?>
              <ul>
                
                <li>Tanggal <strong><?=$a->tgl_mulai_agenda?></strong></li>
     
              </ul>
            </div>
            <a href="<?php echo base_url();?>agenda/detail/<?=$a->id_agenda?>" class="button button-sliding-icon ripple-effect">Detail Agenda <i class="icon-material-outline-arrow-right-alt"></i></a>
          </div>
        </div>
        <!-- Freelancer / End -->


        <?php }
?>



      </div>
      <!-- Freelancers Container / End -->


      <!-- Pagination -->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- Pagination -->
          <div class="pagination-container margin-top-40 margin-bottom-60">
            <nav class="pagination">
              <ul>

                <?php
                $CI =& get_instance();
                            $CI->load->library('pagination');

                            $config['base_url'] = base_url(). 'agenda/index/';
                            $config['total_rows'] = $total_rows;
                            $config['per_page'] = $per_page;
                            $config['attributes'] = array('class' => 'ripple-effect');
                            $config['page_query_string']=TRUE;
                            $CI->pagination->initialize($config);
                            $link = $CI->pagination->create_links();

                      
                            $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                            $link = str_replace("</strong>", "</a></li>", $link);
                            $link = str_replace("&lt;", "Back", $link);
                            $link = str_replace("&gt;", "Next", $link);
                            $link = str_replace("<a ;", "<li><a ", $link);
                             $link = str_replace("</a> ;", "<li></a> ", $link);
                            echo $link;
              ?>



                
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <!-- Pagination / End -->

    </div>
  </div>
</div>


<?php $this->load->view('blog/src/footer');?>


</body>
</html>