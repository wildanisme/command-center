<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header_home');?>
  

</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
  ================================================== -->
  <header id="header-container" >

    <?php $this->load->view('blog/src/navigasi_atas');?>

  </header>
  <div class="clearfix"></div>
  <!-- Header Container / End -->





<!-- Titlebar
  ================================================== -->
  <div class="single-page-header" data-background-image="<?php echo base_url().'data/images/bg-mpp.jpg';?>">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="single-page-header-inner">
            <div class="left-side">
              <div class="header-image"><a href="#"><img src="https://mpp.sumedangkab.go.id/data/logo/skpd/<?=($detail->logo_skpd=='') ? 'sumedang.png' : $detail->logo_skpd  ?>" alt=""></a></div>
              <div class="header-details">
                <h3><?=$detail->nama_skpd;?></h3>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- Page Content
  ================================================== -->
  <div class="container">
    <div class="row">

      <!-- Content -->
      <div class="col-xl-8 col-lg-8 content-right-offset">



        <!-- Freelancers Bidding -->
        <div class="boxed-list margin-bottom-60">
          <div class="boxed-list-headline">
            <h3><i class="icon-material-outline-library-books"></i>Daftar Layanan</h3>
          </div>
          <ul class="boxed-list-ul">

      <?php
            $no=1;

            foreach($layanan as $l){
              $no++;
              ?>
            <li>
              <div class="bid">
                <!-- Avatar -->
                <div class="bids-avatar">
                  <div class="freelancer-avatar">

                    <a href="<?php echo base_url();?>layanan/detail/<?=$l->id_layanan;?>"><div class="circle-kecil" ">
                      <div><?=$no;?></div>
                    </div></a>
                  </div>
                </div>

                <!-- Content -->
                <div class="bids-content">
                  <!-- Name -->
                  <div class="freelancer-name">
                    <h4><a href="<?php echo base_url();?>layanan/detail/<?=$l->id_layanan;?>"><?=$l->nama_layanan;?> </a></h4>
                  </div>
                </div>

                <!-- Bid -->
                <div class="bids-bid">
                  <div class="bid-rate">
                    <div class="rate">Loket</div>
                    <span><?=$l->loket;?></span>
                  </div>
                </div>
              </div>
            </li>
            <?php
          }
          ?>

       



          </ul>
        </div>

      </div>


      <!-- Sidebar -->
      <div class="col-xl-4 col-lg-4">
        <div class="sidebar-container">

         <!-- Widget -->
         <div class="dashboard-box margin-top-0 margin-bottom-50">
          <div class="headline">
            <h3><i class="icon-material-outline-info"></i> Informasi Lain</h3>
          </div>
          <div class="content" style="padding:20px;">

            <div class="freelancer-socials margin-top-10">
              <div class="row">
                <div class="col-md-1"> <i class="icon-material-outline-business"></i></div><div class="col-md-11" ><?=$detail->alamat_skpd;?></div>
              </div>
            </div>

            <div class="freelancer-socials margin-top-10">
              <div class="row">
                <div class="col-md-1"> <i class="icon-material-outline-email"></i></div><div class="col-md-11" ><?=$detail->email_skpd;?></div>
              </div>
            </div>


            <div class="freelancer-socials margin-top-10">
              <div class="row">
                <div class="col-md-1"> <i class="icon-feather-phone"></i></div><div class="col-md-11" ><?=$detail->telepon_skpd;?></div>
              </div>
            </div>


            <div class="freelancer-socials margin-top-10">
              <div class="row">
                <div class="col-md-1"> <i class="icon-feather-home"></i></div><div class="col-md-11" ><?=$detail->website;?></div>
              </div>
            </div>
          </div>
        </div>

        <div class="dashboard-box margin-top-0 margin-bottom-50">
          <div class="headline">
            <h3><i class="icon-material-outline-info"></i> Sosial Media</h3>
          </div>
          <div class="content" style="padding:20px;">
            <div class="freelancer-socials margin-top-25 centered">
              <ul>
                <li><a href="<?=$detail->facebook_skpd;?>" title="Facebok" target="_blank" data-tippy-placement="top"><i class="icon-brand-facebook"></i></a></li>
                <li><a href="<?=$detail->twitter_skpd;?>" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                <li><a href="<?=$detail->instagram_skpd;?>" title="Instagram" data-tippy-placement="top"><i class="icon-brand-instagram"></i></a></li>

              </ul>
            </div>
          </div>


        </div>

        
      </div>

    </div>
  </div>

</div>
</div>

<?php $this->load->view('blog/src/footer');?>


</body>
</html>