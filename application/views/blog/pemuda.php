  <?php $this->load->view('blog/src/header');?>
  
  
  <div class="clearfix"></div>
  <!-- Header Container / End -->

 

<!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h2>Kepemudaan </h2><span>Kab. Bogor</span>

          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="#">Home</a></li>
              <li>Kepemudaan</li>
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>

<!-- Page Content
  ================================================== -->
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="sidebar-container">


          <!-- Category -->
          <div class="sidebar-widget">
            <h3>Kategori</h3>
            <select class="selectpicker default" multiple data-selected-text-format="count" data-size="7" data-placeholder="Pilih Kategori" name="id_kategori_agenda" title="All Categories" >
              <option value="">Semua kategori</option> 
              <?php 
            foreach($kategori as $row){
              $selected = (!empty($id_kategori_pemuda) && $id_kategori_pemuda == $row->id_kategori_pemuda) ? "selected" :"";
              echo "<option $selected value='$row->id_kategori_pemuda'>$row->nama_kategori_pemuda</option>";
            }
            ?>
            </select>
          </div>

          <!-- Keywords -->
          <div class="sidebar-widget">
            <h3>Nama</h3>
            <div class="keywords-container">
              <div class="keyword-input-container">
                <input type="text" placeholder="Cari berdasarkan nama ?" value="<?= !empty($nama_agenda) ? $nama_agenda : ""  ;?>" name="nama_agenda"/ class="keyword-input">

              </div>
              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" value="<?= !empty($nama_pemuda) ? $nama_pemuda : ""  ;?>" />
              <div class="keywords-list"><!-- keywords go here --></div>
              <div class="clearfix"></div>
            </div>
          </div>



      
        <br><br>
        <br>
         <button type="submit" class="button button-block ">Cari</button>
        <div class="clearfix"></div>

      </div>
    </div>
    <div class="col-xl-9 col-lg-8 content-left-offset">




      <!-- Freelancers List Container -->
      <div class="freelancers-container freelancers-grid-layout margin-top-35">

        <?php foreach ($pemuda as $p)
        {
          ?>
   
        <!--Freelancer -->
        <div class="freelancer">

          <!-- Overview -->
          <div class="freelancer-overview">
            <div class="freelancer-overview-inner">
              
  
              
              <!-- Avatar -->
              <div class="freelancer-avatar">
                <div class="verified-badge"></div>
                <a href="#"><img src="<?php echo base_url().'data/pemuda/'.$p->foto;?>" alt=""></a>
              </div>

              <!-- Name -->
              <div class="freelancer-name">
                <h4><a href="<?php echo base_url();?>pemuda/detail/<?=$p->id_pemuda?>"><?=$p->nama_pemuda?> </a></h4>
                <span><?=$p->nama_kategori_pemuda?></span>
              </div>

             
            </div>
          </div>
          
          <!-- Details -->
          <div class="freelancer-details">
          
            <a href="<?php echo base_url();?>pemuda/detail/<?=$p->id_pemuda?>" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
          </div>
        </div>
        <!-- Freelancer / End -->



        <?php }
?>



      </div>
      <!-- Freelancers Container / End -->


      <!-- Pagination -->
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12">
          <!-- Pagination -->
          <div class="pagination-container margin-top-40 margin-bottom-60">
            <nav class="pagination">
              <ul>

               <?php
                $CI =& get_instance();
                            $CI->load->library('pagination');

                            $config['base_url'] = base_url(). 'pemuda/index/';
                            $config['total_rows'] = $total_rows;
                            $config['per_page'] = $per_page;
                            $config['attributes'] = array('class' => 'ripple-effect');
                            $config['page_query_string']=TRUE;
                            $CI->pagination->initialize($config);
                            $link = $CI->pagination->create_links();

                      
                            $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                            $link = str_replace("</strong>", "</a></li>", $link);
                            $link = str_replace("&lt;", "Back", $link);
                            $link = str_replace("&gt;", "Next", $link);
                            $link = str_replace("<a ;", "<li><a ", $link);
                             $link = str_replace("</a> ;", "<li></a> ", $link);
                            echo $link;
              ?>





                
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <!-- Pagination / End -->

    </div>
  </div>
</div>


<?php $this->load->view('blog/src/footer');?>


</body>
</html>