<!DOCTYPE html>
<html lang="en">
  <head>
    
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DPMPTSP Kab. Bogor</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url()."asset/admin/" ;?>css/custom.css" rel="stylesheet">

   <!-- Select2 -->
    <link href="<?php echo base_url()."asset/admin/" ;?>js/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url()."asset/plugins/" ;?>jQuery/jQuery-2.1.3.min.js"></script>
  
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center">
             <h1 align="center"><img src="<?php echo base_url()."asset/logo/" ;?>logo.png" width="300px" height="80px"></h1>
              <h3 style="color:#ffffff;" >Verifikasi SK</h3>
              <h4 style="color:#ff8400; margin-top:-13px;">Silakan Masukan Nomer SK dibawah</h4>
              <div class="mid_center">
               
                 <form action="<?php echo base_url().'verifikasi/nosk/' ;?>" method="POST">
                  <div class="col-xs-12 form-group pull-right top_search" style="margin-top:-5px;">
                    <div class="input-group">
                      <input type="text" class="form-control" name="nomer_sk" placeholder="Nomer SK" required>
                      <span class="input-group-btn">
                              <button class="btn btn-success" style="color:#ffffff;" type="submit"><strong>Verifikasi</strong></button>
                          </span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>
 

        <!-- footer content -->
        
        <!-- /footer content -->
      </div>
    </div>

     <?php $this->load->view('admin/src/bottom');?>
   
    <!-- /gauge.js -->
  </body>
  
</html>


