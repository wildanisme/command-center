
<?php $this->load->view('blog/src/header');?>



<!-- Wrapper -->
<div id="wrapper">


  <div class="clearfix"></div>
  <!-- Header Container / End -->



  <!-- Container / Start -->
  <div class="container">





    <div class="row">

      <!-- Profile -->
      <div class="col-lg-4 col-md-4">

      <?php $this->load->view('cityzen/src/menu');?>
        



      </div>

      <!-- Change Password -->
      <div class="col-lg-8 col-md-8 " style="margin-top: 30px;">
        <div class="notification success closeable margin-bottom-30">
          <p>Akun anda belum <strong>Terverifikasi</strong> silakan lengkapi akun anda!</p>
          <a class="close"></a>
        </div>
       <div class="row">

        <div class="col-lg-4 col-md-4 col-xs-6">
          <div class="dashboard-stat ">
            <div class="dashboard-stat-content"><h4>6</h4> <span>Direktori</span></div>
            <div class="dashboard-stat-icon"><i class="im im-icon-Map2"></i></div>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-xs-6">
          <div class="dashboard-stat ">
          <div class="dashboard-stat-content"><h4>726</h4> <span>Agenda</span></div>
          <div class="dashboard-stat-icon"><i class="im im-icon-Line-Chart"></i></div>
        </div>
        </div>

        <div class="col-lg-4 col-md-4 col-xs-6">
        <div class="dashboard-stat ">
          <div class="dashboard-stat-content"><h4>95</h4> <span>Organisasi</span></div>
          <div class="dashboard-stat-icon"><i class="im im-icon-Add-UserStar"></i></div>
        </div>
        </div>


      </div>

      <div class="row">
      
      <!-- Recent Activity -->
      <div class="col-lg-6 col-md-12">
        <div class="dashboard-list-box with-icons margin-top-20">
          <h4>Log Aktifitas</h4>
          <ul>
            <li>
              <i class="list-box-icon sl sl-icon-layers"></i> Your listing <strong><a href="#">Hotel Govendor</a></strong> has been approved!
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-star"></i> Kathy Brown left a review <div class="numerical-rating high" data-rating="5.0"></div> on <strong><a href="#">Burger House</a></strong>
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-heart"></i> Someone bookmarked your <strong><a href="#">Burger House</a></strong> listing!
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-star"></i> Kathy Brown left a review <div class="numerical-rating mid" data-rating="3.0"></div> on <strong><a href="#">Airport</a></strong>
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-heart"></i> Someone bookmarked your <strong><a href="#">Burger House</a></strong> listing!
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-star"></i> John Doe left a review <div class="numerical-rating high" data-rating="4.0"></div> on <strong><a href="#">Burger House</a></strong>
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>

            <li>
              <i class="list-box-icon sl sl-icon-star"></i> Jack Perry left a review <div class="numerical-rating low" data-rating="2.5"></div> on <strong><a href="#">Tom's Restaurant</a></strong>
              <a href="#" class="close-list-item"><i class="fa fa-close"></i></a>
            </li>
          </ul>
        </div>
      </div>
      
      <!-- Invoices -->
      <div class="col-lg-6 col-md-12">
        <div class="dashboard-list-box invoices with-icons margin-top-20">
          <h4>Daftar Pengajuan</h4>
          <ul>
            
            <li><i class="list-box-icon sl sl-icon-doc"></i>
              <strong>Professional Plan</strong>
              <ul>
                <li class="unpaid">Unpaid</li>
                <li>Order: #00124</li>
                <li>Date: 20/07/2017</li>
              </ul>
              <div class="buttons-to-right">
                <a href="dashboard-invoice.html" class="button gray">View Invoice</a>
              </div>
            </li>
            
            <li><i class="list-box-icon sl sl-icon-doc"></i>
              <strong>Extended Plan</strong>
              <ul>
                <li class="paid">Paid</li>
                <li>Order: #00108</li>
                <li>Date: 14/07/2017</li>
              </ul>
              <div class="buttons-to-right">
                <a href="dashboard-invoice.html" class="button gray">View Invoice</a>
              </div>
            </li>

            <li><i class="list-box-icon sl sl-icon-doc"></i>
              <strong>Extended Plan</strong>
              <ul>
                <li class="paid">Paid</li>
                <li>Order: #00097</li>
                <li>Date: 10/07/2017</li>
              </ul>
              <div class="buttons-to-right">
                <a href="dashboard-invoice.html" class="button gray">View Invoice</a>
              </div>
            </li>
            
            <li><i class="list-box-icon sl sl-icon-doc"></i>
              <strong>Basic Plan</strong>
              <ul>
                <li class="paid">Paid</li>
                <li>Order: #00091</li>
                <li>Date: 30/06/2017</li>
              </ul>
              <div class="buttons-to-right">
                <a href="dashboard-invoice.html" class="button gray">View Invoice</a>
              </div>
            </li>

          </ul>
        </div>
      </div>


      <!-- Copyrights -->
      <div class="col-md-12">
        <div class="copyrights">© 2017 Listeo. All Rights Reserved.</div>
      </div>
    </div>
    </div>



  </div>
  <!-- Container / End -->


  <!-- Spacer -->
  <div class="padding-top-40"></div>
  <!-- Spacer -->

  <!-- Pagination / End -->



  <?php $this->load->view('blog/src/footer');?>

  <!-- Maps -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMvwWO0aCn876VcbHvGHtgzBNRm8HMVEc"></script>
  <script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
  <script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>