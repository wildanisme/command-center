
  <?php $this->load->view('blog/src/header');?>
  





  <div class="clearfix"></div>
  <!-- Header Container / End -->


<!-- Content
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Agenda</h2>
        <span>Disparbudpora Kab. Bogor</span>

        <!-- Breadcrumbs -->
        <nav id="breadcrumbs" class="">
          <ul>
            <li><a href="<?php echo base_url();?>home">Beranda</a></li>
            <li><a href="<?php echo base_url();?>agenda">Agenda</a></li>
            <li>Detail</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<!-- Container / Start -->
<div class="container">

  <div class="row">

    <!-- Contact Details -->
    <div class="col-md-4">


      <img src="<?php echo base_url();?>data/agenda/<?=$detail->poster?>" alt="" class="margin-top-30">
     
      <!-- Contact Details -->
      <div class="sidebar-textbox">
        

        <ul class="contact-details">
          <li><i class="icon-material-baseline-star-border"></i> <strong>Kategori:</strong> <span><?=$detail->nama_kategori_agenda?></span></li>
          <li><i class="icon-material-outline-access-time">  </i> <strong>Tanggal Mulai:</strong> <span><?=$detail->tgl_mulai_agenda?></span></li>
          <li><i class="icon-material-outline-alarm-on"></i> <strong>Sampai Tanggal:</strong> <span><?=$detail->tgl_akhir_agenda?></a></span></li>
          <li><i class="icon-material-outline-where-to-vote"></i> <strong>Alamat:</strong> <span><?=$detail->alamat?></a></span></li>
          
          
        </ul>
      </div>

    </div>

    <!-- Contact Form -->
    <div class="col-md-8">
 <h4 class="headline margin-bottom-5 margin-top-20"><?=$detail->nama_agenda?></h4>
 <p><?=$detail->deskripsi?></p>


  <!-- Google Maps -->
  <div id="singleListingMap-container">
    <div id="singleListingMap" data-latitude="<?= $detail->latitude;?>" data-longitude="<?=$detail->longitude;?>" data-map-icon="im im-icon-Map2"></div>
  
  </div>
  <!-- Google Maps / End -->

      


    </div>
    <!-- Contact Form / End -->

  </div>

</div>
<!-- Container / End -->


<!-- Spacer -->
<div class="padding-top-40"></div>
<!-- Spacer -->

<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>

<!-- Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMvwWO0aCn876VcbHvGHtgzBNRm8HMVEc"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/infobox.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo base_url().'asset/wisata/';?>scripts/maps.js"></script>


</body>
</html>