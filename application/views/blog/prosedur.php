<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Prosedur dan Persyaratan</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content" style='min-height:440px;'>
      <div class="container">
        <div class="row blog-page">

          <!-- Start Blog Posts -->
          <div class="col-md-12 blog-box">
          


          <div class="panel-group" id="accordion">

                    <?php foreach ($query as $row) :?>
                    <!-- izin Gangguan -->
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?=$row->id_prosedur;?>">
                              <?=$row->category_name;?>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse-<?=$row->id_prosedur;?>" class="panel-collapse collapse">
                          <div class="panel-body">
                              <h4><?=$row->nama;?></h4><hr style="margin: 2px 0;" />
                              <p><?=$row->isi;?></p>
                          </div>
                        </div>
                      </div>
                    <!-- akhir izin Gangguan -->
                    <?php endforeach;?>
            
          </div>
      </div>
    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>