<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header_home');?>
  

</head>

<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
  ================================================== -->
  <header id="header-container" >

    <?php $this->load->view('blog/src/navigasi_atas');?>

  </header>
  <div class="clearfix"></div>
  <!-- Header Container / End -->



  <!-- Spacer -->
  <div class="margin-top-90"></div>
  <!-- Spacer / End-->







  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-4">
        <div class="sidebar-container">
		<?= form_open() ;?>
          <!-- Nama Layanan -->
        <div class="sidebar-widget">
          <h3>Cari</h3>
          <div class="input-with-icon">
            <div id="autocomplete-container">
              <input id="keyword" type="text" name="cari" value="<?= !empty($cari) ? $cari : '' ;?>" placeholder="Nama Instansi / Layanan">
            </div>
            
          </div>
        </div>
 

        <!-- Tags -->
        <div class="sidebar-widget">
          <h3>Tags</h3>

          <div class="tags-container">
      <?php 
      
      foreach($tag as $key=>$value){
        $checked = !empty($tags) && in_array($value,$tags) ? "checked" : "";
        if($value!=""){
				?>
		  
            <div class="tag">
              <input type="checkbox" <?=$checked;?> value="<?=$value;?>" name="tags[]" id="tag<?=$key;?>"/>
              <label for="tag<?=$key;?>"><?=$value;?></label>
            </div>
			<?php } }?>
            
           
          </div>
          <div class="clearfix"></div>
        </div>


      <!-- Search Button -->
      <div class="sidebar-search-button-container">
        <button class="button ripple-effect" onclick="" >Cari</button>
      </div>
	  </form>
      <!-- Search Button / End-->

          <div class="clearfix"></div>

        </div>
      </div>
      <div class="col-xl-9 col-lg-8 content-left-offset">

       

        <div class="notify-box margin-top-15">
          <div class="switch-container">
       <span class="switch-text">Daftar Layanan</span></label>
          </div>

       
        </div>
	<div class="row">
        <!-- Tasks Container -->
        
          <?php foreach($layanan as $l){
            ?>
			
			<div class="col-md-4 dashbord-box" >
			<div class="companies-list" >
            <a href="<?php echo base_url();?>layanan/detail/<?=$l->id_layanan?>" class="company">
              <div class="company-inner-alignment" style="padding-top:40px;" >


              <span class="company-logo">   <div class="circle" style="margin-bottom: 0px">
                <div><?=$l->loket;?></div>
              </div></span>
              <br>

              <div style="min-height: 80px">           
                <h4 ><?=$l->nama_layanan;?></h4>

              </div>
              <div style="border-top: 1px solid #d2d2d2;padding-top: 20px;padding-left:10px;padding-right:10px; min-height:80px;vertical-align:middle;">
                <p style="font-size: 13px;line-height: 1.5; color: #858585; vertical-align:middle" ><?=$l->nama_skpd;?>

                </div>
              </div>
            </a>
			     </div>
		</div>
            <?php
          }
          ?>


   
</div>


        <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-10 margin-bottom-20">
              <nav class="pagination">
                <ul>
                   <?php
                if(count($layanan) > 0){
                  $link = make_pagination($pages,$current);

                  /*
                      
                  $link = str_replace("<strong>", "<li><a href class='current-page ripple-effect' >", $link);
                  $link = str_replace("</strong>", "</a></li>", $link);
                  $link = str_replace("&lt;", "Back", $link);
                  $link = str_replace("&gt;", "Next", $link);
                  $link = str_replace("<a ;", "<li><a ", $link);
                   $link = str_replace("</a> ;", "<li></a> ", $link);
                   */
                  $link = str_replace("btn ", "button ", $link);
                            echo $link;
                }
              ?>

                  
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <!-- Pagination / End -->



      </div>


    
    </div>
  </div>
</div>
<div class="clearfix"></div>
<!-- Pagination / End -->



<?php $this->load->view('blog/src/footer');?>


</body>
</html>