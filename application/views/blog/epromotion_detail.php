<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body>
  
    
    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
  
    <!-- End Page Banner -->

<!-- Content
================================================== -->

<div class="clearfix"></div>
            <!-- Slider
================================================== -->
<div class="listing-slider mfp-gallery-container margin-bottom-0">
    <a href="http://xplora.id/img/information/hotel/1/1503898031.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503898031.jpg" class="item mfp-gallery" title="1503898031.jpg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503898025.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503898025.jpg" class="item mfp-gallery" title="1503898025.jpg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503898019.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503898019.jpg" class="item mfp-gallery" title="1503898019.jpg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503898013.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503898013.jpg" class="item mfp-gallery" title="1503898013.jpg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503898006.jpeg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503898006.jpeg" class="item mfp-gallery" title="1503898006.jpeg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503897999.jpeg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503897999.jpeg" class="item mfp-gallery" title="1503897999.jpeg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503897986.jpeg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503897986.jpeg" class="item mfp-gallery" title="1503897986.jpeg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503897965.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503897965.jpg" class="item mfp-gallery" title="1503897965.jpg"></a>
    <a href="http://xplora.id/img/information/hotel/1/1503897959.jpg" 
    data-background-image="http://xplora.id/img/information/hotel/1/1503897959.jpg" class="item mfp-gallery" title="1503897959.jpg"></a>
  </div>

<!-- Content
================================================== -->
<div class="container">
  <div class="row sticky-wrapper">
    <div class="col-lg-8 col-md-8 padding-right-30">

      <!-- Titlebar -->
      <div id="titlebar" class="listing-titlebar">
        <div class="listing-titlebar-title">
          <h2>Sheraton Surabaya Hotel and Towers      Beranda     Surabaya     DETAIL HOTEL <span class="listing-tag">Serviced Apartement</span></h2>
          <span>
            <a href="#listing-location" class="listing-address">
              <i class="fa fa-map-marker"></i>
              Jalan Embong Malang 25-31, Surabaya, East Java, 60261, Indonesia, Kedungdoro,  Tegalsari, Kota Surabaya, Jawa Timur
            </a>
          </span>
          <div class="row">
              <span style='padding-left:20px'><i class="fa fa-eye"></i>  Dilihat 66 kali</span>
            </div>

          <h6>Terverifikasi</h6>
                              <br>
          <h4><i class="sl sl-icon-wallet"></i>  Harga mulai dari : 0</h4>
                    
        </div>
      </div>

      <!-- Listing Nav -->
      <div id="listing-nav" class="listing-nav-container">
        <ul class="listing-nav">
          <li><a href="#description" class="active">Deskripsi</a></li>
                                        <li><a href="#location">Lokasi</a></li>
          <li><a href="#reviews">Katalog</a></li>
                  </ul>
      </div>
      
      <!-- Overview -->
      <div id="description" class="listing-section">

        <!-- Description -->

        <p>If you seek typical East Java hospitality and luxury, look no further 
than Sheraton Surabaya Hotel &amp; Towers. We are located in <a target="_blank" rel="nofollow" href="http://www.sheratonsurabaya.com/local-area">Surabaya city center</a>&nbsp;and
 have direct connection to the famous shopping malls in East Java, 
Tunjungan Plaza, and a few minutes away from many local attractions.<br></p>

      </div>

                
          
          
            <div id="facilities" class="listing-section">
        <h3 class="listing-desc-headline margin-top-20 margin-bottom-30">Fasilitas</h3>

        <div class="show-more">
          
          <div class="row">
            
                                
                    <div class="col-md-12 margin-bottom-20">
                      <h5><strong># Fasilitas dan layanan hotel</strong></h5>
                      <ul class="listing-features margin-top-0">
                                                                        <li><i class="fa fa-check"></i> Banquet hall</li>
                                                                                                <li><i class="fa fa-check"></i> Bar</li>
                                                                                                <li><i class="fa fa-check"></i> BBQ</li>
                                                                                                <li><i class="fa fa-check"></i> Billiard</li>
                                                                                                <li><i class="fa fa-check"></i> Business center</li>
                                                                                                <li><i class="fa fa-check"></i> Cleaning service</li>
                                                                                                <li><i class="fa fa-check"></i> Coffee Shop</li>
                                                                                                <li><i class="fa fa-check"></i> Conference room</li>
                                                                                                <li><i class="fa fa-check"></i> Convenience store</li>
                                                                                                <li><i class="fa fa-check"></i> Dry room</li>
                                                                                                <li><i class="fa fa-check"></i> Esthetics</li>
                                                                                                <li><i class="fa fa-check"></i> Field</li>
                                                                                                <li><i class="fa fa-check"></i> Fitness</li>
                                                                                                <li><i class="fa fa-check"></i> Game corner</li>
                                                                                                <li><i class="fa fa-check"></i> Gymnasium</li>
                                                                                                <li><i class="fa fa-check"></i> Ice-making machine</li>
                                                                                                <li><i class="fa fa-check"></i> Indoor pool</li>
                                                                                                <li><i class="fa fa-check"></i> Jacuzzi</li>
                                                                                                <li><i class="fa fa-check"></i> Karaoke</li>
                                                                                                <li><i class="fa fa-check"></i> Kids club</li>
                                                                                                <li><i class="fa fa-check"></i> Layanan Kamar</li>
                                                                                                <li><i class="fa fa-check"></i> Lounge</li>
                                                                                                <li><i class="fa fa-check"></i> Massage</li>
                                                                                                <li><i class="fa fa-check"></i> Morning Call</li>
                                                                                                <li><i class="fa fa-check"></i> Non-smoking rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Outdoor pool</li>
                                                                                                <li><i class="fa fa-check"></i> Parking Area</li>
                                                                                                <li><i class="fa fa-check"></i> Rental cycle</li>
                                                                                                <li><i class="fa fa-check"></i> Restaurant</li>
                                                                                                <li><i class="fa fa-check"></i> Sauna</li>
                                                                                                <li><i class="fa fa-check"></i> Spa</li>
                                                                                                <li><i class="fa fa-check"></i> Table tennis</li>
                                                                                                <li><i class="fa fa-check"></i> Tennis</li>
                                                                                                <li><i class="fa fa-check"></i> Voltage Converter</li>
                                                                    </ul>
                    </div>
                                      
                    <div class="col-md-12 margin-bottom-20">
                      <h5><strong># Aktivitas Olahraga</strong></h5>
                      <ul class="listing-features margin-top-0">
                                                                        <li><i class="fa fa-check"></i> Canoe</li>
                                                                                                <li><i class="fa fa-check"></i> Diving</li>
                                                                                                <li><i class="fa fa-check"></i> Field</li>
                                                                                                <li><i class="fa fa-check"></i> Fishing</li>
                                                                                                <li><i class="fa fa-check"></i> Golf</li>
                                                                                                <li><i class="fa fa-check"></i> Gymnasium</li>
                                                                                                <li><i class="fa fa-check"></i> Horse riding</li>
                                                                                                <li><i class="fa fa-check"></i> Paraglider</li>
                                                                                                <li><i class="fa fa-check"></i> Putting golf</li>
                                                                                                <li><i class="fa fa-check"></i> Rafting</li>
                                                                                                <li><i class="fa fa-check"></i> Tennis</li>
                                                                    </ul>
                    </div>
                                      
                    <div class="col-md-12 margin-bottom-20">
                      <h5><strong># Internet & Wifi</strong></h5>
                      <ul class="listing-features margin-top-0">
                                                                        <li><i class="fa fa-check"></i> Bebas akses Wi-Fi di lobby</li>
                                                                                                <li><i class="fa fa-check"></i> Kabel LAN</li>
                                                                                                <li><i class="fa fa-check"></i> Koneksi dial-up</li>
                                                                                                <li><i class="fa fa-check"></i> Koneksi internet gratis</li>
                                                                                                <li><i class="fa fa-check"></i> Koneksi LAN (semua kamar)</li>
                                                                                                <li><i class="fa fa-check"></i> Tersedia rental PC</li>
                                                                                                <li><i class="fa fa-check"></i> Wireless LAN</li>
                                                                    </ul>
                    </div>
                                      
                    <div class="col-md-12 margin-bottom-20">
                      <h5><strong># Fasilitas Kamar</strong></h5>
                      <ul class="listing-features margin-top-0">
                                                                        <li><i class="fa fa-check"></i> Air conditioner: All rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Bath and Toilet: All rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Bath towel</li>
                                                                                                <li><i class="fa fa-check"></i> Bathrobe</li>
                                                                                                <li><i class="fa fa-check"></i> Bathtub: All rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Body soap</li>
                                                                                                <li><i class="fa fa-check"></i> BS broadcast</li>
                                                                                                <li><i class="fa fa-check"></i> Comb</li>
                                                                                                <li><i class="fa fa-check"></i> Cottonbud</li>
                                                                                                <li><i class="fa fa-check"></i> Face towel, Hand towel</li>
                                                                                                <li><i class="fa fa-check"></i> Hair Conditioner</li>
                                                                                                <li><i class="fa fa-check"></i> Hair Dryer</li>
                                                                                                <li><i class="fa fa-check"></i> Kitchen: All rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Mini Bar</li>
                                                                                                <li><i class="fa fa-check"></i> Pajamas</li>
                                                                                                <li><i class="fa fa-check"></i> Razor and/or Shaver</li>
                                                                                                <li><i class="fa fa-check"></i> Refrigerator</li>
                                                                                                <li><i class="fa fa-check"></i> Safety box</li>
                                                                                                <li><i class="fa fa-check"></i> Sandal</li>
                                                                                                <li><i class="fa fa-check"></i> Shampoo</li>
                                                                                                <li><i class="fa fa-check"></i> Shower cap</li>
                                                                                                <li><i class="fa fa-check"></i> Shower Toilet</li>
                                                                                                <li><i class="fa fa-check"></i> Shower: All rooms</li>
                                                                                                <li><i class="fa fa-check"></i> Soap</li>
                                                                                                <li><i class="fa fa-check"></i> Toothbrush and/or Toothpaste</li>
                                                                                                <li><i class="fa fa-check"></i> Trouser press (*including rental)</li>
                                                                                                <li><i class="fa fa-check"></i> TV</li>
                                                                                                <li><i class="fa fa-check"></i> VCR atau DVD</li>
                                                                    </ul>
                    </div>
                                  
          </div>

        </div>
        <a href="#" class="show-more-button" data-more-title="Tampilkan lebih" data-less-title="Sembunyikan"><i class="fa fa-angle-down"></i></a>
      </div>
      <div id="room" class="listing-section">
        <h3 class="listing-desc-headline margin-top-20 margin-bottom-30">Kamar</h3>
        
                  <div class="pricing-list-container">
                      </div>
        
      </div>
      
      
      
            
      <!-- Location -->
      <div id="location" class="listing-section">
        <h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Lokasi</h3>

        <div id="singleListingMap-container">
          <div id="singleListingMap" data-latitude="-7.26149968" data-longitude="112.735755" data-map-icon="im im-icon-Hotel"></div>
          <a href="#" id="streetView">Street View</a>
        </div>
      </div>
        
      
      
    </div>


    <!-- Sidebar
    ================================================== -->
    <div class="col-lg-4 col-md-4 margin-top-75 sticky">

            
      <div class="boxed-widget opening-hours margin-top-35">
        <h3><i class="sl sl-icon-clock"></i> Jam operasional</h3>
        <ul>
                                            <li>Senin <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Selasa <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Rabu <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Kamis <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Jumat <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Sabtu <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Mingu <span>07:00 AM - 05:00 PM</span></li>
                                  <li>Hari Libur <span>07:00 AM - 05:00 PM</span></li>
          
                  </ul>
        <div class="listing-badge now-closed">Sekarang Tutup</div>
      </div>
      
      
      <!-- Contact -->
      <div class="boxed-widget margin-top-35">
        <h3><i class="sl sl-icon-pin"></i> Kontak</h3>
        <ul class="listing-details-sidebar">
                    <li><i class="sl sl-icon-phone"></i> 08071 088888</li>
                              <li><i class="sl sl-icon-globe"></i> <a target='_blank' href="http://www.sheratonsurabaya.com/">http://www.sheratonsurabaya.com/</a></li>
                            </ul>

                <ul class="listing-details-sidebar social-profiles">
                                                                                                                                                                                                  </ul>
        
        <!-- Reply to review popup --
        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
          <div class="small-dialog-header">
            <h3>Send Message</h3>
          </div>
          <div class="message-reply margin-top-0">
            <textarea cols="40" rows="3" placeholder="Your message to Burger House"></textarea>
            <button class="button">Send Message</button>
          </div>
        </div>

        <a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>
        -->
      </div>
      <!-- Contact / End-->

      <!-- Share / Like -->
      <div class="listing-share margin-top-40 margin-bottom-40 no-border">

                <i class="sl sl-icon-heart" style='font-size:20px'></i>
        
        <span>0 Orang menyukai list ini</span>

          <!-- Share Buttons -->
          <ul class="share-buttons margin-top-40 margin-bottom-0">
            <li><a class="fb-share" onclick="share('fb')" target="_parent" href="javascript: void(0)"><i class="fa fa-facebook"></i> Share</a></li>
            <li><a class="twitter-share" onclick="share('twitter')" target="_parent" href="javascript: void(0)"><i class="fa fa-twitter"></i> Tweet</a></li>
            <li><a class="gplus-share" onclick="share('google')" target="_parent" href="javascript: void(0)"><i class="fa fa-google-plus"></i> Share</a></li>
            <!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
          </ul>
          <div class="clearfix"></div>

      </div>

    </div>
    <!-- Sidebar / End -->
  </div>
</div>



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>