<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>

<link rel="stylesheet" href="<?php echo base_url().'asset/bootstrap-vertical-tabs-master/';?>bootstrap.vertical-tabs.css">
</head>

<body class="construction">
  <div id="container">

    <!-- Start Header -->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>
    <!-- Breadcrumbs -->
    <nav id="breadcrumbs">
      <ul>
        <li><a href="#">Home</a></li>
        <li>Loker</li>
      </ul>
    </nav>
    <!-- Parallax -->
    <div class="parallax"
    data-background="<?php echo base_url();?>asset/list/images/slider-bg-02.jpg"
    data-color="#36383e"
    data-color-opacity="0.6"
    data-img-width="800"
    data-img-height="505">

    <!-- Infobox -->
    <div class="text-content white-font">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 col-sm-8">
            <h2>E-Loker</h2>
            <p>Masyarakat dapat menginformasikan lowongan pekerjaan atau mencari lowongan perkerjaan dengan menggunakan sistem ini. </p>
            <a href="listings-list-with-sidebar.html" class="button margin-top-25">Pelajari Selengkapnya</a>
          </div>
        </div>

      </div>
    </div>

    <!-- Infobox / End -->

  </div>
  <!-- Parallax / End -->



<!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

         <span>Daftar Lowongan Pekerjaan</span>
         <hr>


       </div>
     </div>
   </div>
 </div>


<!-- Content
  ================================================== -->
  <div class="container">
    <div class="row">


      <form method="GET">    
        <!-- Search -->
        <div class="col-md-12">
          <div class="main-search-input gray-style margin-top-0 margin-bottom-10">

            <div class="main-search-input-item">
              <input type="text" name="s" placeholder="Cari berdasarkan nama loker" value=""/>
            </div>

            <div class="main-search-input-item">
              <select name="c" data-placeholder="Tingkat Pendidikan" class="chosen-select" >          
                <option value="Semua Tingkat">Semua Tingkat</option>
                <option value="SD-SMP">SD-SMP</option>
                <option value="SMA/SMK Sederajat">SMA/SMK Sederajat</option>
                <option value="D3">D3</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
              </select>
            </div>

            <button class="button" type="submit">Search</button>
          </div>
        </div>
        <!-- Search Section / End -->
      </form>


      <div class="col-md-12">

        <!-- Sorting - Filtering Section -->
        <div class="row margin-bottom-25 margin-top-30">

          <div class="col-md-6">
            <!-- Layout Switcher -->
            <div class="layout-switcher">
              <a href="listings-grid-full-width.html" class="grid"><i class="fa fa-th"></i></a>
              <a href="#" class="list active"><i class="fa fa-align-justify"></i></a>
            </div>
          </div>

          <div class="col-md-6">
            <div class="fullwidth-filters">



            </div>
          </div>

        </div>
        <!-- Sorting - Filtering Section / End -->

        <div class="row">

          <?php foreach($query as $q){?>
            <!-- Listing Item -->
            <div class="col-lg-4 col-md-6">
              <a href="<?php echo base_url();?>eloker/detail/<?php echo $q->id_loker ?>" class="listing-item-container compact">
                <div class="listing-item">
                  <img src="<?php echo base_url();?>asset/list/images/listing-item-0<?php echo rand(1,6) ?>.jpg" alt="">

                  <div class="listing-badge now-<?php echo $q->availability ?>"><?php echo ucwords($q->availability) ?></div>
                  <div class="listing-item-details">
                    <ul>
                      <li>Batas waktu: <?php echo tanggal($q->tgl_tutup) ?></li>
                    </ul>
                  </div>
                  <div class="listing-item-content">
                    <h3><?php echo ucwords($q->posisi) ?></h3>
                    <span><?php echo $q->nama_perusahaan ?></span>
                  </div>
                </div>
              </a>
            </div>
            <!-- Listing Item / End -->
          <?php } ?>

        </div>

        <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-20 margin-bottom-40">

              <nav class="pagination">

                <?php
                $CI =& get_instance();
                $CI->load->library('pagination');

                $this->config->load('bootstrap_pagination');
                $config = $this->config->item('pagination');  

                $config['base_url'] = base_url(). 'eloker';
                $config['total_rows'] = $total_rows;
                $config['per_page'] = $per_page;
                $config['page_query_string']=TRUE;

                $config['reuse_query_string'] = TRUE;

                $CI->pagination->initialize($config);
                $link = $CI->pagination->create_links();
                echo $link;
                ?>

              </nav>
            </div>
          </div>
        </div>
        <!-- Pagination / End -->

      </div>

    </div>
  </div>



  <!-- End Content -->
  <footer>
    <?php $this->load->view('blog/src/footer');?>
  </footer>
</div>
</body>
</html>