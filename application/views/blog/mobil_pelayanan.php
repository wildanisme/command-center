<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">
<head>
  <?php $this->load->view('blog/src/header');?>
  <style type="text/css">
  .marginleft2px{
    margin-left: 2px;
  }
</style>
</head>

<body>
  <div id="container">
    
    <!-- Start Header -->
    <!--div class="hidden-header"></div-->
    <header class="clearfix">
      <?php $this->load->view('blog/src/top_nav');?>
    </header>

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="putih">Jadwal Mobil Pelayanan</h2>
           
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href=" <?php echo base_url().'home'; ?> ">Home</a></li>
              <li><a href=" <?php echo base_url().'mobil_pelayanan'; ?> ">Jadwal Mobil Pelayanan</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->

    <!-- Start Content -->
    <div id="content" style='min-height:440px;'>
      <div class="container">
        <div class="row blog-page">
        <div class="col-md-5">
          <img src="<?php echo base_url();?>data/icon/mobil.png" class="img-responsive">
        </div>
          <!-- Start Blog Posts -->
          <div class="col-md-7 blog-box">
          

                        <div id="tab-content" class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="tab1">
                               <p>Berikut adalah jadwal pelayanan mobil keliling <strong>DPMPTSP Kab. Bogor</strong></p>
                            <table class="table table-stripped">
                            <tr class="active"><th>Tanggal</th><th>Lokasi</th><th>Waktu</th></tr>
                            <?php foreach ($jadwal_mobil as $key) :?>
                            <tr><td><?php $tgl = date_create($key->tgl); echo date_format($tgl, "j F Y"); ?></td><td><?php echo $key->lokasi; ?></td><td><?php echo date("H:i",strtotime($key->waktu_awal))." - ".date("H:i",strtotime($key->waktu_akhir)); ?></td></tr>
                              <?php endforeach; ?>


                            </table>
                            
                        </div>
                    </div>
      </div>
    </div>
  </div>
  </div>
    <!-- End Content -->



    <!-- End Content -->
    <footer>
      <?php $this->load->view('blog/src/footer');?>
    </footer>
  </div>
</body>
</html>