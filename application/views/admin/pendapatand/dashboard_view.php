


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
              <div class="media-body"><span class="m-0">Pajak Daerah : </span>
                <h4 class="mb-0 counter"> <i class="icon-bg"></i>
                <?= (@$data_esakip->jml_skpd) ? number_format($data_esakip->jml_skpd) : 0?></h1><i class="icon-bg" data-feather="database"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
    <div class="card gradient-primary o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
          <div class="media-body"><span class="m-0 text-white">Retribusi Daerah</span>
            <h4 class="mb-0 counter"><?php foreach ($jumusaha as $data): ?>
          
				<?php echo $data->total ?> 
            <?php endforeach; ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
    <div class="card gradient-warning o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center">
            <div class="text-white i" data-feather="database"></div>
          </div>
          <div class="media-body"><span class="m-0 text-white">Kekayaan Dipisah</span>
           <h4 class="mb-0 counter text-white"> <?php foreach ($jumaset as $data): ?>

          
			 <?php echo  number_format($data->total,2,',','.') ?> 
			   
			   <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Lain-lain PAD sah</span>
          <h4 class="mb-0 counter text-white"><?php foreach ($jumahasil as $data): ?>

            
			 <?php echo  number_format($data->total,2,',','.') ?>   
			  <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">PAD Transfer</span>
          <h4 class="mb-0 counter text-white"><?php foreach ($jumahasil as $data): ?>

            
			 <?php echo  number_format($data->total,2,',','.') ?>   
			  <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Transfer Daerah</span>
          <h4 class="mb-0 counter text-white"><?php foreach ($jumahasil as $data): ?>

            
			 <?php echo  number_format($data->total,2,',','.') ?>   
			  <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- <div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Capaian SKPD </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Capaian Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div> -->
<!-- <div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Capaian SAKIP</h5>
    </div>
    <div class="card-body progress-showcase row">
        <div class="col-md-3">
          <h5>Perangkat Daerah</h5>
        </div>
        <div class="col-md-9">
          perencanaan kinerja
          <div class="progress">
            <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 92.460%" aria-valuenow="92.460" aria-valuemin="0" aria-valuemax="100"> 92.460 %</div>
          </div>
          pengukuran kinerja
          <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 90.000%" aria-valuenow="90.000" aria-valuemin="0" aria-valuemax="100"> 90.000 %</div>
          </div>
          pelaporan kinerja
          <div class="progress">
            <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 69.467%" aria-valuenow="69.467" aria-valuemin="0" aria-valuemax="100"> 69.467 %</div>
          </div>
          evaluasi internal
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 69.500%" aria-valuenow="69.500" aria-valuemin="0" aria-valuemax="100"> 69.500</div>
          </div>
          pencapaian sasaran
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 71.460%" aria-="" valuenow="71.460" aria-valuemin="0" aria-valuemax="100">71.460</div>
          </div>
        </div>
    </div> -->
    <!-- <div class="card-body progress-showcase row">
        <div class="col-md-3">
          <h5>Kecamatan</h5>
        </div>
        <div class="col-md-9">
          perencanaan kinerja
          <div class="progress">
            <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 92.460%" aria-valuenow="92.460" aria-valuemin="0" aria-valuemax="100"> 92.460 %</div>
          </div>
          pengukuran kinerja
          <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 90.000%" aria-valuenow="90.000" aria-valuemin="0" aria-valuemax="100"> 90.000 %</div>
          </div>
          pelaporan kinerja
          <div class="progress">
            <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 69.467%" aria-valuenow="69.467" aria-valuemin="0" aria-valuemax="100"> 69.467 %</div>
          </div>
          evaluasi internal
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 69.500%" aria-valuenow="69.500" aria-valuemin="0" aria-valuemax="100"> 69.500</div>
          </div>
          pencapaian sasaran
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 71.460%" aria-="" valuenow="71.460" aria-valuemin="0" aria-valuemax="100">71.460</div>
          </div>
        </div>
    </div>
  </div> -->
</div>

<div class="col-lg-12 xl-100">
  <div class="row">

    <!-- <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
      <div class="card">
        <div class="card-header">
          <h5>Chart Pemilik Berdasarkan jenis kelamin </h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="piechart"></div>
        </div>
      </div>
    </div> -->


    

<!-- Plugins JS start-->
<!-- Plugins JS start-->






<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>