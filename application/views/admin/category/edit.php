 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Kategori Post</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url();?>manage_category">Kategori</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Edit</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">
                <?php if (!empty($message)) echo "
				<div class='alert alert-$message_type'>$message</div>";?>
				<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
					<div class="form-group">
						<label class="control">Kategori</label>
					
							<input type='hidden'  value='<?php echo $category_name;?>'  name='old_category' />
							<input type="text" id='category_name' value='<?php echo $category_name;?>' class="form-control" name='category_name' placeholder="">
							<p ><?php echo base_url();?>category/<label id='slug'><?php echo $category_slug;?></label></p>
							<input type="hidden" value='<?php echo $category_slug;?>' class="form-control" id='category_slug' name='category_slug' placeholder="">
					</div>

					<div class="form-group">
						<label class="control">Status</label>							
							<select name="category_status" class="form-control" data-first-option="false">
								<option>Pilih</option>
								<?php
									if ($category_status=="Active"){
										echo "
											<option value='Active' selected>Aktif</option>
											<option value='Not Active'>Nonaktif</option>
										";
									}
									else{
										echo "
											<option value='Active' >Aktif</option>
											<option value='Not Active' selected>Nonaktif</option>
										";
									}
								?>
							</select>
					</div>
					
					<div class="form-group">
						<div class="col-12">
							<button type="submit" class="btn btn-primary waves-effect waves-light pull-right mb-1" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Perbarui</button>
						</div>
						
					</div>
				</form>




                </div>
            </div>
        </div>
    </div>
</div>
</div>
