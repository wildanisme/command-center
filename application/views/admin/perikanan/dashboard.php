
<!DOCTYPE html>
<html lang="en">
  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
	<title>Admin</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fontawesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/animate.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/date-picker.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/owlcarousel.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/prism.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/whether-icon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/pe7-icon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/ionic-icon.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">
    <link id="color" rel="stylesheet" href="<?= base_url(); ?>assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/responsive.css">
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>Data Loading..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
      <div class="page-main-header">
        <div class="main-header-right">
          <div class="main-header-left text-center">
            <div class="logo-wrapper"><a href="index.html"><img src="<?= base_url(); ?>asset/dashboard/images/logo/logo.png" alt="" style="width: 150px"></a></div>
          </div>
          <div class="mobile-sidebar">
            <div class="media-body text-right switch-sm">
              <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
            </div>
          </div>
          <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar">               </i></div>
          <div class="nav-right col pull-right right-menu">
            <ul class="nav-menus">
              <li>
                
                  <div class="form-group">
                    <div class="Typeahead Typeahead--twitterUsers">
                      <div class="u-posRelative">
                        
                        <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                      </div>
                      <div class="Typeahead-menu"></div>
                    </div>
                  </div>
                </form>
              </li>
              <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/bookmark.png" alt="">
                <div class="onhover-show-div bookmark-flip">
                  <div class="flip-card">
                    <div class="flip-card-inner">
                      <div class="front">
                        <ul class="droplet-dropdown bookmark-dropdown">
                          <li class="gradient-primary text-center">
                            <h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
                          </li>
                          <li>
                            <div class="row">
                              <div class="col-4 text-center"><i data-feather="file-text"></i></div>
                              <div class="col-4 text-center"><i data-feather="activity"></i></div>
                              <div class="col-4 text-center"><i data-feather="users"></i></div>
                              <div class="col-4 text-center"><i data-feather="clipboard"></i></div>
                              <div class="col-4 text-center"><i data-feather="anchor"></i></div>
                              <div class="col-4 text-center"><i data-feather="settings"></i></div>
                            </div>
                          </li>
                          <li class="text-center">
                            <button class="flip-btn" id="flip-btn">Add New Bookmark</button>
                          </li>
                        </ul>
                      </div>
                      <div class="back">
                        <ul>
                          <li>
                            <div class="droplet-dropdown bookmark-dropdown flip-back-content">
                              <input type="text" placeholder="search...">
                            </div>
                          </li>
                          <li>
                            <button class="d-block flip-back" id="flip-back">Back</button>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="<?= base_url(); ?>assets/images/dashboard/like.png" alt="">
                <ul class="onhover-show-div droplet-dropdown">
                  <li class="gradient-primary text-center">
                    <h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
                    </div>
                  </li>
                  <li class="text-center">
                    <button class="btn btn-primary btn-air-primary">Follows Up</button>
                  </li>
                </ul>
              </li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/notification.png" alt="">
                <ul class="onhover-show-div notification-dropdown">
                  <li class="gradient-primary">
                    <h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
                  </li>
                  <li>
                    <div class="media">
                      <div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
                      <div class="media-body">
                        <h6>Someone Likes Your Posts</h6>
                        <p class="mb-0"> 2 Hours Ago</p>
                      </div>
                    </div>
                  </li>
                  <li class="pt-0">
                    <div class="media">
                      <div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
                      <div class="media-body">
                        <h6>3 New Comments</h6>
                        <p class="mb-0"> 1 Hours Ago</p>
                      </div>
                    </div>
                  </li>
                  <li class="bg-light txt-dark"><a href="#">All </a> notification</li>
                </ul>
              </li>
              <li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="<?= base_url(); ?>assets/images/dashboard/chat.png" alt=""></a></li>
              <li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="<?= base_url(); ?>assets/images/dashboard/user.png" alt=""></span>
                <ul class="onhover-show-div profile-dropdown">
                  <li class="gradient-primary">
                    <h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
                  </li>
                  <li><i data-feather="user"> </i>Profile</li>
                  <li><i data-feather="message-square"> </i>Inbox</li>
                  <li><i data-feather="file-text"> </i>Taskboard</li>
                  <li><i data-feather="settings"> </i>Settings            </li>
                </ul>
              </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
          </div>
          <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
          <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
        </div>
      </div>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
		<div class="iconsidebar-menu iconbar-mainmenu-close">
                <?php $this->load->view('admin/src/menu'); ?>
            </div>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6 main-header">
                  <h2>Dinas Perikanan & Peternakan</h2>
                  <h6 class="mb-0">Kabupaten Bogor</h6>
                </div>
                <div class="col-lg-6 breadcrumb-right">     
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
                    <li class="breadcrumb-item">Home</li>
                    <li class="breadcrumb-item active">Halaman Data</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid general-widget">
            <div class="row">
              <div class="col-sm-4 col-xl-3 col-lg-5 box-col-4">
                <div class="card gradient-primary o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="database"></i></div>
                      <div class="media-body"><span class="m-0 text-white">Daging (Ton)</span>
                        <h4 class="mb-0 counter">22.385</h4><i class="icon-bg" data-feather="database"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-secondary o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                      <div class="media-body"><span class="m-0">Telur (Ton)</span>
                        <h4 class="mb-0 counter">6.759</h4><i class="icon-bg" data-feather="shopping-bag"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-warning o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center">
                        <div class="text-white i" data-feather="message-circle"></div>
                      </div>
                      <div class="media-body"><span class="m-0 text-white">Susu (Ton)</span>
                        <h4 class="mb-0 counter text-white">8.619</h4><i class="icon-bg" data-feather="message-circle"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-info o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center">
                        <div class="text-white i" data-feather="user-plus"></div>
                      </div>
                      <div class="media-body"><span class="m-0 text-white">Ikan Konsumsi(Ton)</span>
                        <h4 class="mb-0 counter text-white">9.269</h4><i class="icon-bg" data-feather="user-plus"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 xl-100">
                <div class="card">
                  <div class="card-header">
                    <h5>Produksi Perikanan </h5>
                  </div>
                  <div class="card-body apex-chart p-0">
                    <div id="piechart"></div>
                  </div>
                </div>
              </div>

              <div class="col-sm-12 col-xl-6 xl-100">
                <div class="card">
                  <div class="card-header">
                    <h5>Populasi Ternak Besar dan Kecil </h5>
                  </div>
                  <div class="card-body p-0">
                    <div id="area-spaline"></div>
                  </div>
                </div>
              </div>

              <div class="col-sm-25 col-xl-12 xl-120">
                <div class="card">
                  <div class="card-header">
                    <h5>Populasi Unggas 2020</h5>
                  </div>
                  <div class="card-body p-0">
                    <div id="basic-bar"></div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12 xl-106 box-col-15">
                <div class="card o-hidden">
                  <div class="card-header">
                    <h5>Populasi Ternak 2020</h5>
                    <div class="card-header-right">
                      <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                        <li><i class="view-html fa fa-code font-primary"></i></li>
                        <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                        <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                        <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                        <li><i class="icofont icofont-error close-card font-primary"></i></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="user-status cart-table table-responsive">
                      <table class="table table-bordernone">
                        <thead>
                          <tr>
                            <th scope="col">Komoditi</th>
                            <th scope="col">Populasi Awal</th>
                            <th scope="col">Masuk/lahir</th>
                            <th scope="col">Dipotong</th>
                            <th scope="col">Keluar</th>
                            <th scope="col">Mati</th>
                            <th scope="col">Total Populasi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="f-w-600">Sapi Potong</td>
                            <td class="digits">30.458</td>
                            <td class="digits">18.412</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">6711</div>
                            </td>
                            <td class="digits">6.635</td>
                            <td class="digits">609</td>
                            <td class="digits">34.915</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Kerbau</td>
                            <td class="digits">2.349</td>
                            <td class="digits">324</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">3</div>
                            </td>
                            <td class="digits">356</td>
                            <td class="digits">47</td>
                            <td class="digits">2.267</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Kambing</td>
                            <td class="digits">29.220</td>
                            <td class="digits">11.177</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">470</div>
                            </td>
                            <td class="digits">2.465</td>
                            <td class="digits">549</td>
                            <td class="digits">36.913</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Domba</td>
                            <td class="digits">97.750</td>
                            <td class="digits">19.410</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">11.313</div>
                            </td>
                            <td class="digits">8.580</td>
                            <td class="digits">1.952</td>
                            <td class="digits">95.315</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Buras</td>
                            <td class="digits">530.031</td>
                            <td class="digits">818.542</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">696.938</div>
                            </td>
                            <td class="digits">12.190</td>
                            <td class="digits">26.502</td>
                            <td class="digits">612.943</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Pedaging</td>
                            <td class="digits">2.496.850</td>
                            <td class="digits">18.809.806</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">14.981.100</div>
                            </td>
                            <td class="digits">3.620.433</td>
                            <td class="digits">124.843</td>
                            <td class="digits">2.580.280</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Petelur</td>
                            <td class="digits">93.482</td>
                            <td class="digits">1.355.136</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">61.698</div>
                            </td>
                            <td class="digits">839.468</td>
                            <td class="digits">4.674</td>
                            <td class="digits">542.778</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Itik</td>
                            <td class="digits">104.887</td>
                            <td class="digits">350.456</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">81.812</div>
                            </td>
                            <td class="digits">286.894</td>
                            <td class="digits">5.244</td>
                            <td class="digits">81.393</td>
                          </tr>
                          </tr>
                         </tbody>
                      </table>
                    </div>
              </div>    
              </div>        
    
              <div class="col-xl-15 xl-120 box-col-15">
                <div class="card o-hidden">
                  <div class="card-header">
                    <h5>Populasi Ternak 2019</h5>
                    <div class="card-header-right">
                      <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                        <li><i class="view-html fa fa-code font-primary"></i></li>
                        <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                        <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                        <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                        <li><i class="icofont icofont-error close-card font-primary"></i></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="user-status cart-table table-responsive">
                      <table class="table table-bordernone">
                        <thead>
                          <tr>
                            <th scope="col">Komoditi</th>
                            <th scope="col">Populasi Awal</th>
                            <th scope="col">Masuk/lahir</th>
                            <th scope="col">Dipotong</th>
                            <th scope="col">Keluar</th>
                            <th scope="col">Mati</th>
                            <th scope="col">Tot. Populasi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="f-w-600">Sapi Potong</td>
                            <td class="digits">29.783</td>
                            <td class="digits">9.569</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">7.533</div>
                            </td>
                            <td class="digits">765</td>
                            <td class="digits">596</td>
                            <td class="digits">30.458</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Kerbau</td>
                            <td class="digits">2465</td>
                            <td class="digits">46</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">15</div>
                            </td>
                            <td class="digits">98</td>
                            <td class="digits">49</td>
                            <td class="digits">2349</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Kambing</td>
                            <td class="digits">27.957</td>
                            <td class="digits">63.623</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">61.212</div>
                            </td>
                            <td class="digits">589</td>
                            <td class="digits">559</td>
                            <td class="digits">29.220</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Domba</td>
                            <td class="digits">111.324</td>
                            <td class="digits">41.901</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">52.664</div>
                            </td>
                            <td class="digits">585</td>
                            <td class="digits">2.226</td>
                            <td class="digits">97.750</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Buras</td>
                            <td class="digits">519.506</td>
                            <td class="digits">578.275</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">530.031</div>
                            </td>
                            <td class="digits">11.744</td>
                            <td class="digits">25.975</td>
                            <td class="digits">530.031</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Pedaging</td>
                            <td class="digits">2.866.495</td>
                            <td class="digits">6.449.410</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">2.496.850</div>
                            </td>
                            <td class="digits">4.178.880</td>
                            <td class="digits">143.325</td>
                            <td class="digits">2.496.850</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Ayam Petelur</td>
                            <td class="digits">111.883</td>
                            <td class="digits">1.085.000</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">93.482</div>
                            </td>
                            <td class="digits">1.004.325</td>
                            <td class="digits">5.594</td>
                            <td class="digits">93.482</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Itik</td>
                            <td class="digits">49.553</td>
                            <td class="digits">163.982</td>
                            <td>
                              <div class="span badge badge-pill pill-badge-secondary">104.887</div>
                            </td>
                            <td class="digits">1.283</td>
                            <td class="digits">2.478</td>
                            <td class="digits">104.887</td>
                          </tr>
                          </tr>
                         </tbody>
                      </table>
                    </div>
                    <div class="code-box-copy">
                      <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                      <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class=&quot;user-status table-responsive&quot;&gt;
  &lt;table class=&quot;table table-bordernone&quot;&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope=&quot;col&quot;&gt;Details&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Quantity&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Status&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Price&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;Simply dummy text of the printing&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;1&lt;/td&gt;
        &lt;td class=&quot;font-primary&quot;&gt;Pending&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-secondary&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;Long established&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;5&lt;/td&gt;
        &lt;td class=&quot;font-secondary&quot;&gt;cancle&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-success&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;sometimes by accident&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;10&lt;/td&gt;
        &lt;td class=&quot;font-secondary&quot;&gt;cancle&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-warning&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;Classical Latin literature&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;9&lt;/td&gt;
        &lt;td class=&quot;font-primary&quot;&gt;Return&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-primary&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;keep the site on the Internet&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;8&lt;/td&gt;
        &lt;td class=&quot;font-primary&quot;&gt;Pending&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-danger&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;Molestiae consequatur&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;3&lt;/td&gt;
        &lt;td class=&quot;font-secondary&quot;&gt;cancle&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-info&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;Pain can procure&lt;/td&gt;
        &lt;td class=&quot;digits&quot;&gt;8&lt;/td&gt;
        &lt;td class=&quot;font-primary&quot;&gt;Return&lt;/td&gt;
        &lt;td class=&quot;span badge badge-pill pill-badge-primary&quot;&gt;6523&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                    </div>
                  </div>
                </div>
              </div>
              
              
             
                    <div class="code-box-copy">
                      <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head7" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                      <pre><code class="language-html" id="example-head7">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class=&quot;user-status table-responsive&quot;&gt;
  &lt;table class=&quot;table table-bordernone&quot;&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope=&quot;col&quot;&gt;Name&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Designation&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Skill Level&lt;/th&gt;
        &lt;th scope=&quot;col&quot;&gt;Experience&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td class=&quot;bd-t-none u-s-tb&quot;&gt;
          &lt;div class=&quot;align-middle image-sm-size&quot;&gt;
            &lt;img src=&quot;<?= base_url(); ?>assets/images/user/4.jpg&quot; atl=&quot;user&quot; class=&quot;img-radius align-top m-r-15 rounded-circle&quot;&gt;
            &lt;div class=&quot;d-inline-block&quot;&gt;&gt;
              &lt;h6&gt;John Deo &lt;span class=&quot;text-muted digits&quot;&gt;(14+ Online)&lt;/span&gt;&lt;/h6&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;Designer&lt;/td&gt;
        &lt;td&gt;
          &lt;div class=&quot;progress-showcase&quot;&gt;
            &lt;div class=&quot;progress&quot; style=&quot;height:8px;&quot;&gt;
              &lt;div class=&quot;progress-bar bg-primary&quot; role=&quot;progressbar&quot; style=&quot;width: 30%&quot; aria-valuenow=&quot;50&quot; aria-valuemin=&quot;0&quot; aria-valuemax=&quot;100&quot;&gt;&lt;/div&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;class=&quot;digits&quot;&gt;2 Year&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td class=&quot;bd-t-none u-s-tb&quot;&gt;
          &lt;div class=&quot;align-middle image-sm-size&quot;&gt;
            &lt;img src=&quot;<?= base_url(); ?>assets/images/user/1.jpg&quot; atl=&quot;user&quot; class=&quot;img-radius align-top m-r-15 rounded-circle&quot;&gt;
            &lt;div class=&quot;d-inline-block&quot;&gt;&gt;
              &lt;h6&gt;Holio Mako &lt;span class=&quot;text-muted digits&quot;&gt;(250+ Online)&lt;/span&gt;&lt;/h6&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;Developer&lt;/td&gt;
        &lt;td&gt;
          &lt;div class=&quot;progress-showcase&quot;&gt;
            &lt;div class=&quot;progress&quot; style=&quot;height:8px;&quot;&gt;
              &lt;div class=&quot;progress-bar bg-secondary&quot; role=&quot;progressbar&quot; style=&quot;width: 70%&quot; aria-valuenow=&quot;50&quot; aria-valuemin=&quot;0&quot; aria-valuemax=&quot;100&quot;&gt;&lt;/div&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;class=&quot;digits&quot;&gt;3 Year&lt;td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td class=&quot;bd-t-none u-s-tb&quot;&gt;
          &lt;div class=&quot;align-middle image-sm-size&quot;&gt;
            &lt;img src=&quot;<?= base_url(); ?>assets/images/user/5.jpg&quot; atl=&quot;user&quot; class=&quot;img-radius align-top m-r-15 rounded-circle&quot;&gt;
            &lt;div class=&quot;d-inline-block&quot;&gt;&gt;
              &lt;h6&gt;Mohsib lara&lt;span class=&quot;text-muted digits&quot;&gt;(99+ Online)&lt;/span&gt;&lt;/h6&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;Tester&lt;/td&gt;
        &lt;td&gt;
          &lt;div class=&quot;progress-showcase&quot;&gt;
            &lt;div class=&quot;progress&quot; style=&quot;height:8px;&quot;&gt;
              &lt;div class=&quot;progress-bar bg-primary&quot; role=&quot;progressbar&quot; style=&quot;width: 60%&quot; aria-valuenow=&quot;50&quot; aria-valuemin=&quot;0&quot; aria-valuemax=&quot;100&quot;&gt;&lt;/div&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;class=&quot;digits&quot;&gt;5 Month&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td class=&quot;bd-t-none u-s-tb&quot;&gt;
          &lt;div class=&quot;align-middle image-sm-size&quot;&gt;
            &lt;img src=&quot;<?= base_url(); ?>assets/images/user/6.jpg&quot; atl=&quot;user&quot; class=&quot;img-radius align-top m-r-15 rounded-circle&quot;&gt;
            &lt;div class=&quot;d-inline-block&quot;&gt;&gt;
              &lt;h6&gt;Hileri Soli &lt;span class=&quot;text-muted digits&quot;&gt;(150+ Online)&lt;/span&gt;&lt;/h6&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;Designer&lt;/td&gt;
        &lt;td&gt;
          &lt;div class=&quot;progress-showcase&quot;&gt;
            &lt;div class=&quot;progress&quot; style=&quot;height:8px;&quot;&gt;
              &lt;div class=&quot;progress-bar bg-secondary&quot; role=&quot;progressbar&quot; style=&quot;width: 30%&quot; aria-valuenow=&quot;50&quot; aria-valuemin=&quot;0&quot; aria-valuemax=&quot;100&quot;&gt;&lt;/div&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;class=&quot;digits&quot;&gt;3 Month&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td class=&quot;bd-t-none u-s-tb&quot;&gt;
          &lt;div class=&quot;align-middle image-sm-size&quot;&gt;
            &lt;img src=&quot;<?= base_url(); ?>assets/images/user/7.jpg&quot; atl=&quot;user&quot; class=&quot;img-radius align-top m-r-15 rounded-circle&quot;&gt;
            &lt;div class=&quot;d-inline-block&quot;&gt;&gt;
              &lt;h6&gt;Pusiz bia &lt;span class=&quot;text-muted digits&quot;&gt;(14+ Online)&lt;/span&gt;&lt;/h6&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;Designer&lt;/td&gt;
        &lt;td&gt;
          &lt;div class=&quot;progress-showcase&quot;&gt;
            &lt;div class=&quot;progress&quot; style=&quot;height:8px;&quot;&gt;
              &lt;div class=&quot;progress-bar bg-primary&quot; role=&quot;progressbar&quot; style=&quot;width: 90%&quot; aria-valuenow=&quot;50&quot; aria-valuemin=&quot;0&quot; aria-valuemax=&quot;100&quot;&gt;&lt;/div&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;class=&quot;digits&quot;&gt;5 Year&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;                        </code></pre>
                    </div>
                  </div>
                </div>
              </div>
              
                 
                    <div class="code-box-copy">
                      <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head3" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                      <pre><code class="language-html" id="example-head3">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class=&quot;contact-form card-body&quot;&gt;
  &lt;form class=&quot;theme-form&quot;&gt;
    &lt;div class=&quot;form-icon&quot;&gt;
      &lt;i class=&quot;icofont icofont-envelope-open&quot;&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class=&quot;form-group&quot;&gt;
      &lt;label for=&quot;exampleInputName&quot;&gt;Your Name&lt;/label&gt;
      &lt;input type=&quot;text&quot; class=&quot;form-control&quot; id=&quot;exampleInputName&quot; placeholder=&quot;John Dio&quot;&gt;
    &lt;/div&gt;
    &lt;div class=&quot;form-group&quot;&gt;
      &lt;label for=&quot;exampleInputEmail1&quot; class=&quot;col-form-label&quot;&gt;Email&lt;/label&gt;
      &lt;input type=&quot;email&quot; class=&quot;form-control&quot; id=&quot;exampleInputEmail1&quot; placeholder=&quot;Demo@gmail.com&quot;&gt;
    &lt;/div&gt;
    &lt;div class=&quot;form-group&quot;&gt;
      &lt;label for=&quot;exampleInputEmail1&quot; class=&quot;col-form-label&quot;&gt;Message&lt;/label&gt;
      &lt;textarea row=&quot;3&quot; cols=&quot;50&quot; class=&quot;form-control textarea&quot; placeholder=&quot;Your Message&quot;&gt;&lt;/textarea&gt;
    &lt;/div&gt;
    &lt;div class=&quot;text-sm-right&quot;&gt;
      &lt;button class=&quot;btn btn-primary-gradien&quot;&gt;SEND IT&lt;/button&gt;
    &lt;/div&gt;
  &lt;/form&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                 <div class="col-xl-6 col-sm-12 xl-100 col-md-12 box-col-12">
                <div class="card height-equal">
                  <div class="card-header">
                  <h5>Column Chart</h5>
                  </div>
                  <div class="card-body p-0">
                    <div id="annotationchart"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 xl-100">
                <div class="card">
                  <div class="card-header">&lt;!-- Cod Box Copy begin --&gt;
&lt;ul class=&quot;crm-activity&quot;&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;span class=&quot;mr-3 font-primary&quot;&gt;A&lt;/span&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;h6 class=&quot;mt-0&quot;&gt;Any desktop publishing packages and web page editors.&lt;/h6&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;span class=&quot;mr-3 font-secondary&quot;&gt;C&lt;/span&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;h6 class=&quot;mt-0&quot;&gt;Contrary to popular belief, Lorem Ipsum is not simply.&lt;/h6&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;span class=&quot;mr-3 font-primary&quot;&gt;E&lt;/span&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;h6 class=&quot;mt-0&quot;&gt;Established fact that a reader will be distracted&lt;/h6&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;span class=&quot;mr-3 font-secondary&quot;&gt;H&lt;/span&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;h6 class=&quot;mt-0&quot;&gt;H-Code - A premium portfolio template from ThemeZaa&lt;/h6&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;span class=&quot;mr-3 font-primary&quot;&gt;T&lt;/span&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;h6 class=&quot;mt-0&quot;&gt;There isn't anything embarrassing hidden.&lt;/h6&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
  &lt;li class=&quot;media&quot;&gt;
    &lt;div class=&quot;align-self-center media-body&quot;&gt;
      &lt;ul class=&quot;dates&quot;&gt;
        &lt;li class=&quot;digits&quot;&gt;25 July 2017&lt;/li&gt;
        &lt;li class=&quot;digits&quot;&gt;20 hours ago&lt;/li&gt;
      &lt;/ul&gt;
    &lt;/div&gt;
  &lt;/li&gt;
&lt;/ul&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright © 2021 Bogor. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand-crafted & made with Diskominfosanditik<i class="fa fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="<?= base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap js-->
    <script src="<?= base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
    <!-- feather icon js-->
    <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
    <!-- Sidebar jquery-->
    <script src="<?= base_url(); ?>assets/js/sidebar-menu.js"></script>
    <script src="<?= base_url(); ?>assets/js/config.js"></script>
    <!-- Plugins JS start-->
    <script src="<?= base_url(); ?>assets/js/owlcarousel/owl.carousel.js"></script>
    <script src="<?= base_url(); ?>assets/js/prism/prism.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/clipboard/clipboard.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/counter/jquery.waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/counter/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/counter/counter-custom.js"></script>
    <script src="<?= base_url(); ?>assets/js/custom-card/custom-card.js"></script>
    <script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.js"></script>
    <script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.en.js"></script>
    <script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.custom.js"></script>
    <script src="<?= base_url(); ?>assets/js/chat-menu.js"></script>
    <script src="<?= base_url(); ?>assets/js/general-widget.js"></script>
    <script src="<?= base_url(); ?>assets/js/height-equal.js"></script>
    <script src="<?= base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?= base_url(); ?>assets/js/script.js"></script>
    <script src="<?= base_url(); ?>assets/js/theme-customizer/customizer.js"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <script>
    // pie chart
var options8 = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Ikan Hias', 'Benih Ikan'],
    series: [670553, 197969],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:['#7e37d8', '#fe80b2', '#80cf00']
}

var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
);

chart8.render();

// area spaline chart
var options1 = {
    chart: {
        height: 375,
        type: 'area',
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'smooth'
    },
    
    series: [{
        name: '2019',
        data: [30458 ,  4028 ,  29220 ,  97750]
    }, {
        name: '2020', 
        data: [34915, 3703, 36913, 95135]
    }],
    

    xaxis: {
        type: 'text',
        categories: ["Sapi Potong", "Sapi perah", "Kambing", "Domba"]
    },
    legend: {
                position: 'bottom'
            },  
    tooltip: {
        x: {
            format: 'text'
        },
    },
    colors:['#7e37d8', '#fd517d']
}

var chart1 = new ApexCharts(
    document.querySelector("#area-spaline"),
    options1
);

chart1.render();
</script>
<script>
//basic bar chart
var options2 = {
    chart: {
        height: 450,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        data: [3243, 2484, 5117, 15071, 36059, 16997, 55179, 15711, 11001, 17851, 19440, 11648, 20610, 69218, 69027, 36923, 20773, 5447, 10133, 43732, 52215, 24712, 23450, 5710, 15592, 5600]
    }],
    xaxis: {
        categories: ['Jatinangor', 'Cimanggung', 'Tanjungsari', 'Sukasari', 'Pamulihan', 'Rancakalong', 'Bogor Selatan', 'Bogor Utara', 'Ganeas', 'Situraja','Cisitu','Darmaraja','Cibugel','Wado','Jatinunggal','Jatigede','Tomo','Ujungjaya','Conggeang','Paseh','Cimalaka','Cisarua','Tanjungkerta','Tanjungmedar','Buahdua','Surian'],
    },
    colors:['#7e37d8']
}

var chart2 = new ApexCharts(
    document.querySelector("#basic-bar"),
    options2
);

chart2.render();
</script>


  </body>
</html>