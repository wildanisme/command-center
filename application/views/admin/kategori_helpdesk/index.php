 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Kategori Konsultasi</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Konsultasi</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                	<?php if (!empty($message)) echo "
				<div class='alert alert-$message_type'>$message</div>";?>

				<div class="row">
					<div class="col-sm-12">
						<div class="white-box">
							<div class="pull-right">

							</div>
							<h3 class="box-title"><a href="<?php echo base_url();?>kategori_helpdesk/add" class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a></h3>
							<p class="text-muted m-b-20"></p>
							<div class="table-responsive">

								<table class="table table-striped datatable" id="data">
									<thead>
										<tr>
											<th>#</th>
											<th>Kategori</th>
											<th>Slug</th>
											<th>Status</th>
											<th width=70px>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$num = 1;
										foreach ($query as $row) {
											echo"
											<tr>
											<td>$num</td>
											<td>$row->nama_kategori_helpdesk</td>
											<td>$row->kategori_slug</td>
											<td>$row->status</td>
											<td>
											<a href='".base_url()."kategori_helpdesk/edit/$row->id_kategori_helpdesk' class='btn-xs' title='Edit' data-toggle=\"tooltip\" data-original-title=\"Edit\">

											<i class=\"fa fa-pencil text-inverse m-r-10\"></i>
											</a>
											<a class='btn-xs' title='Delete'  onclick='hapus(\"$row->id_kategori_helpdesk\")' data-toggle=\"tooltip\" data-original-title=\"Close\">
											<i class=\"fa fa-close text-danger\"></i>
											</a>

											</td>
											</tr>
											";

											$num++;
										}
										?>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
function hapus(id)
{
  //alert(id);
  swal({
    title: "Hapus kategori?",
    //icon: "info",
    buttons: true,
    dangerMode: false,
  })
  .then((isConfirm) => {
    if (isConfirm) {

      $.ajax({
          url :"<?php echo base_url("kategori_helpdesk/delete")?>",
          type:'post',
          data:{
            id:id,
            "<?=$this->security->get_csrf_token_name();?>" : "<?= $this->security->get_csrf_hash();?>",
          },
           success    : function(data){
              console.log(data);

              swal("Kategori berhasil dihapus", {
                icon: "success",
              });

              setTimeout(function() {
                window.location.href = "<?=base_url();?>kategori_helpdesk";
              }, 500);
           },
               error: function(xhr, status, error) {
                //swal("Opps","Error","error");
                console.log(xhr);
              }
      });

    }
  });
}
</script>
