<?php 
$url = "https://dirga.bogorkab.go.id/";

$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

$options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
          );

$ch      = curl_init( $url );
curl_setopt_array( $ch, $options );
$content = curl_exec( $ch );
$err     = curl_errno( $ch );
$errmsg  = curl_error( $ch );
$header  = curl_getinfo( $ch );
curl_close( $ch );

$header['errno']   = $err;
$header['errmsg']  = $errmsg;
$header['content'] = $content;
// echo $header['content'];

?>
<script type="text/javascript">
  var token = "MzQ5MzhhNjc1MmI2MDBjOWQyODYyZWUzZjI2OGZhNDUwY2NiMTQxZjdmYjAxZTFjODRlN2RhYWI5MzU5NWQ3";
  var xhr = new XMLHttpRequest();

  xhr.open('GET', 'https://dirga.bogorkab.go.id/');
  xhr.onreadystatechange = handler;
  xhr.responseType = 'blob';
  xhr.setRequestHeader('Authorization', 'token_auth ' + token);
  xhr.send();

  function handler() {
    if (this.readyState === this.DONE) {
      if (this.status === 200) {
        // this.response is a Blob, because we set responseType above
        var data_url = URL.createObjectURL(this.response);
        document.querySelector('#frame').src = data_url;
      } else {
        console.error('no pdf :(');
      }
    }
  }
</script>

<iframe id="frame" src="https://dirga.bogorkab.go.id/" frameborder="0" gesture="media" allow="encrypted-media" allowtransparency="true" allowfullscreen="true" style="width: 100%; min-height: 720px; height: max-content;"></iframe>

<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>