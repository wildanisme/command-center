<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Parkir Berlangganan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Transportasi</li>
            <li class="breadcrumb-item active">Parkir</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->

      <div class="col-xl-4 box-col-6">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-secondary">Retribusi Hari Ini</h5>
            <div class="setting-dot">
              <div class="setting-bg-secondary position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div>
            <div id="bitcoinchart-1"></div>
          </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent"> <i class="ion ion-social-bitcoin"> </i></div>
            </div>
            <div class="media-body">
              <h5>Rp. <?= number_format($retribusi_hari_ini); ?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?= tanggal(date('Y-m-d')); ?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 box-col-6">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-primary">Total Transaksi</h5>
            <div class="setting-dot">
              <div class="setting-bg-primary position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div id="bitcoinchart-2"> </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent"> <i class="ion ion-social-yen"> </i></div>
            </div>
            <div class="media-body">
              <h5><?= number_format($total_transaksi); ?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?= $tahun; ?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 box-col-12">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-warning">Total Retribusi</h5>
            <div class="setting-dot">
              <div class="setting-bg-warning position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div id="bitcoinchart-3"> </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent"> <i class="ion ion-social-yen"> </i></div>
            </div>
            <div class="media-body">
              <h5>Rp. <?= number_format($total_retribusi); ?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?= $tahun; ?></div>
            </div>
          </div>
        </div>
      </div>
        <div class="col-md-12">
          <div class="card" style="width: 100%;">
            <div class="card-header no-border d-flex">
              <h5>Transaksi Hari Ini</h5>
              <ul class="creative-dots">
                <li class="bg-primary big-dot"></li>
                <li class="bg-secondary semi-big-dot"></li>
                <li class="bg-warning medium-dot"></li>
                <li class="bg-info semi-medium-dot"></li>
                <li class="bg-secondary semi-small-dot"></li>
                <li class="bg-primary small-dot"></li>
              </ul>
            </div>
            <div class="card-body ">
            <div class="table-responsive">
            <table  class="display" id="basic-1">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Kode Bayar</th>
                                            <th scope="col">No. Polisi</th>
                                            <th scope="col">Nama Pemilik</th>
                                            <th scope="col">Jenis</th>
                                            <th scope="col">Nominal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $total = 0;
                                        if(empty($laporan_parkir)){
                                            ?>
                                            <tr>
                                                <td colspan="6" class="text-center">
                                            <h4 class="text-primary">Oops</h4>
                                            <p>Data tidak ditemukan</p>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        foreach ($laporan_parkir as $l) {
                                        ?>
                                            <tr>
                                                <th scope="row"><?= $no ?></th>
                                                <td><?= $l->kode_bayar ?></td>
                                                <td><?= $l->no_polisi ?></td>
                                                <td><?= $l->nama_pemilik ?></td>
                                                <td><?= $l->kode_kendaraan ?></td>
                                                <td><?= rupiah($l->total_bayar) ?></td>
                                            </tr>
                                        <?php $no++;
                                            $total += $l->total_bayar;
                                        } ?>
                                    </tbody>
                                    <tfooter>
                                        <tr>
                                            <td colspan="5" style="text-align:right;font-weight:bold">Total Pembayaran</td>
                                            <td style="font-weight:bold"><?= rupiah($total) ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align:right;font-weight:bold">Total Transaksi</td>
                                            <td style="font-weight:bold"><?= count($laporan_parkir) ?></td>
                                        </tr>
                                    </tfooter>
                                </table>
            </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>


<script src="<?= base_url(); ?>js/perizinan/dashboard.js" defer></script>