<div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-lg-6 main-header">
					<h2>Ekonomi<span>Edit</span></h2>
					<h6 class="mb-0">Bogor Comand Center</h6>
				</div>
				<div class="col-lg-6 breadcrumb-right">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
						<li class="breadcrumb-item">Apps </li>
						<li class="breadcrumb-item">Ekonomi</li>
						<li class="breadcrumb-item active">Ekonomi</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<form class="form-horizontal form-material" method='post' autocomplete="off">

								<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
								<?php if (!empty($message)) echo "
                        <div class='alert alert-$message_type'>$message</div>";?>
								<h3 class="text-primary">Data :</h3>
								<div class="form-group">
									<label class="col-md-12">APBD</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_investasi->id; ?>">
										<input type="text" name="apbd_e" value=" <?= $data_investasi->apbd_e; ?>" class="form-control form-control-line">
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-12">Target Pendapatan</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_investasi->id; ?>">
										<input type="text" name="target_investasi" value=" <?= $data_investasi->target_investasi; ?>" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Realiasi Pendapatan</label>
									<div class="col-md-12">
										<input type="text" name="realisasi_investasi" class="form-control form-control-line" value="<?= $data_investasi->realisasi_investasi; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Anggaran Belanja</label>
									<div class="col-md-12">
										<input type="text" name="agg_blj" class="form-control form-control-line" value="<?= $data_investasi->agg_blj; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Realiasi Belanja</label>
									<div class="col-md-12">
										<input type="text" name="real_blj" class="form-control form-control-line" value="<?= $data_investasi->real_blj; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Tahun</label>
									<div class="col-md-12">
										<input type="text" name="tahun" class="form-control form-control-line" value=" <?= $data_investasi->tahun; ?>">
									</div>
								</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12">
								<a href="<?=base_url();?>investasi" class="btn btn-light m-l-10"><span class="icon-angle-left"></span> Batal</a>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12"><button type="submit" class="btn btn-primary m-l-10" style="float: right;"><span class="icon-save"></span> Simpan</button>

							</div>
						</div>
						</form>
					</div>
				</div>