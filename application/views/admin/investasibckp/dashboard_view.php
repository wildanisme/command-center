


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

<div class="col-lg-12 xl-100">
  <div class="row">


    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5>Total APBD</h5>
          <div class="card-header-right">
            <?php
              $total_data_tahun_ini = count($data_investasi);

              if ($total_data_tahun_ini >= 1) {

              }
            ?>
          <a href="<?= $total_data_tahun_ini >= 1 ? '#' : site_url('investasi/add/') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus text-white"></i> </a>
          </div>
        </div>
        <div class="card-body p-0">

          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_investasi">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">APBD</th>
                  <th scope="col">Target Pendapatan</th>
                  <th scope="col">Realisasi Pendapatan</th>
                  <th scope="col">Anggaran Belanja</th>
                  <th scope="col">Realisasi Belanja</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>

              <?php foreach ($data_investasi as $data): ?>
              <tbody>
                <tr>
                  <td class="f-w-600"> <?php echo $data->id?> </td>
                  <td class="f-w-600"> <?php echo $data->apbd_e?> </td>
                  <td class="f-w-600"> <?php echo $data->target_investasi?> </td>
                  <td class="f-w-600"> <?php echo $data->realisasi_investasi?> </td>
                  <td class="f-w-600"> <?php echo $data->agg_blj?> </td>
                  <td class="f-w-600"> <?php echo $data->real_blj?> </td>
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('investasi/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
              </tbody>
              <?php endforeach; ?>
            </table>
          </div>

        </div>
      </div>
    </div>


  </div>
</div>

<!-- Modal dialog hapus data-->
<div class="modal fade" id="myModalDelete" tabindex="-1" aria-labelledby="myModalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalDeleteLabel">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda ingin menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger" id="btdelete">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>

<script>
  $('#table_investasi').on('click', '.item-delete', function() {
        //ambil data dari atribute data 
        var id = $(this).attr('data');
        $('#myModalDelete').modal('show');
        //ketika tombol lanjutkan ditekan, data id akan dikirim ke method delete 
        //pada controller mahasiswa
        $('#btdelete').unbind().click(function() {
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>investasi/delete/',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(response) {
                    $('#myModalDelete').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>







<!-- Plugins JS start-->
<!-- Plugins JS start-->


<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>




<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

<div class="col-lg-12 xl-100">
  <div class="row">


    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5> APBD OPD</h5>
          <div class="card-header-right">
            <?php
              $total_data_tahun_ini = count($data_investasi);

              if ($total_data_tahun_ini >= 1) {

              }
            ?>
          <a href="<?= $total_data_tahun_ini >= 1 ? '#' : site_url('investasi/add/') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus text-white"></i> </a>
          </div>
        </div>
        <div class="card-body p-0">

          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_investasi">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama OPD</th>
                  <th scope="col">APBD</th>
                  <th scope="col">Target Pendapatan</th>
                  <th scope="col">Realisasi Pendapatan</th>
                  <th scope="col">Anggaran Belanja</th>
                  <th scope="col">Realisasi Belanja</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>

              <?php foreach ($data_investasi as $data): ?>
              <tbody>
                <tr>
                  <td class="f-w-600"> <?php echo $data->id?> </td>
                  <td class="f-w-600"> <?php echo $data->nama_opd?> </td>
                  <td class="f-w-600"> <?php echo $data->apbd_o?> </td>
                  <td class="f-w-600"> <?php echo $data->target_investasio?> </td>
                  <td class="f-w-600"> <?php echo $data->realisasi_investasio?> </td>
                  <td class="f-w-600"> <?php echo $data->agg_bljo?> </td>
                  <td class="f-w-600"> <?php echo $data->real_bljo?> </td>
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('investasi/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
              </tbody>
              <?php endforeach; ?>
            </table>
          </div>

        </div>
      </div>
    </div>


  </div>
</div>

<!-- Modal dialog hapus data-->
<div class="modal fade" id="myModalDelete" tabindex="-1" aria-labelledby="myModalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalDeleteLabel">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda ingin menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger" id="btdelete">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>

<script>
  $('#table_investasi').on('click', '.item-delete', function() {
        //ambil data dari atribute data 
        var id = $(this).attr('data');
        $('#myModalDelete').modal('show');
        //ketika tombol lanjutkan ditekan, data id akan dikirim ke method delete 
        //pada controller mahasiswa
        $('#btdelete').unbind().click(function() {
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>investasi/delete/',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(response) {
                    $('#myModalDelete').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>







<!-- Plugins JS start-->
<!-- Plugins JS start-->


<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>







