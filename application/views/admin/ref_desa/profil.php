<script type="text/javascript" src="<?php echo base_url()."asset" ;?>/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: ".text-area",
            theme: "modern",
            plugins: [ 
			"advlist autolink link image lists charmap print preview hr anchor pagebreak", 
			"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking", 
			"table contextmenu directionality emoticons paste textcolor filemanager" 
			], 
			image_advtab: true, 
			toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
			 });
    </script>


<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Profil desa</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url();?>admin"><i class="entypo-home"></i>Home</a>
				</li>
				<li>
					<a href="<?php echo base_url('ref_desa');?>"><i class="entypo-home"></i>Desa</a>
				</li>

				<li class="active">		
					<strong>Profil</strong>
				</li>
			</ol>
		</div>
		<!-- /.col-lg-12 -->
	</div>

<div class="row">
	<div class="col-md-12">
		
			<div class="white-box">
				<?php if (!empty($message)) echo "
				<div class='alert alert-$message_type'>$message</div>";?>
				<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
   
					<div class="form-group">
						<label class="col-sm-2 control-label">Nama desa</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?php echo $dt_desa->desa;?>' id='desa' name='desa' readonly placeholder="">
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Alamat</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->alamat : "" ;?>' id='alamat' name='alamat' placeholder="">
						</div>
					</div>
					
                    <div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->email : "" ;?>' id='email' name='email' placeholder="">
						</div>
					</div>
					
                    <div class="form-group">
						<label class="col-sm-2 control-label">Telp</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->telp : "" ;?>' id='telp' name='telp' placeholder="">
						</div>
					</div>
					

					<div class="form-group">
						<label class="col-sm-2 control-label">Foto</label>
						
						<div class="col-sm-10">
                            <?php 
                            if(!empty($profil_desa)){
                            echo"
							<div class='member-img'>
								<img src='".base_url()."data/desa/$profil_desa->foto' class='img-rounded' style='max-width:200px' />
								
							</div><br>"; } ?>
							<input type="file" name='foto' class="file2 inline btn " data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
							<p>
								Max : 500px | 1MB
							</p>
							<?php if (!empty($error)) echo "
				<div class='alert alert-warning'>$error</div>";?>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Kepala desa</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->kepala_desa : "" ;?>' id='kepala_desa' name='kepala_desa' placeholder="Nama kepala desa">
						</div>
					</div>
					

					<div class="form-group">
						<label class="col-sm-2 control-label">Visi</label>
						<div class="col-sm-10">
							<textarea class="form-control text-area" name='visi'><?= !empty($profil_desa) ? $profil_desa->visi :"" ;?></textarea>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Misi</label>
						<div class="col-sm-10">
							<textarea class="form-control text-area" name='misi'><?= !empty($profil_desa) ? $profil_desa->misi :"" ;?></textarea>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Program kerja</label>
						<div class="col-sm-10">
							<textarea class="form-control text-area" name='program_kerja'><?= !empty($profil_desa) ? $profil_desa->program_kerja :"" ;?></textarea>
						</div>
					</div>


                    <div class="form-group">
						<label class="col-sm-2 control-label">Struktur organisasi</label>
						
						<div class="col-sm-10">
                            <?php 
                            if(!empty($profil_desa)){
                            echo"
							<div class='member-img'>
								<img src='".base_url()."data/desa/$profil_desa->struktur_organisasi' class='img-rounded' style='max-width:200px' />
								
							</div><br>"; } ?>
							<input type="file" name='struktur_organisasi' class="file2 inline btn " data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
							<p>
								Max : 500px | 1MB
							</p>
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Longitude</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->longitude : "" ;?>' id='longitude' name='longitude' >
						</div>
					</div>

                    <div class="form-group">
						<label class="col-sm-2 control-label">Latitude</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->latitude : "" ;?>' id='latitude' name='latitude' >
						</div>
					</div>
                    <div class="form-group">
						<label class="col-sm-2 control-label">Peta</label>
						<div class="col-sm-10">
                            <div id="googleMap" class="gmaps"></div>
                        </div>
					</div>


					<div class="form-group">
						<label class="col-sm-2 control-label">Anggaran</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" value='<?= !empty($profil_desa) ? $profil_desa->anggaran : "" ;?>' id='anggaran' name='anggaran' >
						</div>
					</div>


					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-5">
							<button type="submit" class="btn btn-primary  waves-effect waves-light">Ubah profil desa</button>	
                            
						</div>
						
					</div>
				</form>
			
			</div>

		</div>
	</div>
</div>


<script>


	function loadMap() {
      	var default_lat = '-6.864113885641478';
		var default_lng = '107.9237869248534';
		  
	  	var lat = $("#latitude").val();
		var lng = $("#longitude").val();
		if(lat!="" && lng!=""){
			var location = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
		
		}
		else{
			var location = new google.maps.LatLng(parseFloat(default_lat),parseFloat(default_lng));
		}	
			var mapProp= {
				center:location,
				zoom:12,
				//disableDefaultUI: true,
				//mapTypeId:google.maps.MapTypeId.HYBRID
			};
			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			
			var marker = new google.maps.Marker({
				position: location,
				//map: map,
				//animation: google.maps.Animation.BOUNCE
				});


			
			var infowindow = new google.maps.InfoWindow({
				content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
			});

			if(lat!="" && lng!=""){
				marker.setMap(map);
				markers.push(marker);
				infowindow.open(map,marker);
			}
			

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
			
			
			google.maps.event.addListener(map, 'click', function(event) {
				placeMarker(map, event.latLng);
			});
		

    }
    var markers = [];
    function placeMarker(map, location) {
      var marker = new google.maps.Marker({
        position: location,
        map: map,
        animation: google.maps.Animation.BOUNCE,
      });
      var infowindow = new google.maps.InfoWindow({
        content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
      });

      //hapus tanda
      for(var i=0; i < markers.length ; i++){
        markers[i].setMap(null);
      }

      infowindow.open(map,marker);
      markers.push(marker);
      $("#latitude").val(location.lat());
      $("#longitude").val(location.lng());
	}
	
	
	

    </script>
