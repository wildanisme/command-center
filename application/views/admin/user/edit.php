<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper" data-select2-id="12">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-md-left mb-0">Edit User</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb default-breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">User</a></li><li class="breadcrumb-item active">Edit</li>                            </ol>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body" data-select2-id="11">
            <!-- Dashboard Analytics Start -->
             <!-- users edit start -->
 <section class="users-edit" data-select2-id="10">
     <div class="card" data-select2-id="9">
         <div class="card-content" data-select2-id="8">
             <div class="card-body" data-select2-id="7">
                                  <form action="https://modistracompany.com/manage_user/edit/6" class="form-group" enctype="multipart/form-data" method="post" accept-charset="utf-8" data-select2-id="6">
                 <ul class="nav nav-tabs mb-3" role="tablist">
                     <li class="nav-item">
                         <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                             <i class="feather icon-credit-card mr-25"></i><span class="d-none d-sm-block">Informasi Pengguna</span>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information" aria-controls="information" role="tab" aria-selected="false">
                             <i class="feather icon-lock mr-25"></i><span class="d-none d-sm-block">Password</span>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a class="nav-link d-flex align-items-center" id="information2-tab" data-toggle="tab" href="#information2" aria-controls="information2" role="tab" aria-selected="false">
                             <i class="feather icon-link mr-25"></i><span class="d-none d-sm-block">Hak Akses</span>
                         </a>
                     </li>
                 </ul>
                 <div class="tab-content" data-select2-id="5">
                     <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel" data-select2-id="account">
                         <!-- users edit media object start -->
                         <div class="media">
                             <a href="javascript: void(0);">
                                 <img id="profilePic" style="object-fit: cover" src="https://modistracompany.com/app-assets/images/user/default.png" class="rounded mr-75" alt="profile image" height="64" width="64">
                             </a>
                             <div class="media-body mt-75">
                                 <div class="col-12 px-0 d-flex flex-sm-row flex-column justify-content-start">
                                     <label class="btn btn-sm btn-primary ml-50 mb-50 mb-sm-0 cursor-pointer waves-effect waves-light" for="account-upload">Upload foto baru</label>
                                     <input type="file" name="foto" onchange="updatePhoto()" id="account-upload" hidden="">
                                     <a href="javascript:void(0)" id="btnHapus" onclick="deletePhoto()" class="btn btn-sm btn-outline-warning ml-50 waves-effect waves-light">Hapus Foto</a>
                                 </div>
                                 <p class="text-muted ml-75 mt-50" style="margin-bottom:0px"><small>File yg diizinkan JPG, GIF atau PNG. Max
                                         ukuran
                                         2MB</small></p>
                                 <span id="uploadMessage" style="display: none"></span>
                             </div>
                         </div>
                         <hr>
                         
                             <div class="form-group">
                                 <div class="controls">
                                     <label>Nama Lengkap</label>
                                     <input type="text" name="nama_lengkap" value="gani" class="form-control birthdate-picker" placeholder="Masukkan Nama Lengkap" required="">
                                 </div>
                             </div>
                             <div class="form-group">
                                 <div class="controls">
                                     <label>Email</label>
                                     <input type="text" name="email" value="gani@gmail.com" class="form-control" placeholder="Masukkan Alamat Email" required="">
                                 </div>
                             </div>

                             <div class="form-group">
                                 <label>Level</label>
                                 <select class="form-control" onchange="toggleLevel()" name="id_level" required="">
                                     <option value="">Pilih Level User</option>
                                     <option value="1">Administrator</option><option value="2" selected="">User</option>                                 </select>
                             </div>
                             <div class="form-group" id="forKaryawan" data-select2-id="forKaryawan">
                                 <label>Nama Kelompok</label>
                                 <select name="id_karyawan" class="form-control select2 select2-hidden-accessible" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                     <option value="" data-select2-id="13">Pilih Kelompok</option>
                                     </select>
                             </div>
                             <div class="form-group">
                                 <div class="controls">
                                     <label>Username</label>
                                     <input type="text" name="username" value="gani" class="form-control" placeholder="Masukkan Username" required="" readonly="">
                                 </div>
                             </div>
                             <!-- users edit account form ends -->
                             <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                 <button type="submit" name="menu" value="detail" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 waves-effect waves-light">Simpan</button>
                                 <a href="https://modistracompany.com/manage_user" class="btn btn-outline-warning waves-effect waves-light">Kembali</a>
                             </div>
                         
                     </div>
                     <div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
                         <form method="POST">
                             <div class="form-group">
                                 <label>Password Lama</label>
                                 <input type="password" name="password_lama" class="form-control" placeholder="Masukkan Password Lama" required="">
                             </div>
                             <div class="form-group">
                                 <label>Password Baru</label>
                                 <input type="password" name="password" class="form-control" placeholder="Masukkan Password" required="">
                             </div>

                             <div class="form-group">
                                 <label>Konfirmasi Password Baru</label>
                                 <input type="password" name="c_password" class="form-control" placeholder="Masukkan Kembali Password" required="">
                             </div>
                             <div class="col-12 d-flex flex-sm-row flex-column justify-content-start mt-1">
                                 <button type="submit" name="menu" value="password" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 waves-effect waves-light">Ganti Password</button>
                             </div>
                         </form>
                     </div>
                     <div class="tab-pane" id="information2" aria-labelledby="information-tab" role="tabpanel">

                         <form method="POST">
                             <div class="table-responsive border rounded px-1 ">
                                 <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i class="feather icon-lock mr-50 "></i>Hak Akses</h6>
                                 <div style="padding-top: 20px;padding-left: 15px;padding-bottom: 20px">
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="referensi" id="customSwitch0">
                                             <label class="custom-control-label" for="customSwitch0">
                                             </label>
                                             <span class="switch-label">Referensi</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="transaksi" id="customSwitch1" checked="">
                                             <label class="custom-control-label" for="customSwitch1">
                                             </label>
                                             <span class="switch-label">Transaksi</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="pembayaran" id="customSwitch2">
                                             <label class="custom-control-label" for="customSwitch2">
                                             </label>
                                             <span class="switch-label">Pembayaran</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="produk" id="customSwitch3" checked="">
                                             <label class="custom-control-label" for="customSwitch3">
                                             </label>
                                             <span class="switch-label">Produk</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="profil_konveksi" id="customSwitch4">
                                             <label class="custom-control-label" for="customSwitch4">
                                             </label>
                                             <span class="switch-label">Profil Konveksi</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="data_vendor" id="customSwitch5">
                                             <label class="custom-control-label" for="customSwitch5">
                                             </label>
                                             <span class="switch-label">Data Vendor</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="data_karyawan" id="customSwitch6">
                                             <label class="custom-control-label" for="customSwitch6">
                                             </label>
                                             <span class="switch-label">Data Karyawan</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="data_supplier" id="customSwitch7">
                                             <label class="custom-control-label" for="customSwitch7">
                                             </label>
                                             <span class="switch-label">Data Supplier</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="data_pelanggan" id="customSwitch8">
                                             <label class="custom-control-label" for="customSwitch8">
                                             </label>
                                             <span class="switch-label">Data Pelanggan</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="inventori" id="customSwitch9">
                                             <label class="custom-control-label" for="customSwitch9">
                                             </label>
                                             <span class="switch-label">Inventori</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="jurnal" id="customSwitch10">
                                             <label class="custom-control-label" for="customSwitch10">
                                             </label>
                                             <span class="switch-label">Jurnal</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="laporan" id="customSwitch11">
                                             <label class="custom-control-label" for="customSwitch11">
                                             </label>
                                             <span class="switch-label">Laporan</span>
                                         </div>
                                                                              <div class="custom-control custom-switch custom-control-inline d-block mb-1">
                                             <input type="checkbox" name="arr_privilege[]" class="custom-control-input" value="data_user" id="customSwitch12">
                                             <label class="custom-control-label" for="customSwitch12">
                                             </label>
                                             <span class="switch-label">Data User</span>
                                         </div>
                                                                      </div>
                             </div>
                             <div class="col-12 d-flex flex-sm-row flex-column justify-content-start mt-1">
                                 <button type="submit" name="menu" value="hak_akses" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1 waves-effect waves-light">Simpan Hak Akses</button>
                             </div>
                         </form>
                     </div>
                 </div></form>
                 
             </div>
         </div>
     </div>
 </section>
 <!-- users edit ends -->

 <div class="modal fade text-left" id="jabatanForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h4 class="modal-title" id="myModalLabel33">Tambah Jabatan</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">×</span>
                 </button>
             </div>
             <div class="modal-body">
                 <div id="messageJabatan"></div>
                 <label>Nama Jabatan</label>
                 <div class="form-group">
                     <input type="text" name="nama_jabatan" placeholder="Masukkan Nama Jabatan" class="form-control">
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" onclick="saveJabatan()" class="btn btn-primary waves-effect waves-light">Tambah</button>
             </div>
         </div>
     </div>
 </div>

 <script>
     function addJabatan() {
         $('#messageJabatan').html('');
         $('#jabatanForm').modal('show');
     }

     function saveJabatan() {
         var nama = $('[name="nama_jabatan"]').val();
         if (nama !== '') {
             $('#messageJabatan').html('<div class="alert alert-info">Memproses ...</div>');
             $.ajax({
                 url: 'https://modistracompany.com/jabatan/add_ajax',
                 type: 'POST',
                 data: {
                     nama_jabatan: nama
                 },
                 success: function(html) {
                     $('#messageJabatan').html('<div class="alert alert-success">Berhasil menambahkan</div>');
                     $('[name="id_jabatan"]').html(html);
                     $('[name="nama_jabatan"]').val('');
                     $('#jabatanForm').modal('hide');
                 }
             });
         }
     }

     function updatePhoto() {
         var file_data = $('[name="foto"]').prop('files')[0];
         if (file_data !== '') {
             $('#uploadMessage').html('<small class="text-info ml-75">Mengupload foto ...</small>');
             var form_data = new FormData();
             form_data.append('foto', file_data);
             form_data.append('id_user', 6);
             $.ajax({
                 url: 'https://modistracompany.com/manage_user/update_photo',
                 dataType: 'JSON',
                 cache: false,
                 contentType: false,
                 processData: false,
                 data: form_data,
                 type: 'post',
                 success: function(result) {
                     if (result.res) {
                         $('#profilePic').attr('src', result.link);
                         $('#uploadMessage').html('<small class="text-success ml-75">' + result.message + '</small>');
                         $('#btnHapus').show();
                     } else {
                         $('#uploadMessage').html('<small class="text-danger ml-75">' + result.message + '</small>');
                     }
                     $('#uploadMessage').show();
                 }
             });
         }
     }

     function deletePhoto() {
         var form_data = new FormData();
         form_data.append('id_user', 6);
         $.ajax({
             url: 'https://modistracompany.com/manage_user/update_photo/delete',
             dataType: 'JSON',
             cache: false,
             contentType: false,
             processData: false,
             data: form_data,
             type: 'post',
             success: function(result) {
                 if (result.res) {
                     $('#profilePic').attr('src', result.link);
                     $('#uploadMessage').html('<small class="text-success ml-75">' + result.message + '</small>');
                     $('#btnHapus').hide();
                 } else {
                     $('#uploadMessage').html('<small class="text-danger ml-75">' + result.message + '</small>');
                 }
                 $('#uploadMessage').show();
             }
         });
     }
 </script>            <!-- Dashboard Analytics end -->

        </div>
    </div>
</div>