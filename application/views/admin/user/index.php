
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>User<span>Cards</span></h2>
          <h6 class="mb-0">admin panel</h6>
      </div>
      <div class="col-lg-6 breadcrumb-right">     
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Apps    </li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active">User Cards</li>
        </ol>
    </div>
</div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <a href="<?php echo base_url();?>manage_user/new"><button type="button" class="btn btn-primary btn-block m-t-5 m-b-20"><span class="icon-user"></span> Tambah User</button></a>

            <div class="default-according style-1 faq-accordion job-accordion" id="accordionoc">
              <div class="row">
                <div class="col-xl-12">
                  <div class="card">
                    <div class="card-header">
                      <h5 class="mb-0">
                        <button class="btn btn-link pl-0" data-toggle="collapse" data-target="#collapseicon" aria-expanded="true" aria-controls="collapseicon">Filter</button>
                    </h5>
                </div>
                  <form method="POST">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />

                <div class="collapse show" id="collapseicon" aria-labelledby="collapseicon" data-parent="#accordion">
                  <div class="card-body filter-cards-view animate-chk">
                    <div class="job-filter">
                      <div class="faq-form">
                        <input class="form-control" type="text" placeholder="Nama Lengkap" value="<?= !empty($_POST['fullname']) ? $_POST['fullname'] : "";?>" name="fullname">

                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search search-icon"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                    </div>
                </div>
                <div class="job-filter">
                  <div class="faq-form">
                    <select name="id_level" class="form-control">
                          <option value="">Semua</option>
                          <?php foreach ($level as $key){
                            $selected = (!empty($_POST['id_level']) && $_POST['id_level']==$key->level_id) ? "selected" : "";
                            ?>
                            <option <?=$selected;?>  value="<?php echo $key->level_id ?>"><?php echo $key->level ?></option>
                        <?php } ?>
                    </select>


                </div>
            </div>

            <button class="btn btn-outline-primary text-center m-t-20 m-b-20 " type="button">Cari</button>
        </form>
        </div>
    </div>
</div>
</div>




</div>
</div>
</div>

<div class="col-md-9">
    <div class="row">

        <?php
        foreach ($query as $row) {
            $status = "check";
            if ($row->user_status!="Active") $status="cancel";
            ?>

            <div class="col-md-6 col-lg-6 col-xl-4 box-col-6 xl-50">
                <div class="card custom-card">

                  <div class="card-profile" style="margin-top: 50px" ><img class="rounded-circle" src="../assets/images/avtar/3.jpg" alt=""></div>

                  <div class="text-center profile-details">
                    <h4><?php echo $row->full_name; ?></h4>
                    <h6><?php echo $row->level; ?></h6>
                </div>
                <div class="card-footer row">
                    <div class="col-12 col-md-12">
                     <a href="<?php echo base_url('manage_user/contact/'.$row->user_id); ?>"> <button type="button" class="btn btn-outline-primary "> Detail</button></a>
                  </div>

              </div>
          </div>
      </div>

  <?php } ?>


</div>
</div>

<!-- Container-fluid Ends-->
<nav aria-label="Page navigation example">


        <ul class="pagination pagination-success justify-content-center mt-2">

          <?php



          $CI =& get_instance();
          $CI->load->library('pagination');

          $config['base_url'] = base_url(). 'manage_user/index/';
          $config['total_rows'] = $total_rows;
          $config['per_page'] = $per_page;

          $config['cur_tag_open']     = '<li class="page-item active" aria-current="page"><a style="padding:10px 15px"  class=" page-link">';
          $config['cur_tag_close']    = '<a class="sr-only">(current)</a></a></li>';


          $config['page_query_string']=TRUE;
          $CI->pagination->initialize($config);
          $link = $CI->pagination->create_links();

          $link = str_replace("<li>",'<li class="page-item">',$link);
          $link = str_replace('data-ci-pagination-page','class="page-link" data-ci-pagination-page',$link);
          echo $link;

          ?>
      </ul>
  </nav>

</div>
</div>
</div>
<!-- Container-fluid Ends-->
</div>






</div>
</div>
</div>
