
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>User<span>Add</span></h2>
          <h6 class="mb-0">Bogor Comand Center</h6>
      </div>
      <div class="col-lg-6 breadcrumb-right">     
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Apps    </li>
            <li class="breadcrumb-item">Users</li>
            <li class="breadcrumb-item active">User Cards</li>
        </ol>
    </div>
</div>
</div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
  <div class="card">
    <div class="card-content">
        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <form class="form-horizontal form-material" method='post' enctype="multipart/form-data">

                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                        <?php if (!empty($message)) echo "
                        <div class='alert alert-$message_type'>$message</div>";?>
                        <h3 class="text-primary">Data Pribadi :</h3>


                        <div class="form-group">
                            <label class="col-md-12">Nama Pengguna</label>
                            <div class="col-md-12">
                                <input type="text" name="username"  class="form-control form-control-line">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Kata Sandi</label>
                            <div class="col-md-12">
                                <input type="password" name="password"  class="form-control form-control-line">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Ulangi Kata Sandi</label>
                            <div class="col-md-12">
                                <input type="password" name="repassword"  class="form-control form-control-line">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-12">Level Pengguna</label>
                            <div class="col-md-12">
                                <select onchange="select_level()" id="user_level" name="user_level" class="form-control">
                                    <?php foreach ($level as $key): ?>
                                        <option value="<?php echo $key->level_id ?>"><?php echo $key->level ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="row-mentor" style="display:none">
                            <label class="col-md-12">SKPD</label>
                            <div class="col-md-12">
                                <select  id="id_mentor" name="id_mentor" class="form-control select2">
                                    <option value="">Pilih</option>
                                    
                                    <option value="">SKPD</option>

                                </select>
                            </div>
                        </div>

                        


                        <div class="form-group">
                          <label class="col-md-12">Foto</label>
                          <div class="col-md-12">
                            <input type="file" name='userfile'  id="input-file-now-custom-3" class="dropify"  data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-12">Nama Lengkap</label>
                        <div class="col-md-12">
                            <input type="text" name="employee_name" placeholder="fullname" class="form-control form-control-line">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-md-12">Tanggal Lahir</label>
                        <div class="col-md-12">
                            <input type="text" name="birth_date" placeholder="12" class="form-control form-control-line mydatepicker" value="<?php echo date('Y-m-d');?>" data-format="D, dd MM yyyy">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Jenis Kelamin</label>
                        <div class="col-md-12">
                            <select name="gender" class="form-control form-control-line">
                                <option value='male'>Male</option>
                                <option value='female'>Female</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Alamat</label>
                        <div class="col-md-12">
                            <textarea rows="5" name="employee_address" class="form-control form-control-line"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group ">
                        <label class="col-md-12">Telepon</label>
                        <div class="col-md-12">
                            <input type="text" name="employee_phone" placeholder="" class="form-control form-control-line">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">Email</label>
                        <div class="col-md-12">
                            <input type="email" name="employee_email" placeholder="" class="form-control form-control-line">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">Status Pengguna</label>
                        <div class="col-md-12">
                            <select name="user_status" class="form-control">
                                <option value="Active">Aktif</option>
                                <option value="Not Active">Nonaktif</option>
                            </select>
                        </div>
                    </div>




                </div>


                <div class="col-6">

                 <h3 class="text-primary">Hak Akses :</h3>

                 <div class="media m-t-5 ">
                    <label class="col-form-label m-r-10">Perizinan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>
                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Pajak</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Layanan Publik</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Keuangan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Penduduk</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">wisata</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">UMKM</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">ASN</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>
                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Pendidikan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Sosial</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Kesehatan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Sakip</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Pertanian</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Perternakan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Perikanan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Transfortasi</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Keamanan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>

                  <div class="media m-t-5">
                    <label class="col-form-label m-r-10">Kepemudaan</label>
                    <div class="media-body text-right icon-state">
                      <label class="switch">
                        <input type="checkbox" checked=""><span class="switch-state"></span>
                    </label>
                     </div>
                </div>


    </div>
     <div class="form-group" >
                <div class="col-md-12 offset-md-12">
                    <a href="<?=base_url();?>manage_user" class="btn btn-light m-l-10"><span class="icon-angle-left"></span> Batal</a>

                </div>
            </div>

    <div class="form-group" >
                <div class="col-md-12 offset-md-12"><button type="submit" class="btn btn-primary m-l-10" style="float: right;"><span class="icon-save"></span> Simpan</button>
                    
                </div>
            </div>
        </form>
</div>
</div>



<script>
    function select_level()
    {
      var user_level = $("#user_level").val();
  if(user_level==2) // mentor
  {
    $("#row-mentor").show();
}
else{
    $("#row-mentor").hide();
}
$("#id_mentor").val("").trigger("change");
}
</script>

</div>
</div>
</div>
</div>
</div>
</div>
