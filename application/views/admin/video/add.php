 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Video</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url();?>manage_video">Manage Video</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">add</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <?php if (!empty($message)) echo "
                    <div class='alert alert-$message_type'>$message</div>";?>
                    <form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />

                        <div class="form-group">
                            <label class="control">Judul Video</label>
                            <input type="text" class="form-control" id='judul' name='judul' placeholder="">

                        </div>
                        <div class="form-group">
                            <label class="control">Link Youtube</label>
                            <input type="text" class="form-control"  name='link' placeholder="">
                        </div>
                        <div class="form-group">
                            <label class="control">Kategori</label>
                            <select name="category_video_id" class="form-control">
                                <?php foreach($category as $k){?>
                                    <option value="<?=$k->category_video_id?>"><?=$k->category_video_name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control">Deskripsi</label>

                            <textarea class="form-control" rows="18" name="content" id="post_content">
                            </textarea>
                        </div>

                        <div class="form-group">
                            <div class="col12">
                               <button type="submit" class="btn btn-primary waves-effect waves-light pull-right mb-1" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Simpan</button>

                           </div>

                       </div>
                   </form>

               </div>
           </div>
       </div>
   </div>
</div>
</div>
