<style type="text/css">
  #peta {
    height: 65vh;
    width: 100%;
  }
</style>

<div class="col-lg-12 xl-100">
  <div class="row">
    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5>List Dinas</h5>
          <div class="card-header-right">
            <?php
            $total_data_tahun_ini = count($data_listopd);
            if ($total_data_tahun_ini >= 1) {

            }
            ?>
           
          </div>
          
        </div>

        
        <div class="card-body p-0">
          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_card_">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama Dinas</th>
                  <th scope="col">Jumlah Integrasi cc</th>
                  <th scope="col">weblist integrasi cc</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php echo $i=1;?>
                <?php foreach ($data_listopd as $key => $data): ?>
                <tr >
                  <td class="f-w-600"> <?php echo $i++ ?> </td>
                  <td class="f-w-600"> <?php echo $data->nm_dinas?> </td>
                  <td class="f-w-600"> <?php echo $data->jml_icc?> </td>
                  <td class="f-w-600"> <?php echo $data->web_list?> </td>
          
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('listopd/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready( function () {
    $('#table_card_').DataTable();
} );
</script>
