
<?php $this->load->view('admin/src/head');?>
  <!-- <link rel="stylesheet" type="text/css" href="<?=base_url()?>asset/kube/app-assets/css/pages/data-list-view.css"> -->
    <!--- Font Icon -->
    <!-- <link rel="stylesheet" href="<?=base_url()?>asset/map/css/fontawesome.min.css"> -->
    <link rel="stylesheet" href="<?=base_url()?>asset/map/css/icomoon.css">
    
    <!-- Place favicon.ico in the root directory -->
    <!-- <link rel="shortcut icon" href="<?=base_url()?>asset/map/images/favicon.ico"> -->

    <!-- Plugins css -->
    <!-- <link rel="stylesheet" href="<?=base_url()?>asset/map/css/plugins.css"> -->
    
    <!-- Theme Style -->
    <link rel="stylesheet" href="<?=base_url()?>asset/map/style.css">
    
    <style type="text/css">

      html,body { height: 100%; margin: 0px; padding: 0px; }
      .full { height: 100% }
      .listing-google-map-content.full #map_todo_listing { height: 100% }

      footer {    position: absolute !important; right: 0 !important; bottom: 0 !important;}
      footer:hover {opacity: 0;}

      .iconbar-mainmenu.custom-scrollbar {top: 0 !important; height: calc(100vh) !important;}

      #map_todo_listing > div > div > div:nth-child(13) > div > div:nth-child(2) > div {margin-top: -40px}

      .infoBox > img:nth-child(1) {width :20px; z-index :9000000;}

    </style>
    <!-- Modernizr js -->


<body>
<!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>Bogor data loading..</h1>
      </div>
    </div>



        <!-- page-wrapper Start-->
    <div class="page-wrapper full">
      <!-- Page Header Start-->
          <?php $this->load->view('admin/src/top_nav');?>
     
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper full">
        <!-- Page Sidebar Start-->
        <div class="iconsidebar-menu iconbar-mainmenu-close">
         <?php $this->load->view('admin/src/menu');?>

   

        </div>
        <!-- Page Sidebar Ends-->

        <?php 
            if ($content!="") {
                $this->load->view("admin/map_view");
            }
        ?>

        <!-- Right sidebar Ends-->
       



   <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020<a class="text-bold-800 grey darken-2" href="" target="_blank">Bogor,</a>All rights Reserved</span><span class="float-md-right d-none d-md-block">Hand-crafted & Made with<i class="feather icon-heart pink"></i></span>
            <!-- <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button> -->
        </p>
    </footer>
    <!-- END: Footer-->


    	<?php $this->load->view('admin/src/bottom');?>

    <!-- jQuery js -->
    <!-- <script src="<?=base_url()?>asset/map/js/jquery.min.js"></script> -->
    <!-- Bootstrap js -->
    <!-- <script src="<?=base_url()?>asset/map/js/bootstrap.min.js"></script> -->
    <!-- Plugin js -->
    <script src="<?=base_url()?>asset/map/js/plugins.js"></script>
    <!-- Google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBF-EKYJaTXFn5AsQudXlemdxuzApgTTjw&amp;libraries=places&amp;"></script>
    <!-- Markerclusterer js -->
    <script src="<?=base_url()?>asset/map/js/markerclusterer.js"></script>
    <!-- Maps js -->
    <script src="<?=base_url()?>asset/map/js/regionmap.js"></script>
    <script src="<?=base_url()?>asset/map/js/mapstyles.js"></script>
    <script src="<?=base_url()?>asset/map/js/maps.js"></script>
    <!-- Infobox js -->
    <script src="<?=base_url()?>asset/map/js/infobox.min.js"></script>
    <!-- main js -->
    <script src="<?=base_url()?>asset/map/js/main.js"></script>

    
    <!-- <script src="<?=base_url()?>asset/kube/app-assets/js/scripts/ui/data-list-view.js"></script> -->

    <script type="text/javascript">

	  $(document).ready(function(){
	    	$('.page-main-header').fadeOut();
	        $('#c-pills-tab').fadeOut();
	        $( ".page-body-wrapper" ).addClass( "scrolled" );
	        $( ".iconsidebar-menu" ).addClass( "scrolled" );
	        $( ".iconMenu-bar" ).addClass( "scrolled" );
	        $( "#scroll-top" ).hide();
	  });

        function hide_particle() {
         	$('.iconsidebar-menu').fadeOut();
         	$('#map-filter').fadeOut();
         	$('.footer').fadeOut();
         }
        function show_particle() {
         	$('.iconsidebar-menu').fadeIn();
         	$('#map-filter').fadeIn();
         	$('.footer').fadeIn();
         }
    </script>
</body>
<!-- END: Body-->

</html>













