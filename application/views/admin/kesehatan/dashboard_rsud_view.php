
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-primary o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="database"></i></div>
                      <div class="media-body"><span class="m-0 text-white">Jumlah Pasien</span>
                        <h4 class="mb-0 counter">0</h4><i class="icon-bg" data-feather="database"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-secondary o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                      <div class="media-body"><span class="m-0">Jumlah Poli</span>
                        <h4 class="mb-0 counter">0</h4><i class="icon-bg" data-feather="shopping-bag"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-warning o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center">
                        <div class="text-white i" data-feather="message-circle"></div>
                      </div>
                      <div class="media-body"><span class="m-0 text-white">Total Bed</span>
                        <h4 class="mb-0 counter text-white">0</h4><i class="icon-bg" data-feather="message-circle"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                <div class="card gradient-info o-hidden">
                  <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                      <div class="align-self-center text-center">
                        <div class="text-white i" data-feather="user-plus"></div>
                      </div>
                      <div class="media-body"><span class="m-0 text-white">Nilai Kepuasan</span>
                        <h4 class="mb-0 counter text-white">A</h4><i class="icon-bg" data-feather="user-plus"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-7 col-md-12 box-col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-9">
                        <h5>Pasien Rumah Sakit</h5>
                      </div>
                      <div class="col-3 text-right"><i class="text-muted" data-feather="navigation"></i></div>
                    </div>
                  </div>
                  <div class="card-body pt-0 px-0">
                    <div id="line-adwords"></div>
                  </div>
                </div>
              </div>
              <div class="col-xl-5 col-lg-12 box-col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-9">
                        <h5>Kelompok Pasien</h5>
                      </div>
                      <div class="col-3 text-right"><i class="text-muted" data-feather="shopping-bag"></i></div>
                    </div>
                  </div>
                  <div class="card-body pt-0 r-radial">
                    <div id="piechart"></div>
                  </div>
                </div>
              </div>

              <div class="col-lg-3">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-center">
                          <span class="badge badge-pill badge-primary pull-right">Kosong</span><br/>
                          <h7 class="mt-2">Ruangan</h7>
                          <h4 class="f-w-600 mt-2"><a href="#!">Angrek</a></h4>
                          <p class="mt-2">Kelas 1</p>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="row border-top text-center mt-3">
	                    <div class="col-6 mt-3">
	                      <span>Total Bed</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                    <div class="col-6 mt-3">
	                      <span>Tersedia</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-center">
                          <span class="badge badge-pill badge-primary pull-right">Kosong</span><br/>
                          <h7 class="mt-2">Ruangan</h7>
                          <h4 class="f-w-600 mt-2"><a href="#!">Angrek</a></h4>
                          <p class="mt-2">Kelas 1</p>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="row border-top text-center mt-3">
	                    <div class="col-6 mt-3">
	                      <span>Total Bed</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                    <div class="col-6 mt-3">
	                      <span>Tersedia</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-center">
                          <span class="badge badge-pill badge-primary pull-right">Kosong</span><br/>
                          <h7 class="mt-2">Ruangan</h7>
                          <h4 class="f-w-600 mt-2"><a href="#!">Angrek</a></h4>
                          <p class="mt-2">Kelas 1</p>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="row border-top text-center mt-3">
	                    <div class="col-6 mt-3">
	                      <span>Total Bed</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                    <div class="col-6 mt-3">
	                      <span>Tersedia</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                  </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-center">
                          <span class="badge badge-pill badge-primary pull-right">Kosong</span><br/>
                          <h7 class="mt-2">Ruangan</h7>
                          <h4 class="f-w-600 mt-2"><a href="#!">Angrek</a></h4>
                          <p class="mt-2">Kelas 1</p>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="row border-top text-center mt-3">
	                    <div class="col-6 mt-3">
	                      <span>Total Bed</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                    <div class="col-6 mt-3">
	                      <span>Tersedia</span>
	                      <div class="follow-num counter"><strong>0</strong></div>
	                    </div>
	                  </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-left">
                          <span class="badge badge-pill badge-primary pull-right">Buka</span>
                          <h6 class="f-w-600 mt-2"><a href="#!">Ploklinik <b>Gigi</b></a></h6>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="media border-top mt-3"><img class="img-40 img-fluid m-r-20 mt-3" src="../assets/images/job-search/1.jpg" alt="">
                        <div class="media-body mt-3">
                          <h6 class="f-w-600">drg. Rikky Bagus Pratama Putra</h6>
                          <p>08:00 - 19:00</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-left">
                          <span class="badge badge-pill badge-primary pull-right">Buka</span>
                          <h6 class="f-w-600 mt-2"><a href="#!">Ploklinik <b>Gigi</b></a></h6>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="media border-top mt-3"><img class="img-40 img-fluid m-r-20 mt-3" src="../assets/images/job-search/1.jpg" alt="">
                        <div class="media-body mt-3">
                          <h6 class="f-w-600">drg. Rikky Bagus Pratama Putra</h6>
                          <p>08:00 - 19:00</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="card">
                  <div class="job-search">
                    <div class="card-body">
                      <!-- <div class="media"> -->
                        <div class="media-body text-left">
                          <span class="badge badge-pill badge-primary pull-right">Buka</span>
                          <h6 class="f-w-600 mt-2"><a href="#!">Ploklinik <b>Gigi</b></a></h6>
                        </div>
                      <!-- </div> -->
                      
	                  <div class="media border-top mt-3"><img class="img-40 img-fluid m-r-20 mt-3" src="../assets/images/job-search/1.jpg" alt="">
                        <div class="media-body mt-3">
                          <h6 class="f-w-600">drg. Rikky Bagus Pratama Putra</h6>
                          <p>08:00 - 19:00</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 xl-100 box-col-12">
                <div class="card o-hidden">
                  <div class="card-header">
                    <h5>Sumber Daya Manusia</h5>
                    <div class="card-header-right">
                      <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                        <li><i class="view-html fa fa-code font-primary"></i></li>
                        <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                        <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                        <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                        <li><i class="icofont icofont-error close-card font-primary"></i></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="user-status cart-table table-responsive">
                      <table class="table table-bordernone">
                        <thead>
                          <tr>
                            <th scope="col">Poli</th>
                            <th scope="col">Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">Poliklinik Gigi</td>
                            <td class="digits">3</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 xl-100 box-col-12">
                <div class="card o-hidden">
                  <div class="card-header">
                    <h5>Fasilitas</h5>
                    <div class="card-header-right">
                      <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                        <li><i class="view-html fa fa-code font-primary"></i></li>
                        <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                        <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                        <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                        <li><i class="icofont icofont-error close-card font-primary"></i></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="user-status cart-table table-responsive">
                      <table class="table table-bordernone">
                        <thead>
                          <tr>
                            <th scope="col">Poli</th>
                            <th scope="col">Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="f-w-600">Gamma Camera</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">PET CT</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">CT Scan</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">MRI</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">ESMR</td>
                            <td class="digits">0</td>
                          </tr>
                          <tr>
                            <td class="f-w-600">ESWL</td>
                            <td class="digits">0</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

<script type="text/javascript">
  var optionsLine = {
   yaxis: {
     show: false,
   },
  grid: {
    show: false,
  },

  xaxis: {
     axisBorder: {
          show: true,
          color: '#cccccc',
          height: 1,
          width: '100%',
          offsetX: 0,
          offsetY: 0
      },  
  },

  chart: {
    height: 360,
    type: 'line',
    zoom: {
      enabled: false
    },
    toolbar: {
        show: false
    }
  },
  stroke: {
    curve: 'smooth',
    width: 4
  },
  colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  series: [{
      name: "Umum",
      data: [1, 2, 3, 4, 5, 6]
    },
    {
      name: "BPJS",
      data: [1, 2, 3, 4, 5, 6]
    },
  ],
  subtitle: {
    text: 'Pasien',
    offsetY: 0,
    offsetX: 0
  },
  markers: {
    size: 6,
    strokeWidth: 0,
    hover: {
      size: 9
    }
  },
  labels: ['01/15/2020', '01/16/2020', '01/17/2020', '01/18/2020', '01/19/2020', '01/20/2020'],
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    offsetY: -20
  }
}
var chartLine = new ApexCharts(document.querySelector('#line-adwords'), optionsLine);
chartLine.render();


var optionsCircle4 = {
  
  chart: {
    type: 'radialBar',
    width: 490,
    height: 360,
  },
  plotOptions: {
    radialBar: {
      size: undefined,
      inverseOrder: true,
      hollow: {
        margin: 5,
        size: '48%',
        background: 'transparent',
      },
      track: {
        show: false,
      },
      startAngle: -180,
      endAngle: 180
    },
  },
  stroke: {
    lineCap: 'round'
  },
  colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  series: [2, 1],
  labels: ['Umum', 'BPJS'],
  legend: {
    show: true,
    floating: true,
    position: 'right',
    offsetX: 70,
    offsetY: 240
  },
}

var chartCircle4 = new ApexCharts(document.querySelector('#radialBarBottom'), optionsCircle4);
chartCircle4.render();

var options8 = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Umum', 'BPJS'],
    series: [44, 55],
    legend: {
        position: 'bottom'
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:[pocoAdminConfig.primary, '#fd517d', '#80cf00', '#06b5dd', '#fd517d']
}

var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
);

chart8.render();

</script>

<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>