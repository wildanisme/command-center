<script type="text/javascript" src="<?php echo base_url()."asset" ;?>/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
		selector: "#tentang",
		theme: "modern",
		plugins: [ 
		"advlist autolink link image lists charmap print preview hr anchor pagebreak", 
		"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking", 
		"table contextmenu directionality emoticons paste textcolor filemanager" 
		], 
		image_advtab: true, 
		toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
	});
</script>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Setting Antrian</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			
			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url();?>admin"><i class="entypo-home"></i>Home</a>
				</li>
				
				<li class="active">		
					<strong>Setting Antrian</strong>
				</li>
			</ol>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- .row -->
	<div class="row">
		<div class="col-sm-6">
			<div class="white-box">

				<div class="row">
					<div class="col-md-12">
						
						<div class="panel panel-primary" data-collapsed="0">
							
							
							
						</div>
						<div class="panel-body">
							<?php if (!empty($message)) echo "
							<div class='alert alert-$message_type'>$message</div>";?>
							<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-2 control-label">Logo</label>
									
									<div class="col-sm-10">
										<?php echo"
										<div class='member-img'>
										<img src='".base_url()."data/logo/antrian/$logo' class='img-rounded' style='max-width:200px' />
										
										</div><br>
										";?>
										<input type="file" name='userfile' id="input-file-now-custom-3" class="dropify"  data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
										<p>
											Max : 500px | 1MB
										</p>
										<?php if (!empty($error)) echo "
										<div class='alert alert-warning'>$error</div>";?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">header 1</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value='<?php echo $header;?>' name='header' placeholder="">
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">header 2</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value='<?php echo $header2;?>' name='header2' placeholder="">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label">header 3</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value='<?php echo $header3;?>' name='header3' placeholder="">
									</div>
								</div>

								
								<div class="form-group">
									<label class="col-sm-2 control-label">tagline</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name='tagline' placeholder="" value='<?php echo $tagline;?>' >
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-sm-2 control-label">Footer</label>
									<div class="col-sm-10">
										<input type="text" class="form-control"  name='footer' placeholder="" value='<?php echo $footer;?>' >
									</div>
								</div>


								<div class="form-group">
									<label class="col-sm-2 control-label">Footer 2</label>
									<div class="col-sm-10">
										<input type="text" class="form-control"  name='footer2' placeholder="" value='<?php echo $footer2;?>' >
									</div>
								</div>

								
								
								<div class="form-group">
									<div class="col-sm-2"></div>
									<div class="col-sm-10">
										<button type="submit" class="btn btn-primary waves-effect waves-light pull-right" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Perbarui</button>	
									</div>
									
								</div>
							</form>
							
						</div>

					</div>
				</div>
			</div>


		</div>

		<div class="col-sm-6">
			<div class="white-box">
				<span class="label label-info m-l-5">Preview</span>

				<div width="100%" padding:="" 0px="" 0px;="" font-family:arial;="" line-height:28px;="" height:100%;="" width:="" 100%;="" color:="" #514d6a;"="">
					<div style="max-width: 600px; padding-left:30px; padding-right: 30px; padding-top: 5px;  margin: 0px auto; font-size: 14px">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
							<tbody>


								<tr>
									<td style="vertical-align: top; " align="center">
										<!-- <img src="http://202.93.229.205/im-pos/data/logo/logotoko.png" alt="home" class="light-logo" style="width: 60px;margin-left: -10px;" alt=" Responsive web app kit" style="border:none"> -->

										<img src="<?php echo base_url()."data/logo/antrian/$logo"?>" style="width: 150px;margin-top: 50px" alt="user-img" class="img-circle">
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top; " align="center">
										<strong><?php echo $header;?></strong>
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top; " align="center">
										<?php echo $header2;?>
									</td>
								</tr>
								
								<tr>
									<td style="vertical-align: top; " align="center">
										<?php echo $header3;?>
									</td>
								</tr>

								<tr style="background: #161616;color: #ffffff;height: 40px;vertical-align: middle;" >
									<td style="vertical-align: top;padding-top:10px; border-top:1px solid #f6f6f6;" align="center">
										<strong>SISTEM ANTRIAN  </strong>       </td>
									</tr>
									<tr>

									</tr>

									<tr>
										<td style="vertical-align: top; " align="center">

										</td>
									</tr>



								</tbody>
							</table>

							<hr>

							<div style="padding-top:10px;background: #fff;">

								<div class="row  b-b">
									<div class="col-md-7">
										<b>No Antrian :</b>
										<strong>A-002</strong></h1>
										<br>
										<b>Loket</b>
										<h1 style="font-size: 100px,"><strong>A</strong></h1>
										<p>27-09-2019 Time : 09:00:00</p>
										<br>
										<p>Silakan scan Qrcode<br>tersebut untuk melihat<br>status antrian</p>

									</div>
									<div class="col-md-5">
										<img src="http://202.93.229.205/im-pos/data/toko/qr_code/sample.png" alt="home" class="light-logo" style="width: 250px;margin-left: -10px;">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 p-t-10">
										<center>
											<strong> -- <?php echo $tagline;?> --</strong> 

											<p><?php echo $footer;?></p>
											<p><?php echo $footer2;?></p>

										</center>

									</div>

								</div>





							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
