
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Sambutan</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url();?>admin"><i class="entypo-home"></i>Home</a>
				</li>

				<li class="active">		
					<strong>Sambutan</strong>
				</li>
			</ol>
		</div>
		<!-- /.col-lg-12 -->
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<a href="<?=base_url()?>manage_company_profile/add_sambutan" class="btn btn-primary btn-sm waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a>
				<hr>
				<table class="table table-striped" id="myTable">
					<thead>
						<tr>
							<th width=70px>#</th>
							<th>Nama</th>
							<th>Jabatan</th>
							<th width=60px>Foto</th>
							<th width=70px>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$num = 1;
						foreach ($query as $row) {
							echo"
							<tr>
							<td>$num</td>
							<td>$row->nama</td>
							<td>$row->jabatan</td>
							<td>
							<img src='".base_url()."/data/images/sambutan/$row->foto' style='max-width:50px' />
							</td>

							<td>
							<a href='".base_url()."manage_company_profile/edit_sambutan/$row->id_sambutan' class='btn-xs' title='Edit'>

							<i class='entypo-pencil'></i>
							</a>
							<a class='btn-xs' title='Delete' onclick='jQuery(\"#confirm\").modal(\"show\");delete_(\"$row->id_sambutan\")'>
							<i class='entypo-cancel'></i>
							</a>

							</td>
							</tr>
							";

							$num++;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
		<script type="text/javascript">
			function delete_(id)
			{
				$('#confirm_title').html('Confirmation');
				$('#confirm_content').html('Are you sure want to delete it?');
				$('#confirm_btn').html('Delete');
				$('#confirm_btn').attr('href',"<?php echo base_url();?>manage_company_profile/delete_sambutan/"+id);
			}
		</script>