 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Dashboard Perizinan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Perizinan</li>
            <li class="breadcrumb-item active">Dashboard  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <!-- <div class="row"> -->

                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Monitoring Kinerja SKPD</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                      <form method="get">
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                            <input type="date" id="tgl_awal" name="tgl_awal" class="form-control" value="<?=$tgl_awal?>" onchange="cetak();" required="">
                                </div>
                                <div class="col-md-3">
                            <input type="date" id="tgl_akhir" name="tgl_akhir" class="form-control" value="<?=$tgl_akhir?>" onchange="cetak();"  required="">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary m-l-20 btn-rounded btn-outline waves-effect waves-light">Tampilkan</button>
                            <a id="cetak" href="#!" class="btn btn-dark m-l-20 btn-rounded btn-outline waves-effect waves-light">Cetak</a>
                                </div>
                            </div>
                      </form>
                    </div>
                </div>
                <!-- /.row -->

            <?php foreach ($list_skpd as $skpd): ?>
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img src="https://ptsp.sumedangkab.go.id/app/data/user_picture/user_default.png" alt="user" width="100%">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)"><div data-label="<?=$skpd->persentase?>%" class="css-bar css-bar-<?=roundUpToAny($skpd->persentase,5)?> css-bar-lg css-bar-primary" style="margin-top: -20px;"></div></a>
                                        <h5 class="text-white">Persentase Ketepatan Waktu</h5> 
                                        <h4 class="text-white"><?=$skpd->nama_skpd?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-purple">TOTAL</p>
                                    <h1><?=$skpd->count?></h1> </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue">TERBIT</p>
                                    <h1><?=$skpd->terbit?></h1> </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger">DITOLAK</p>
                                    <h1><?=$skpd->batal?></h1> </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-blue"><b class="text-dark"><?=$skpd->tepat?></b> TEPAT WAKTU</p> </div>
                                <div class="col-md-6 col-sm-6 text-center">
                                    <p class="text-danger"><b class="text-dark"><?=$skpd->lambat?></b> TERLAMBAT</p> </div>
                                <div class="stats-row col-md-12 m-t-10 m-b-0 text-center">
                                    <div class="stat-item">
                                        <h6>Rata-rata</h6> <b><i class="ti-time"></i> <?=$skpd->ratarata?></b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title" style="position: absolute;">Grafik</h3>
                            <ul class="list-inline text-right">
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #00b5c2;"></i>Persentase Ketepatan</h5>
                                </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #2c5ca9;"></i>Total Rekomendasi</h5>
                                </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #f75b36;"></i>Rata-Rata Waktu</h5>
                                </li>
                            </ul>
                            <div id="grafik-<?=$skpd->id_skpd?>" style="height: 356px;"></div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            <?php $nid_skpd = $skpd->id_skpd; foreach ($list_unit->$nid_skpd as $unit): ?>

                <div class="row">

                    <div class="col-md-12 col-sm-12 ">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2><?=$unit->full_name?></h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Settings 1</a>
                                  <a class="dropdown-item" href="#">Settings 2</a>
                                </div>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                          <div class="dashboard-widget-content">

                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <div class="white-box m-b-0 bg-info">
                                    <h3 class="text-white box-title">PRESENTASE REKOMENDASI TERBIT <span class="pull-right"><?=$unit->persentase?>%</span></h3>
                                    <div id="presentase-rekomendasi-<?=$unit->user_id?>" class="text-center"></div>
                                </div>
                                <div class="white-box">
                                    <div class="row">
                                        <div class="pull-left m-r-30">
                                            <div class="text-blue m-t-20">TEPAT WAKTU</div>
                                            <h2><?=$unit->tepat?></h2>
                                        </div>
                                        <div class="pull-left">
                                            <div class="text-danger m-t-20">TERLAMBAT</div>
                                            <h2><?=$unit->lambat?></h2>
                                        </div>
                                        <div data-label="<?=$unit->persentase?>%" class="css-bar css-bar-<?=roundUpToAny($unit->persentase,5)?> css-bar-lg m-b-0 css-bar-primary pull-right"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <div class="white-box m-b-0 bg-primary">
                                    <h3 class="text-white box-title">TOTAL REKOMENDASI <span class="pull-right"><?=$unit->count?></span></h3>
                                    <div id="total-rekomendasi-<?=$unit->user_id?>" class="text-center"></div>
                                </div>
                                <div class="white-box">
                                    <div class="row">
                                        <div class="pull-left">
                                            <ul class="country-state">
                                                <li>
                                                    <h2><?=$unit->terbit?> <small class="text-blue">TERBIT</small></h2> 
                                                </li>
                                                <li>
                                                    <h2><?=$unit->proses?> <small class="text-info">PROSES</small></h2>
                                                </li>
                                                <li>
                                                    <h2><?=$unit->batal?> <small class="text-danger">DITOLAK</small></h2>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="proses-rekomendasi-<?=$unit->user_id?>" class="text-center pull-right"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-xs-12 col-sm-12">
                                <div class="white-box m-b-0 bg-dark">
                                    <h3 class="text-white box-title">WAKTU RATA-RATA PROSES REKOMENDASI</h3>
                                    <span class="text-center text-white" style="position: absolute;top: 0;right: 0;left: 0;padding-top: 140px;"><i class="ti-time"></i> <?=$unit->ratarata?></span>
                                    <div id="ratarata-rekomendasi-<?=$unit->user_id?>" class="text-center"></div>
                                </div>
                                <div class="white-box">
                                    <div class="row">
                                        <div>
                                            <div class="text-blue m-t-0">REKOMENDASI TERCEPAT</div>
                                            <h5><i class="ti-time"></i> <?=$unit->tercepat?></h5>
                                            <div class="text-danger m-t-0 text-right">REKOMENDASI TERLAMA</div>
                                            <h5 class=" text-right"><i class="ti-time"></i> <?=$unit->terlama?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-xs-12 col-sm-12">

                                <div class="message-center">
                                    <div class="row">

                                        <?php $nuser_id = $unit->user_id; foreach ($list_izin->$nid_skpd->$nuser_id as $izin): ?>
                                        <div class="col-md-6">
                                            <a href="#!">
                                                <div class="user-img"> 
                                                    <div class="img-circle" style="width: 50px; height: 50px;">
                                                        <div data-label="<?=$izin->persentase?>%" class="css-bar css-bar-<?=roundUpToAny($izin->persentase,5)?> css-bar-sm css-bar-primary" style="position: absolute;"></div>
                                                    </div>
                                                </div>
                                                <div class="mail-contnet" style="margin-left: 70px;">
                                                    <h5><?=$izin->nama_jenisizin?>
                                                        <span class="pull-right">TOTAL: <?=$izin->count?></span>
                                                    </h5>
                                                    <span class="mail-desc">Rata-rata: <?=$izin->ratarata?>
                                                        <span class="pull-right m-l-5 text-danger">DITOLAK: <b class="text-dark"><?=$izin->batal?></b></span>
                                                        <span class="pull-right m-l-5 text-info">PROSES: <b class="text-dark"><?=$izin->proses?></b></span>
                                                        <span class="pull-right m-l-5 text-blue">TERBIT: <b class="text-dark"><?=$izin->terbit?></b></span>
                                                    </span>
                                                    <span class="time">Tercepat: <?=$izin->tercepat?>
                                                        <span class="pull-right m-l-5 text-danger">TERLAMBAT: <b class="text-dark"><?=$izin->lambat?></b></span>
                                                        <span class="pull-right m-l-5 text-blue">TEPAT WAKTU: <b class="text-dark"><?=$izin->tepat?></b></span>
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
                                        <?php endforeach ?>

                                    </div>
                                </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                <script type="text/javascript">
                    $( document ).ready(function() {

                        $('#total-rekomendasi-<?=$unit->user_id?>').sparkline(<?=@json_encode(array_values($unit->count_g))?>, {
                            type: 'bar',
                            height: '70',
                            barWidth: '5',
                            resize: true,
                            barSpacing: '10',
                            barColor: '#fff'
                        });

                        $('#proses-rekomendasi-<?=$unit->user_id?>').sparkline([<?=$unit->batal?>, <?=$unit->proses?>, <?=$unit->terbit?>], {
                            type: 'pie',
                            height: '100',
                            resize: true,
                            sliceColors: [ '#f75b36', '#008efa', '#02bec9']
                        });

                        $("#presentase-rekomendasi-<?=$unit->user_id?>").sparkline(<?=@json_encode(array_values($unit->persentase_g))?>, {
                            type: 'line',
                            width: '100%',
                            height: '70',
                            lineColor: '#fff',
                            fillColor: 'transparent',
                            spotColor: '#fff',
                            minSpotColor: undefined,
                            maxSpotColor: undefined,
                            highlightSpotColor: undefined,
                            highlightLineColor: undefined
                        });

                        $("#ratarata-rekomendasi-<?=$unit->user_id?>").sparkline(<?=@json_encode(array_values($unit->ratarata_g))?>, {
                            type: 'line',
                            width: '100%',
                            height: '70',
                            lineColor: '#fff',
                            fillColor: 'transparent',
                            spotColor: '#fff',
                            minSpotColor: undefined,
                            maxSpotColor: undefined,
                            highlightSpotColor: undefined,
                            highlightLineColor: undefined
                        });
                    });
                </script>

                
            <?php endforeach ?>

            <script type="text/javascript">
                $( document ).ready(function() {
                    Morris.Area({
                        element: 'grafik-<?=$skpd->id_skpd?>',
                        data: [<?php foreach ($tgl_group as $tg): ?>
                         {
                            period: '<?=$tg?>',
                            persentase: <?=$skpd->persentase_g->$tg?>,
                            total: <?=$skpd->count_g->$tg?>,
                            ratarata: <?=$skpd->ratarata_g->$tg?>
                        },<?php endforeach;?>],
                        xkey: 'period',
                        ykeys: ['persentase', 'total', 'ratarata'],
                        labels: ['Persentase Ketepatan', 'Total Rekomendasi', 'Rata-Rata Waktu'],
                        pointSize: 0,
                        fillOpacity: 0.6,
                        pointStrokeColors:['#00b5c2 ', '#008efa', '#f75b36'],
                        behaveLikeLine: true,
                        gridLineColor: '#e0e0e0',
                        lineWidth:0,
                        hideHover: 'auto',
                        lineColors: ['#00b5c2 ', '#008efa', '#f75b36'],
                        resize: true
                        
                    });
                });
            </script>

            <?php endforeach ?>



    <!-- </div> -->
  </div>


  <script src="<?=base_url();?>js/perizinan/dashboard.js" defer></script>
    <link href="https://e-office.sumedangkab.go.id/asset/pixel/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">