 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Dashboard Perizinan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Perizinan</li>
            <li class="breadcrumb-item active">Dashboard  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->

      <div class="col-xl-4 box-col-6">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-secondary">Nilai Investasi</h5>
            <div class="setting-dot">
              <div class="setting-bg-secondary position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div>
            <div id="bitcoinchart-1"></div>
          </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent">                           <i class="ion ion-social-bitcoin"> </i></div>
            </div>
            <div class="media-body">
              <h5>Rp. <?=number_format($data_perizinan->total_investasi);?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?=$tahun;?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 box-col-6">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-primary">Retribusi IMB</h5>
            <div class="setting-dot">
              <div class="setting-bg-primary position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div id="bitcoinchart-2">  </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent">                           <i class="ion ion-social-yen"> </i></div>
            </div>
            <div class="media-body">
              <h5>Rp. <?=number_format($data_perizinan->total_retribusi_imb);?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?=$tahun;?></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 box-col-12">
        <div class="card o-hidden card-bg">
          <div class="card-header no-border">
            <h5 class="font-warning">Retribusi Trayek</h5>
            <div class="setting-dot">
              <div class="setting-bg-warning position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
            </div>
          </div>
          <div id="bitcoinchart-3">  </div>
          <div class="media">
            <div class="media-left">
              <div class="btn-rounded-transparent">                           <i class="ion ion-social-yen"> </i></div>
            </div>
            <div class="media-body">
            <h5>Rp. <?=number_format($data_perizinan->total_retribusi_trayek);?></h5>
            </div>
            <div class="media-right">
              <div class="badge badge-pill badge-transparent"><i class="ion ion-arrow-up-c"></i><?=$tahun;?></div>
            </div>
          </div>
        </div>
      </div>

      <!-- status widget Start-->
      <!-- <div class="row"> -->
        <div class="col-xl-8 col-md-12 box-col-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-9">
                  <h5>Nilai Investasi</h5>
                </div>
                <div class="col-3 text-right"><i class="text-muted" data-feather="navigation"></i></div>
              </div>
            </div>
            <div class="card-body pt-0 px-0">
              <div id="grafik-investasi"></div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-12 box-col-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-9">
                  <h5>Status Perizinan</h5>
                  <p><?= date("M Y")." (".$data_perizinan->total_izin_filter.")";?></p>
                </div>
                <div class="col-3 text-right"><i class="text-muted" data-feather="shopping-bag"></i></div>
              </div>
            </div>
            <div class="card-body r-dount" style="padding-bottom: 13px">
              <div id="grafik-status-perizinan" style="height: 360px"> </div>
            </div>
          </div>
        </div>
        <!-- </div> -->


          <div class="col-xl-4 col-lg-12 box-col-12">
            <div class="card gradient-primary o-hidden ">
              <div class="b-r-4 card-body">
                <div class="media static-top-widget">
                  <div class="align-self-center text-center"><i data-feather="database"></i></div>
                  <div class="media-body"><span class="m-0 text-white">Total Izin Terbit</span>
                    <h4 class="mb-0 "><?=$data_perizinan->total_izin_terbit;?></h4><i class="icon-bg" data-feather="database"></i>
                  </div>
                </div>
              </div>
            </div>


            <div class="card gradient-secondary o-hidden">
              <div class="b-r-4 card-body">
                <div class="media static-top-widget">
                  <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                  <div class="media-body"><span class="m-0">Jumlah Layanan Izin</span>
                    <h4 class="mb-0 "><?=$data_perizinan->total_layanan_izin;?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
                  </div>
                </div>
              </div>
            </div>


            <div class="card gradient-warning o-hidden">
              <div class="b-r-4 card-body">
                <div class="media static-top-widget">
                  <div class="align-self-center text-center">
                    <div class="text-white i" data-feather="message-circle"></div>
                  </div>
                  <div class="media-body"><span class="m-0 text-white">Jumlah Layanan Bidang</span>
                    <h4 class="mb-0  text-white"><?=$data_perizinan->total_bidang_izin;?></h4><i class="icon-bg" data-feather="message-circle"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-8 col-lg-12 box-col-12 ">
            <div class="card">
              <div class="card-header">
                <h5>Grafik Perizinan </h5>
              </div>
              <div class="card-body p-0">
                <div id="grafik-izin-perbulan"></div>
              </div>
            </div>
          </div>

    

        <!-- status widget Ends-->
        <div class="col-xl-12 col-xl-12 xl-100">
          <div class="card">
            <div class="card-header">
              <h5>Jumlah Izin Per kecamatan</h5>
            </div>
            <div class="card-body p-0">
              <div id="grafik_izin_kecamatan"></div>
            </div>
          </div>
        </div>





        <div class="col-xl-4">
          <div class="card gradient-secondary o-hidden monthly-overview">
            <div class="card-header no-border bg-transparent pb-0">
              <h5>Indeks Kepuasan Masyarakat</h5>
              <h6 class="mb-2">DPMPTSP Kab. Bogor</h6>
              <!--<span class="pull-right right-badge">
              <span class="badge badge-pill">2020</span></span>-->
            </div>
            <div class="card-body pt-0">
              <ul class="rounds-decore">
                <li></li>
                <li></li>
                <li></li>
                <li>                                       </li>
              </ul><span class="overview-dots full-lg-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
              <div class="p-watch p-t-50" style="height: 330px">
                <h1 class="display-1 f-w-900 text-center"><?=$data_perizinan->ikm->mutu;?></h1>
                
                <h5 class="text-center" style="margin-top: -20px"><?=$data_perizinan->ikm->nama_mutu;?></h5>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 ">
          <div class="card">
            <div class="card-header">
              <h5>Performance Pelayanan</h5>
            </div>
            <div class="card-body p-0">
              <div id="grafik_performance_pelayanan"></div>
            </div>
          </div>
        </div>

        <div class="col-xl-4 col-sm-12 ">
          <div class="card">
            <div class="card-header">
              <h5>Lama Proses Izin </h5>
            </div>
            <div class="card-body p-0">
              <div id="lama_proses"></div>
            </div>
          </div>
        </div>


        <div class="col-xl-6 col-sm-12  ">
          <div class="card">
            <div class="card-header">
              <h6>Izin Perbidang<span class="badge badge-pill pill-badge-primary f-14 f-w-600 pull-right"><?=$tahun;?></span></h6>
            </div>
            <div class="card-body apex-chart p-0">
              <div id="izin_perbidang"></div>
            </div>
          </div>
        </div>
        <div class=" col-xl-6 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h6>Izin Persektor <span class="badge badge-pill pill-badge-secondary f-14 f-w-600 pull-right"><?=$tahun;?></span></h6>
            </div>
            <div class="card-body apex-chart p-0">
              <div id="izin_persektor"></div>
            </div>
          </div>
        </div>
        <div class=" col-xl-6 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h6>Performance Layanan <span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=$tahun;?></span></h6>
            </div>
            <div class="card-body apex-chart p-0">
              <div id="izin_perlayanan"></div>
            </div>
          </div>
        </div>

        <!--
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header">
              <h5>Tabel Realisasi Anggaran dan Pendapatan Belanja</h5>
            </div>
            <div class="card-body">
              <div class="dt-ext table-responsive">
                <table class="display" id="export-button">
                  <thead>
                    <tr>
                      <th>Kode Rekening</th>
                      <th>Uraian</th>
                      <th>Jumlah Anggaran</th>
                      <th>Realisasi</th>
                      <th>%</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>4.</td>
                      <td>PENDAPATAN DAERAH</td>
                      <td>2786788212742,17</td>
                      <td>637822024653,55</td>
                      <td>22,89</td>
                    </tr>
                    <tr>
                      <td>4.1.</td>
                      <td>PENDAPATAN ASLI DAERAH</td>
                      <td>474906593495,07</td>
                      <td>114902837935,55</td>
                      <td>24,19</td>
                    </tr>
                    <tr>
                      <td>4.1.1.</td>
                      <td>HASIL PAJAK DAERAH</td>
                      <td>194759747254</td>
                      <td>39256622266</td>
                      <td>20,15</td>
                    </tr>
                    <tr>
                      <td>4.1.1.01.</td>
                      <td>Pajak Hotel</td>
                      <td>3815326128</td>
                      <td>592336936</td>
                      <td>15,52</td>
                    </tr>
                    <tr>
                      <td>4.1.1.01.004.</td>
                      <td>Hotel Bintang Tiga</td>
                      <td>2014200116</td>
                      <td>274445938</td>
                      <td>13,63</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Kode Rekening</th>
                      <th>Uraian</th>
                      <th>Jumlah Anggaran</th>
                      <th>Realisasi</th>
                      <th>%</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>



         batas -->
      </div>
    </div>
  </div>


  <script src="<?=base_url();?>js/perizinan/dashboard.js" defer></script>