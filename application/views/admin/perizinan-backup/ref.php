 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Ref. Keuangan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Keuangan</li>
            <li class="breadcrumb-item active">Referensi  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->


      <!-- Zero Configuration  Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Ref. Keuangan</h5>
            <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-original-title="test" data-target="#exampleModal">Tambah Referensi</button>
          </div>
          <div class="card-body">
            <iframe src="https://e-office.sumedangkab.go.id/monitoring_pegawai/?frame=true" height="700px" width="100%" frameborder="no"></iframe>
            <div class="table-responsive">
              <table class="display" id="basic-1">
                <thead>
                  <tr>
                    <th>Kode Rekening</th>
                    <th>Uraian</th>
                    <th>Ket.</th>
                    <th>Opsi</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>4.</td>
                    <td>PENDAPATAN DAERAH</td>
                    <td>-</td>
                    <td><a href="#" class="btn btn-outline-warning btn-sm"> <span class="icon-pencil"></span> Edit</a><a href="#" class="btn btn-outline-danger btn-sm m-l-10"> <span class="icon-trash"></span> Hapus</a></td>

                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Zero Configuration  Ends-->

      <!-- batas -->
    </div>
  </div>

</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Referensi</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">

        <form class="form-horizontal">
          <fieldset>

           
            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="kode_rekening">Kode Rekening</label>  
              <div class="col-lg-12">
                <input id="kode_rekening" name="kode_rekening" type="text" placeholder="masukan kode rekeing" class="form-control ">

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="Uraian">Uraian</label>  
              <div class="col-lg-12">
                <input id="Uraian" name="Uraian" type="text" placeholder="Uraian" class="form-control btn-square input-md">

              </div>
            </div>

            <!-- Textarea -->
            <div class="form-group row mb-0">
              <label class="col-lg-12 control-label text-lg-left" for="Keterangan">Keterangan</label>
              <div class="col-lg-12">                     
                <textarea class="form-control btn-square" id="Keterangan" name="Keterangan"></textarea>
              </div>
            </div>

          </fieldset>
        </form>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" type="button" data-dismiss="modal">Batal</button>
        <button class="btn btn-secondary" type="button">Simpan</button>
      </div>
    </div>
  </div>
</div>


<script src="<?=base_url();?>js/keuangan/dashboard.js" defer></script>