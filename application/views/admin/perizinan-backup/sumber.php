 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Sumber Data Keuangan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Keuangan</li>
            <li class="breadcrumb-item active">Referensi  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->


      <!-- Zero Configuration  Starts-->
      <div class="col-sm-12">
       <div class="card">
        <div class="card-header">
          <h5 class="pull-left">Sumber Data</h5>
        </div>
        <div class="card-body">
          <div class="tabbed-card">
            <ul class="nav nav-pills nav-primary" id="pills-clrtab1" role="tablist">

              <li class="nav-item"><a class="nav-link active" id="top-perencanaan-tab" data-toggle="tab" href="#top-perencanaan" role="tab" aria-controls="top-perencanaan" aria-selected="false">Perencanaan</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link" id="top-realisasi-tab" data-toggle="tab" href="#top-realisasi" role="tab" aria-controls="top-realisasi" aria-selected="false">Realisasi</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link " id="api-top-tab" data-toggle="tab" href="#top-api" role="tab" aria-controls="top-profile" aria-selected="true">By API</a>
                <div class="material-border"></div>
              </li>

            </ul>
            <div class="tab-content" id="top-tabContent">
              <!--perencanaan -->
              <div class="tab-pane active show" id="top-perencanaan" role="tabpanel" aria-labelledby="top-perencanaan-tab">
                <a href="#" class="btn btn-primary"><span class="fa fa-plus"></span> Input Data</a>

                <a href="#" class="btn btn-success "> <span class="fa fa-download"></span> Import Excel</a>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
                      <div class="card-header">
                        <h5>Perencanaan</h5>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="display" id="basic-1">
                            <thead>
                              <tr>
                                <th>Kode Rekening</th>
                                <th>Uraian</th>
                                <th>APBD Murni</th>
                                <th>Target Murni</th>
                                <th>APBD Perubahan</th>
                                <th>Target Perubahan</th>
                                <th>Tahun</th>
                                <th>Opsi</th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>4.1</td>
                                <td>Pendapatan Daerah</td>
                                <td>2.020.250.014</td>
                                <td>42.250.000</td>
                                <td>56.045.201.100</td>
                                <td>86.521.102</td>
                                <td>2020</td>
                                <td><a href="#" class="btn btn-outline-warning btn-sm"> <span class="icon-pencil"></span> Edit</a><a href="#" class="btn btn-outline-danger btn-sm m-l-10"> <span class="icon-trash"></span> Hapus</a>
                                </td>

                              </tr>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

              </div>

              <!--end perencanaan-->

              <!-- realisasi -->
              <div class="tab-pane fade  " id="top-realisasi" role="tabpanel" aria-labelledby="realisasi-top-tab">
                 <a href="#" class="btn btn-primary"><span class="fa fa-plus"></span> Input Data</a>

                <a href="#" class="btn btn-success "> <span class="fa fa-download"></span> Import Excel</a>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
                      <div class="card-header">
                        <h5>Realisasi</h5>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="display" id="basic-2">
                            <thead>
                              <tr>
                                <th>Kode Rekening</th>
                                <th>Uraian</th>
                                <th>Anggaran Realisasi Murni</th>
                                <th>Anggaran Realisasi Perubahan</th>
                                <th>Tgl. Realisasi</th>
                                <th>Opsi</th>

                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>4.1</td>
                                <td>Pendapatan Daerah</td>
                                <td>2.020.250.014</td>
                                <td>-</td>
                                <td>20/02/2020</td>

                                <td><a href="#" class="btn btn-outline-warning btn-sm"> <span class="icon-pencil"></span> Edit</a><a href="#" class="btn btn-outline-danger btn-sm m-l-10"> <span class="icon-trash"></span> Hapus</a>
                                </td>

                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- end realisasi -->
               <!-- Api -->
              <div class="tab-pane fade" id="top-api" role="tabpanel" aria-labelledby="api-top-tab">
                  <div class="row">
                  <div class="col-sm-12">

                    <div class="alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                      <p>Layanan API belum tersedia</p>
                      
                    </div>

                  </div>
                </div>



              </div>
               <!-- end api -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Zero Configuration  Ends-->

    <!-- batas -->
  </div>
</div>

</div>




<script src="<?=base_url();?>js/keuangan/dashboard.js" defer></script>