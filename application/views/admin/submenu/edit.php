<div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-lg-6 main-header">
					<h2>Menu<span>Edit</span></h2>
					<h6 class="mb-0">Bogor Comand Center</h6>
				</div>
				<div class="col-lg-6 breadcrumb-right">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
						<li class="breadcrumb-item">Apps </li>
						<li class="breadcrumb-item">Menu</li>
						<li class="breadcrumb-item active"> Menu</li>
					</ol>
				</div>
			</div>
		</div>
	</div> 
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<form class="form-horizontal form-material" method='post' enctype="multipart/form-data">

								<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
								<?php if (!empty($message)) echo "
                        <div class='alert alert-$message_type'>$message</div>";?>
								<h3 class="text-primary">Data :</h3>

								<input type="hidden" name="id_submenu" value="<?=$submenu->id_submenu?>">
								<div class="form-group">
									<label class="col-md-12">Menu</label>
									<div class="col-md-12">
										<select class="form-control" name="id_menu" id="id_menu" >
											<option value="" selected disabled>Pilih Menu</option>
											<?php 
											foreach ($menu as $menus):
												$sel = ($submenu->id_menu==$menus->id_menu) ? 'selected' : '' ;
												echo "<option value='$menus->id_menu' $sel>$menus->nama_menu</option>";
											endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Nama Sub Menu</label>
									<div class="col-md-12">
										<input type="text" name="nama_submenu" class="form-control form-control-line" value="<?=$submenu->nama_submenu?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Link</label>
									<div class="col-md-12">
										<input type="text" name="link" class="form-control form-control-line" value="<?=$submenu->link?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Icon</label>
									<div class="col-md-12">
										<input type="text" name="icon"  class="form-control form-control-line" value="<?=$submenu->icon?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Urutan</label>
									<div class="col-md-12">
										<input type="number" name="urutan"  min="0" class="form-control form-control-line" value="<?=$submenu->urutan?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Is Active</label>
									<div class="col-md-12">
										<select class="form-control" name="is_active" id="is_active">
											<?php 
											$sel1 = ($submenu->is_active=='Y') ? 'selected' : '' ; 
											$sel2 = ($submenu->is_active=='N') ? 'selected' : '' ; ?>
											<option value=""></option>
											<option value="Y" <?=$sel1?>>Y</option>
											<option value="N" <?=$sel2?>>N</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Header</label>
									<div class="col-md-12">
										<select class="form-control" name="header" id="header">
											<?php 
											$sel1 = ($submenu->header=='Y') ? 'selected' : '' ; 
											$sel2 = ($submenu->header=='N') ? 'selected' : '' ; ?>
											<option value=""></option>
											<option value="Y" <?=$sel1?>>Y</option>
											<option value="N" <?=$sel2?>>N</option>
										</select>
									</div>
								</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12">
								<a href="<?=base_url();?>submenu" class="btn btn-light m-l-10"><span class="icon-angle-left"></span> Batal</a>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12"><button type="submit" class="btn btn-primary m-l-10" style="float: right;"><span class="icon-save"></span> Simpan</button>

							</div>
						</div>
						</form>
					</div>
				</div>