    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Ref. Kategori Produk</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Kategori Produk</a>
                                    </li>

                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">

                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <table class="table data-list-view">
                            <thead>
                                <tr>

                                    <th>Nama Kategori Produk</th>
                                    <th>Sub Kategori</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($list as $key=> $row) {
                                ?>
                                <tr>

                                  <td class="kode_akun"><span id="nama_kategori_produk_<?=$row->id_kategori_produk;?>"><?=$row->nama_kategori_produk;?></span></td>
                                  <td class="product-category"><span data-id="<?=$row->id_induk;?>" id="id_induk_<?=$row->id_kategori_produk;?>"><?=$row->nama_kategori_induk;?></span></td>

                                  <td>
                                      <div class="chip <?=($row->status=="active") ?  'chip-success':'chip-warning';?> ">
                                          <div class="chip-body">
                                              <div class="chip-text" id="status_<?=$row->id_kategori_produk;?>"><?=$row->status;?></div>
                                          </div>
                                      </div>
                                  </td>
                                <td class="product-action">
                                  <span class="" onclick="edit(<?=$row->id_kategori_produk;?>)"><i class="feather icon-edit"></i></span>
                                  <span class="" onclick="hapus(<?=$row->id_kategori_produk;?>)"><i class="feather icon-trash"></i></span>
                                </td>
                            </tr>

                          <?php }?>

                        </tbody>
                    </table>
                </div>
                <!-- DataTable ends -->

                <!-- add new sidebar starts -->
                <div class="add-new-data-sidebar">
                    <div class="overlay-bg"></div>
                    <div class="add-new-data">
                      <form method="post">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                        <input type="hidden" name="id_kategori_produk" id="id_kategori_produk" value="" />
                        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                            <div>
                                <h4 class="text-uppercase">Kategori Produk</h4>
                            </div>
                            <div class="hide-data-sidebar">
                                <i class="feather icon-x"></i>
                            </div>
                        </div>
                        <div class="data-items pb-3">
                            <div class="data-fields px-2 mt-3">
                                <div class="row">
                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-name">Nama Kategori</label>
                                        <input type="text" class="form-control" id="nama_kategori_produk" name="nama_kategori_produk">
                                    </div>


                                    <div class="col-sm-12 data-field-col" id="row-induk">
                                        <label for="data-status">Dari Sub Kategori</label>
                                        <select class="form-control" id="id_induk" name="id_induk">
                                            <option value="">-Tidak Memiliki- </option>
                                            <?php foreach ($dt_induk as $row) {
                                              echo "<option value='$row->id_kategori_produk'>$row->nama_kategori_produk</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>



                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-category"> Status </label>
                                        <select class="form-control select2" id="status" name="status">
                                            <option value="active">Aktif</option>
                                            <option value="not active">Not Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="add-data-footer d-flex  px-3 mt-2">
                            <div class="add-data-btn">
                                <button class="btn btn-primary">Simpan</button>
                            </div>

                        </div>
                      </form>
                    </div>

                </div>
                <!-- add new sidebar ends -->
            </section>
            <!-- Data list view end -->

        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<script>

  function edit(id)
  {
    //alert(id);
    $("#id_kategori_produk").val(id);
    var nama_kategori_produk = $("#nama_kategori_produk_"+id).html();
    var id_induk = $("#id_induk_"+id).attr("data-id");
    //alert(nama_jenis);
    var status = $("#status_"+id).html();
    $("#nama_kategori_produk").val(nama_kategori_produk);
    $("#status").val(status).trigger("change");

    if(id_induk=="")
    {
      $("#row-induk").hide();
      $("#id_induk").val("").trigger("change");

    }
    else{

      $("#row-induk").show();
      $("#id_induk").val(id_induk).trigger("change");
    }

    console.log(id_induk);
    $(".add-new-data").addClass("show");
    $(".overlay-bg").addClass("show");
  }
  function hapus(id)
  {
    //alert(id);
    swal({
      title: "Hapus Kategori?",
      //icon: "info",
      buttons: true,
      dangerMode: false,
    })
    .then((isConfirm) => {
      if (isConfirm) {

        $.ajax({
            url :"<?php echo base_url("ref_kategoriproduk/delete")?>",
            type:'post',
            data:{
              id:id,
              "<?=$this->security->get_csrf_token_name();?>" : "<?= $this->security->get_csrf_hash();?>",
            },
             success    : function(data){
                console.log(data);

                swal("Kategori berhasil dihapus", {
                  icon: "success",
                });

                setTimeout(function() {
                  window.location.href = "<?=base_url();?>ref_kategoriproduk";
                }, 500);
             },
                 error: function(xhr, status, error) {
                  //swal("Opps","Error","error");
                  console.log(xhr);
                }
        });

      }
    });
  }
  function tambah()
  {
    $("#row-induk").show();
      $("#id_kategori_produk").val("");
      $("#nama_kategori_produk").val("");
      $("#id_induk").val("").trigger("change");
      $("#status").val("active").trigger("change");
  }
</script>
