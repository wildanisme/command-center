 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Kategori Agenda</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">List</a>
                                </li>
                           

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                	<h3 class="box-title"><a href="<?php echo base_url();?>kategori_agenda/add" class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a></h3>
							<p class="text-muted m-b-20"></p>
							<div class="table-responsive">

								<table class="table table-striped datatable" id="data">
									<thead>
										<tr>
											<th>#</th>
											<th>Kategori</th>
											<th>Slug</th>
											<th>Status</th>
											<th width=70px>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$num = 1;
										foreach ($query as $row) {
											echo"
											<tr>
											<td>$num</td>
											<td>$row->nama_kategori_agenda</td>
											<td>$row->kategori_slug</td>
											<td>$row->status</td>
											<td>
											<a href='".base_url()."kategori_agenda/edit/$row->id_kategori_agenda' class='btn-xs' title='Edit' data-toggle=\"tooltip\" data-original-title=\"Edit\">

											<i class=\"fa fa-pencil text-inverse m-r-10\"></i>
											</a>
											<a class='btn-xs' title='Delete'  onclick='delete_(\"$row->id_kategori_agenda\")' data-toggle=\"tooltip\" data-original-title=\"Close\">
											<i class=\"fa fa-close text-danger\"></i>
											</a>

											</td>
											</tr>
											";

											$num++;
										}
										?>
									</tbody>
								</table>
							</div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
	function delete_(id)
	{
		swal({
            title: "Apakah anda yakin?",
            text: "Anda tidak dapat mengembalikan data ini lagi jika sudah terhapus!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
            closeOnConfirm: false
        }, function(){
        	window.location = "<?php echo base_url();?>kategori_agenda/delete/"+id;
            swal("Berhasil!", "Data telah terhapus.", "success");
        });
	}
</script>


<script type="text/javascript">
var responsiveHelper;
var breakpointDefinition = {
    tablet: 1024,
    phone : 480
};
var tableContainer;

	jQuery(document).ready(function($)
	{
		tableContainer = $("#data");

		tableContainer.dataTable({
			"sPaginationType": "bootstrap",
			"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"bStateSave": true,


		    // Responsive Settings
		    bAutoWidth     : false,
		    fnPreDrawCallback: function () {
		        // Initialize the responsive datatables helper once.
		        if (!responsiveHelper) {
		            responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
		        }
		    },
		    fnRowCallback  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
		        responsiveHelper.createExpandIcon(nRow);
		    },
		    fnDrawCallback : function (oSettings) {
		        responsiveHelper.respond();
		    }
		});

		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});

</script>
