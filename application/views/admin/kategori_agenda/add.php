 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Kategori Agenda</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url();?>kategori_agenda">Kategori Agenda</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Add</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                		<?php if (!empty($message)) echo "
				<div class='alert alert-$message_type'>$message</div>";?>
				<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
														
					<div class="form-group">
						<label class="control">Kategori </label>
							<input type="text" class="form-control" id='nama_kategori' onkeyup="getCat()"  name='nama_kategori_agenda' placeholder="">
							<p ><?php echo base_url();?>kategori_agenda/<label id='slug'></label></p>
							<input type="hidden" class="form-control" id='kategori_slug' name='kategori_slug' placeholder="">
					</div>			
					
					<div class="form-group">
						<label class="control">Status</label>
							<select name="status" class="form-control select2" data-first-option="false">
								<option>Pilih</option>
								<option value="Active" selected>Aktif</option>
								<option value="Not Active">Nonaktif</option>
							</select>
					</div>
					
					<div class="form-group">
						<div class="col-12">
							<button type="submit" class="btn btn-primary waves-effect waves-light pull-right mb-1" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Simpan</button>
						</div>
						
					</div>
				</form>

                </div>
            </div>
        </div>
    </div>
</div>

</div>

<script type="text/javascript">
	
function getCat()

{
	var permalink = $("#nama_kategori").val();
    // Trim empty space
    //permalink = $.trim($(this).val());
    // replace more then 1 space with only one
    permalink = permalink.replace(/\s+/g,' ');
    $('#kategori_slug').val(permalink.toLowerCase());
    $('#kategori_slug').val($('#kategori_slug').val().replace(/\W/g, ' '));
    $('#kategori_slug').val($.trim($('#kategori_slug').val()));
    $('#kategori_slug').val($('#kategori_slug').val().replace(/\s+/g, '-'));
    var gappermalink = $('#kategori_slug').val();
    $('#slug').html(gappermalink);
  //console.log(permalink);
}

</script>
