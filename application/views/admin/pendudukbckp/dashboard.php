<div class="page-body">        
 <div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-6 main-header">
        <h2>Dashboard Kependudukan</h2>
        <h6 class="mb-0">Kabupaten Bogor</h6>
      </div>
      <div class="col-lg-6 breadcrumb-right">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
          <li class="breadcrumb-item">Dashboard</li>
          <li class="breadcrumb-item active">Kependudukan  </li>
        </ol>
      </div>
    </div><br>

  <div class="row">
    <div class="col-lg-12 xl-100">
      <div class="row ecommerce-chart-card">
        <div class="col-xl-3 xl-50 col-md-3 box-col-3">
          <div class="card gradient-primary o-hidden">
            <div class="card-body tag-card">
              <div class="ecommerce-chart">
                <div class="media ecommerce-small-chart">
                  <div class="small-bar">
                    <div class="small-chart1 flot-chart-container"></div>
                  </div>
                  <div class="sale-chart">   
                    <div class="media-body m-l-40">
                      <h6 class="f-w-700 m-l-10">Total Penduduk</h6>
                      <h4 class="mb-0 f-w-700 m-l-10"><?= number_format($total_penduduk[0]->total,0,",",".") ?></h4>
                    </div>
                  </div>
                </div>
              </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">       </span></span></span>
            </div>
          </div>
        </div>
        <div class="col-xl-3 xl-50 col-md-3 box-col-3">
          <div class="card gradient-secondary o-hidden">
            <div class="card-body tag-card">
              <div class="ecommerce-chart">
                <div class="media ecommerce-small-chart">
                  <div class="small-bar">
                    <div class="small-chart2 flot-chart-container"></div>
                  </div>
                  <div class="sale-chart">   
                    <div class="media-body m-l-40">
                      <h6 class="f-w-700 m-l-10">Rata-Rata Pertumbuhan</h6>
                      <h4 class="mb-0 f-w-700 m-l-10"><?= $rata_tembuh[0]->total,"%" ?></h4>
                    </div>
                  </div>
                </div>
              </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">             </span></span></span>
            </div>
          </div>
        </div>
        <div class="col-xl-3 xl-50 col-md-3 box-col-3">
          <div class="card gradient-warning o-hidden">
            <div class="card-body tag-card">
              <div class="ecommerce-chart">
                <div class="media ecommerce-small-chart">
                  <div class="small-bar">
                    <div class="small-chart3 flot-chart-container"></div>
                  </div>
                  <div class="sale-chart">   
                    <div class="media-body m-l-40">
                      <h6 class="f-w-700 m-l-10">Total Laki-Laki</h6>
                      <h4 class="mb-0 f-w-700 m-l-10"><?= number_format($total_laki[0]->total,0,",",".") ?></h4>
                    </div>
                  </div>
                </div>
              </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">                    </span></span></span>
            </div>
          </div>
        </div>
        <div class="col-xl-3 xl-50 col-md-3 box-col-3">
          <div class="card gradient-info o-hidden">
            <div class="card-body tag-card">
              <div class="ecommerce-chart">
                <div class="media ecommerce-small-chart">
                  <div class="small-bar">
                    <div class="small-chart4 flot-chart-container"></div>
                  </div>
                  <div class="sale-chart">   
                    <div class="media-body m-l-40">
                      <h6 class="f-w-700 m-l-10">Total Perempuan</h6>
                      <h4 class="mb-0 f-w-700 m-l-10"><?= number_format($total_perempuan[0]->total,0,",",".") ?></h4>
                    </div>
                  </div>
                </div>
              </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">                    </span></span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 col-xl-6 xl-100 lg-100">
      <div class="card">
        <div class="card-header">
          <h5>Jumlah Penduduk Per Kecamatan</h5>
        </div>
        <div class="card-body p-0">
          <div id="column-chart"></div>
        </div>
      </div>
    </div>
  
    

  <div class="row">
    <div class="col-sm-6 col-xl-6 xl-50">
      <div class="card">
        <div class="card-header">
          <h5>Agama Penduduk </h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="piechart"></div>
        </div>
      </div>
    </div>


 <div class="col-sm-6 col-xl-6 xl-50">
  <div class="card">
    <div class="card-header">
      <h5>Pendidikan Penduduk </h5>
    </div>
    <div class="table-responsive">
      <table class="table table-borderedfor">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Pendidikan</th>
            <th scope="col">Jumlah</th>
          </tr>
        </thead>
        <tbody>
          <tr class="table-active">
            <td class="bg-danger">1</td>
            <td class="bg-danger">Tidak/Belum Sekolah Dasar</td>
            <td class="bg-danger">1.282.135</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Belum Tamat SD/Sederajat</td>
            <td>517.523</td>
          </tr>
          <tr>
            <td class="bg-danger">3</td>
            <td class="bg-danger">Tamat SD/Sederajat</td>
            <td class="bg-danger">1.538.347</td>
          </tr>
          <tr>
            <td>4</td>
            <td>SLTP/Sederajat</td>
            <td>808.462</td>
          </tr>
          <tr>
            <td class="bg-danger">5</td>
            <td class="bg-danger">SLTA/Sederajat</td>
            <td class="bg-danger">1.097.563</td>
          </tr>
          <tr>
            <td>6</td>
            <td>Diploma I/II</td>
            <td>14.580</td>
          </tr>
          <tr>
            <td class="bg-danger">7</td>
            <td class="bg-danger">Akademi/Diploma III/S. Muda</td>
            <td class="bg-danger">57.198</td>
          </tr>
          <tr>
            <td>8</td>
            <td>Diploma IV/Strata I</td>
            <td>166.719</td>
          </tr>
          <tr>
            <td class="bg-danger">9</td>
            <td class="bg-danger">Strata II</td>
            <td class="bg-danger">11.723</td>
          </tr>
          <tr>
            <td>10</td>
            <td>Strata III</td>
            <td>1.122</td>
          </tr>
        </tbody>
      </table>
    </div>
    </div>
  </div>


  <div class="col-sm-12 col-xl-12 xl-100">
    <div class="card">
    <div class="card-header">
      <h5>Pekerjaan Penduduk</h5>
    </div>
    <div class="card-body p-0">
      <div id="basic-bar"></div>
    </div>
    </div>
  </div>
</div>
<script>

var dataCowo = "<?= $perKecamatanCowo ?>".split(",");
  var dataCewe = "<?= $perKecamatanCewe ?>".split(",");
  var dataKec = "<?= $data_kec ?>".split(",");

  // column chart
  var options3 = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Laki-Laki',
        data: dataCowo
    }, {
        name: 'Perempuan',
        data: dataCewe
    }],
    xaxis: {
        categories: dataKec,
    },
    yaxis: {
        title: {
            text: 'Orang'
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,') + " Orang"
            }
        }
    },
    colors:['#7e37d8', '#fe80b2', '#80cf00']
  }

  var chart3 = new ApexCharts(
    document.querySelector("#column-chart"),
    options3
  );

chart3.render();


  var dataKtpL = "<?= $data_ktp_l ?>".split(",");
  var dataKtpP = "<?= $data_ktp_p ?>".split(",");
  var dataKiaL = "<?= $data_kia_l ?>".split(",");
  var dataKiaP = "<?= $data_kia_p ?>".split(",");
  var dataKec = "<?= $data_kec ?>".split(",");

  // column chart
  var options4 = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'KTP Laki-Laki',
        data: dataKtpL
    },{
        name: 'KTP Perempuan',
        data: dataKtpP
    },{
        name: 'KIA Laki-Laki',
        data: dataKiaL
    },{
        name: 'KIA Perempuan',
        data: dataKiaP
    }],
    xaxis: {
        categories: dataKec,
    },
    yaxis: {
        title: {
            text: 'Orang'
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, '$&,') + " Orang"
            }
        }
    },
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#000000']
  }

  var chart4 = new ApexCharts(
    document.querySelector("#column-chart-ktp"),
    options4
  );

chart4.render();

  // pie chart
  var options8 = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Belum Kawin', 'Kawin', 'Cerai Hidup', 'Cerai Mati'],
    series: [<?= $status_kawin[0]->bk ?>, <?= $status_kawin[0]->kawin ?>, <?= $status_kawin[0]->ch ?>, <?= $status_kawin[0]->cm ?>],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
  }

  var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
  );
  chart8.render();



  // pie chart
var options10 = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['A', 'B','AB', 'O', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'],
    series: [<?= $perGoldarah[0]->A?>, <?= $perGoldarah[0]->B?>, <?= $perGoldarah[0]->AB?>, <?= $perGoldarah[0]->O?>,
             <?= $perGoldarah[0]->A_plus?>, <?= $perGoldarah[0]->A_minus?>, <?= $perGoldarah[0]->B_plus?>, <?= $perGoldarah[0]->B_minus?>,
             <?= $perGoldarah[0]->AB_plus?>, <?= $perGoldarah[0]->AB_minus?>, <?= $perGoldarah[0]->O_plus?>, <?= $perGoldarah[0]->O_minus?>],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:['#7e37d8', '#fe80b2', '#fc7303', '#dffc03', '#a1fc03', '#03fcce', '#0398fc', '#9403fc', '#fcf803', '#f2f18d', '#c8f28d','#f2d78d','#8df2d9', '#8dc3f2', '#f28d8d', '#f5a4f1']
}

var chart10 = new ApexCharts(
    document.querySelector("#piechart10"),
    options10
);

chart10.render();

// basic bar chart
var options2 = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        data: [<?=$pendidikan[0]->tb_sekolah?>, <?=$pendidikan[0]->bt_sd?>, <?=$pendidikan[0]->tmt_sd?>, <?=$pendidikan[0]->sltp?>, <?=$pendidikan[0]->slta?>, <?=$pendidikan[0]->d1?>, <?=$pendidikan[0]->d3?>, <?=$pendidikan[0]->s1?>, <?=$pendidikan[0]->s2?>, <?=$pendidikan[0]->s3?>]
    }],
    xaxis: {
        categories: ['Tdk/Blm Sekolah', 'Blm Tamat SD', 'Tamat SD', 'SLTP', 'SLTA', 'D1/D2', 'D3', 'D4/S1', 'S2', 'S3'],
    },
    colors:['#0398fc', '#9403fc', '#fcf803', '#f2f18d', '#c8f28d','#f2d78d','#8df2d9', '#8dc3f2', '#f28d8d', '#f5a4f1'],
    tooltip: {
      x: {
        formatter: function (val) {
          return val;
        }
      }
    }
}

var chart2 = new ApexCharts(
    document.querySelector("#basic-bar"),
    options2
);

chart2.render();
</script>

        </div>

        
      </div>