<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="Dashboard Bogor">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
	<title>Admin</title>
	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- Font Awesome-->
	<link rel="stylesheet" type="text/css" href="assets/css/fontawesome.css">
	<!-- ico-font-->
	<link rel="stylesheet" type="text/css" href="assets/css/icofont.css">
	<!-- Themify icon-->
	<link rel="stylesheet" type="text/css" href="assets/css/themify.css">
	<!-- Flag icon-->
	<link rel="stylesheet" type="text/css" href="assets/css/flag-icon.css">
	<!-- Feather icon-->
	<link rel="stylesheet" type="text/css" href="assets/css/feather-icon.css">
	<link rel="stylesheet" type="text/css" href="assets/css/animate.css">
	<!-- Plugins css start-->
	<link rel="stylesheet" type="text/css" href="assets/css/pe7-icon.css">
	<!-- Plugins css Ends-->
	<!-- Bootstrap css-->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<!-- App css-->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link id="color" rel="stylesheet" href="assets/css/color-1.css" media="screen">
	<!-- Responsive css-->
	<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
</head>

<body>
	<!-- Loader starts
	<div class="loader-wrapper">
		<div class="typewriter">
			<h1>New Era Admin Loading"</h1>
		</div>
	</div>-->
	<!-- Loader ends-->
	<!-- page-wrapper Start-->
	<div class="page-wrapper">

		<div class="page-main-header">
			<div class="main-header-right">
				<div class="main-header-left text-center">
					<div class="logo-wrapper"><a href="index.html"><img src="<?= base_url(); ?>asset/dashboard/images/logo/logo.png" alt="" style="width: 150px"></a></div>
				</div>
				<div class="mobile-sidebar">
					<div class="media-body text-right switch-sm">
						<label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
					</div>
				</div>
				<div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar"> </i></div>
				<div class="nav-right col pull-right right-menu">
					<ul class="nav-menus">
						<li>
							<form class="form-inline search-form" action="#" method="get">
								<div class="form-group">
									<div class="Typeahead Typeahead--twitterUsers">
										<div class="u-posRelative">
											<input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Cari Data...">
											<div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
										</div>
										<div class="Typeahead-menu"></div>
									</div>
								</div>

							</form>
						</li>
						<li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
						<li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>asset/dashboard/images/dashboard/bookmark.png" alt="">
							<div class="onhover-show-div bookmark-flip">
								<div class="flip-card">
									<div class="flip-card-inner">
										<div class="front">
											<ul class="droplet-dropdown bookmark-dropdown">
												<li class="gradient-primary text-center">
													<h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
												</li>
												<li>
													<div class="row">
														<div class="col-4 text-center"><i data-feather="file-text"></i></div>
														<div class="col-4 text-center"><i data-feather="activity"></i></div>
														<div class="col-4 text-center"><i data-feather="users"></i></div>
														<div class="col-4 text-center"><i data-feather="clipboard"></i></div>
														<div class="col-4 text-center"><i data-feather="anchor"></i></div>
														<div class="col-4 text-center"><i data-feather="settings"></i></div>
													</div>
												</li>
												<li class="text-center">
													<button class="flip-btn" id="flip-btn">Add New Bookmark</button>
												</li>
											</ul>
										</div>
										<div class="back">
											<ul>
												<li>
													<div class="droplet-dropdown bookmark-dropdown flip-back-content">
														<input type="text" placeholder="search...">
													</div>
												</li>
												<li>
													<button class="d-block flip-back" id="flip-back">Back</button>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="<?= base_url(); ?>asset/dashboard/images/dashboard/like.png" alt="">
							<ul class="onhover-show-div droplet-dropdown">
								<li class="gradient-primary text-center">
									<h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
								</li>
								<li>
									<div class="row">
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
										<div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
									</div>
								</li>
								<li class="text-center">
									<button class="btn btn-primary btn-air-primary">Follows Up</button>
								</li>
							</ul>
						</li>
						<li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>asset/dashboard/images/dashboard/notification.png" alt="">
							<ul class="onhover-show-div notification-dropdown">
								<li class="gradient-primary">
									<h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
								</li>
								<li>
									<div class="media">
										<div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
										<div class="media-body">
											<h6>Someone Likes Your Posts</h6>
											<p class="mb-0"> 2 Hours Ago</p>
										</div>
									</div>
								</li>
								<li class="pt-0">
									<div class="media">
										<div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
										<div class="media-body">
											<h6>3 New Comments</h6>
											<p class="mb-0"> 1 Hours Ago</p>
										</div>
									</div>
								</li>
								<li class="bg-light txt-dark"><a href="#">All </a> notification</li>
							</ul>
						</li>
						<li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="<?= base_url(); ?>asset/dashboard/images/dashboard/chat.png" alt=""></a></li>
						<li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="<?= base_url(); ?>asset/dashboard/images/dashboard/user.png" alt=""></span>
							<ul class="onhover-show-div profile-dropdown">
								<li class="gradient-primary">
									<h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
								</li>
								<li><i data-feather="user"> </i>Profile</li>
								<li><i data-feather="message-square"> </i>Inbox</li>
								<li><i data-feather="file-text"> </i>Taskboard</li>
								<li><i data-feather="settings"> </i>Settings </li>
							</ul>
						</li>
					</ul>
					<div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
				</div>
				<script id="result-template" type="text/x-handlebars-template">
					<div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
				<script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
			</div>
		</div>

		<!-- Page Body Start-->
		<div class="page-body-wrapper">
			
			            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <?php $this->load->view('admin/src/menu'); ?>

            </div>
			
			<div class="page-body">
				<div class="container-fluid">
					<div class="container-fluid">
						<div class="page-header">
							<div class="row">
								<div class="col-lg-6 main-header">
									<h2><span>Dashboard Kepemudaan</span></h2>
									<h6 class="mb-0">Dinas Pariwisata, Kebudayaan, Kepemudaan dan Olahraga</h6>
								</div>
								<div class="col-lg-6 breadcrumb-right">
									<ol class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
										<li class="breadcrumb-item">Kepemudaan</li>
										<li class="breadcrumb-item active">Dashboard </li>
									</ol>
								</div>
							</div>
						</div>
					</div>

					<div class="row ecommerce-chart-card">
						<div class="col-xl-3 xl-50 col-md-6">
							<div class="card gradient-primary o-hidden">
								<div class="b-r-4 card-body">
									<div class="media static-top-widget">
										<div class="align-self-center text-center"><i data-feather="database"></i></div>
										<div class="media-body"><span class="m-0 text-white">JUMLAH ORGANISASI KEPEMUDAAN</span>
											<h4 class="mb-0 ">96</h4><i class="icon-bg" data-feather="database"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 xl-50 col-md-6 box-col-6">
							<div class="card gradient-secondary o-hidden">
								<div class="b-r-4 card-body">
									<div class="media static-top-widget">
										<div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
										<div class="media-body"><span class="m-0">JUMLAH PENGUSAHA MUDA DI KABUPATEN SUMEDANG </span>
											<h4 class="mb-0 ">27.439</h4><i class="icon-bg" data-feather="shopping-bag"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 xl-50 col-md-6 box-col-6">
							<div class="card gradient-warning o-hidden">
								<div class="b-r-4 card-body">
									<div class="media static-top-widget">
										<div class="align-self-center text-center">
											<div class="text-white i" data-feather="message-circle"></div>
										</div>
										<div class="media-body"><span class="m-0 text-white">JUMLAH PEMUDA USIA 16-30 DI KABUPATEN SUMEDANG</span>
											<h4 class="mb-0  text-white">266.994</h4><i class="icon-bg" data-feather="message-circle"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 xl-50 col-md-6 box-col-6">
							<div class="card gradient-info o-hidden">
								<div class="b-r-4 card-body">
									<div class="media static-top-widget">
										<div class="align-self-center text-center">
											<div class="text-white i" data-feather="user-plus"></div>
										</div>
										<div class="media-body"><span class="m-0 text-white">JUMLAH PEMUDA YANG AKTIF DI ORGANISASI KEPEMUDAAN</span>
											<h4 class="mb-0  text-white">4784</h4><i class="icon-bg" data-feather="user-plus"></i>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header">
								<div class="card-header">
									<h5>Tabel Organisasi Pemuda</h5>
								</div>
								<div class="table-responsive">
									<table class="table">
										<thead class="thead-dark">
											<thead>
												<tr>
													<th scope="col">NO</th>
													<th scope="col">Nama OKP</th>
													<th scope="col">Jenis Aktifitas</th>

												</tr>
											</thead>
										<tbody>
											<tr>
												<th scope="row">1</th>
												<td>ANGKATAN MUDA KABAH KAB.SUMEDANG</td>
												<td>POLITIK</td>

											</tr>
											<tr>
												<th scope="row">2</th>
												<td>BANTENG MUDA INDONESIA KAB.SUMEDANG</td>
												<td>POLITIK</td>

											</tr>
											<tr>
												<th scope="row">3</th>
												<td>BKPRMI KAB.SUMEDANG</td>
												<td>SOSIAL</td>

											</tr>
											<tr>
												<th scope="row">4</th>
												<td>FATAYAT NU KAB.SUMEDANG</td>
												<td>SOSIAL/KEAGAMAAN</td>

											</tr>
											<tr>
												<th scope="row">5</th>
												<td>GARDA BANGSA KAB.SUMEDANG</td>
												<td>POLITIK</td>

											</tr>
											<tr>
												<th scope="row">6</th>
												<td>GEMA KEADILAN</td>
												<td>POLITIK</td>

											</tr>
											<tr>
												<th scope="row">7</th>
												<td>GEMA BBC KAB.SUMEDANG</td>
												<td>SOSIAL</td>

											</tr>
											<tr>
												<th scope="row">8</th>
												<td>GENERASI MUDA AMS KAB.SUMEDANG</td>
												<td>SOSIAL</td>

											</tr>
											<tr>
												<th scope="row">9</th>
												<td>GENERASI MUDA BBC</td>
												<td>SOSIAL</td>

											</tr>
											<tr>
												<th scope="row">10</th>
												<td>GERAKAN PEMUDA SEHAT</td>
												<td>SOSIAL/OLAHRAGA</td>

											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</div>
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header">
									<h5>Tabel Wirausaha Muda</h5>
								</div>
								<div class="card-block row">
									<div class="col-sm-12 col-lg-12 col-xl-12">
										<div class="table-responsive">
											<table class="table">
												<thead class="thead-dark">
													<tr>
														<th scope="col">NO</th>
														<th scope="col">Desa</th>
														<th scope="col">Wirausaha Muda</th>

													</tr>
												</thead>
												<tbody>
													<tr>
														<th scope="row">1</th>
														<td>WADO</td>
														<td>722</td>

													</tr>
													<tr>
														<th scope="row">2</th>
														<td>JATINUNGGAL</td>
														<td>945</td>

													</tr>
													<tr>
														<th scope="row">3</th>
														<td>DARMARAJA</td>
														<td>880</td>

													</tr>
													<tr>
														<th scope="row">4</th>
														<td>CIBUGEL</td>
														<td>750</td>

													</tr>
													<tr>
														<th scope="row">5</th>
														<td>CISITU</td>
														<td>790</td>

													</tr>
													<tr>
														<th scope="row">6</th>
														<td>SITURAJA</td>
														<td>1.260</td>

													</tr>
													<tr>
														<th scope="row">7</th>
														<td>CONGGEANG</td>
														<td>775</td>

													</tr>
													<tr>
														<th scope="row">8</th>
														<td>PASEH</td>
														<td>1.443</td>

													</tr>
													<tr>
														<th scope="row">9</th>
														<td>SURIAN</td>
														<td>270</td>

													</tr>
													<tr>
														<th scope="row">10</th>
														<td>BUAHDUA</td>
														<td>827</td>

													</tr>

												</tbody>
											</table>
										</div>

									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
										<h5>Tabel Jumlah Pemuda 16-30 Tahun </h5>
									</div>
									<div class="card-block row">
										<div class="col-sm-12 col-lg-12 col-xl-12">
											<div class="table-responsive">
												<table class="table">
													<thead class="thead-dark">
														<tr>
															<th scope="col">NO</th>
															<th scope="col">Kecamatan</th>
															<th scope="col">Jumlah Penduduk 16-30</th>

														</tr>
													</thead>
													<tbody>
														<tr>
															<th scope="row">1</th>
															<td>WADO</td>
															<td>10.621</td>

														</tr>
														<tr>
															<th scope="row">2</th>
															<td>JATINUNGGAL</td>
															<td>10.415</td>

														</tr>
														<tr>
															<th scope="row">3</th>
															<td>DARMARAJA</td>
															<td>8.685</td>

														</tr>
														<tr>
															<th scope="row">4</th>
															<td>CISITU</td>
															<td>5.865</td>

														</tr>
														<tr>
															<th scope="row">5</th>
															<td>CISITU</td>
															<td>6.663</td>

														</tr>
														<tr>
															<th scope="row">6</th>
															<td>SITURAJA</td>
															<td>9.454</td>

														</tr>
														<tr>
															<th scope="row">7</th>
															<td>CONGGEANG</td>
															<td>5.779</td>

														</tr>
														<tr>
															<th scope="row">8</th>
															<td>PASEH</td>
															<td>8.750</td>

														</tr>
														<tr>
															<th scope="row">9</th>
															<td>SURIAN</td>
															<td>2.458</td>

														</tr>
														<tr>
															<th scope="row">10</th>
															<td>BUAHDUA</td>
															<td>6.488</td>

														</tr>
													</tbody>
												</table>
											</div>

										</div>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="card">
										<div class="card-header">
											<div class="card-header">
												<h5>Tabel Jumlah Pemuda Yang Aktif Di Organisasi Kepemudaan</h5>
											</div>
											<div class="table-responsive">
												<table class="table">
													<thead class="thead-dark">
														<thead>
															<tr>
																<th scope="col">NO</th>
																<th scope="col">Nama OKP</th>
																<th scope="col">Jenis Aktifitas</th>
																<th scope="col">Jumlah Anggota</th>
															</tr>
														</thead>
													<tbody>
														<tr>
															<th scope="row">1</th>
															<td>ANGKATAN MUDA KABAH KAB.SUMEDANG</td>
															<td>POLITIK</td>
															<td>140</td>
														</tr>
														<tr>
															<th scope="row">2</th>
															<td>BANTENG MUDA INDONESIA KAB.SUMEDANG</td>
															<td>POLITIK</td>
															<td>53</td>
														</tr>
														<tr>
															<th scope="row">3</th>
															<td>BKPRMI KAB.SUMEDANG</td>
															<td>SOSIAL</td>
															<td>214</td>
														</tr>
														<tr>
															<th scope="row">4</th>
															<td>FATAYAT NU KAB.SUMEDANG</td>
															<td>SOSIAL/KEAGAMAAN</td>
															<td>344</td>
														</tr>
														<tr>
															<th scope="row">5</th>
															<td>GARDA BANGSA KAB.SUMEDANG</td>
															<td>POLITIK</td>
															<td>100</td>
														</tr>
														<tr>
															<th scope="row">6</th>
															<td>GEMA KEADILAN</td>
															<td>POLITIK</td>
															<td>186</td>
														</tr>
														<tr>
															<th scope="row">7</th>
															<td>GEMA BBC KAB.SUMEDANG</td>
															<td>SOSIAL</td>
															<td>80</td>
														</tr>
														<tr>
															<th scope="row">8</th>
															<td>GENERASI MUDA AMS KAB.SUMEDANG</td>
															<td>SOSIAL</td>
															<td>100</td>
														</tr>
														<tr>
															<th scope="row">9</th>
															<td>GENERASI MUDA BBC</td>
															<td>SOSIAL</td>
															<td>50</td>
														</tr>
														<tr>
															<th scope="row">10</th>
															<td>GERAKAN PEMUDA SEHAT</td>
															<td>SOSIAL/OLAHRAGA</td>
															<td>50</td>
														</tr>
													</tbody>
												</table>
											</div>

										</div>
									</div>

									</tr>
									</tbody>
									</table>
								</div>
							</div>
						</div>

						<!-- footer start-->
						<footer class="footer">
							<div class="container-fluid">
								<div class="row">
									<div class="col-md-6 footer-copyright">
										<p class="mb-0">Copyright © 2021 Desa Kab. Bogor. All rights reserved.</p>
									</div>
									<div class="col-md-6">
										<p class="pull-right mb-0">Hand-crafted & made with Diskominfosanditik <i class="fa fa-heart"></i></p>
									</div>
								</div>
							</div>
						</footer>
					</div>
				</div>
			</div>
		</div>
		<!-- latest jquery-->
		<script src="assets/js/jquery-3.5.1.min.js"></script>
		<!-- Bootstrap js-->
		<script src="assets/js/bootstrap/popper.min.js"></script>
		<script src="assets/js/bootstrap/bootstrap.js"></script>
		<!-- feather icon js-->
		<script src="assets/js/icons/feather-icon/feather.min.js"></script>
		<script src="assets/js/icons/feather-icon/feather-icon.js"></script>
		<!-- Sidebar jquery-->
		<script src="assets/js/sidebar-menu.js"></script>
		<script src="assets/js/config.js"></script>
		<!-- Plugins JS start-->
		<script src="assets/js/chat-menu.js"></script>
		<script src="assets/js/height-equal.js"></script>

		<script src="assets/js/touchspin/vendors.min.js"></script>
		<script src="assets/js/touchspin/touchspin.js"></script>
		<script src="assets/js/touchspin/input-groups.min.js"></script>
		<!-- Plugins JS Ends-->
		<!-- Theme js-->
		<script src="assets/js/script.js"></script>
		<script src="assets/js/theme-customizer/customizer.js"></script>
		<!-- login js-->
		<!-- Plugin used-->
</body>

</html>