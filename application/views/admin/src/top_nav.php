  <div class="page-main-header" style="height: 70px !important;">
        <div class="main-header-right">
          <div class="main-header-left text-center">
            <div class="logo-wrapper"><a href=""><img src="<?=base_url();?>asset/dashboard/images/logo/logo.png" alt="" style="width: 150px"></a></div>
          </div>
          <div class="mobile-sidebar">
            <div class="media-body text-right switch-sm">
              <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
            </div>
          </div>
          <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar">               </i></div>
          <div class="nav-right col pull-right right-menu">
            <ul class="nav-menus">
              <li>
                <form class="form-inline search-form" action="#" method="get">
                  <div class="form-group">
                    <div class="Typeahead Typeahead--twitterUsers">
                      <div class="u-posRelative">
                        <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Cari Data...">
                        <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                      </div>
                      <div class="Typeahead-menu"></div>
                    </div>
                  </div>

                </form>
              </li>
              <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>

             
              
              </li>
           

             
              <li class="onhover-dropdown" id="cika-top" onmouseout="document.querySelector('#cika-open').setAttribute('onmouseover', 'hello_cika()');this.classList.remove('glow-shadow');"> <span id="cika-open" class="media user-header" onmouseover="hello_cika();"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/user.png" alt=""></span>
                <ul class="onhover-show-div profile-dropdown" id="cika-chat" style="width: calc(200% + 180px); left: calc(-200% - 65px) !important;">
                  <li id="cika-text" class="gradient-primary" style="border-radius: 15px;">
                    
                  </li>
                </ul>
              </li>
	<li><a href="admin/login" onclick="return confirm('Anda yakin ingin keluar?');"><i class="fa fa-power-off"></i> Keluar</a></li>


            </ul>
            
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
          </div>
          <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
          <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
        </div>
      </div>