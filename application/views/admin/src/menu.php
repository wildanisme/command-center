<!-- BEGIN: Main Menu-->
<?php
if (empty($active_menu)) {
  $active_menu = "dashboard";
}


$user_level = $this->session->userdata('user_level');

// var_dump($user_level);
$main_menu = $this->db->select('b.nama_menu,b.icon,b.link,b.id_menu');
$main_menu =$this->db->join('tbl_menu b', 'a.id_menu=b.id_menu');
$main_menu =$this->db->join('user_level c', 'a.id_level=c.level_id' );
$main_menu =$this->db->where('a.id_level',$user_level);
$main_menu =$this->db->where('a.view','Y');
$main_menu = $this->db->where('b.is_active', 'Y');
$main_menu =$this->db->order_by('urutan ASC');  
$main_menu =$this->db->get('tbl_akses_menu a');
?>
<div class="sidebar">
  <ul class="iconMenu-bar custom-scrollbar">
    <?php foreach ($main_menu->result() as $main): ?>

      <li><a class="bar-icons" href="javascript:void(0)"><i class="<?php echo $main->icon ?>"></i><span><?php echo $main->nama_menu ?> </span></a>
        <ul class="iconbar-mainmenu custom-scrollbar">
          <?php 
          $sub_menu = $this->db->join('tbl_submenu b','a.id_submenu=b.id_submenu');
          $sub_menu = $this->db->where('a.id_level', $user_level);
          $sub_menu = $this->db->where('b.id_menu', $main->id_menu);
          $sub_menu = $this->db->where('a.view', 'Y');
          $sub_menu = $this->db->where('b.is_active', 'Y');
          $sub_menu = $this->db->order_by('b.urutan', 'ASC');
          $sub_menu = $this->db->get('tbl_akses_submenu a'); 

          ?>
          <?php foreach ($sub_menu->result() as $sub): ?>
            <?php if ($sub->header=='Y'): ?>
              <li class="iconbar-header"><?php echo $sub->nama_submenu ?></li>
            <?php else : ?>
          
          <li><a href="<?= base_url($sub->link); ?>"><?php echo $sub->nama_submenu ?></a></li>
          <?php endif ?>
          <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach ?>
  </ul>
</div>
<!-- END: Main Menu-->