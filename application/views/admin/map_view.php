
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Start Page Title Area
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!-- <div class="col-12 full"> -->
        <!-- <div class="container"> -->
            <div class="row" id="map-filter" style="position: absolute; z-index: 3; margin: 10vh 0; padding: 0 20vw; width: 100vw">
                <div class="col-12">
                    <div class="page-header-content">
                        <form method="get" class="hero-search-form style-two">
                            <div class="inner-form">
                                <div class="hero-form-input search">
                                    <input id="search" name="q" type="text" placeholder="Apa yang Anda cari?" value="<?=@$_GET['q']?>" />
                                </div><!--~./ search ~-->
                                <!-- <select class="hero-form-input custom-select location">
                                    <option>Location </option>
                                    <option>New York</option>
                                    <option>California</option>
                                    <option>Washington</option>
                                    <option>New Jersey</option>
                                    <option>Los Angeles</option>
                                    <option>Florida</option>
                                </select> -->
                                <select name="c" class="hero-form-input custom-select category">
                                    <option value="">Kategori</option>
                                    <option value="skpd" <?=(@$_GET['c'] == "skpd") ? "selected" : "" ?>>SKPD</option>
                                    <option value="kecamatan" <?=(@$_GET['c'] == "kecamatan") ? "selected" : "" ?>>Kecamatan</option>
                                    <option value="desa" <?=(@$_GET['c'] == "desa") ? "selected" : "" ?>>Desa</option>
                                    <!-- <option>Rutilahu</option>
                                    <option>Wisata</option>
                                    <option>Rumah Sakit</option>
                                    <option>Puskesmas</option>
                                    <option>UMKM</option>
                                    <option>Fasilitas Umum</option> -->
                                </select><!--~./ location ~-->
                                <div class="hero-form-input submitbtn">
                                    <button class="btn btn-default" type="submit">JELAJAHI</button>
                                </div><!--~./ location ~-->
                            </div>
                        </form><!--./ hero-search-form -->
                    </div><!--~~./ page-header-content ~~-->
                </div>
            </div>
        <!-- </div>~~./ end container ~~ -->
    
    
    <!--********************************************************-->
    <!--********************* SITE CONTENT *********************-->
    <!--********************************************************-->
    <div class="site-content full" onmouseup="show_particle();" ontouchend="show_particle();" onmousedown="hide_particle();" ontouchstart="hide_particle();" >
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Start Main Wrapper
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="main-wrapper full">
            <div class="listing-google-map-content full"> 
                <div id="map_todo_listing" class="full" onload="$('#map_todo_listing > div > div > div:nth-child(13) > div > div:nth-child(2)').css('margin-top', '-30px');$('.infoBox > img:nth-child(1)').css('width', '20px');"></div>
            </div><!--~~./ listing-google-map-content ~~-->
            
        </div><!--~./ end main wrapper ~-->
    </div>
    <!--~./ end site content ~-->
    
    <!-- </div> -->
    
    <!--********************************************************-->
    <!--********************** SITE FOOTER *********************-->
    <!--********************************************************-->
    
    <!--~./ end site footer ~-->
    
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Start Header Search Model
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Start User Registation Model
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    
	<!-- add new sidebar starts -->
	<!-- <div class="add-new-data-sidebar">
		<div class="overlay-bg bahasa" onclick="close_sidebar('bahasa')"></div>
		<div class="add-new-data bahasa" style="overflow-y: auto;">
			<form action="javascript: void(0)" id="form-bahasa" onsubmit="submit_bahasa()">
				
				<div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
					<div>
						<h4 class="text-uppercase" id="bahasa-titleform">Riwayat Bahasa</h4>
					</div>
					<div class="hide-data-sidebar" onclick="close_sidebar('bahasa')">
						<i class="feather icon-x"></i>
					</div>
				</div>
				<div class="data-items pb-3">
					<div class="data-fields px-2 mt-3">
						<div class="row">
							<div class="col-sm-12 data-field-col">
								<label for="data-name">Kode BKN</label>
								<input type="text" name="kode_bkn_bahasa" id="bahasa-kode_bkn_bahasa" class="form-control" required="">
							</div>
							<div class="col-sm-12 data-field-col">
								<label for="data-name">Bahasa</label>
								<input type="text" name="bahasa" id="bahasa-bahasa" class="form-control">
							</div>
							<div class="col-sm-12 data-field-col">
								<label for="data-name">Kemampuan</label>
								<select class="form-control" id="bahasa-kemampuan" name="kemampuan" required="">
									<option value="">-- PILIH --</option>
									<option value="Dasar">Dasar</option>
									<option value="Mahir">Mahir</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="add-data-footer d-flex justify-content-around px-3 mt-2 pb-3">
					<div class="add-data-btn">
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
					<div class="cancel-data-btn" onclick="close_sidebar('bahasa')">
						<button type="button" class="btn btn-outline-danger">Batal</button>
					</div>
				</div>
			</form>
		</div>
	</div> -->

    <script type="text/javascript">
        var markersData = {
            'Marker': [
            <?php foreach ($query as $row): if (is_numeric($row->latitude) AND is_numeric($row->longitude)): ?>
            {
                name: "<?=$row->nama_skpd?>",
                location_latitude: <?=$row->latitude?>,
                location_longitude: <?=$row->longitude?>,
                map_image_url: '../asset/map/images/todo/sumedang.png',
				<?php if ($row->jenis_skpd == "kecamatan"): ?>
                map_marker: '../asset/map/images/icon/marker/pin_kec.png',
				<?php elseif ($row->jenis_skpd == "desa"): ?>
                map_marker: '../asset/map/images/icon/marker/pin_desa.png',
                <?php else: ?>
                map_marker: '../asset/map/images/icon/marker/pin.png',
				<?php endif ?>
                <?php if ($row->jenis_skpd == "desa"): ?>
                rate: '<?=isset($row->target_rtm)? $row->target_rtm : "<i>-</i> %" ?>',
                rate2: '<?=isset($row->target_ps)? $row->target_ps : "<i>-</i> %" ?>',
                rate3: '<?=isset($row->target_ikm)? $row->target_ikm : "<i>-</i> %" ?>',
                <?php else: ?>
                rate: '<?=isset($row->nilai)? $row->nilai : "<i>-</i>" ?>',
                rate2: '<?=isset($row->nilai)? round($grafik_capaian[$row->id_skpd], 1) : "-" ?>%',
                <?php endif ?>
                name_point: "<?=$row->nama_skpd?>",
                <?php if ($row->jenis_skpd == "desa"): ?>
                url_point: "<?=base_url()?>admin/map_detail/<?=$row->id_skpd?>/desa",
                _blank: 'target="_blank"',
                <?php else: ?>
                url_point: "<?=base_url()?>admin/map_detail/<?=$row->id_skpd?>",
                _blank: '',
                <?php endif ?>
                <?php if ($row->jenis_skpd == "desa"): ?>
                    review: 'Rumah Tangga Miskin',
                    review2: 'Pencegahan Stunting',
                    review3: 'Indeks kepuasan Masyarakat',
                <?php else: ?>
                    review: 'Nilai Sakip',
                    review2: 'Pengukuran Sakip',
                <?php endif ?>
                address: "<?=$row->alamat_skpd?>",
                status: "<?=$row->kode_pos?>",
                telephone: "<?=$row->telepon_skpd?>"
            },
            <?php endif; endforeach ?>
            ]

        };
    </script>