 <!-- BEGIN: Content-->
 <div class="app-content content">
 	<div class="content-overlay"></div>
 	<div class="header-navbar-shadow"></div>
 	<div class="content-wrapper">
 		<div class="content-header row">
 			<div class="content-header-left col-md-9 col-12 mb-2">
 				<div class="row breadcrumbs-top">
 					<div class="col-12">
 						<h2 class="content-header-title float-left mb-0">Monitoring Kelompok</h2>
 						<div class="breadcrumb-wrapper col-12">
 							<ol class="breadcrumb">
 								<li class="breadcrumb-item"><a href="#">Home</a>
 								</li>
 								<li class="breadcrumb-item active"><a href="#">Monitoring Kelompok</a>
 								</li>

 							</ol>
 						</div>
 					</div>
 				</div>
 			</div>

 		</div>
 		<div class="content-body">
 			<div id="user-profile">

 				<section id="profile-info">
 					<div class="row">
 						<div class="col-lg-3 col-12">
 							<div class="card">
 								<div class="card-header">
 									<div class="avatar-xl">
 										<img class="img-fluid" src="<?=base_url();?>/data/images/avatar.png" alt="img placeholder" style="width: 100%">

 									</div>
 									<h4 class="text-primary">Kelompok 1</h4>

 									<div class="badge badge-success">Pertanian</div>
 								</div>
 								<div class="card-body">
 									<h6 class="mb-0 text-muted">Alamat:</h6>
 									<p>Dsn. Cinangsi RT 01 / RW 04 Ds. Cinangsi Kec. Cisitu Kab. Bogor</p>
 									<div class="mt-1">
 										<h6 class="mb-0 ">No. SK:</h6>
 										<p>8945-1514-4151</p>
 									</div>

 									<div class="mt-1">
 										<h6 class="mb-0 text-muted">Telepon:</h6>
 										<p>082151-51515</p>
 									</div>

 									<div class="mt-1">
 										<button type="button" class="btn btn-sm btn-icon btn-primary mr-25 p-25"><i class="feather icon-facebook"></i></button>
 										<button type="button" class="btn btn-sm btn-icon btn-primary mr-25 p-25"><i class="feather icon-twitter"></i></button>
 										<button type="button" class="btn btn-sm btn-icon btn-primary p-25"><i class="feather icon-instagram"></i></button>
 									</div>
 								</div>
 							</div>
 							<div class="card">
 								<div class="card-header d-flex justify-content-between">
 									<h4>Anggota</h4>
 									<i class="feather icon-more-horizontal cursor-pointer"></i>
 								</div>
 								<div class="card-body">
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-5.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Carissa Dolle</h6>
 											<span class="font-small-2">Ketua</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto "><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-6.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Liliana Pecor</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-7.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Isidra Strunk</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-8.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Gerald Licea</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-9.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Kelle Herrick</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-10.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Cesar Lee</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-1">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-11.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">John Doe</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>
 									<div class="d-flex justify-content-start align-items-center mb-2">
 										<div class="avatar mr-50">
 											<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-12.jpg" alt="avtar img holder" height="35" width="35">
 										</div>
 										<div class="user-page-info">
 											<h6 class="mb-0">Tonia Seabold</h6>
 											<span class="font-small-2">Anggota</span>
 										</div>
 										<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 									</div>

 								</div>
 							</div>

 						</div>
 						<div class="col-lg-6 col-12">

 							<div class="card">
 								<div class="card-header d-flex justify-content-between align-items-end">
 									<h4 class="card-title">Keuangan</h4>
 									<p class="font-medium-5 mb-0"><i class="feather icon-settings text-muted cursor-pointer"></i></p>
 								</div>
 								<div class="card-content">
 									<div class="card-body pb-0">
 										<div class="d-flex justify-content-start">
 											<div class="mr-2">
 												<p class="mb-50 text-bold-600">Pemasukan</p>
 												<h2 class="text-bold-400">
 													<sup class="font-medium-1">Rp.</sup>
 													<span class="text-success">86,589</span>
 												</h2>
 											</div>
 											<div>
 												<p class="mb-50 text-bold-600">Pengeluaran</p>
 												<h2 class="text-bold-400">
 													<sup class="font-medium-1">Rp.</sup>
 													<span>73,683</span>
 												</h2>
 											</div>

 										</div>
 										<div id="revenue-chart"></div>
 									</div>
 								</div>
 							</div>
 							<div class="row">


 								<div class="col-12 col-md-12 col-12">
 									<div class="card">
 										<div class="card-header">
 											<h4 class="card-title">Bantuan Terakhir</h4>
 										</div>
 										<div class="card-content">
 											<div class="card-body">
 												<table class="table table-striped">
 													<thead class="primary">
 														<tr>
 															<th>Jenis Barang</th>
 															<th>Satuan</th>
 															<th>Volume</th>
 															<th>Harga Satuan</th>
 															<th>Besaran Alokasi (Rp.)</th>
 														</tr>
 													</thead>
 													<tbody>
 														<tr>
 															<td>Pupuk Kandang</td>
 															<td>Kg</td>
 															<td>5000</td>
 															<td>1600</td>
 															<td>8.000.000</td>
 														</tr>
 														<tr>
 															<td>Bibit mentimun</td>
 															<td>Gram</td>
 															<td>5000</td>
 															<td>1600</td>
 															<td>6.800.000</td>
 														</tr>
 														<tr>
 															<td>Bibit Kacang Panjang</td>
 															<td>Kg</td>
 															<td>5000</td>
 															<td>1600</td>
 															<td>4.000.000</td>
 														</tr>
 														<tr>
 															<td>Bibit Buncis</td>
 															<td>Bungkus</td>
 															<td>5000</td>
 															<td>1600</td>
 															<td>3.840.000</td>
 														</tr>
 														<tr>
 															<td>Pupuk Non organik / NPK</td>
 															<td>Liter</td>
 															<td>5000</td>
 															<td>1600</td>
 															<td>825.000</td>
 														</tr>

 														<tr>
 															<td colspan="3">
 																<strong>Total </strong>
 																<td>
 																	<td>35.000.000
 																	</tr>


 																</tbody>

 															</table>
 														</div>
 													</div>
 												</div>

 											</div>

 										</div>


 										<div class="col-md-12">
 											<div class="card">
 												<div class="card-body">
 													<div class="d-flex justify-content-start align-items-center mb-1">
 														<div class="mr-1">
 															<img src="<?=base_url();?>/data/images/avatar.png" alt="avtar img holder" height="45" width="45">
 														</div>
 														<div class="user-page-info">
 															<p class="mb-0">Kelompok 1</p>
 															<span class="font-small-2">12 Dec 2018 at 1:16 AM</span>
 														</div>
 														<div class="ml-auto user-like text-danger"><i class="fa fa-heart"></i></div>
 													</div>
 													<p>Memanen hasil perkebunan untuk yang pertama kalinya, dan menghasilkan kurang lebih 200kg Kacang panjang, 100 Kacang Merah,</p>
 													<img class="img-fluid card-img-top rounded-sm mb-2" src="<?=base_url();?>data/images/panen.jpg" alt="avtar img holder">
 								
 												
 												</div>
 											</div>
 										
 											<div class="card">
 												<div class="card-body">
 													<div class="d-flex justify-content-start align-items-center mb-1">
 														<div class="avatar mr-1">
 															<img src="<?=base_url();?>/data/images/avatar.png" alt="avtar img holder" height="45" width="45">
 														</div>
 														<div class="user-page-info">
 															<h6 class="mb-0">Kelompok 1</h6>
 															<span class="font-small-2">10 Dec 2018 at 5:35 AM</span>
 														</div>
 														<div class="ml-auto user-like"><i class="feather icon-heart"></i></div>
 													</div>
 													<p>Bimbingan pelatihan untuk cara menanam kacang panjang yang diselenggarakan oleh Desa</p>
 													<iframe src="https://www.youtube.com/embed/vBvLHrC3lTM" class="w-100 height-250"></iframe>
 
 												</div>
 											</div>
 										</div>



 									</div>
 									<div class="col-lg-3 col-12">
 										<div class="card">
 											<div class="card-header d-flex justify-content-between align-items-end">
 												<h4>Persentase Keuangan</h4>
 												<div class="dropdown chart-dropdown">
 													<button class="btn btn-sm border-0 dropdown-toggle px-0" type="button" id="dropdownItem1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 														Last 7 Days
 													</button>
 													<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem1">
 														<a class="dropdown-item" href="#">Last 28 Days</a>
 														<a class="dropdown-item" href="#">Last Month</a>
 														<a class="dropdown-item" href="#">Last Year</a>
 													</div>
 												</div>
 											</div>

 											<div class="card-content">
 												<div class="card-body pt-0">
 													<div id="session-chart" class="mb-1"></div>
 													<div class="chart-info d-flex justify-content-between mb-1">
 														<div class="series-info d-flex align-items-center">
 															<i class="feather icon-monitor font-medium-2 text-primary"></i>
 															<span class="text-bold-600 mx-50">Pemasukan</span>
 															<span> - 58.6%</span>
 														</div>
 														<div class="series-result">
 															<span>2%</span>
 															<i class="feather icon-arrow-up text-success"></i>
 														</div>
 													</div>
 													<div class="chart-info d-flex justify-content-between mb-1">
 														<div class="series-info d-flex align-items-center">
 															<i class="feather icon-tablet font-medium-2 text-warning"></i>
 															<span class="text-bold-600 mx-50">Pengeluaran</span>
 															<span> - 34.9%</span>
 														</div>
 														<div class="series-result">
 															<span>8%</span>
 															<i class="feather icon-arrow-up text-success"></i>
 														</div>
 													</div>

 												</div>
 											</div>
 										</div>
 										<div class="card">
 											<div class="card-header">
 												<h4>Maps</h4>
 											</div>
 											<div class="card-body">
 												<div id="map" style="height: 250px;width: 100%"></div>
 											</div>
 										</div>
 										<div class="card">
 											<div class="card-header d-flex justify-content-between">
 												<h4>Mentor</h4>
 												<hr>
 											</div>
 											<div class="card-body">
 												<div class="d-flex justify-content-start align-items-center mb-1">
 													<div class="avatar mr-50">
 														<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-5.jpg" alt="avtar img holder" height="35" width="35">
 													</div>
 													<div class="user-page-info">
 														<h6 class="mb-0">Carissa Dolle</h6>
 														<span class="font-small-2">082-1154-4851</span>
 													</div>
 													<button type="button" class="btn btn-outline-primary btn-icon ml-auto"><i class="feather icon-eye"></i></button>
 												</div>

 											</div>
 										</div>
 										    <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Terima Bantuan</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <ul class="activity-timeline timeline-left list-unstyled">
                                            <li>
                                                <div class="timeline-icon bg-primary">
                                                    <i class="feather icon-plus font-medium-2"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <p class="font-weight-bold">Bantuan Pertama </p>
                                                    <span>Total Rp. 35.000.000</span>
                                                </div>
                                                <small class="">12 September 2020 </small>
                                            </li>
                                            <li>
                                                <div class="timeline-icon bg-success">
                                                    <i class="feather icon-plus font-medium-2"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <p class="font-weight-bold">Bantuan Kedua</p>
                                                    <span>Total Rp. 15.000.000</span>
                                                </div>
                                                <small class="">12 September 2021 </small>
                                            </li>
                                            <li>
                                                <div class="timeline-icon bg-success">
                                                    <i class="feather icon-plus font-medium-2"></i>
                                                </div>
                                                <div class="timeline-info">
                                                    <p class="font-weight-bold">Bantuan Ketiga</p>
                                                    <span>Total Rp. 5.000.000
                                                    </span>
                                                </div>
                                                <small class="">12 September 2022 </small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
 									</div>
 								</div>
 								<div class="row">
 									<div class="col-12 text-center">
 										<button type="button" class="btn btn-primary block-element mb-1">Load More</button>
 									</div>
 								</div>
 							</section>
 						</div>



 					</div>
 				</div>
 			</div>
    <!-- END: Content-->