 <!-- BEGIN: Content-->
 <div class="app-content content">
 	<div class="content-overlay"></div>
 	<div class="header-navbar-shadow"></div>
 	<div class="content-wrapper">
 		<div class="content-header row">
 			<div class="content-header-left col-md-9 col-12 mb-2">
 				<div class="row breadcrumbs-top">
 					<div class="col-12">
 						<h2 class="content-header-title float-left mb-0">Monitoring Kelompok</h2>
 						<div class="breadcrumb-wrapper col-12">
 							<ol class="breadcrumb">
 								<li class="breadcrumb-item"><a href="#">Home</a>
 								</li>
 								<li class="breadcrumb-item active"><a href="#">Monitoring Kelompok</a>
 								</li>

 							</ol>
 						</div>
 					</div>
 				</div>
 			</div>

 		</div>
 		<div class="content-body">

 			<div class="row col-row-spacing">

 				<div class="col-md-12 inputselect-group">
 					<form method="POST">
 						<div class="form-group select-container">
 							<select name="id_jabatan" class="form-control">
 								<option value="">Kecamatan</option>

 							</select>
 						</div>
 						<fieldset class="form-group position-relative has-icon-left input-divider-left">
 							<div class="input-group">
 								<input type="text" value="" name="" class="round form-control" id="iconLeft3" placeholder="Cari Nama Kelompok ...">
 								<div class="form-control-position">
 									<i class="feather icon-user"></i>
 								</div>
 								<div class="input-group-append" id="button-addon2">
 									<button class="btn btn-primary round" type="submit" name="submit" value="search"><i class="feather icon-search"></i></button>
 								</div>
 							</div>
 						</fieldset>
 					</form>


 				</div>
 			</div>
 			<!-- Basic example and Profile cards section start -->
 			<section id="basic-examples">
 				<div class="row match-height">

 					<div class="col-md-6 col-12">
 						<div class="card">
 							<div class="card-content">
 								<div class="card-body">
 									<div class="row pb-50">
 										<div class="col-lg-6 col-12 d-flex justify-content-between flex-column order-lg-1 order-2 mt-lg-0 mt-2">
 											<div class="row">
 												<div class="col-6">
 													<div class="avatar-xl">
 														<img class="img-fluid" src="<?=base_url();?>/data/images/avatar.png" alt="img placeholder" style="width: 100%">

 													</div>
 												</div>
 												<div class="col-6 pt-75">
 													<h4 class="text-bold-700 mb-25 mt-20">Kelompok 1</h4>
 													<div class="badge badge-success">Pertanian</div>
 													<h5 class="font-medium-2 pt-75 mt-25">
 														<span class="text-success">10 </span>Anggota <br>
 														<span class="text-success">2 </span>Produk</span>
 													</h5>

 													<p class="text-bold-500 mb-25 mt-20" >Kec. Cisitu</p>
 													

 												</div>
 											</div>
 											<a href="<?=base_url();?>monitoring_kelompok/detail" class="btn btn-primary shadow">Detail Kelompok<i class="feather icon-chevrons-right"></i></a>
 										</div>
 										<div class="col-lg-6 col-12 d-flex justify-content-between flex-column text-right order-lg-2 order-1">
 											<div class="dropdown chart-dropdown">
 												<button class="btn btn-sm border-0 dropdown-toggle p-0" type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 													Last 7 Days
 												</button>
 												<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
 													<a class="dropdown-item" href="#">Last 28 Days</a>
 													<a class="dropdown-item" href="#">Last Month</a>
 													<a class="dropdown-item" href="#">Last Year</a>
 												</div>
 											</div>
 											<div id="avg-session-chart"></div>
 										</div>
 									</div>
 								    <hr style="border-top: 1px solid rgb(0 0 0 / 5%);">
 									<div class="row avg-sessions pt-50">
 										<div class="col-6">
 											<p class="mb-0">Sisa Keuangan</p>
 											<div class="progress progress-bar-primary mt-25">
 												<div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" style="width:50%"></div>
 											</div>
 										</div>
 										
 										<div class="col-6">
 											<div class="media" style="line-height: 5px">
 												<a class="media-left" href="#">
 													<div class="avatar mr-1 avatar-lg">
 														<img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-20.jpg" alt="avtar img holder">
 													</div>
 												</a>
 												<div class="media-body pt-75" >
 													<h4 class="media-heading">Nandang Koswara</h4>
 													Mentor
 												</div>
 											</div>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>




 				</div>
 			</section>
 			<!-- // Basic example and Profile cards section end -->




 		</div>
 	</div>
 </div>
    <!-- END: Content-->