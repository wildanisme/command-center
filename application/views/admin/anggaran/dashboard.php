 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Dashboard </span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active">Default  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-26 col-xl-12 col-lg-18 box-col-26">
        <div class="card gradient-primary o-hidden" id="total-investasi">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              <div class="media-body">
                <h1 class="mb-1"><span class="m-10 text-muted">Test :       </span>    Rp.     <?= (@$data_investasi->realisasi_investasi) ? number_format($data_investasi->realisasi_investasi) : 0?></h1><i class="icon-bg" data-feather="database"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden" id="total-pajak">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
              <div class="media-body"><span class="m-0">test</span>
                <h4 class="mb-0 "><?= (@$data_pendapatan_pajak->realisasi_pendapatan_pajak) ? number_format($data_pendapatan_pajak->realisasi_pendapatan_pajak) : 0?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
        <div class="card gradient-warning o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="message-circle"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">test</span>
                <h4 class="mb-1  text-white"><?= (@$data_belanja_langsung->realisasi_belanja_langsung) ? number_format($data_belanja_langsung->realisasi_belanja_langsung) : 0?></h4><i class="icon-bg" data-feather="message-circle"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-md-6 box-col-6">
        <div class="card gradient-primary o-hidden monthly-overview">
          <div class="card-body pb-0" id="customers-ratio"> 
            <div class="media customers">
              <div class="media-body">
                <h5>Realisasi Anggaran</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-md-6 box-col-6">
        <div class="card gradient-primary o-hidden monthly-overview">
          <div class="card-body pb-0" id="customers-ratio2"> 
            <div class="media customers">
              <div class="media-body">
                <h5>Realisasi Belanja</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-info o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="user-plus"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Real.Belanja</span>
                <h4 class="mb-0  text-white"><?= (@$data_belanja_tidak_langsung->realisasi_belanja_tidak_langsung) ? number_format($data_belanja_tidak_langsung->realisasi_belanja_tidak_langsung) : 0?></h4><i class="icon-bg" data-feather="user-plus"></i>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <div class="col-sm-12 col-xl-12 xl-100">
           <div class="card">
             <div class="card-header">
               <h5>Realiasasi Belanja Perangkat Daerah</h5>
             </div>
             <div class="card-body p-0">
               <div id="column-chart2"></div>
             </div>
           </div>
          </div>

          <div class="col-lg-12">
           <div class="row">
             <div class="col-xl-3 col-md-6 box-col-6">
          <!--      <div class="card gradient-primary o-hidden">-->
          <!--        <div class="card-body tag-card">-->
          <!--          <div class="default-chart">-->
          <!--            <div class="apex-widgets">-->
          <!--              <div id="area-widget-chart"></div>-->
          <!--            </div>-->
          <!--            <div class="widgets-bottom">-->
          <!--              <h5 class="f-w-700 mb-0">Visit Web<span class="pull-right">Oktober   </span></h5>-->
          <!--            </div>-->
          <!--          </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>-->
          <!--        </div>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-xl-3 col-md-6 box-col-6">-->
          <!--      <div class="card gradient-secondary o-hidden">-->
          <!--        <div class="card-body tag-card">-->
          <!--          <div class="default-chart">-->
          <!--            <div class="apex-widgets">-->
          <!--              <div id="area-widget-chart-2"></div>-->
          <!--            </div>-->
          <!--            <div class="widgets-bottom">-->
          <!--              <h5 class="f-w-700 mb-0">Visit E-office<span class="pull-right">Oktober  </span></h5>-->
          <!--            </div>-->
          <!--          </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>-->
          <!--        </div>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-xl-3 col-md-6 box-col-6">-->
          <!--      <div class="card gradient-warning o-hidden">-->
          <!--        <div class="card-body tag-card">-->
          <!--          <div class="default-chart">-->
          <!--            <div class="apex-widgets">-->
          <!--              <div id="area-widget-chart-3"></div>-->
          <!--            </div>-->
          <!--            <div class="widgets-bottom">-->
          <!--              <h5 class="f-w-700 mb-0">E-office Desa<span class="pull-right">Oktober  </span></h5>-->
          <!--            </div>-->
          <!--          </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">     </span></span></span>-->
          <!--        </div>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-xl-3 col-md-6 box-col-6">-->
          <!--      <div class="card gradient-info o-hidden">-->
          <!--        <div class="card-body tag-card">-->
          <!--          <div class="default-chart">-->
          <!--            <div class="apex-widgets">-->
          <!--              <div id="area-widget-chart-4"></div>-->
          <!--            </div>-->
          <!--            <div class="widgets-bottom">-->
          <!--              <h5 class="f-w-700 mb-0">Visit MPP<span class="pull-right">Oktober  </span></h5>-->
          <!--            </div>-->
          <!--          </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">     </span></span></span>-->
          <!--        </div>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--  </div>-->
          </div>
      <!-- <div class="col-xl-12 xl-100 box-col-12">
        <div class="card crypto-revenue">
          <div class="card-header pb-0 d-flex">
            <h5>Mal Pelayanan Publik<span class="badge badge-pill pill-badge-secondary f-14 f-w-600"><?=date('Y')?></span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <div class="header-right pull-right text-right">
              <h5 class="mb-2"><?=(@$data_mpp->total_terlayani) ? number_format($data_mpp->total_terlayani) :"-";?></h5>
              <h6 class="f-w-700 mb-0">Jumlah Pengunjung</h6>
            </div>
          </div>
          <div class="card-body pt-0 px-0">
            <div id="grafik-terlayani"></div>
          </div>
        </div>
      </div> -->



      <!-- <div class="col-xl-8 col-md-8 box-col-12">
        <div class="card year-overview">
          <div class="card-header no-border d-flex">
            <h5>Izin Terbit</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <div class="header-right pull-right text-right">
              <h5 class="mb-2"><?=(@$data_perizinan->total_izin_terbit) ? number_format($data_perizinan->total_izin_terbit) :"-";?></h5>
              <h6 class="f-w-700 mb-0 default-text">Total Izin Terbit</h6>
            </div>
          </div>
          <div class="card-body row">
            <div class="col-xl-12 col-lg-12 box-col-12 ">
              <div id="grafik-izin-perbulan"></div>
            </div>
          </div>
        </div>
      </div>-->


	<!-- komen
      <div class="col-xl-4  box-col-6">
        <div class="card gradient-primary o-hidden monthly-overview yearly">
          <div class="card-header no-border bg-transparent">
            <h5>Kunjungan Wisata</h5>
            <h6 class="mb-0">Monday</h6><span class="pull-right right-badge"><span class="badge badge-pill">50 / 100</span></span>
          </div>
          <div class="card-body p-0">
            <!-- <div class="text-bg"><span>0.5</span></div> -->
		
         <!-- komen  <div class="area-range-apex">
              <div id="area-range-1"></div>
            </div><span class="overview-dots full-width-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
          </div>
        </div>
      </div>
	



      <div class="col-xl-4 box-col-6">
        <div class="card gradient-primary o-hidden monthly-overview yearly">
          <div class="card-header no-border bg-transparent">
            <h5>Pengluaran Anggaran</h5>
            <h6 class="mb-0">Monday</h6><span class="pull-right right-badge"><span class="badge badge-pill">50 / 100</span></span>
          </div>
          <div class="card-body p-0">
            <!-- <div class="text-bg"><span>0.5</span></div> -->
         <!-- komen   <div class="area-range-apex">
              <div id="area-range-2"></div>
            </div><span class="overview-dots full-width-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
          </div>
        </div>
      </div>

      <div class="col-xl-4 box-col-12">
        <div class="card gradient-secondary o-hidden monthly-overview">
          <div class="card-header no-border bg-transparent">
            <h5>Pendapatan Pajak</h5>
            <h6 class="mb-0">2021</h6><span class="pull-right right-badge"><span class="badge badge-pill">63.38 / 100</span></span>
          </div>
          <div class="card-body p-0">
            <!-- <div class="text-bg"><span>0.7</span></div> -->
          <!-- komen  <div class="area-range-apex">
              <div id="area-range"></div>
            </div><span class="overview-dots full-lg-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
          </div>
        </div>
      </div>
	 akhir komen-->


      <!-- status widget Start-->
      <!-- <div class="row"> -->
      <!--  <div class="col-xl-8 col-md-12 box-col-12">-->
      <!--    <div class="card">-->
      <!--      <div class="card-header">-->
      <!--        <div class="row">-->
      <!--          <div class="col-9">-->
      <!--            <h5>Angka Kematian & Kelahiran</h5>-->
      <!--          </div>-->
      <!--          <div class="col-3 text-right"><i class="text-muted" data-feather="navigation"></i></div>-->
      <!--        </div>-->
      <!--      </div>-->
      <!--      <div class="card-body pt-0 px-0">-->
      <!--        <div id="line-adwords"></div>-->
      <!--      </div>-->
      <!--    </div>-->
      <!--  </div>-->
      <!--  <div class="col-xl-4 col-lg-12 box-col-12">-->
      <!--   <div class="card">-->
      <!--    <div class="card-header">-->
      <!--      <div class="row">-->
      <!--        <div class="col-9">-->
      <!--          <h5>Jumlah Penduduk</h5>-->
      <!--        </div>-->
      <!--        <div class="col-3 text-right"><i class="text-muted" data-feather="shopping-bag"></i></div>-->
      <!--      </div>-->
      <!--    </div>-->
      <!--    <div class="card-body r-dount" style="padding-bottom: 13px">-->
      <!--      <div id="chart1" style="height: 360px"> </div>-->
      <!--    </div>-->
      <!--  </div>-->
      <!--</div>-->
      <!-- </div> -->
      <!-- status widget Ends-->


      <!--<div class="col-sm-12 col-xl-12 xl-100">-->
      <!--  <div class="card">-->
      <!--    <div class="card-header">-->
      <!--      <h5>Jumlah UMKM</h5>-->
      <!--    </div>-->
      <!--    <div class="card-body p-0">-->
      <!--      <div id="column-chart"></div>-->
      <!--    </div>-->
      <!--  </div>-->
      <!--</div>-->

      <div class="col-xl-11">
        <div class="owl-carousel owl-theme crypto-slider">
          <div class="item">
            <div class="card o-hidden">
              <div class="card-body crypto-graph-card coin-card"> 
                <div class="media">
                  <div class="media-body d-flex align-items-center">
                    <div class="rounded-icon bg-success"><i class="ion ion-social-euro"></i></div>
                    <div class="bitcoin-graph-content">
                      <h5 class="f-w-700 mb-0">DPMPTSP </h5>
                    </div>
                  </div>
                  <div class="right-setting d-flex align-items-center">
                    <h6 class="font-success f-w-700 mb-0">0%<i class="f-20 ion ion-arrow-up-c p-l-10 font-success mt-1"></i></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card o-hidden">
              <div class="card-body crypto-graph-card coin-card">
                <div class="media">
                  <div class="media-body d-flex align-items-center">
                    <div class="rounded-icon bck-gradient-secondary"><i class="ion ion-social-bitcoin"></i></div>
                    <div class="bitcoin-graph-content">
                      <h5 class="f-w-700 mb-0">Dinkes</h5>
                    </div>
                  </div>
                  <div class="right-setting d-flex align-items-center">
                    <h6 class="font-secondary f-w-700 mb-0">0%<i class="f-20 ion ion-arrow-up-c p-l-10 font-secondary mt-1"></i></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card o-hidden">
              <div class="card-body crypto-graph-card coin-card">
                <div class="media">
                  <div class="media-body d-flex align-items-center">
                    <div class="rounded-icon bck-gradient-primary"><i class="fa fa-try"></i></div>
                    <div class="bitcoin-graph-content">
                      <h5 class="f-w-700 mb-0">BAPPEDA</h5>
                    </div>
                  </div>
                  <div class="right-setting d-flex align-items-center">
                    <h6 class="font-primary f-w-700 mb-0">0%<i class="f-20 ion ion-arrow-up-c p-l-10 font-primary mt-1"></i></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="card o-hidden">
              <div class="card-body crypto-graph-card coin-card">
                <div class="media">
                  <div class="media-body d-flex align-items-center">
                    <div class="rounded-icon bck-gradient-secondary"><i class="ion ion-social-bitcoin"></i></div>
                    <div class="bitcoin-graph-content">
                      <h5 class="f-w-700 mb-0">BPKAD</h5>
                    </div>
                  </div>
                  <div class="right-setting d-flex align-items-center">
                    <h6 class="font-secondary f-w-700 mb-0">0%<i class="f-20 ion ion-arrow-up-c p-l-10 font-secondary mt-1"></i></h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-1 Add-card">
        <!-- <div class="add-arrow"> -->
          <!-- <div class="more-btn text-center"> -->
            <!-- <button class="btn btn-secondary btn-block f-w-700">Add more </button> -->
            <!-- <button class="btn btn-light"><i class="ion ion-plus font-secondary f-22"></i></button> -->
            <!-- </div> -->
            <!-- </div> -->
          </div>


          <!-- <div class="col-xl-6 box-col-12">
            <div class="card weather-bg">
              <div class="card-header no-border bg-transparent">
                <h5>Perkiraan Cuaca</h5>
              </div>
              <div class="card-body weather-bottom-bg p-0">
                <div class="cloud"><img src="<?=base_url();?>asset/dashboard/images/cloud.png" alt=""></div>
                <div class="cloud-rain"></div>
                <div class="media weather-details"><span class="weather-title"><i class="fa fa-circle-o d-block text-right"></i><span>29</span></span>
                  <div class="media-body">
                    <h5>Bogor</h5><span class="d-block">23, Feb 2021</span>
                    <h6 class="mb-0">Wind : 9km/h  </h6>
                  </div>
                </div><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/weather-image.png" alt="">
              </div>
            </div>
          </div> -->
          <div class="col-xl-6 box-col-12">
            <div class="card">
              <div class="card-header no-border">
                <h5>Agenda</h5>
                <ul class="creative-dots">
                  <li class="bg-primary big-dot"></li>
                  <li class="bg-secondary semi-big-dot"></li>
                  <li class="bg-warning medium-dot"></li>
                  <li class="bg-info semi-medium-dot"></li>
                  <li class="bg-secondary semi-small-dot"></li>
                  <li class="bg-primary small-dot"></li>
                </ul>
                <div class="card-header-right">
                  <ul class="list-unstyled card-option">
                    <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                    <li><i class="view-html fa fa-code font-primary"></i></li>
                    <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                    <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                    <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                    <li><i class="icofont icofont-error close-card font-primary"></i></li>
                  </ul>
                </div>
              </div>
              <div class="card-body pt-0">
                <div class="activity-table table-responsive">
                  <table class="table table-bordernone">
                    <tbody>
                      <tr>
                        <td>
                          <div class="activity-image"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/clipboard.png" alt=""></div>
                        </td>
                        <td>
                          <div class="activity-details">
                            <h4 class="default-text">1 <span class="f-14">Oktober</span></h4>
                            <h6>Peresmian</h6>
                          </div>
                        </td>
                        <td>
                          <div class="activity-time"><span class="font-primary f-w-700">Peresmian Command Center Kabupaten Bogor</span><span class="d-block light-text">Command Center</span></div>
                        </td>
                        <td>
                          <button class="btn btn-shadow-primary">View</button>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div class="activity-image activity-secondary"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/greeting.png" alt=""></div>
                        </td>
                        <td>
                          <div class="activity-details">
                            <h4 class="default-text">24 <span class="f-14">Oktober</span></h4>
                            <h6>Bogor Keliling</h6>
                          </div>
                        </td>
                        <td>
                          <div class="activity-time"><span class="font-secondary f-w-700">1 days Ago</span><span class="d-block light-text">kecamatan Tenjolaya</span></div>
                        </td>
                        <td>
                          <button class="btn btn-shadow-secondary">View</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="code-box-copy">
                  <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head3" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                  <pre></pre>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 box-col-12">
            <div class="card">
              <div class="card-header no-border">
                <h5>ASN Berprestasi</h5>
                <ul class="creative-dots">
                  <li class="bg-primary big-dot"></li>
                  <li class="bg-secondary semi-big-dot"></li>
                  <li class="bg-warning medium-dot"></li>
                  <li class="bg-info semi-medium-dot"></li>
                  <li class="bg-secondary semi-small-dot"></li>
                  <li class="bg-primary small-dot"></li>
                </ul>
                <div class="card-header-right">
                  <ul class="list-unstyled card-option">
                    <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                    <li><i class="view-html fa fa-code font-primary"></i></li>
                    <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                    <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                    <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                    <li><i class="icofont icofont-error close-card font-primary"></i></li>
                  </ul>
                </div>
              </div>



              



              <div class="card-body pt-0">
                <div class="activity-table table-responsive recent-table">
                  <table class="table table-bordernone">
                    <tbody>
                      <tr>
                        <td>
                          <div class="recent-images"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/1.png" alt=""></div>
                        </td>
                        <td>
                          <h5 class="default-text mb-0 f-w-700 f-18">Nama Pejabat</h5>
                        </td>
                        <td><span class="badge badge-pill recent-badge f-12">Nama Dinas</span></td>
                        <td class="f-w-700">Jabatan</td>

                        <td><span class="badge badge-pill recent-badge"><i data-feather="more-horizontal"></i></span></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="recent-images-primary"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/1.png" alt=""></div>
                        </td>
                        <td>
                          <h5 class="font-primary mb-0 f-w-700 f-18">Nama Pejabat</h5>
                        </td>
                        <td><span class="badge badge-pill recent-badge f-12">Nama Dinas</span></td>
                        <td class="f-w-700">Jabatan</td>

                        <td><span class="badge badge-pill recent-badge"><i data-feather="more-horizontal"></i></span></td>
                      </tr>
                      <tr>
                        <td>
                          <div class="recent-images-secondary"><img class="img-fluid" src="<?=base_url();?>asset/dashboard/images/dashboard/1.png" alt=""></div>
                        </td>
                        <td>
                          <h5 class="font-secondary mb-0 f-w-700 f-18">Nama Pejabat</h5>
                        </td>
                        <td><span class="badge badge-pill recent-badge f-12">Nama Dinas</span></td>
                        <td class="f-w-700">Jabatan</td>

                        <td><span class="badge badge-pill recent-badge"><i data-feather="more-horizontal"></i></span></td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
                <div class="code-box-copy">
                  <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head21" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                  <pre></pre>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-xl-4 box-col-12">
            <div class="card gradient-primary o-hidden">
              <div class="card-body">
                <div class="setting-dot">
                  <div class="setting-bg-primary date-picker-setting position-set pull-right"><i class="fa fa-spin fa-cog"></i></div>
                </div>
                <div class="default-datepicker">
                  <div class="datepicker-here" data-language="en"></div>
                </div><span class="default-dots-stay overview-dots full-width-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small">   </span></span></span>
              </div>
            </div>
          </div> -->

     


          <!-- <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h5 class="text-uppercase">REKAPITULASI DTKS TAHUN 2020</h5>
              </div>
              <div class="card-body">
                <div class="row">     
                  <div class="col-md-4 col-sm-12">
                    <div class="browser-widget b-r-light">
                      <div class="panel-block text-center" style="border-bottom: 1px solid rgb(227 227 227);"><h5>RUTA (RUMAH TANGGA)</h5></div>
                      <div class="media">
                        <div class="media-body align-self-center">
                          <div>
                            <p>Desil 1 </p>
                            <h4><span class="">0</span></h4>
                          </div>
                          <div>
                            <p>Desil 2 </p>
                            <h4><span class="">0</span></h4>
                          </div>
                          <div>
                            <p>Jumlah </p>
                            <h4><span class="">0</span></h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="browser-widget b-r-light">
                      <div class="panel-block text-center" style="border-bottom: 1px solid rgb(227 227 227);"><h5>ANGGOTA RUMAH TANGGA</h5></div>
                      <div class="media">
                        <div class="media-body align-self-center">
                          <div>
                            <p>Desil 1 </p>
                            <h4><span class="">0</span></h4>
                          </div>
                          <div>
                            <p>Desil 2 </p>
                            <h4><span class="">0</span></h4>
                          </div>
                          <div>
                            <p>Jumlah </p>
                            <h4><span class="">0</span></h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div> -->



          <!-- batas -->
        </div>
      </div>
      <!-- Container-fluid Ends-->
      <div class="welcome-popup modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-body">
              <div class="modal-header"></div>
              <div class="contain p-30">
                <div class="text-center">
                  <h5>Selamat Datang  to Bogor Command Center</h5>
                  <p>Data yang ditampilkan adalah data realtime</p>
                  <button class="btn btn-primary btn-lg txt-white" type="button" data-dismiss="modal" aria-label="Close">Get Started</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




    <?php 
$url = "https://pentakabogor.lfdev.cloud/home";

$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

$options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
          );

$ch      = curl_init( $url );
curl_setopt_array( $ch, $options );
$content = curl_exec( $ch );
$err     = curl_errno( $ch );
$errmsg  = curl_error( $ch );
$header  = curl_getinfo( $ch );
curl_close( $ch );

$header['errno']   = $err;
$header['errmsg']  = $errmsg;
$header['content'] = $content;
// echo $header['content'];

?>
<script type="text/javascript">
  var token = "MzQ5MzhhNjc1MmI2MDBjOWQyODYyZWUzZjI2OGZhNDUwY2NiMTQxZjdmYjAxZTFjODRlN2RhYWI5MzU5NWQ3";
  var xhr = new XMLHttpRequest();

  xhr.open('GET', 'https://pancakarsa.bogorkab.go.id/siber-remote');
  xhr.onreadystatechange = handler;
  xhr.responseType = 'blob';
  xhr.setRequestHeader('Authorization', 'token_auth ' + token);
  xhr.send();

  function handler() {
    if (this.readyState === this.DONE) {
      if (this.status === 200) {
        // this.response is a Blob, because we set responseType above
        var data_url = URL.createObjectURL(this.response);
        document.querySelector('#frame').src = data_url;
      } else {
        console.error('no pdf :(');
      }
    }
  }
</script>

<iframe id="frame" src="https://pancakarsa.bogorkab.go.id/siber-remote" frameborder="0" gesture="media" allow="encrypted-media" allowtransparency="true" allowfullscreen="true" style="width: 100%; min-height: 720px; height: max-content;"></iframe>

<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>
<!-- 
<script>
// column chart
var options3 = {
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      horizontal: false,
      endingShape: 'rounded',
      columnWidth: '55%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series: [{
    name: 'Jumlah',
    data: [<?php foreach ($jumkec as $data): ?>
      <?php echo $data->totalumkm.","?> 
      <?php endforeach; ?>]


    }],
    xaxis: {
      categories: [<?php foreach ($jumkec as $data): ?>
        <?php echo "'" .$data->kecamatan. "',"?> 
        <?php endforeach; ?>],
      },
      yaxis: {
        title: {
          text: 'Jumlah UMKM'
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function (val) {
            return "" + val + " UKMM"

          }


        }
      },
      colors:['#80cf00', '#fe80b2', '#80cf00']
    }

    var chart3 = new ApexCharts(
      document.querySelector("#column-chart11"),
      options3
      );

    chart3.render();

  </script> -->
