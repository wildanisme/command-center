<div class="col-sm-12 col-xl-4 ">
  <div class="card">
    <div class="card-header">
      <h6>Rekapitulasi Pegawai Eselon <span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=date('Y')?></span></h6>
    </div>
    <div class="card-body apex-chart p-0">
      <div id="keuanganchart3"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-4 ">
  <div class="card">
    <div class="card-header">
      <h6>Rekapitulasi Pegawai Menurut Gender <span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=date('Y')?></span></h6>
    </div>
    <div class="card-body apex-chart p-0">
      <div id="keuanganchart1"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-4 ">
  <div class="card gradient-secondary o-hidden monthly-overview" style="background-image: linear-gradient(to bottom right, #ff9cc3, #007bff);">
    <div class="card-header no-border bg-transparent pb-0">
      <h5>Indeks Sistem Merit</h5>
      <h6 class="mb-2">BKPSDM</h6><span class="pull-right right-badge"><span class="badge badge-pill">2020</span></span>
    </div>
    <div class="card-body pt-0">
      <ul class="rounds-decore">
        <li></li>
        <li></li>
        <li></li>
        <li>                                       </li>
      </ul><span class="overview-dots full-lg-dots"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"></span></span></span>
      <div class="p-watch p-t-50" style="height: 330px">
        <h1 class="display-1 f-w-900 text-center">301.5</h1>
        
        <h5 class="text-center" style="margin-top: -20px">Baik</h5>
      </div>
    </div>
  </div>
</div>
        <div class="container-fluid">
        <div class="col">
        <div class="col-sm-12 col-xl-12 xl-100">
          <div class="card-header">
          <h6>Jumlah Pegawai Menurut Golongan<span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=date('Y')?></span></h6>
          </div>
          <div class="card-body apex-chart p-0">
        <div id="kepegawaianchart2"></div>
      </div>
      </div>
      </div>
      </div>
      <!-- <div class="col-sm-12 col-xl-12 xl-100">
      <div class="card-header">
      <h6>Jumlah Pegawai Per SKPD<span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=date('Y')?></span></h6>
      </div>
      <div class="card-body p-0">
      <div id="kepegawaianchart1"></div>
      </div>
      </div> -->
       <div class="col-sm-12 col-xl-12 xl-100">
      <div class="card-header">
      <h6>Jumlah Pegawai Per Jenis Jabatan<span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=date('Y')?></span></h6>
      </div>
      <div class="card-body p-0">
      <div id="kepegawaianchart3"></div>
      </div>
      </div>
<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Rekapitulasi Pegawai Per SKPD</h5>
    </div>
    <div class="card-body">
      <div class="dt-ext table-responsive">
        <table class="display datatables" >
          <thead>
            <tr>
              <td rowspan="2">OPD</td>
              <td colspan="21" align="center">Golongan Ruang dan OPD</td>
              <td rowspan="2">Total</td>
            </tr>
            <tr>
            <td align="center">I/a</td>
    <td align="center">I/b</td>
    <td align="center">I/c</td>
    <td align="center">I/d</td>
    <td align="center">Jml</td>
   <td align="center">II/a</td>
    <td align="center">II/b</td>
    <td align="center">II/c</td>
    <td align="center">II/d</td>
    <td align="center">Jml</td>  
   <td align="center">III/a</td>
    <td align="center">III/b</td>
    <td align="center">III/c</td>
    <td align="center">III/d</td>
    <td align="center">Jml</td> 
  <td align="center">IV/a</td>
    <td align="center">IV/b</td>
    <td align="center">IV/c</td>
    <td align="center">IV/d</td>
    <td align="center">IV/e</td>
    <td align="center">Jml</td>
</tr>
            </tr>
          </thead>
          <tbody>
            <tr>
  <td>Sekretariat Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">19</td>
    <td align="center">23</td>
    <td align="center">46</td>
    <td align="center">28</td>
    <td align="center">40</td>
    <td align="center">28</td>
    <td align="center">37</td>
    <td align="center">133</td>
    <td align="center">7</td>
    <td align="center">8</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">18</td>
    <td align="center">198</td>
  </tr>
  <tr>
  <td>Sekretariat DPRD</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">6</td>
    <td align="center">9</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">6</td>
    <td align="center">5</td>
    <td align="center">18</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">33</td>
  </tr>
  <tr>
  <td>Dinas Pendidikan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">7</td>
    <td align="center">11</td>
    <td align="center">51</td>
    <td align="center">102</td>
    <td align="center">85</td>
    <td align="center">31</td>
    <td align="center">269</td>
    <td align="center">811</td>
    <td align="center">396</td>
    <td align="center">520</td>
    <td align="center">665</td>
    <td align="center">2392</td>
    <td align="center">1196</td>
    <td align="center">2154</td>
    <td align="center">17</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3367</td>
    <td align="center">6039</td>
  </tr>
  <tr>
    <td>Dinas Kesehatan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">199</td>
    <td align="center">16</td>
    <td align="center">221</td>
    <td align="center">80</td>
    <td align="center">223</td>
    <td align="center">118</td>
    <td align="center">248</td>
    <td align="center">669</td>
    <td align="center">69</td>
    <td align="center">15</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">87</td>
    <td align="center">979</td>
  </tr>
  <tr>
  <td>Dinas Pekerjaan Umum dan Penataan Ruang</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">13</td>
    <td align="center">47</td>
    <td align="center">44</td>
    <td align="center">108</td>
    <td align="center">32</td>
    <td align="center">20</td>
    <td align="center">28</td>
    <td align="center">14</td>
    <td align="center">94</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">205</td>
  </tr>
  <tr>
  <td>Dinas Perumahan, Kawasan Permukiman dan Pertanahan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">10</td>
    <td align="center">6</td>
    <td align="center">17</td>
    <td align="center">18</td>
    <td align="center">13</td>
    <td align="center">10</td>
    <td align="center">7</td>
    <td align="center">48</td>
    <td align="center">4</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">70</td>
  </tr>
  <tr>
  <td>Dinas Sosial, Pemberdayaan Perempuan dan Perlindungan Anak</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">7</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">7</td>
    <td align="center">16</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
    <td align="center">33</td>
  </tr>
  <tr>
  <td>Dinas Tenaga Kerja dan Transmigrasi</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">9</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">7</td>
    <td align="center">14</td>
    <td align="center">28</td>
    <td align="center">7</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">10</td>
    <td align="center">47</td>
  </tr>
  <tr><td>Dinas Lingkungan Hidup dan Kehutanan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">11</td>
    <td align="center">11</td>
    <td align="center">30</td>
    <td align="center">11</td>
    <td align="center">40</td>
    <td align="center">13</td>
    <td align="center">94</td>
    <td align="center">12</td>
    <td align="center">18</td>
    <td align="center">20</td>
    <td align="center">17</td>
    <td align="center">67</td>
    <td align="center">8</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">10</td>
    <td align="center">182</td>
  </tr>
  <tr>
  <td>Dinas Kependudukan dan Pencatatan Sipil</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">9</td>
    <td align="center">15</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">5</td>
    <td align="center">10</td>
    <td align="center">24</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">44</td>
  </tr>
  <tr>
  <td>Dinas Pemberdayaan Masyarakat dan Desa</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">9</td>
    <td align="center">8</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">12</td>
    <td align="center">27</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">42</td>
  </tr>
  <tr>
  <td>Dinas Pengendalian Penduduk dan Keluarga Berencana</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">5</td>
    <td align="center">9</td>
    <td align="center">5</td>
    <td align="center">21</td>
    <td align="center">40</td>
    <td align="center">9</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">10</td>
    <td align="center">54</td>
  </tr>
  <tr>
  <td>Dinas Perhubungan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">15</td>
    <td align="center">22</td>
    <td align="center">2</td>
    <td align="center">8</td>
    <td align="center">11</td>
    <td align="center">12</td>
    <td align="center">33</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">60</td>
  </tr>
  <tr>
  <td>Dinas Komunikasi, Informatika, Persandian dan Statistik</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">9</td>
    <td align="center">11</td>
    <td align="center">4</td>
    <td align="center">7</td>
    <td align="center">12</td>
    <td align="center">34</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">46</td>
  </tr>
  <tr><td>Dinas Koperasi, Usaha Kecil, Menengah, Perdagangan dan Perindustrian</td>
  <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">7</td>
    <td align="center">6</td>
    <td align="center">5</td>
    <td align="center">20</td>
    <td align="center">8</td>
    <td align="center">12</td>
    <td align="center">3</td>
    <td align="center">16</td>
    <td align="center">39</td>
    <td align="center">7</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">9</td>
    <td align="center">71</td>
  </tr>
  <tr>
  <td>Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">7</td>
    <td align="center">4</td>
    <td align="center">5</td>
    <td align="center">13</td>
    <td align="center">10</td>
    <td align="center">32</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">44</td>
  </tr>
  <tr>
  <td>Dinas Pariwisata, Kebudayaan, Kepemudaan dan Olah Raga</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">12</td>
    <td align="center">9</td>
    <td align="center">8</td>
    <td align="center">8</td>
    <td align="center">37</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">46</td>
  </tr>
  <tr>
  <td>Dinas Arsip dan Perpustakaan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">7</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">8</td>
    <td align="center">17</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">30</td>
  </tr>
  <tr>
  <td>Dinas Pertanian dan Ketahanan Pangan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">28</td>
    <td align="center">2</td>
    <td align="center">7</td>
    <td align="center">17</td>
    <td align="center">54</td>
    <td align="center">19</td>
    <td align="center">26</td>
    <td align="center">26</td>
    <td align="center">65</td>
    <td align="center">136</td>
    <td align="center">16</td>
    <td align="center">5</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">23</td>
    <td align="center">213</td>
  </tr>
  <tr>
  <td>Dinas Perikanan dan Peternakan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">6</td>
    <td align="center">9</td>
    <td align="center">6</td>
    <td align="center">8</td>
    <td align="center">15</td>
    <td align="center">19</td>
    <td align="center">48</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">7</td>
    <td align="center">65</td>
  </tr>
  <tr><td>Badan Perencanaan Pembangunan, Penelitian dan Pengembangan Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">8</td>
    <td align="center">10</td>
    <td align="center">15</td>
    <td align="center">14</td>
    <td align="center">47</td>
    <td align="center">7</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
    <td align="center">62</td>
  </tr>
  <tr>
  <td>Badan Kepegawaian dan Pengembangan Sumber Daya Manusia</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">6</td>
    <td align="center">10</td>
    <td align="center">10</td>
    <td align="center">5</td>
    <td align="center">9</td>
    <td align="center">34</td>
    <td align="center">5</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">7</td>
    <td align="center">47</td>
  </tr>
  <tr>
  <td>Badan Pengelolaan Keuangan dan Aset Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">7</td>
    <td align="center">12</td>
    <td align="center">11</td>
    <td align="center">7</td>
    <td align="center">17</td>
    <td align="center">14</td>
    <td align="center">49</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">67</td>
  </tr>
  <tr>
  <td>Badan Pengelolaan Pendapatan Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">11</td>
    <td align="center">8</td>
    <td align="center">6</td>
    <td align="center">18</td>
    <td align="center">8</td>
    <td align="center">40</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
    <td align="center">59</td>
  </tr>
  <tr>
  <td>Inspektorat</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">4</td>
    <td align="center">7</td>
    <td align="center">15</td>
    <td align="center">6</td>
    <td align="center">32</td>
    <td align="center">8</td>
    <td align="center">11</td>
    <td align="center">4</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">23</td>
    <td align="center">60</td>
  </tr>
  <tr>
  <td>Satuan Polisi Pamong Praja</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">2</td>
    <td align="center">18</td>
    <td align="center">24</td>
    <td align="center">1</td>
    <td align="center">7</td>
    <td align="center">11</td>
    <td align="center">14</td>
    <td align="center">33</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">64</td>
  </tr>
  <tr>
  <td>Rumah Sakit Umum Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">10</td>
    <td align="center">52</td>
    <td align="center">36</td>
    <td align="center">99</td>
    <td align="center">61</td>
    <td align="center">112</td>
    <td align="center">53</td>
    <td align="center">108</td>
    <td align="center">334</td>
    <td align="center">38</td>
    <td align="center">13</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">55</td>
    <td align="center">488</td>
  </tr>
  <tr>
  <td>Kantor Kesatuan Bangsa dan Politik</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">13</td>
  </tr>
  <tr>
  <td>Badan Penanggulangan Bencana Daerah</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">10</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">17</td>
  </tr>
  <tr>
  <td>Kecamatan Jatinangor</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">5</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">16</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">22</td>
  </tr>
  <tr>
  <td>Kecamatan Cimanggung</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">12</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">21</td>
  </tr>
  <tr>
  <td>Kecamatan Tanjungsari</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">5</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">14</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">22</td>
  </tr>
  <tr>
  <td>Kecamatan Pamulihan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">5</td>
    <td align="center">3</td>
    <td align="center">7</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">14</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">21</td>
  </tr>
  <tr>
  <td>Kecamatan Bogor Selatan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">6</td>
    <td align="center">10</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">16</td>
  </tr>
  <tr><td>Kecamatan Bogor Utara</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">10</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">18</td>
  </tr>
  <tr>
  <td>Kecamatan Cimalaka</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">12</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">20</td>
  </tr>
  <tr>
  <td>Kecamatan Paseh</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">11</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">15</td>
  </tr>
  <tr>
  <td>Kecamatan Darmaraja</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">4</td>
    <td align="center">9</td>
    <td align="center">8</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">16</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">27</td>
  </tr>
  <tr>
  <td>Kecamatan Situraja</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">5</td>
    <td align="center">6</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">15</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">23</td>
  </tr>
  <tr>
  <td>Kecamatan Wado</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">8</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">17</td>
  </tr>
  <tr><td>Kecamatan Rancakalong</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">16</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">21</td>
  </tr>
  <tr>
  <td>Kecamatan Tanjungkerta</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">9</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">16</td>
  </tr>
  <tr>
  <td>Kecamatan Conggeang</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">8</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">15</td>
  </tr>
  <tr>
  <td>Kecamatan Buahdua</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">8</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">15</td>
  </tr>
  <tr>
  <td>Kecamatan Jatinunggal</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">9</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">16</td>
  </tr>
  <tr>
  <td>Kecamatan Jatigede</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">11</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">18</td>
  </tr>
  <tr><td>Kecamatan Tomo</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">7</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">11</td>
  </tr>
  <tr>
  <td>Kecamatan Ujungjaya</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">12</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">18</td>
  </tr>
  <tr>
  <td>Kecamatan Cisitu</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">12</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">15</td>
  </tr> 
  <tr>
  <td>Kecamatan Sukasari</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">4</td>
    <td align="center">6</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">8</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">17</td>
  </tr>
  <tr>
  <td>Kecamatan Ganeas</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">8</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">13</td>
  </tr>
  <tr>
  <td>Kecamatan Cisarua</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">6</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">4</td>
    <td align="center">14</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">17</td>
  </tr>
  <tr>
  <td>Kecamatan Cibugel</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">7</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">15</td>
  </tr>
  <tr>
  <td>Kecamatan Tanjungmedar</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">12</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">18</td>
  </tr>
<tr>
  <td>Kecamatan Surian</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">5</td>
    <td align="center">7</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">14</td>
  </tr> 
<tr>
  <td>Kelurahan Pasanggrahan Baru</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">6</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
  </tr> 
<tr>
  <td>Kelurahan Regolwetan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">7</td>
  </tr> 
<tr>
  <td>Kelurahan Cipameungpeuk</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">1</td>
    <td align="center">6</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">7</td>
  </tr> 
<tr>
  <td>Kelurahan Kotakulon</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">3</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
  </tr> 
  <tr><td>Kelurahan Talun</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">2</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">7</td>
  </tr> 

<tr>
  <td>Kelurahan Kotakaler</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">4</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">7</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
  </tr>
  <tr>
  <td>Kelurahan Situ</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">5</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">8</td>
  </tr> 
  <tr>
  <td>Akademi Keperawatan</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">2</td>
    <td align="center">3</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">6</td>
  </tr>
  <tr>
  <td>Komisi Pemilihan Umum</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">0</td>
    <td align="center">2</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">6</td>
  </tr>
  <tr>
  <td>Badan Narkotika Nasional</td>
  <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">1</td>
    <td align="center">0</td>
    <td align="center">1</td>
    <td align="center">3</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">0</td>
    <td align="center">4</td>
  </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- <div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Tabel Pegawai</h5>
    </div>
    <div class="card-body">
      <div class="dt-ext table-responsive">
        <table class="display datatables" >
          <thead>
            <tr>
              <th>NIP</th>
              <th>NAMA</th>
              <th>GOLONGAN</th>
              <th>JABATAN</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($query as $row): ?>
              <tr>
                <td><?=$row['nip']?></td>
                <td><?=$row['nama']?></td>
                <td><?=$row['golongan']?></td>
                <td><?=$row['jabatan']?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div> -->
<script type="text/javascript">
<?php
  $labels3 = ['Eselon II.a','Eselon II.b','Eselon III.a','Eselon III.b','Eselon IV.a','Eselon IV.b'];
  $series3 = [1,27,73,117,560,195];
   // foreach ($query as $row) {
   //  if ($row['eselon'] == 'II.a') {
   //    $series3[0] += 1 ;
   //  }
   //  if ($row['eselon'] == 'II.b') {
   //    $series3[1] += 1 ;
   //  }
   //  if ($row['eselon'] == 'III.a') {
   //    $series3[2] += 1 ;
   //  }
   //  if ($row['eselon'] == 'III.b') {
   //    $series3[3] += 1 ;
   //  }
   //  if ($row['eselon'] == 'IV.a') {
   //    $series3[4] += 1 ;
   //  }
   //  if ($row['eselon'] == 'IV.b') {
   //    $series3[5] += 1 ;
   //  }
   // }
?>  
var labels3 = <?=json_encode($labels3)?>;
var series3 = <?=json_encode($series3)?>;
console.log(series3)
</script>
<script type="text/javascript">
<?php
  $labels1 = ['Laki-laki','Perempuan'];
  $series1 = [4565,5343];
   // foreach ($query as $row) {
   //  if ($row['jen_kel'] == 'Laki-laki') {
   //    $series1[0] += 1 ;
   //  }
   //  if ($row['jen_kel'] == 'Perempuan') {
   //    $series1[1] += 1 ;
   //  }
   // }
?>  
var labels1 = <?=json_encode($labels1)?>;
var series1 = <?=json_encode($series1)?>;
console.log(series1)
</script>
<script type="text/javascript">
<?php
  $labels4 = ['I/a','I/b','I/c','I/d','II/a','II/b','II/c','II/d','III/a','III/b','III/c','III/d','IV/a','IV/b','IV/c','IV/d'];
  $series4 = [0,1,11,28,137,207,550,345,1255,1056,1052,1493,1472,2249,49,3];
   // foreach ($query as $row) {
   //  if ($row['golongan'] == 'I/a') {
   //    $series4[0] += 1 ;
   //  }
   //  if ($row['golongan'] == 'I/b') {
   //    $series4[1] += 1 ;
   //  }
   //  if ($row['golongan'] == 'I/c') {
   //    $series4[2] += 1 ;
   //  }
   //  if ($row['golongan'] == 'I/d') {
   //    $series4[3] += 1 ;
   //  }
   //  if ($row['golongan'] == 'II/a') {
   //    $series4[4] += 1 ;
   //  }
   //  if ($row['golongan'] == 'II/b') {
   //    $series4[5] += 1 ;
   //  }
   //  if ($row['golongan'] == 'II/c') {
   //    $series4[6] += 1 ;
   //  }
   //  if ($row['golongan'] == 'II/d') {
   //    $series4[7] += 1 ;
   //  }
   //  if ($row['golongan'] == 'III/a') {
   //    $series4[8] += 1 ;
   //  }
   //  if ($row['golongan'] == 'III/b') {
   //    $series4[9] += 1 ;
   //  }
   //  if ($row['golongan'] == 'III/c') {
   //    $series4[10] += 1 ;
   //  }
   //  if ($row['golongan'] == 'III/d') {
   //    $series4[11] += 1 ;
   //  }
   //  if ($row['golongan'] == 'IV/a') {
   //    $series4[12] += 1 ;
   //  }
   //  if ($row['golongan'] == 'IV/b') {
   //    $series4[13] += 1 ;
   //  }
   //  if ($row['golongan'] == 'IV/c') {
   //    $series4[14] += 1 ;
   //  }
   //  if ($row['golongan'] == 'IV/d') {
   //    $series4[15] += 1 ;
   //  }
   // }
?>  
var labels4 = <?=json_encode($labels4)?>;
var series4 = <?=json_encode($series4)?>;
console.log(series4)
</script>
<script type="text/javascript">
<?php
  $labels8 = ['Sekretariat Daerah','Sekretariat DPRD','Dinas Pendidikan','Dinas Kesehatan','Dinas Pekerjaan Umum dan Penataan Ruang','Dinas Perumahan, Kawasan Permukiman dan Pertanahan','Dinas Sosial, Pemberdayaan Perempuan dan Perlindungan Anak','Dinas Tenaga Kerja dan Transmigrasi','Dinas Lingkungan Hidup dan Kehutanan','Dinas Kependudukan dan Pencatatan Sipil','Dinas Pemberdayaan Masyarakat dan Desa','Dinas Pengendalian Penduduk dan Keluarga Berencana','Dinas Perhubungan','Dinas Komunikasi,Informatika, Persandian dan Statistik','Dinas Koperasi, Usaha Kecil, Menengah, Perdagangan dan Perindustrian','Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu','Dinas Pariwisata, Kebudayaan, Kepemudaan dan Olah Raga','Dinas Arsip dan Perpustakaan','Dinas Pertanian dan Ketahanan Pangan','Dinas Perikanan dan Peternakan','Badan Perencanaan Pembangunan, Penelitian dan Pengembangan Daerah','Badan Kepegawaian dan Pengembangan Sumber Daya Manusia','Badan Pengelolaan Keuangan dan Aset Daerah','Badan Pengelolaan Pendapatan Daerah','Inspektorat','Satuan Polisi Pamong Praja','Rumah Sakit Umum Daerah','Kantor Kesatuan Bangsa dan Politik','Badan Penanggulangan Bencana Daerah','Kec. Jatinangor','Kec. Cimanggung','Kec. Tanjungsari','Kec. Pamulihan','Kec. Bogor Selatan','Kec. Bogor Utara','Kec. Cimalaka','Kec. Paseh','Kec. Darmaraja','Kec. Situraja','Kec. Wado','Kec. Rancakalong','Kec. Tanjungkerta','Kec. Conggeang','Kec. Buahdua','Kec. Jatinunggal','Kec. Jatigede','Kec. Tomo','Kec. Ujungjaya','Kec. Cisitu','Kec. Sukasari','Kec. Ganeas','Kec. Cisarua','Kec. Cibugel','Kec. Tanjungmedar','Kec. Surian','Kelurahan Pasanggrahan Baru','Kelurahan Regolwetan','Kelurahan Cipameungpeuk','Kelurahan Kotakulon','Kelurahan Talun','Kelurahan Kotakaler','Kelurahan Situ','Akademi Keperawatan','Komisi Pemilihan Umum','Badan Narkotika Nasional'];
  $series8 = [198,33,6039,979,205,70,33,47,182,44,42,54,60,46,71,44,46,30,213,65,62,47,67,59,60,64,488,13,17,22,21,22,21,16,18,20,15,27,23,17,21,16,15,15,16,18,11,18,15,17,13,17,15,18,14,8,7,7,8,7,8,8,6,6,4];
?>  
var labels8 = <?=json_encode($labels8)?>;
var series8 = <?=json_encode($series8)?>;
console.log(series8, labels8)
</script>
<script type="text/javascript">
<?php
  $labels10 = ['Golongan I/a','Golongan I/b','Golongan I/c','Golongan I/d','Golongan II/a','Golongan II/b','Golongan II/c','Golongan II/d','Golongan III/a','Golongan III/b','Golongan III/c','Golongan III/d','Golongan IV/a','Golongan IV/b','Golongan IV/c','Golongan IV/d'];
  $series10 = [0,1,11,28,137,207,550,345,1255,1056,1052,1493,1472,2249,49,3];
?>  
var labels10 = <?=json_encode($labels10)?>;
var series10 = <?=json_encode($series10)?>;
console.log(series10, labels10)
</script>
<script type="text/javascript">
var option8 = {
  chart: {
      height: 2000,
      type: 'bar',
  },
  plotOptions:{
    bar: {
        horizontal: true,
        endingShape: 'rounded',
        columnWidth: '100%',
    },
  },
  dataLabels: {
    enabled: true,
    trim: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series:[{
        name: 'pegawai',
        data: series8
  }],
  xaxis:{
    labels8:{
    show: true,
    rotate: -45,
    rotateAlways: true,
    hideOverlappingLabels: true,
    showDuplicates: false,
    trim: false
    },
    height: '50px',
    width: '500px',
    categories: labels8


  },
  yaxis:{
    title:{
        text: 'Jumlah Pegawai Per SKPD'
    }
  },
  fill:{
    opacity: 1
  },
  tooltip:{
    y:{
        formatter: function(val){
          return val+ " Pegawai"
        }
    }

  },
  colors:['#06b5dd']
}
var chart8 = new ApexCharts(
  document.querySelector("#kepegawaianchart1"),
  option8
  );
chart8.render();
</script>
<script type="text/javascript">
var option10 = {
  chart: {
      height: 400,
      type: 'bar',
  },
  plotOptions:{
    bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '100%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series:[{
        name: 'pegawai',
        data: series10
  }],
  xaxis:{
    height: '50px',
    width: '500px',
    categories: labels10



  },
  yaxis:{
    title:{
        text: 'Jumlah Pegawai Menurut Golongan'
    }
  },
  fill:{
    opacity: 1
  },
  tooltip:{
    y:{
        formatter: function(val){
          return val+ " Pegawai"
        }
    }

  },
  colors:['#06b5dd']
}
var chart10 = new ApexCharts(
  document.querySelector("#kepegawaianchart2"),
  option10
  );
chart10.render();
</script>
<script src="<?=base_url();?>js/keuangan/dashboard.js" defer></script>
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>
<script type="text/javascript">
  var colors = ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d', '#ffc717','#0a1a4a','#522a06','#10e028','#76b5c5'];

 if (typeof labels4 !== 'undefined' && typeof series4 !== 'undefined') {
    var options4 = {
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: labels4,
        series: series4,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Pegawai";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart4 = new ApexCharts(
        document.querySelector("#keuanganchart4"),
        options4
        );

    chart4.render();
} 
</script>
<script type="text/javascript">
  if (typeof labels3 !== 'undefined' && typeof series3 !== 'undefined') {
    var options3 = {
        chart: {
            width: 380,
            type: 'donut',
        },
        labels: labels3,
        series: series3,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Pegawai";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart3 = new ApexCharts(
        document.querySelector("#keuanganchart3"),
        options3
        );

    chart3.render();
}
</script>
<script type="text/javascript">
  if (typeof labels1 !== 'undefined' && typeof series1 !== 'undefined') {
    var options1 = {
        chart: {
            width: 380,
            type: 'donut',
        },
        labels: labels1,
        series: series1,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        // dataLabels: {
        //   enabled: true,
        //   textAnchor: 'start',
        //   formatter: function(val, opt) {
        //       // return [val.toFixed(2) + "% ", opt.w.globals.series[opt.seriesIndex] + " pegawai"] 
        //       return [opt.w.globals.series[opt.seriesIndex] + " pegawai"] 
        //   },
        //   offsetX: 0,
        // },
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Pegawai";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart1 = new ApexCharts(
        document.querySelector("#keuanganchart1"),
        options1
        );

    chart1.render();
}
</script>
<script type="text/javascript">
var option11 = {
  chart: {
      height: 500,
      type: 'bar',
  },
  plotOptions:{
    bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '80%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series:[{
        name: 'pegawai',
        data: [988,6019,2901]
  }],
  xaxis:{
    categories: ['Struktural','Fungsional','Pelaksana']
  },
  yaxis:{
    title:{
        text: 'Jumlah Pegawai Menurut Jenis jabatan'
    }
  },
  fill:{
    opacity: 1
  },
  tooltip:{
    y:{
        formatter: function(val){
          return val+ " Pegawai"
        }
    }

  },
  colors:['#06b5dd']
}
var chart11 = new ApexCharts(
  document.querySelector("#kepegawaianchart3"),
  option11
  );
chart11.render();
</script>