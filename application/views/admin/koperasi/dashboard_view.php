





<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

      <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
              <div class="media-body"><span class="m-0">Jumlah koperasi : </span>
              <h4 class="mb-0 counter"><?php foreach ($jumusaha as $data): ?>
                
					<?php echo $data->total ?> 
              <?php endforeach; ?>
            </h4><i class="icon-bg" data-feather="user-plus"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
    <div class="card gradient-primary o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
          <div class="media-body"><span class="m-0 text-white">Jumlah jenis usaha</span>
            <h4 class="mb-0 counter"><?php foreach ($jumusaha as $data): ?>
          
				<?php echo $data->total ?> 
            <?php endforeach; ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
    <div class="card gradient-warning o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center">
            <div class="text-white i" data-feather="database"></div>
          </div>
          <div class="media-body"><span class="m-0 text-white">jumlah aset</span>
           <h4 class="mb-0 counter text-white"> <?php foreach ($jumaset as $data): ?>

          
			 RP. <?php echo  number_format($data->total,2,',','.') ?> 
			   
			   <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Jumlah hasil</span>
          <h4 class="mb-0 counter text-white"><?php foreach ($jumahasil as $data): ?>

            
			 RP. <?php echo  number_format($data->total,2,',','.') ?>   
			  <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Statistik koperasi Per Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div>

<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Info grafis Gerai koperasi Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="peta"></div>
    </div>
  </div>
</div>

<div class="col-lg-12 xl-100">
  <div class="row">

   


    






<!-- Plugins JS start-->
<!-- Plugins JS start-->


  <script>
// pie chart
var options8 = {
  chart: {
    width: 380,
    type: 'pie',
  },
  labels: ['Perempuan', 'Laki-laki'],
  series: [<?php foreach ($jk as $data): ?>
    <?php echo $data->totalwanita.","?> 
    <?php echo $data->totallaki?>
    <?php endforeach; ?>],
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 300
        },
        legend: {
          position: 'bottom'
        }
      }
    }],
    colors:['#fe80b2', '#06b5dd']
  }

  var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
    );

  chart8.render();

</script>

<script type="text/javascript">

  var mapOptions = {
             center: [-6.821018391805014, 107.94189545895269],
             zoom: 20
            }
            var peta = new L.map('peta', mapOptions);

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 11,
              id: 'mapbox/streets-v11',
              tileSize: 512,
              zoomOffset: -1,
              accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(peta);
            
        <?php foreach ($gerai as $data): ?>



          var marker = new L.Marker([<?php echo $data->lat?>, <?php echo $data->lng?>]);
          marker.bindPopup("<b><?php echo $data->nama?></b>.").openPopup();
          marker.addTo(peta);
        <?php endforeach; ?>
        
        
      </script>



<script src="<?=base_url();?>js/koperasi/dashboard.js" defer></script>
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>