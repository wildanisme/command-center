 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Bantuan</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Manage Bantuan</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
            <div class="row col-row-spacing">
               <div class="col-md-3">
                <a href="<?= base_url('manage_post/add') ?>" class="btn btn-block btn-primary"><i class="feather icon-plus"></i> Tambah</a>
            </div>
            <div class="col-md-9 inputselect-group">
                <fieldset class="form-group position-relative has-icon-left input-divider-left">
                    <form role="search" class="app-search hidden-xs mr-1">
                        <div class="input-group">
                            <input type="text" value="" name="s" class="round form-control" id="iconLeft3" placeholder="Cari Berita ...">
                            <div class="form-control-position">
                                <i class="feather icon-search"></i>
                            </div>

                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
        <!-- Basic example and Profile cards section start -->
        <div class="card">
            <div class="card-content">
                <div class="card-body">

                 <div class="pull-right">
                    <form role="search" class="app-search hidden-xs mr-1">
                        <input type="text" name="s" placeholder="Cari..." class="form-control" style="background:#eee;"> <a href="" class="active"></a>
                    </form>
                </div>

                <div class="col-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Pembuat</th>
                                <th>Tanggal Posting</th>
                                <th>Hits</th>
                                <th>Status</th>
                                <th width=70px>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $num = 0;
                            if (!empty($offset)) $num = $offset;
                            foreach ($query as $row) {
                                $num++;
                                echo"
                                <tr>
                                <td>$num</td>
                                <td>
                                <a target='_blank' href='".base_url()."berita/read/$row->title_slug' >$row->title</a></td>
                                <td>";
                                if ($row->category_name!="")
                                    echo"
                                <a target='_blank' href='".base_url()."berita/category/$row->category_slug' >
                                $row->category_name</a>";
                                else
                                    echo "::Internal::";
                                echo"
                                </td>
                                <td>$row->full_name</td>
                                <td>
                                ". date('d M Y',strtotime($row->date)) ." $row->time
                                </td>
                                <th>$row->hits</th>
                                <td>$row->post_status</td>
                                <td>
                                <a href='".base_url()."manage_post/edit/$row->post_id' class='btn-xs' title='Edit' data-toggle=\"tooltip\" data-original-title=\"Edit\">

                                <i class=\"fa fa-pencil text-inverse m-r-10\"></i> 
                                </a>
                                <a class='btn-xs' title='Delete'  onclick='delete_(\"$row->post_id\")' data-toggle=\"tooltip\" data-original-title=\"Close\">
                                <i class=\"fa fa-close text-danger\"></i>
                                </a>

                                </td>
                                </tr>
                                ";


                            }
                            if ($num==0){
                                echo"
                                <tr>
                                <td colspan=8 align=center>No data</td>
                                </tr>
                                ";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php

                echo"<div class='row'>
                <div class='col-md-12 pager'>";

                $CI =& get_instance();
                $CI->load->library('pagination');

                $config['base_url'] = base_url(). 'manage_post/index/';
                $config['total_rows'] = $total_rows;
                $config['per_page'] = $per_page;
                $config['attributes'] = array('class' => 'btn btn-primary btn-xm marginleft2px');
                $config['page_query_string']=TRUE;
                $CI->pagination->initialize($config);
                $link = $CI->pagination->create_links();
                $link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled marginleft2px' >", $link);
                $link = str_replace("</strong>", "</button>", $link);
                echo $link;

                ?>
            </div>
        </div>



    </div>
</div>
</div>



</div>
</div>
</div>
    <!-- END: Content-->