 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Post</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url();?>manage_post">Post</a>
                                </li>
                               

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">


                 <script type="text/javascript" src="<?php echo base_url()."asset" ;?>/tinymce4x/tinymce/tinymce.min.js"></script>
                 <link rel="stylesheet" type="text/css" href="<?php echo base_url()."asset" ;?>/tinymce4x/tinymce/plugins/codesample/prism.css">
                 <script src="<?php echo base_url()."asset" ;?>/tinymce4x/tinymce/plugins/codesample/prism.js"></script>
                 <script type="text/javascript">
                    tinymce.init({
                        selector: "#post_content",
                        theme: "modern",
                        plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                        "table contextmenu directionality emoticons paste textcolor filemanager codesample ",

                        ],
                        codesample_dialog_width: '400',
                        codesample_dialog_height: '400',
                        codesample_languages: [
                        {text: 'HTML/XML', value: 'markup'},
                        {text: 'JavaScript', value: 'javascript'},
                        {text: 'CSS', value: 'css'},
                        {text: 'PHP', value: 'php'},
                        {text: 'Ruby', value: 'ruby'},
                        {text: 'Python', value: 'python'},
                        {text: 'Java', value: 'java'},
                        {text: 'C', value: 'c'},
                        {text: 'C#', value: 'csharp'},
                        {text: 'C++', value: 'cpp'}
                        ],
                        image_advtab: true,
                        image_title: true, 
                        automatic_uploads: true,
                        theme_advanced_buttons1 : "openmanager",
                        file_browser_callback_types: 'file image media',

                        file_browser_callback: function(field_id, url, type, win, editor) { 

                // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
                var w = window,
                d = document,
                e = d.documentElement,
                g = d.getElementsByTagName('body')[0],
                x = w.innerWidth || e.clientWidth || g.clientWidth,
                y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                if(type == 'image') {           
                    type_id = "1";
                } else if(type == 'file') {           
                    type_id = "2";
                } else if(type == 'media') {           
                    type_id = "3";
                } else {           
                    type_id = "0";
                }

                var cmsURL = '<?php echo base_url()?>asset/tinymce/plugins/filemanager/dialog.php?type='+type_id+'&field_id='+field_id+'&relative_url=1&lang='+tinymce.settings.language + '&subfolder=' + tinymce.settings.subfolder;
                
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title: 'File Manager',
                    filetype: 'all',
                    classes: 'filemanager',
                    inline: "yes",
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });  
            },

            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code codesample"
        });
    </script>

    <?php if (!empty($message)) echo "
    <div class='alert alert-$message_type'>$message</div>";?>

    <form method="post" role="form" enctype="multipart/form-data">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
        <!-- Title and Publish Buttons -->
        <div class="row">

            <div class="col-12">
                <input value="<?php echo set_value('title'); ?>" type="text" onkeyup="getSlug()" class="form-control input-lg" value='' id="judul" name="title" placeholder="Masukkan Judul" />
            </div>
        </div>

        <div class="row">
            <br>

            <div class="col-sm-10" style='text-align:left'>
                <p ><?php echo base_url();?>berita/read/<label id='slug'></label></p>
                <input type="hidden" value="<?php echo set_value('title_slug'); ?>" name="title_slug" id="title_slug" value='' />
                <input type="hidden" value="<?php echo set_value('old_title_slug'); ?>" name="old_title_slug" id="old_title_slug" value='' />
            </div>

        </div>


        <div class="row">
            <div class="col-sm-12">
                <textarea class="form-control" rows="18" name="content" id="post_content"><?php echo set_value('content'); ?>
            </textarea>
        </div>

    </div>


    <div class="row">

      <div class="col-md-4">
        <div class="card">
            <div class="card-header">Pengaturan Publikasi

            </div>
            <div class="card-content " style="">
                <div class="card-body">

                    <div class="form-group">
                        <div class="controls">
                            <label>Tanggal Posting <span class="text-danger">*</span></label>
                            <input type="text" name='date' value="<?php echo set_value('date',date('Y-m-d')); ?>" class="form-control pickadate" value="<?php echo date('Y-m-d');?>" data-format="yyyy-mm-dd">
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="controls">
                            <label>Waktu <span class="text-danger">*</span></label>

                            <input type="text" value="<?php echo set_value('time',date('H:i')); ?>" name='time' value="" class="form-control pickname" data-autoclose="true" />
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <div class="col-md-4">
        <div class="card card-default">
            <div class="card-header">Gambar fitur

            </div>
            <div class="card-content" >
                <div class="card-body">
                    <div class="form-group">
                        <div class="controls">
                            <label>File foto <span class="text-danger">*</span></label>

                            <div class="fileinput fileinput-new dropify" data-provides="fileinput">
                                <?php
                                $picture="sys/f-img.png";
                                if (!empty($error)) echo "
                                <div class='alert alert-danger'>$error</div>";?>
                                <input type="file" name="userfile" accept="image/*" id="input-file-now-custom-3" class="dropify" data-height="160px" data-default-file="<?php echo base_url()."data/images/$picture";?>">


                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-3">
        <div class="card">
            <div class="card-header">
                Kategori
            </div>
            <div class="card-content" >
                <div class="card-body">

                    <div class="form-group">
                        <div class="controls">
                            <label>Kategori <span class="text-danger">*</span></label>
                            <select name="category_id" class="form-control select2" >
                                <option value=''>Pilih Kategori</option>
                                <?php
                                $selected_ = "selected";
                                foreach ($categories as $row) {
                                    $selected = "";
                                    echo "<option value='$row->category_id' $selected>$row->category_name</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <div class="controls">
                            <label>Status <span class="text-danger">*</span></label>
                            <select name="post_status" class="form-control">
                                <optgroup label="Post Status">
                                    <option value='Publish' selected>Publikasi</option>
                                    <option value='Draft'>Draf</option>

                                </optgroup>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
         <button class="btn btn-primary float-right mr-1 ml-1" type="submit">Simpan</button>
        <button class="btn btn-outline-light float-right">Batal</button>
       
    </div>

</form>


</div>






</div>
</div>
</div>
</div>
</div>
</div>
