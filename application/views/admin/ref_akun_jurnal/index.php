    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                      <div class="col-12">
                          <h2 class="content-header-title float-left mb-0">Akun Jurnal</h2>
                          <div class="breadcrumb-wrapper col-12">
                              <ol class="breadcrumb">
                                  <li class="breadcrumb-item"><a href="index.html">Home</a>
                                  </li>
                                  <li class="breadcrumb-item active"><a href="#">Akun Jurnal</a>
                                  </li>

                              </ol>
                          </div>
                      </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">


                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <table class="table data-list-view">
                            <thead>
                                <tr>

                                    <th>KODE AKUN</th>
                                    <th>NAMA AKUN</th>
                                    <th>JENIS</th>
                                    <th>SUB AKUN</th>
                                    <th>STATUS KUNCI</th>
                                    <th>DESKRIPSI</th>
                                    <th>STATUS</th>
                                    <th>OPSI</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($list as $key => $row) {?>
                                <tr>

                                  <td><span id="kode_akun_<?=$row->id_akun;?>"><?= $row->kode_akun ;?></span></td>
                                  <td><span id="nama_akun_<?=$row->id_akun;?>"><?= $row->nama_akun ;?></span></td>
                                  <td><span id="jenis_<?=$row->id_akun;?>"><?= $row->jenis ;?></span></td>
                                  <td><span id="id_induk_<?=$row->id_akun;?>" data-id="<?=$row->id_induk;?>"><?= $row->nama_akun_induk ;?></span></td>
                                  <td class="product-price">
                                    <?php if($row->terkunci=="Y"){?>
                                      <div class="badge badge-warning mr-1 mb-1">
                                        <i class="feather icon-lock"></i>
                                        <span id="terkunci_<?=$row->id_akun;?>" data-id="Y">Terkunci</span>
                                      </div>
                                  <?php } else {?>
                                      <div class="badge badge-success mr-1 mb-1">
                                        <span id="terkunci_<?=$row->id_akun;?>" data-id="N">Tidak</span>
                                      </div>
                                  <?php }?>
                                  </td>
                                  <td><span id="deskripsi_<?=$row->id_akun;?>"><?= $row->deskripsi ;?></span></td>
                                  <td>
                                      <div class="chip <?=($row->status=="active") ?  'chip-success':'chip-warning';?> ">
                                          <div class="chip-body">
                                              <div class="chip-text" id="status_<?=$row->id_akun;?>"><?=$row->status;?></div>
                                          </div>
                                      </div>
                                  </td>
                                <td class="product-action">
                                  <span class="" onclick="edit(<?=$row->id_akun;?>)"><i class="feather icon-edit"></i></span>
                                  <span class="" onclick="hapus(<?=$row->id_akun;?>)"><i class="feather icon-trash"></i></span>
                                </td>
                            </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- DataTable ends -->

                <!-- add new sidebar starts -->
                <div class="add-new-data-sidebar">
                    <div class="overlay-bg"></div>
                    <div class="add-new-data">
                      <form method="post">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                        <input type="hidden" name="id_akun" id="id_akun" value="" />
                        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                            <div>
                                <h4 class="text-uppercase">Akun Jurnal</h4>
                            </div>
                            <div class="hide-data-sidebar">
                                <i class="feather icon-x"></i>
                            </div>
                        </div>
                        <div class="data-items pb-1">
                            <div class="data-fields px-2 mt-1">
                                <div class="row">
                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-name">Kode Akun</label>
                                        <input type="text" class="form-control" id="kode_akun" name="kode_akun">
                                    </div>

                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-name">Nama Akun</label>
                                        <input type="text" class="form-control" id="nama_akun" name="nama_akun">
                                    </div>


                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-category"> Jenis </label>
                                        <select onchange="get_akun_induk()" class="form-control select2" id="jenis" name="jenis">
                                            <option value="pemasukan">Pemasukan</option>
                                            <option value="pengeluaran">Pengeluaran</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 data-field-col" id="row-induk">
                                        <label for="data-status">Sub akun dari</label>
                                        <select class="form-control select2" id="id_induk" name="id_induk">
                                          <option value="">-</option>
                                          <?php foreach ($dt_induk as $row) {
                                            echo "<option value='$row->id_akun'>$row->kode_akun - $row->nama_akun</option>";
                                          }
                                          ?>
                                        </select>
                                    </div>

                                     <div class="col-sm-12 data-field-col">
                                        <label for="data-name">Deskripsi</label>
                                        <textarea class="form-control" name="deskripsi" id="deskripsi"></textarea>
                                    </div>

                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-status">Terkunci</label>
                                        <select class="form-control select2" id="terkunci" name="terkunci">
                                            <option value="Y">Ya</option>
                                            <option value="N">Tidak</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 data-field-col">
                                        <label for="data-status">Status</label>
                                        <select class="form-control select2" id="status" name="status">
                                            <option value="active">Aktif</option>
                                            <option value="not active">Not Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="add-data-footer d-flex px-3 mt-1">
                            <div class="add-data-btn">
                                <button class="btn btn-primary">Simpan</button>
                            </div>

                        </div>
                      </form>
                    </div>
                </div>
                <!-- add new sidebar ends -->
            </section>
            <!-- Data list view end -->

        </div>
    </div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<script>

 var id_induk = "";
  function edit(id)
  {
    //alert(id);
    $("#id_akun").val(id);
    var kode_akun = $("#kode_akun_"+id).html();
    var nama_akun = $("#nama_akun_"+id).html();
    var jenis = $("#jenis_"+id).html();
    var deskripsi = $("#deskripsi_"+id).html();
    var terkunci = $("#terkunci_"+id).attr("data-id");
    id_induk = $("#id_induk_"+id).attr("data-id");
    //alert(terkunci);
    var status = $("#status_"+id).html();
    $("#nama_akun").val(nama_akun);
    $("#kode_akun").val(kode_akun);
    $("#deskripsi").val(deskripsi);
    $("#jenis").val(jenis).trigger("change");
    $("#status").val(status).trigger("change");
    $("#terkunci").val(terkunci).trigger("change");

    if(id_induk=="")
    {
      $("#row-induk").hide();
      $("#id_induk").val("").trigger("change");

    }
    else{
      $("#row-induk").show();
      //$("#id_induk").val(id_induk).trigger("change");
    }

    console.log(id_induk);
    $(".add-new-data").addClass("show");
    $(".overlay-bg").addClass("show");
  }
  function hapus(id)
  {
    //alert(id);
    swal({
      title: "Hapus Akun?",
      //icon: "info",
      buttons: true,
      dangerMode: false,
    })
    .then((isConfirm) => {
      if (isConfirm) {

        $.ajax({
            url :"<?php echo base_url("ref_akunjurnal/delete")?>",
            type:'post',
            data:{
              id:id,
              "<?=$this->security->get_csrf_token_name();?>" : "<?= $this->security->get_csrf_hash();?>",
            },
             success    : function(data){
                console.log(data);

                swal("Akun berhasil dihapus", {
                  icon: "success",
                });

                setTimeout(function() {
                  window.location.href = "<?=base_url();?>ref_akunjurnal";
                }, 500);
             },
                 error: function(xhr, status, error) {
                  //swal("Opps","Error","error");
                  console.log(xhr);
                }
        });

      }
    });
  }
  function tambah()
  {
    id_induk ="";
    $("#row-induk").show();
      $("#id_akun").val("");
      $("#kode_akun").val("");
      $("#nama_akun").val("");
      $("#deskripsi").val("");
      $("#id_induk").val("").trigger("change");
      $("#jenis").val("pemasukan").trigger("change");
      $("#terkunci").val("N").trigger("change");
      $("#status").val("active").trigger("change");
  }

  //get_akun_induk();
  function get_akun_induk(selected="")
  {
    var jenis = $("#jenis").val();
    $.ajax({
        url :"<?php echo base_url("ref_akunjurnal/get_akun_induk")?>/"+jenis,
        type:'get',
        dataType : "json",
        data:{},
         success    : function(data){
            //console.log(data.dt_induk);
            var dt_induk = data.dt_induk;

            var opt = "<option value=''>-</option>";
            for(i in dt_induk)
            {
              var sel = (id_induk==dt_induk[i].id_akun) ? "selected" : "";
              opt += "<option "+sel+" value='"+dt_induk[i].id_akun+"'>"+dt_induk[i].kode_akun+" - "+dt_induk[i].nama_akun+"</option>";
              //console.log(dt_induk[i]);
            }
            $("#id_induk").html(opt).trigger("change");
         },
             error: function(xhr, status, error) {
              //swal("Opps","Error","error");
              console.log(xhr);
            }
    });
  }
</script>
