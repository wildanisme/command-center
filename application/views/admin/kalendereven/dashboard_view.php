<?php 

$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';


?>
 <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kalender dengan Hari Minggu Berwarna Merah</title>
    <style>
        /* Gaya untuk kalender */
        .calendar {
            font-family: Arial, sans-serif;
            width: 400px;
            margin: 0 auto;
        }
        .calendar table {
            width: 100%;
            border-collapse: collapse;
        }
        .calendar th, .calendar td {
            border: 1px solid #ddd;
            text-align: center;
            padding: 12px;
            font-size: 20px;
        }
        .calendar th {
            background-color: #f2f2f2;
        }
        /* Gaya untuk hari Minggu */
        .sunday {
            color: red;
        }
    </style>
</head>
<body>
    <div class="calendar" id="calendar">
        <!-- Ini adalah tempat untuk menampilkan kalender -->
    </div>

    <script>
        // Fungsi untuk membuat kalender dinamis
        function buatKalender(tanggal) {
            var calendar = document.getElementById('calendar');
            calendar.innerHTML = '';

            var bulan = tanggal.getMonth();
            var tahun = tanggal.getFullYear();
            var hariIni = new Date();

            var namaBulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            var table = document.createElement('table');
            var thead = document.createElement('thead');
            var tbody = document.createElement('tbody');

            var row = document.createElement('tr');
            var prevButton = document.createElement('th');
            prevButton.innerHTML = '&lt;';
            prevButton.addEventListener('click', function() {
                buatKalender(new Date(tanggal.getFullYear(), tanggal.getMonth() - 1, 1));
            });
            row.appendChild(prevButton);

            var monthYear = document.createElement('th');
            monthYear.setAttribute('colspan', '5');
            monthYear.textContent = namaBulan[bulan] + ' ' + tahun;
            row.appendChild(monthYear);

            var nextButton = document.createElement('th');
            nextButton.innerHTML = '&gt;';
            nextButton.addEventListener('click', function() {
                buatKalender(new Date(tanggal.getFullYear(), tanggal.getMonth() + 1, 1));
            });
            row.appendChild(nextButton);

            thead.appendChild(row);

            // Membuat baris untuk nama hari
            var rowHari = document.createElement('tr');
            var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            for (var i = 0; i < 7; i++) {
                var th = document.createElement('th');
                th.textContent = hari[i];
                rowHari.appendChild(th);
            }
            thead.appendChild(rowHari);

            // Membuat sel-sel tanggal
            var tanggalAwal = new Date(tahun, bulan, 1);
            var tanggalAkhir = new Date(tahun, bulan + 1, 0);
            var tanggalIterasi = new Date(tanggalAwal);

            while (tanggalIterasi <= tanggalAkhir) {
                var tr = document.createElement('tr');
                for (var i = 0; i < 7; i++) {
                    var td = document.createElement('td');
                    td.textContent = tanggalIterasi.getDate();
                    if (tanggalIterasi.getMonth() !== bulan) {
                        td.classList.add('disabled');
                    }
                    if (tanggalIterasi.getDay() === 0) {
                        // Tambahkan kelas "sunday" untuk hari Minggu
                        td.classList.add('sunday');
                    }
                    if (tanggalIterasi.toDateString() === hariIni.toDateString()) {
                        td.classList.add('today');
                    }
                    tr.appendChild(td);
                    tanggalIterasi.setDate(tanggalIterasi.getDate() + 1);
                }
                tbody.appendChild(tr);
            }

            table.appendChild(thead);
            table.appendChild(tbody);
            calendar.appendChild(table);
        }

        // Memanggil fungsi untuk pertama kali dengan tanggal saat ini
        buatKalender(new Date());
    </script>
</body>
</script>



<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>