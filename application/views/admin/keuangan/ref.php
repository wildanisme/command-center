 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Ref. Keuangan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Keuangan</li>
            <li class="breadcrumb-item active">Referensi  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">

    <div class="row">
      <!-- awal -->


      <!-- Zero Configuration  Starts-->
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Ref. Keuangan</h5>
            <button class="btn btn-primary pull-right" type="button" onclick="addRef();">Tambah Referensi</button>
          </div>
          <div class="card-body">
            <?php echo flashdata_notif();?>
            <div class="table-responsive">
              <table class="display" id="basic-1">
                <thead>
                  <tr>
                    <th>Kode Rekening</th>
                    <th>Uraian</th>
                    <th>Ket.</th>
                    <th>Opsi</th>

                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($query as $row): ?>

                    <tr>
                      <td><?=$row->kode_rekening?></td>
                      <td><?=$row->uraian?></td>
                      <td><?=$row->ket?></td>
                      <td><a href="javascript:editRef('<?=$row->id_ref_keuangan?>')" class="btn btn-outline-warning btn-sm"> <span class="icon-pencil"></span> Edit</a><a href="javascript:deleteRef('<?=$row->id_ref_keuangan?>')" class="btn btn-outline-danger btn-sm m-l-10"> <span class="icon-trash"></span> Hapus</a></td>

                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Zero Configuration  Ends-->

      <!-- batas -->
    </div>
  </div>

</div>

<div class="modal fade" id="modalReferensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Referensi</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form id="formRef" class="form-horizontal" method="POST">
        <div class="modal-body">

          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
          <input type="hidden" name="save_method" value="add">
          <input type="hidden" name="id_ref_keuangan" value="">
          <fieldset>


            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="kode_rekening">Kode Rekening</label>  
              <div class="col-lg-12">
                <input id="kode_rekening" name="kode_rekening" type="text" placeholder="masukan kode rekeing" class="form-control ">
                <input name="kode_rekening_old" type="hidden">

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">Uraian</label>  
              <div class="col-lg-12">
                <input id="urairan" name="uraian" type="text" placeholder="Uraian" class="form-control btn-square input-md">

              </div>
            </div>

            <!-- Textarea -->
            <div class="form-group row mb-0">
              <label class="col-lg-12 control-label text-lg-left" for="ket">Keterangan</label>
              <div class="col-lg-12">                     
                <textarea class="form-control btn-square" id="ket" name="ket"></textarea>
              </div>
            </div>

          </fieldset>

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" data-dismiss="modal">Batal</button>
          <button class="btn btn-secondary" type="submit">Simpan</button>
        </div>

      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
 var save_method;

 function addRef() {
   save_method = 'add';
   $('#formRef')[0].reset();
   $('#modalReferensi').modal('show');
   $('.modal-title').text('Tambah Referensi');
   $('[name="save_method"]').val(save_method);
 }

 function editRef(id) {
   save_method = 'update';
   $('#formRef')[0].reset();
   $.ajax({
     url: "<?= base_url() . $this->router->fetch_class(). '/fetch_ref/' ?>" + id,
     type: "GET",
     dataType: "JSON",
     success: function(data) {
       $('[name="save_method"]').val(save_method);
       $('[name="id_ref_keuangan"]').val(id);
       $('[name="kode_rekening"]').val(data.kode_rekening);
       $('[name="kode_rekening_old"]').val(data.kode_rekening);
       $('[name="uraian"]').val(data.uraian);
       $('[name="ket"]').val(data.ket);

       $('#modalReferensi').modal('show');
       $('.modal-title').text('Ubah Referensi');

     },
     error: function(jqXHR, textStatus, errorThrown) {
       alert("Gagal mendapatkan data");
     }
   });

 }

 function deleteRef(id) {

  swal({
    title: "Hapus Data?",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        url: "<?= base_url() . $this->router->fetch_class(). '/delete_ref/' ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
          $('#modalReferensi').modal('hide');
          swal("Berhasil", "Data Berhasil Dihapus!", "success");
          location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert('Error deleting data');
        }
      });
    }
  });
}

</script>