<?php 
  $max1 = 0;
  $max2 = array();
  $max3 = array();
  foreach ($query as $row) {
    $keuangan[$row['kode_rekening']] = $row;
    $e = explode('.', $row['kode_rekening']);
    if (@$e[0] > $max1) 				$max1 				= $e[0];
    if (@$e[1] > @$max2[$e[0]]) 		$max2[$e[0]] 		= $e[1];
    if (@$e[2] > @$max3[$e[0]][$e[1]])	$max3[$e[0]][$e[1]]	= $e[2];
  }
  $status_apbd 				= (date("m-d") >= "10-23") ? "Perubahan" : "Murni";

  $apbd_by_status			= ($status_apbd=="Murni") ? "apbd_murni" : "apbd_perubahan";
  $realisasi_by_status 		= ($status_apbd=="Murni") ? "anggaran_realisasi_murni" : "anggaran_realisasi_perubahan";

  $belanja_langsung       = (array_key_exists('5.2.',$keuangan)) ? rupiah($keuangan['5.2.'][$apbd_by_status]) : "Tidak ada data.";
  $belanja_tdk_langsung   = (array_key_exists('5.1.',$keuangan)) ? rupiah($keuangan['5.1.'][$apbd_by_status]) : "Tidak ada data.";
  $pengeluaran_pembiayaan = (array_key_exists('6.2.',$keuangan)) ? rupiah($keuangan['6.2.'][$apbd_by_status]) : "Tidak ada data.";
  $anggaran_belanja       = (array_key_exists('4.',$keuangan)) ? rupiah($keuangan['4.'][$apbd_by_status]) : "Tidak ada data.";

  $tabel_1 = (array_key_exists('4.',$keuangan)) ? true : false;
  $tabel_2 = (array_key_exists('5.',$keuangan)) ? true : false;
  $tabel_3 = (array_key_exists('6.',$keuangan)) ? true : false;

  $selisih_penerimaan       = (array_key_exists('6.1.',$keuangan)) ? rupiah(abs($keuangan['6.1.']['anggaran_realisasi_murni']-$keuangan['6.1.']['apbd_murni'])/1000000000)." Miliar" : "Tidak ada data.";
  $persentase_penerimaan    = (array_key_exists('6.1.',$keuangan) AND ($keuangan['6.1.']['anggaran_realisasi_murni']>0 AND $keuangan['6.1.']['apbd_murni']>0)) ? dot($keuangan['6.1.']['anggaran_realisasi_murni']/$keuangan['6.1.']['apbd_murni']*100,2)."%" : "";

  $selisih_pengeluaran      = (array_key_exists('6.2.',$keuangan)) ? rupiah(abs($keuangan['6.2.']['anggaran_realisasi_murni']-$keuangan['6.2.']['apbd_murni'])/1000000000)." Miliar" : "Tidak ada data.";
  $persentase_pengeluaran   = (array_key_exists('6.2.',$keuangan) AND ($keuangan['6.2.']['anggaran_realisasi_murni']>0 AND $keuangan['6.2.']['apbd_murni']>0)) ? dot($keuangan['6.2.']['anggaran_realisasi_murni']/$keuangan['6.2.']['apbd_murni']*100,2)."%" : "";

  $array_label = array('primary','secondary','success','danger','info','light','dark','warning');
?>
<!-- awal -->

<!-- <pre><?php print_r($max1)?><?php print_r($max2)?><?php print_r($max3)?></pre> -->

<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-primary o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center"><i data-feather="database"></i></div>
        <div class="media-body"><span class="m-0 text-white">Belanja Langsung</span>
          <h4 class="mb-0 "><?=$belanja_langsung?></h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-secondary o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
        <div class="media-body"><span class="m-0">Belanja Tdk Langsung</span>
          <h4 class="mb-0 "><?=$belanja_tdk_langsung?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-warning o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="message-circle"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Pengeluaran Pembiayaan</span>
          <h4 class="mb-0  text-white"><?=$pengeluaran_pembiayaan?></h4><i class="icon-bg" data-feather="message-circle"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="user-plus"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Total Anggaran Belanja</span>
          <h4 class="mb-0  text-white"><?=$anggaran_belanja?></h4><i class="icon-bg" data-feather="user-plus"></i>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-sm-12 col-xl-4 ">
  <div class="card">
    <div class="card-header">
      <h6>Anggaran Belanja Langsung <span class="badge badge-pill pill-badge-primary f-14 f-w-600 pull-right"><?=$tahun?></span></h6>
    </div>
    <div class="card-body apex-chart p-0">
      <div id="keuanganchart1"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-4 ">
  <div class="card">
    <div class="card-header">
      <h6>Anggaran Belanja Tdk Langsung <span class="badge badge-pill pill-badge-secondary f-14 f-w-600 pull-right"><?=$tahun?></span></h6>
    </div>
    <div class="card-body apex-chart p-0">
      <div id="keuanganchart2"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-4 ">
  <div class="card">
    <div class="card-header">
      <h6>Anggaran Pendapatan <span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right"><?=$tahun?></span></h6>
    </div>
    <div class="card-body apex-chart p-0">
      <div id="keuanganchart3"></div>
    </div>
  </div>
</div>

<div class="col-xl-12  box-col-12">
  <div class="card">
    <div class="card-header pb-0 d-flex" style="justify-content: space-between;">
      <h5>Perbandingan Anggaran dan Realisasi Penerimaan Pembiayaan <span class="badge badge-pill pill-badge-danger f-14 f-w-600"><?=$tahun?></span></h5>
      <ul class="creative-dots">
        <li class="bg-primary big-dot"></li>
        <li class="bg-secondary semi-big-dot"></li>
        <li class="bg-warning medium-dot"></li>
        <li class="bg-info semi-medium-dot"></li>
        <li class="bg-secondary semi-small-dot"></li>
        <li class="bg-primary small-dot"></li>
      </ul>
      <div class="pull-right text-right">
        <h5 class="mb-2"><?=$persentase_penerimaan?></h5>
        <h6 class="f-w-700 mb-0"><?=$selisih_penerimaan?></h6>
      </div>
    </div>
    <div class="card-body pt-0">
      <div class="row">
        <div class="col-xl-4 apex-chart">
          <div id="keuanganchart4"></div>
        </div>
        <div class="col-xl-8 apex-chart">
          <div id="keuanganchart5"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-xl-12  box-col-12">
  <div class="card">
    <div class="card-header pb-0 d-flex" style="justify-content: space-between;">
      <h5>Perbandingan Anggaran dan Realisasi Pengeluaran Pembiayaan <span class="badge badge-pill pill-badge-info f-14 f-w-600"><?=$tahun?></span></h5>
      <ul class="creative-dots">
        <li class="bg-primary big-dot"></li>
        <li class="bg-secondary semi-big-dot"></li>
        <li class="bg-warning medium-dot"></li>
        <li class="bg-info semi-medium-dot"></li>
        <li class="bg-secondary semi-small-dot"></li>
        <li class="bg-primary small-dot"></li>
      </ul>
      <div class="pull-right text-right">
        <h5 class="mb-2"><?=$persentase_pengeluaran?></h5>
        <h6 class="f-w-700 mb-0"><?=$selisih_pengeluaran?></h6>
      </div>
    </div>
    <div class="card-body pt-0">
      <div class="row">
        <div class="col-xl-8 apex-chart">
          <div id="keuanganchart7"></div>
        </div>
        <div class="col-xl-4 apex-chart">
          <div id="keuanganchart6"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php for ($i=4; $i <= 6; $i++) : ?>
<div class="col-xl-4  box-col-12 display-flex">
  <div class="card">
    <div class="card-header no-border">
      <h5><?=$i?>. <?=($tabel_1)?$keuangan[$i.'.']['uraian']:"Tidak ada data."?><span class="badge badge-pill f-14 pill-badge-<?=$array_label[$i%3]?> pull-right"><?=($tabel_1)?dot($keuangan[$i.'.']['anggaran_realisasi_murni']/$keuangan[$i.'.']['apbd_murni']*100,2)."%":""?></span></h5>
      <ul class="creative-dots">
        <li class="bg-primary big-dot"></li>
        <li class="bg-secondary semi-big-dot"></li>
        <li class="bg-warning medium-dot"></li>
        <li class="bg-info semi-medium-dot"></li>
        <li class="bg-secondary semi-small-dot"></li>
        <li class="bg-primary small-dot"></li>
      </ul>
    </div>
    <div class="card-body p-0">
      <div class="sales-product-table crypto-table-market table-responsive">
        <table class="table table-bordernone">
          <tbody>
            <?php if ($tabel_1): $no=1; while (array_key_exists("{$i}.{$no}.",$keuangan)) : $label = $array_label[$no-1%3]; ?>
              <tr>
                <td class="font-<?=$label?> f-w-700"><?=$i?>.<?=$no?>.</td>
                <td><span class="f-w-700"><?=$keuangan[$i.'.'.$no.'.']['uraian']?></span></td>
                <td></td>
                <td><span class="badge badge-pill f-14 font-<?=$label?>"><?=($keuangan[$i.'.'.$no.'.']['anggaran_realisasi_murni']>0 AND $keuangan[$i.'.'.$no.'.']['apbd_murni']>0) ? dot($keuangan[$i.'.'.$no.'.']['anggaran_realisasi_murni']/$keuangan[$i.'.'.$no.'.']['apbd_murni']*100,2)."%" : ""?></span></td>
              </tr>
            <?php $no++; endwhile; endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php endfor ?>

<div class="col-sm-12">
  <div class="card">
    <div class="card-header">
      <h5>Tabel Realisasi Anggaran dan Pendapatan Belanja</h5>
    </div>
    <div class="card-body">
      <div class="dt-ext table-responsive">
        <table class="display datatables">
          <thead>
            <tr>
              <th>Kode Rekening</th>
              <th>Uraian</th>
              <th class="text-right">Jumlah Anggaran</th>
              <th class="text-right">Realisasi</th>
              <th class="text-right">% Murni</th>
              <?php if ($status_apbd == "Perubahan"): ?>
              <th class="text-right">Jumlah Anggaran</th>
              <th class="text-right">Realisasi</th>
              <th class="text-right">% Perubahan</th>
              <?php endif ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($query as $row): ?>
              <tr>
                <td><?=$row['kode_rekening']?></td>
                <td><?=$row['uraian']?></td>
                <td class="text-right"><?=rupiah($row['apbd_murni'])?></td>
                <td class="text-right"><?=rupiah($row['anggaran_realisasi_murni'])?></td>
                <td class="text-right"><?=($row['anggaran_realisasi_murni']>0 AND $row['apbd_murni']>0)?dot($row['anggaran_realisasi_murni']/$row['apbd_murni']*100,2):""?></td>
	            <?php if ($status_apbd == "Perubahan"): ?>
                <td class="text-right"><?=rupiah($row['apbd_perubahan'])?></td>
                <td class="text-right"><?=rupiah($row['anggaran_realisasi_perubahan'])?></td>
                <td class="text-right"><?=($row['anggaran_realisasi_perubahan']>0 AND $row['apbd_perubahan']>0)?dot($row['anggaran_realisasi_perubahan']/$row['apbd_perubahan']*100,2):""?></td>
	            <?php endif ?>
              </tr>
            <?php endforeach ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Kode Rekening</th>
              <th>Uraian</th>
              <th class="text-right">Jumlah Anggaran</th>
              <th class="text-right">Realisasi</th>
              <th class="text-right">% Murni</th>
              <?php if ($status_apbd == "Perubahan"): ?>
              <th class="text-right">Jumlah Anggaran</th>
              <th class="text-right">Realisasi</th>
              <th class="text-right">% Perubahan</th>
              <?php endif ?>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<div id="footer-mini" style="position: fixed; margin-left: -120px; bottom: 0; background-color: #fff; box-shadow: 0 0 15px 3px rgba(176,185,189,0.3);">
  <table class="display compact datatables-mini">
    <thead>
      <tr>
        <th>Kode Rekening</th>
        <th>Uraian</th>
        <th class="text-right">Jumlah Anggaran</th>
        <th class="text-right">Realisasi</th>
        <th class="text-right">% Murni</th>
        <?php if ($status_apbd == "Perubahan"): ?>
        <th class="text-right">Jumlah Anggaran</th>
        <th class="text-right">Realisasi</th>
        <th class="text-right">% Perubahan</th>
        <?php endif ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($query as $row): ?>
        <tr>
          <td><?=$row['kode_rekening']?></td>
          <td><?=$row['uraian']?></td>
          <td class="text-right"><?=rupiah($row['apbd_murni'])?></td>
          <td class="text-right"><?=rupiah($row['anggaran_realisasi_murni'])?></td>
          <td class="text-right"><?=($row['anggaran_realisasi_murni']>0 AND $row['apbd_murni']>0)?dot($row['anggaran_realisasi_murni']/$row['apbd_murni']*100,2):""?></td>
        <?php if ($status_apbd == "Perubahan"): ?>
          <td class="text-right"><?=rupiah($row['apbd_perubahan'])?></td>
          <td class="text-right"><?=rupiah($row['anggaran_realisasi_perubahan'])?></td>
          <td class="text-right"><?=($row['anggaran_realisasi_perubahan']>0 AND $row['apbd_perubahan']>0)?dot($row['anggaran_realisasi_perubahan']/$row['apbd_perubahan']*100,2):""?></td>
        <?php endif ?>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>



<!-- batas -->

<script type="text/javascript">
  <?php 
    if (@$max3[5][2]>0) {
      for ($no=1; $no <= $max3[5][2]; $no++) { 
      	if (isset($keuangan['5.2.'.$no.'.']['uraian'])) {
	        $labels1[] = $keuangan['5.2.'.$no.'.']['uraian'];
	        $series1[] = round($keuangan['5.2.'.$no.'.']['apbd_murni']/1000000000,2);
      	}
      }
    } elseif (array_key_exists('5.2.',$keuangan)) {
      $labels1[] = $keuangan['5.2.']['uraian'];
      $series1[] = round($keuangan['5.2.']['apbd_murni']/1000000000,2);
    }
  ?>
  var labels1 = <?=json_encode($labels1)?>;
  var series1 = <?=json_encode($series1)?>;

  <?php 
    if (@$max3[5][1]>0) {
      for ($no=1; $no <= $max3[5][1]; $no++) { 
      	if (isset($keuangan['5.1.'.$no.'.']['uraian'])) {
	        $labels2[] = $keuangan['5.1.'.$no.'.']['uraian'];
	        $series2[] = round($keuangan['5.1.'.$no.'.']['apbd_murni']/1000000000,2);
      	}
      }
    } elseif (array_key_exists('5.1.',$keuangan)) {
      $labels2[] = $keuangan['5.1.']['uraian'];
      $series2[] = round($keuangan['5.1.']['apbd_murni']/1000000000,2);
    }
  ?>
  var labels2 = <?=json_encode($labels2)?>;
  var series2 = <?=json_encode($series2)?>;

  <?php 
    if (@count($max3[4])>0) {
      for ($no=1; $no <= $max2[4]; $no++) { 
      	for ($no2=1; $no2 <= $max3[4][$no]; $no2++) { 
      		if (isset($keuangan['4.'.$no.'.'.$no2.'.']['uraian'])) {
		        $labels3[] = $keuangan['4.'.$no.'.'.$no2.'.']['uraian'];
          		$series3[] = round($keuangan['4.'.$no.'.'.$no2.'.']['apbd_murni']/1000000000,2);
	      	}
	    }
      }
    } elseif (@$max2[4]>0) {
      for ($no=1; $no <= $max2[4]; $no++) { 
      	if (isset($keuangan['4.'.$no.'.']['uraian'])) {
	        $labels3[] = $keuangan['4.'.$no.'.']['uraian'];
        	$series3[] = round($keuangan['4.'.$no.'.']['apbd_murni']/1000000000,2);
      	}
      }
    }
  ?>
  var labels3 = <?=json_encode($labels3)?>;
  var series3 = <?=json_encode($series3)?>;
</script>

<script type="text/javascript">
  <?php 
    if (array_key_exists('6.1.',$keuangan)) {
      $labels4 = array('Anggaran','Realisasi');
      $series4 = array(round($keuangan['6.1.']['apbd_murni']/1000000000,2),round($keuangan['6.1.']['anggaran_realisasi_murni']/1000000000,2));
    }
  ?>
  var labels4 = <?=json_encode($labels4)?>;
  var series4 = <?=json_encode($series4)?>;
</script>

<script type="text/javascript">
  <?php 
    if (array_key_exists('6.2.',$keuangan)) {
      $labels6 = array('Anggaran','Realisasi');
      $series6 = array(round($keuangan['6.2.']['apbd_murni']/1000000000,2),round($keuangan['6.2.']['anggaran_realisasi_murni']/1000000000,2));
    }
  ?>
  var labels6 = <?=json_encode($labels6)?>;
  var series6 = <?=json_encode($series6)?>;
</script>

<script src="<?=base_url();?>js/keuangan/dashboard.js" defer></script>
<!-- <script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script> -->