 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Dashboard Keuangan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Keuangan</li>
            <li class="breadcrumb-item active">Dashboard  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-primary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              <div class="media-body"><span class="m-0 text-white">5.2. Belanja Langsung</span>
                <h4 class="mb-0 ">951.886.476.006</h4><i class="icon-bg" data-feather="database"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
              <div class="media-body"><span class="m-0">5.1. Belanja Tdk Langsung</span>
                <h4 class="mb-0 ">1.737.647.406.998</h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-warning o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="message-circle"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">6.2. Pengeluaran Pembiayaan</span>
                <h4 class="mb-0  text-white">54.356.739.319</h4><i class="icon-bg" data-feather="message-circle"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-info o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="user-plus"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">4. Total Anggaran Belanja</span>
                <h4 class="mb-0  text-white">2.743.890.622.322</h4><i class="icon-bg" data-feather="user-plus"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-xl-4 xl-100">
        <div class="card">
          <div class="card-header">
            <h6>Anggaran Belanja Langsung <span class="badge badge-pill pill-badge-primary f-14 f-w-600 pull-right">2021</span></h6>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="keuanganchart1"></div>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-xl-4 xl-100">
        <div class="card">
          <div class="card-header">
            <h6>Anggaran Belanja Tdk Langsung <span class="badge badge-pill pill-badge-secondary f-14 f-w-600 pull-right">2021</span></h6>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="keuanganchart2"></div>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-xl-4 xl-100">
        <div class="card">
          <div class="card-header">
            <h6>Anggaran Pendapatan <span class="badge badge-pill pill-badge-success f-14 f-w-600 pull-right">2021</span></h6>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="keuanganchart3"></div>
          </div>
        </div>
      </div>

      <div class="col-xl-12 xl-100 box-col-12">
        <div class="card">
          <div class="card-header pb-0 d-flex" style="justify-content: space-between;">
            <h5>Perbandingan Anggaran dan Realisasi Penerimaan Pembiayaan <span class="badge badge-pill pill-badge-danger f-14 f-w-600">2021</span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <div class="pull-right text-right">
              <h5 class="mb-2">893.9%</h5>
              <h6 class="f-w-700 mb-0">131,74 Miliar</h6>
            </div>
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-xl-4 apex-chart">
                <div id="keuanganchart4"></div>
              </div>
              <div class="col-xl-8 apex-chart">
                <div id="keuanganchart5"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-12 xl-100 box-col-12">
        <div class="card">
          <div class="card-header pb-0 d-flex" style="justify-content: space-between;">
            <h5>Perbandingan Anggaran dan Realisasi Pengeluaran Pembiayaan <span class="badge badge-pill pill-badge-info f-14 f-w-600">2021</span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <div class="pull-right text-right">
              <h5 class="mb-2">893.9%</h5>
              <h6 class="f-w-700 mb-0">131,74 Miliar</h6>
            </div>
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-xl-8 apex-chart">
                <div id="keuanganchart7"></div>
              </div>
              <div class="col-xl-4 apex-chart">
                <div id="keuanganchart6"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-4 xl-100 box-col-12 display-flex">
        <div class="card">
          <div class="card-header no-border">
            <h5>4. Pendapatan Daerah<span class="badge badge-pill f-14 pill-badge-success pull-right">22,89%</span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
          </div>
          <div class="card-body p-0">
            <div class="sales-product-table crypto-table-market table-responsive">
              <table class="table table-bordernone">
                <tbody>
                  <tr>
                    <td class="font-primary f-w-700">4.1.</td>
                    <td><span class="f-w-700">Pendapatan Asli Daerah</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-primary">24,19%</span></td>
                  </tr>
                  <tr>
                    <td class="font-secondary f-w-700">4.2.</td>
                    <td><span class="f-w-700">Dana Perimbangan</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-secondary">30,22%</span></td>
                  </tr>
                  <tr>
                    <td class="font-success f-w-700">4.3.</td>
                    <td><span class="f-w-700">Lain-lain Pendapatan Daerah yang Sah</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-success">3,74%</span></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 xl-100 box-col-12 display-flex">
        <div class="card">
          <div class="card-header no-border">
            <h5>5. Belanja Daerah<span class="badge badge-pill f-14 pill-badge-danger pull-right">12,14%</span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
          </div>
          <div class="card-body p-0">
            <div class="sales-product-table crypto-table-market table-responsive">
              <table class="table table-bordernone">
                <tbody>
                  <tr>
                    <td class="font-primary f-w-700">5.1.</td>
                    <td><span class="f-w-700">Belanja Tidak Langsung</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-primary">13,09%</span></td>
                  </tr>
                  <tr>
                    <td class="font-secondary f-w-700">5.2.</td>
                    <td><span class="f-w-700">Belanja Langsung</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-secondary">10,33%</span></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-4 xl-100 box-col-12 display-flex">
        <div class="card">
          <div class="card-header no-border">
            <h5>6. Pembiayaan Daerah<span class="badge badge-pill f-14 pill-badge-primary pull-right">584,47%</span></h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
          </div>
          <div class="card-body p-0">
            <div class="sales-product-table crypto-table-market table-responsive">
              <table class="table table-bordernone">
                <tbody>
                  <tr>
                    <td class="font-primary f-w-700">6.1.</td>
                    <td><span class="f-w-700">Penerimaan Pembiayaan Daerah</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-primary">564,65%</span></td>
                  </tr>
                  <tr>
                    <td class="font-secondary f-w-700">6.2.</td>
                    <td><span class="f-w-700">Pengeluaran Pembiayaan Daerah</span></td>
                    <td></td>
                    <td><span class="badge badge-pill f-14 font-secondary">487,80%</span></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="card">
          <div class="card-header">
            <h5>Tabel Realisasi Anggaran dan Pendapatan Belanja</h5>
          </div>
          <div class="card-body">
            <div class="dt-ext table-responsive">
              <table class="display" id="export-button">
                <thead>
                  <tr>
                    <th>Kode Rekening</th>
                    <th>Uraian</th>
                    <th>Jumlah Anggaran</th>
                    <th>Realisasi</th>
                    <th>%</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>4.</td>
                    <td>PENDAPATAN DAERAH</td>
                    <td>2786788212742,17</td>
                    <td>637822024653,55</td>
                    <td>22,89</td>
                  </tr>
                  <tr>
                    <td>4.1.</td>
                    <td>PENDAPATAN ASLI DAERAH</td>
                    <td>474906593495,07</td>
                    <td>114902837935,55</td>
                    <td>24,19</td>
                  </tr>
                  <tr>
                    <td>4.1.1.</td>
                    <td>HASIL PAJAK DAERAH</td>
                    <td>194759747254</td>
                    <td>39256622266</td>
                    <td>20,15</td>
                  </tr>
                  <tr>
                    <td>4.1.1.01.</td>
                    <td>Pajak Hotel</td>
                    <td>3815326128</td>
                    <td>592336936</td>
                    <td>15,52</td>
                  </tr>
                  <tr>
                    <td>4.1.1.01.004.</td>
                    <td>Hotel Bintang Tiga</td>
                    <td>2014200116</td>
                    <td>274445938</td>
                    <td>13,63</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <th>Kode Rekening</th>
                    <th>Uraian</th>
                    <th>Jumlah Anggaran</th>
                    <th>Realisasi</th>
                    <th>%</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>



      <!-- batas -->
    </div>
  </div>
</div>


<script src="<?=base_url();?>js/keuangan/dashboard.js" defer></script>