 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Sumber Data Keuangan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Keuangan</li>
            <li class="breadcrumb-item active">Referensi  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->


      <!-- Zero Configuration  Starts-->
      <div class="col-sm-12">
       <div class="card">
        <div class="card-header">
          <h5 class="pull-left">Sumber Data</h5>
        </div>
        <div class="card-body">
          <?php echo flashdata_notif();?>
          <div class="tabbed-card">
            <ul class="nav nav-pills nav-primary" id="pills-clrtab1" role="tablist">

              <li class="nav-item"><a class="nav-link active" id="top-perencanaan-tab" data-toggle="tab" href="#top-perencanaan" role="tab" aria-controls="top-perencanaan" aria-selected="false">Penganggaran</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link" id="top-realisasi-tab" data-toggle="tab" href="#top-realisasi" role="tab" aria-controls="top-realisasi" aria-selected="false">Realisasi</a>
                <div class="material-border"></div>
              </li>
              <li class="nav-item"><a class="nav-link " id="api-top-tab" data-toggle="tab" href="#top-api" role="tab" aria-controls="top-profile" aria-selected="true">By API</a>
                <div class="material-border"></div>
              </li>

            </ul>
            <div class="tab-content" id="top-tabContent">
              <!--perencanaan -->
              <div class="tab-pane active show" id="top-perencanaan" role="tabpanel" aria-labelledby="top-perencanaan-tab">
                <a href="<?=base_url('keuangan/sumber_perencanaan_add');?>" class="btn btn-primary"><span class="fa fa-plus"></span> Input Data</a>

                <a href="#" class="btn btn-success "> <span class="fa fa-download"></span> Import Excel</a>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
                      <div class="card-header">
                        <h5>Penganggaran</h5>
                      </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="display datatables">
                            <thead>
                              <tr>
                                <th>Kode Rekening</th>
                                <th>Uraian</th>
                                <th>Tahun</th>
                                <th>APBD Murni</th>
                                <th>APBD Perubahan</th>
                                <th>Opsi</th>

                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($query as $row): ?>
                                <tr>
                                  <td><?=$row->kode_rekening?></td>
                                  <td><?=$row->uraian?></td>
                                  <td><?=$row->tahun?></td>
                                  <td class="text-right"><?=rupiah($row->apbd_murni)?></td>
                                  <td class="text-right"><?=rupiah($row->apbd_perubahan)?></td>
                                  <td><a href="javascript:editRef('<?=$row->id_keuangan?>')" class="btn btn-outline-warning btn-sm"> <span class="icon-pencil"></span> Edit</a><a href="javascript:deleteRef('<?=$row->id_keuangan?>')" class="btn btn-outline-danger btn-sm m-l-10"> <span class="icon-trash"></span> Hapus</a>
                                  </td>

                                </tr>
                              <?php endforeach ?>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>

              </div>

              <!--end perencanaan-->

              <!-- realisasi -->
              <div class="tab-pane fade  " id="top-realisasi" role="tabpanel" aria-labelledby="realisasi-top-tab">
               <a href="<?=base_url('keuangan/sumber_realisasi_add');?>" class="btn btn-primary"><span class="fa fa-plus"></span> Input Data</a>

               <a href="#" class="btn btn-success "> <span class="fa fa-download"></span> Import Excel</a>

               <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h5>Realisasi</h5>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="display datatables">
                          <thead>
                            <tr>
                              <th>Kode Rekening</th>
                              <th>Uraian</th>
                              <th>Tahun</th>
                              <th>Anggaran Realisasi Murni</th>
                              <th>%</th>
                              <th>Anggaran Realisasi Perubahan</th>
                              <th>%</th>
                              <th>Opsi</th>

                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($query2 as $row): ?>
                              <tr>
                                <td><?=$row->kode_rekening?></td>
                                <td><?=$row->uraian?></td>
                                <td><?=$row->tahun?></td>
                                <td class="text-right"><?=rupiah($row->anggaran_realisasi_murni)?></td>
                                <td class="text-right"><?=($row->apbd_murni>0) ? dot($row->anggaran_realisasi_murni/$row->apbd_murni*100,2) : 'Belum ada realisasi.'?></td>
                                <td class="text-right"><?=rupiah($row->anggaran_realisasi_perubahan)?></td>
                                <td class="text-right"><?=($row->apbd_perubahan>0) ? dot($row->anggaran_realisasi_perubahan/$row->apbd_perubahan*100,2) : 'Belum ada realisasi.'?></td>

                                <td><a href="#row-detail-realisasi" onclick="detailRef('<?=$row->id_keuangan?>')" class="btn btn-outline-primary btn-sm"> <span class="icon-list"></span> Detail</a>
                                </td>

                              </tr>
                            <?php endforeach ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h5>Detail Realisasi</h5>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table class="display datatables" id="table-detail-realisasi">
                          <thead>
                            <tr>
                              <th>No.</th>
                              <th>Tanggal</th>
                              <th>Jenis</th>
                              <th>Anggaran Realisasi</th>
                              <th>Opsi</th>

                            </tr>
                          </thead>
                          <tbody id="row-detail-realisasi">

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- end realisasi -->
            <!-- Api -->
            <div class="tab-pane fade" id="top-api" role="tabpanel" aria-labelledby="api-top-tab">
              <div class="row">
                <div class="col-sm-12">

                  <div class="alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
                    <p>Layanan API belum tersedia</p>

                  </div>

                </div>
              </div>



            </div>
            <!-- end api -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Zero Configuration  Ends-->

  <!-- batas -->
</div>
</div>

</div>




<div class="modal fade" id="modalReferensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Referensi</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form id="formRef" class="form-horizontal" method="POST">
        <div class="modal-body">

          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
          <input type="hidden" name="save_method" value="add">
          <input type="hidden" name="id_keuangan" value="">
          <fieldset>


            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">APBD Murni</label>  
              <div class="col-lg-12">
                <input id="apbd_murni" type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_murni" style="min-width: 200px" />
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">APBD Perubahan</label>  
              <div class="col-lg-12">
                <input id="apbd_perubahan" type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_perubahan" style="min-width: 200px" />
              </div>
            </div>

          </fieldset>

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" data-dismiss="modal">Batal</button>
          <button class="btn btn-secondary" type="submit">Simpan</button>
        </div>

      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
 var save_method;

 function editRef(id) {
   save_method = 'update_perencanaan';
   $('#formRef')[0].reset();
   $.ajax({
     url: "<?= base_url() . $this->router->fetch_class(). '/fetch_perencanaan/' ?>" + id,
     type: "GET",
     dataType: "JSON",
     success: function(data) {
       $('[name="save_method"]').val(save_method);
       $('[name="id_keuangan"]').val(id);
       $('[name="apbd_murni"]').val(data.apbd_murni);
       $('[name="apbd_perubahan"]').val(data.apbd_perubahan);

       $('#modalReferensi').modal('show');
       $('.modal-title').text('Ubah Referensi');

     },
     error: function(jqXHR, textStatus, errorThrown) {
       alert("Gagal mendapatkan data");
     }
   });

 }

 function deleteRef(id) {

  swal({
    title: "Hapus Data?",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        url: "<?= base_url() . $this->router->fetch_class(). '/delete_perencanaan/' ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
          $('#modalReferensi').modal('hide');
          swal("Berhasil", "Data Berhasil Dihapus!", "success");
          location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert('Error deleting data');
        }
      });
    }
  });
}
</script>


<div class="modal fade" id="modalReferensi2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Referensi</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <form id="formRef2" class="form-horizontal" method="POST">
        <div class="modal-body">

          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
          <input type="hidden" name="save_method" value="add">
          <input type="hidden" name="id_realisasi" value="">
          <fieldset>


            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">Tanggal</label>  
              <div class="col-lg-12">
                <input id="tgl_realisasi" type="date" class="form-control" placeholder="yyyy-mm-dd" name="tgl_realisasi" autocomplete="off" data-date-format="dd MM yyyy" data-language="en" style="min-width: 200px"/>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">Jenis Realisasi</label>  
              <div class="col-lg-12">
                <select id="jenis_realisasi" class="form-control" name="jenis_realisasi">
                  <option>-- Pilih --</option>
                  <option value="murni">Murni</option>
                  <option value="perubahan">Perubahan</option>
                </select>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label class="col-lg-12 control-label text-lg-left" for="uraian">Anggaran Realisasi</label>  
              <div class="col-lg-12">
                <input id="anggaran_realisasi" type="text" class="form-control text-right currency" placeholder="Rp." name="anggaran_realisasi" style="min-width: 200px" />
              </div>
            </div>

          </fieldset>

        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="button" data-dismiss="modal">Batal</button>
          <button class="btn btn-secondary" type="submit">Simpan</button>
        </div>

      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
 function detailRef(id) {
   $('#row-detail-realisasi').empty();
   $('#row-detail-realisasi').html('<tr><th colspan="5" class="text-center">-- Memuat Data --</th></tr>');
   $.ajax({
     url: "<?= base_url() . $this->router->fetch_class(). '/fetch_detail_realisasi/' ?>" + id,
     type: "GET",
     dataType: "JSON",
     success: function(data) {

      $('#row-detail-realisasi').empty();
      $('#table-detail-realisasi').DataTable().destroy();
      $('#row-detail-realisasi').html(data.table);
      $('#table-detail-realisasi').DataTable();

    },
    error: function(jqXHR, textStatus, errorThrown) {
     alert("Gagal mendapatkan data");
   }
 });

 }

 var save_method;

 function editRef2(id) {
   save_method = 'update_realisasi';
   $('#formRef2')[0].reset();
   $.ajax({
     url: "<?= base_url() . $this->router->fetch_class(). '/fetch_realisasi/' ?>" + id,
     type: "GET",
     dataType: "JSON",
     success: function(data) {
       $('[name="save_method"]').val(save_method);
       $('[name="id_realisasi"]').val(id);
       $('[name="tgl_realisasi"]').val(data.tgl_realisasi);
       $('[name="jenis_realisasi"]').val(data.jenis_realisasi);
       $('[name="anggaran_realisasi"]').val(data.anggaran_realisasi);

       $('#modalReferensi2').modal('show');
       $('.modal-title').text('Ubah Realisasi');

       $(".currency").inputmask({ alias : "currency", prefix: 'Rp', removeMaskOnSubmit: true });
     },
     error: function(jqXHR, textStatus, errorThrown) {
       alert("Gagal mendapatkan data");
     }
   });

 }

 function deleteRef2(id) {

  swal({
    title: "Hapus Data?",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        url: "<?= base_url() . $this->router->fetch_class(). '/delete_realisasi/' ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
          $('#modalReferensi2').modal('hide');
          swal("Berhasil", "Data Berhasil Dihapus!", "success");
          location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert('Error deleting data');
        }
      });
    }
  });
}
</script>
