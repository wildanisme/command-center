 <div class="page-body">
 	<div class="container-fluid">
 		<div class="page-header">
 			<div class="row">
 				<div class="col-lg-6 main-header">
 					<h2><span>Sumber Data Keuangan</span></h2>
 					<h6 class="mb-0">Kab. Bogor</h6>
 				</div>
 				<div class="col-lg-6 breadcrumb-right">
 					<ol class="breadcrumb">
 						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
 						<li class="breadcrumb-item">Keuangan</li>
 						<li class="breadcrumb-item active">Sumber Data  </li>
 						<li class="breadcrumb-item active">Add  </li>
 					</ol>
 				</div>
 			</div>
 		</div>
 	</div>
 	<!-- Container-fluid starts-->
 	<div class="container-fluid">
 		<div class="row">
 			<!-- awal -->

 			<!-- Zero Configuration  Starts-->
 			<div class="col-sm-12">
 				<form role="form" method='post' enctype="multipart/form-data">
 					<div class="card">
 						<div class="card-header">
 							<h5 class="pull-left">Sumber Data Penganggaran Add </h5>
 							<button type="submit" class="btn btn-primary pull-right"><span class="fa fa-send"></span> Simpan</button>
 						</div>
 						<div class="card-body">
 							<?php echo flashdata_notif();?>
 							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
 							<div class="table table-responsive">
 								<table class="table table-border table-striped">
 									<thead class="success">
 										<tr>
 											<th style="width: 40%">Kode Rekening</th>
 											<th>Tahun</th>
 											<th>APBD Murni</th>
 											<th>APBD Perubahan</th>
 											<th>Opsi</th>
 										</tr>
 									</thead>

 									<tbody id="data-row">

 										<?php if (!empty($id_ref_keuangan)): ?>
 											<?php foreach ($id_ref_keuangan as $key => $value): ?>
 												<tr id="row<?=$key+1?>">
 													<td>
 														<select class="form-control select2" name="id_ref_keuangan[]">
 															<option>-- Pilih --</option>
 															<?php foreach ($ref as $row): ?>
 																<option value="<?=$row->id_ref_keuangan?>" <?=($row->id_ref_keuangan == $id_ref_keuangan[$key])? "selected" : ""?>><?=$row->kode_rekening?> <?=$row->uraian?></option>
 															<?php endforeach ?>
 														</select>
 													</td>
 													<td><input type="text" value="<?=$tahun[$key]?>" class="form-control datepicker-here" placeholder="yyyy" maxlength="4" name="tahun[]" autocomplete="off" style="min-width: 70px" data-date-format="yyyy" data-min-view="years" data-view="years" data-language="en"/></td>
 													<td><input type="text" value="<?=$apbd_murni[$key]?>" class="form-control text-right currency" placeholder="Rp." name="apbd_murni[]" style="min-width: 200px" /></td>
 													<td><input type="text" value="<?=$apbd_perubahan[$key]?>" class="form-control text-right currency" placeholder="Rp." name="apbd_perubahan[]" style="min-width: 200px" /></td>

 														<td align="center">
 															<?php if($key>0) :?>
 																<a href="javascript:hapus(<?=($key+1);?>)">
 																	<i class="feather text-danger icon-trash"></i>
 																</a>
 															<?php endif;?>
 														</td>
 													</tr>
 												<?php endforeach ?>

 												<?php else: ?>
 													<tr id="row1">
 														<td>
 															<select class="form-control select2" name="id_ref_keuangan[]">
 																<option>-- Pilih --</option>
 																<?php foreach ($ref as $row): ?>
 																	<option value="<?=$row->id_ref_keuangan?>"><?=$row->kode_rekening?> <?=$row->uraian?></option>
 																<?php endforeach ?>
 															</select>
 														</td>
 														<td><input type="text" class="form-control datepicker-here" placeholder="yyyy" maxlength="4" name="tahun[]" autocomplete="off" style="min-width: 70px" data-date-format="yyyy" data-min-view="years" data-view="years" data-language="en"/></td>
 														<td><input type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_murni[]" style="min-width: 200px" /></td>
 														<td><input type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_perubahan[]" style="min-width: 200px" /></td>

 														<td align="center"></td>
 													</tr>
 												<?php endif ?>
 											</tbody>

 										</table>
 									</div>
 									<div class="col-md-12 text-center p-t-20">
 										<a href="javascript:tambah()" class="btn btn-outline-primary "><i class="feather icon-plus"></i> Tambah baris</a>
 									</div>

 								</div>
 							</div>
 						</form>
 					</div>
 					<!-- Zero Configuration  Ends-->

 					<!-- batas -->
 				</div>
 			</div>

 		</div>




 		<script>
 			var row = <?= !empty($jenis_barang) ? count($jenis_barang) : '1' ?>;
 			function tambah()
 			{
 				row++;
 				var content = '<tr id="row'+row+'">'
 				+ '<td><select class="form-control select2" name="id_ref_keuangan[]"><option>-- Pilih --</option>'
 				+ '<?php foreach ($ref as $row): ?><option value="<?=$row->id_ref_keuangan?>"><?=$row->kode_rekening?> <?=$row->uraian?></option><?php endforeach ?>'
 				+ '</select></td>'
 				+ '<td><input type="text" class="form-control datepicker-here" placeholder="yyyy" maxlength="4" name="tahun[]" autocomplete="off" style="min-width: 70px" data-date-format="yyyy" data-min-view="years" data-view="years" data-language="en"/></td>'
 				+ '<td><input type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_murni[]" style="min-width: 200px" /></td>'
 				+ '<td><input type="text" class="form-control text-right currency" placeholder="Rp." name="apbd_perubahan[]" style="min-width: 200px" /></td>'
 				+ '<td align="center">'
 				+ '<a href="javascript:hapus('+row+')">'
 				+ '<i class="feather text-danger icon-trash"></i>'
 				+ '</a>'
 				+ '</td>'
 				+ '</tr>';

 				$("#data-row").append(content);
 				$(".select2").select2();
 				$(".currency").inputmask({ alias : "currency", prefix: 'Rp', removeMaskOnSubmit: true });
 				$(".datepicker-here").datepicker();
 			}
 			function hapus(row)
 			{
 				if(row>1)
 				{
 					$("#row"+row).remove();
 				}
 			}
 		</script>