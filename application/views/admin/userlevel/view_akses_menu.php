<div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-lg-6 main-header">
					<h2>Hak<span>Akses</span></h2>
					<h6 class="mb-0">Bogor Comand Center</h6>
				</div>
				<div class="col-lg-6 breadcrumb-right">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
						<li class="breadcrumb-item">Apps </li>
						<li class="breadcrumb-item">Akses</li>
						<li class="breadcrumb-item active">Hak Akses</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<div class="table-responsive bordered">
								<table class="table">
									<thead>
										<tr>
											<th>Nama Menu</th>
											<th>Akses</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($data_menu as $key => $row): ?>
											<tr>
												<?php 
												$id_menu=$row->id_menu;
												$akses_menu = $this->db->where('id_level', $id_level);
												$akses_menu = $this->db->where('id_menu', $id_menu);
												$akses_menu = $this->db->get('tbl_akses_menu')->row();
												$vi= (!empty($akses_menu->view))? $akses_menu->view:'';
												$id_akses_menu = (!empty($akses_menu->id))? $akses_menu->id : '0';
												$chek=($vi=="Y") ? 'checked' : '';
													
												?>

												<td style="background-color:  #7e37d8;"><?php echo $row->nama_menu; ?></td>
												<td><input type="checkbox" id="menu<?=$id_menu?>" onclick="update_menu('<?=$id_akses_menu?>','<?=$id_menu?>','<?=$id_level?>')" <?=$chek?>></td>

											</tr>	
											<?php foreach ($data_submenu as $key => $sub): ?>

												<?php if ($id_menu==$sub->id_menu): ?>
													<?php $id_submenu = $sub->id_submenu;
													$akses_submenu = $this->db->where('id_level', $id_level);
													$akses_submenu = $this->db->where('id_submenu', $id_submenu);
													$akses_submenu = $this->db->get('tbl_akses_submenu')->row();
													$vvie= (!empty($akses_submenu->view))? $akses_submenu->view:'';
													$id_akses_submenu=(!empty($akses_submenu->id))?$akses_submenu->id:'0';
													$chek1 =($vvie=="Y") ? 'checked':''; 

													?>
													<tr>
														<td><?php echo $sub->nama_submenu; ?></td>
														<td><input type="checkbox" id="sub<?=$id_submenu?>" onclick="update_sub('<?=$id_akses_submenu?>','<?=$id_submenu?>','<?=$id_level?>')"  <?=$chek1?>></td>
													</tr>
												<?php endif ?>
												

											<?php endforeach ?>							
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function update_menu(id,id_menu,level) {
			var v;
			if ($('#menu'+id_menu).is(':checked')) {
				v='Y';
			}else{
				v='N';
			}

			$.ajax({
				url : '<?php echo site_url('userlevel/update_akses_menu') ?>',
				data : {id:id,view:v,level:level,id_menu:id_menu},
				type : 'post',
				dataType : 'json',
				success :  function (data) {
					if (data.status) {
						location.reload();
						notif_load_success();
					}
				}
			})

		}

		function update_sub(id,id_submenu,level) {
			var v;
			if ($('#sub'+id_submenu).is(':checked')) {
				v='Y';
			}else{
				v='N';
			}

			$.ajax({
				url : '<?php echo site_url('userlevel/update_akses_submenu') ?>',
				data : {id:id,view:v,level:level,id_submenu:id_submenu},
				type : 'post',
				dataType : 'json',
				success :  function (data) {
					if (data.status) {
						location.reload();
						notif_load_success();
					}
				}
			})

		}
	</script>