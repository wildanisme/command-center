


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

<div class="col-lg-12 xl-100">
  <div class="row">


    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5>List User Level</h5>
          <div class="card-header-right">
            
          <a href="<?= site_url('userlevel/add') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus text-white"></i> </a>
          </div>
        </div>
        <div class="card-body p-0">

          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_card_menu">
              <thead>
                <tr>
                  <th scope="col">Level</th>
                  <th>Hak Akses</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>

              
              <tbody>
                <?php foreach ($level as $data): ?>
                <tr>
                  <td class="f-w-600"> <?php echo $data->level?> </td>
                  <td>
                    <a type="button" href="<?= site_url('userlevel/view_akses_menu/' . $data->level_id) ?>" class="btn btn-success btn-sm">Hak Akses </a>
                  </td>
                  <td>
                    <a href="<?= site_url('userlevel/edit/' . $data->level_id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->level_id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
                 <?php endforeach; ?>
              </tbody>
             
            </table>
          </div>

        </div>
      </div>
    </div>


  </div>
</div>

<!-- Modal dialog hapus data-->
<div class="modal fade" id="myModalDelete" tabindex="-1" aria-labelledby="myModalDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalDeleteLabel">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Anda ingin menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-danger" id="btdelete">Lanjutkan</button>
            </div>
        </div>
    </div>
</div>

<script>
  $('#table_card_menu').dataTable();
  $('#table_card_menu').on('click', '.item-delete', function() {
        //ambil data dari atribute data 
        var level_id = $(this).attr('data');
        $('#myModalDelete').modal('show');
        //ketika tombol lanjutkan ditekan, data id akan dikirim ke method delete 
        //pada controller mahasiswa
        $('#btdelete').unbind().click(function() {
            $.ajax({
                type: 'ajax',
                method: 'post',
                async: false,
                url: '<?php echo base_url() ?>userlevel/delete/',
                data: {
                    level_id: level_id
                },
                dataType: 'json',
                success: function(response) {
                    $('#myModalDelete').modal('hide');
                    location.reload();
                }
            });
        });
    });
</script>







<!-- Plugins JS start-->
<!-- Plugins JS start-->


<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>