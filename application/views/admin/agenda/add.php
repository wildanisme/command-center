<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
				if (!empty($message)){
					?>
					<div class="alert alert-<?= $type;?> alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?= $message;?>
					</div>
					<?php }?>
					<div class="x_panel">
						<form method='post' enctype="multipart/form-data" >
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
														
						<input type="hidden" name="id_user" value="<?=$this->user_id?>">
							<div class="x_content">
								<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
									<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									<label id='status'></label>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="panel panel-default">
											<div class="panel-heading">Poster</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<input type="file" id="input-file-now" name="poster" class="dropify" />
														</div>
													</div>

												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-body">

												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Tanggal Mulai Agenda</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="icon-calender"></i></div>
															<input type="date" class="form-control" id="datepicker" placeholder="yyyy-mm-dd"  name="tgl_mulai_agenda">
														</div>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Tanggal Akhir Agenda</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="icon-calender"></i></div>
															<input type="date" class="form-control" id="datepicker" placeholder="yyyy-mm-dd"  name="tgl_akhir_agenda">
														</div>
													</div>
												</div>

												
											</div>
										</div>


									</div>
									<div class="col-md-9">	
										<div class="panel panel-default">
											<div class="panel-heading">
												Detail Agenda
											</div>
											<div class="panel-body">
												<div class="row">

													<div class="col-md-12">
														<div class="form-group">
															<label>Kategori</label>
															<select name="id_kategori_agenda" class="form-control form-control-line select2">
																<?php 
																foreach ($kategori as $row) {
																	echo "<option value='$row->id_kategori_agenda'>$row->nama_kategori_agenda</option>";
																}
																?>
															</select>
														</div>
													</div>

													

													<div class="col-md-12">
														<div class="form-group">
															<label>Nama Agenda </label>
															<input type="text" id="nama_agenda" name="nama_agenda" value="<?= !empty($nama_agenda) ? $nama_agenda: "";?>" class="form-control" placeholder="Masukkan Nama Agenda" onkeyup="getCat()">
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label></label>
															<?php echo base_url();?>agenda/<label id='label-slug'></label>
															<input type="hidden" class="form-control" id='slug' name='slug' placeholder="" value="<?= !empty($slug) ? $slug: "";?>">
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Dekripsi Agenda</label>
															<textarea class="textarea_editor form-control" rows="10" name="deskripsi"><?= !empty($deskripsi) ? $deskripsi: "" ;?></textarea>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Alamat</label>
															<textarea class="form-control" name="alamat"><?= !empty($alamat) ? $alamat : '';?></textarea>
															<small class="error"><?= form_error("alamat");?></small>
															<select name="id_desa" class="form-control form-control-line select2">
																<option value="">Pilih desa</option>
																<?php 
																
																foreach ($desa as $row) {
																	$selected = (!empty($id_desa) && $id_desa == $row->id_desa ) ? "selected" : "";
																	echo "<option $selected value='$row->id_desa'>$row->desa, $row->kecamatan</option>";
																}
																?>
															</select>
															<small class="error"><?= form_error("id_desa");?></small>
														</div>
													</div>

													<div class="col-md-12">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label>longitude</label>
																	<input type="text" value="<?= !empty($longitude) ? $longitude : '';?>" id="longitude" onkeyup="loadMap()" name="longitude" class="form-control" placeholder="Masukkan Longitude">
																	
																</div>
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<label>latitude</label>
																	<input type="text" id="latitude" value="<?= !empty($latitude) ? $latitude : '';?>" onkeyup="loadMap()" name="latitude" class="form-control" placeholder="Masukkan Latitude">
																	
																</div>
															</div>

														</div>
														
													</div>

													<div class="col-md-12">
													<input style="margin-bottom:10px"  class="form-control" type="text" id="pac-input" placeholder="Cari alamat"/>
              
														<div id="googleMap" class="gmaps"></div>
													</div>



													<div class="col-md-12">
														<div class="form-group">
															<label>Tag</label>
															<textarea class="form-control" name="tag" value="<?=  !empty($tag) ? $tag: "";?>" placeholder="Dipisah dengan tanda koma (,)"></textarea>
														</div>
													</div>

													




												</div>
											</div>
										</div>  





										<button type='submit' class='btn btn-primary pull-right' style="margin-left: 6px;">Submit</button> 
										<a href='<?= base_url();?>manage_agenda' class='btn btn-default pull-right'>Back</a>
										


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<script>

					function hideMe()
					{$('#pesan').hide();}

					
				</script>
<script type="text/javascript">
	
function getCat()

{
	var permalink = $("#nama_agenda").val();
    // Trim empty space
    //permalink = $.trim($(this).val());
    // replace more then 1 space with only one
    permalink = permalink.replace(/\s+/g,' ');
    $('#slug').val(permalink.toLowerCase());
    $('#slug').val($('#slug').val().replace(/\W/g, ' '));
    $('#slug').val($.trim($('#slug').val()));
    $('#slug').val($('#slug').val().replace(/\s+/g, '-')+'-<?=bin2hex(random_bytes(2))?>');
    var gappermalink = $('#slug').val();
    $('#label-slug').html(gappermalink);
  //console.log(permalink);
}

function loadMap() {
      	var default_lat = '-6.864113885641478';
		var default_lng = '107.9237869248534';
		  
	  	var lat = $("#latitude").val();
		var lng = $("#longitude").val();
		if(lat!="" && lng!=""){
			var location = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
		
		}
		else{
			var location = new google.maps.LatLng(parseFloat(default_lat),parseFloat(default_lng));
		}	
			var mapProp= {
				center:location,
				zoom:12,
				//disableDefaultUI: true,
				//mapTypeId:google.maps.MapTypeId.HYBRID
			};
			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			
			var marker = new google.maps.Marker({
				position: location,
				//map: map,
				//animation: google.maps.Animation.BOUNCE
				});


			
			var infowindow = new google.maps.InfoWindow({
				content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
			});

			if(lat!="" && lng!=""){
				marker.setMap(map);
				markers.push(marker);
				infowindow.open(map,marker);
			}
			

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
			
			
			google.maps.event.addListener(map, 'click', function(event) {
				placeMarker(map, event.latLng);
			});

			var input = document.getElementById('pac-input');
        //var types = document.getElementById('type-selector');
        //var strictBounds = document.getElementById('strict-bounds-selector');

        //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        //var infowindowContent = document.getElementById('infowindow-content');
        //infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          //infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
          
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            
            map.fitBounds(place.geometry.viewport);
           
          } else {
            
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }

          //hapus tanda
          for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
          }

          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          markers.push(marker);
          var location  = place.geometry.location;
          
          $("#latitude").val(location.lat());
          $("#longitude").val(location.lng());

          var infowindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
            });
          
            infowindow.open(map, marker);
	
		});
		

    }
    var markers = [];
    function placeMarker(map, location) {
      var marker = new google.maps.Marker({
        position: location,
        map: map,
        animation: google.maps.Animation.BOUNCE,
      });
      var infowindow = new google.maps.InfoWindow({
        content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
      });

      //hapus tanda
      for(var i=0; i < markers.length ; i++){
        markers[i].setMap(null);
      }

      infowindow.open(map,marker);
      markers.push(marker);
      $("#latitude").val(location.lat());
	  $("#longitude").val(location.lng());
	  $("#pac-input").val("");
	}

</script>