 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Agenda</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Manage Agenda</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>


              <section id="content-types">

           <div class="row col-row-spacing">
            <div class="col-md-3">
                <a href="<?= base_url('manage_agenda/add') ?>" class="btn btn-block btn-primary"><i class="feather icon-user-plus"></i> Tambah</a>
            </div>
            <div class="col-md-9 inputselect-group">
                <form method="POST">
                    <div class="form-group select-container">
                        <select name="id_jabatan" class="form-control">
                            <option value="">Kategori</option>
                            
                        </select>
                    </div>
                    <fieldset class="form-group position-relative has-icon-left input-divider-left">
                        <div class="input-group">
                            <input type="text" value="" name="" class="round form-control" id="iconLeft3" placeholder="Cari Nama Kelompok ...">
                            <div class="form-control-position">
                                <i class="feather icon-user"></i>
                            </div>
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary round" type="submit" name="submit" value="search"><i class="feather icon-search"></i></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                
                
            </div>
        </div>

						

                    <div class="row match-height">
                    	<?php foreach($agenda as $l){
							
							?>

                    	  <div class="col-xl-4 col-md-6">
                            <div class="card">
                                <div class="card-header mb-1">
                                    <h4 class="card-title"><?=$l->nama_agenda?></h4>
                                </div>
                                <div class="card-content">
                                    <img class="img-fluid" src="<?=base_url()?>data/agenda/<?= ($l->poster=='') ? 'sumedang.png' : $l->poster?>" alt="Card image cap" style=" width: 100%;height: 400px;object-fit: cover;">
                                    <div class="card-body">
                                        <p class="card-text"><?=$l->nama_kategori_agenda?></p>
                                    </div>
                                </div>
                                <div class="card-footer text-muted">
                                    <span class="float-left"><?=$l->tgl_mulai_agenda?></span>
                                    <span class="float-right">
                                        <a href="<?php echo base_url();?>manage_agenda/view/<?=$l->id_agenda?>" class="card-link">Detail <i class="fa fa-angle-right"></i></a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    </div>
                </section>
</div>
</div>
