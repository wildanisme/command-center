 <!-- BEGIN: Content-->
 <div class="app-content content">
 	<div class="content-overlay"></div>
 	<div class="header-navbar-shadow"></div>
 	<div class="content-wrapper">
 		<div class="content-header row">
 			<div class="content-header-left col-md-9 col-12 mb-2">
 				<div class="row breadcrumbs-top">
 					<div class="col-md-12">
 						<h2 class="content-header-title float-left mb-0">Manage Agenda</h2>
 						<div class="breadcrumb-wrapper col-12">
 							<ol class="breadcrumb">
 								<li class="breadcrumb-item"><a href="#">Home</a>
 								</li>
 								<li class="breadcrumb-item "><a href="<?=base_url();?>manage_agenda">Manage Agenda</a>
 									<li class="breadcrumb-item active"><a href="#">Detail Agenda</a>
 									</li>

 								</ol>
 							</div>
 						</div>
 					</div>
 				</div>

 			</div>
 			<div class="content-body">

 				<!-- page users view start -->
 				<section class="page-users-view">
 					<div class="row">
 						<!-- account start -->
 						<div class="col-12">
 							<div class="card">
 								<div class="card-header">
 									<div class="card-title"></div>
 								</div>
 								<div class="card-body">
 									<div class="row">
 										<div class="col-md-2 ">
 											<div class="users-view-image">
 												<img src="<?=base_url()?>data/agenda/<?= ($detail->poster=='') ? 'sumedang.png' : $detail->poster?>" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
 											</div>
 										</div>
 										<div class="col-md-4 ">
 											<div class="form-group validate">
 												<div class="controls" >
 													<p class="text-muted" style="line-height: 0.2rem !important" >Nama Agenda</p>
 													<p><strong><?php echo title($title) ?></strong></p>
 												</div>
 											</div>  
 											<div class="form-group validate">
 												<div class="controls">
 													<p class="text-muted" style="line-height: 0.2rem !important" >Kategori</p>
 													<p><strong><?=$detail->nama_kategori_agenda?></strong></p>
 												</div>
 											</div>  
 											<div class="form-group validate">
 												<div class="controls">
 													<p class="text-muted" style="line-height: 0.2rem !important" >Deskripsi</p>
 													<p><strong><?=$detail->deskripsi?></strong></p>
 												</div>

 											</div>  
 											<div class="form-group validate">
 												<div class="controls">
 													<p class="text-muted" style="line-height: 0.2rem !important" >Alamat</p>
 													<p><strong><p><?=$detail->alamat?></p>
															<?php 
															if(!empty($detail_desa))
															{
																echo $detail_desa->desa.", ".$detail_desa->kecamatan;
															}
															?></strong></p>
 												</div>
 											</div>                                         

 										</div>

 										<div class="col-md-5">
 											
 											<div id="map" style="width: 100%;height: 230px;"></div>

 										</div>
 										<div class="col-12">
 											<a href="" class="fcbtn btn btn-outline btn-primary" style="margin-left: 7px;" data-toggle="modal" data-target="#editagenda" ><i class="ti-pencil"></i>Edit agenda</a>
 											<a class="fcbtn btn btn-outline btn-primary" style="margin-left: 7px;"  onclick="delete_agenda_(<?=$detail->id_agenda;?>)" ><i class="ti-trash"></i>Hapus Agenda</a>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 						<!-- account end -->
 						
 						
 						
 					
 					</div>
 				</section>
 				<!-- page users view end -->




 			</div>
 		</div>
 	</div>
 	<!-- END: Content-->

 	<div id="editagenda" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true" style="display: none;">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="panel-heading">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														<h4 class="modal-title" id="myLargeModalLabel1" style="color:white;">Edit Agenda</h4>
													</div>
													<div class="modal-body">
														<form method="POST" enctype="multipart/form-data" >
														<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
														
															<!-- <input type="hidden" name="flag" value="" id="flag" /> -->
															<input type="hidden" name="id_agenda" value="<?=$detail->id_agenda?>" id="id_agenda"/>
															<div class="form-group">
																<label for="exampleInputuname">Nama Agenda</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="ti-pencil"></i></div>
																	<input name="nama_agenda" id="nama_agenda" value="<?=$detail->nama_agenda?>" type="text" class="form-control" placeholder="Nama Agenda" onkeyup="getCat()">
																</div>
																<?php echo base_url();?>agenda/<label id='label-slug'><?=$detail->slug?></label>
																<input type="hidden" value="<?=$detail->slug?>" class="form-control" id='slug' name='slug' placeholder="">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Deskripsi Agenda</label>
																<textarea name="deskripsi" id="deskripsi" class="textarea_editor form-control"><?=$detail->deskripsi?></textarea>
															</div>

															<div class="form-group">
															<label for="exampleInputEmail1">Tanggal Mulai Agenda</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="icon-calender"></i></div>
																<input type="date" class="form-control" value="<?=$detail->tgl_mulai_agenda?>" id="datepicker" placeholder="yyyy-mm-dd"  name="tgl_mulai_agenda">
															</div>
															</div>

															<div class="form-group">
																<label for="exampleInputEmail1">Tanggal Akhir Agenda</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="icon-calender"></i></div>
																	<input type="date" class="form-control" id="datepicker" placeholder="yyyy-mm-dd"  name="tgl_akhir_agenda" value="<?=$detail->tgl_akhir_agenda?>">
																</div>
															</div>

															<div class="col-md-12">
																				<div class="form-group">
																					<label>Alamat</label>
																					<textarea class="form-control" name="alamat"><?= $detail->alamat;?></textarea>
																					<select name="id_desa" class="form-control form-control-line select2">
																						<option value="">Pilih desa</option>
																						<?php 
																						if(empty($id_desa)) $id_desa = $detail->id_desa;
																						foreach ($desa as $row) {
																							
																							$selected = (!empty($id_desa) && $id_desa==$row->id_desa) ? "selected" : "";
																							echo "<option $selected value='$row->id_desa'>$row->desa, $row->kecamatan</option>";
																						}
																						?>
																					</select>
																				</div>
																			</div>

															<div class="col-md-12">
																				<div class="row">
																					<div class="col-md-6">
																						<div class="form-group">
																							<label>longitude</label>
																							<input type="text" id="longitude" onkeyup="loadMap()" name="longitude" class="form-control" placeholder="Masukkan Longitude" value="<?=$detail->longitude;?>">
																						</div>
																					</div>
																					<div class="col-md-6">
																						<div class="form-group">
																							<label>latitude</label>
																							<input type="text" id="latitude" onkeyup="loadMap()" name="latitude" class="form-control" placeholder="Masukkan Latitude" value="<?=$detail->latitude?>">
																						</div>
																					</div>

																				</div>
																				
																			</div>

																			<div class="col-md-12">
																				<div id="googleMapEdit" class="gmaps"></div>
																			</div>

															<div class="form-group">
																<label for="exampleInputEmail1">Tag</label>
																<div class="input-group">
																	<div class="input-group-addon"><i class="ti-pencil"></i></div>
																	<textarea name="tag" class="form-control" id="tag" placeholder="Dipisah dengan tanda koma (,)"><?=$detail->tag?></textarea>
																</div>
															</div>
															<div class="form-group">
																<label>Poster</label>
																<input type="file" id="input-file-now" name="poster" class="dropify" />
															</div>

														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
															<button type="submit" class="btn btn-primary waves-effect text-left">Kirim</button>
														</div>
													</form>
												</div>
												<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										</div>


						<script>
											
							
							function delete_agenda_(id)
							{
								swal({   
									title: "Apakah anda yakin menghapus Agenda?",   
									text: "Data tersebut secara permanen akan terhapus",   
									type: "warning",   
									showCancelButton: true,   
									confirmButtonColor: "#DD6B55",   
									confirmButtonText: "Hapus",
									closeOnConfirm: false 
								}, function(){   
									window.location = "<?php echo base_url();?>manage_agenda/hapus_agenda/"+id;
									swal("Berhasil!", "Data telah terhapus.", "success"); 
								});
							}
							
							function setujui(id)
							{
								swal({   
									title: "Apakah anda yakin menyetujui event ini?",   
									type: "warning",   
									showCancelButton: true,   
									confirmButtonColor: "#DD6B55",   
									confirmButtonText: "Setuju",
									closeOnConfirm: false 
								}, function(){   
									window.location = "<?php echo base_url();?>manage_agenda/setuju/"+id;
									
								});
							}
							
							function edit_agenda(id_agenda,nama_agenda,deskripsi,longitude,latitude,tgl_awal_agenda,tgl_akhir_agenda,tag)
							{
								$("#id_agenda").val(id_agenda);
								
								$("#nama_agenda").val(nama_agenda);
								$("#deskripsi").html(deskripsi);
								$("#longitude").html(longitude);
								$("#latitude").html(latitude);
								$("#tgl_awal_agenda").html(tgl_awal_agenda);
								$("#tgl_akhir_agenda").html(tgl_akhir_agenda);
							
							}
							
							
							
						</script>
 	<script type="text/javascript">

 		function getCat()

 		{
 			var permalink = $("#nama_agenda").val();
    // Trim empty space
    //permalink = $.trim($(this).val());
    // replace more then 1 space with only one
    permalink = permalink.replace(/\s+/g,' ');
    $('#slug').val(permalink.toLowerCase());
    $('#slug').val($('#slug').val().replace(/\W/g, ' '));
    $('#slug').val($.trim($('#slug').val()));
    $('#slug').val($('#slug').val().replace(/\s+/g, '-')+'-<?=bin2hex(random_bytes(2))?>');
    var gappermalink = $('#slug').val();
    $('#label-slug').html(gappermalink);
  //console.log(permalink);
}
function loadMap() {


	var lat = <?= $detail->latitude;?>;
	var lng = <?= $detail->longitude;?>;
	if(lat!="" && lng!=""){
		var location = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
		
	}
	
	var mapProp= {
		center:location,
		zoom:12,
				//disableDefaultUI: true,
				//mapTypeId:google.maps.MapTypeId.HYBRID
			};
			var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
			

			var marker = new google.maps.Marker({
				position: location,
				//map: map,
				//animation: google.maps.Animation.BOUNCE
			});


			
			var infowindow = new google.maps.InfoWindow({
				content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
			});

			if(lat!="" && lng!=""){
				marker.setMap(map);
				//markers.push(marker);
				infowindow.open(map,marker);
			}
			loadMapEdit();	
		}

		function loadMapEdit() {
			var default_lat = '-6.864113885641478';
			var default_lng = '107.9237869248534';

			var lat = <?= $detail->latitude;?>;
			var lng = <?= $detail->longitude;?>;
			if(lat!="" && lng!=""){
				var location = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));

			}
			else{
				var location = new google.maps.LatLng(parseFloat(default_lat),parseFloat(default_lng));
			}	
			var mapProp= {
				center:location,
				zoom:12,
				//disableDefaultUI: true,
				//mapTypeId:google.maps.MapTypeId.HYBRID
			};
			var map=new google.maps.Map(document.getElementById("googleMapEdit"),mapProp);
			
			var marker = new google.maps.Marker({
				position: location,
				//map: map,
				//animation: google.maps.Animation.BOUNCE
			});


			
			var infowindow = new google.maps.InfoWindow({
				content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
			});

			if(lat!="" && lng!=""){
				marker.setMap(map);
				markers.push(marker);
				infowindow.open(map,marker);
			}
			

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
			
			
			google.maps.event.addListener(map, 'click', function(event) {
				placeMarker(map, event.latLng);
			});


		}
		var markers = [];
		function placeMarker(map, location) {
			var marker = new google.maps.Marker({
				position: location,
				map: map,
				animation: google.maps.Animation.BOUNCE,
			});
			var infowindow = new google.maps.InfoWindow({
				content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
			});

      //hapus tanda
      for(var i=0; i < markers.length ; i++){
      	markers[i].setMap(null);
      }

      infowindow.open(map,marker);
      markers.push(marker);
      $("#latitude").val(location.lat());
      $("#longitude").val(location.lng());
  }
</script>


