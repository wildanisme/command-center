<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Mal Pelayanan Publik </span></h2>
          <h6 class="mb-0">Indeks kepuasan masyarakat</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item ">Layanan  </li>
            <li class="breadcrumb-item active">IKM  </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-primary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="users"></i></div>
              <div class="media-body"><span class="m-0 text-white">Jumlah Peserta</span>
                <h4 class="mb-0"><?= number_format($data->jumlah_peserta);?></h4><i class="icon-bg" data-feather="users"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="check-circle"></i></div>
              <div class="media-body"><span class="m-0">Nilai IKM</span>
                <h4 class="mb-0 "><?= number_format($data->nilai_ikm);?></h4><i class="icon-bg" data-feather="check-circle"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-success o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="award"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Nilai Indeks</span>
                <h4 class="mb-0  text-white"><?= number_format($data->nilai_indeks);?></h4><i class="icon-bg" data-feather="award"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-<?=($data->color=="dark") ? "warning" : $data->color;?> o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="bookmark"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Kategori</span>
                <h4 class="mb-0  text-white"><?= $data->kategori." / " . $data->nama_kategori;?></h4><i class="icon-bg" data-feather="bookmark"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      

        <div class="col-xl-12 col-md-12 box-col-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-10">
                  <h5>Jumlah responden</h5>
                </div>
                <div class="col-2 text-right">
                    <select class="form-control select2" id="tahun" onchange="set_year()">
                    <?php foreach($data->tahunArr as $key => $row){
                        if($row->tahun>0)
                        {
                            $selected = ($tahun == $row->tahun) ? "selected":"";
                            echo '<option '.$selected.' value="'.$row->tahun.'">'.$row->tahun.'</option>';
                        }
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="card-body pt-0 px-0">
                
              <div id="grafik-ikm"></div>
            </div>
          </div>
        </div>
        
      
      <!-- </div> -->
      <!-- status widget Ends-->


      

      

      <div class="col-md-1 Add-card">
        <!-- <div class="add-arrow"> -->
          <!-- <div class="more-btn text-center"> -->
            <!-- <button class="btn btn-secondary btn-block f-w-700">Add more </button> -->
            <!-- <button class="btn btn-light"><i class="ion ion-plus font-secondary f-22"></i></button> -->
            <!-- </div> -->
            <!-- </div> -->
          </div>


          
          <div class="col-xl-12 box-col-12">
            <div class="card">
              <div class="card-header no-border">
                <h5>Hasil Indeks Kepuasan Masyarakat</h5>
                <ul class="creative-dots">
                  <li class="bg-primary big-dot"></li>
                  <li class="bg-secondary semi-big-dot"></li>
                  <li class="bg-warning medium-dot"></li>
                  <li class="bg-info semi-medium-dot"></li>
                  <li class="bg-secondary semi-small-dot"></li>
                  <li class="bg-primary small-dot"></li>
                </ul>
                
              </div>
              <div class="card-body pt-0">
                <div class="activity-table table-responsive recent-table">
                  <table class="table table-bordernone" id="data-layanan_">
                    <thead>
                        
                        <tr>
                            <th style="text-align:left;">Kode</th>
                            <th style="text-align:left;">Unsur</th>
                            <th style="text-align:left;">Indikator</th>
                            <th style="text-align:center;">Nilai rata-rata</th>
                            
                        </tr>
                    </thead>
                    <tbody id="row-data">
                      <?php foreach($data->dt_indikator as $row):?>
                        <tr>
                            <td style="text-align:center;"><?=$row->kd_unsur;?></td>
                            <td><?=$row->nama_unsur;?></td>
                            <td><?=$row->nama_indikator;?></td>
                            <td style="text-align:center;"><?=$row->rata2;?></td>
                        </tr>
                      <?php endforeach?>
                    </tbody>
                  </table>
                  
                </div>
                <div class="code-box-copy">
                  <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head21" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                  <pre></pre>
                </div>
              </div>
            </div>
          </div>
          

          
          

          

          <!-- batas -->
        </div>
      </div>
      
<script>
    function set_year()
    {
        var tahun = $("#tahun").val();
        window.location = "<?=base_url();?>mpp/ikm?tahun="+tahun;
    }



</script>