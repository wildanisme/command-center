<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Mal Pelayanan Publik </span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active">Layanan  </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-xl-4 col-lg-6 box-col-6">
        <div class="card gradient-primary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="server"></i></div>
              <div class="media-body"><span class="m-0 text-white">Total Instansi</span>
                <h4 class="mb-0"><?= number_format($data_mpp->instansi);?></h4><i class="icon-bg" data-feather="server"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-4 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="check-circle"></i></div>
              <div class="media-body"><span class="m-0">Total Layanan</span>
                <h4 class="mb-0 "><?= number_format($data_mpp->total_layanan);?></h4><i class="icon-bg" data-feather="check-circle"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-4 col-lg-6 box-col-6">
        <div class="card gradient-success o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="users"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Jumlah Pengunjung</span>
                <h4 class="mb-0  text-white"><?= number_format($data_mpp->total_terlayani);?></h4><i class="icon-bg" data-feather="users"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      

        <div class="col-xl-12 col-md-12 box-col-12">
          <div class="card">
            <div class="card-header">
              <div class="row">
                <div class="col-10">
                  <h5>Total Masyarakat Terlayani</h5>
                </div>
                <div class="col-2 text-right">
                    <select class="form-control select2" id="tahun" onchange="set_year()">
                    <?php foreach($data_mpp->tahunArr as $key => $row){
                        if($row->tahun>0)
                        {
                            $selected = ($tahun == $row->tahun) ? "selected":"";
                            echo '<option '.$selected.' value="'.$row->tahun.'">'.$row->tahun.'</option>';
                        }
                    }
                    ?>
                    </select>
                </div>
              </div>
              <div class="card-body pt-0 px-0">
                
                <div id="grafik-terlayani"></div>
              </div>
            </div>
          </div>
        <div>
        <div class="row">
            <div class="col-xl-4 col-lg-12 box-col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-9">
                                <h5>Status Jalur Antrian</h5>
                            </div>
                            <div class="col-3 text-right"><a class="btn btn-light" href="<?=base_url();?>mpp/skpd?tahun=<?=$tahun;?>">Detail</a></div>
                        </div>
                    </div>
                    <div class="card-body r-dount" style="padding-bottom: 13px">
                        <div id="gJalur" style="height: 360px"> </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-12 box-col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-9">
                                <h5> Performance Pelayanan</h5>
                            </div>
                            <div class="col-3 text-right"><i class="text-muted" data-feather="bar-chart"></i></div>
                        </div>
                    </div>
                    <div class="card-body" style="padding-bottom: 13px">
                        <div class="row">
                            <div class="col-md-6">
                                <table border="0" cellpadding="5">
                                    <tbody><tr>
                                    <td><img height="80px" src="https://mpp.sumedangkab.go.id/data/images/puas_blue.png"></td>
                                    <td><img height="80px" src="https://mpp.sumedangkab.go.id/data/images/tidak_puas_blue.png"></td>
                                    </tr>
                                    <tr align="center">
                                    <td><span class="paid"><?=$data_mpp->puas;?></span></td>
                                    <td><span class="paid"><?=$data_mpp->tidak_puas;?></span></td>
                                    </tr>
                                    </tbody>
                                </table>
                   
                            </div>
                        
                            <div class="col-md-6">
                                <h5>Rata-Rata Waktu Pelayanan</h5>
                                <h1 style="font-size: 50px;"><?=$data_mpp->rata_waktu;?></h1>
                                <p class="text-white" style="margin-top: 10px">Layanan Permenit</p>
                            </div>

                            <div class="col-md-12 mt-5">
                                <h5> Survey Kepuasan Terhadap Layanan (Online)</h5>

                                <a class="<?= ((floor($data_mpp->rating))>=1) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->rating))>=2) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->rating))>=3) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->rating))>=4) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->rating))>=5) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                (<?=$data_mpp->rating;?>)
                                <div>
                                    <a class="mt-3 btn btn-light" href="<?=base_url();?>mpp/rating">Detail</a>
                                </div>
                            </div>

                            <div class="col-md-12 mt-5">
                                <h5> Indeks kepuasan masyarakat (IKM)</h5>

                                <a class="<?= ((floor($data_mpp->nilai_indeks))>=1) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->nilai_indeks))>=2) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->nilai_indeks))>=3) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($data_mpp->nilai_indeks))>=4) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                (<?=$data_mpp->kategori;?> / <?=$data_mpp->nama_kategori;?>)
                                <div>
                                    <a class="mt-3 btn btn-light" href="<?=base_url();?>mpp/ikm">Detail</a>
                                </div>
                            </div>
                
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>

      
      <!-- </div> -->
      <!-- status widget Ends-->


      

      

      <div class="col-md-1 Add-card">
        <!-- <div class="add-arrow"> -->
          <!-- <div class="more-btn text-center"> -->
            <!-- <button class="btn btn-secondary btn-block f-w-700">Add more </button> -->
            <!-- <button class="btn btn-light"><i class="ion ion-plus font-secondary f-22"></i></button> -->
            <!-- </div> -->
            <!-- </div> -->
          </div>


          
          <div class="col-xl-12 box-col-12">
            <div class="card">
              <div class="card-header no-border">
                <h5>Data Pelayanan</h5>
                <ul class="creative-dots">
                  <li class="bg-primary big-dot"></li>
                  <li class="bg-secondary semi-big-dot"></li>
                  <li class="bg-warning medium-dot"></li>
                  <li class="bg-info semi-medium-dot"></li>
                  <li class="bg-secondary semi-small-dot"></li>
                  <li class="bg-primary small-dot"></li>
                </ul>
                
              </div>
              <div class="card-body pt-0">
                <div class="activity-table table-responsive recent-table">
                  <table class="table table-bordernone" id="data-layanan_">
                    <thead>
                        <tr>
                            <th rowspan="2">Penyedia Layanan</th>
                            <th rowspan="2">Layanan</th>
                            <th rowspan="2">Offline</th>
                            <th rowspan="2">Online</th>
                            <th colspan="7" style="text-align:center;">Status</th>
                            <th rowspan="2">Total</th>
                        </tr>
                        <tr>
                            <th style="text-align:center;">Belum antri</th>
                            <th style="text-align:center;">Menunggu</th>
                            <th style="text-align:center;">Dijadwal Ulang</th>
                            <th style="text-align:center;">Pending</th>
                            <th style="text-align:center;">Dilayani</th>
                            <th style="text-align:center;">Selesai</th>
                            <th style="text-align:center;">Dibatalkan</th>
                        </tr>
                    </thead>
                    <tbody id="row-data">
                      
                    </tbody>
                  </table>
                  <div class="row mt-3 col-md-12 text-center" style="text-align:center;" id="pagination">
                      
                  </div>
                </div>
                <div class="code-box-copy">
                  <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head21" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                  <pre></pre>
                </div>
              </div>
            </div>
          </div>
          

          
          

          

          <!-- batas -->
        </div>
      </div>
      
<script>
    function set_year()
    {
        var tahun = $("#tahun").val();
        window.location = "<?=base_url();?>mpp?tahun="+tahun;
    }

    var rowData = {};

    function loadPagination(page_num)
    {
       
        $("#row-data").html('<tr><td colspan="12" style="text-align:center;">Sedang memuat data ...</td></tr>');
        $.ajax({
         url        : "<?=base_url();?>mpp/get_layanan/"+page_num,
         type       : 'post',
         dataType   : 'json',
         data       : {
            
         },
         success    : function(data){
            rowData = data.result;
            //console.log(data);
            loadData(data.result,data.row);
            $("#pagination").html(data.pagination);
         },
         error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
       });
       
    }

    function loadData(data,row)
    {
        row = Number(row);
        $("#row-data").empty();
        for(i in data)
        {
            //console.log(data[i]);
            row++;
           

            var content = '<tr>'
                +'<td>'+data[i].nama_skpd+'</td>'
                +'<td>'+data[i].nama_layanan+'</td>'
                +'<td align="center">'+data[i].offline+'</td>'
                +'<td align="center">'+data[i].online+'</td>'
                +'<td align="center" class="text-dark">'+data[i].belum_antri+'</td>'
                +'<td align="center">'+data[i].menunggu+'</td>'
                +'<td align="center">'+data[i].dijadwal_ulang+'</td>'
                +'<td align="center">'+data[i].pending+'</td>'
                +'<td align="center">'+data[i].dilayani+'</td>'
                +'<td align="center">'+data[i].selesai+'</td>'
                +'<td align="center">'+data[i].dibatalkan+'</td>'
                +'<td align="center">'+data[i].total+'</td>'
                
            +'</tr>'
            ;

            $("#row-data").append(content);
            
        }
        
     
    }


</script>