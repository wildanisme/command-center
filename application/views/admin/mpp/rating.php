<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Mal Pelayanan Publik </span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item">Layanan  </li>
            <li class="breadcrumb-item active">Rating  </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="row">
      
      
        

      
      <!-- </div> -->
      <!-- status widget Ends-->


      

      

      <div class="col-md-1 Add-card">
        <!-- <div class="add-arrow"> -->
          <!-- <div class="more-btn text-center"> -->
            <!-- <button class="btn btn-secondary btn-block f-w-700">Add more </button> -->
            <!-- <button class="btn btn-light"><i class="ion ion-plus font-secondary f-22"></i></button> -->
            <!-- </div> -->
            <!-- </div> -->
          </div>


          
          <div class="col-xl-12 box-col-12">
            <div class="card">
              <div class="card-header no-border">
                <h5>Data Rating</h5>
                <ul class="creative-dots">
                  <li class="bg-primary big-dot"></li>
                  <li class="bg-secondary semi-big-dot"></li>
                  <li class="bg-warning medium-dot"></li>
                  <li class="bg-info semi-medium-dot"></li>
                  <li class="bg-secondary semi-small-dot"></li>
                  <li class="bg-primary small-dot"></li>
                </ul>
                
              </div>
              <div class="card-body pt-0">
                <div class="activity-table table-responsive recent-table">
                  <table class="table table-bordernone" id="data-layanan">
                    <thead>
                        <tr>
                            <th style="text-align:left;">Penyedia Layanan</th>
                            <th style="text-align:left;">Layanan</th>
                            <th style="text-align:center;">Rata-rata waktu pelayanan</th>
                            <th width="200px" style="text-align:center;">Rating</th>
                        </tr>
                    </thead>
                    <tbody id="row-data">
                    <?php foreach($data as $row):?>
                        <tr>
                            <td><?=$row->nama_skpd;?></td>
                            <td><?=$row->nama_layanan;?></td>
                            <td><?=$row->rata_waktu_pelayanan;?></td>
                            <td>
                                <a class="<?= ((floor($row->rating))>=1) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($row->rating))>=2) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($row->rating))>=3) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($row->rating))>=4) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                <a class="<?= ((floor($row->rating))>=5) ? 'text-primary' :'' ;?>"><i data-feather="star"></i></a>
                                (<?=$row->rating;?>)
                            </td>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                  </table>
                  
                </div>
                <div class="code-box-copy">
                  <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head21" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                  <pre></pre>
                </div>
              </div>
            </div>
          </div>
          

          
          

          

          <!-- batas -->
        </div>
      </div>
      
