<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Mal Pelayanan Publik </span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active">Layanan  </li>
          </ol>
        </div>
      </div>
    </div>
    <div class="row">
     
        
        
        
        <div class="col-xl-8 col-lg-8 box-col-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">
                            <h5><?=$data->data[0]->nama_skpd;?></h5>
                        </div>
                        <div class="col-2 text-right">
                            <select class="form-control select2" id="tahun" onchange="set_filter()">
                            <?php foreach($data->tahunArr as $key => $row){
                                if($row->tahun>0)
                                {
                                    $selected = ($tahun == $row->tahun) ? "selected":"";
                                    echo '<option '.$selected.' value="'.$row->tahun.'">'.$row->tahun.'</option>';
                                }
                            }
                            ?>
                            </select>
                        </div>
                        <div class="col-4 text-right">
                            <select class="form-control select2" id="id_skpd" onchange="set_filter()">
                            <?php foreach($data->dt_skpd as $key => $row){
                                
                                    $selected = ($id_skpd == $row->id_skpd) ? "selected":"";
                                    echo '<option '.$selected.' value="'.$row->id_skpd.'">'.$row->nama_skpd.'</option>';
                                
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body pt-0 px-0">
                    <div id="grafik-terlayani"></div>
                </div>
            </div>
        </div> 


        <div class="col-xl-4 col-lg-4 box-col-4">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-9">
                            <h5>Status Jalur Antrian</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body r-dount" style="padding-bottom: 13px">
                    <div id="gJalur" style="height: 360px"> </div>
                </div>
            </div>
        </div> 
        
        
            

    </div>

      
      <!-- </div> -->
      <!-- status widget Ends-->


      

      

      <div class="col-md-1 Add-card">
        <!-- <div class="add-arrow"> -->
          <!-- <div class="more-btn text-center"> -->
            <!-- <button class="btn btn-secondary btn-block f-w-700">Add more </button> -->
            <!-- <button class="btn btn-light"><i class="ion ion-plus font-secondary f-22"></i></button> -->
            <!-- </div> -->
            <!-- </div> -->
          </div>


          
          
          

          
          

          

          <!-- batas -->
        </div>
      </div>
      
<script>
    function set_filter()
    {
        var tahun = $("#tahun").val();
        var id_skpd = $("#id_skpd").val();
        window.location = "<?=base_url();?>mpp/skpd?tahun="+tahun+"&id_skpd="+id_skpd;
    }

    


</script>