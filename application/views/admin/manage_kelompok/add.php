<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
       <div class="content-header row">
           <div class="content-header-left col-md-9 col-12 mb-2">
               <div class="row breadcrumbs-top">
                   <div class="col-md-12">
                       <h2 class="content-header-title float-left mb-0">Manage Kelompok</h2>
                       <div class="breadcrumb-wrapper col-12">
                           <ol class="breadcrumb">
                               <li class="breadcrumb-item"><a href="#">Home</a>
                               </li>
                               <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Manage kelompok</a>
                                   <li class="breadcrumb-item active"><a href="#">Tambah kelompok</a>
                                   </li>

                               </ol>
                           </div>
                       </div>
                   </div>
               </div>

           </div>
           <div class="content-body">

               <!-- page users view start -->
               <section class="page-users-view">
                 <form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
                 <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                   <div class="row">
                       <!-- account start -->
                       <div class="col-12">
                           <div class="card">
                               <div class="card-header">
                                   <div class="card-title">Kelompok Bersama</div>
                               </div>
                               <div class="card-body">
                                   <div class="row">
                                       <div class="col-md-2 ">
                                           <div class="users-view-image">
                                               <img src="<?=base_url();?>data/images/avatar.png" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                           </div>
                                       </div>
                                       <div class="col-md-4 ">
                                          <div class="form-group validate">
                                           <div class="controls">
                                               <label>Nama Kelompok</label>
                                               <input type="text" value="<?= !empty($nama_kelompok) ? $nama_kelompok : "" ;?>" class="form-control" placeholder="Nama kelompok" name="nama_kelompok" >
                                               <?= form_error("nama_kelompok",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Jenis Usaha</label>
                                               <select class="form-control select2" name="id_jenis_usaha">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_jenis_usaha as $row) {
                                                     $selected = (!empty($id_jenis_usaha) && $id_jenis_usaha==$row->id_jenis_usaha) ? "selected" : "";
                                                     echo "<option $selected value='$row->id_jenis_usaha'>$row->nama_jenis</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_jenis_usaha",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Mentor</label>
                                               <select class="form-control select2" name="id_mentor">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_mentor as $row){
                                                      $selected = (!empty($id_mentor) && $id_mentor==$row->id_mentor) ? "selected" : "";
                                                      echo "<option $selected value='$row->id_mentor'>$row->nama_mentor</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_mentor",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Ketua Kelompok</label>
                                               <select class="form-control select2" name="id_ketua">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_ketua as $row){
                                                      $selected = (!empty($id_ketua) && $id_ketua==$row->user_id) ? "selected" : "";
                                                      echo "<option $selected value='$row->user_id'>$row->full_name</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_ketua",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                         <div class="controls">
                                           <label>Telepon Kelompok</label>
                                           <input type="text" value="<?= !empty($telepon) ? $telepon : "" ;?>" class="form-control" name="telepon" placeholder="Masukan No. Telp" >
                                           <?= form_error("telepon",'<div class="text-danger">',"</div>") ;?>
                                       </div>
                                   </div>

                               </div>

                               <div class="col-md-5">
                                <div class="form-group validate">
                                   <div class="controls">
                                       <label>Kecamatan</label>
                                       <select class="form-control select2" name="id_kecamatan" id="id_kecamatan" onchange="get_desa()">
                                           <option value="">Pilih</option>
                                           <?php
                                           foreach ($dt_kecamatan as $row) {
                                             $selected = (!empty($id_kecamatan) && $id_kecamatan==$row->id_kecamatan) ? "selected" : "";
                                             echo "<option $selected value='$row->id_kecamatan'>$row->kecamatan</option>";
                                           }
                                           ?>
                                       </select>
                                       <?= form_error("id_kecamatan",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>
                               <div class="form-group validate">
                                   <div class="controls">
                                       <label>Desa</label>
                                       <select class="form-control select2" name="id_desa" id="id_desa">
                                           <option value="">Pilih</option>
                                           <?php
                                           foreach ($dt_desa as $row) {
                                             $selected = (!empty($id_desa) && $id_desa==$row->id_desa) ? "selected" : "";
                                             echo "<option $selected value='$row->id_desa'>$row->desa</option>";
                                           }
                                           ?>
                                       </select>
                                       <?= form_error("id_desa",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>

                               <div class="form-group validate">
                                   <div class="controls">
                                       <label>Alamat Lengkap</label>
                                       <textarea class="form-control" name="alamat" style="height: 120px"><?= !empty($alamat) ? $alamat :"";?></textarea>
                                       <?= form_error("alamat",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>

                               <ul class="list-unstyled mb-0">
                                   <li class="d-inline-block mr-2">
                                       <fieldset>
                                           <div class="vs-radio-con vs-radio-success">
                                               <input type="radio" name="status" <?= (empty($status) || (!empty($status) && $status =="active")) ? "checked" :"" ;?> value="active">
                                               <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                               </span>
                                               <span class="">Active</span>
                                           </div>
                                       </fieldset>
                                   </li>

                                   <li class="d-inline-block mr-2">
                                       <fieldset>
                                           <div class="vs-radio-con vs-radio-danger">
                                               <input type="radio" name="status"  <?= (!empty($status) && $status =="not active") ? "checked" :"" ;?> value="not active">
                                               <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                               </span>
                                               <span class="">Not Active</span>
                                           </div>
                                       </fieldset>
                                   </li>
                                 </ul>
                                 <?= form_error("status",'<div class="text-danger">',"</div>") ;?>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <!-- account end -->
           <!-- information start -->
           <div class="col-md-6 col-12 ">
               <div class="card">
                   <div class="card-header">
                       <div class="card-title mb-2">Maps</div>
                   </div>
                   <div class="card-body">

                       <div class="row">
                           <div class="col-md-6 ">
                              <div class="form-group ">
                               <div class="controls">
                                   <label>Latitude</label>
                                   <input type="text" value="<?= !empty($latitude) ? $latitude : "" ;?>" class="form-control" name="latitude" id="latitude" placeholder="latitude" >
                                   <?= form_error("latitude",'<div class="text-danger">',"</div>") ;?>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6 ">
                          <div class="form-group ">
                           <div class="controls">
                               <label>Longitude</label>
                               <input type="text" value="<?= !empty($longitude) ? $longitude : "" ;?>" class="form-control" name="longitude" id="longitude" placeholder="longitude" >
                               <?= form_error("longitude",'<div class="text-danger">',"</div>") ;?>
                           </div>
                       </div>
                   </div>

                   <div class="col-md-12">
                     <div id="map" style="width: 100%;height: 315px;"></div>
                 </div>

           </div>
       </div>
   </div>
</div>
<!-- information start -->
<!-- social links end -->
<div class="col-md-6 col-12 ">
   <div class="card">
       <div class="card-header">
           <div class="card-title mb-2">Media Lainnya</div>
       </div>
       <div class="card-body">
           <div class="form-group ">
               <div class="controls">
                   <label>Facebook</label>
                   <input type="text" value="<?= !empty($facebook) ? $facebook : "" ;?>" class="form-control" name="facebook" placeholder="Masukan link" >
                   <?= form_error("facebook",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Instagram</label>
                   <input type="text" value="<?= !empty($instagram) ? $instagram : "" ;?>" class="form-control" name="instagram" placeholder="Masukan link" >
                   <?= form_error("instagram",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Tokopedia</label>
                   <input type="text" value="<?= !empty($tokopedia) ? $tokopedia : "" ;?>" class="form-control" name="tokopedia" placeholder="Masukan link" >
                   <?= form_error("tokopedia",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Shopee</label>
                   <input type="text" value="<?= !empty($shopee) ? $shopee : "" ;?>" class="form-control" name="shopee" placeholder="Masukan link" >
                   <?= form_error("shopee",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Bukalapak</label>
                   <input type="text" value="<?= !empty($bukalapak) ? $bukalapak : "" ;?>" class="form-control" name="bukalapak" placeholder="Masukan link" >
                   <?= form_error("bukalapak",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

       </div>
   </div>
</div>

</div>

<div class="row mb-3 mt-1">

  <div class="col-12 ">
      <button class="btn btn-primary ">Submit</button>
      <a href="<?=base_url();?>manage_kelompok" class="btn btn-outline-primary mr-1 ">Batal</a>

  </div>

</div>
</form>
</section>





</div>
</div>
</div>


<script>
function get_desa()
{
  var id_kecamatan = $("#id_kecamatan").val();
  $.ajax({
      url :"<?php echo base_url("manage_kelompok/get_desa")?>/"+id_kecamatan,
      type:'get',
      dataType : "json",
      data:{},
       success    : function(data){
          //console.log(data.dt_induk);
          var dt_desa = data.dt_desa;

          var opt = "<option value=''>Pilih</option>";
          for(i in dt_desa)
          {
            opt += "<option value='"+dt_desa[i].id_desa+"'>"+dt_desa[i].desa+"</option>";
            //console.log(dt_induk[i]);
          }
          $("#id_desa").html(opt).trigger("change");
       },
           error: function(xhr, status, error) {
            //swal("Opps","Error","error");
            console.log(xhr);
          }
  });
}



	function hideMe()
	{$('#pesan').hide();}






    </script>
