 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Manage Kelompok</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Manage kelompok</a>
                                    <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok/detail">Detail kelompok</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Detail Anggota</a>
                                    </li>
                                    
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

                <!-- page users view start -->
                <section class="page-users-view">
                    <div class="row">
                        <!-- account start -->
                        <div class="col-3">

                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Kelompok 1</div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="users-view-image">
                                                <img src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-11.jpg" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <a href="app-user-edit.html" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> Edit </a>
                                            <button class="btn btn-outline-danger"><i class="feather icon-trash-2 "></i> Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-9">

                            <div class="card">
                                <div class="card-header border-bottom">
                                    <div class="card-title mb-1 text-primary">Kelompok 1</div>
                                </div>

                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-12 ">
                                           <div class="form-group validate">
                                            <div class="controls" >
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Nama Anggota</p>
                                                <p><strong>Nandang Koswara</strong></p>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >NIK</p>
                                                <p><strong>32015151854</strong></p>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >No. KK</p>
                                                <p><strong>3213210546</strong></p>
                                            </div>
                                        </div> 

                                        <div class="form-group validate">
                                          <div class="controls">
                                            <p class="text-muted" style="line-height: 0.2rem !important" >No. DTKS.</p>
                                            <p><strong>08251550154</strong></p>
                                        </div>
                                    </div>  

                                    <div class="form-group validate">
                                      <div class="controls">
                                        <p class="text-muted" style="line-height: 0.2rem !important" >No. Telepon.</p>
                                        <p><strong>08251550154</strong></p>
                                    </div>
                                </div>


                                <div class="form-group validate">
                                      <div class="controls">
                                        <p class="text-muted" style="line-height: 0.2rem !important" >Jenis Kelamin</p>
                                        <p><strong>Laki-Laki</strong></p>
                                    </div>
                                </div>

                                <div class="form-group validate">
                                 <div class="controls">
                                    <p class="text-muted" style="line-height: 0.2rem !important" >Alamat</p>
                                    <p><strong>Dsn. Ciumpleng Rt.01 Rw. 03</strong></p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
</section>
<!-- page users view end -->




</div>
</div>
</div>
    <!-- END: Content-->