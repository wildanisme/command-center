<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
       <div class="content-header row">
           <div class="content-header-left col-md-9 col-12 mb-2">
               <div class="row breadcrumbs-top">
                   <div class="col-md-12">
                       <h2 class="content-header-title float-left mb-0">Manage Kelompok</h2>
                       <div class="breadcrumb-wrapper col-12">
                           <ol class="breadcrumb">
                               <li class="breadcrumb-item"><a href="#">Home</a>
                               </li>
                               <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Manage kelompok</a>
                                   <li class="breadcrumb-item active"><a href="#">Edit kelompok</a>
                                   </li>

                               </ol>
                           </div>
                       </div>
                   </div>
               </div>

           </div>
           <div class="content-body">

               <!-- page users view start -->
               <section class="page-users-view">
                 <form action="<?=base_url();?>manage_kelompok/edit/<?=$id_kelompok;?>" role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
                   <input type="hidden" value="<?=$id_kelompok;?>" name="id_kelompok"/>
                 <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                   <div class="row">
                       <!-- account start -->

                       <div class="col-12">
                           <div class="card">
                               <div class="card-header">
                                   <div class="card-title">Kelompok Bersama</div>
                               </div>
                               <div class="card-body">
                                   <div class="row">
                                       <div class="col-md-2 ">
                                           <div class="users-view-image">
                                               <img src="<?=base_url();?>data/images/avatar.png" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                           </div>
                                       </div>
                                       <div class="col-md-4 ">
                                          <div class="form-group validate">
                                           <div class="controls">
                                               <label>Nama Kelompok</label>
                                               <input type="text" value="<?= !empty($nama_kelompok) ? $nama_kelompok : "" ;?>" class="form-control" placeholder="Nama kelompok" name="nama_kelompok" >
                                               <?= form_error("nama_kelompok",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Jenis Usaha</label>
                                               <select class="form-control select2" name="id_jenis_usaha">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_jenis_usaha as $row) {
                                                     $selected = (!empty($id_jenis_usaha) && $id_jenis_usaha==$row->id_jenis_usaha) ? "selected" : "";
                                                     echo "<option $selected value='$row->id_jenis_usaha'>$row->nama_jenis</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_jenis_usaha",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Mentor</label>
                                               <select class="form-control select2" name="id_mentor">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_mentor as $row){
                                                      $selected = (!empty($id_mentor) && $id_mentor==$row->id_mentor) ? "selected" : "";
                                                      echo "<option $selected value='$row->id_mentor'>$row->nama_mentor</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_mentor",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                           <div class="controls">
                                               <label>Ketua Kelompok</label>
                                               <select class="form-control select2" name="id_ketua">
                                                   <option value="">Pilih</option>
                                                   <?php
                                                   foreach ($dt_ketua as $row){
                                                      $selected = (!empty($id_ketua) && $id_ketua==$row->user_id) ? "selected" : "";
                                                      echo "<option $selected value='$row->user_id'>$row->full_name</option>";
                                                   }
                                                   ?>
                                               </select>
                                               <?= form_error("id_ketua",'<div class="text-danger">',"</div>") ;?>
                                           </div>
                                       </div>
                                       <div class="form-group validate">
                                         <div class="controls">
                                           <label>Telepon Kelompok</label>
                                           <input type="text" value="<?= !empty($telepon) ? $telepon : "" ;?>" class="form-control" name="telepon" placeholder="Masukan No. Telp" >
                                           <?= form_error("telepon",'<div class="text-danger">',"</div>") ;?>
                                       </div>
                                   </div>

                               </div>

                               <div class="col-md-5">
                                <div class="form-group validate">
                                   <div class="controls">
                                       <label>Kecamatan</label>
                                       <select class="form-control select2" name="id_kecamatan" id="id_kecamatan" onchange="get_desa()">
                                           <option value="">Pilih</option>
                                           <?php
                                           foreach ($dt_kecamatan as $row) {
                                             $selected = (!empty($id_kecamatan) && $id_kecamatan==$row->id_kecamatan) ? "selected" : "";
                                             echo "<option $selected value='$row->id_kecamatan'>$row->kecamatan</option>";
                                           }
                                           ?>
                                       </select>
                                       <?= form_error("id_kecamatan",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>
                               <div class="form-group validate">
                                   <div class="controls">
                                       <label>Desa</label>
                                       <select class="form-control select2" name="id_desa" id="id_desa">
                                           <option value="">Pilih</option>
                                           <?php
                                           foreach ($dt_desa as $row) {
                                             $selected = (!empty($id_desa) && $id_desa==$row->id_desa) ? "selected" : "";
                                             echo "<option $selected value='$row->id_desa'>$row->desa</option>";
                                           }
                                           ?>
                                       </select>
                                       <?= form_error("id_desa",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>

                               <div class="form-group validate">
                                   <div class="controls">
                                       <label>Alamat Lengkap</label>
                                       <textarea class="form-control" name="alamat" style="height: 120px"><?= !empty($alamat) ? $alamat :"";?></textarea>
                                       <?= form_error("alamat",'<div class="text-danger">',"</div>") ;?>
                                   </div>
                               </div>

                               <ul class="list-unstyled mb-0">
                                   <li class="d-inline-block mr-2">
                                       <fieldset>
                                           <div class="vs-radio-con vs-radio-success">
                                               <input type="radio" name="status" <?= (empty($status) || (!empty($status) && $status =="active")) ? "checked" :"" ;?> value="active">
                                               <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                               </span>
                                               <span class="">Active</span>
                                           </div>
                                       </fieldset>
                                   </li>

                                   <li class="d-inline-block mr-2">
                                       <fieldset>
                                           <div class="vs-radio-con vs-radio-danger">
                                               <input type="radio" name="status"  <?= (!empty($status) && $status =="not active") ? "checked" :"" ;?> value="not active">
                                               <span class="vs-radio">
                                                   <span class="vs-radio--border"></span>
                                                   <span class="vs-radio--circle"></span>
                                               </span>
                                               <span class="">Not Active</span>
                                           </div>
                                       </fieldset>
                                   </li>
                                 </ul>
                                 <?= form_error("status",'<div class="text-danger">',"</div>") ;?>

                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <!-- account end -->
           <!-- information start -->
           <div class="col-md-6 col-12 ">
               <div class="card">
                   <div class="card-header">
                       <div class="card-title mb-2">Maps</div>
                   </div>
                   <div class="card-body">

                       <div class="row">
                           <div class="col-md-6 ">
                              <div class="form-group ">
                               <div class="controls">
                                   <label>Latitude</label>
                                   <input type="text" value="<?= !empty($latitude) ? $latitude : "" ;?>" class="form-control" name="latitude" id="latitude" placeholder="latitude" >
                                   <?= form_error("latitude",'<div class="text-danger">',"</div>") ;?>
                               </div>
                           </div>
                       </div>
                       <div class="col-md-6 ">
                          <div class="form-group ">
                           <div class="controls">
                               <label>Longitude</label>
                               <input type="text" value="<?= !empty($longitude) ? $longitude : "" ;?>" class="form-control" name="longitude" id="longitude" placeholder="longitude" >
                               <?= form_error("longitude",'<div class="text-danger">',"</div>") ;?>
                           </div>
                       </div>
                   </div>

                   <div class="col-md-12">
                     <div id="map" style="width: 100%;height: 315px;"></div>
                 </div>

           </div>
       </div>
   </div>
</div>
<!-- information start -->
<!-- social links end -->
<div class="col-md-6 col-12 ">
   <div class="card">
       <div class="card-header">
           <div class="card-title mb-2">Media Lainnya</div>
       </div>
       <div class="card-body">
           <div class="form-group ">
               <div class="controls">
                   <label>Facebook</label>
                   <input type="text" value="<?= !empty($facebook) ? $facebook : "" ;?>" class="form-control" name="facebook" placeholder="Masukan link" >
                   <?= form_error("facebook",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Instagram</label>
                   <input type="text" value="<?= !empty($instagram) ? $instagram : "" ;?>" class="form-control" name="instagram" placeholder="Masukan link" >
                   <?= form_error("instagram",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Tokopedia</label>
                   <input type="text" value="<?= !empty($tokopedia) ? $tokopedia : "" ;?>" class="form-control" name="tokopedia" placeholder="Masukan link" >
                   <?= form_error("tokopedia",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Shopee</label>
                   <input type="text" value="<?= !empty($shopee) ? $shopee : "" ;?>" class="form-control" name="shopee" placeholder="Masukan link" >
                   <?= form_error("shopee",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

           <div class="form-group ">
               <div class="controls">
                   <label>Bukalapak</label>
                   <input type="text" value="<?= !empty($bukalapak) ? $bukalapak : "" ;?>" class="form-control" name="bukalapak" placeholder="Masukan link" >
                   <?= form_error("bukalapak",'<div class="text-danger">',"</div>") ;?>
               </div>
           </div>

       </div>
   </div>
</div>

</div>

<div class="row mb-3 mt-1">

  <div class="col-12 ">
      <button class="btn btn-primary ">Simpan</button>
      <a href="<?=base_url();?>manage_kelompok/detail/<?=$id_kelompok;?>" class="btn btn-outline-primary mr-1 ">Batal</a>

  </div>

</div>
</form>

<div class="row mt-2" id="anggota">
  <div class="col-12">
      <div class="card">
          <div class="card-header border-bottom mx-2 px-0">
              <h6 class="border-bottom py-1 mb-0 font-medium-2"><i class="feather icon-user mr-50 "></i>Anggota
              </h6>
              <button type="button" onclick="add_anggota()" class="btn btn-outline-success" data-toggle="modal" data-target="#formAnggota">
                  Tambah Anggota
              </button>
          </div>
          <div class="card-body px-75">
              <div class="table-responsive users-view-permission">
                  <table class="table table-border table-striped">
                      <thead class="success">

                          <tr>
                              <th>Foto</th>
                              <th>ID DTKS</th>
                              <th>NIK</th>
                              <th>No. KK</th>
                              <th>Nama Anggota</th>
                              <th>Jenis Kelamin</th>
                              <th>Telepon</th>
                              <th>Alamat</th>
                              <th>Jabatan</th>
                              <th>Opsi</th>
                          </tr>
                      </thead>

                      <tbody>
                        <?php if($dt_anggota){
                          foreach ($dt_anggota as $row) {
                            $url_foto = base_url()."data/anggota/".$row->foto;
                            $foto ='<img id="foto_'.$row->id_anggota.'" class="round" src="'.$url_foto.'" alt="avatar" height="40" width="40">';
                            //$tgl_lahir = ($row->tgl_lahir) ? date("d/m/Y",strtotime($row->tgl_lahir)) : "";
                            $aksi = '
                            <a class="" data-toggle="modal" data-target="#formAnggota" onclick="edit_anggota('.$row->id_anggota.')"><i class="feather icon-edit"></i></a>
                            <a class="" onclick="hapus_anggota('.$row->id_anggota.')"><i class="feather icon-trash"></i></a>';

                            echo "
                              <tr>
                                <td>$foto</td>
                                <td><span id='no_dtks_$row->id_anggota'>$row->no_dtks</span></td>
                                <td><span id='nik_$row->id_anggota'>$row->nik</span></td>
                                <td><span id='no_kk_$row->id_anggota'>$row->no_kk</span></td>
                                <td><span id='nama_$row->id_anggota'>$row->nama</span></td>
                                <td><span id='jenis_kelamin_$row->id_anggota'>$row->jenis_kelamin</span></td>
                                <td><span id='no_hp_$row->id_anggota'>$row->no_hp</span></td>
                                <td><span id='alamat_$row->id_anggota'>$row->alamat</span></td>
                                <td><span id='jabatan_$row->id_anggota'>$row->jabatan</span></td>
                                <td class='product-action'>
                                <input type='hidden' id='tgl_lahir_$row->id_anggota' value='$row->tgl_lahir' />
                                <input type='hidden' id='status_$row->id_anggota' value='$row->status' />
                                $aksi</td>
                              </tr>
                            ";
                          }
                        }
                        else{
                          echo "<tr><td colspan='8' align='center'>-Tidak ada data-</td></tr>";
                        }
                        ?>
              </tbody>
          </table>
      </div>
  </div>
  </div>
  </div>
</div>
</section>





</div>
</div>
</div>


<div class="modal fade text-left" id="formAnggota" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_tittle">Anggota</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form class="" id="form-data">
              <div class="row">
                <div class="col-md-6">
                  <label>NO. DTKS: </label>
                  <div class="form-group">
                      <input type="text" id="no_dtks" name="no_dtks" placeholder="Masukan No. DTKS" class="form-control">
                      <p id="err_no_dtks" class="text-danger"></p>
                  </div>

                  <label>NO. KK: </label>
                  <div class="form-group">
                      <input type="text" id="no_kk" name="no_kk" placeholder="Masukan KK" class="form-control">
                      <p id="err_no_kk" class="text-danger"></p>
                  </div>


                  <label>NIK: </label>
                  <div class="form-group">
                      <input type="text" id="nik" name="nik" placeholder="Masukan NIK" class="form-control">
                      <p id="err_nik" class="text-danger"></p>
                  </div>

                  <label>Nama: </label>
                  <div class="form-group">
                      <input type="text" id="nama" name="nama" placeholder="Masukan Nama" class="form-control">
                      <p id="err_nama" class="text-danger"></p>
                  </div>

                  <div class="row">
                    <div class="col-md-5">

                      <label>Jenis Kelamin: </label>
                      <div class="form-group">
                         <ul class="list-unstyled mb-0">
                          <li class="d-inline-block mr-2">
                              <fieldset>
                                  <div class="vs-radio-con vs-radio-info">
                                      <input type="radio" id="jenis_kelamin_Pria" name="jenis_kelamin" checked value="Pria">
                                      <span class="vs-radio">
                                          <span class="vs-radio--border"></span>
                                          <span class="vs-radio--circle"></span>
                                      </span>
                                      <span class="">Pria</span>
                                  </div>
                              </fieldset>
                          </li>
                          <li class="d-inline-block mr-2">
                              <fieldset>
                                  <div class="vs-radio-con vs-radio-success">
                                      <input type="radio" id="jenis_kelamin_Wanita" name="jenis_kelamin" value="Wanita">
                                      <span class="vs-radio">
                                          <span class="vs-radio--border"></span>
                                          <span class="vs-radio--circle"></span>
                                      </span>
                                      <span class="">Wanita</span>
                                  </div>
                              </fieldset>
                          </li>
                        </ul>
                        <p id="err_jenis_kelamin" class="text-danger"></p>
                      </div>
                    </div>
                    <div class="col-md-7 col-12">

                      <label>Tanggal Lahir : </label>
                      <div class="form-group">
                          <input type="text" id="tgl_lahir" name="tgl_lahir" placeholder="Cth:1990-12-25 (Tahun-bln-tgl)" class="form-control">
                          <p id="err_tgl_lahir" class="text-danger"></p>
                      </div>
                    </div>
                  </div>

                  <label>No. Telepon: </label>
                  <div class="form-group">
                      <input type="text" id="no_hp" name="no_hp" placeholder="Masukan No. Telepon" class="form-control">
                      <p id="err_no_hp" class="text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">




                    <label>Alamat: </label>
                    <div class="form-group">
                        <textarea class="form-control" id="alamat" name="alamat"></textarea>
                        <p id="err_alamat" class="text-danger"></p>
                    </div>

                    <label>Jabatan: </label>
                    <div class="form-group">
                       <ul class="list-unstyled mb-0">
                        <li class="d-inline-block mr-2">
                            <fieldset>
                                <div class="vs-radio-con">
                                    <input type="radio" id="jabatan_Ketua" name="jabatan" value="Ketua">
                                    <span class="vs-radio">
                                        <span class="vs-radio--border"></span>
                                        <span class="vs-radio--circle"></span>
                                    </span>
                                    <span class="">Ketua</span>
                                </div>
                            </fieldset>
                        </li>
                        <li class="d-inline-block mr-2">
                            <fieldset>
                                <div class="vs-radio-con vs-radio-success">
                                    <input type="radio" id="jabatan_Anggota" name="jabatan" value="Anggota">
                                    <span class="vs-radio">
                                        <span class="vs-radio--border"></span>
                                        <span class="vs-radio--circle"></span>
                                    </span>
                                    <span class="">Anggota</span>
                                </div>
                            </fieldset>
                        </li>
                    </ul>
                    <p id="err_jabatan" class="text-danger"></p>
                </div>


                <label>Foto: </label>
                <div class="form-group">
                    <input type="file" name="foto" class="dropify" placeholder="Masukkan foto" >
                    <p id="err_foto" class="text-danger"></p>
                </div>

                <label>Status: </label>
                <div class="form-group">
                   <ul class="list-unstyled mb-0">
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-success">
                                <input type="radio" id="status_active" name="status" value="active">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Active</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-danger">
                                <input type="radio" id="status_not_active" name="status" value="not active">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Not Active</span>
                            </div>
                        </fieldset>
                    </li>
                </ul>
                <p id="err_status" class="text-danger"></p>
            </div>

                </div>
              </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="save_anggota()" class="btn btn-outline-primary" >Simpan</button>
            </div>
        </div>
    </div>
</div>

<script>
function get_desa()
{
  var id_kecamatan = $("#id_kecamatan").val();
  $.ajax({
      url :"<?php echo base_url("manage_kelompok/get_desa")?>/"+id_kecamatan,
      type:'get',
      dataType : "json",
      data:{},
       success    : function(data){
          //console.log(data.dt_induk);
          var dt_desa = data.dt_desa;

          var opt = "<option value=''>Pilih</option>";
          for(i in dt_desa)
          {
            opt += "<option value='"+dt_desa[i].id_desa+"'>"+dt_desa[i].desa+"</option>";
            //console.log(dt_induk[i]);
          }
          $("#id_desa").html(opt).trigger("change");
       },
           error: function(xhr, status, error) {
            //swal("Opps","Error","error");
            console.log(xhr);
          }
  });
}
</script>

<script type="text/javascript">

  var csrf_hash = "<?=$this->security->get_csrf_hash();?>";
  var action = "";
  var id_anggota=0;
  function add_anggota()
	{
		$("#modal_tittle").html("Tambah Anggota");
		$("#no_dtks").val("");
		$("#no_kk").val("");
		$("#nik").val("");
    $("#nama").val("");
		$("#jenis_kelamin_Pria").trigger("click");
    $("#jabatan_Anggota").trigger("click");
    $("#status_active").trigger("click");
    $("#tgl_lahir").val("");
    $("#no_hp").val("");
    $("#alamat").val("");

		var picture = "<?=base_url();?>data/anggota/default.png";
		$(".dropify-render").html("<img src='"+picture+"' />");
		$(".dropify-preview").attr("style","display: block;");
		$(".dropify-loader").attr("style","display:none;");
		$("#foto").attr("data-default-file",'');

		action = "add";
		id_anggota = 0;

		$(".text-danger").html("");
	}

  function edit_anggota(id)
	{
		$("#modal_tittle").html("Edit Anggota");
		$("#no_dtks").val($("#no_dtks_"+id).html());
		$("#no_kk").val($("#no_kk_"+id).html());
		$("#nik").val($("#nik_"+id).html());
    $("#nama").val($("#nama_"+id).html());
		$("#jenis_kelamin_"+$("#jenis_kelamin_"+id).html()).trigger("click");
    $("#jabatan_"+$("#jabatan_"+id).html()).trigger("click");

    $("#tgl_lahir").val($("#tgl_lahir_"+id).val());
    $("#no_hp").val($("#no_hp_"+id).html());
    $("#alamat").val($("#alamat_"+id).html());

    var status = $("#status_"+id).val();
    if(status=="active"){
      $("#status_active").trigger("click");
    }
    else{
      $("#status_not_active").trigger("click");
    }


		var picture = $("#foto_"+id).attr("src");
		$(".dropify-render").html("<img src='"+picture+"' />");
		$(".dropify-preview").attr("style","display: block;");
		$(".dropify-loader").attr("style","display:none;");
		$("#foto").attr("data-default-file",'');

		action = "edit";
		id_anggota = id;

		$(".text-danger").html("");
	}

    function save_anggota()
    {

        $(".text-danger").html("");

        $("#btn_save").attr("disabled",true);
        var formdata = new FormData(document.getElementById('form-data'));
        formdata.append("id_anggota",id_anggota);
        formdata.append("action",action);
        formdata.append("id_kelompok","<?=$id_kelompok;?>");
        formdata.append("<?=$this->security->get_csrf_token_name();?>",csrf_hash);
        $.ajax({
         url        : "<?=base_url()?>manage_kelompok/save_anggota/",
         type       : 'post',
         dataType   : 'json',
         data       : formdata,
         processData:false,
         contentType:false,
         cache:false,
         async:false,
         success    : function(data){
            console.log(data);
            csrf_hash = data.csrf_hash;
            $("#btn_save").attr("disabled",false);

            if(data.status){
            	swal("Sukses", data.message, "success");
              setTimeout(function() {
                var link = "<?=base_url();?>manage_kelompok/edit/<?=$id_kelompok;?>" ;
                window.location.href = link;
              }, 500);
            }
            else{
                for(err in data.errors)
                {
                    $("#err_"+err).html(data.errors[err]);

                    console.log(data.errors[err]);
                }
                if(data.errors.length==0){
                    swal("Opps", data.message, "warning");
                }
            }
         },
             error: function(xhr, status, error) {
             	console.log(xhr);
             	$("#btn_save").attr("disabled",false);
              	swal("Opps","Terjadi kesalahan","error");
            }
       });
    }


    function hapus_anggota(id)
    {
        swal({
          title: "Hapus anggota?",
          //icon: "info",
          buttons: true,
          dangerMode: false,
        })
        .then((isConfirm) => {
          if (isConfirm) {

            $.ajax({
                url :"<?php echo base_url("manage_kelompok/delete_anggota")?>",
                type:'post',
                data:{
                  id:id,
                  "<?=$this->security->get_csrf_token_name();?>" : csrf_hash,
                },
                 success    : function(data){

                   swal("Sukses", "Anggota berhasil dihapus", "success");
                   setTimeout(function() {
                     var link = "<?=base_url();?>manage_kelompok/edit/<?=$id_kelompok;?>" ;
                     window.location.href = link;
                   }, 500);
                 },
                     error: function(xhr, status, error) {
                      //swal("Opps","Error","error");
                      console.log(xhr);
                    }
            });

          }
        });
    }

</script>
