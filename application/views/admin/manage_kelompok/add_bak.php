 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Manage Kelompok</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Manage kelompok</a>
                                    <li class="breadcrumb-item active"><a href="#">Tambah kelompok</a>
                                    </li>
                                    
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

                <!-- page users view start -->
                <section class="page-users-view">
                    <div class="row">
                        <!-- account start -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Kelompok Bersama</div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 ">
                                            <div class="users-view-image">
                                                <img src="<?=base_url();?>data/images/avatar.png" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                            </div>
                                        </div>
                                        <div class="col-md-4 ">
                                           <div class="form-group validate">
                                            <div class="controls">
                                                <label>Nama Kelompok</label>
                                                <input type="text" class="form-control" placeholder="nama kelompok" required="" data-validation-required-message="This  field is required" aria-invalid="false">
                                                <div class="help-block"></div>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <label>Jenis Usaha</label>
                                                <select class="form-control" aria-invalid="false">
                                                    <option>usaha 1</option>
                                                    <option>usaha</option>
                                                    <option>usaha</option>
                                                </select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <label>Mentor</label>
                                                <select class="form-control" aria-invalid="false">
                                                    <option>mentor 1</option>
                                                    <option>mentor</option>
                                                    <option>mentor</option>
                                                </select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                          <div class="controls">
                                            <label>Telepon Kelompok</label>
                                            <input type="text" class="form-control" placeholder="Masukan No. Telp" required="" data-validation-required-message="This  field is required" aria-invalid="false">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>                                         

                                </div>

                                <div class="col-md-5">
                                 <div class="form-group validate">
                                    <div class="controls">
                                        <label>Kecamatan</label>
                                        <select class="form-control" aria-invalid="false">
                                            <option>Kec 1</option>
                                            <option>Kec 2</option>
                                        </select>
                                        <div class="help-block"></div>
                                    </div>
                                </div>  
                                <div class="form-group validate">
                                    <div class="controls">
                                        <label>Desa</label>
                                        <select class="form-control" aria-invalid="false">
                                            <option>desa 1</option>
                                            <option>desa 2</option>
                                        </select>
                                        <div class="help-block"></div>
                                    </div>
                                </div> 

                                <div class="form-group validate">
                                    <div class="controls">
                                        <label>Alamat Lengkap</label>
                                        <textarea class="form-control" aria-invalid="false" style="height: 120px"></textarea>
                                        <div class="help-block"></div>
                                    </div>
                                </div> 

                            </div>
                            <div class="col-12">
                                <a href="app-user-edit.html" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> Simpan</a>
                                <button class="btn btn-outline-danger"><i class="feather icon-trash-2"></i> Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- account end -->
            <!-- information start -->
            <div class="col-md-6 col-12 ">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title mb-2">Maps</div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6 ">
                               <div class="form-group ">
                                <div class="controls">
                                    <label>Latitude</label>
                                    <input type="text" class="form-control" placeholder="latitude">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                           <div class="form-group ">
                            <div class="controls">
                                <label>Langitude</label>
                                <input type="text" class="form-control" placeholder="langitude">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                      <div id="map" style="width: 100%;height: 230px;"></div>
                  </div>

                  <div class="col-12">
                    <a href="app-user-edit.html" class="btn btn-primary mr-1 mt-1"><i class="feather icon-edit-1"></i> Simpan</a>
                    <button class="btn btn-outline-danger mt-1"><i class="feather icon-trash-2"></i> Batal</button>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- information start -->
<!-- social links end -->
<div class="col-md-6 col-12 ">
    <div class="card">
        <div class="card-header">
            <div class="card-title mb-2">Media Lainnya</div>
        </div>
        <div class="card-body">
            <div class="form-group ">
                <div class="controls">
                    <label>Facebook</label>
                    <input type="text" class="form-control" placeholder="link facebook" >
                </div>
            </div>

            <div class="form-group ">
                <div class="controls">
                    <label>Instagram</label>
                    <input type="text" class="form-control" placeholder="link Instagram" >
                </div>
            </div>

            <div class="form-group ">
                <div class="controls">
                    <label>Tokopedia</label>
                    <input type="text" class="form-control" placeholder="link Tokopedia" >
                </div>
            </div>

            <div class="form-group ">
                <div class="controls">
                    <label>Shopee</label>
                    <input type="text" class="form-control" placeholder="link Shopee" >
                </div>
            </div>

            <div class="form-group ">
                <div class="controls">
                    <label>Bukalapak</label>
                    <input type="text" class="form-control" placeholder="link Bukalapak" >
                </div>
            </div>


            <div class="col-12 ">
                <a href="#" class="btn btn-primary mr-1 "><i class="feather icon-edit-1"></i> Simpan</a>
                <button class="btn btn-outline-danger "><i class="feather icon-trash-2"></i> Batal</button>
            </div>

        </div>
    </div>
</div>
<!-- social links end -->
<!-- permissions start -->
<div class="col-12">
    <div class="card">
        <div class="card-header border-bottom mx-2 px-0">
            <h6 class="border-bottom py-1 mb-0 font-medium-2"><i class="feather icon-user mr-50 "></i>Anggota
            </h6>
            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#inlineForm">
                Tambah Anggota
            </button>
        </div>
        <div class="card-body px-75">
            <div class="table-responsive users-view-permission">
                <table class="table table-border table-striped">
                    <thead class="success">

                        <tr>
                            <th>Foto</th>
                            <th>ID DTKS</th>
                            <th>NIK</th>
                            <th>No. KK</th>
                            <th>Nama Anggota</th>
                            <th>Jenis Kelamin</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                            <th>Jabatan</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td><img class="round" src="<?=base_url();?>asset/kube/app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></td>
                            <td>
                               3520210200212121
                           </td>
                           <td>
                               3520210200212121
                           </td>
                           <td>
                               3520210200212121
                           </td>

                           <td>
                            Nandang Koswara
                        </td>
                        <td>
                           Laki-Laki
                       </td>
                       <td>
                           0895555055045
                       </td>
                       <td>
                        Dsn. Cijati RT 03/02 
                    </td>
                    <td>
                        Ketua
                    </td>
                    <td>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
<!-- permissions end -->
</div>
</section>
<!-- page users view end -->



<!-- Modal -->
<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">Tambah Anggota </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#">
                <div class="modal-body">

                    <label>NO. DTKS: </label>
                    <div class="form-group">
                        <input type="text" placeholder="Masukan No. DTKS" class="form-control">
                    </div>

                    <label>NO. KK: </label>
                    <div class="form-group">
                        <input type="text" placeholder="Masukan KK" class="form-control">
                    </div>


                    <label>NIK: </label>
                    <div class="form-group">
                        <input type="text" placeholder="Masukan NIK" class="form-control">
                    </div>

                    <label>Nama: </label>
                    <div class="form-group">
                        <input type="text" placeholder="Masukan Nama" class="form-control">
                    </div>

                    <label>Jenis Kelamin: </label>
                    <div class="form-group">
                       <ul class="list-unstyled mb-0">
                        <li class="d-inline-block mr-2">
                            <fieldset>
                                <div class="vs-radio-con">
                                    <input type="radio" name="radiocolor" checked value="false">
                                    <span class="vs-radio">
                                        <span class="vs-radio--border"></span>
                                        <span class="vs-radio--circle"></span>
                                    </span>
                                    <span class="">Pria</span>
                                </div>
                            </fieldset>
                        </li>
                        <li class="d-inline-block mr-2">
                            <fieldset>
                                <div class="vs-radio-con vs-radio-success">
                                    <input type="radio" name="radiocolor" value="false">
                                    <span class="vs-radio">
                                        <span class="vs-radio--border"></span>
                                        <span class="vs-radio--circle"></span>
                                    </span>
                                    <span class="">Wanita</span>
                                </div>
                            </fieldset>
                        </li>
                    </ul>
                </div>

                <label>No. Telepon: </label>
                <div class="form-group">
                    <input type="text" placeholder="Masukan No. Telepon" class="form-control">
                </div>

                <label>Alamat: </label>
                <div class="form-group">
                    <textarea class="form-control"></textarea>
                </div>

                <label>Jabatan: </label>
                <div class="form-group">
                   <ul class="list-unstyled mb-0">
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con">
                                <input type="radio" name="jabatan" checked value="false">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Ketua</span>
                            </div>
                        </fieldset>
                    </li>
                    <li class="d-inline-block mr-2">
                        <fieldset>
                            <div class="vs-radio-con vs-radio-success">
                                <input type="radio" name="jabatan" value="false">
                                <span class="vs-radio">
                                    <span class="vs-radio--border"></span>
                                    <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">Anggota</span>
                            </div>
                        </fieldset>
                    </li>
                </ul>
            </div>

            <label>Foto: </label>
            <div class="form-group">
                <input type="file" name="foto" class="dropify" placeholder="Masukkan foto" >
            </div>





        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
        </div>
    </form>
</div>
</div>
</div>


</div>
</div>
</div>
    <!-- END: Content-->