 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Manage Kelompok</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Manage kelompok</a>
                                    <li class="breadcrumb-item active"><a href="#">Detail kelompok</a>
                                    </li>

                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

                <!-- page users view start -->
                <section class="page-users-view">
                    <div class="row">
                        <!-- account start -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">Kelompok Bersama</div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2 ">
                                            <div class="users-view-image">
                                                <img src="<?=base_url();?>data/images/avatar.png" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                            </div>
                                        </div>
                                        <div class="col-md-4 ">
                                           <div class="form-group validate">
                                            <div class="controls" >
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Nama Kelompok</p>
                                                <p><strong><?=$nama_kelompok;?></strong></p>
                                            </div>
                                        </div>
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Jenis Usaha</p>
                                                <p><strong><?=$nama_jenis_usaha;?></strong></p>
                                            </div>
                                        </div>
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Mentor</p>
                                                <p><strong><?=$nama_mentor;?></strong></p>
                                            </div>

                                        </div>
                                        <div class="form-group validate">
                                          <div class="controls">
                                            <p class="text-muted" style="line-height: 0.2rem !important" >Telp.</p>
                                            <p><strong><?=$telepon;?></strong></p>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-5">
                                 <div class="form-group validate">
                                    <div class="controls">
                                        <p class="text-muted" style="line-height: 0.2rem !important" >Kecamatan</p>
                                        <p><strong><?=$nama_kecamatan;?></strong></p>
                                    </div>
                                </div>
                                <div class="form-group validate">
                                    <div class="controls">
                                        <p class="text-muted" style="line-height: 0.2rem !important" >Desa</p>
                                        <p><strong><?=$nama_desa;?></strong></p>
                                    </div>
                                </div>

                                <div class="form-group validate">
                                 <div class="controls">
                                    <p class="text-muted" style="line-height: 0.2rem !important" >Alamat</p>
                                    <p><strong><?=$alamat;?></strong></p>
                                </div>
                            </div>

                        </div>
                        <div class="col-12">
                            <a href="<?=base_url();?>manage_kelompok/edit/<?=$id_kelompok;?>" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> Edit </a>
                            <button class="btn btn-outline-danger" onclick="hapus()"><i class="feather icon-trash-2"></i> Hapus</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- account end -->
        <!-- information start -->
        <div class="col-md-6 col-12 ">
            <div class="card">
                <div class="card-header">
                    <input type="hidden" id="latitude" value="<?=$latitude;?>"/>
                    <input type="hidden" id="longitude" value="<?=$longitude;?>"/>
                    <div class="card-title mb-2">Maps</div>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-6 ">
                           <div class="form-group ">
                            <div class="controls">
                                <p class="text-muted" style="line-height: 0.2rem !important" >Latitude</p>
                                <p><strong><?=$latitude;?></strong></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                       <div class="form-group ">
                        <div class="controls">
                            <p class="text-muted" style="line-height: 0.2rem !important" >Longitude</p>
                            <p><strong><?=$longitude;?></strong></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                  <div id="map" style="width: 100%;height: 230px;"></div>
              </div>



          </div>
      </div>
  </div>
</div>

<div class="col-md-6 col-12 ">
    <div class="card">
        <div class="card-header">
            <div class="card-title mb-2">Media Lainnya</div>
        </div>
        <div class="card-body">
            <div class="form-group ">
               <div class="controls">
                <p class="text-muted" style="line-height: 0.2rem !important" >Facebook</p>
                <p><strong><?= ($facebook) ? "<a href='$facebook'>$facebook</a>":"-" ;?></strong></p>
            </div>
        </div>

        <div class="form-group ">
            <div class="controls">
                <p class="text-muted" style="line-height: 0.2rem !important" >Instagram</p>
                <p><strong><?= ($instagram) ? "<a href='$instagram'>$instagram</a>":"-" ;?></strong></p>
            </div>
        </div>

        <div class="form-group ">
            <div class="controls">
                <p class="text-muted" style="line-height: 0.2rem !important" >Tokopedia</p>
                <p><strong><?= ($tokopedia) ? "<a href='$tokopedia'>$tokopedia</a>":"-" ;?></strong></p>
            </div>
        </div>

        <div class="form-group ">
            <div class="controls">
                <p class="text-muted" style="line-height: 0.2rem !important" >Shopee</p>
                <p><strong><?= ($shopee) ? "<a href='$shopee'>$shopee</a>":"-" ;?></strong></p>
            </div>
        </div>

        <div class="form-group ">
            <div class="controls">
                <p class="text-muted" style="line-height: 0.2rem !important" >Bukalapak</p>
                <p><strong><?= ($bukalapak) ? "<a href='$bukalapak'>$bukalapak</a>":"-" ;?></strong></p>
            </div>
        </div>



    </div>
</div>
</div>
<!-- social links end -->

<div class="col-12">
    <div class="card">
        <div class="card-header border-bottom mx-2 px-0">
            <h6 class="border-bottom py-1 mb-0 font-medium-2"><i class="feather icon-user mr-50 "></i>Anggota
            </h6>

        </div>
        <div class="card-body px-75">
            <div class="table-responsive users-view-permission">
                <table class="table table-border table-striped">
                    <thead class="success">

                        <tr>
                            <th>Foto</th>
                            <th>NIK</th>
                            <th>Nama Anggota</th>
                            <th>Jenis Kelamin</th>
                            <th>Telepon</th>
                            <th>Tgl. Lahir</th>
                            <th>Jabatan</th>
                            <th>Opsi</th>

                        </tr>
                    </thead>

                    <tbody>
                    <?php if($dt_anggota){
                      foreach ($dt_anggota as $row) {
                        $url_foto = base_url()."data/anggota/".$row->foto;
                        $foto ='<img class="round" src="'.$url_foto.'" alt="avatar" height="40" width="40">';
                        $tgl_lahir = ($row->tgl_lahir) ? date("d/m/Y",strtotime($row->tgl_lahir)) : "";
                        echo "
                          <tr>
                            <td>$foto</td>
                            <td>$row->nik</td>
                            <td>$row->nama</td>
                            <td>$row->jenis_kelamin</td>
                            <td>$row->no_hp</td>
                            <td>$tgl_lahir</td>
                            <td>$row->jabatan</td>
                            <td><a href='".base_url()."manage_kelompok/detail_anggota/$row->id_anggota' class='btn btn-outline-primary'>Detail</a></td>
                          </tr>
                        ";
                      }
                    }
                    else{
                      echo "<tr><td colspan='8' align='center'>-Tidak ada data-</td></tr>";
                    }
                    ?>

            </tbody>
        </table>
    </div>
</div>
</div>
</div>


<section id="ecommerce-products" class="grid-view mr-1 ml-1">

        <div class="card ecommerce-card ">
            <div class="card-content">
                <div class="item-img text-center">
                    <a href="app-ecommerce-details.html">
                        <img class="img-fluid" src="<?=base_url();?>data/produk/produk2.jpg" alt="img-placeholder">
                    </a>
                </div>
                <div class="card-body">
                    <div class="item-wrapper">
                        <div class="item-rating">
                            <div class="badge badge-primary badge-md">
                                <span>Sayuran</span>
                            </div>
                        </div>
                        <div>
                            <h6 class="item-price">
                                Rp. 50.000
                            </h6>
                        </div>
                    </div>
                    <div class="item-name">
                        <a href="app-ecommerce-details.html">Sayur </a>

                    </div>
                    <div>
                        <p class="item-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        </p>
                    </div>
                </div>
                <div class="item-options text-center">
                    <div class="cart">
                        <i class="feather ecommerce-application mr-25"></i>  <a href="<?=base_url();?>manage_kelompok/detail_produk" class="view-in-cart"><span class="">Detail produk</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ecommerce-card">
            <div class="card-content">
                <div class="item-img text-center">
                    <a href="app-ecommerce-details.html"><img class="img-fluid" src="<?=base_url();?>data/produk/produk3.jpg" alt="img-placeholder"></a>
                </div>
                  <div class="card-body">
                    <div class="item-wrapper">
                        <div class="item-rating">
                            <div class="badge badge-primary badge-md">
                                <span>Buah-buahan</span>
                            </div>
                        </div>
                        <div>
                            <h6 class="item-price">
                                Rp. 20.000
                            </h6>
                        </div>
                    </div>
                    <div class="item-name">
                        <a href="app-ecommerce-details.html">Mangga Cristal</a>

                    </div>
                    <div>
                        <p class="item-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        </p>
                    </div>
                </div>
                <div class="item-options text-center">
                    <div class="cart">
                        <i class="feather ecommerce-application mr-25"></i>  <a href="<?=base_url();?>manage_kelompok/detail_produk" class="view-in-cart"><span class="">Detail produk</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ecommerce-card">
            <div class="card-content">
                <div class="item-img text-center">
                    <a href="app-ecommerce-details.html"> <img class="img-fluid" src="<?=base_url();?>data/produk/produk4.jpg" alt="img-placeholder"></a>
                </div>
                   <div class="card-body">
                    <div class="item-wrapper">
                        <div class="item-rating">
                            <div class="badge badge-primary badge-md">
                                <span>Buah-buahan</span>
                            </div>
                        </div>
                        <div>
                            <h6 class="item-price">
                                Rp. 15.000
                            </h6>
                        </div>
                    </div>
                    <div class="item-name">
                        <a href="app-ecommerce-details.html">Jeruk</a>

                    </div>
                    <div>
                        <p class="item-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        </p>
                    </div>
                </div>
                <div class="item-options text-center">
                    <div class="cart">
                        <i class="feather ecommerce-application mr-25"></i>  <a href="<?=base_url();?>manage_kelompok/detail_produk" class="view-in-cart"><span class="">Detail produk</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card ecommerce-card">
            <div class="card-content">
                <div class="item-img text-center">
                    <a href="app-ecommerce-details.html">
                        <img class="img-fluid" src="<?=base_url();?>data/produk/produk5.jpg" alt="img-placeholder">
                    </a>
                </div>
                 <div class="card-body">
                    <div class="item-wrapper">
                        <div class="item-rating">
                            <div class="badge badge-primary badge-md">
                                <span>Sayuran</span>
                            </div>
                        </div>
                        <div>
                            <h6 class="item-price">
                                Rp. 5.000
                            </h6>
                        </div>
                    </div>
                    <div class="item-name">
                        <a href="app-ecommerce-details.html">Cabai Merah</a>

                    </div>
                    <div>
                        <p class="item-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                        </p>
                    </div>
                </div>
                <div class="item-options text-center">
                    <div class="cart">
                        <i class="feather ecommerce-application mr-25"></i>  <a href="<?=base_url();?>manage_kelompok/detail_produk" class="view-in-cart"><span class="">Detail produk</span></a>
                    </div>
                </div>
            </div>
        </div>


    </section>



</div>
</section>
<!-- page users view end -->




</div>
</div>
</div>
    <!-- END: Content-->
