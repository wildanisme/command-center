


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

      <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
              <div class="media-body"><span class="m-0">Jumlah Parpol : </span>
                <h4 class="mb-0 counter">
					<?php echo $parpol->total ?> 
            </h4><i class="icon-bg" data-feather="user-plus"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
    <div class="card gradient-primary o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
          <div class="media-body"><span class="m-0 text-white">Jumlah jenis usaha</span>
            <h4 class="mb-0 counter"><?php foreach ($jumusaha as $data): ?>
          
				<?php echo $data->total ?> 
            <?php endforeach; ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
    <div class="card gradient-warning o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center">
            <div class="text-white i" data-feather="database"></div>
          </div>
          <div class="media-body"><span class="m-0 text-white">jumlah aset</span>
           <h4 class="mb-0 counter text-white"> <?php foreach ($jumaset as $data): ?>

          
			 RP. <?php echo  number_format($data->total,2,',','.') ?> 
			   
			   <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Jumlah hasil</span>
          <h4 class="mb-0 counter text-white"><?php foreach ($jumahasil as $data): ?>

            
			 RP. <?php echo  number_format($data->total,2,',','.') ?>   
			  <?php endforeach; ?> </h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div> -->


<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Statistik PARPOL Per Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div>

<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Info grafis PARPOL Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="peta"></div>
    </div>
  </div>
</div>

<div class="col-lg-12 xl-100">
  <div class="row">

    <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
      <div class="card">
        <div class="card-header">
          <h5>Chart Pemilik Berdasarkan jenis kelamin </h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="piechart"></div>
        </div>
      </div>
    </div>


    <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
      <div class="card">
        <div class="card-header">
          <h5>Top Jenis Usaha</h5>
          <div class="card-header-right">

          </div>
        </div>
        <div class="card-body p-0">

          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone">
              <thead>
                <tr>
                  <th scope="col">Jenis Usaha</th>
                  <th scope="col">jumlah</th>

                </tr>
              </thead>
				
              <?php foreach ($kategori as $data): ?>
                <tbody>
                  <tr> <td class="f-w-600">    <?php echo $data->jenis?> </td>
                    <td class="digits">
					<?php echo $data->total ?> 
				
                  </tr></tbody>
                <?php endforeach; ?>
              </table>
            </div>

          </div>
        </div>
      </div>


    </div>
  </div>






  <div class="col-lg-12 xl-100">
    <div class="row">

      <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
        <div class="card">
          <div class="card-header">
            <h5>TOP Aset PARPOL </h5>
          </div>
          <div class="card-body p-0">

            <div class="user-status cart-table table-responsive">
              <table class="table table-bordernone">
                <thead>
                  <tr>
                    <th scope="col">nama parpol</th>
                    <th scope="col">Jenis Usaha</th>
                    <th scope="col">jumlah</th>

                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($asset as $data): ?>
                    <tbody>
                      <tr> 
                        <td class="f-w-600">    <?php echo $data->nama_umkm ?> </td>
                        <td>    <?php echo $data->jenis?> </td>
                        <td class="digits">
							 RP. <?php echo  number_format($data->asset,2,',','.') ?> 
							
						  </td>
                      </tr></tbody>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <div>
              </div>
            </div>
          </div>
        </div>


        <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
          <div class="card">
            <div class="card-header">
              <h5>Top Hasil PARPOL</h5>
              <div class="card-header-right">

              </div>
            </div>
            <div class="card-body p-0">

              <div class="user-status cart-table table-responsive">
                <table class="table table-bordernone">
                  <thead>
                    <tr>
                      <th scope="col">nama parpol</th>
                      <th scope="col">Jenis Usaha</th>
                      <th scope="col">jumlah</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($hasil as $data): ?>
                      <tbody>
                        <tr> 
                          <td class="f-w-600">    <?php echo $data->nama_umkm ?> </td>
                          <td>    <?php echo $data->jenis?> </td>
                          <td class="digits">
							   RP. <?php echo  number_format($data->hasil_usaha,2,',','.') ?> 
							  
							</td>
                        </tr></tbody>
                      <?php endforeach; ?>

                    </tbody>
                  </table>
                </div>
                <div>
                </div>


              </div>
            </div>









          </div>

        </div>
      </div>



<!-- Plugins JS start-->
<!-- Plugins JS start-->
<script>
// column chart
var options3 = {
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      horizontal: false,
      endingShape: 'rounded',
      columnWidth: '55%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series: [{
    name: 'Jumlah',
    data: [<?php foreach ($jumkec as $data): ?>
      <?php echo $data->totalumkm.","?> 
      <?php endforeach; ?>]


    }],
    xaxis: {
      categories: [<?php foreach ($jumkec as $data): ?>
        <?php echo "'" .$data->kecamatan. "',"?> 
        <?php endforeach; ?>],
      },
      yaxis: {
        title: {
          text: 'Jumlah UMKM'
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function (val) {
            return "" + val + " UKMM"

          }


        }
      },
      colors:['#80cf00', '#fe80b2', '#80cf00']
    }

    var chart3 = new ApexCharts(
      document.querySelector("#column-chart"),
      options3
      );

    chart3.render();

  </script>

  <script>
// pie chart
var options8 = {
  chart: {
    width: 380,
    type: 'pie',
  },
  labels: ['Perempuan', 'Laki-laki'],
  series: [<?php foreach ($jk as $data): ?>
    <?php echo $data->totalwanita.","?> 
    <?php echo $data->totallaki?>
    <?php endforeach; ?>],
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 300
        },
        legend: {
          position: 'bottom'
        }
      }
    }],
    colors:['#fe80b2', '#06b5dd']
  }

  var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
    );

  chart8.render();

</script>

<script type="text/javascript">

  var mapOptions = {
             center: [-6.821018391805014, 107.94189545895269],
             zoom: 20
            }
            var peta = new L.map('peta', mapOptions);

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 11,
              id: 'mapbox/streets-v11',
              tileSize: 512,
              zoomOffset: -1,
              accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(peta);
            
        <?php foreach ($gerai as $data): ?>



          var marker = new L.Marker([<?php echo $data->lat?>, <?php echo $data->lng?>]);
          marker.bindPopup("<b><?php echo $data->nama?></b>.").openPopup();
          marker.addTo(peta);
        <?php endforeach; ?>
        
        
      </script>



<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>