
<?php foreach ($bidang as $data) : ?>
    <?php if (strpos($data->bidang, 'Pemerintahan')) { ?>
        <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
            <div class="card gradient-primary o-hidden">
                <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center"><i data-feather="database"></i></div>
                        <div class="media-body"><a href="#section5"><span class="m-0 text-white"><?php echo $data->bidang; ?></span> </a>
                            <h4 class="mb-0 counter"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="database"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php endforeach; ?>

<?php foreach ($bidang as $data) : ?>
    <?php if (strpos($data->bidang, 'Luar Negeri')) { ?>
        <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
            <div class="card gradient-secondary o-hidden">
                <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                        <div class="media-body"><a href="#section6"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                            <h4 class="mb-0 counter"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php endforeach; ?>

<?php foreach ($bidang as $data) : ?>
    <?php if (strpos($data->bidang, 'Swasta')) { ?>
        <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
            <div class="card gradient-warning o-hidden">
                <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center">
                            <div class="text-white i" data-feather="message-circle"></div>
                        </div>
                        <div class="media-body"><a href="#section7"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                            <h4 class="mb-0 counter text-white"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="message-circle"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php endforeach; ?>

<?php foreach ($bidang as $data) : ?>
    <?php if (strpos($data->bidang, 'Pendidikan')) { ?>
        <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
            <div class="card gradient-info o-hidden">
                <div class="b-r-4 card-body">
                    <div class="media static-top-widget">
                        <div class="align-self-center text-center">
                            <div class="text-white i" data-feather="user-plus"></div>
                        </div>
                        <div class="media-body"><a href="#section8"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                            <h4 class="mb-0 counter text-white"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="user-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php endforeach; ?>


<!-- <div class="col-md-12">
    <div class="card">

        <div class="card-body">
            <form class="needs-validation" novalidate="" action="<?php echo site_url('kerjasama/filter'); ?>" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-5">
                        <label class="mr-sm-2" for="inlineFormCustomSelect">Filter Tahun</label>
                        <select class="form-control" id=" inlineFormCustomSelect" name="tahun" required="">
                            <?php if ($pilih_tahun == "") { ?>
                                <option hidden></option>
                            <?php } else { ?>
                                <option value="<?php echo $pilih_tahun ?>"><?php echo $pilih_tahun ?></option>
                            <?php } ?>

                            <?php foreach ($filter_tahun as $tahun) : ?>
                                <option value="<?php echo $tahun->year ?>"><?php echo $tahun->year ?></option>
                            <?php endforeach; ?>
                            <option value="Semua">Semua</option>
                        </select>
                        <div class="invalid-feedback">Mohon Pilih Tahun</div>
                    </div>
                    <div class="col-md-5">
                        <label class="mr-sm-2" for="inlineFormCustomSelect">Filter Status</label>
                        <select class="form-control" id="inlineFormCustomSelect" name="status" required="">
                            <?php if ($pilih_status == "") { ?>
                                <option hidden></option>
                            <?php } else { ?>
                                <option value="<?php echo $pilih_status ?>"><?php echo $pilih_status ?></option>
                            <?php } ?>
                            <?php foreach ($filter_status as $data) : ?>
                                <option value="<?php echo $data->nama_status ?>"><?php echo $data->nama_status ?></option>
                            <?php endforeach; ?>
                            <option value="Semua">Semua</option>
                        </select>
                        <div class="invalid-feedback">Mohon Pilih Status</div>
                    </div>
                    <div class="row align-items-end">
                        <div class="col">
                            <input class="btn btn-success" type="submit" name="btn" value="Filter" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> -->

<div id="section2" class="col-lg-6 col-md-6 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Statistik Bidang Kerjasama  test </h5>
        </div>
        <div class="card-body apex-chart p-0">
            <div id="piechart"></div>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Statistik Status Kerjasama</h5>
        </div>
        <div class="card-body apex-chart p-0">
            <div id="donutchart"></div>
        </div>
    </div>
</div>


<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Statistik Kerjasama SKPD</h5>
        </div>
        <div class="card-body p-0">
            <div id="basic-bar"></div>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Galeri Kerjasama Terbaru</h5>
        </div>
        <div class="card-body p-0">
            <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="<?php echo base_url('uploads/kerjasama/333.jpg') ?>" alt="Los Angeles">
                    </div>
                    <?php foreach ($galeri_new as $data) : ?>
                        <div class="carousel-item">
                            <img src="<?php echo base_url('uploads/kerjasama/' . $data->galeri); ?>" alt="Chicago">
                        </div>
                    <?php endforeach ?>
                </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
    <div class="card">
        <div class="card-header">
            <h5>Galeri Kerjasama Selesai</h5>
        </div>
        <div class="card-body p-0">
            <div id="demo2" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo2" data-slide-to="0" class="active"></li>
                    <li data-target="#demo2" data-slide-to="1"></li>
                    <li data-target="#demo2" data-slide-to="2"></li>
                </ul>
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="<?php echo base_url('uploads/kerjasama/333.jpg') ?>" alt="Los Angeles">
                    </div>
                    <?php foreach ($galeri_finish as $data) : ?>
                        <div class="carousel-item">
                            <img src="<?php echo base_url('uploads/kerjasama/' . $data->galeri); ?>" alt="Chicago">
                        </div>
                    <?php endforeach ?>
                </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo2" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo2" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
        <div class="card-footer"></div>
    </div>
</div>


                <!-- Table On Going Starts-->
                <div id="section3" class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Tabel Kerjasama Status On Going</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display datatables">
                                    <thead>
                                        <tr>
                                            <th>No Surat Kerjasama</th>
                                            <th>Judul kerjasama</th>
                                            <th>Pihak Pertama</th>
                                            <th>Pihak Kedua</th>
                                            <th>Tanggal</th>
                                            <th>Bidang</th>
                                            <th>Status</th>
                                            <th>Deskripsi</th>
                                            <th>Dokumen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($tbl_kerjasama as $kerjasama) : ?>
                                            <?php if ($kerjasama->status == "On Going") { ?>
                                                <tr>
                                                    <td width="150">
                                                        <?php echo $kerjasama->no_surat_ket ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->judul_kerjasama ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->pihak_pertama ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->pihak_kedua ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->tanggal ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->bidang ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->status ?>
                                                    </td>
                                                    <td class="small">
                                                        <?php echo substr($kerjasama->deskripsi, 0, 120) ?></td>
                                                    <td>
                                                        <a href="<?php echo site_url('uploads/kerjasama/' . $kerjasama->dokumen) ?>" class="btn btn-small"><i class="fa fa-download"></i> Download</a>
                                                    </td>

                                                </tr>
                                            <?php } ?>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Table On GOing  Ends-->

                <!-- Table  Starts-->
                <div id="section4" class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Tabel Kerjasama</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display datatables">
                                    <thead>
                                        <tr>
                                            <th>No Surat Kerjasama</th>
                                            <th>Judul kerjasama</th>
                                            <th>Pihak Pertama</th>
                                            <th>Pihak Kedua</th>
                                            <th>Tanggal</th>
                                            <th>Bidang</th>
                                            <th>Status</th>
                                            <th>Deskripsi</th>
                                            <th>Dokumen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($tbl_kerjasama as $kerjasama) : ?>
                                            <tr>
                                                <td width="150">
                                                    <?php echo $kerjasama->no_surat_ket ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->judul_kerjasama ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->pihak_pertama ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->pihak_kedua ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->tanggal ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->bidang ?>
                                                </td>
                                                <td>
                                                    <?php echo $kerjasama->status ?>
                                                </td>
                                                <td class="small">
                                                    <?php echo substr($kerjasama->deskripsi, 0, 120) ?></td>
                                                <!-- <td>
                                                    <img src="<?php echo base_url('upload/' . $kerjasama->galeri) ?>" width="64" />
                                                </td> -->
                                                <td>
                                                    <a href="<?php echo site_url('uploads/kerjasama/' . $kerjasama->dokumen) ?>" class="btn btn-small"><i class="fa fa-download"></i> Download</a>
                                                </td>

                                            </tr>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Table  Ends-->

        <script>
        // donut chart
        var options9 = {
            chart: {
                width: 380,
                height: 300,
                type: 'donut',
            },
            series: [<?php foreach ($status as $data) : ?>
                    <?php echo $data->total; ?>,
                <?php endforeach; ?>
            ],
            labels: [<?php foreach ($status as $data) : ?> '<?php echo $data->status; ?>',
                <?php endforeach; ?>
            ],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart9 = new ApexCharts(
            document.querySelector("#donutchart"),
            options9
        );

        chart9.render();
    </script>

    <script>
        // pie chart
        var options8 = {
            chart: {
                width: 500,
                height: 300,
                type: 'pie',
            },
            labels: [<?php foreach ($bidang as $data) : ?> '<?php echo $data->bidang; ?>',
                <?php endforeach; ?>
            ],
            series: [<?php foreach ($bidang as $data) : ?> <?php echo $data->total; ?>,
                <?php endforeach; ?>
            ],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart8 = new ApexCharts(
            document.querySelector("#piechart"),
            options8
        );

        chart8.render();
    </script>

    <script>
        // basic bar chart
        var options2 = {
            chart: {
                height: 380,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                }
            },
            dataLabels: {
                enabled: true
            },
            series: [{
                data: [<?php foreach ($skpd as $data) : ?> <?php echo $data->total; ?>,
                    <?php endforeach; ?>
                ]
            }],

            labels: [<?php foreach ($skpd as $data) : ?> '<?php echo $data->skpd; ?>',
                <?php endforeach; ?>
            ],

            xaxis: {
                categories: [<?php foreach ($skpd as $data) : ?> '<?php echo $data->skpd; ?>',
                    <?php endforeach; ?>
                ],
            },
            colors: ['#7e37d8']
        }

        var chart2 = new ApexCharts(
            document.querySelector("#basic-bar"),
            options2
        );
        chart2.render();
    </script>

    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 350px;
        }
    </style>
