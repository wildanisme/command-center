<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="Dashboard Bogor">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?=base_url();?>assets/images/logo/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo/icon.png" type="image/x-icon">
    <title>Kerjasama</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/owlcarousel.css">

     <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/date-picker.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/prism.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/material-design-icon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/select2.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/pe7-icon.css">
    <!-- Plugins css Ends-->

     <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/datatable-extension.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/pe7-icon.css">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/bootstrap.css">
    <!-- App css-->

    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/style.css">

    <link id="color" rel="stylesheet" href="<?=base_url();?>assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/responsive.css">

    
  </head>