<!-- latest jquery-->
<script src="<?= base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap js-->
<script src="<?= base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
<script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="<?= base_url(); ?>assets/js/sidebar-menu.js"></script>
<script src="<?= base_url(); ?>assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="<?= base_url(); ?>assets/js/typeahead/handlebars.js"></script>
<script src="<?= base_url(); ?>assets/js/typeahead/typeahead.bundle.js"></script>
<script src="<?= base_url(); ?>assets/js/typeahead/typeahead.custom.js"></script>
<script src="<?= base_url(); ?>assets/js/typeahead-search/handlebars.js"></script>
<script src="<?= base_url(); ?>assets/js/typeahead-search/typeahead-custom.js"></script>
<script src="<?= base_url(); ?>assets/js/chart/chartist/chartist.js"></script>
<script src="<?= base_url(); ?>assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<script src="<?= base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
<script src="<?= base_url(); ?>assets/js/chart/apex-chart/stock-prices.js"></script>
<script src="<?= base_url(); ?>assets/js/prism/prism.min.js"></script>
<script src="<?= base_url(); ?>assets/js/clipboard/clipboard.min.js"></script>
<script src="<?= base_url(); ?>assets/js/counter/jquery.waypoints.min.js"></script>
<script src="<?= base_url(); ?>assets/js/counter/jquery.counterup.min.js"></script>
<script src="<?= base_url(); ?>assets/js/counter/counter-custom.js"></script>
<script src="<?= base_url(); ?>assets/js/custom-card/custom-card.js"></script>
<script src="<?= base_url(); ?>assets/js/notify/bootstrap-notify.min.js"></script>
<script src="<?= base_url(); ?>assets/js/notify/index.js"></script>
<script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.js"></script>
<script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.en.js"></script>
<script src="<?= base_url(); ?>assets/js/datepicker/date-picker/datepicker.custom.js"></script>

<script src="<?= base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/datatable/datatable-extension/buttons.html5.min.js"></script>
<script src="<?= base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>
<script src="<?= base_url(); ?>assets/js/chat-menu.js"></script>

<!-- Plugins JS Ends-->

<!-- Plugins JS start-->

<script src="<?= base_url(); ?>assets/js/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url(); ?>assets/js/owlcarousel/owl.carousel.js"></script>
<script src="<?= base_url(); ?>assets/js/select2/select2.full.min.js"></script>
<script src="<?= base_url(); ?>assets/js/select2/select2-custom.js"></script>

<!-- Plugins JS Ends-->


<!-- Theme js-->
<script src="<?= base_url(); ?>assets/js/script.js"></script>
<script src="<?= base_url(); ?>assets/js/theme-customizer/customizer.js"></script>
<!-- login js-->
<!-- Plugin used-->
<script src="<?= base_url(); ?>assets/js/jquery.inputmask.bundle.min.js"></script>

<script src="<?= base_url(); ?>assets/js/jquery.scrollTo.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>
<script src="<?= base_url(); ?>assets/js/form-validation-custom.js"></script>


<script type="text/javascript">
    function load_dashboard(dashboard) {
        $.ajax({
            url: "<?= base_url($this->router->fetch_class()) ?>/dashboard",
            type: "GET",
            dataType: "HTML",
            beforeSend: function() {
                notif_load();
            },
            success: function(data) {
                $('#' + dashboard + '-dashboard').html(data);
                load_script();
                $.notifyClose();
                notif_load_success();
                setAutoScroll();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $.notifyClose();
                notif_load_error();
                setTimeout(() => {
                    $.ajax(this)
                }, 10000);
            }
        });
    }

    function load_script() {
        $('.datatables').DataTable();
        $('.datatables-mini').DataTable({
            dom: 't',
            responsive: true,
            pageLength: 1,
            // paging:   false,
            ordering: false,
            info: false
        });
        $('#footer-mini').css('padding-left', '90px');
        $('#datatable-export').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
        $(".select2").select2();
        $(".currency").inputmask({
            alias: "currency",
            prefix: 'Rp',
            removeMaskOnSubmit: true
        });
        feather.replace();
    }

    load_script();
</script>

<script type="text/javascript">
    function notif_load() {
        $.notify({
            title: 'Mengambil data...',
            message: '',
            icon: 'icon fa fa-spin fa-circle-o-notch'
        }, {
            type: 'primary',
            allow_dismiss: false,
            newest_on_top: false,
            mouse_over: false,
            showProgressbar: false,
            spacing: 10,
            timer: 0,
            placement: {
                from: 'bottom',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animated pulse',
                exit: 'animated fadeOut'
            }
        });
    }

    function notif_load_success() {
        $.notify({
            title: 'Data berhasil diperbarui...',
            message: '',
            icon: 'icon fa fa-check-square'
        }, {
            type: 'success',
            allow_dismiss: true,
            newest_on_top: false,
            mouse_over: true,
            showProgressbar: false,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animated pulse',
                exit: 'animated fadeOut'
            }
        });
    }

    function notif_load_error() {
        $.notify({
            title: 'Gagal mendapatkan data...',
            message: '',
            icon: 'icon fa fa-warning'
        }, {
            type: 'secondary',
            allow_dismiss: true,
            newest_on_top: false,
            mouse_over: true,
            showProgressbar: false,
            spacing: 10,
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            },
            offset: {
                x: 30,
                y: 30
            },
            delay: 1000,
            z_index: 10000,
            animate: {
                enter: 'animated pulse',
                exit: 'animated fadeOut'
            }
        });
    }
</script>

<script type="text/javascript">
    var timeout = false;
    var autopage = false;
    var autotable = false;
    var Height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var currentHeight = document.documentElement.scrollTop;
    var bool = true;
    var step = 1;
    var speed = 10;

    function setAutoScroll() {
        if (timeout) clearTimeout(timeout);
        if (autopage) clearInterval(autopage);
        if (autotable) clearInterval(autotable);
        $('#footer-mini').fadeOut();
        timeout = setTimeout(userIsIdle, 60000); //10 menit
    }

    function userIsIdle() {
        Height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        currentHeight = document.documentElement.scrollTop;
        autopage = setInterval(scrollpage, speed);
        autotable = setInterval(scrolldatatable, 5000);
        $('#footer-mini').fadeIn();
    }

    function scrollpage() {
        if (currentHeight < 0 || currentHeight > Height)
            bool = !bool;
        if (bool) {
            window.scrollTo(0, currentHeight += step);
        } else {
            window.scrollTo(0, currentHeight -= step);
        }
    }

    function scrolldatatable() {
        var datatable = $('.datatables').DataTable();
        var info = datatable.page.info();
        var pageNum = (info.page < info.pages) ? info.page + 1 : 1;
        datatable.page(pageNum).draw(false);

        var datatable_mini = $('.datatables-mini').DataTable();
        var info = datatable_mini.page.info();
        var pageNum = (info.page < info.pages) ? info.page + 1 : 1;
        datatable_mini.page(pageNum).draw(false);
    }
</script>

<script type="text/javascript" defer>
    $(document).ready(function() {
        if (dashboard) {
            load_dashboard(dashboard);
            setInterval(function() {
                load_dashboard(dashboard);
            }, 600000);

            timeout = setTimeout(userIsIdle, 10000);
            $(document.body).bind('keydown keyup mousemove mouseup scroll touchstart touchmove click', function() {
                setAutoScroll();
            });
        }
    });
</script>