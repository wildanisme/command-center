<?php $this->load->view('admin/kerjasama/admin/_partials/head'); ?>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/kerjasama/admin/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header sub-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>kerjasama/admin">Tabel Kerjasama</a></li>
                                <li class="iconbar-header sub-header">Parameter</li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterSkpd">SKPD</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterBidang">Bidang</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterStatus">Status</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->

            <div class="page-body">

                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Master<span>SKPD </span></h2>
                                <h6 class="mb-0">admin panel</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">Default </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Content Row -->

                <!-- DataTables -->
                <div class="row">
                    <!-- Zero Configuration  Starts-->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="<?php echo site_url('kerjasama/addParameterSkpd') ?>"><i class="fa fa-plus"></i> Tambah Baru</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display datatables ">
                                        <thead>
                                            <tr>
                                                <th>Nama SKPD</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($skpd as $data_skpd) : ?>
                                                <tr>
                                                    <td width="150">
                                                        <?php echo $data_skpd->nama_skpd ?>
                                                    </td>
                                                    <td width="250">
                                                        <a href="<?php echo site_url('kerjasama/editParameterSkpd/' . $data_skpd->id_skpd) ?>" class="btn btn-small"><i class="fa fa-edit"></i> Edit</a>
                                                        <a onclick="deleteConfirm('<?php echo site_url('kerjasama/deleteParameterSkpd/' . $data_skpd->id_skpd) ?>')" href="#!" class="btn btn-small text-danger"><i class="fa fa-trash"></i> Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End of Main Content -->

                <!-- footer start-->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 footer-copyright">
                                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        </div>

        <?php $this->load->view('admin/kerjasama/admin/_partials/bottom'); ?>
        <?php $this->load->view('admin/kerjasama/admin/_partials/logout_modal'); ?>
        <script>
            function deleteConfirm(url) {
                $('#btn-delete').attr('href', url);
                $('#deleteModal').modal();
            }
        </script>
