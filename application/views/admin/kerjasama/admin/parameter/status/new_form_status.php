<?php $this->load->view('admin/_partials/head'); ?>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header sub-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>admin">Tabel Kerjasama</a></li>
                                <li class="iconbar-header sub-header">Parameter</li>
                                <li><a href="<?= base_url(); ?>admin/parameter/Skpd">SKPD</a></li>
                                <li><a href="<?= base_url(); ?>admin/parameter/Bidang">Bidang</a></li>
                                <li><a href="<?= base_url(); ?>admin/parameter/Status">Status</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->

            <div class="page-body">

                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Master<span>Status </span></h2>
                                <h6 class="mb-0">Tambah Data</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">Default </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Content Row -->

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <div class="card mb-3">
                    <div class="card-header">
                        <a href="<?php echo site_url('admin/parameter/status/') ?>"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="card-body">

                        <form class="needs-validation" novalidate="" action="<?php echo site_url('admin/parameter/status/save') ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="nama_status">Nama Status*</label>
                                <input class="form-control" type="text" name="nama_status" required="" />
                                <div class="invalid-feedback">Mohon Isi Status</div>

                            </div>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </form>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- footer start-->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 footer-copyright">
                            <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
                        </div>
                        <div class="col-md-6">
                            <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                        </div>
                    </div>
                </div>
            </footer>

            <?php $this->load->view('admin/_partials/bottom'); ?>

        </div>
    </div>

</body>

</html>