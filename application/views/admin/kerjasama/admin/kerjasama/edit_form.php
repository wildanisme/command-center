<?php $this->load->view('admin/kerjasama/admin/_partials/head'); ?>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/kerjasama/admin/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header sub-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>kerjasama/admin">Tabel Kerjasama</a></li>
                                <li class="iconbar-header sub-header">Parameter</li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterSkpd">SKPD</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterBidang">Bidang</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterStatus">Status</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->

            <div class="page-body">

                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Data<span>Kerjasama </span></h2>
                                <h6 class="mb-0">Edit Data</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">Default </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Content Row -->

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <!-- Card  -->
                <div class="card mb-3">
                    <div class="card-header">

                        <a href="<?php echo site_url('kerjasama/admin') ?>"><i class="fa fa-arrow-left"></i>
                            Back</a>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo site_url('kerjasama/updateKerjasama') ?>" method="post" enctype="multipart/form-data" class="needs-validation" novalidate="">
                            <input type="hidden" name="id" value="<?php echo $tbl_kerjasama->id_surat ?>" />
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="no_surat_ket">No Surat Kerjasama*</label>
                                        <input class="form-control" type="text" name="no_surat_ket" value="<?php echo $tbl_kerjasama->no_surat_ket ?>" required="" />
                                        <div class="invalid-feedback">Mohon Isi No Surat Kerjasama</div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="no_surat_ket">Nama kerjasama*</label>
                                        <input class="form-control" type="text" name="judul_kerjasama" value="<?php echo $tbl_kerjasama->judul_kerjasama ?>" required="" />
                                        <div class="invalid-feedback">Mohon Isi Nama Kerjasama</div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Pihak Pertama*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="pihak_pertama" required="">
                                            <option value="<?php echo $tbl_kerjasama->pihak_pertama ?>"><?php echo $tbl_kerjasama->pihak_pertama ?></option>
                                            <?php foreach ($data_skpd as $skpd) : ?>
                                                <!-- kondisi if untuk memfilter supaya value sebelumnya tidak muncul double di listnya -->
                                                <?php if ($skpd->nama_skpd != $tbl_kerjasama->pihak_pertama) { ?>
                                                    <option value="<?php echo $skpd->nama_skpd ?>"><?php echo $skpd->nama_skpd ?></option>
                                                <?php } ?>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Pihak Pertana</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="pihak_kedua">Pihak Kedua*</label>
                                        <input class="form-control" type="text" name="pihak_kedua" value="<?php echo $tbl_kerjasama->pihak_kedua ?>" required="" />
                                        <div class="invalid-feedback">Mohon Isi Pihak Kedua</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tanggal">Tanggal Mulai*</label>
                                        <input class="form-control" type="date" name="tanggal" value="<?php echo $tbl_kerjasama->tanggal ?>" required="" />
                                        <div class="invalid-feedback">Mohon Pilih Tanggal Mulai</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tanggal_selesai">Tanggal Selesai*</label>
                                        <input class="form-control" type="date" name="tanggal_selesai" value="<?php echo $tbl_kerjasama->tanggal_selesai ?>" required="" />
                                        <div class="invalid-feedback">Mohon Pilih Tanggal Selesai</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Bidang*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="bidang" required="">
                                            <option value="<?php echo $tbl_kerjasama->bidang ?>"><?php echo $tbl_kerjasama->bidang ?></option>
                                            <?php foreach ($data_bidang as $bidang) : ?>
                                                <!-- kondisi if untuk memfilter supaya value sebelumnya tidak muncul double di listnya -->
                                                <?php if ($bidang->nama_bidang != $tbl_kerjasama->bidang) { ?>
                                                    <option value="<?php echo $bidang->nama_bidang ?>"><?php echo $bidang->nama_bidang ?></option>
                                                <?php } ?>

                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Bidang</div>

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Status*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="status" required="">
                                            <option value="<?php echo $tbl_kerjasama->status ?>"><?php echo $tbl_kerjasama->status ?></option>
                                            <?php foreach ($data_status as $status) : ?>
                                                <!-- kondisi if untuk memfilter supaya value sebelumnya tidak muncul double di listnya -->
                                                <?php if ($status->nama_status != $tbl_kerjasama->status) { ?>
                                                    <option value="<?php echo $status->nama_status ?>"><?php echo $status->nama_status ?></option>
                                                <?php } ?>
                                            <?php endforeach; ?>

                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Status</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">Deskripsi</label>
                                        <textarea class="form-control" name="deskripsi"><?php echo $tbl_kerjasama->deskripsi; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="galeri">Galeri</label>
                                        <input class="form-control-file" type="file" name="galeri" />
                                        <input name="old_galeri" value="<?php echo $tbl_kerjasama->galeri; ?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="dokumen">Dokumen</label>
                                        <input class="form-control-file" type="file" name="dokumen" />
                                        <input name="old_dokumen" value="<?php echo $tbl_kerjasama->dokumen; ?>" />
                                    </div>
                                </div>
                            </div>

                            <input class="btn btn-success" type="submit" name="btn" value="Update" />
                        </form>

                    </div>


                </div>

                <!-- End of Main Content -->

                <!-- footer start-->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 footer-copyright">
                                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                            </div>
                        </div>
                    </div>
                </footer>

                <?php $this->load->view('admin/kerjasama/admin/_partials/bottom'); ?>

            </div>
        </div>