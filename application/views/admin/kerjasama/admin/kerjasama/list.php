<?php $this->load->view('admin/kerjasama/admin/_partials/head'); ?>

<body style="
    margin-left: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
">    
	<!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/kerjasama/admin/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header sub-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>kerjasama/admin">Tabel Kerjasama</a></li>
                                <li class="iconbar-header sub-header">Parameter</li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterSkpd">SKPD</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterBidang">Bidang</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterStatus">Status</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->

            <div class="page-body">

                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Data<span>Kerjasama </span></h2>
                                <h6 class="mb-0">admin panel</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">Default </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Widget Bidang-->
                <div class="container-fluid general-widget">
                    <div class="row">
                        <?php foreach ($bidang as $data) : ?>
                            <?php if (strpos($data->bidang, 'Pemerintahan')) { ?>
                                <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                                    <div class="card gradient-primary o-hidden">
                                        <div class="b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="align-self-center text-center"><i data-feather="database"></i></div>
                                                <div class="media-body"><a href="#section5"><span class="m-0 text-white"><?php echo $data->bidang; ?></span> </a>
                                                    <h4 class="mb-0 counter"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="database"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endforeach; ?>

                        <?php foreach ($bidang as $data) : ?>
                            <?php if (strpos($data->bidang, 'Luar Negeri')) { ?>
                                <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                                    <div class="card gradient-secondary o-hidden">
                                        <div class="b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
                                                <div class="media-body"><a href="#section6"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                                                    <h4 class="mb-0 counter"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endforeach; ?>

                        <?php foreach ($bidang as $data) : ?>
                            <?php if (strpos($data->bidang, 'Swasta')) { ?>
                                <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                                    <div class="card gradient-warning o-hidden">
                                        <div class="b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="align-self-center text-center">
                                                    <div class="text-white i" data-feather="message-circle"></div>
                                                </div>
                                                <div class="media-body"><a href="#section7"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                                                    <h4 class="mb-0 counter text-white"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="message-circle"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endforeach; ?>

                        <?php foreach ($bidang as $data) : ?>
                            <?php if (strpos($data->bidang, 'Pendidikan')) { ?>
                                <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
                                    <div class="card gradient-info o-hidden">
                                        <div class="b-r-4 card-body">
                                            <div class="media static-top-widget">
                                                <div class="align-self-center text-center">
                                                    <div class="text-white i" data-feather="user-plus"></div>
                                                </div>
                                                <div class="media-body"><a href="#section8"><span class="m-0 text-white"><?php echo $data->bidang; ?></span></a>
                                                    <h4 class="mb-0 counter text-white"><?php echo $data->total; ?></h4><i class="icon-bg" data-feather="user-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- End Widget Bidang-->

                <!--FILTER-
                <div id="section3">
                    <div class="card-body">
                        <div class="col-sm-12 col-xl-6 xl-100">
                            <form action="<?php echo site_url('admin/kerjasama/filter'); ?>" method="post" enctype="multipart/form-data">
                                <div class="col-auto my-1">
                                    <label class="mr-sm-2" for="inlineFormCustomSelect">Filter Tahun</label>
                                    <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="tanggal" placeholder="-Pilih Tahun-">
                                        <option hidden>Pilih Tahun</option>
                                        <?php foreach ($tahuns as $tahun) : ?>
                                            <option value="<?php echo $tahun->year ?>"><?php echo $tahun->year ?></option>
                                        <?php endforeach; ?>
                                        <option value="all">Semua</option>
                                    </select>
                                    <input class="btn btn-success" type="submit" name="btn" value="Filter" />

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                END FILTER-->

                <div class="row">
                    <!-- Zero Configuration  Starts-->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="<?php echo site_url('kerjasama/addKerjasama') ?>"><i class="fa fa-plus"></i> Tambah Baru</a>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display datatables">
                                        <thead>
                                            <tr>
                                                <th>No Surat Kerjasama</th>
                                                <th>Judul kerjasama</th>
                                                <th>Pihak Pertama</th>
                                                <th>Pihak Kedua</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Tanggal Selesai</th>
                                                <th>Bidang</th>
                                                <th>Status</th>
                                                <th>Deskripsi</th>
                                                <th>Galeri</th>
                                                <th>Dokumen</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($tbl_kerjasama as $kerjasama) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kerjasama->no_surat_ket ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->judul_kerjasama ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->pihak_pertama ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->pihak_kedua ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->tanggal ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->tanggal_selesai ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->bidang ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kerjasama->status ?>
                                                    </td>
                                                    <td class="small">
                                                        <?php echo substr($kerjasama->deskripsi, 0, 300) ?></td>
                                                    <td>
                                                        <img src="<?php echo base_url('uploads/kerjasama/' . $kerjasama->galeri) ?>" width="64" />
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url('uploads/kerjasama/' . $kerjasama->dokumen) ?>" class="btn btn-small"><i class="fa fa-edit"></i> Download</a>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo site_url('kerjasama/editKerjasama/' . $kerjasama->id_surat) ?>" class="btn btn-small"><i class="fa fa-edit"></i> Edit</a>
                                                        <a onclick="deleteConfirm('<?php echo site_url('kerjasama/deleteKerjasama/' . $kerjasama->id_surat) ?>')" href="#!" class="btn btn-small text-danger"><i class="fa fa-trash"></i> Hapus</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Zero Configuration  Ends-->

                </div>

            </div>
            <!-- Container-fluid Ends-->
            <!-- footer start-->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 footer-copyright">
                            <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
                        </div>
                        <div class="col-md-6">
                            <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
    </div>

    <?php $this->load->view('admin/kerjasama/admin/_partials/bottom'); ?>
    <?php $this->load->view('admin/kerjasamaadmin/_partials/logout_modal'); ?>

    <script>
        function deleteConfirm(url) {
            $('#btn-delete').attr('href', url);
            $('#deleteModal').modal();
        }
    </script>


    <script>
        // donut chart
        var options9 = {
            chart: {
                width: 380,
                type: 'donut',
            },
            series: [<?php foreach ($on_going as $onGoing) : echo $onGoing->total ?> <?php endforeach; ?>, 55, 41, 17],
            labels: ['On Going', 'Terminate', 'Pending', 'Finish'],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart9 = new ApexCharts(
            document.querySelector("#donutchart"),
            options9
        );

        chart9.render();
    </script>

    <script>
        // basic bar chart
        var options2 = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: true,
                }
            },
            dataLabels: {
                enabled: true
            },
            series: [{
                data: [30, 40, 48, 47, 40, 58, 60, 10, 12, 13],
            }],

            xaxis: {
                categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan', 'United States', 'China', 'Germany', ],
            },
            colors: ['#7e37d8']
        }

        var chart2 = new ApexCharts(
            document.querySelector("#basic-bar"),
            options2
        );
        chart2.render();
    </script>


