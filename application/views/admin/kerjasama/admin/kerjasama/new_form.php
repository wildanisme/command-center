<?php $this->load->view('admin/kerjasama/admin/_partials/head'); ?>

<body>
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/kerjasama/admin/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header sub-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>admin">Tabel Kerjasama</a></li>
                                <li class="iconbar-header sub-header">Parameter</li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterSkpd">SKPD</a></li>
                                <li><a href="<?= base_url(); ?>kerjasama/adminParameterBidang">Bidang</a></li>
                                <li><a href="<?= base_url(); ?>/kerjasama/adminParameterStatus">Status</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->

            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Data<span>Kerjasama </span></h2>
                                <h6 class="mb-0">Tambah Data</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Dashboard</li>
                                    <li class="breadcrumb-item active">Default </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($this->session->flashdata('success')) : ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php endif; ?>

                <div class="card ">
                    <div class="card-header">
                        <a href="<?php echo site_url('kerjasama/admin') ?>"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>

                    <div class="card-body">
                        <form class="needs-validation" novalidate="" action="<?php echo site_url('kerjasama/saveKerjasama') ?>" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="no_surat_ket">No Surat Kerjasama*</label>
                                        <input class="form-control" type="text" name="no_surat_ket" required="" />
                                        <div class="invalid-feedback">Mohon Isi No Surat Kerjasama</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="judul_kerjasama">Nama Kerjasama*</label>
                                        <input class="form-control" type="text" name="judul_kerjasama" required="" />
                                        <div class="invalid-feedback">Mohon Isi Nama Kerjasama</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Pihak Pertama*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="pihak_pertama" required="">
                                            <option hidden></option>
                                            <?php foreach ($data_skpd as $skpd) : ?>
                                                <option value="<?php echo $skpd->nama_skpd ?>"><?php echo $skpd->nama_skpd ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Pihak Pertana</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="pihak_kedua">Pihak Kedua*</label>
                                        <input class="form-control" type="text" name="pihak_kedua" required="" />
                                        <div class="invalid-feedback">Mohon Isi Pihak Kedua</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tanggal">Tanggal Mulai*</label>
                                        <input class="form-control" type="date" name="tanggal" required="" />
                                        <div class="invalid-feedback">Mohon Pilih Tanggal Mulai</div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="tanggal_selesai">Tanggal Selesai*</label>
                                        <input class="form-control" type="date" name="tanggal_selesai" required="" />
                                        <div class="invalid-feedback">Mohon Pilih Tanggal Selesai</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Bidang*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="bidang" required="">
                                            <option hidden></option> <?php foreach ($data_bidang as $bidang) : ?>
                                                <option value="<?php echo $bidang->nama_bidang ?>"><?php echo $bidang->nama_bidang ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Bidang</div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="mr-sm-2" for="inlineFormCustomSelect">Status*</label>
                                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="status" required="">
                                            <option hidden></option>
                                            <?php foreach ($data_status as $status) : ?>
                                                <option value="<?php echo $status->nama_status ?>"><?php echo $status->nama_status ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback">Mohon Pilih Status</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">Deskripsi</label>
                                        <textarea class="form-control" name="deskripsi" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="galeri">Galeri</label>
                                        <input class="form-control" type="file" name="galeri" />
                                        <span>(format .jpg/png maks 10MB)</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="dokumen">Dokumen </label>
                                        <input class="form-control" type="file" name="dokumen" />
                                        <span>(format .pdf maks 20MB)</span>
                                    </div>
                                </div>
                            </div>

                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </form>

                    </div>


                </div>

            </div>
        </div>
    </div>

    <?php $this->load->view('admin/kerjasama/admin/_partials/bottom'); ?>

    <!-- End of Main Content -->