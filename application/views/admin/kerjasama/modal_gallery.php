<div class="container-fluid">
    <?php foreach ($tbl_kerjasama as $kerjasama) : ?>

        <div class="row">
            <img class="align-self-center mr-3" src="<?php echo base_url('upload/kerjasama/' . $kerjasama->dokumen) ?>" alt="Generic placeholder image" width="300" height="300">
        </div>
        <div class="row">
            <h5>No Surat Kerjasama</h5>
        </div>
        <div class="row">
            <h5>Judul Kerjasama</h5>
        </div>
        <div class="row">
            <h5>Pihak Pertama : </h5>
            <h6><?php echo $kerjasama->pihak_pertama ?> </h6>
        </div>
        <div class="row">
            <h5>Pihak Kedua</h5>
        </div>
        <div class="row">
            <h5>Tanggal</h5>
        </div>
        <div class="row">
            <h5>Bidang</h5>
        </div>
        <div class="row">
            <h5>Status</h5>
        </div>
        <div class="row">
            <h5>Deskripsi</h5>
        </div>
    <?php endforeach; ?>
</div>


<table width="100%" style="font-size:14px">
    <tr>
        <td width="150">No. Induk</td>
        <td width="10">:</td>
        <td>' + data[0] + '</td>
    </tr>
    <tr>
        <td>Nomor Surat Kerjasama</td>
        <td>:</td>
        <td>' + data[1] + '</td>
    </tr>
    <tr>
        <td>Judul Kerjasama</td>
        <td>:</td>
        <td>' + data[2] + '</td>
    </tr>
    <tr>
        <td>Pihak Pertama</td>
        <td>:</td>
        <td?>' + data[3] + '</td>
    </tr>
    <tr>
        <td>Pihak Kedua</td>
        <td>:</td>
        <td?>' + data[4] + '</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:</td>
        <td>' + data[5] + '</td>
    </tr>
    <tr>
        <td>Jenjang</td>
        <td>:</td>
        <td>' + data[6] + '</td>
    </tr>
</table>'