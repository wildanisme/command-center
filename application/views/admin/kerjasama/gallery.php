<?php $this->load->view('admin/kerjasama/_partials/head'); ?>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view('admin/kerjasama/_partials/page_header'); ?>
        <!-- Page Header End-->

        <!-- Page Body Start-->
        <div class="page-body-wrapper">

            <!-- Page Sidebar Start-->
            <div class="iconsidebar-menu iconbar-mainmenu-close">
                <div class="sidebar">
                    <ul class="iconMenu-bar custom-scrollbar">
                        <li><a class="bar-icons" href="javascript:void(0)">
                                <!--img(src='<?= base_url(); ?>assets/images/menu/home.png' alt='')--><i class="pe-7s-home"></i><span>General </span>
                            </a>
                            <ul class="iconbar-mainmenu custom-scrollbar">
                                <li class="iconbar-header">Dashboard</li>
                                <li><a href="<?= base_url(); ?>">Home</a></li>
                                <li><a href="<?= base_url(); ?>#section1">Filter Tahun</a></li>
                                <li><a href="<?= base_url(); ?>#section2">Statistik</a></li>
                                <li><a href="<?= base_url(); ?>#section3">Tabel</a></li>
                                <li><a href="<?= base_url(); ?>interface/gallery">Galeri</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Page Sidebar Ends-->


            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Galeri<span>Terbaru</span></h2>
                                <h6 class="mb-0">user panel</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url(); ?>"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item active">Galeri</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <!--FILTER-->
                <div id="section1">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body">
                                <form class="needs-validation" novalidate="" action="<?php echo site_url('kerjasama/search'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input class="form-control" type="text" name="search" required="" placeholder="search nama kerjasama..." />
                                            <div class="invalid-feedback">Mohon isi nama kerjasama</div>
                                        </div>

                                        <div class="row align-items-end">
                                            <div class="col">
                                                <input class="btn btn-success" type="submit" name="btn" value="Search" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END FILTER-->

                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-9 xl-60">
                            <div class="row">

                                <?php foreach ($tbl_kerjasama as $kerjasama) : ?>

                                    <div class="col-xl-4 xl-50 col-sm-6 box-col-6">
                                        <div class="card">
                                            <div class="blog-box blog-grid text-center product-box">
                                                <div class="product-img"><img class="img-fluid top-radius-blog" src="<?php echo base_url('uploads/kerjasama/' . $kerjasama->galeri) ?>" alt="" width="600" height="450">
                                                    <!-- <div class="product-hover">
                                                        <ul>
                                                            <li><i class="icon-link"></i></li>
                                                            <li><i class="icon-import"></i></li>
                                                        </ul>
                                                    </div> -->
                                                </div>
                                                <div class="blog-details-main">
                                                    <!-- <ul class="blog-social">
                                                        <li class="digits">9 April 2018</li>
                                                        <li class="digits">by: Admin</li>
                                                        <li class="digits">0 Hits</li>
                                                    </ul> -->
                                                    <hr>
                                                    <h6 class="blog-bottom-details"><?php echo $kerjasama->judul_kerjasama ?></h6>
                                                    <a href="#ModalDetail" id="<?php echo $kerjasama->galeri ?>|<?php echo $kerjasama->no_surat_ket ?>|<?php echo $kerjasama->judul_kerjasama ?>|<?php echo $kerjasama->pihak_pertama ?>|<?php echo $kerjasama->pihak_kedua ?>|<?php echo $kerjasama->bidang ?>|<?php echo $kerjasama->tanggal ?>|<?php echo $kerjasama->status ?>|<?php echo $kerjasama->deskripsi ?>" data-toggle="modal" class="detail-galeri">Lihat Detail</a>
                                                    </td>


                                                    <!-- Modal -->
                                                    <div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="myModalLabel"><span class="fa fa-user"></span>&nbsp;Detail</h4>
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                </div>
                                                                <div class="modal-body" id="IsiModal">
                                                                    ...
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal"><span class="fa fa-close"></span> Tutup</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- akhir kode modal dialog -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>


                            </div>
                        </div>

                    </div>
                    <!-- Container-fluid Ends-->
                </div>
                <!-- footer start-->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 footer-copyright">
                                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
                            </div>
                            <div class="col-md-6">
                                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <?php $this->load->view('admin/kerjasama/_partials/bottom'); ?>

        <script>
            $(document).ready(function() {
                // even ketika link a href diklik
                $('.detail-galeri').on("click", function() {
                    // ambil nilai id dari link detail simpan dalam var DataMahasiswa
                    var DataGaleri = this.id;
                    // Pecah DataMahasiswa dengan tanda | sebagai pemisah data hasilnya
                    // disimpan dalam array data
                    var data = DataGaleri.split("|");
                    // Untuk pengujian data

                    // bagian ini yang akan ditampilkan pada modal bootstrap
                    // pengetikan HTML tidak boleh dienter, karena oleh javascript akan dibaca \r\n sehingga
                    // modal bootstrap tidak akan jalan
                    //$("#IsiModal").html('<table width="100%" style="font-size:14px"><tr><td width="150">No. Induk</td><td width="10">:</td><td>' + data[1] + '</td></tr><tr><td>Nama Lengkap</td><td>:</td><td>' + data[2] + '</td></tr><tr><td>Tempat, Tanggal  Lahir</td><td>:</td><td>' + data[3] + ', ' + data[3] + '</td></tr><tr><td>Jenis Kelamin</td><td>:</td><td>' + jk + '</td></tr><tr><td>Program Studi</td><td>:</td><td>' + data[5] + '</td></tr><tr><td>Jenjang</td><td>:</td><td>' + data[6] + '</td></tr></table>');
                    $("#IsiModal").html(' <img class="align-self-center mr-3" src="<?php echo base_url('uploads/kerjasama/') ?>' + data[0] + '" alt="Generic placeholder image" width="auto" height="auto"> <hr> <table width="100%" style="font-size:14px"><tr><td width="150">Nomor Surat Kerjasama</td><td>:</td><td>' + data[1] + '</td></tr><tr><td>Judul Kerjasama</td><td>:</td><td>' + data[2] + '</td></tr><tr><td>Pihak Pertama</td><td>:</td><td>' + data[3] + '</td></tr><tr><td>Pihak Kedua</td><td>:</td><td>' + data[4] + '</td></tr><tr><td>Bidang</td><td>:</td><td>' + data[5] + '</td></tr><tr><td>Tanggal</td><td>:</td><td>' + data[6] + '</td></tr><tr><td>Status</td><td>:</td><td>' + data[7] + '</td></tr><tr><td>Deskripsi</td><td>:</td><td>' + data[8] + '</td></tr></table>');


                });

            });
        </script>
</body>

</html>