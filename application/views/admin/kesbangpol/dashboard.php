<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
	<title>Admin</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="../assets/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="../assets/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="../assets/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="../assets/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="../assets/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/animate.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="../assets/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/pe7-icon.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/style.css">
    <link id="color" rel="stylesheet" href="../assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="../assets/css/responsive.css">
</head>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <div class="page-main-header">
            <div class="main-header-right">
                <div class="main-header-left text-center">
                    <div class="logo-wrapper"><a href="index.html"><img src="<?= base_url(); ?>asset/dashboard/images/logo/logo.png" alt="" style="width: 150px"></a></div>
                </div>
                <div class="mobile-sidebar">
                    <div class="media-body text-right switch-sm">
                        <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
                    </div>
                </div>
                <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar"> </i></div>
                <div class="nav-right col pull-right right-menu">
                    <ul class="nav-menus">
                        <li>
                            <form class="form-inline search-form" action="#" method="get">
                                <div class="form-group">
                                    <div class="Typeahead Typeahead--twitterUsers">
                                        <div class="u-posRelative">
                                            <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Search Your Product...">
                                            <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                                        </div>
                                        <div class="Typeahead-menu"></div>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
                        <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="../assets/images/dashboard/bookmark.png" alt="">
                            <div class="onhover-show-div bookmark-flip">
                                <div class="flip-card">
                                    <div class="flip-card-inner">
                                        <div class="front">
                                            <ul class="droplet-dropdown bookmark-dropdown">
                                                <li class="gradient-primary text-center">
                                                    <h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
                                                </li>
                                                <li>
                                                    <div class="row">
                                                        <div class="col-4 text-center"><i data-feather="file-text"></i></div>
                                                        <div class="col-4 text-center"><i data-feather="activity"></i></div>
                                                        <div class="col-4 text-center"><i data-feather="users"></i></div>
                                                        <div class="col-4 text-center"><i data-feather="clipboard"></i></div>
                                                        <div class="col-4 text-center"><i data-feather="anchor"></i></div>
                                                        <div class="col-4 text-center"><i data-feather="settings"></i></div>
                                                    </div>
                                                </li>
                                                <li class="text-center">
                                                    <button class="flip-btn" id="flip-btn">Add New Bookmark</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="back">
                                            <ul>
                                                <li>
                                                    <div class="droplet-dropdown bookmark-dropdown flip-back-content">
                                                        <input type="text" placeholder="search...">
                                                    </div>
                                                </li>
                                                <li>
                                                    <button class="d-block flip-back" id="flip-back">Back</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="../assets/images/dashboard/like.png" alt="">
                            <ul class="onhover-show-div droplet-dropdown">
                                <li class="gradient-primary text-center">
                                    <h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
                                        <div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
                                    </div>
                                </li>
                                <li class="text-center">
                                    <button class="btn btn-primary btn-air-primary">Follows Up</button>
                                </li>
                            </ul>
                        </li>
                        <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="../assets/images/dashboard/notification.png" alt="">
                            <ul class="onhover-show-div notification-dropdown">
                                <li class="gradient-primary">
                                    <h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
                                </li>
                                <li>
                                    <div class="media">
                                        <div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
                                        <div class="media-body">
                                            <h6>Someone Likes Your Posts</h6>
                                            <p class="mb-0"> 2 Hours Ago</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="pt-0">
                                    <div class="media">
                                        <div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
                                        <div class="media-body">
                                            <h6>3 New Comments</h6>
                                            <p class="mb-0"> 1 Hours Ago</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="bg-light txt-dark"><a href="#">All </a> notification</li>
                            </ul>
                        </li>
                        <li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="../assets/images/dashboard/chat.png" alt=""></a></li>
                        <li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="../assets/images/dashboard/user.png" alt=""></span>
                            <ul class="onhover-show-div profile-dropdown">
                                <li class="gradient-primary">
                                    <h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
                                </li>
                                <li><i data-feather="user"> </i>Profile</li>
                                <li><i data-feather="message-square"> </i>Inbox</li>
                                <li><i data-feather="file-text"> </i>Taskboard</li>
                                <li><i data-feather="settings"> </i>Settings </li>
                            </ul>
                        </li>
                    </ul>
                    <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
                </div>
                <script id="result-template" type="text/x-handlebars-template">
                    <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
                <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
            </div>
        </div>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper">
		<div class="iconsidebar-menu iconbar-mainmenu-close">
                <?php $this->load->view('admin/src/menu'); ?>
            </div>
            <!-- Page Sidebar Ends-->
            <!-- Right sidebar Start-->
            <div class="right-sidebar" id="right_side_bar">
                <div>
                    <div class="container p-0">
                        <div class="modal-header p-l-20 p-r-20">
                            <div class="col-sm-8 p-0">
                                <h6 class="modal-title font-weight-bold">Contacts Status</h6>
                            </div>
                            <div class="col-sm-4 text-right p-0"><i class="mr-2" data-feather="settings"></i></div>
                        </div>
                    </div>
                    <div class="friend-list-search mt-0">
                        <input type="text" placeholder="search friend"><i class="fa fa-search"></i>
                    </div>
                    <div class="p-l-30 p-r-30">
                        <div class="chat-box">
                            <div class="people-list friend-list">
                                <ul class="list">
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/1.jpg" alt="">
                                        <div class="status-circle online"></div>
                                        <div class="about">
                                            <div class="name">Vincent Porter</div>
                                            <div class="status"> Online</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/2.jpg" alt="">
                                        <div class="status-circle away"></div>
                                        <div class="about">
                                            <div class="name">Ain Chavez</div>
                                            <div class="status"> 28 minutes ago</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/8.jpg" alt="">
                                        <div class="status-circle online"></div>
                                        <div class="about">
                                            <div class="name">Kori Thomas</div>
                                            <div class="status"> Online</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/4.jpg" alt="">
                                        <div class="status-circle online"></div>
                                        <div class="about">
                                            <div class="name">Erica Hughes</div>
                                            <div class="status"> Online</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/5.jpg" alt="">
                                        <div class="status-circle offline"></div>
                                        <div class="about">
                                            <div class="name">Ginger Johnston</div>
                                            <div class="status"> 2 minutes ago</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/6.jpg" alt="">
                                        <div class="status-circle away"></div>
                                        <div class="about">
                                            <div class="name">Prasanth Anand</div>
                                            <div class="status"> 2 hour ago</div>
                                        </div>
                                    </li>
                                    <li class="clearfix"><img class="rounded-small user-image" src="./../assets/images/user/7.jpg" alt="">
                                        <div class="status-circle online"></div>
                                        <div class="about">
                                            <div class="name">Hileri Jecno</div>
                                            <div class="status"> Online</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Right sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>KESBANGPOL <span>SUMEDANG </span></h2>
                                <h6 class="mb-0">KANTOR KESATUAN BANGSA DAN POLITIK</h6>
                            </div>
                            <div class="col-lg-6 breadcrumb-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
                                    <li class="breadcrumb-item">Tables</li>
                                    <li class="breadcrumb-item">Data Tables</li>
                                    <li class="breadcrumb-item active">Basic Init</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">
                        <!-- Multi-Column Ends-->
                        <!-- Multiple tables Start-->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2>Rencana Aksi Tim Terpadu<h2>
                                            <h3>Penanganan Konflik Sosial</h3>
                                            <h4>Tahun 2020</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="show-case">
                                            <thead>
                                                <tr>
                                                    <th>Rencana Aksi</th>
                                                    <th>Penanggung Jawab</th>
                                                    <th>Instansi Terkait</th>
                                                    <th>Ukuran Keberhasilan</th>
                                                    <th>Capaian</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Mendorong Percepatan Penyelesaian Pembebasan Lahan</td>
                                                    <td>Tim Pembebasan Lahan (BPN), Satker Pembebasan Tanah</td>
                                                    <td>Kementrian PU (Satker jalan TOL) BPN, Bagian Tata Pemerintahan Setda, PT Jasa Sarana, BAPPPPEDA</td>
                                                    <td>Pebebasan lahan tepat waktu sesuai target, sosialisasi yg komprehensif dari Tim, Fasilitasi Pemda untuk mendorong percepatan</td>
                                                    <td>80%</td>
                                                </tr>
                                                <tr>
                                                    <td>Penyelesaian Masalah Disposal (Penyimpanan Tanah Sisa Galian/Cut)</td>
                                                    <td>Satker Jalan TOL</td>
                                                    <td>Kementrian PU (Satker Jl TOL), BPN, Bappppeda, PU dan Penataan Ruang, Setda (Asisten Pemerintahan), Kabupaten Bogor, Camat setempat, Kepala Desa Setempat </td>
                                                    <td>Lahan pembuangan yang aman dan jauh dari pemukiman warga, cepat tanggap dari Satker, Dorongan dari Pemda kepada Satker</td>
                                                    <td>90%</td>
                                                </tr>
                                                <tr>
                                                    <td>Penyelesaian Dampak Perubahan Trase TOL</td>
                                                    <td>Satker Jalan TOL</td>
                                                    <td>Kementrian PU (Satker Jl TOL), Bappppeda, PU dan Penataan Ruang</td>
                                                    <td>Sosilisasi yg komprehensif, Data yang tetap pengamanan lahan dari calo tanah</td>
                                                    <td>70%</td>
                                                </tr>
                                                <tr>
                                                    <td>Masih menyisakan permasalahan harga ganti rugi lahan </td>
                                                    <td>Satker Jalan TOL</td>
                                                    <td>BPN, Satker Jalan Tol</td>
                                                    <td>Dengan SK Bupati Bogor tentang kenaikan harga tanah sesuai dengan sekarang</td>
                                                    <td>90%</td>
                                                </tr>
                                                <tr>
                                                    <td>Penataan PKL</td>
                                                    <td>Disperindag</td>
                                                    <td>Satpol PP, Polres, Kodim, Dishub</td>
                                                    <td>Terciftanya situasi yang kondusif yang indah dan bersih</td>
                                                    <td>90%</td>
                                                </tr>
                                                <tr>
                                                    <td>Penataan Pasar</td>
                                                    <td>Disperindag</td>
                                                    <td>Satpol PP, Polres, Kodim, Dishub</td>
                                                    <td>Terciptanya ketertiban dan kerapihan semua pedagang</td>
                                                    <td>90%</td>
                                                </tr>
                                                <tr>
                                                    <td>Evaluasi ASN dan P3K</td>
                                                    <td>BKPSDM</td>
                                                    <td>Polres, Kesbangpol, Satpol PP</td>
                                                    <td>Hasil seleksi tidak menimbulkan dampak negative dan Perekrutan Pegawai P3K</td>
                                                    <td>100%</td>
                                                </tr>
                                                <tr>
                                                    <td>Narkotika</td>
                                                    <td>BNNK</td>
                                                    <td>BNN, Kesbangpol, Polres, Satpol PP, Dinkes</td>
                                                    <td>Terciftanya situasi yang kondusif di masyarakat serta terbebas dari narkotika</td>
                                                    <td>100%</td>
                                                </tr>
                                                <tr>
                                                    <td>LGBT</td>
                                                    <td>Dinas Kesehatan</td>
                                                    <td>Polres, Kesbang, Kemenag, Dinsos, Satpol PP</td>
                                                    <td>Terungkapnya kasus – kasus HIV / AIDS yang terkait dengan LGBT </td>
                                                    <td>100 %</td>
                                                </tr>
                                                <tr>
                                                    <td>Pendataan, Penertiban, Pemanggilan dan Pembinaan Anak Jalanan / Punk</td>
                                                    <td>Dinsos</td>
                                                    <td>Dinkes,</td>
                                                    <td>Terdatanya anak jalanan/punk dan lokasi tempat berkumpulnya</td>
                                                    <td>100%</td>
                                                </tr>
                                                <tr>
                                                    <td>Penyelenggaraan Pilkades</td>
                                                    <td>DPMPD</td>
                                                    <td>Kesbangpol, Desa, Satpol PP, Polres, Kodim, Camat</td>
                                                    <td>Adanya Kepala Desa Terpilih yang berkualitas</td>
                                                    <td>100%</td>
                                                </tr>
                                            <tbody>
                                                <table>
                                                    <div>
                                                    </div>
                                                    <!-- Container-fluid Ends-->
                                    </div>
                                    <!-- footer start-->
                                    <footer class="footer">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6 footer-copyright">
                                                    <p class="mb-0">Copyright © 2021 Bogor. All rights reserved.</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="pull-right mb-0">Hand-crafted & made with Diskominfosanditik<i class="fa fa-heart"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <!-- latest jquery-->
                            <script src="../assets/js/jquery-3.5.1.min.js"></script>
                            <!-- Bootstrap js-->
                            <script src="../assets/js/bootstrap/popper.min.js"></script>
                            <script src="../assets/js/bootstrap/bootstrap.js"></script>
                            <!-- feather icon js-->
                            <script src="../assets/js/icons/feather-icon/feather.min.js"></script>
                            <script src="../assets/js/icons/feather-icon/feather-icon.js"></script>
                            <!-- Sidebar jquery-->
                            <script src="../assets/js/sidebar-menu.js"></script>
                            <script src="../assets/js/config.js"></script>
                            <!-- Plugins JS start-->
                            <script src="../assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
                            <script src="../assets/js/datatable/datatables/datatable.custom.js"></script>
                            <script src="../assets/js/chat-menu.js"></script>
                            <!-- Plugins JS Ends-->
                            <!-- Theme js-->
                            <script src="../assets/js/script.js"></script>
                            <script src="../assets/js/theme-customizer/customizer.js"></script>
                            <!-- login js-->
                            <!-- Plugin used-->
</body>

</html>