 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Bantuan</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                               
                                
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="content-body">

           <div class="row col-row-spacing">
           
            <div class="col-md-12 inputselect-group">
                <form method="POST">
                    <div class="form-group select-container">
                        <select name="id_jabatan" class="form-control">
                            <option value="">Kecamatan</option>
                            
                        </select>
                    </div>
                    <fieldset class="form-group position-relative has-icon-left input-divider-left">
                        <div class="input-group">
                            <input type="text" value="" name="" class="round form-control" id="iconLeft3" placeholder="Cari Nama Kelompok ...">
                            <div class="form-control-position">
                                <i class="feather icon-user"></i>
                            </div>
                            <div class="input-group-append" id="button-addon2">
                                <button class="btn btn-primary round" type="submit" name="submit" value="search"><i class="feather icon-search"></i></button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                
                
            </div>
        </div>
        <!-- Basic example and Profile cards section start -->
        <section id="basic-examples">
            <div class="row match-height">
               
              
                <!-- Profile Cards Starts -->
                <div class="col-xl-4 col-md-6 col-sm-12 profile-card-1">
                    <div class="card">
                        <div class="card-header mx-auto">
                            <div class="avatar-xl" >
                                <img class="img-fluid" src="<?=base_url();?>data/images/avatar.png" alt="img placeholder" style="width: 220px">
                            </div>
                        </div>
                        
                        <div class="card-content">
                            <div class="card-body text-center">
                                <h4>Kelompok 1</h4>
                                <div class="badge badge-success font-medium-2">Rp. 35.000.000</div>
                               <p class="text-muted">Total Bantuan</p>
                                <div class="d-flex justify-content-between">
                                    <div class="float-left">
                                        <i class="feather icon-star text-warning mr-50"></i> Kec. Cisitu
                                    </div>
                                    <div class="float-right">
                                        <i class="feather icon-briefcase text-primary mr-50"></i> 37 Anggota
                                    </div>
                                </div>
                                <hr style="border-top: 1px solid rgb(0 0 0 / 15%);">
                                <div class="d-flex justify-content-between">
                                   <a href="<?=base_url()?>manage_bantuan/detail" class="btn btn-outline-primary waves-effect waves-light btn-block">Detail</a>
                               </div>

                           </div>
                       </div>
                       
                   </div>
               </div>

               
           </div>
       </section>
       <!-- // Basic example and Profile cards section end -->


       

   </div>
</div>
</div>
    <!-- END: Content-->