
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Detail Produk</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=base_url();?>manage_kelompok/detail">Kelompok</a>
                                    </li>
    
                                    <li class="breadcrumb-item active">Detail Produk
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
     
            </div>
            <div class="content-body">
                <!-- app ecommerce details start -->
                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-4 mt-2">
                                <div class="col-12 col-md-3 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img src="<?=base_url();?>data/produk/produk5.jpg" class="img-fluid" alt="product image">
                                    </div>
                                </div>
                                <div class="col-12 col-md-9">
                                    <h5>Cabai Merah
                                    </h5>
                                    <p class="text-muted">Kelompok 1</p>
                                    <div class="ecommerce-details-price d-flex flex-wrap">

                                        <p class="text-primary font-medium-3 mr-1 mb-0">Rp. 10.000</p>
                                    </div>
                                    <hr>
                                    <p>Shoot professional photos and videos with this Canon EOS 5D Mk V 24-70mm lens kit. A huge 30.4-megapixel
                                        full-frame sensor delivers outstanding image clarity, and 4K video is possible from this DSLR for powerful
                                        films. Ultra-precise autofocus and huge ISO ranges give you the images you want from this Canon EOS 5D Mk V
                                        24-70mm lens kit.</p>
                                   
                                    <hr>

                                    <hr>
                                    <p>Stock - <span class="text-success">5000</span></p>
                                    <hr style="border-top: 1px solid rgb(115 115 115 / 10%);">
                                    Link Marketplace
                                    
                                    <hr>

                                    <a href="#"><img src="<?=base_url();?>data/icon/wa.png" width="40px" class="mr-1"></a>
                                    <a href="#"><img src="<?=base_url();?>data/icon/bukalapak.png" width="40px" class="mr-1"></a>
                                    <a href="#"><img src="<?=base_url();?>data/icon/shopee.png" width="40px" class="mr-1"></a>
                                    <a href="#"><img src="<?=base_url();?>data/icon/tokopedia.png" width="40px" class="mr-1"></a>
                                    

                                    <hr style="border-top: 1px solid rgb(115 115 115 / 10%);">
                           
                                    <div class="d-flex flex-column flex-sm-row">
                                       
                                        <button class="btn btn-outline-primary"><i class="feather icon-arrow-left mr-25"></i>Kembali</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    
                        
                    </div>
                </section>
                <!-- app ecommerce details end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->