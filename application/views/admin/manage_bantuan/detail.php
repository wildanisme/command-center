 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Manage Bantuan</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">

                                <li class="breadcrumb-item "><a href="<?=base_url();?>manage_bantuan">Manage Bantuan</a>
                                    <li class="breadcrumb-item active"><a href="#">Detail kelompok</a>
                                    </li>
                                    
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

                <!-- page users view start -->
                <section class="page-users-view">
                    <div class="row">
                        <!-- account start -->
                        <div class="col-12">
                            <div class="card">

                                <div class="card-body mt-1">
                                    <div class="row">
                                        <div class="col-md-2 ">
                                            <div class="users-view-image">
                                                <img src="<?=base_url();?>data/images/avatar.png" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                            </div>
                                        </div>
                                        <div class="col-md-4 ">
                                           <div class="form-group validate">
                                            <div class="controls" >
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Nama Kelompok</p>
                                                <p><strong>Kelompok 1</strong></p>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Jenis Usaha</p>
                                                <p><strong>Pertanian</strong></p>
                                            </div>
                                        </div>  
                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Mentor</p>
                                                <p><strong>Nandang Koswara</strong></p>
                                            </div>

                                        </div>  
                                        <div class="form-group validate">
                                          <div class="controls">
                                            <p class="text-muted" style="line-height: 0.2rem !important" >Telp.</p>
                                            <p><strong>08251550154</strong></p>
                                        </div>

                                        <div class="form-group validate">
                                          <div class="controls">
                                            <p class="text-muted" style="line-height: 0.2rem !important" >Alamat</p>
                                            <p><strong>Dsn. Cinangsi RT 08/07 Ds.Cinangsi Kec. Cisitu</strong></p>
                                        </div>

                                    </div>                                         

                                </div>
                            </div>
                            <div class="col-md-5">
                             <div class="form-group validate">
                                 <div id="map" style="width: 100%;height: 260px;"></div>
                             </div>  

                         </div>

                     </div>
                 </div>
             </div>
         </div>
         <!-- account end -->

         <button type="button" class="btn btn-success ml-1 mb-1" data-toggle="modal" data-target="#inlineForm">
            Tambah Bantuan
        </button>

        <div class="col-12">
            <div class="card">
                <div class="card-header border-bottom mx-2 px-0">
                    <h6 class="border-bottom py-1 mb-0 font-medium-2 "><i class="feather icon-user mr-50 "></i>Bantuan Pertama
                    </h6>
                    <div class="float-right">
                        <a href="#"><button class="btn btn-outline-primary " data-toggle="modal" data-target="#inlineForm">Edit</button></a>
                        <a href="#"><button class="btn btn-outline-warning ">Hapus</button></a>
                    </div>

                </div>
                <div class="card-body px-75">
                    <div class="table table-responsive">
                        <table class="table table-border table-striped">
                            <thead class="success">
                                <tr>
                                    <th>Jenis Barang</th>
                                    <th>Satuan</th>
                                    <th>Volume</th>
                                    <th>Harga Satuan</th>
                                    <th>Besaran Alokasi</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                   <td style="width: 40%">Pupuk Kandang</td>
                                   <td>Kg</td>
                                   <td>40</td>
                                   <td>40000</td>
                                   <td>50.000.000</td>
                                   <td>
                                    <a href="#">
                                        <button class="btn btn-outline-primary"> Edit</button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-outline-danger"> Hapus</button>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>





</div>
</section>
<!-- page users view end -->




</div>
</div>
</div>
<!-- END: Content-->

<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">Tambahkan Bantuan </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#">
                <div class="modal-body">
                    <label>Nama Bantuan</label>
                    <div class="form-group">
                        <input type="text" placeholder="Nama Bantuan" class="form-control">
                    </div>

                    <label>Tgl Bantuan: </label>
                    <div class="form-group">
                        <input type="date" placeholder="Tgl. Bantuan" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>