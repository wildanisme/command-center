<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="pixelstrap">
  <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" type="image/x-icon">
  <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.png" type="image/x-icon">
  <title>SIE - Kab. Bogor</title>
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Font Awesome-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/fontawesome.css">
  <!-- ico-font-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/icofont.css">
  <!-- Themify icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/themify.css">
  <!-- Flag icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/flag-icon.css">
  <!-- Feather icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/feather-icon.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/animate.css">
  <!-- Plugins css start-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/pe7-icon.css">
  <!-- Plugins css Ends-->
  <!-- Bootstrap css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/bootstrap.css">
  <!-- App css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">
  <link id="color" rel="stylesheet" href="<?= base_url(); ?>assets/css/color-1.css" media="screen">
  <!-- Responsive css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/responsive.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/datatables.css">

</head>

<body>
  <!-- Loader starts-->
  <div class="loader-wrapper">
    <div class="typewriter">
      <h1>New Era Admin Loading..</h1>
    </div>
  </div>
  <!-- Loader ends-->
  <!-- page-wrapper Start-->
  <div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
      <div class="main-header-right">
        <div class="main-header-left text-center">
          <div class="logo-wrapper"><a href="index.html"><img src="<?= base_url(); ?>assets/images/logo/logo.png" alt="" style="width: 150px"></a></div>
        </div>
        <div class="mobile-sidebar">
          <div class="media-body text-right switch-sm">
            <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
          </div>
        </div>
        <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar"> </i></div>
        <div class="nav-right col pull-right right-menu">
          <ul class="nav-menus">
            <li>
              <form class="form-inline search-form" action="#" method="get">
                <div class="form-group">
                  <div class="Typeahead Typeahead--twitterUsers">
                    <div class="u-posRelative">
                      <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Search Your Product...">
                      <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                    </div>
                    <div class="Typeahead-menu"></div>
                  </div>
                </div>
              </form>
            </li>
            <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/bookmark.png" alt="">
              <div class="onhover-show-div bookmark-flip">
                <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="front">
                      <ul class="droplet-dropdown bookmark-dropdown">
                        <li class="gradient-primary text-center">
                          <h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
                        </li>
                        <li>
                          <div class="row">
                            <div class="col-4 text-center"><i data-feather="file-text"></i></div>
                            <div class="col-4 text-center"><i data-feather="activity"></i></div>
                            <div class="col-4 text-center"><i data-feather="users"></i></div>
                            <div class="col-4 text-center"><i data-feather="clipboard"></i></div>
                            <div class="col-4 text-center"><i data-feather="anchor"></i></div>
                            <div class="col-4 text-center"><i data-feather="settings"></i></div>
                          </div>
                        </li>
                        <li class="text-center">
                          <button class="flip-btn" id="flip-btn">Add New Bookmark</button>
                        </li>
                      </ul>
                    </div>
                    <div class="back">
                      <ul>
                        <li>
                          <div class="droplet-dropdown bookmark-dropdown flip-back-content">
                            <input type="text" placeholder="search...">
                          </div>
                        </li>
                        <li>
                          <button class="d-block flip-back" id="flip-back">Back</button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="<?= base_url(); ?>assets/images/dashboard/like.png" alt="">
              <ul class="onhover-show-div droplet-dropdown">
                <li class="gradient-primary text-center">
                  <h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
                </li>
                <li>
                  <div class="row">
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
                  </div>
                </li>
                <li class="text-center">
                  <button class="btn btn-primary btn-air-primary">Follows Up</button>
                </li>
              </ul>
            </li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/notification.png" alt="">
              <ul class="onhover-show-div notification-dropdown">
                <li class="gradient-primary">
                  <h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
                </li>
                <li>
                  <div class="media">
                    <div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
                    <div class="media-body">
                      <h6>Someone Likes Your Posts</h6>
                      <p class="mb-0"> 2 Hours Ago</p>
                    </div>
                  </div>
                </li>
                <li class="pt-0">
                  <div class="media">
                    <div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
                    <div class="media-body">
                      <h6>3 New Comments</h6>
                      <p class="mb-0"> 1 Hours Ago</p>
                    </div>
                  </div>
                </li>
                <li class="bg-light txt-dark"><a href="#">All </a> notification</li>
              </ul>
            </li>
            <li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="<?= base_url(); ?>assets/images/dashboard/chat.png" alt=""></a></li>
            <li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="<?= base_url(); ?>assets/images/dashboard/user.png" alt=""></span>
              <ul class="onhover-show-div profile-dropdown">
                <li class="gradient-primary">
                  <h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
                </li>
                <li><i data-feather="user"> </i>Profile</li>
                <li><i data-feather="message-square"> </i>Inbox</li>
                <li><i data-feather="file-text"> </i>Taskboard</li>
                <li><i data-feather="settings"> </i>Settings </li>
              </ul>
            </li>
          </ul>
          <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
          <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
        <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
      </div>
    </div>

    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
      <div class="iconsidebar-menu iconbar-mainmenu-close">
        <?php $this->load->view('admin/src/menu'); ?>

      </div>
      <!-- Page Sidebar Start-->
      <!-- sumber data  -->
      <?php
      //key


      $curlk = curl_init();

      curl_setopt_array($curlk, array(
        CURLOPT_PORT => "8003",
        CURLOPT_URL => "http://118.97.191.59:8003/api/AppUser/Login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n  \"username\": \"kominfo\",\r\n  \"password\": \"pass1234\"\r\n}",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/json",
          "postman-token: 8a3ad102-b857-7a27-2e32-98a3e3a0e5e7"
        ),
      ));

      $responsek = curl_exec($curlk);
      $key = substr($responsek, 15, -4);




      //ke 2
      $curl2 = curl_init();
      curl_setopt_array($curl2, array(
        CURLOPT_PORT => "8003",
        CURLOPT_URL => "http://118.97.191.59:8003/api/SP2D?PageSize=300&CurrentPage=$page",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "accept: */*",
          "authorization: Bearer $key",
          "cache-control: no-cache",
          "postman-token: 4e83ce04-0a4a-a7fc-59e4-f55433212cbe"
        ),
      ));

      $response2 = curl_exec($curl2);
      $content2 = utf8_encode($response2);
      $result2a = json_decode($content2, true);
      $result2 = $result2a['data'];
      $pagena = $result2a['pagination'];
      ?>

      <!--endsumberdata -->

      <!-- Page Sidebar Ends-->

      <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-12 main-header">
                <h2>Dashboard</h2>



                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-header">
                      <h5>Transparansi Data SP2D</h5>
                      <form action="<?php echo site_url('Sp2d/index') ?>" method="post">
                        <h7 class="mb-0">Chose Curent Page</h7>
                        <select id="page" name="page">

                          <option value="<?php echo $page ?>"><?php echo $page ?></option>
                          <?php $i = 0;

                          while ($i <= 12) {

                            $i++;

                            //  echo $i.'<br/>';

                          ?>

                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                          <?php } ?>
                        </select>
                        <input type="submit" value="Submit">
                      </form>
                    </div>
                    <div class="card-body">
                      <div id="column-chart"></div>
                      <div class="table-responsive">
                        <table class="display" id="basic-1">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>SP2D</th>
                              <th>SPM</th>
                              <th>Jumlah</th>
                              <th>Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $noUrut = 0; ?>
                            <?php foreach ($result2 as $row) :
                              $noUrut++; ?>


                              <tr>
                                <td><?php echo $noUrut ?></td>
                                <td>
                                  <?php echo '<i style="width:12px;height:12px;color:#7e37d8" data-feather="hash"></i> ' . $row['nosp2d'] ?>
                                  <br>
                                  <?php echo '<i style="width:12px;height:12px;color:#7e37d8" data-feather="calendar"></i> ' . tanggal(substr($row['tglsp2d'], 0, 10)) ?>
                                </td>
                                <td>

                                  <?php echo '<i style="width:12px;height:12px;color:#7e37d8" data-feather="hash"></i> ' . $row['nospm'] ?>
                                  <br>
                                  <?php echo '<i style="width:12px;height:12px;color:#7e37d8" data-feather="calendar"></i>  ' . tanggal(substr($row['tglspm'], 0, 10)) ?>
                                </td>
                                <td>
                                  <?php echo "Rp." . number_format($row['nilai'], 2, ',', '.') ?>
                                </td>
                                <td>

                                  <?php echo $row['keperluan'] ?>
                                </td>


                              </tr>
                            <?php endforeach; ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>








                </div>

              </div>
            </div>


          </div>
          <!-- footer start-->
          <footer class="footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-6 footer-copyright">
                  <p class="mb-0">Copyright © Dinas Sosial P3A Kab. Bogor.</p>
                </div>
                <div class="col-md-6">
                  <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
                </div>
              </div>
            </div>
          </footer>

        </div>
      </div>


      <!-- latest jquery-->
      <script src="<?= base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
      <!-- Bootstrap js-->
      <script src="<?= base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
      <!-- feather icon js-->
      <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
      <!-- Sidebar jquery-->
      <script src="<?= base_url(); ?>assets/js/sidebar-menu.js"></script>
      <script src="<?= base_url(); ?>assets/js/config.js"></script>
      <!-- Plugins JS start-->
      <script src="<?= base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
      <script src="<?= base_url(); ?>assets/js/chart/apex-chart/stock-prices.js"></script>
      <script src="<?= base_url(); ?>assets/js/chat-menu.js"></script>
      <!-- Plugins JS Ends-->
      <!-- Theme js-->
      <script src="<?= base_url(); ?>assets/js/script.js"></script>
      <script src="<?= base_url(); ?>assets/js/theme-customizer/customizer.js"></script>
      <script src="<?= base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
      <script src="<?= base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>
      <!-- login js-->
      <!-- Plugin used-->
      <script>
        var options1 = {
          chart: {
            height: 350,
            type: 'bar',
          },
          plotOptions: {
            bar: {
              horizontal: false,
              endingShape: 'rounded',
              columnWidth: '55%',
            },
          },
          dataLabels: {
            enabled: false
          },
          stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
          },
          series: [{
            name: 'presntase',
            //data: [1141, 728, 705, 443, 781, 350, 610, 422, 223,199, 287, 162, 268, 192,410,218,236,212,146,141,110,166,84,296,203,91]
            data: [<?php foreach ($result2 as $row) : ?>
                <?php echo $row['prosentase'] . "," ?>
              <?php endforeach; ?>
            ]

          }],
          xaxis: {
            // categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
            categories: [<?php foreach ($result2 as $row) : ?>
                <?php echo "'" . $row['nmunit'] . "'," ?>
              <?php endforeach; ?>
            ],
          },
          yaxis: {
            title: {
              text: ''
            }
          },
          fill: {
            opacity: 1

          },
          tooltip: {
            y: {
              formatter: function(val) {
                return "" + val + " "
              }
            }
          },
          colors: ['#80cf00', '#fe80b2', '#80cf00']
        }

        var chart1 = new ApexCharts(
          document.querySelector("#column-chart"),
          options1
        );

        chart1.render();


        //chart 2
        // pie chart
        var options2 = {
          chart: {
            width: 800,
            type: 'pie',
          },
          //labels: ['Taman Kanak Kanak', 'Kelompok Belajar', 'PKBM', 'Sekolah Dasar', 'SMP', 'SMA'],
          labels: [<?php foreach ($result3 as $row) : ?>
              <?php echo "'" . $row['uraian'] . "'," ?>
            <?php endforeach; ?>
          ],

          //series: [565, 1263, 139, 5924, 2549, 872],
          series: [<?php foreach ($result3 as $row) : ?>
              <?php echo $row['nilai'] . "," ?>
            <?php endforeach; ?>
          ],
          responsive: [{
            breakpoint: 100,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'center'
              }
            }
          }],
          colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d', '#eb4034']
        }

        var chart2 = new ApexCharts(
          document.querySelector("#piechart"),
          options2
        );

        chart2.render();

        //chart 3
        // pie chart2
        var options3 = {
          chart: {
            width: 800,
            type: 'pie',
          },
          //labels: ['Taman Kanak Kanak', 'Kelompok Belajar', 'PKBM', 'Sekolah Dasar', 'SMP', 'SMA'],
          labels: [<?php foreach ($result4 as $row) : ?>
              <?php echo "'" . $row['uraian'] . "'," ?>
            <?php endforeach; ?>
          ],

          //series: [565, 1263, 139, 5924, 2549, 872],
          series: [<?php foreach ($result4 as $row) : ?>
              <?php echo $row['nilai'] . "," ?>
            <?php endforeach; ?>
          ],
          responsive: [{
            breakpoint: 100,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'center'
              }
            }
          }],
          colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d', '#eb4034']
        }

        var chart3 = new ApexCharts(
          document.querySelector("#piechart2"),
          options3
        );

        chart3.render();
      </script>
</body>

</html>