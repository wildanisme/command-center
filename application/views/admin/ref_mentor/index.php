 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Mentor</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item "><a href="<?=base_url();?>manage_kelompok">Mentor</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

               <section class="page-users-view">



                <div class="row col-row-spacing">
                    <div class="col-md-3">
                        <a href="<?= base_url('ref_mentor/add') ?>" class="btn btn-block btn-primary"><i class="feather icon-user-plus"></i> Tambah</a>
                    </div>
                    <div class="col-md-9 inputselect-group">

                            <div class="form-group select-container">
                                <select name="id_kelompok" id="id_kelompok" onchange="loadPagination(1)" class="form-control">
                                    <option value="">Kelompok</option>
                                    <?php
                                    foreach ($dt_kelompok as $key => $row) {
                                      echo "<option value='$row->id_kelompok'>$row->nama_kelompok</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <fieldset class="form-group position-relative has-icon-left input-divider-left">
                                <div class="input-group">
                                    <input type="text" value="" name="search" onkeyup="loadPagination(1)" id="search" class="round form-control" id="iconLeft3" placeholder="Cari Nama mentor ...">
                                    <div class="form-control-position">
                                        <i class="feather icon-user"></i>
                                    </div>
                                    <div class="input-group-append" id="button-addon2">
                                        <button class="btn btn-primary round" type="submit" name="submit" value="search"><i class="feather icon-search"></i></button>
                                    </div>
                                </div>
                            </fieldset>


                        <a href="<?= base_url('ref_mentor') ?>" class="filter-btn">Reset Filter</a>

                    </div>
                </div>

                <div class="row match-height mt-1" id="row-content">


                </div>

                <div class="row">
                    <div class="col-12 list">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-success justify-content-center mt-2" id="pagination">
                        </ul>
                      </nav>
                    </div>
                </div>


            </div>
        </section>
        <!-- page users view end -->




    </div>
</div>
</div>
    <!-- END: Content-->


  <script type='text/javascript'>

    var csrf_hash = "<?=$this->security->get_csrf_hash();?>";

    function loadPagination(page_num)
    {

        var search        = $("#search").val();
        var id_kelompok   = $("#id_kelompok").val();

        $.ajax({
         url        : "<?=base_url()?>ref_mentor/get_list/"+page_num,
         type       : 'post',
         dataType   : 'json',
         data       : {
            search        : search,
            id_kelompok   : id_kelompok,
         },
         success    : function(data){

            $("#row-content").html(data.row_content);
            //console.log(data.result);
            //console.log(data);
            //loadData(data.result,data.row);
            $("#pagination").html(data.pagination);
         },
             error: function(xhr, status, error) {
              //swal("Opps","Error","error");
              console.log(xhr);
            }
       });

    }


    function hapus(id)
    {
        swal({
          title: "Hapus mentor?",
          //icon: "info",
          buttons: true,
          dangerMode: false,
        })
        .then((isConfirm) => {
          if (isConfirm) {

            $.ajax({
                url :"<?php echo base_url("ref_mentor/delete")?>",
                type:'post',
                data:{
                  id:id,
                  "<?=$this->security->get_csrf_token_name();?>" : csrf_hash,
                },
                 success    : function(data){
                   csrf_hash = data.csrf_hash;
                    //console.log(data);
                    loadPagination(1);
                    swal("Mentor berhasil dihapus", {
                      icon: "success",
                    });
                 },
                     error: function(xhr, status, error) {
                      //swal("Opps","Error","error");
                      console.log(xhr);
                    }
            });

          }
        });
    }



    </script>
