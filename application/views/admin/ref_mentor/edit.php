<!-- BEGIN: Content-->
<div class="app-content content">
   <div class="content-overlay"></div>
   <div class="header-navbar-shadow"></div>
   <div class="content-wrapper">
       <div class="content-header row">
           <div class="content-header-left col-md-9 col-12 mb-2">
               <div class="row breadcrumbs-top">
                   <div class="col-md-12">
                       <h2 class="content-header-title float-left mb-0">Mentor</h2>
                       <div class="breadcrumb-wrapper col-12">
                           <ol class="breadcrumb">
                               <li class="breadcrumb-item"><a href="#">Home</a>
                               </li>
                               <li class="breadcrumb-item "><a href="<?=base_url();?>ref_mentor">Ref. Mentor</a>
                                   <li class="breadcrumb-item active"><a href="#">Edit Mentor</a>
                                   </li>

                               </ol>
                           </div>
                       </div>
                   </div>
               </div>

           </div>
           <div class="content-body">

             <section class="users-edit">
   <div class="card">
       <div class="card-content">
           <div class="card-body">

             <form class="" id="form-data">
               <div class="row mt-1">

                   <div class="col-12 col-sm-6">
                       <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Data Mentor</h5>
                       <div class="form-group">
                           <div class="controls">
                               <label>Nama Lengkap <span class="text-danger">*</span></label>
                               <input type="text" value="<?=$detail->nama_mentor;?>" name="nama_mentor" id="nama_mentor" class="form-control birthdate-picker" placeholder="Masukkan Nama Lengkap">
                               <p id="err_nama_mentor" class="text-danger"></p>
                           </div>
                       </div>



                       <div class="form-group">
                           <div class="controls">
                               <label>Email <span class="text-danger">*</span></label>
                               <input type="text" value="<?=$detail->email;?>" name="email" id="email" class="form-control" placeholder="Masukkan Alamat Email">
                               <p id="err_email" class="text-danger"></p>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="controls">
                               <label>No. HP <span class="text-danger">*</span></label>
                               <input type="text" value="<?=$detail->telepon;?>" name="telepon" id="telepon" class="form-control" placeholder="Masukkan Nomor Handphone">
                               <p id="err_telepon" class="text-danger"></p>
                           </div>
                       </div>

                       <div class="form-group">
                           <div class="controls">
                               <label><i class="feather icon-map-pin"></i> Alamat <span class="text-danger">*</span></label>
                               <textarea name="alamat" id="alamat" class="form-control" placeholder="Masukkan Alamat"><?=$detail->alamat;?></textarea>
                               <p id="err_alamat" class="text-danger"></p>
                           </div>
                       </div>

                       <div class="form-group">
                           <div class="controls">
                               <label>Status <span class="text-danger">*</span></label>
                               <select class="form-control select2" id="status" name="status">
                                   <option <?= ($detail->status=="active") ? "selected" :"" ;?> value="active">Aktif</option>
                                   <option <?= ($detail->status!="active") ? "selected" :"" ;?> value="not active">Not Aktif</option>
                               </select>
                               <p id="err_status" class="text-danger"></p>
                           </div>
                       </div>
                   </div>
                   <div class="col-12 col-sm-6">
                       <h5 class="mt-2 mb-1 mt-sm-0"><i class="feather icon-credit-card mr-25"></i>Kartu Identitas
                           <small class="mb-1 text-warning" style="font-size:11px">Boleh dikosongkan</small></h5>
                       <div class="form-group">
                           <div class="controls">
                               <label>No. KTP</label>
                               <input type="text" value="<?=$detail->nik;?>" name="nik" id="nik" class="form-control" placeholder="Masukkan Nomor KTP">
                               <p id="err_nik" class="text-danger"></p>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="controls">
                               <label>File Foto</label>
                               <input type="file" class="dropify form-control" name="foto" id="foto" data-default-file="<?=base_url()."data/mentor/".$detail->foto;?>" data-show-remove="false">
                               <small class="">* <?= $file_type_allowed." | Max: ".$file_max_size;?> Kb</small>
                               <p id="err_foto" class="text-danger"></p>
                           </div>
                       </div>
                   </div>


               </div>

             </form>

             <div class="row">
               <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                   <button id="btn_save" type="submit" onclick="save()" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Simpan</button>
                   <a href="<?= base_url('ref_mentor') ?>" class="btn btn-outline-warning">Kembali</a>
               </div>
             </div>

               <!-- users edit Info form ends -->
           </div>
       </div>
   </div>
</section>
   </div>
</div>
</div>

<script type="text/javascript">

 var csrf_hash = "<?=$this->security->get_csrf_hash();?>";

   function save()
   {

       $(".text-danger").html("");

       $("#btn_save").attr("disabled",true);
       var formdata = new FormData(document.getElementById('form-data'));
       //formdata.append("expense_id","");
       formdata.append("action","edit");
       formdata.append("id","<?=$detail->id_mentor;?>");
       formdata.append("<?=$this->security->get_csrf_token_name();?>",csrf_hash);
       $.ajax({
        url        : "<?=base_url()?>ref_mentor/save/",
        type       : 'post',
        dataType   : 'json',
        data       : formdata,
        processData:false,
        contentType:false,
        cache:false,
        async:false,
        success    : function(data){
           console.log(data);
           csrf_hash = data.csrf_hash;
           $("#btn_save").attr("disabled",false);

           if(data.status){
             swal("Sukses", data.message, "success");
           }
           else{
               for(err in data.errors)
               {
                   $("#err_"+err).html(data.errors[err]);

                   console.log(data.errors[err]);
               }
               if(data.errors.length==0){
                   swal("Opps", data.message, "warning");
               }
           }
        },
            error: function(xhr, status, error) {
             console.log(xhr);
             $("#btn_save").attr("disabled",false);
               swal("Opps","Terjadi kesalahan","error");
           }
      });
   }


</script>
