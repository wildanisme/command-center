 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-md-12">
                        <h2 class="content-header-title float-left mb-0">Mentor</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item "><a href="<?=base_url();?>ref_mentor">Mentor</a></li>
                                <li class="breadcrumb-item active "><a href="">Detail</a>
                                </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">

                <!-- page users view start -->
                <section class="page-users-view">
                    <div class="row match-height">
                        <!-- account start -->
                        <div class="col-3">

                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title"></div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="users-view-image">
                                                <img src="<?=base_url();?>data/mentor/<?=$detail->foto;?>" class="users-avatar-shadow rounded mb-2 pr-2 ml-1" alt="avatar" style="width: 100%">
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <a href="<?=base_url();?>ref_mentor/edit/<?=$detail->id_mentor;?>" class="btn btn-primary mr-1"><i class="feather icon-edit-1"></i> Edit </a>
                                            <button class="btn btn-outline-danger" onclick="hapus(<?=$detail->id_mentor;?>)"><i class="feather icon-trash-2 "></i> Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-9">

                            <div class="card">
                                <div class="card-header border-bottom">
                                    <div class="card-title mb-1 text-primary">Detail Mentor </div>
                                </div>

                                <div class="card-body">
                                    <div class="row">

                                        <div class="col-md-12 ">
                                           <div class="form-group validate">
                                            <div class="controls" >
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Nama Mentor</p>
                                                <p><strong><?=$detail->nama_mentor;?></strong></p>
                                            </div>
                                        </div>

                                         <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >NIK</p>
                                                <p><strong><?=$detail->nik;?></strong></p>
                                            </div>
                                        </div>

                                        <div class="form-group validate">
                                            <div class="controls">
                                                <p class="text-muted" style="line-height: 0.2rem !important" >Email</p>
                                                <p><strong><?= ($detail->email) ? $detail->email : "-" ;?></strong></p>
                                            </div>
                                        </div>




                                <div class="form-group validate">
                                 <div class="controls">
                                    <p class="text-muted" style="line-height: 0.2rem !important" >Alamat</p>
                                    <p><strong><?=$detail->alamat;?></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
</section>
<!-- page users view end -->




</div>
</div>
</div>

<script>
var csrf_hash = "<?=$this->security->get_csrf_hash();?>";
function hapus(id)
{
    swal({
      title: "Hapus mentor?",
      //icon: "info",
      buttons: true,
      dangerMode: false,
    })
    .then((isConfirm) => {
      if (isConfirm) {

        $.ajax({
            url :"<?php echo base_url("ref_mentor/delete")?>",
            type:'post',
            data:{
              id:id,
              "<?=$this->security->get_csrf_token_name();?>" : csrf_hash,
            },
             success    : function(data){
               csrf_hash = data.csrf_hash;

                swal("Mentor berhasil dihapus", {
                  icon: "success",
                });

                setTimeout(function() {
                  window.location.href = "<?=base_url();?>ref_mentor";
                }, 500);
             },
                 error: function(xhr, status, error) {
                  //swal("Opps","Error","error");
                  console.log(xhr);
                }
        });

      }
    });
}
</script>
