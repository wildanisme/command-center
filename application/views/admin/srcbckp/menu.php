<!-- BEGIN: Main Menu-->
<?php
if (empty($active_menu)) {
  $active_menu = "dashboard";
}
?>
<div class="sidebar">
  <ul class="iconMenu-bar custom-scrollbar">
    <li><a class="bar-icons" href="javascript:void(0)">
        <!--img(src='<?= base_url(); ?>asset/dashboard/images/menu/home.png' alt='')--><i
          class="pe-7s-home"></i><span>Dashboard </span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Dashboard Utama</li>
        <!-- <li><a href="<?= base_url(); ?>d119">Dashboard 119</a></li> -->
        <li><a href="<?= base_url(); ?>admin">Keuangan</a></li>
        <li><a href="<?= base_url(); ?>perizinan">Perizinan</a></li>
        <!-- <li><a href="<?= base_url(); ?>isu">Media Monitoring</a></li> -->
        <li><a href="<?= base_url(); ?>isubaru">Saiber (Sistem Informasi & Berita Bogor)</a></li>
        <!-- <li><a href="<?= base_url(); ?>cuaca">Cuaca</a></li> -->
       
        <!-- <li><a href="<?= base_url(); ?>sak">sak</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>sosial">Sosial</a></li> -->
      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)"><i
          class="icon-shopping-cart"></i><span>Pemerintahan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Pemerintahan</li>
        <li><a href="<?= base_url(); ?>kepegawaian">Kepegawaian</a></li>
        <li><a href="<?= base_url(); ?>sakip">SAKIP</a></li>
        <!-- <li><a href="<?= base_url(); ?>">ORMAS</a></li> -->
        <li><a href="<?= base_url(); ?>organisasi"> ORMAS</a></li>
        <li><a href="<?= base_url(); ?>parpol"> Parpol</a></li>
      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)"><i
          class="icon-shopping-cart"></i><span>Ekonomi</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Ekonomi</li>
        <!-- <li><a href="https://dirga.bogorkab.go.id/#/home">Info Harga</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>umkm"> UMKM</a></li> -->
        <li><a href="<?= base_url(); ?>infoharga"> Info Harga</a></li>
        <!-- <li><a href="<?= base_url(); ?>koperasi"> Koperasi</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>koper"> Koperasi</a></li>
        <li><a href="<?= base_url(); ?>inflasi">Inflasi</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>pendapatand">Pendapatan</a></li> -->
         <!-- <li><a href="<?= base_url(); ?>penduduk">Pendapatan</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>pajak"> Pajak</a></li> -->
        
      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)"><ib
          class="icofont icofont-bed-patient"></ib><span>Kesehatan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Kesehatan </li>
        <li><a href="<?= base_url(); ?>daftar_penyakit">Daftar Penyakit</a></li>
        <li><a href="<?= base_url(); ?>d119">Dashboard 119</a></li>
        <li><a href="<?= base_url(); ?>epuskesmas">e-Puskesmas</a></li>
        <li><a href="<?= base_url(); ?>kesehatan/rsud">Rumah Sakit</a></li>
        <!-- <li><a href="https://anisa.bogorkab.go.id/">Stunting</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>anisa">Iframe</a></li>
        <li><a href="<?= base_url(); ?>tubercolosis">Tuberclosis</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>borkab">BOR/Sitegar</a></li> -->
      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)"><i
          class="icofont icofont-graduate-alt"></i><span>Pendidikan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Pendidikan</li>
        <li><a href="<?= base_url(); ?>pendidikan">Pendidikan</a></li>
      </ul>
    </li>

    <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-world"></i><span>Wisata</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Wisata</li>
        <li><a href="<?= base_url(); ?>wisata">Wisatawan</a></li>
        <li><a href="<?= base_url(); ?>event">Kalender Event</a></li>
        <li><a href="<?= base_url(); ?>destinasiwisata">Potensi Wisata</a></li>
      </ul>
    </li>

    <li><a class="bar-icons" href="javascript:void(0)"><i class="icon-shield"></i><span>Kegawat Daruratan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Kegawat Daruratan</li>
        <li><a href="<?= base_url(); ?>kegawatan">Siaga 112</a></li>
        <li><a href="<?= base_url(); ?>dinsos">KDRT</a></li>
        <li><a href="<?= base_url(); ?>kebencanaan">Kebencanaan</a></li>
        
      </ul>
    </li>
    <!-- <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-portfolio"></i><span>Infrastruktur</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Infrastruktur</li>
        <li><a href="<?= base_url(); ?>jalan">Jalan</a></li>
        <li><a href="<?= base_url(); ?>jembatan">Jembatan</a></li>
        <li><a href="<?= base_url(); ?>drainase">Drainase/Saluran</a></li>
        <li><a href="<?= base_url(); ?>menara">Menara Telekomunikasi</a></li>
        <li><a href="<?= base_url(); ?>transportasi">Transportasi</a></li>
        <li><a href="<?= base_url(); ?>perumahan">Perumahan</a></li>
      </ul>
    </li> -->

    <li><a class="bar-icons" href="javascript:void(0)"><i class="icofont icofont-fish"></i><span>Sumber Daya Alam</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">SDA</li>
        <li><a href="<?= base_url(); ?>peternakan">Peternakan & perikanan</a></li>
        <li><a href="<?= base_url(); ?>kehutanan">Kehutanan</a></li>
      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)"><i class="icofont icofont-atom"></i><span>Dinas Kab.Bogor</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Dinas</li>
        <li><a href="<?= base_url(); ?>listopd">List OPD</a></li>
       
      </ul>
    </li>
      

   
    <!-- <li><span class="badge badge-pill badge-danger">New</span><a class="bar-icons" href="javascript:void(0)"><i
          class="icon-agenda"></i><span>UMKM</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">UMKM dan Koperasi</li>
        <li><a href="<?= base_url(); ?>umkm">Dashboard UMKM</a></li>
        <li><a href="<?= base_url(); ?>koperasi">Dashboard Koperasi</a></li>
      </ul>
    </li> -->
    
   

    <!-- <li><a class="bar-icons" href="javascript:void(0)"><i class="icon-shopping-cart"></i><span>ASN</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">ASN</li>
        <li><a href="<?= base_url(); ?>kepegawaian">Dashboard</a></li>
      </ul>
    </li> -->
    <!-- <li><a class="bar-icons" href="javascript:void(0)"><i class="icofont icofont-paper"></i><span>SAKIP</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">SAKIP </li>
        <li><a href="<?= base_url(); ?>sakip">Dashboard</a></li>
        <li><a href="<?= base_url(); ?>esakip">Data Sakip</a></li>
        <li><a href="#">Sumber Data</a></li>
      </ul> -->
    <!-- </li>
    <li><a class="bar-icons" href="javascript:void(0)"><i
          class="icofont icofont-bus-alt-1"></i><span>Transportasi</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Transportasi </li>
        <li><a href="<?= base_url(); ?>Transportasi">Dashboard</a></li>
        <li><a href="<?= base_url(); ?>parkir">Parkir</a></li>
      </ul>
    </li>

    <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-graph3"></i><span>Perumahan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Perumahan </li>
        <li><a href="<?= base_url(); ?>perkim">Dashboard</a></li>
      </ul>
    </li>

    <li><a class="bar-icons" href="javascript:void(0)"><i class="pe-7s-graph3"></i><span>Kehutanan</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Kehutanan </li>
        <li><a href="<?= base_url(); ?>kehutanan">Dashboard</a></li>
      </ul>
    </li> -->
    <li><a class="bar-icons" href="javascript:void(0)"><i data-feather="users"></i><span>Setting</span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">User </li>
        <li><a href="<?= base_url(); ?>manage_user">Manage User</a></li>

      </ul>
    </li>
    <li><a class="bar-icons" href="javascript:void(0)">
        <!--img(src='<?= base_url(); ?>asset/dashboard/images/menu/home.png' alt='')--><i
          class="pe-7s-home"></i><span>Master Statistik </span></a>
      <ul class="iconbar-mainmenu custom-scrollbar">
        <li class="iconbar-header">Card Dashboard utama</li>
        <li><a href="<?= base_url(); ?>investasi">-APBD</a></li>
        <!-- <li><a href="<?= base_url(); ?>pendapatan_pajak	">-Realisasi Anggaran</a></li>
        <li><a href="<?= base_url(); ?>belanja_langsung	">-Realisasi Pendapatan</a></li>
        <li><a href="<?= base_url(); ?>grafikutama">-Grafis</a></li>
        <li><a href="<?= base_url(); ?>grutama">-Grutama</a></li> -->
        <!-- <li><a href="<?= base_url(); ?>ekemiskinan	">-Kemiskinan</a></li> -->
      
      <li class="iconbar-header">Card Pemerintahan</li> 
        <li><a href="<?= base_url(); ?>eparpol">-Parpol</a></li>
        <li><a href="<?= base_url(); ?>eormas">-Ormas</a></li>
        <li><a href="<?= base_url(); ?>esakip">-Sakip</a></li>
      <li class="iconbar-header">Card Ekonomi</li> 
      <!-- <li><a href="<?= base_url(); ?>ekoper">-Koperasi</a></li> -->
      <!-- <li><a href="<?= base_url(); ?>disduk">-pendapatan</a></li> -->
        
      </ul>
    </li>
  </ul>
</div>
<!-- END: Main Menu-->