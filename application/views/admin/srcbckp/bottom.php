


<!-- latest jquery-->
<script src="<?=base_url();?>asset/dashboard/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap js-->
<script src="<?=base_url();?>asset/dashboard/js/bootstrap/popper.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/bootstrap/bootstrap.js"></script>
<!-- feather icon js-->
<script src="<?=base_url();?>asset/dashboard/js/icons/feather-icon/feather.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/icons/feather-icon/feather-icon.js"></script>
<!-- Sidebar jquery-->
<script src="<?=base_url();?>asset/dashboard/js/sidebar-menu.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/config.js"></script>
<!-- Plugins JS start-->
<script src="<?=base_url();?>asset/dashboard/js/chart/chartist/chartist.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chart/apex-chart/apex-chart.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chart/apex-chart/stock-prices.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/prism/prism.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/clipboard/clipboard.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/counter/jquery.waypoints.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/counter/jquery.counterup.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/counter/counter-custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/custom-card/custom-card.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/notify/bootstrap-notify.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/dashboard/default.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/notify/index.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datepicker/date-picker/datepicker.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datepicker/date-picker/datepicker.en.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datepicker/date-picker/datepicker.custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chat-menu.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/modal-animated.js"></script>

<script src="<?=base_url();?>asset/dashboard/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datatable/datatable-extension/buttons.html5.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datatable/datatables/datatable.custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/datatable/datatable-extension/custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chat-menu.js"></script>

<!-- Plugins JS Ends-->
<script src="<?=base_url();?>asset/dashboard/js/tour/intro.js"></script>

<!-- Plugins JS start-->

<!-- <script src="https://e-office.sumedangkab.go.id/asset/pixel/plugins/bower_components/sweetalert/sweetalert.min.js"></script> -->
<script src="<?=base_url();?>asset/dashboard/js/sweet-alert/sweetalert.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/sweet-alert/app.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/dashboard/ecommerce-custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chart-widget.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/chart/apex-chart/chart-custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/custom-card/custom-card.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/owlcarousel/owl.carousel.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/dashboard/crypto-custom.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/select2/select2.full.min.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/select2/select2-custom.js"></script>

<!-- Plugins JS Ends-->


<!-- Theme js-->
<script src="<?=base_url();?>asset/dashboard/js/script.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/theme-customizer/customizer.js"></script>
<!-- login js-->
<!-- Plugin used-->
<script src="<?=base_url();?>asset/dashboard/js/jquery.inputmask.bundle.min.js"></script>

<script src="<?=base_url();?>asset/dashboard/js/jquery.scrollTo.js"></script>
<script src="<?=base_url();?>asset/dashboard/js/jquery.scrollTo.min.js"></script>


<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
crossorigin="" ></script>

<script type="text/javascript">
	var cika_i;
	var cika_txt;
	var cika_speed;
	var cika_element;
	var cika_timeout;
	var cika2_timeout;
	function typing(text,element = "#cika-text") {
		if (cika_timeout) clearTimeout(cika_timeout);
		cika_i = 0;
		cika_txt = text;
		cika_speed = 50;
		cika_element = element;
		cika_timeout = setTimeout(function(){ document.querySelector(cika_element).innerHTML = ""; typeWriter(); }, 300);
	}
	function typeWriter() {
		if (cika_i < cika_txt.length) {
			document.querySelector(cika_element).innerHTML += cika_txt.charAt(cika_i);
			cika_i++;
			cika_timeout = setTimeout(typeWriter, cika_speed);
		} else {

		}
	}
</script>

<script type="text/javascript">
	function hello_cika() {
		if (cika_timeout) clearTimeout(cika_timeout);
		if (cika2_timeout) clearTimeout(cika2_timeout);
		$("#cika-open").attr("onmouseover", "");
		$("#cika-open").attr("onclick", "");
		$("#cika-chat").attr("onclick", "");
		var cika1 = 'Selamat datang di website command center kabupaten bogor .';
		typing(cika1);
		cika2_timeout = setTimeout(function(){ 
			typing('adakah yang saya bisa bantu?'); 
			$("#cika-open").attr("onclick", "rekomendasi_cika();");
			$("#cika-chat").attr("onclick", "rekomendasi_cika();");
			// $("#cika-top").attr("ontouchend", "rekomendasi_cika();");
			$("#cika-top").addClass("glow-shadow");
		}, (cika1.length*cika_speed)+2000);
	}

	function rekomendasi_cika() {
		if (typeof steps_intro !== "undefined") {
			for (var i = steps_intro.length - 1; i >= 0; i--) {
				steps_intro[i]
				document.querySelector(steps_intro[i].element).setAttribute("data-intro",steps_intro[i].intro);
			}
		}
		if (typeof steps !== "undefined") {
			if (steps.length == 0) {
				introJs().start();
			} else {
				introJs().setOptions({
					steps: steps,
					showStepNumbers: false,
					showBullets: false,
					keyboardNavigation: false,
				}).start();
			}
		} else {
			introJs().setOptions({
				steps: [{
					title: 'Welcome',
					intro: 'Halo saya CiKa, senang bertemu dengan anda! 👋'
				},
				{
					element: document.querySelector('#total-investasi'),
					intro: 'This step focuses on an image'
				},
				{
					title: 'Farewell!',
					element: document.querySelector('#total-pajak'),
					intro: 'And this is our final step!'
				}]
			}).start();
		}
	}
</script>

<script type="text/javascript">
	function load_dashboard(dashboard,uridash="dashboard") {
		$.ajax({                                      
			url: "<?=base_url($this->router->fetch_class())?>/"+uridash,              
			type: "GET",
			dataType: "HTML",
			beforeSend: function() {
				notif_load();
			},
			success: function(data) {
				$('#'+dashboard+'-dashboard').html(data);
				load_script(dashboard);
				$.notifyClose();
				notif_load_success();
				setAutoScroll();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$.notifyClose();
				notif_load_error();
				setTimeout(() => { $.ajax(this) }, 10000);
			}
		});
	}

	function load_script(dashboard="") {
		$('.datatables').DataTable();
		$('.datatables-mini').DataTable( {
			dom: 't',
			responsive: true,
			pageLength: 1,
            // paging:   false,
            ordering: false,
            info:     false
        } );
		$('#footer-mini').css('padding-left','90px');
		$('#datatable-export').DataTable( {
			dom: 'Bfrtip',
			buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
			]
		} );
		$(".select2").select2();
		$(".currency").inputmask({ alias : "currency", prefix: 'Rp', removeMaskOnSubmit: true });
		feather.replace();

	}

	load_script();

</script>

<script type="text/javascript">
	function notif_load() {
		$.notify({
			title:'Mengambil data...',
			message:'',
			icon:'icon fa fa-spin fa-circle-o-notch'
		},
		{
			type:'primary',
			allow_dismiss:false,
			newest_on_top:false ,
			mouse_over:false,
			showProgressbar:false,
			spacing:10,
			timer:0,
			placement:{
				from:'bottom',
				align:'right'
			},
			offset:{
				x:30,
				y:30
			},
			delay:1000 ,
			z_index:10000,
			animate:{
				enter:'animated pulse',
				exit:'animated fadeOut'
			}
		});
	}
	function notif_load_success() {
		$.notify({
			title:'Data berhasil diperbarui...',
			message:'',
			icon:'icon fa fa-check-square'
		},
		{
			type:'success',
			allow_dismiss:true,
			newest_on_top:false ,
			mouse_over:true,
			showProgressbar:false,
			spacing:10,
			timer:2000,
			placement:{
				from:'bottom',
				align:'right'
			},
			offset:{
				x:30,
				y:30
			},
			delay:1000 ,
			z_index:10000,
			animate:{
				enter:'animated pulse',
				exit:'animated fadeOut'
			}
		});
	}
	function notif_load_error() {
		$.notify({
			title:'Gagal mendapatkan data...',
			message:'',
			icon:'icon fa fa-warning'
		},
		{
			type:'secondary',
			allow_dismiss:true,
			newest_on_top:false ,
			mouse_over:true,
			showProgressbar:false,
			spacing:10,
			timer:2000,
			placement:{
				from:'bottom',
				align:'right'
			},
			offset:{
				x:30,
				y:30
			},
			delay:1000 ,
			z_index:10000,
			animate:{
				enter:'animated pulse',
				exit:'animated fadeOut'
			}
		});
	}
</script>

<script type="text/javascript">
	var timeout = false;
	var autopage = false;
	var autotable = false;
	var Height = document.documentElement.scrollHeight-document.documentElement.clientHeight;
	var currentHeight = document.documentElement.scrollTop;
	var bool = true;
	var step = 0.1;
	var speed = 1;

	function setAutoScroll() {
		if (timeout) clearTimeout(timeout);
		if (autopage) clearInterval(autopage);
        // if (autotable) clearInterval(autotable);
        $('#footer-mini').fadeOut();
	    // timeout = setTimeout(userIsIdle, 60000);    //10 menit
	}

	function userIsIdle() {
		Height = document.documentElement.scrollHeight-document.documentElement.clientHeight;
		currentHeight = document.documentElement.scrollTop;
		autopage = setInterval(scrollpage, speed);
        // autotable = setInterval(scrolldatatable, 5000);
        // $('#footer-mini').fadeIn();
    }

    function scrollpage() {
    	if (currentHeight < 0 || currentHeight > Height) 
    		bool = !bool;
    	if (bool) {
    		window.scrollTo(0, currentHeight += step);
    	} else {
    		window.scrollTo(0, currentHeight -= step);
    	}
    }

    function scrolldatatable() {
    	var datatable = $('.datatables').DataTable();
    	var info = datatable.page.info();
    	var pageNum = (info.page < info.pages) ? info.page + 1 : 1;
    	datatable.page(pageNum).draw(false);  

    	var datatable_mini = $('.datatables-mini').DataTable();
    	var info = datatable_mini.page.info();
    	var pageNum = (info.page < info.pages) ? info.page + 1 : 1;
    	datatable_mini.page(pageNum).draw(false);   
    }
</script>

<script type="text/javascript" defer>
	$(document).ready(function(){
		if (dashboard) {
			if (typeof uridash === 'undefined') { load_dashboard(dashboard);}
			else {load_dashboard(dashboard,uridash);}
			setInterval(function() {
				if (typeof uridash === 'undefined') { load_dashboard(dashboard);}
				else {load_dashboard(dashboard,uridash);}
			}, 600000);

        // timeout = setTimeout(userIsIdle, 10000);
        $(document.body).bind('keydown keyup mousemove mouseup scroll touchstart touchmove click', function() {
        	setAutoScroll();
        });
    }

    $('#data-table').DataTable({
    	scrollY: '40vh',
    	scrollCollapse: true,
    	paging:         false
    });

    
});

  // $(document).ready(function(){
  //   if (dashboard_map) {
  //       load_dashboard_map(dashboard_map);
  //       setInterval(function() {
  //           load_dashboard_map(dashboard_map);
  //       }, 600000);

  //       // timeout = setTimeout(userIsIdle, 10000);
  //       // $(document.body).bind('keydown keyup mousemove mouseup scroll touchstart touchmove click', function() {
  //       //     // setAutoScroll();
  //       // });
  //   }
  // });
  <?php if(!empty($page) && $page=="desa" && $this->uri->segment(2) != 'detail'){
	 $sp_desa = 0;
	 $sp_kelurahan = 0;
	 $sp_upt = 0;
	 $sp_nagari = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['status_pemerintahan'] as $key => $value) {
			if ($value['status_pemerintahan'] == 'Desa') {
				 $sp_desa = $value['total'];
			}elseif ($value['status_pemerintahan'] == 'Kelurahan') {
				 $sp_kelurahan = $value['total'];
			}elseif ($value['status_pemerintahan'] == 'UPT/SPT') {
				$sp_upt = $value['total'];
			}elseif ($value['status_pemerintahan'] == 'Nagari') {
				$sp_nagari = $value['total'];
			}
		 }
	 }
	 $bmd_ada = 0;
	 $bmd_tidak_ada = 0;
	 $jml_listrik = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['badan_permusyawaratan'] as $key => $value) {
			if ($value['badan_permusyawaratan'] == 'Ada') {
				 $bmd_ada = $value['total'];
			}elseif ($value['badan_permusyawaratan'] == 'Tidak Ada') {
				 $bmd_tidak_ada = $value['total'];
			}
		 }
		 $jml_listrik = round($sdgs['jml']['jml_listrik_pln']['jml_listrik_pln']) + round($sdgs['jml']['jml_listrik_nonpln']['jml_listrik_nonpln']);
	 }
	 $pu_besar = 0;
	 $pu_kecil = 0;
	 $pu_tidak_ada = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['penerangan_utama'] as $key => $value) {
			if ($value['penerangan_utama'] == 'Ada, sebagian kecil') {
				 $pu_besar = $value['total'];
			}elseif ($value['penerangan_utama'] == 'Ada , sebagian besar') {
				 $pu_kecil = $value['total'];
			}elseif ($value['penerangan_utama'] == 'Tidak Ada') {
				$pu_tidak_ada = $value['total'];
			}
		 }
	 }
	 $bb_gas_kota = 0;
	 $bb_lpghijau = 0;
	 $bb_lpg_biru = 0;
	 $bb_minyak_tanah = 0;
	 $bb_kayubakar = 0;
	 $bb_lainnya = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['bb_memasak_keluarga'] as $key => $value) {
			if ($value['bb_memasak_keluarga'] == 'bb_gas_kota') {
				 $bb_gas_kota = $value['total'];
			}elseif ($value['bb_memasak_keluarga'] == 'bb_lpghijau') {
				 $bb_lpghijau = $value['total'];
			}elseif ($value['bb_memasak_keluarga'] == 'bb_lpg_biru') {
				$bb_lpg_biru = $value['total'];
			}elseif ($value['bb_memasak_keluarga'] == 'bb_minyak_tanah') {
				$bb_minyak_tanah = $value['total'];
			}elseif ($value['bb_memasak_keluarga'] == 'bb_kayubakar') {
				$bb_kayubakar = $value['total'];
			}elseif ($value['bb_memasak_keluarga'] == 'bb_lainnya') {
				$bb_lainnya = $value['total'];
			}
		 }
	 }
	 $ps_tempatsampah = 0;
	 $ps_lubang = 0;
	 $ps_sungai = 0;
	 $ps_drainase = 0;
	 $ps_lainnya = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['tempat_sampah_keluarga'] as $key => $value) {
			if ($value['tempat_sampah_keluarga'] == 'ps_tempatsampah') {
				 $ps_tempatsampah = $value['total'];
			}elseif ($value['tempat_sampah_keluarga'] == 'ps_lubang') {
				 $ps_lubang = $value['total'];
			}elseif ($value['tempat_sampah_keluarga'] == 'ps_sungai') {
				$ps_sungai = $value['total'];
			}elseif ($value['tempat_sampah_keluarga'] == 'ps_drainase') {
				$ps_drainase = $value['total'];
			}elseif ($value['tempat_sampah_keluarga'] == 'ps_lainnya') {
				$ps_lainnya = $value['total'];
			}
		 }
	 }
	 $fj_sendiri = 0;
	 $fj_bersama = 0;
	 $fj_umum = 0;
	 $fj_bukan = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['bab_keluarga'] as $key => $value) {
			if ($value['bab_keluarga'] == 'Jamban Sendiri') {
				 $fj_sendiri = $value['total'];
			}elseif ($value['bab_keluarga'] == 'Jamban Bersama') {
				 $fj_bersama = $value['total'];
			}elseif ($value['bab_keluarga'] == 'Jamban Umum') {
				$fj_umum = $value['total'];
			}elseif ($value['bab_keluarga'] == 'Bukan Jamban') {
				$fj_bukan = $value['total'];
			}
		 }
	 }
	 $bt_lubang = 0;
	 $bt_tangki = 0;
	 $bt_sawah = 0;
	 $bt_lainnya = 0;
	 if (isset($sdgs)) {
		 foreach ($sdgs['kat']['buang_tinja_keluarga'] as $key => $value) {
			if ($value['buang_tinja_keluarga'] == 'Lubang Tanah') {
				 $bt_lubang = $value['total'];
			}elseif ($value['buang_tinja_keluarga'] == 'Tangki/instalasi Pengelolaan Air Limbah') {
				 $bt_tangki = $value['total'];
			}elseif ($value['buang_tinja_keluarga'] == 'Sawah/Kolam/Sungai/Danau/Laut atau Pantai/Tanah Lapang/Kebun') {
				$bt_sawah = $value['total'];
			}elseif ($value['buang_tinja_keluarga'] == 'Lainnya') {
				$bt_lainnya = $value['total'];
			}
		 }
	 }
	?>
	// Kelayakan Kantor Chart
	var options = {
          series: [<?=$sp_desa?>, <?=$sp_kelurahan?>, <?=$sp_upt?>, <?=$sp_nagari?>],
          chart: {
          width: 380,
          type: 'pie',
        },
        labels: ['Desa', 'Kelurahan', 'UPT/SPT', 'Nagari'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#kelayakan_kantor_chart"), options);
        chart.render();
	// Kelayakan Kantor Chart END !
	// BMD CHART	
	var options = {
          series: [<?=$bmd_ada?>, <?=$bmd_tidak_ada?>],
          chart: {
          width: 380,
          type: 'pie',
        },
        labels: ['Ada', 'Tidak'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#bmd_chart"), options);
        chart.render();
	//BMD CHART END !	
	// Kependudukan CHART	
	var options = {
          series: [44, 55],
          chart: {
          width: 380,
          type: 'pie',
        },
        labels: ['Laki-laki', 'Perempuan'],
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#kependudukan_chart"), options);
        chart.render();
	//Kependudukan CHART END !
	//Pengguna Listrik Chart
	var options = {
          series: [<?=round($jml_listrik)?>, <?=round($sdgs['jml']['jml_non_listrik']['jml_non_listrik'])?>],
          chart: {
          type: 'donut',
        },
		labels: ['Sudah', 'Belum'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#pengguna_listrik_chart"), options);
        chart.render();
	//Pengguna Listrik Chart END	
	//Sumber Listrik Chart
	var options = {
          series: [<?=round($sdgs['jml']['jml_listrik_pln']['jml_listrik_pln'])?>, <?=round($sdgs['jml']['jml_listrik_nonpln']['jml_listrik_nonpln'])?>],
          chart: {
          type: 'donut',
        },
		labels: ['PLN', 'Non PLN'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#sumber_listrik_chart"), options);
        chart.render();
	//Sumber Listrik Chart END	
	//Sumber Listrik Chart
	var options = {
          series: [<?=$pu_besar?>, <?=$pu_kecil?>, <?=$pu_tidak_ada?>],
          chart: {
          type: 'donut',
        },
		labels: ['Sebagian Besar', 'Sebagian Kecil', 'Tidak ada'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#penerangan_jalan_chart"), options);
        chart.render();
	//Sumber Listrik Chart END	
	// Bahan Bakar Masak
	var options = {
          series: [{
          data: [<?=$bb_gas_kota?>, <?=$bb_lpghijau?>, <?=$bb_lpg_biru?>, <?=$bb_minyak_tanah?>, <?=$bb_kayubakar?>, <?=$bb_lainnya?>]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
			  'Gas Kota',
			  'LPG 3 kg', 
			  'LPG > 3 kg', 
			  'Minyak Tanah', 
			  'Kayu Bakar', 
			  'Lainnya',
          ],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#bahan_bakar_masak_chart"), options);
        chart.render();
	//Bahan Bakar Masak End !
	// Tempat Buang Sampah
	var options = {
          series: [{
          data: [<?=$ps_tempatsampah?>, <?=$ps_lubang?>, <?=$ps_sungai?>, <?=$ps_drainase?>, <?=$ps_lainnya?>]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
			horizontal: true,
            columnWidth: '45%',
            distributed: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
			  'Tempat Sampah / Diangkut',
			  'Lubang / Dibakar',
			  'Sungai / Saluran Irigiasi',
			  'Drainese',
			  'Lainnya',
          ],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#tempat_buang_sampah_chart"), options);
        chart.render();
	//Tempat Buang Sampah End !
	//BAB CHART

		var options = {
          series: [<?=$fj_sendiri?>, <?=$fj_bersama?>, <?=$fj_umum?>, <?=$fj_bukan?>],
          chart: {
          width: 400,
          type: 'pie',
        },
        labels: ['Jamban Sendiri', 'Jamban Bersama', 'Jamban Umum', 'Bukan Jamban'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };		

        var chart = new ApexCharts(document.querySelector("#buang_air_besar_chart"), options);
        chart.render();

	//BAB CHART END !
	//Pembuangan Akhir Tinja CHART

		var options = {
          series: [<?=$bt_tangki?>, <?=$bt_sawah?>, <?=$bt_lubang?>, <?=$bt_lainnya?>],
          chart: {
          width: 400,
          type: 'pie',
        },
        labels: ['Tanki/Instalasi .. .', 'Sawah/Kolam ...', 'Lubang tanah', 'Lainnya'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };	

        var chart = new ApexCharts(document.querySelector("#pembuangan_akhir_tinja_chart"), options);
        chart.render();
	//Pembuangan Akhir Tinja CHART END !
	//Bencana Alam dan Mitigasi CHART
		
	var options = {
          series: [{
		  name: "Banyak Kejadian",
          data: [<?=$sdgs['jml']['bencana_kejadian_lalu_longsor']['bencana_kejadian_lalu_longsor']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_banjir']['bencana_kejadian_lalu_banjir']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_banjir_bandang']['bencana_kejadian_lalu_banjir_bandang']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_gempa']['bencana_kejadian_lalu_gempa']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_tsunami']['bencana_kejadian_lalu_tsunami']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_gelombang_pasang']['bencana_kejadian_lalu_gelombang_pasang']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_puyuh']['bencana_kejadian_lalu_puyuh']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_gunung_meletus']['bencana_kejadian_lalu_gunung_meletus']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_kebakaran_hutan']['bencana_kejadian_lalu_kebakaran_hutan']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_lalu_kekeringan']['bencana_kejadian_lalu_kekeringan']?>]
        }, {
		  name: "Korban Jiwa",
          data: [<?=$sdgs['jml']['bencana_korban_lalu_longsor']['bencana_korban_lalu_longsor']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_banjir']['bencana_korban_lalu_banjir']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_banjir_bandang']['bencana_korban_lalu_banjir_bandang']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_gempa']['bencana_korban_lalu_gempa']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_tsunami']['bencana_korban_lalu_tsunami']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_gelombang_pasang']['bencana_korban_lalu_gelombang_pasang']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_puyuh']['bencana_korban_lalu_puyuh']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_gunung_meletus']['bencana_korban_lalu_gunung_meletus']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_kebakaran_hutan']['bencana_korban_lalu_kebakaran_hutan']?>, 
		  <?=$sdgs['jml']['bencana_korban_lalu_kekeringan']['bencana_korban_lalu_kekeringan']?>]
        }],
          chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: ["Tanah Longsor", "Banjir", "Banjir Bandang", "Gempa Bumi", "Tsunami", "Gelombang pasang laut", "Angin puyuh/puting beliung/ topan"],
        },
        };

        var chart = new ApexCharts(document.querySelector("#b_a_d_m_b_a_prev_year"), options);
        chart.render();

	var options = {
          series: [{
		  name: "Banyak Kejadian",
          data: [<?=$sdgs['jml']['bencana_kejadian_sekarang_longsor']['bencana_kejadian_sekarang_longsor']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_banjir']['bencana_kejadian_sekarang_banjir']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_banjir_bandang']['bencana_kejadian_sekarang_banjir_bandang']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_gempa']['bencana_kejadian_sekarang_gempa']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_tsunami']['bencana_kejadian_sekarang_tsunami']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_gelombang_pasang']['bencana_kejadian_sekarang_gelombang_pasang']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_puyuh']['bencana_kejadian_sekarang_puyuh']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_gunung_meletus']['bencana_kejadian_sekarang_gunung_meletus']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_kebakaran_hutan']['bencana_kejadian_sekarang_kebakaran_hutan']?>, 
		  <?=$sdgs['jml']['bencana_kejadian_sekarang_kekeringan']['bencana_kejadian_sekarang_kekeringan']?>]
        }, {
		  name: "Jumlah Korban",
          data: [<?=$sdgs['jml']['bencana_korban_sekarang_longsor']['bencana_korban_sekarang_longsor']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_banjir']['bencana_korban_sekarang_banjir']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_banjir_bandang']['bencana_korban_sekarang_banjir_bandang']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_gempa']['bencana_korban_sekarang_gempa']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_tsunami']['bencana_korban_sekarang_tsunami']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_gelombang_pasang']['bencana_korban_sekarang_gelombang_pasang']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_puyuh']['bencana_korban_sekarang_puyuh']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_gunung_meletus']['bencana_korban_sekarang_gunung_meletus']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_kebakaran_hutan']['bencana_korban_sekarang_kebakaran_hutan']?>, 
		  <?=$sdgs['jml']['bencana_korban_sekarang_kekeringan']['bencana_korban_sekarang_kekeringan']?>]
        }],
          chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: ["Tanah Longsor", "Banjir", "Banjir Bandang", "Gempa Bumi", "Tsunami", "Gelombang pasang laut", "Angin puyuh/puting beliung/ topan"],
        },
        };

        var chart = new ApexCharts(document.querySelector("#b_a_d_m_b_a_this_year"), options);
        chart.render();
      
      
	//Bencana Alam dan Mitigasi END !
  <?php } ?>
  <?php if(!empty($page) && $page=="desa" && $this->uri->segment(2) == 'detail'){
	$jml_listrik_pln_0 = $sdgs_detail['pengisian_podes']['jml_listrik_pln'];
	$jml_listrik_nonpln_0 = $sdgs_detail['pengisian_podes']['jml_listrik_nonpln'];
	$jml_listrik_pln = ($jml_listrik_pln_0) ? $jml_listrik_pln_0 : 0 ;
	$jml_listrik_nonpln = ($jml_listrik_nonpln_0) ? $jml_listrik_nonpln_0 : 0 ;
	$jml_listrik = $jml_listrik_pln + $jml_listrik_nonpln;
	$jml_non_listrik_0 = $sdgs_detail['pengisian_podes']['jml_non_listrik'];
	$jml_non_listrik = ($jml_non_listrik_0) ? $jml_non_listrik_0 : 0 ;
	//Penerangan Jalan
	$jml_non_listrik_0 = $sdgs_detail['pengisian_podes']['jml_non_listrik'];
	$jml_non_listrik = ($jml_non_listrik_0) ? $jml_non_listrik_0 : 0 ;
	?>
	//Pengguna Listrik Chart
	var options = {
          series: [<?=$jml_listrik?>, <?=$jml_non_listrik?>],
          chart: {
		height: 280,
          type: 'donut',
        },
		labels: ['Sudah', 'Belum'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#pengguna_listrik_chart"), options);
        chart.render();
	//Pengguna Listrik Chart END	
	//Sumber Listrik Chart
	var options = {
          series: [<?=$jml_listrik_pln?>, <?=$jml_listrik_nonpln?>],
          chart: {
		  height: 280,
          type: 'donut',
        },
		labels: ['PLN', 'Non PLN'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#sumber_listrik_chart"), options);
        chart.render();
	//Sumber Listrik Chart END	
	//Sumber Listrik Chart
	var options = {
          series: [23, 11, 21],
          chart: {
          type: 'donut',
        },
		labels: ['Sebagian Besar', 'Sebagian Kecil', 'Tidak ada'],
		legend: {
			position: 'bottom'
		},
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart = new ApexCharts(document.querySelector("#penerangan_jalan_chart"), options);
        chart.render();
	//Sumber Listrik Chart END	
	// Bahan Bakar Masak
	var options = {
          series: [{
          data: [42, 11, 12, 23, 12, 12]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
            columnWidth: '45%',
            distributed: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
			  'Gas Kota',
			  'LPG 3 kg', 
			  'LPG > 3 kg', 
			  'Minyak Tanah', 
			  'Kayu Bakar', 
			  'Lainnya',
          ],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#bahan_bakar_masak_chart"), options);
        chart.render();
	//Bahan Bakar Masak End !
	// Tempat Buang Sampah
	var options = {
          series: [{
          data: [43, 12, 13, 15, 41]
        }],
          chart: {
          height: 350,
          type: 'bar',
          events: {
            click: function(chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: colors,
        plotOptions: {
          bar: {
			horizontal: true,
            columnWidth: '45%',
            distributed: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
			  'Tempat Sampah / Diangkut',
			  'Lubang / Dibakar',
			  'Sungai / Saluran Irigiasi',
			  'Drainese',
			  'Lainnya',
          ],
          labels: {
            style: {
              colors: colors,
              fontSize: '12px'
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#tempat_buang_sampah_chart"), options);
        chart.render();
	//Tempat Buang Sampah End !
	//BAB CHART
	
	var options = {
          series: [23, 12, 32, 41],
          chart: {
          height: 350,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            dataLabels: {
              name: {
                fontSize: '22px',
              },
              value: {
                fontSize: '16px',
              },
              total: {
                show: true,
                label: 'Total',
                formatter: function (w) {
                  // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                  return 31
                }
              }
            }
          }
        },
        labels: ['Jamban Sendiri', 'Jamban Bersama', 'Jamban Umum', 'Bukan Jamban'],
        };

        var chart = new ApexCharts(document.querySelector("#buang_air_besar_chart"), options);
        chart.render();

	//BAB CHART END !
	//Pembuangan Akhir Tinja CHART
	
	var options = {
          series: [12, 32, 123, 21],
          chart: {
          height: 350,
          type: 'radialBar',
        },
        plotOptions: {
          radialBar: {
            dataLabels: {
              name: {
                fontSize: '22px',
              },
              value: {
                fontSize: '16px',
              },
              total: {
                show: true,
                label: 'Total',
                formatter: function (w) {
                  // By default this function returns the average of all series. The below is just an example to show the use of custom formatter function
                  return 32
                }
              }
            }
          }
        },
        labels: ['Tanki/Instalasi .. .', 'Sawah/Kolam ...', 'Lubang tanah', 'Lainnya'],
        };

        var chart = new ApexCharts(document.querySelector("#pembuangan_akhir_tinja_chart"), options);
        chart.render();
	//Pembuangan Akhir Tinja CHART END !
	//Bencana Alam dan Mitigasi CHART
		
	var options = {
          series: [{
		  name: "Banyak Kejadian",
          data: [<?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_longsor']['bencana_kejadian_lalu_longsor']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_longsor']['bencana_kejadian_lalu_longsor']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir_bandang']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir_bandang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gempa']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gempa']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_tsunami']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_tsunami']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gelombang_pasang']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gelombang_pasang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_puyuh']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_puyuh']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gunung_meletus']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gunung_meletus']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kebakaran_hutan']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kebakaran_hutan']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kekeringan']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kekeringan']: 0;?>]
        }, {
		  name: "Korban Jiwa",
          data: [<?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_longsor']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_longsor']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir_bandang']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir_bandang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_gempa']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_gempa']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_tsunami']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_tsunami']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_gelombang_pasang']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_gelombang_pasang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_puyuh']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_puyuh']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_gunung_meletus']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_gunung_meletus']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_kebakaran_hutan']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_kebakaran_hutan']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_lalu_kekeringan']) ? $sdgs_detail['pengisian_podes']['bencana_korban_lalu_kekeringan']: 0;?>]
        }],
          chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: ["Tanah Longsor", "Banjir", "Banjir Bandang", "Gempa Bumi", "Tsunami", "Gelombang pasang laut", "Angin puyuh/puting beliung/ topan"],
        },
        };

        var chart = new ApexCharts(document.querySelector("#b_a_d_m_b_a_prev_year"), options);
        chart.render();

	var options = {
          series: [{
		  name: "Banyak Kejadian",
          data: [<?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_longsor']['bencana_kejadian_sekarang_longsor']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_longsor']['bencana_kejadian_sekarang_longsor']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir_bandang']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir_bandang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gempa']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gempa']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_tsunami']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_tsunami']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gelombang_pasang']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gelombang_pasang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_puyuh']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_puyuh']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gunung_meletus']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gunung_meletus']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kebakaran_hutan']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kebakaran_hutan']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kekeringan']) ? $sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kekeringan']: 0;?>]
        }, {
		  name: "Korban Jiwa",
          data: [<?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_longsor']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_longsor']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir_bandang']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir_bandang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gempa']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gempa']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_tsunami']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_tsunami']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gelombang_pasang']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gelombang_pasang']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_puyuh']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_puyuh']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gunung_meletus']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gunung_meletus']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kebakaran_hutan']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kebakaran_hutan']: 0;?>, 
		  <?=isset($sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kekeringan']) ? $sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kekeringan']: 0;?>]
        }],
          chart: {
          type: 'bar',
          height: 430
        },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
            },
          }
        },
        dataLabels: {
          enabled: true,
          offsetX: -6,
          style: {
            fontSize: '12px',
            colors: ['#fff']
          }
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#fff']
        },
        tooltip: {
          shared: true,
          intersect: false
        },
        xaxis: {
          categories: ["Tanah Longsor", "Banjir", "Banjir Bandang", "Gempa Bumi", "Tsunami", "Gelombang pasang laut", "Angin puyuh/puting beliung/ topan"],
        },
        };

        var chart = new ApexCharts(document.querySelector("#b_a_d_m_b_a_this_year"), options);
        chart.render();
      
      
	//Bencana Alam dan Mitigasi END !
  <?php } ?>
  <?php if(!empty($page) && $page=="dashboard"):?>

	var monthly = JSON.parse('<?= json_encode($data_mpp->monthly) ;?>');
	var monthly_label = JSON.parse('<?= json_encode($data_mpp->monthly_label) ;?>');

	var optionsLine = {
		yaxis: {
			show: false,
		},
		grid: {
			show: false,
		},

		xaxis: {
			axisBorder: {
				show: true,
				color: '#cccccc',
				height: 1,
				width: '100%',
				offsetX: 0,
				offsetY: 0
			},  
		},

		chart: {
			height: 360,
			type: 'line',
			zoom: {
				enabled: false
			},
			toolbar: {
				show: false
			}
		},
		stroke: {
			curve: 'smooth',
			width: 4
		},
		colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
		series: [{
			name: "Jumlah terlayani",
			data: monthly
		},

		],
		subtitle: {
			text: 'Statistics',
			offsetY: 55,
			offsetX: 20
		},
		markers: {
			size: 6,
			strokeWidth: 0,
			hover: {
				size: 9
			}
		},
		labels: monthly_label,
		legend: {
			position: 'top',
			horizontalAlign: 'right',
			offsetY: -20
		}
	}
	var gTerlayani = new ApexCharts(document.querySelector('#grafik-terlayani'), optionsLine);
	gTerlayani.render();


	//
	var monthly_label_perizinan = JSON.parse('<?= json_encode($data_perizinan->monthly_label) ;?>');

	var dt_total_izin = JSON.parse('<?= json_encode($data_perizinan->total_izin) ;?>');

	var options_izin_per_bulan = {
		chart: {
			height: 350,
			type: 'area',
			zoom: {
				enabled: false
			}

		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'straight'
		},
		series: [{
			name: "Total izin",
			data: dt_total_izin
		}],
		title: {
			text: 'Total perizinan',
			align: 'left'
		},
		subtitle: {
			text: 'Perbulan',
			align: 'left'
		},
		labels: monthly_label_perizinan,
		xaxis: {
			type: 'month',
		},
		yaxis: {
			opposite: true
		},
		legend: {
			horizontalAlign: 'left'
		},
		colors:['#7e37d8']

	}

	var grafik_izin_perbulan = new ApexCharts(
		document.querySelector("#grafik-izin-perbulan"),
		options_izin_per_bulan
		);

	grafik_izin_perbulan.render();


<?php endif?>

  <?php if(!empty($page) && $page=="mpp"):?>

  	var monthly = JSON.parse('<?= json_encode($data_mpp->monthly) ;?>');
  	var monthly_label = JSON.parse('<?= json_encode($data_mpp->monthly_label) ;?>');

  	var optionsLine = {
  		yaxis: {
  			show: false,
  		},
  		grid: {
  			show: false,
  		},

  		xaxis: {
  			axisBorder: {
  				show: true,
  				color: '#cccccc',
  				height: 1,
  				width: '100%',
  				offsetX: 0,
  				offsetY: 0
  			},  
  		},

  		chart: {
  			height: 360,
  			type: 'line',
  			zoom: {
  				enabled: false
  			},
  			toolbar: {
  				show: false
  			}
  		},
  		stroke: {
  			curve: 'smooth',
  			width: 4
  		},
  		colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  		series: [{
  			name: "Jumlah terlayani",
  			data: monthly
  		},

  		],
  		subtitle: {
  			text: 'Statistics',
  			offsetY: 55,
  			offsetX: 20
  		},
  		markers: {
  			size: 6,
  			strokeWidth: 0,
  			hover: {
  				size: 9
  			}
  		},
  		labels: monthly_label,
  		legend: {
  			position: 'top',
  			horizontalAlign: 'right',
  			offsetY: -20
  		}
  	}
  	var gTerlayani = new ApexCharts(document.querySelector('#grafik-terlayani'), optionsLine);
  	gTerlayani.render();


  	var options = {
  		chart: {
  			width: 490,
  			height: 300,
  			type: 'donut',
  		},
  		dataLabels: {
  			enabled: false
  		},
  		series: [<?=$data_mpp->online;?>,<?=$data_mpp->offline;?>],
  		labels: ['Online','Offline'],
  		responsive: [{
  			breakpoint: 200,
  			options: {
  				chart: {
  					width: 200
  				},
  				legend: {
  					show: false
  				}
  			}
  		}],
  		legend: {
  			position: 'bottom'
  		},
  		fill: {
  			opacity: 1
  		},
  		colors:['#7e37d8', '#06b5dd'],

  	}

  	var gJalur = new ApexCharts(
  		document.querySelector("#gJalur"),
  		options
  		);

  	gJalur.render()






  <?php endif?>

  <?php if(!empty($page) && $page=="mpp_skpd"):?>


  	var options = {
  		chart: {
  			width: 490,
  			height: 300,
  			type: 'donut',
  		},
  		dataLabels: {
  			enabled: false
  		},
  		series: [<?=$data->online;?>,<?=$data->offline;?>],
  		labels: ['Online','Offline'],
  		responsive: [{
  			breakpoint: 200,
  			options: {
  				chart: {
  					width: 200
  				},
  				legend: {
  					show: false
  				}
  			}
  		}],
  		legend: {
  			position: 'bottom'
  		},
  		fill: {
  			opacity: 1
  		},
  		colors:['#7e37d8', '#fd517d'],

  	}

  	var gJalur = new ApexCharts(
  		document.querySelector("#gJalur"),
  		options
  		);

  	gJalur.render()


  	var dt_online = JSON.parse('<?= json_encode($data->data[0]->online) ;?>');
  	var dt_offline = JSON.parse('<?= json_encode($data->data[0]->offline) ;?>');
  	var monthly_label = JSON.parse('<?= json_encode($data->monthly_label) ;?>');

  	var optionsLine = {
  		yaxis: {
  			show: false,
  		},
  		grid: {
  			show: false,
  		},

  		xaxis: {
  			axisBorder: {
  				show: true,
  				color: '#cccccc',
  				height: 1,
  				width: '100%',
  				offsetX: 0,
  				offsetY: 0
  			},  
  		},

  		chart: {
  			height: 360,
  			type: 'line',
  			zoom: {
  				enabled: false
  			},
  			toolbar: {
  				show: false
  			}
  		},
  		stroke: {
  			curve: 'smooth',
  			width: 4
  		},
  		colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  		series: [{
  			name: "Online",
  			data: dt_online
  		},
  		{
  			name: "Offline",
  			data: dt_offline
  		}

  		],
  		subtitle: {
  			text: 'Statistics',
  			offsetY: 55,
  			offsetX: 20
  		},
  		markers: {
  			size: 6,
  			strokeWidth: 0,
  			hover: {
  				size: 9
  			}
  		},
  		labels: monthly_label,
  		legend: {
  			position: 'top',
  			horizontalAlign: 'right',
  			offsetY: -20
  		}
  	}
  	var gTerlayani = new ApexCharts(document.querySelector('#grafik-terlayani'), optionsLine);
  	gTerlayani.render();


  <?php endif?>

  <?php if(!empty($page) && $page=="mpp_ikm"):?>

  	var monthly = JSON.parse('<?= json_encode($data->monthly) ;?>');
  	var monthly_label = JSON.parse('<?= json_encode($data->monthly_label) ;?>');

  	var optionsLine = {
  		yaxis: {
  			show: false,
  		},
  		grid: {
  			show: false,
  		},

  		xaxis: {
  			axisBorder: {
  				show: true,
  				color: '#cccccc',
  				height: 1,
  				width: '100%',
  				offsetX: 0,
  				offsetY: 0
  			},  
  		},

  		chart: {
  			height: 360,
  			type: 'line',
  			zoom: {
  				enabled: false
  			},
  			toolbar: {
  				show: false
  			}
  		},
  		stroke: {
  			curve: 'smooth',
  			width: 4
  		},
  		colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  		series: [{
  			name: "Jumlah responden",
  			data: monthly
  		},

  		],
  		subtitle: {
  			text: 'Statistics',
  			offsetY: 55,
  			offsetX: 20
  		},
  		markers: {
  			size: 6,
  			strokeWidth: 0,
  			hover: {
  				size: 9
  			}
  		},
  		labels: monthly_label,
  		legend: {
  			position: 'top',
  			horizontalAlign: 'right',
  			offsetY: -20
  		}
  	}
  	var gTerlayani = new ApexCharts(document.querySelector('#grafik-ikm'), optionsLine);
  	gTerlayani.render();

  <?php endif?>


  <?php if(!empty($page) && $page=="perizinan"):?>

  	var investasi_pmdn = JSON.parse('<?= json_encode($data_perizinan->investasi->pmdn) ;?>');
  	var investasi_pma = JSON.parse('<?= json_encode($data_perizinan->investasi->pma) ;?>');
  	var investasi_lainya = JSON.parse('<?= json_encode($data_perizinan->investasi->lainya) ;?>');
  	var monthly_label = JSON.parse('<?= json_encode($data_perizinan->monthly_label) ;?>');

  	var optionsLine = {
  		yaxis: {
  			show: false,
  		},
  		grid: {
  			show: false,
  		},

  		xaxis: {
  			axisBorder: {
  				show: true,
  				color: '#cccccc',
  				height: 1,
  				width: '100%',
  				offsetX: 0,
  				offsetY: 0
  			},  
  		},

  		chart: {
  			height: 360,
  			type: 'line',
  			zoom: {
  				enabled: false
  			},
  			toolbar: {
  				show: false
  			}
  		},
  		stroke: {
  			curve: 'smooth',
  			width: 4
  		},
  		colors: [pocoAdminConfig.primary, '#fd517d', '#ffc717'],
  		series: [{
  			name: "PMDN",
  			data: investasi_pmdn
  		},
  		{
  			name: "PMA",
  			data: investasi_pma
  		},
  		{
  			name: "Lainya",
  			data: investasi_lainya
  		}
  		],
  		subtitle: {
  			text: 'Investasi',
  			offsetY: 55,
  			offsetX: 20
  		},
  		markers: {
  			size: 6,
  			strokeWidth: 0,
  			hover: {
  				size: 9
  			}
  		},
  		labels: monthly_label,
  		legend: {
  			position: 'top',
  			horizontalAlign: 'right',
  			offsetY: -20
  		}
  	}
  	var chartLine = new ApexCharts(document.querySelector('#grafik-investasi'), optionsLine);
  	chartLine.render();

//
var options = {
	chart: {
		width: 490,
		height: 300,
		type: 'donut',
	},
	dataLabels: {
		enabled: false
	},
	series: [<?=$data_perizinan->total_izin_filter_proses;?>,<?=$data_perizinan->total_izin_filter_disetujui;?>,<?=$data_perizinan->total_izin_filter_ditolak;?>],
	labels: ['Sedang proses','Izin disetujui','Izin ditolak'],
	responsive: [{
		breakpoint: 200,
		options: {
			chart: {
				width: 200
			},
			legend: {
				show: false
			}
		}
	}],
	legend: {
		position: 'bottom'
	},
	fill: {
		opacity: 1
	},
	colors:['#ffc717','#7e37d8', '#fd517d'],

}

var grafik_status_perizinan = new ApexCharts(
	document.querySelector("#grafik-status-perizinan"),
	options
	);

grafik_status_perizinan.render();

//

var dt_total_izin = JSON.parse('<?= json_encode($data_perizinan->total_izin) ;?>');

var options_izin_per_bulan = {
	chart: {
		height: 350,
		type: 'area',
		zoom: {
			enabled: false
		}

	},
	dataLabels: {
		enabled: false
	},
	stroke: {
		curve: 'straight'
	},
	series: [{
		name: "Total izin",
		data: dt_total_izin
	}],
	title: {
		text: 'Total perizinan',
		align: 'left'
	},
	subtitle: {
		text: 'Perbulan',
		align: 'left'
	},
	labels: monthly_label,
	xaxis: {
		type: 'month',
	},
	yaxis: {
		opposite: true
	},
	legend: {
		horizontalAlign: 'left'
	},
	colors:['#7e37d8']

}

var grafik_izin_perbulan = new ApexCharts(
	document.querySelector("#grafik-izin-perbulan"),
	options_izin_per_bulan
	);

grafik_izin_perbulan.render();

var kec_data = JSON.parse('<?= json_encode($data_perizinan->kec_data) ;?>');
var kec_label = JSON.parse('<?= json_encode($data_perizinan->kec_label) ;?>');

var opt_grafik_izin_kecamatan = {
	chart: {
		height: 350,
		type: 'bar',
	},
	plotOptions: {
		bar: {
			horizontal: false,
			endingShape: 'rounded',
			columnWidth: '55%',
		},
	},
	dataLabels: {
		enabled: false
	},
	stroke: {
		show: true,
		width: 2,
		colors: ['transparent']
	},
	series: [{
		name: 'Total',
		data: kec_data
	}],
	xaxis: {
		categories: kec_label,
	},
	yaxis: {
		title: {
			text: 'Jumlah Izin'
		}
	},
	fill: {
		opacity: 1

	},
	tooltip: {
		y: {
			formatter: function (val) {
				return val + " izin"
			}
		}
	},
	colors:['#80cf00', '#fe80b2', '#80cf00']
}

var grafik_izin_kecamatan = new ApexCharts(
	document.querySelector("#grafik_izin_kecamatan"),
	opt_grafik_izin_kecamatan
	);

grafik_izin_kecamatan.render();


/// 
var data_options_grafik_performance_pelayanan = JSON.parse('<?= json_encode($data_perizinan->performance_layanan) ;?>');
var options_grafik_performance_pelayanan = {
	chart: {
		type: 'line',
		height: 350
	},
	stroke: {
		curve: 'stepline',
	},
	dataLabels: {
		enabled: false
	},
	series: [{
		data: data_options_grafik_performance_pelayanan
	}],
	title: {
		text: '1 tahun terakhir',
		align: 'left'
	},
	markers: {
		hover: {
			sizeOffset: 4
		}
	},
	colors:['#7e37d8']

}

var grafik_performance_pelayanan = new ApexCharts(
	document.querySelector("#grafik_performance_pelayanan"),
	options_grafik_performance_pelayanan
	);
grafik_performance_pelayanan.render();

////
var performance_layanan_level_data = JSON.parse('<?= json_encode($data_perizinan->performance_layanan_level_data) ;?>');
var performance_layanan_level_label = JSON.parse('<?= json_encode($data_perizinan->performance_layanan_level_label) ;?>');
var options_lama_proses = {
	chart: {
		height: 350,
		type: 'radar',
	},
	series: [{
		name: 'Performance',
		data: performance_layanan_level_data,
	}],
	labels: performance_layanan_level_label,
	plotOptions: {
		radar: {
			size: 140,
			polygons: {
				strokeColor: '#e9e9e9',
				fill: {
					colors: ['rgba(126,55,216,0.1)', '#fff'],
				}
			}
		}
	},
	title: {
		text: 'Performance per-level'
	},
	colors: ['#fe80b2'],
	markers: {
		size: 4,
		colors: ['#fe80b2'],
		strokeColor: '#fe80b2',
		strokeWidth: 2,
	},
	tooltip: {
		y: {
			formatter: function(val) {
				return val+ '%'
			}
		}
	},
	yaxis: {
		tickAmount: 7,
		labels: {
			formatter: function(val, i) {
				return val+ '%'
			}
		}
	},
	colors:['#fe80b2', '#000000']
}

var lama_proses = new ApexCharts(
	document.querySelector("#lama_proses"),
	options_lama_proses
	);

lama_proses.render();


// 
var izin_perbidang_label = JSON.parse('<?= json_encode($data_perizinan->izin_perbidang_label) ;?>');
var izin_perbidang_data = JSON.parse('<?= json_encode($data_perizinan->izin_perbidang_data) ;?>');
var options_izin_perbidang = {
	chart: {
		width: 490,
		height: 300,
		type: 'donut',
	},
	dataLabels: {
		enabled: false
	},
	series: izin_perbidang_data,
	labels: izin_perbidang_label,
	responsive: [{
		breakpoint: 200,
		options: {
			chart: {
				width: 200
			},
			legend: {
				show: false
			}
		}
	}],
	legend: {
		position: 'bottom'
	},
	fill: {
		opacity: 1
	},
	colors:['#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d'],

}

var grafik_izin_perbidang = new ApexCharts(
	document.querySelector("#izin_perbidang"),
	options_izin_perbidang
	);

grafik_izin_perbidang.render();

var izin_persektor_label = JSON.parse('<?= json_encode($data_perizinan->izin_persektor_label) ;?>');
var izin_persektor_data = JSON.parse('<?= json_encode($data_perizinan->izin_persektor_data) ;?>');
var options_izin_persektor = {
	chart: {
		width: 490,
		height: 300,
		type: 'donut',
	},
	dataLabels: {
		enabled: false
	},
	series: izin_persektor_data,
	labels: izin_persektor_label,
	responsive: [{
		breakpoint: 200,
		options: {
			chart: {
				width: 200
			},
			legend: {
				show: false
			}
		}
	}],
	legend: {
		position: 'bottom'
	},
	fill: {
		opacity: 1
	},
	colors:['#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d'],

}

var grafik_izin_persektor = new ApexCharts(
	document.querySelector("#izin_persektor"),
	options_izin_persektor
	);

grafik_izin_persektor.render();


//
var izin_perlayanan_label = JSON.parse('<?= json_encode($data_perizinan->izin_perlayanan_label) ;?>');
var izin_perlayanan_data = JSON.parse('<?= json_encode($data_perizinan->izin_perlayanan_data) ;?>');
var options_izin_perlayanan = {
	chart: {
		width: 490,
		height: 300,
		type: 'donut',
	},
	dataLabels: {
		enabled: false
	},
	series: izin_perlayanan_data,
	labels: izin_perlayanan_label,
	responsive: [{
		breakpoint: 200,
		options: {
			chart: {
				width: 200
			},
			legend: {
				show: false
			}
		}
	}],
	legend: {
		position: 'bottom'
	},
	fill: {
		opacity: 1
	},
	colors:['#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d','#ffc717','#7e37d8', '#fd517d'],

}

var grafik_izin_perlayanan = new ApexCharts(
	document.querySelector("#izin_perlayanan"),
	options_izin_perlayanan
	);

grafik_izin_perlayanan.render();
<?php endif?>

$(document).ready(function() {


	if(typeof loadPagination==="function"){
		loadPagination(1);
	}
	var page_num = 1;
	var pagination = document.getElementById("pagination");
	if(pagination){
		$('#pagination').on('click','a',function(e){
			e.preventDefault();
			page_num = $(this).attr('data-ci-pagination-page');
			if(typeof loadPagination==="function"){
				loadPagination(page_num);
			}
		});

	}
}
);
</script>