<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="Dashboard Bogor">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?=base_url();?>asset/dashboard/images/logo/icon.png" type="image/x-icon">
    <title><?php echo $title ;?></title>
    <!-- Google font-->
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/fontawesome.css">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/feather-icon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/owlcarousel.css">

     <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/chartist.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/date-picker.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/prism.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/material-design-icon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/select2.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/pe7-icon.css">
    <!-- Plugins css Ends-->

     <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/datatable-extension.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/pe7-icon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/tour.css">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/bootstrap.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/bootstrap-modal-bs3patch.css"> -->
    <!-- App css-->

    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/style.css">

    <link id="color" rel="stylesheet" href="<?=base_url();?>asset/dashboard/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/rating.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>asset/dashboard/css/pe7-icon.css">
    
    

    
  </head>

  
