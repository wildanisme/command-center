<div class="container-fluid">
	
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo title($title) ?></h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                <ol class="breadcrumb">
                    <?php echo breadcrumb($this->uri->segment_array()); ?>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<?php
				$tipe = (empty($error))? "info" : "danger";
				if (!empty($message)){
			?>
				<div class="alert alert-<?= $tipe;?> alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <?= $message;?>
                  </div>
				<?php }?>
			<div class="x_panel">
				<form method='post' enctype="multipart/form-data" >
				<div class="x_content">
					<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
						<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<label id='status'></label>
					 </div>
					<div class="panel panel-default">
						<div class="panel-heading">
                            Edit Pegawai
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Nama Lengkap</label>
										<input type="text" value="<?=$nama_lengkap?>" name="nama_lengkap" class="form-control" placeholder="Masukkan Nama Lengkap">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>NIP / NRP</label>
										<input type="text" name="nip" value="<?=$nip?>" class="form-control" placeholder="Masukkan NIP / NRP">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Unit Kerja</label>
										<select name="id_unit_kerja" class="form-control select2">
											<option value="">Pilih Unit Kerja</option>
											<?php 
												foreach($unit_kerja as $uk){
													if($uk->id_unit_kerja==$id_unit_kerja){
														$selected = ' selected';
													}else{
														$selected = '';
													}
													echo'<option value="'.$uk->id_unit_kerja.'"'.$selected.'>'.$uk->nama_unit_kerja.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Jabatan</label>
										<select name="id_jabatan" class="form-control select2">
											<option value="">Pilih Jabatan</option>
											<?php 
												foreach($jabatan as $j){
													if($j->id_jabatan==$id_jabatan){
														$selected = ' selected';
													}else{
														$selected = '';
													}
													echo'<option value="'.$j->id_jabatan.'"'.$selected.'>'.$j->nama_jabatan.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Eselon</label>
										<select name="eselon" class="form-control">
											<option value="">Pilih Eselon</option>
											<?php 
												$eselonn = array('1'=>'I','2'=>'II','3'=>'III','4'=>'IV');
												foreach ($eselonn as $key => $value) {
													if($key==$eselon){
														$selected = ' selected';
													}else{
														$selected = '';
													}
													echo '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Tugas Pokok dan Fungsi</label>
										<textarea class="form-control" name="tugas_pokok_fungsi" placeholder="Masukkan Tugas Pokok dan Fungsi"><?=$tugas_pokok_fungsi?></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>  
					<div class="panel panel-default">
						<div class="panel-heading">File Foto</div>
						<div class="panel-body">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										<label>Upload Foto</label>
										<br />
										<img width="100px" src="<?= base_url()."data/user_picture/$foto";?>">
										<input type='hidden'	name='foto'  id='foto' " value='<?= $foto;?>' />
										<br />
										<br />
										<label>Ukuran foto maksimal <?= $max_size."KB (".$max_width." x ".$max_height." pixel)"?></label>
										<input type="file" class="form-control" name="userfile" placeholder="Tidak ada File"> 
										</div>
									</div>
								</div>
						</div>
					</div>
					
					<button type='submit' class='btn btn-primary'>Submit</button>
					<a href='<?= base_url();?>master_pegawai' class='btn btn-default'>Back</a>
					<label>*Wajib</label>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>

	function hideMe()
	{$('#pesan').hide();}
	
	function getKabupaten(){
		var id = $('#id_provinsi').val();
		$('#id_desa').html('<option value="">Pilih</option>');
		$('#id_kecamatan').html('<option value="">Pilih</option>');
		$.post("<?= base_url();?>master_pegawai/get_kabupaten/"+id,{},function(obj){
			$('#id_kabupaten').html(obj);
		});
		
	}
	function getKecamatan(){
		$('#id_desa').html('<option value="">Pilih</option>');
		var id = $('#id_kabupaten').val();
		$.post("<?= base_url();?>master_pegawai/get_kecamatan/"+id,{},function(obj){
			$('#id_kecamatan').html(obj);
		});
		
	}
	function getDesa(){
		var id = $('#id_kecamatan').val();
		$.post("<?= base_url();?>master_pegawai/get_desa/"+id,{},function(obj){
			$('#id_desa').html(obj);
		});
	}
</script>