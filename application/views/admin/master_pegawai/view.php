<div class="container-fluid">

    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?php echo title($title) ?></h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                <ol class="breadcrumb">
                    <?php echo breadcrumb($this->uri->segment_array()); ?>
              </ol>
        </div>
        <!-- /.col-lg-12 -->
  </div>

  <div class="white-box row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="row">


      <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar-tabs" role="tablist">

                  <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Informasi Pegawai</a>
                  </li>
                  
            </li>
            
                    <?php if($registered){ ?>
                          <li role="presentation" class=""><a href="#tab_content12" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pengaturan Akun</a>
                          </li>
                    <?php } ?>


              </ul>
  </div>
</div>

<div class="x_panel" style="margin-top: 10px;">
 <div class="x_content">

  <div class="col-md-2 col-sm-2 col-xs-12 profile_left">

   <div class="profile_img">

    <!-- end of image cropping -->
    <div id="crop-avatar">
     <!-- Current avatar -->
     <?php if ($foto=="") $foto ="user_default.png";?>
     <img class="img-responsive avatar-view" src="<?php echo base_url()."data/user_picture/$foto" ?>" alt="Foto" title="<?= $nama_lengkap;?>">



     <!-- Loading state -->
     <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
</div>
<!-- end of image cropping -->

</div>
<h4><?= $nama_lengkap;?></h4>

<ul class="list-unstyled user_data">
    <li id='label_status'>Status: <?= $status;?></li>

</ul>


<div class="btn-group btn-block">
    <button data-toggle="dropdown" class="btn btn-primary btn-block dropdown-toggle" type="button" aria-expanded="false">Aksi <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
     <li><a href="<?= base_url()."master_pegawai/edit/$id_pegawai";?>">Edit</a>
     </li>
     <li>

      <?php
      if ($status=="belum diverifikasi"){
       echo "<a href='javascript:void(0)' id='btn-status'  onclick='proses()'>Proses verifikasi</a>";
 }
 else if ($status=="proses verifikasi"){
       echo "<a href='javascript:void(0)'' id='btn-status'  onclick='verifikasi()'>Verifikasi</a>";
 }
 else if ($status=="aktif"){
       echo "<a href='javascript:void(0)' id='btn-status'  onclick='nonaktif()'>Non Aktifkan</a>";
 }
 else if ($status=="non aktif"){
       echo "<a href='javascript:void(0)' id='btn-status' onclick='aktif()'>Aktifkan</a>";
 }
 ?>
 <?php 
 if($registered){
       ?>
       <a id="btn-register" href="javascript:void(0)" onclick="deleteRegister()">Hapus User</a>
 <?php }else{
       ?>
       <a id="btn-register" href="javascript:void(0)" onclick="showRegister()">Register User</a>
       <?php
 } ?>
       <a id="btn-register" href="javascript:void(0)" onclick="deletePegawai()">Hapus Pegawai</a>
</li>
</ul>
</div>
<!-- start skills -->


</div>
<div class="col-md-10 col-sm-9 col-xs-12">
   <div id="myTabContent" class="tab-content">
    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

     <div class="panel panel-default">
      <div class="panel-body">
       <div class="row">
        
<div class="col-md-12">
   <div class="form-group">
    <label>Nama lengkap</label>
    <input readonly class="form-control" name="nama_lengkap" id='nama_lengkap' placeholder='Nama lengkap' value='<?= $nama_lengkap;?>'>
</div>
</div>

<div class="col-md-12">
   <div class="form-group">
    <label>ID Identitas </label>
    <input readonly class="form-control" name="nama_lengkap" id='nama_lengkap' placeholder='' value='<?= $nip;?>'>
</div>
</div>

<div class="col-md-12">
   <div class="form-group">
    <label>Unit Kerja</label>
    <input readonly class="form-control" placeholder='Nama lengkap' value='<?= $nama_unit_kerja;?>'>
</div>
</div>


<div class="col-md-12">
   <div class="form-group">
    <label>Jabatan</label>
    <input readonly class="form-control" value='<?= $nama_jabatan;?>'>
</div>
</div>

<div class="col-md-12">
   <div class="form-group">
    <label>Eselon</label>
    <input readonly class="form-control" name="nama_lengkap" id='nama_lengkap' placeholder='Nama lengkap' value='Eselon <?= $nama_eselon;?>'>
</div>
</div>



</div>
</div>
</div>    
</div>
							<?php if($registered){ ?>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content12" style="min-height: 555px;">
                                    <div id="message">
                                        <!-- <div class="alert alert-success">User Setting successfully changed</div> -->
                                    </div>
                                    <form action="#" method="POST" id="form-setting" class="form-horizontal form-material">
                                        <div class="form-group">
                                            <label class="col-md-12">Username</label>
                                            <div class="col-md-12">
                                                <input type="text" value="<?=$detail_user->username?>" name="username" class="form-control form-control-line">
                                                <input type="hidden" value="<?=$detail_user->username?>" name="old_username" class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Password</label>
                                            <div class="col-md-12">
                                                <input name="password" type="password" placeholder="Masukan Password Baru" class="form-control form-control-line">
                                                <small>*Kosongkan jika tidak mengubah Password Baru</small>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Ulangi Password</label>
                                            <div class="col-md-12">
                                                <input name="conf_password" type="password" placeholder="Masukan Kembali Password" class="form-control form-control-line">
                                            </div>
                                        </div>
                                    </form>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="button" id="btnSetting" onclick="save_setting(<?=$detail_user->user_id?>)" class="btn btn-success">Ubah Password</button>
                                            </div>
                                        </div>
                                    <hr/>
                                </div>
                            <?php } ?>
</div>



</div>

















								

								

								

								


								



								





							




								



							
							

						

<!-- sample modal content -->
<div id="modalRegister" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Register User</h4> </div>
				<div class="modal-body">	
					<div class="row">
						<div class="col-md-12">
							<form id="formRegister" method="post">
								<div id="message"></div>
								<input type="hidden" name="id_pegawai" value="<?php echo $this->uri->segment(3) ?>">
								<div class="form-group">
									<label class="control-label">Username</label>
									<input type="text" name="username" class="form-control" placeholder="Masukkan Username">
								</div>
								<div class="form-group">
									<label class="control-label">Password</label>
									<input type="password" name="password" class="form-control" placeholder="Masukkan Password">
								</div>
								<div class="form-group">
									<label class="control-label">Konfirmasi Password</label>
									<input type="password" name="conf_password" class="form-control" placeholder="Ulangi Password">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
					<button type="button" id="btnSave" onclick="saveUser()" class="btn btn-success waves-effect" data-dismiss="modal">Submit</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<script>
		var arrGol = new Array;
		var id_pegawai_upload ="";
		var id_riwayat_upload ="";
		var jenis_riwayat_upload="";
		<?php
		foreach ($golongan as $row ){
			?>
			arrGol[<?=$row->id_golongan;?>]= new Array("<?=$row->pangkat;?>","<?= $row->golongan;?>");

			<?php
		}
		?>

		function uploadBerkas() {
			console.log("submit event");
			$("#loader").show();
			var fd = new FormData(document.getElementById("fileinfo"));
            //fd.append("label", "WEBUPLOAD");
            fd.append("id_pegawai_upload", id_pegawai_upload);
            fd.append("id_riwayat_upload", id_riwayat_upload);
            fd.append("jenis_riwayat_upload", jenis_riwayat_upload);
            $.ajax({
            	url: "<?= base_url();?>master_pegawai/upload_berkas",
            	type: "POST",
            	data: fd,
              processData: false,  // tell jQuery not to process the data
              contentType: false   // tell jQuery not to set contentType
        }).done(function( data ) {
           console.log("PHP Output:");
           console.log( data );
           $("#loader").hide();
           alert(data);
           if (data=="Upload berhasil"){
            resetForm("upload");
            update_data(jenis_riwayat_upload);
      }
});
        return false;
  }

  function showUploadForm(id_pegawai,id_riwayat,jenis_riwayat){
       id_pegawai_upload = id_pegawai;
       id_riwayat_upload = id_riwayat;
       jenis_riwayat_upload = jenis_riwayat;
       resetForm("upload");

 }
 function resetForm(form)
 {
       if (form=="pangkat"){
        $('#id_golongan').val('');
        $('#tmt_berlaku').val('');
        $('#gaji_pokok').val('');
        $('#nama_pejabat').val('');
        $('#no_sk').val('');
        $('#tgl_sk').val('');
        $("#txt_golongan").val('');
  }
  else if (form=="jabatan"){
        $('#jab_level1').val('');
        $('#id_golongan_jb').val('');
        $('#tgl_mulai_jb').val('');
        $('#tgl_akhir_jb').val('');
        $('#gaji_pokok_jb').val('');
        $('#nama_pejabat_jb').val('');
        $('#no_sk_jb').val('');
        $('#tgl_sk_jb').val('');
        $("#jenis_jabatan").val('');
        $("#id_eselon").val('');
        $("#id_eselon").attr("disabled",true);
        reset_jab(2);
  }
  else if (form=="pendidikan"){
        $('#id_jenjangpendidikan').val('');
        $('#id_tempatpendidikan').val('');
        $('#id_jurusan').val('');
        $('#nama_pejabat_pend').val('');
        $('#no_sk_pend').val('');
        $('#tgl_sk_pend').val('');
  }
  else if (form=="diklat"){
        $('#id_jenisdiklat').val('');
        $('#nama_diklat').val('');
        $('#tempat_diklat').val('');
        $('#penyelenggara_diklat').val('');
        $('#angkatan_diklat').val('');
        $('#tgl_mulai_diklat').val('');
        $('#tgl_akhir_diklat').val('');
        $('#no_sptl').val('');
        $('#tgl_sptl').val('');
  }
  else if (form=="unit_kerja"){
        $('#skpd_level1').val('');
        $('#tmt_awal').val('');
        $('#tmt_akhir').val('');
        $('#no_sk_awal').val('');
        $('#no_sk_akhir').val('');
        reset(2);
  }
  else if (form=="penghargaan"){
        $('#id_jenispenghargaan').val('');
        $('#nama_penghargaan').val('');
        $('#tahun').val('');
        $('#asal_perolehan').val('');
        $('#penandatangan').val('');
        $('#no_penghargaan').val('');
        $('#tgl_penghargaan').val('');
  }
  else if (form=="cuti"){
        $('#id_jeniscuti').val('');
        $('#keterangan').val('');
        $('#pejabat_penetapan').val('');
        $('#no_sk_cuti').val('');
        $('#tgl_sk_cuti').val('');
        $('#tgl_awal_cuti').val('');
        $('#tgl_akhir_cuti').val('');
  }
  else if (form=="upload"){
        $('#file').val('');
  }
}
function tambahPangkat()
{
 var id_golongan = $('#id_golongan').val();
 var tmt_berlaku = $('#tmt_berlaku').val();
 var gaji_pokok = $('#gaji_pokok').val();
 var nama_pejabat = $('#nama_pejabat').val();
 var no_sk = $('#no_sk').val();
 var tgl_sk = $('#tgl_sk').val();
 if (id_golongan!="" && tmt_berlaku!="" && gaji_pokok!="" && nama_pejabat!="" && no_sk!="" && tgl_sk!=""){
  $.post("<?= base_url();?>master_pegawai/tambah_riwayat_pangkat",
  {
   'id_golongan'	: id_golongan,
   'tmt_berlaku'	: tmt_berlaku,
   'gaji_pokok'	: gaji_pokok,
   'nama_pejabat'	: nama_pejabat,
   'no_sk'			: no_sk,
   'tgl_sk'		: tgl_sk,
   'id_pegawai'	: '<?= $id_pegawai;?>'
},
function(response){
   if (response=="success"){
    alert('Tambah riwayat kepangkatan berhasil.');
    resetForm('pangkat');
						// TO DO : update table
						update_data('pangkat');
					}
				}
				);
}
else{
  alert('data belum lengkap');
}

}

function tambahJabatan()
{

 var id_jabatan=0;
 var ketemu=false;
 for(var i=7;i>=1;i--)
 {
  var jab_level="jab_level"+i;
  if ($("#"+jab_level).val() !="" && !ketemu){
   id_jabatan=$("#"+jab_level).val();
   ketemu=true;
}
}


var id_golongan = $('#id_golongan_jb').val();
var tgl_mulai = $('#tgl_mulai_jb').val();
var tgl_akhir = $('#tgl_akhir_jb').val();
var gaji_pokok = $('#gaji_pokok_jb').val();
var nama_pejabat = $('#nama_pejabat_jb').val();
var no_sk = $('#no_sk_jb').val();
var tgl_sk = $('#tgl_sk_jb').val();
var nuptk = $('#nuptk').val();

var jabatan_terakhir = $("jabatan_terakhir").val();
if (id_jabatan!=0 && id_golongan!="" && tgl_mulai!="" && gaji_pokok!="" && nama_pejabat!="" && no_sk!="" && tgl_sk!=""){
  if (tgl_akhir=="" || jabatan_terakhir==1){
   tgl_akhir = "9999-12-31";
}
var id_eselon = 0;
if ($("#jenis_jabatan").val()==1) id_eselon = $("#id_eselon").val();
$.post("<?= base_url();?>master_pegawai/tambah_riwayat_jabatan",
{
   'id_jabatan'	: id_jabatan,
   'id_golongan'	: id_golongan,
   'tgl_mulai'		: tgl_mulai,
   'tgl_akhir'		: tgl_akhir,
   'gaji_pokok'	: gaji_pokok,
   'nama_pejabat'	: nama_pejabat,
   'no_sk'			: no_sk,
   'tgl_sk'		: tgl_sk,
   'nuptk'			: nuptk,
   'id_eselon'		: id_eselon,
   'id_pegawai'	: '<?= $id_pegawai;?>'
},
function(response){
   if (response=="success"){
    alert('Tambah riwayat jabatan berhasil.');
    resetForm('jabatan');
						// TO DO : update table
						update_data('jabatan');
					}
				}
				);
}
else{
  alert('data belum lengkap');
}

}
function tambahDiklat()
{
 var id_jenisdiklat = $('#id_jenisdiklat').val();
 var nama_diklat = $('#nama_diklat').val();
 var tempat = $('#tempat_diklat').val();
 var penyelenggara	 = $('#penyelenggara_diklat').val();
 var angkatan = $('#angkatan_diklat').val();
 var tgl_mulai = $('#tgl_mulai_diklat').val();
 var tgl_akhir = $('#tgl_akhir_diklat').val();
 var no_sptl = $('#no_sptl').val();
 var tgl_sptl = $('#tgl_sptl').val();

 if (id_jenisdiklat!="" && nama_diklat!="" && tempat!="" && tgl_mulai!="" && tgl_akhir!="" && tgl_sptl!=""){
  $.post("<?= base_url();?>master_pegawai/tambah_riwayat_diklat",
  {
   'id_jenisdiklat'	: id_jenisdiklat,
   'nama_diklat'	: nama_diklat,
   'tempat'		: tempat,
   'penyelenggara'		: penyelenggara,
   'angkatan'	: angkatan,
   'tgl_mulai'	: tgl_mulai,
   'tgl_akhir'			: tgl_akhir,
   'no_sptl'		: no_sptl,
   'tgl_sptl'		: tgl_sptl,
   'id_pegawai'	: '<?= $id_pegawai;?>'
},
function(response){
					//console.log(response);
					if (response=="success"){
						alert('Tambah riwayat diklat berhasil.');
						resetForm('diklat');
						// TO DO : update table
						update_data('diklat');
					}
				}
				);
}
else{
  alert('data belum lengkap');
}

}
function tambahPendidikan()
{
 var id_jenjangpendidikan = $('#id_jenjangpendidikan').val();
 var id_tempatpendidikan = $('#id_tempatpendidikan').val();
 var id_jurusan = $('#id_jurusan').val();
 var nama_pejabat = $('#nama_pejabat_pend').val();
 var no_sk = $('#no_sk_pend').val();
 var tgl_sk = $('#tgl_sk_pend').val();

 if (id_jenjangpendidikan!="" && id_tempatpendidikan!="" && nama_pejabat && no_sk!="" && tgl_sk!=""){
  $.post("<?= base_url();?>master_pegawai/tambah_riwayat_pendidikan",
  {
   'id_jenjangpendidikan'	: id_jenjangpendidikan,
   'id_tempatpendidikan'	: id_tempatpendidikan,
   'id_jurusan'		: id_jurusan,
   'nama_pejabat'	: nama_pejabat,
   'no_sk'			: no_sk,
   'tgl_sk'		: tgl_sk,
   'id_pegawai'	: '<?= $id_pegawai;?>'
},
function(response){
					//console.log(response);
					if (response=="success"){
						
						alert('Tambah riwayat pendidikan berhasil.');
						resetForm('pendidikan');
						// TO DO : update table
						update_data('pendidikan');
					}
				}
				);
}
else{
  alert('data belum lengkap');
}

}
function tambahUnitKerja()
{
 var kd_skpd=0;
 var ketemu=false;
 for(var i=7;i>=1;i--)
 {
  var skpd_level="skpd_level"+i;
  if ($("#"+skpd_level).val() !="" && !ketemu){
   kd_skpd=$("#"+skpd_level).val();
   ketemu=true;
}
}
		//console.log("kd_skpd="+kd_skpd);
		
		var id_unit_kerja = kd_skpd;
		var tmt_awal = $('#tmt_awal').val();
		var tmt_akhir = $('#tmt_akhir').val();
		var no_sk_awal	 = $('#no_sk_awal').val();
		var no_sk_akhir = $('#no_sk_akhir').val();
		//console.log("id_unit_kerja:"+id_unit_kerja+", tmt_awal:"+tmt_awal+", tmt_akhir:"+tmt_akhir+", no_sk_awal:"+no_sk_awal+", no_sk_akhir:"+no_sk_akhir);
		if (id_unit_kerja!=0 && tmt_awal!="" && tmt_akhir!="" && no_sk_awal!="" && no_sk_akhir!="" ){
			$.post("<?= base_url();?>master_pegawai/tambah_riwayat_unit_kerja",
			{
				'id_unit_kerja'	: id_unit_kerja,
				'tmt_awal'	: tmt_awal,
				'tmt_akhir'		: tmt_akhir,
				'no_sk_awal'		: no_sk_awal,
				'no_sk_akhir'	: no_sk_akhir,
				'id_pegawai'	: '<?= $id_pegawai;?>'
			},
			function(response){
					//console.log(response);
					if (response=="success"){
						alert('Tambah riwayat unit kerja berhasil.');
						resetForm('unit_kerja');
						// TO DO : update table
						update_data('unit_kerja');
					}
				}
				);
		}
		else{
			alert('data belum lengkap');
		}
		
	}
	function tambahPenghargaan()
	{
		var id_jenispenghargaan = $('#id_jenispenghargaan').val();
		var nama_penghargaan = $('#nama_penghargaan').val();
		var tahun = $('#tahun').val();
		var asal_perolehan	 = $('#asal_perolehan').val();
		var penandatangan = $('#penandatangan').val();
		var no_penghargaan = $('#no_penghargaan').val();
		var tgl_penghargaan = $('#tgl_penghargaan').val();
		
		if (id_jenispenghargaan!="" && nama_penghargaan!="" && tahun!="" && asal_perolehan!="" && penandatangan!="" && no_penghargaan!="" && tgl_penghargaan!=""){
			$.post("<?= base_url();?>master_pegawai/tambah_riwayat_penghargaan",
			{
				'id_jenispenghargaan'	: id_jenispenghargaan,
				'nama_penghargaan'	: nama_penghargaan,
				'tahun'		: tahun,
				'asal_perolehan'		: asal_perolehan,
				'penandatangan'	: penandatangan,
				'no_penghargaan'	: no_penghargaan,
				'tgl_penghargaan'	: tgl_penghargaan,
				'id_pegawai'	: '<?= $id_pegawai;?>'
			},
			function(response){
					//console.log(response);
					if (response=="success"){
						alert('Tambah riwayat penghargaan berhasil.');
						resetForm('penghargaan');
						// TO DO : update table
						update_data('penghargaan');
					}
				}
				);
		}
		else{
			alert('data belum lengkap');
		}
		
	}
	function tambahCuti()
	{
		var id_jeniscuti = $('#id_jeniscuti').val();
		var keterangan= $('#keterangan').val();
		var pejabat_penetapan = $('#pejabat_penetapan').val();
		var no_sk	 = $('#no_sk_cuti').val();
		var tgl_sk = $('#tgl_sk_cuti').val();
		var tgl_awal_cuti = $('#tgl_awal_cuti').val();
		var tgl_akhir_cuti = $('#tgl_akhir_cuti').val();
		
		if (id_jeniscuti!="" && keterangan!="" && pejabat_penetapan!="" && no_sk!="" && tgl_sk!="" && tgl_awal_cuti!="" && tgl_akhir_cuti!=""){
			$.post("<?= base_url();?>master_pegawai/tambah_riwayat_cuti",
			{
				'id_jeniscuti'	: id_jeniscuti,
				'keterangan'	: keterangan,
				'pejabat_penetapan'		: pejabat_penetapan,
				'no_sk'		: no_sk,
				'tgl_sk'	: tgl_sk,
				'tgl_awal_cuti'	: tgl_awal_cuti,
				'tgl_akhir_cuti'	: tgl_akhir_cuti,
				'id_pegawai'	: '<?= $id_pegawai;?>'
			},
			function(response){
					//console.log(response);
					if (response=="success"){
						alert('Tambah riwayat penghargaan cuti.');
						resetForm('cuti');
						// TO DO : update table
						update_data('cuti');
					}
				}
				);
		}
		else{
			alert('data belum lengkap');
		}
		
	}
	function verifikasi(){
		$.post("<?= base_url();?>master_pegawai/verifikasi",{'id_pegawai':'<?= $id_pegawai;?>'},
			function(response){
				if (response=="success"){
					alert('Verifikasi sukses');
					$('#label_status').html('Status: aktif');
					$('#btn-status').attr('onclick','nonaktif()');
					// $('#btn-status').attr('class','btn btn-danger');
					$('#btn-status').html('Non Aktifkan');
				}
				else{
					alert('Error');
				}
			});
		
	}
	function proses(){
		$.post("<?= base_url();?>master_pegawai/proses",{'id_pegawai':'<?= $id_pegawai;?>'},
			function(response){
				if (response=="success"){
					alert('Sedang diproses');
					$('#label_status').html('Status: proses verifikasi');
					$('#btn-status').attr('onclick','verifikasi()');
					// $('#btn-status').attr('class','btn btn-primary');
					$('#btn-status').html('Verifikasi');
				}
				else{
					alert('Error');
				}
			});
		
	}
	function aktif(){
		$.post("<?= base_url();?>master_pegawai/aktif",{'id_pegawai':'<?= $id_pegawai;?>'},
			function(response){
				if (response=="success"){
					alert('Pegawai telah diaktifkan');
					$('#label_status').html('Status: aktif');
					$('#btn-status').attr('onclick','nonaktif()');
					// $('#btn-status').attr('class','btn btn-danger');
					$('#btn-status').html('Non Aktifkan');
				}
				else{
					alert('Error');
				}
			});
	}
	function nonaktif(){
		$.post("<?= base_url();?>master_pegawai/nonaktif",{'id_pegawai':'<?= $id_pegawai;?>'},
			function(response){
				if (response=="success"){
					alert('Pegawai telah dinonaktifkan');
					$('#label_status').html('Status: nonaktif');
					$('#btn-status').attr('onclick','aktif()');
					// $('#btn-status').attr('class','btn btn-primary');
					$('#btn-status').html('Aktifkan');
				}
				else{
					alert('Error');
				}
			});
	}
	function ubah_status(data,status,id){
		$.post("<?= base_url();?>master_pegawai/ubah_status",
		{
			'id_pegawai':'<?= $id_pegawai;?>',
			'data' : data,
			'status' : status,
			'id' :id
		},
		function(response){
			$("#data_"+data).html(response);
		});
	}
	function update_data(data){
		$.post("<?= base_url();?>master_pegawai/get_data_riwayat",
		{
			'id_pegawai':'<?= $id_pegawai;?>',
			'data' : data
		},
		function(response){
			$("#data_"+data).html(response);
		});
	}
	
	function hapus(data,id,berkas){
		var konfirmasi = confirm("Apakah anda yakin akan menghapus data?");
		if (konfirmasi){
			$.post("<?= base_url();?>master_pegawai/hapus_riwayat",
			{
				'id_pegawai':'<?= $id_pegawai;?>',
				'data' : data,
				'id' :id,
				'berkas' : berkas
			},
			function(response){
				$("#data_"+data).html(response);
			});
		}
	}
	
	
	function current_histoty(self,target)
	{
		var val = $("#"+self).val();
		if (val==0){
			$("#"+self).val(1);
			$("#"+target).attr('disabled',true);
		}
		else if (val==1){
			$("#"+self).val(0);
			$("#"+target).attr('disabled',false);
		}
	}
	
	function setGol()
	{
		
		var id_gol = $("#id_golongan").val();
		var golongan = arrGol[id_gol][1];
		$("#txt_golongan").val(golongan);
	}
	function getJabatan()
	{
		var jenis_jabatan = $("#jenis_jabatan").val();
		if (jenis_jabatan==1) {
			$("#id_eselon").attr("disabled",false);
		}
		else{
			$("#id_eselon").attr("disabled",true);
			$("#id_eselon").val("");
		}
		$.post("<?= base_url();?>ref_jabatan/get_jabatan",
		{
			'jenis_jabatan':jenis_jabatan
		},
		function(response){
			$("#jab_level1").html(response);
		});
		reset_jab(2);
		if (jenis_jabatan=="") $("#jab_level1").html("<option value=''>Pilih</option>");
	}
	
	function get_sub_induk(no)
	{
		var id_induk = $('#skpd_level'+no).val();
		no++;
		reset(no);
		if (id_induk!=""){
			$('#induk'+no).show();
			$.post("<?php echo base_url();?>ref_skpd/get_induk",{'id_induk':id_induk},function(obj){
				if (obj!="")
					$('#skpd_level'+no).html(obj);
				else 
					$('#induk'+no).hide();
			});
		}
	}
	function reset(no){
		while(no <= 7){
			$('#skpd_level'+no).val('');
			$('#induk'+no).hide();
			no++;
		}
	}
	
	//jabatan
	function get_sub_induk_jab(no)
	{
		var id_induk = $('#jab_level'+no).val();
		no++;
		reset_jab(no);
		if (id_induk!=""){
			$('#induk_jab'+no).show();
			$.post("<?php echo base_url();?>ref_jabatan/get_induk",{'id_induk':id_induk},function(obj){
				if (obj!="")
					$('#jab_level'+no).html(obj);
				else 
					$('#induk_jab'+no).hide();
			});
		}
		//console.log("id_induk="+id_induk);
	}
	function reset_jab(no){
		while(no <= 7){
			$('#jab_level'+no).val('');
			$('#induk_jab'+no).hide();
			no++;
		}
	}
	
	function get_sekolah()
	{
		var id_jenjangpendidikan = $("#id_jenjangpendidikan").val();
		$.post("<?= base_url();?>ref_pendidikan/get_sekolah",
		{
			'id_jenjangpendidikan':id_jenjangpendidikan
		},
		function(response){
			$("#id_tempatpendidikan").html(response);
			console.log(response);
		});
	}



	function showRegister(){
		$('#formRegister')[0].reset(); 
		$('#message').html(''); 
		$('#modalRegister').modal('show');
	}

	function deleteRegister(){
		swal({
			title: "Hapus User",
			text: "Apakah anda yakin akan menghapus data user ini?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Ya',
			cancelButtonText: "Tidak",
			closeOnConfirm: false
		},
		function(isConfirm){
			if (isConfirm){
				var id = '<?=$this->uri->segment(3)?>';
				$.ajax({
					url : "<?php echo base_url('master_pegawai/delete_user')?>/"+id,
					type: "POST",
					dataType: "JSON",
					success: function(data)
					{
						swal("Berhasil", "User Berhasil Dihapus!", "success");
						$('#btn-register').attr('onclick','showRegister()');
						$('#btn-register').html('Register User');
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						swal("Failed", "An error occured", "error");
					}
				});
			}
		});
	}

  function deletePegawai(){
    swal({
      title: "Hapus Pegawai",
      text: "Apakah anda yakin akan menghapus data pegawai ini?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Ya',
      cancelButtonText: "Tidak",
      closeOnConfirm: false
    },
    function(isConfirm){
      if (isConfirm){
        var id = '<?=$this->uri->segment(3)?>';
        $.ajax({
          url : "<?php echo base_url('master_pegawai/delete_pegawai')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
            swal("Berhasil", "Pegawai Berhasil Dihapus!", "success");
            window.location.replace("<?=base_url('master_pegawai')?>");
            // $('#btn-register').attr('onclick','showRegister()');
            // $('#btn-register').html('Register User');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            swal("Failed", "An error occured", "error");
          }
        });
      }
    });
  }

	function saveUser(){

		$('#btnSave').text('Menyimpan...'); 
		$('#message').html(''); 
		$('#btnSave').attr('disabled',true); 
		$.ajax({
			url : "<?php echo base_url('master_pegawai/register_user')?>",
			type: "POST",
			data: $('#formRegister').serialize(),
			dataType: "JSON",
			success: function(data)
			{

				if(data.status) 
				{
					$('#modalRegister').modal('hide');
					// alert('berhasil');

					swal("Berhasil", "Registrasi User berhasil!", "success");
					$('#btn-register').attr('onclick','deleteRegister()');
					$('#btn-register').html('Hapus User');
					// reload_table();
				}else{
					$('#message').html('<div class="alert alert-danger">'+data.message+'</div>'); 
				}
				$('#btnSave').text('Simpan'); 
				$('#btnSave').attr('disabled',false); 


			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
				$('#btnSave').text('Simpan'); 
				$('#btnSave').attr('disabled',false); 

			}
		});
	}

	function savePassword(){

		$('#btnSimpan').text('Menyimpan...'); 
		$('#btnSimpan').attr('disabled',true); 
		$.ajax({
			url : "<?php echo base_url('master_pegawai/change_user')?>",
			type: "POST",
			data: $('#formPassword').serialize(),
			dataType: "JSON",
			success: function(data)
			{

				if(data.status) 
				{
					swal("Berhasil", "Perubahan berhasil disimpan!", "success");
				}else{
					swal("Gagal", data.message , "error");
					// $('#message').html('<div class="alert alert-danger">'+data.message+'</div>'); 
				}
				$('#btnSimpan').text('Simpan'); 
				$('#btnSimpan').attr('disabled',false); 


			},
			error: function (jqXHR, textStatus, errorThrown)
			{
                       swal("Gagal",'Terjadi kesalahan' , "error");
                       $('#btnSimpan').text('Simpan'); 
                       $('#btnSimpan').attr('disabled',false); 

                 }
           });
	}

  function save_setting(employee_id){
        $.ajax({
        url:"<?php echo base_url('manage_user/change_password/"+employee_id+"');?>",
            type : "POST",
            data: $('#form-setting').serialize(),
            success:function(data){
                $("#message").html(data);
                $("#btnSetting").html('Update Profile');
            }
            ,beforeSend:function()
                {
                $("#message").html('');
                $("#btnSetting").html('<i class="fa fa-circle-o-notch fa-spin"></i> Please wait ...');
            }

        })

        return false;
    }
</script>		 