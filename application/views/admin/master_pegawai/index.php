<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Master Pegawai</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		

<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			<div class="row">
				<div class="col-md-3 b-r">
					<a href="<?php echo base_url(). "master_pegawai/add" ;?>">
					<button  class="btn btn-primary m-t-15 btn-block">Tambah Pegawai</button>
				</a>
				</div>
				<form method="POST">
					<?php if($user_level=='Administrator'){ ?>

					<div class="col-md-3">
						<div class="form-group">
							<label>Nama </label>
							<input type="text" class="form-control" placeholder="Nama lengkap" name='nama_lengkap'>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label for="exampleInputEmail1">Unit kerja</label>
							<select name="id_unit_kerja" class="form-control select2">
								<option value="">Pilih Unit Kerja</option>
								<?php 
								foreach($unit_kerja as $uk){
									echo'<option value="'.$uk->id_unit_kerja.'">'.$uk->nama_unit_kerja.'</option>';
								}
								?>
							</select>			
						</div>
					</div>
					<?php } ?>
					<div class="col-md-3">
						<div class="form-group">

							<br>
							<button type="submit" class="btn btn-primary m-t-5 btn-outline">Filter</button>
						</div>
					</div>

				</form>
			</div>

		</div>
	</div>

</div>


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">

		<div class="x_content">

			<?php 
			$no = $offset+1;
			foreach ($query as $row){ 
				?>

				<div class="col-md-4 col-sm-6">
					<div class="white-box">
						<div class="row">
							<div class="col-md-4 col-sm-4 text-center">

							<img src="<?=base_url()?>/data/user_picture/useravatar.png" alt="user" class="img-circle img-responsive">
							</div>
							<div class="col-md-8 col-sm-8">
								<br>
								<h3 class="box-title m-b-0"><?= $row->nama_lengkap;?></h3>
								<div style="height: 100px" class="well"><small><?= $row->nama_jabatan;?></small></div>
								<address>
									<a href="<?php echo base_url(). "master_pegawai/view/".$row->id_pegawai ;?>">
										<button class="fcbtn btn btn-primary btn-outline btn-1b btn-block">Detail Profil</button>
									</a>
								</address>
								<p></p>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<!-- /.col -->
			</div>




			<div class='row'>
				<div class='col-md-12 pager'>
					<?php


					$CI =& get_instance();
					$CI->load->library('pagination');

					$config['base_url'] = base_url(). 'master_pegawai/index/';
					$config['total_rows'] = $total_rows;
					$config['per_page'] = $per_page;
					$config['attributes'] = array('class' => 'btn btn-primary btn-xm marginleft2px');
					$config['page_query_string']=TRUE;
					$CI->pagination->initialize($config);
					$link = $CI->pagination->create_links();
					$link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled marginleft2px' >", $link);
					$link = str_replace("</strong>", "</button>", $link);
					echo $link;

					?>
				</div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	function delete_(id)
	{
		$('#confirm_title').html('Konfirmasi');
		$('#confirm_content').html('Apakah anda yakin akan menghapus data pegawai?');
		$('#confirm_btn').html('Hapus');
		$('#confirm_btn').attr('href',"<?php echo base_url();?>master_pegawai/delete/"+id);
	}
	
	function get_sub_induk(no)
	{
		var id_induk = $('#skpd_level'+no).val();
		no++;
		reset(no);
		if (id_induk!=""){
			$('#induk'+no).show();
			$.post("<?php echo base_url();?>ref_skpd/get_induk",{'id_induk':id_induk},function(obj){
				if (obj!="")
					$('#skpd_level'+no).html(obj);
				else 
					$('#induk'+no).hide();
			});
		}
		
	}
	function reset(no){
		while(no <= 7){
			$('#skpd_level'+no).val('');
			$('#induk'+no).hide();
			no++;
		}
	}
	
</script>