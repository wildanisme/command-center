<?php $this->load->view('admin/src/head'); ?>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2 style="color:black">Dashboard Potensi Desa (Podes)</h2>
          <h6 class="mb-0"><?=$skpd['skpd_detail'][0]['nama_skpd'] ?></h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url()?>"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Desa</li>
            <li class="breadcrumb-item active"><?=$skpd['skpd_detail'][0]['nama_skpd'] ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <!-- awal -->
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-primary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              <div class="media-body"><span class="m-0 text-white">Jumlah Desa</span>
                <h4 class="mb-0 "><?= count($skpd['desa']) ?></h4><i class="icon-bg" data-feather="database"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
              <div class="media-body"><span class="m-0">Jumlah Kelurahan</span>
                <h4 class="mb-0 "><?= count($skpd['kelurahan']) ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-warning o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="message-circle"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Jumlah RW</span>
                <h4 class="mb-0  text-white"><?= $sdgs['jml']['jml_rw']['jml_rw'] ?></h4><i class="icon-bg" data-feather="message-circle"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-info o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center">
                <div class="text-white i" data-feather="user-plus"></div>
              </div>
              <div class="media-body"><span class="m-0 text-white">Jumlah RT</span>
                <h4 class="mb-0  text-white"><?= $sdgs['jml']['jml_rt']['jml_rt'] ?></h4><i class="icon-bg" data-feather="user-plus"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- batas -->
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>Status Pemerintahan </h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="kelayakan_kantor_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card">
        <div class="card-header">
          <h5>Terdapat BPD / LMK</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="bmd_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
      <div class="card gradient-primary o-hidden">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"><i data-feather="database"></i></div>
            <div class="media-body"><span class="m-0 text-white">Jumlah Laki-Laki</span>
              <h4 class="mb-0 "><?= round($disduk->data->laki_laki) ?></h4><i class="icon-bg" data-feather="database"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card gradient-secondary o-hidden">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
            <div class="media-body"><span class="m-0">Jumlah Perempuan</span>
              <h4 class="mb-0 "><?= round($disduk->data->perempuan) ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card gradient-info o-hidden">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center text-white"><i data-feather="users"></i></div>
            <div class="media-body"><span class="m-0 text-white">Jumlah KK</span>
              <h4 class="mb-0 text-white"><?= round($sdgs['jml']['jml_keluarga']['jml_keluarga']) ?></h4><i class="icon-bg" data-feather="users"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card gradient-success o-hidden">
        <div class="b-r-4 card-body">
          <div class="media static-top-widget">
            <div class="align-self-center text-center text-white"><i data-feather="user"></i></div>
            <div class="media-body"><span class="m-0 text-white">Jumlah KK Petani</span>
              <h4 class="mb-0 text-white"><?= round($sdgs['jml']['jml_keluarga_pertanian']['jml_keluarga_pertanian']) ?></h4><i class="icon-bg" data-feather="user"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h5>Pengguna Listrik</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="pengguna_listrik_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h5>Sumber Listrik</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="sumber_listrik_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          <h5>Penerangan Jalan</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="penerangan_jalan_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Bahan Bakar Masak</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="bahan_bakar_masak_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Tempat Buang Sampah</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="tempat_buang_sampah_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Fasilitas Buang Air Besar</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="buang_air_besar_chart"></div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Pembuangan Akhir Tinja</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="pembuangan_akhir_tinja_chart"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Bencana Alam dan Mitigasi Bencana Alam 2020</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="b_a_d_m_b_a_prev_year"></div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h5>Bencana Alam dan Mitigasi Bencana Alam 2021</h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="b_a_d_m_b_a_this_year"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-8 xl-100 box-col-12">
      <div class="card">
        <div class="card-header no-border">
          <h5>Daftar Desa / Kelurahan</h5>
          <ul class="creative-dots">
            <li class="bg-primary big-dot"></li>
            <li class="bg-secondary semi-big-dot"></li>
            <li class="bg-warning medium-dot"></li>
            <li class="bg-info semi-medium-dot"></li>
            <li class="bg-secondary semi-small-dot"></li>
            <li class="bg-primary small-dot"></li>
          </ul>
          <br>
          <!-- <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                      <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                      <li><i class="view-html fa fa-code font-primary"></i></li>
                      <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                      <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                      <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                      <li><i class="icofont icofont-error close-card font-primary"></i></li>
                    </ul>
                  </div> -->
        </div>
        <div class="card-body pt-0">
          <div class="activity-table table-responsive recent-table">
            <table class="display table table-bordernone" id="basic-1">
            <thead>
              <tr>
                <th></th>
                <th>Desa</th>
                <th>Telp</th>
                <th>Email</th>
                <th>Data Podes</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($skpd['desa'] as $key => $v) { ?>
                <tr>
                  <td>
                    ...
                  </td>
                  <td>
                    <p ><?=strtoupper($v['nama_skpd'])?></p>
                  </td>
                  <td><?=$v['telepon_skpd']?></td>
                  <td>
                    <p><?=$v['email_skpd']?></p>
                  </td>
                  <td>
                      <?php if (!empty($v['simpatik'])) {
                        foreach ($v['simpatik'] as $key => $value) { ?>
                          <a href="<?=base_url('desa/detail/'.$value['id_simpatik_kuesioner_periode'])?>"><?=$value['tahun']?></a>
                        <?php }
                      } ?>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <?php $this->load->view('admin/src/bottom'); ?>
      <script src="<?= base_url(); ?>js/keuangan/dashboard.js" defer></script>