<?php $this->load->view('admin/src/head'); ?>
<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2 style="color:black">Dashboard Potensi Desa (Podes)</h2>
          <h6 class="mb-0"><?=$sdgs_detail['detail_skpd']['nama_skpd'] ?></h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=base_url()?>"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Desa</li>
            <li class="breadcrumb-item active"><?=$sdgs_detail['detail_skpd']['nama_skpd'] ?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="row">
            <div class="col-sm-12 col-lg-12 col-xl-12">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th rowspan="4" style="width:30%">
                      <center><img style="width:100%;" src="https://e-officedesa.sumedangkab.go.id/data/logo/skpd/sumedang.png" alt="user" /> </center>
                    </th>
                    <th colspan="2">
                      <div class="alert alert-primary"><?=$sdgs_detail['detail_skpd']['nama_skpd']?></div>
                    </th>
                  </tr>
                  <tr>
                    <th>Nama Kepala</th>
                    <td><?=$sdgs_detail['kepala_skpd']['nama_lengkap']?></td>
                  </tr>
                  <tr>
                    <th>Alamat SKPD</th>
                    <td><?=$sdgs_detail['detail_skpd']['alamat_skpd']?></td>
                  </tr>
                  <tr>
                    <th>Email / Telp</th>
                    <td><?=$sdgs_detail['detail_skpd']['email_skpd']?> / <?=$sdgs_detail['detail_skpd']['telepon_skpd']?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="card gradient-primary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="database"></i></div>
              <div class="media-body"><span class="m-0 text-white">Laki-Laki</span>
                <h4 class="mb-0 "><?= round($disduk->data->laki_laki) ?></h4><i class="icon-bg" data-feather="database"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
              <div class="media-body"><span class="m-0">Perempuan</span>
                <h4 class="mb-0 "><?= round($disduk->data->perempuan) ?></h4><i class="icon-bg" data-feather="shopping-bag"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card gradient-info o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center text-white"><i data-feather="users"></i></div>
              <div class="media-body"><span class="m-0 text-white">Kartu Keluarga</span>
                <h4 class="mb-0 text-white"><?=$sdgs_detail['pengisian_podes']['jml_keluarga'] ?></h4><i class="icon-bg" data-feather="users"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card gradient-success o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center text-white"><i data-feather="user"></i></div>
              <div class="media-body"><span class="m-0 text-white">KK Petani</span>
                <h4 class="mb-0 text-white"><?=$sdgs_detail['pengisian_podes']['jml_keluarga_pertanian'] ?></h4><i class="icon-bg" data-feather="user"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5>Pengguna Listrik</h5>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="pengguna_listrik_chart"></div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5>Sumber Listrik</h5>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="sumber_listrik_chart"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5>Bencana Alam dan Mitigasi Bencana Alam 2020</h5>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="b_a_d_m_b_a_prev_year"></div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card">
          <div class="card-header">
            <h5>Bencana Alam dan Mitigasi Bencana Alam 2021</h5>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="b_a_d_m_b_a_this_year"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-8 xl-100 box-col-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>Jenjang Pendidikan</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table text-left">
                <thead>
                  <tr class="border-bottom-primary">
                    <th rowspan="2">Jenis / Jenjang Pendidikan</th>
                    <th colspan="2">Jumlah Lembaga</th>
                    <th rowspan="2">Jarak (km)</th>
                    <th rowspan="2">Kemudahan untuk mencapai</th>
                  </tr>
                  <tr class="border-bottom-primary">
                    <th>Negeri</th>
                    <th>Swasta</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="border-bottom-primary">
                    <td>a. Pos Pendidikan Anak Usia Dini (POS Paud)</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_paud'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_paud'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_paud'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_paud'] ?></td>
                  </tr>
                  <tr class="border-bottom-primary">
                    <td>b. TK</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_tk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_tk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_tk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_tk'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>c. RA/BA</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_ra'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_ra'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_ra'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_ra'] ?></td>
                  </tr>
                  <tr class="border-bottom-primary">
                    <td>d. SD</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_sd'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_sd'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_sd'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_sd'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>e. MI</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_mi'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_mi'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_mi'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_mi'] ?></td>
                  </tr>
                  <tr class="border-bottom-primary">
                    <td>f. SMP</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_smp'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_smp'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_smp'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_smp'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>g. MTs</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_mts'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_mts'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_mts'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_mts'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>h. SMA</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_sma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_sma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_sma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_sma'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>i. MA</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_ma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_ma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_ma'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_ma'] ?></td>
                  </tr>

                  <tr class="border-bottom-primary">
                    <td>j. SMK</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_smk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_smk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_smk'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_smk'] ?></td>
                  </tr>
                  <tr class="border-bottom-primary">
                    <td>k. Akademi / Perguruan Tinggi</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_akademik'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_akademik'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_jarak_akademik'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_akademik'] ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-8 xl-100 box-col-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>Sarana Kesehatan</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <thead>
                  <tr>
                    <th rowspan="2">Sarana Kesehasatan</th>
                    <th rowspan="2">Jumlah</th>
                    <th rowspan="2">Jarak (km)</th>
                    <th rowspan="2">Kemudahan untuk mencapai</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>a. Rumah sakit</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_rs'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_rs'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_rs'] ?></td>
                  </tr>
                  <tr>
                    <td>b. Rumah Sakit Bersalin</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_rs_bersalin'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_rs_bersalin'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_rs_bersalin'] ?></td>
                  </tr>

                  <tr>
                    <td>c. Puskesmas dengan rawat inap</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_puskesmas_ri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_puskesmas_ri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_puskesmas_ri'] ?></td>
                  </tr>
                  <tr>
                    <td>d. Puskesmas tanpa rawat inap</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_puskesmas_tri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_puskesmas_tri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_puskesmas_tri'] ?></td>
                  </tr>

                  <tr>
                    <td>e. Puskesmas Pembantu</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_puskesmas_pembantu'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_puskesmas_pembantu'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_puskesmas_pembantu'] ?></td>
                  </tr>
                  <tr>
                    <td>f. Poliklinik / balai pengobatan</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_poliklinik'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_poliklinik'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_poliklinik'] ?></td>
                  </tr>

                  <tr>
                    <td>g. Tempat Praktik dokter</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_praktik_dokter'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_praktik_dokter'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_praktik_dokter'] ?></td>
                  </tr>

                  <tr>
                    <td>h. Rumah Bersalin</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_rs'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_rs'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_rs'] ?></td>
                  </tr>

                  <tr>
                    <td>i. Tempat Praktik Bidan</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_praktik_bidan'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_praktik_bidan'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_praktik_bidan'] ?></td>
                  </tr>

                  <tr>
                    <td>j. Poskesdes</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_poskesdes'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_poskesdes'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_poskesdes'] ?></td>
                  </tr>
                  <tr>
                    <td>k. Polindes</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_polindes'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_polindes'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_polindes'] ?></td>
                  </tr>

                  <tr>
                    <td>k. Apotek</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_apotek'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_apotek'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_apotek'] ?></td>
                  </tr>

                  <tr>
                    <td>m. Toko khusus obat</td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jumlah_jamu'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_jarak_jamu'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['sk_kemudahan_jamu'] ?></td>
                  </tr>

                  <tr>
                    <td>k. Posyandu menurut kegiatan</td>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>

                  <tr>
                    <td>1. Posyandu yang melakukan kegiatan penyuluhan/pendidikan</td>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>

                  <tr>
                    <td>2. Posyandu yang memberikan makanan/minuman tambahan</td>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>

                  <tr>
                    <td>3. Posyandu dengan kegiatan/pelayanan setiap sebulan sekali</td>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>

                  <tr>
                    <td>
                      4. Posyandu dengan kegiatan/pelayanan setiap 2 bulan sekali atau lebih
                    </td>
                    <td>1</td>
                    <td>-</td>
                    <td>-</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-8 xl-100 box-col-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>KLB / Penyakit</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <thead>
                  <tr>
                    <th rowspan="2">Jenis KLB/wabah penyakit</th>
                    <th rowspan="2">Kejadian</th>
                    <th rowspan="2">Jumlah Penderita</th>
                    <th rowspan="2">Korban Meninggal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>a. Muntaber/diare</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_muntaber'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_muntaber'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_muntaber'] ?></td>
                  </tr>

                  <tr>
                    <td>b. Demam Berdarah</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_dbd'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_dbd'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_dbd'] ?></td>
                  </tr>

                  <tr>
                    <td>c. Campak</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_campak'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_campak'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_campak'] ?></td>
                  </tr>

                  <tr>
                    <td>d. Malaria</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_malaria'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_malaria'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_malaria'] ?></td>
                  </tr>

                  <tr>
                    <td>e. Flu Burung / Sars</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_flu_burung'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_flu_burung'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_flu_burung'] ?></td>
                  </tr>


                  <tr>
                    <td>f. Hepatitis</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_hepatitis'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_hepatitis'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_hepatitis'] ?></td>
                  </tr>

                  <tr>
                    <td>g. Diferi</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_difteri'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_difteri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_difteri'] ?></td>
                  </tr>

                  <tr>
                    <td>h. Corona / Covid-19</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_corona'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_corona'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_corona'] ?></td>
                  </tr>

                  <tr>
                    <td>i. Lainya</td>
                    <td><span class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_lainnya'] ?></span></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderita_lainnya'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_lainnya'] ?></td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-8 xl-100 box-col-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>Olahraga dan Hiburan</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <thead>
                  <tr>
                    <th>Jenis Olahraga</th>
                    <th>Fasilitas / Lapangan olahraga</th>
                    <th>Kelompok Kegiatan</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>a. Sepak Bola</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></td>
                  </tr>
                  <tr>
                    <td>b. Bola voli</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bola_voli'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bola_voli'] ?></td>
                  </tr>
                  <tr>
                    <td>c. Bulu Tangkis</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bulutangkis'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bulutangkis'] ?></td>
                  </tr>

                  <tr>
                    <td>d. Bola Baskes</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bola_basket'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bola_basket'] ?></td>
                  </tr>

                  <tr>
                    <td>e. Tenis Lapangan</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_tenis_lapangan'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_tenis_lapangan'] ?></td>
                  </tr>

                  <tr>
                    <td>f. tenis meja</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_tenis_meja'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_tenis_meja'] ?></td>
                  </tr>
<!-- 
                  <tr>
                    <td>g. Futsal</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_futsal'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_futsal'] ?></td>
                  </tr> -->

                  <tr>
                    <td>h. Renang</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_renang'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_renang'] ?></td>
                  </tr>
                  <tr>
                    <td>i. Bela diri (silat, karate,dll)</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bela_diri'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bela_diri'] ?></td>
                  </tr>
                  <tr>
                    <td>j. Bilyard</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bilyard'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bilyard'] ?></td>
                  </tr>
                  <tr>
                    <td>k. Pusat Kebugaran</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_pusat_kebugaran'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_pusat_kebugaran'] ?></td>
                  </tr>
                  <tr>
                    <td>l. Lainya</td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_lainnya'] ?></td>
                    <td><?=$sdgs_detail['pengisian_podes']['ol_kelompok_lainnya'] ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5 class="text-uppercase">ANGKUTAN DAN KOMUNIKASI</h5>
            <div class="card-header-right">
              <ul class="list-unstyled card-option">
                <li>
                  <i class="icofont icofont-gear fa fa-spin font-primary"></i>
                </li>
                <li><i class="view-html fa fa-code font-primary"></i></li>
                <li>
                  <i class="icofont icofont-maximize full-card font-primary"></i>
                </li>
                <li>
                  <i class="icofont icofont-minus minimize-card font-primary"></i>
                </li>
                <li>
                  <i class="icofont icofont-refresh reload-card font-primary"></i>
                </li>
                <li>
                  <i class="icofont icofont-error close-card font-primary"></i>
                </li>
              </ul>
            </div>
          </div>
          <div class="card-body">
            <ul class="crm-activity">
              <li class="media">
                <span class="mr-3 font-primary">A</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">
                    Lalu lintas dari/ke desa/kelurahan melalui :.
                  </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['lalu_lintas_dari_ke_desa'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="media">
                <span class="mr-3 font-secondary">B</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">
                    Jenis permukaan jalan darat antar desa/kelurahan yang
                    terluas :
                  </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['jenis_permukaan_terluas'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="media">
                <span class="mr-3 font-primary">C</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">
                    Jalan darat antar desa/kelurahan dapat dilalui kendaraan
                    bermotor roda 4 atau lebih :
                  </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary"><?=$sdgs_detail['pengisian_podes']['jenis_permukaan_terluas'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="media">
                <span class="mr-3 font-secondary">D</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Keberadaan angkutan umum :</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['keberadaan_angkutan_umum'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">E</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Jam Operasional :</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['jam_opr_angkot_utama'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">F</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Jumlah Keluarga yang berlangganan internet :</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['keberadaan_internet'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">G</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Keberadaan warga yang menggunakan telepon seluler/handphone:</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['warga_menggunakan_telepon'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">H</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Keberadaan internet untuk warnet, game online, dan fasilitas lainnya di desa/kelurahan: </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['fasilitas_internet'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">H</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Jumlah menara telepon seluler atau Base Transceiver Station (BTS) : </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['jml_menara_bts'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">J</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Jumlah operator layanan komunikasi telepon seluler/handphone yang menjangkau di desa/kelurahan : </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['jml_operator_seluler'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">K</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Sinyal telepon seluler/handphone di sebagian besar wilayah desa/kelurahan : </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['sinyal_telepon_seluler'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">L</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Sinyal internet telepon seluler/handphone di sebagian besar wilayah di desa/kelurahan :</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['sinyal_internet_telepon'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">M</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">a. Komputer/PC/laptop yang masih berfungsi di kantor kepala desa/lurah:</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['komputer_pc_laptop_berfungsi'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">N</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Fasilitas internet di kantor kepala desa/lurah: </h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['fasilitas_internet'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">N</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Kantor pos/pos pembantu/rumah pos:</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['fungsi_kantor_pos'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>

              <li class="media">
                <span class="mr-3 font-secondary">O</span>
                <div class="align-self-center media-body">
                  <h6 class="mt-0">Perusahaan/agen jasa ekspedisi (pengiriman barang/dokumen) swasta:</h6>
                  <ul class="dates">
                    <li class="digits">
                      <label class="badge badge-primary">
                      <?=$sdgs_detail['pengisian_podes']['ekspedisi_swasta'] ?></label>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>Pemutakhiran Data Perkembangan Desa 2020</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table" class="table">
                <colgroup span="3" width="64"></colgroup>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">I. Keterangan Tempat</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">Kode</font>
                  </td>
                  <td>
                    <font face="Arial">Isian</font>
                  </td>
                  <td>
                    <font face="Arial">Jawaban</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">101</font>
                  </td>
                  <td>
                    <font face="Arial">Provinsi</font>
                  </td>
                  <td>
                    <font face="Arial">Jawa Barat</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">102</font>
                  </td>
                  <td>
                    <font face="Arial">Kabupaten</font>
                  </td>
                  <td>
                    <font face="Arial">Bogor</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">103</font>
                  </td>
                  <td>
                    <font face="Arial">Kecamatan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['detail_skpd']['nama_skpd_induk'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">104</font>
                  </td>
                  <td>
                    <font face="Arial">Desa / Kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['detail_skpd']['nama_skpd'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">105</font>
                  </td>
                  <td>
                    <font face="Arial">Status Daerah</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['status_daerah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">106</font>
                  </td>
                  <td>
                    <font face="Arial">SK pembentukan/pengesahan desa/kelurahan:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['sk_pembentukan'] ?> / <?=$sdgs_detail['pengisian_podes']['no_sk'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=4 " 
                    <font face=" Arial">107</font>
                  </td>
                  <td>
                    <font face="Arial">Status definitif desa dan operasional desa/kelurahan:</font>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A. Ada wilayah desa/kelurahan dengan batas yang jelas</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ada_batas_wilayah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Ada penduduk yang menetap di wilayah desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ada_penduduk'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">C. Ada pemerintah desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ada_pemerintah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=5 " 
                    <font face=" Arial">108</font>
                  </td>
                  <td>
                    <font face="Arial">Lokasi pelayanan pemerintah desa / kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A. Alamat lengkap</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['alamat_pelayanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Nomor telepon</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['telp_pelayanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">C. Alamat e-mail</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['email_pelayanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">Kode Pos</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['kodepos_pelayanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">III. Keterangan Umum Desa / Kelurahan</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">301</font>
                  </td>
                  <td>
                    <font face="Arial">Status Pemerintahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['status_pemerintahan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">302</font>
                  </td>
                  <td>
                    <font face="Arial">Badan Permusyawarahan Desa/Lembaga Musyawarah Kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['badan_permusyawaratan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">303</font>
                  </td>
                  <td>
                    <font face="Arial">Peta desa/kelurahan yang ditetapkan dalam peraturan Bupati/Walikota atau Gubernur</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['peta_desa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">IV. Kependudukan dan Kewilayahan</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=5 " 
                    <font face=" Arial">401</font>
                  </td>
                  <td>
                    <font face="Arial">Penduduk dan keluarga pada 1 Januari 2021</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A. Jumlah penduduk laki-laki</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_penduduk_laki'] ?> orang</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Jumlah penduduk perempuan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_penduduk_perempuan'] ?> orang</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">C. Jumlah Keluarga</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_keluarga'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">D. Jumlah keluarga pertanian (tanaman pangan, hortikultura, perkebunan, peternakan, perikanan, kehutanan)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_keluarga_pertanian'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">402</font>
                  </td>
                  <td>
                    <font face="Arial">Luas Wilayah Desa/Kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['luas_wilayah'] ?> km2 (1 Ha = 0,01 km2)</font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">V. Perumahan dan Lingkungan</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=4 " 
                    <font face=" Arial">501</font>
                  </td>
                  <td>
                    <font face="Arial">A. Jumlah keluarga penggunga listrik</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A1. PLN (Perusahaan Listrik Negara)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_listrik_pln'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A2. Non PLN (misalnya swasta, swadaya, atau perseorangan)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_listrik_nonpln'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Jumlah keluarga bukan pengguna listrik</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_non_listrik'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 " 
                    <font face=" Arial">502</font>
                  </td>
                  <td>
                    <font face="Arial">A. Penerangan di jalan utama desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['penerangan_utama'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Jika ada penerangan di jalan utama desa/kelurahan (R502 berkode 1 atau 2), jenis penerangan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jenis_penerangan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=8 " 
                    <font face=" Arial">503</font>
                  </td>
                  <td>
                    <font face="Arial">A. Bahan bakar untuk memasak yang digunakan oleh keluarga</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A1. Gas kota</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_gas_kota'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A2. LPG 3kg</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_lpghijau'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A3. LPG lebih dari 3kg</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_lpgbiru'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A4. Minyak tanah</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_minyaktanah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A5. Kayu bakar</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_kayubakar'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A6. Lainnya</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_lainnya'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Bahan bakar untuk memasak sebagian besar keluarga: (Pilih salah satu kode pada R503 yang dijawab &quot;YA&quot;)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bb_memasak_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=8 " 
                    <font face=" Arial">504</font>
                  </td>
                  <td>
                    <font face="Arial">A. Tempat buang sampah keluarga</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A1. Tempat sampah, kemudian diangkut</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ps_tempatsampah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A2. Dalam lubang atau dibakar</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ps_lubang'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A3. Sungai/ saluran irigasi/danau/laut</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ps_sungai'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A4. Drainase (got/selokan)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ps_drainase'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">A5. Lainnya</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ps_lainnya'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">B. Tempat buang sampah sebagian besar keluarga: (Pilih salah satu kode pada R504a yang dijawab &quot;YA&quot;) </font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['tempat_sampah_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">C. Tempat pembuangan sampah sementara (TPS)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['tempat_buang_sampah_sementara'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">505</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan bank sampah di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bank_sampah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">506</font>
                  </td>
                  <td>
                    <font face="Arial">Kegiatan pengolahan sampah: pengeolahan/daur ulang sampah/limbah (reuse, recycle) selama setahun terakhir</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['kegiatan_olah_sampah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 " 
                    <font face=" Arial">507</font>
                  </td>
                  <td>
                    <font face="Arial">a. Penggunaan fasilitas buang air besar sebagian besar keluarga di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['bab_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Tempat pembuangan akhir tinja sebagian besar keluarga:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['buang_tinja_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">508</font>
                  </td>
                  <td>
                    <font face="Arial">Sumber air untuk minum sebagian besar keluarga di desa/kelurahan berasal dari</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['air_minum_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">509</font>
                  </td>
                  <td>
                    <font face="Arial">Sumber air untuk mandi/cuci sebagian besar keluarga berasal dari</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['air_mandi_keluarga'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">510</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan sungai di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keberadaan_sungai'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">511</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah embung desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jmlembung'] ?> buah</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=5 " 
                    <font face=" Arial">512</font>
                  </td>
                  <td>
                    <font face="Arial">a. Keberadaan permukiman kumuh (sanitasi lingkungan buruk, bangunan padat, dan sebagian besar tidak layak huni)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keberadaan_kumuh'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Jika permukiman kumuh</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b1. Jumlah lokasi</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jmllokasi'] ?> lokasi</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b2. Jumlah bangunan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jmlbangunan'] ?> unit</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b3. Jumlah keluarga (isian tidak boleh lebih dari isian R401c)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jmlkeluarga'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">VI. Pendidikan dan Kesehatan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">601</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan sarana pendidikan menurut jenjang pendidikan di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">602</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan Taman Bacaan Masyarakat (TBM)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keberadaan_tbm'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">603</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan sarana kesehatan di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">604</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah warga penderita gizi buruk (marasmus dan kwashiorkor) pada tahun <?=$sdgs_detail['detail']['tahun'] ?></font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_gizi_buruk'] ?> orang</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">605</font>
                  </td>
                  <td>
                    <font face="Arial">Kejadian luar biasa (KLB) atau wabah penyakit selama setahun terakhir</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">VII. Olahraga dan Hiburan</font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">701</font>
                  </td>
                  <td>
                    <font face="Arial">Ketersediaan fasilitas/lapangan dan kelompok kegiatan olahraga di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">702</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan pub/diskotek/tempat karoke yang masih berfungsi</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ket_hiburan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">703</font>
                  </td>
                  <td>
                    <font face="Arial">Ruang publik terbuka yang peruntukan utamanya sebagai tempat bagi warga desa/kelurahan untuk bersantai/bermain tanpa perlu membayar (misalnya lapangan terbuka/alun-alun, taman, dll)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ruang_publik_terbuka'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">VIII. Angkutan dan Komunikasi</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=9 7" <font face="Arial">801</font>
                  </td>
                  <td>
                    <font face="Arial">Prasarana dan sarana transportasi antar desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Lalu lintas dari/ke desa/kelurahan melalui</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['lalu_lintas_dari_ke_desa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Jika lalu lintas dari/ke desa/kelurahan melalui darat atau darat air (R801 berkode 1 atau 3)</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b1. Jenis permukaan jalan darat antar desa/kelurahan yang terluas</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jenis_permukaan_terluas'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b2. Jalan darat antar desa/kelurahan dapat dilalui kendaraan bermotor roda 4 atau lebih</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jalan_darat_roda_4'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Angkutan umum yang melewati desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c1. Keberadaan angkutan umum</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keberadaan_angkutan_umum'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c2. Operasional angkutan umum yang utama</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['opr_angkot_utama'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c3. Jam operasional angkutan umum yang utama</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jam_opr_angkot_utama'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td <font face="Arial">802</font>
                  </td>
                  <td>
                    <font face="Arial">Sarana transportasi dari kantor kepala desa/lurah ke kantor camat/bupati/walikota</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 " 
                    <font face=" Arial">803</font>
                  </td>
                  <td>
                    <font face="Arial">a. Jumlah keluarga yang berlangganan telepon kabel</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jam_opr_angkot_utama'] ?> keluarga</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Keberadaan warga yang menggunakan telepon seluler/handphone</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jam_opr_angkot_utama'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " 
                    <font face=" Arial">804</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan internet untuk warnet, game online, dan fasilitas lainnya di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keberadaan_internet'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=5 " 
                    <font face=" Arial">805</font>
                  </td>
                  <td>
                    <font face="Arial">Keberadaan menara telepon seluler, sinyal telepon dan sinyal internet di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><br></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Jumlah menara telepon seluler atau Base Transceiver Station (BTS)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_menara_bts'] ?> buah</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Jumlah operator layanan komunikasi telepon seluler/handphone yang menjangkau di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_operator_seluler'] ?> jenis</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Sinyal telepon seluler/handphone di sebagian besar wilayah desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_operator_seluler'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">d. Sinyal internet telepon seluler/handphone di sebagian besar wilayah di desa/kelurahan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['sinyal_internet_telepon'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 " 
                    <font face=" Arial">806</font>
                  </td>
                  <td>
                    <font face="Arial">a. Komputer/PC/Laptop yang masih berfungsi di kantor kepala desa/lurah</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['komputer_pc_laptop_berfungsi'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Fasilitas internet di kantor kepada desa/lurah</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['fasilitas_internet'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=3 " 
                    <font face=" Arial">807</font>
                  </td>
                  <td>
                    <font face="Arial">a. Kantor pos/pos pembantu/rumah pos:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['fungsi_kantor_pos'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Layanan pos keliling</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['layanan_pos_keliling'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Perusahaan/agen jasa ekspedisi (pengiriman barang/dokumen) swasta</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['ekspedisi_swasta'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">IX. Ekonomi</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">Kode</font>
                  </td>
                  <td>
                    <font face="Arial">Isian</font>
                  </td>
                  <td>
                    <font face="Arial">Jawaban</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=9 1" <font face="Arial">901</font>
                  </td>
                  <td>
                    <font face="Arial">Industri mikro dan kecil (memiliki tenaga kerja kurang dari 20 pekerja) menurut bahan baku utama:</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Industri barang dari kulit (tas, sepatu, sandal, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_kulit'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Industri barang dari kayu (meja, kursi, lemari, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_kayu'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Industri barang dari logam mulia atau bahan logam (perabot dan perhiasan dari logam, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_logam'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">d. Industri barang dari kain/tenun (kerajinan tenun, konveksi, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_kain'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">e. Industri gerabah/keramik/batu (genteng, batu bata, porselin, tegel, keramik, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_gerabah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">f. Industri anyaman yang terbuat dari rotan/bambu, rumput, pandan, dll. (tikar, tas, hiasan dinding, dan produk lainnya).</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_rotan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">g. Industri makanan dan minuman (pengolahan dan pengawetan daging, ikan, buah&ndash; buahan, sayuran, minyak dan lemak, susu dan makanan dari susu, makanan dan minuman lain, dll.</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_makanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">h. Industri lainnya</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_lainnya'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=4 " >
                    <font face=" Arial">1002</font>
                  </td>
                  <td>
                    <font face="Arial">a.Kejadian perkelahian massal di desa/kelurahan selama setahun terakhir:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['kejadian_perkelahian'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Jika ada kejadian perkelahian massal, jumlah perkelahian massal yang terjadi</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jumlah_perkelahian_massal'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Keberadaan korban:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['perkelahian_korban_meninggal'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">d. Penyebab perkelahian: (Pilihan boleh lebih dari satu)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['perkelahian_korban_luka'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td " >
                    <font face=" Arial">1003</font>
                  </td>
                  <td>
                    <font face="Arial">Industri gerabah/keramik/batu (genteng, batu bata, porselin, tegel, keramik, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['imk_jumlah_gerabah'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">1004</font>
                  </td>
                  <td class="text-right" colspan="3">
                    <font face="Arial">Tindak kejahatan yang terjadi di desa/kelurahan selama setahun terakhir :</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=5 " >
                    <font face=" Arial">1005</font>
                  </td>
                  <td>
                    <font face="Arial">a. Kegiatan warga desa/kelurahan untuk menjaga keamanan lingkungan di desa/kelurahan selama setahun terakhir:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keamanan_pemeliharaan_poskamling'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Pembentukan/pengaturan regu keamanan:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keamanan_pemeliharaan_poskamling'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Penambahan Jumlah Anggota Hansip / Linmas</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keamanan_penambahan_hansip'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">d. Pelaporan tamu yang menginap lebih dari 24 jam ke aparat lingkungan</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keamanan_laporan_tamu'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">e. pengaktifan sistem keamanann lingkungan berasal dari inisiatif warga</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['keamanan_sistem_keamanan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=6 " >
                    <font face=" Arial">1102</font>
                  </td>
                  <td>
                    <font face="Arial">Fasilitas/upaya antisipasi/mitigasi bencana alam yang ada di desa/kelurahan:</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Sistem peringatan dini bencana alam</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['upaya_peringatan_dini_bencana'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Sistem peringatan dini khusus tsunami</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['upaya_peringatan_disni_tsunami'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Perlengkapan keselamatan (perahu karet, tenda, masker, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['upaya_perlengkapan_kelesamatan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">d. Rambu&ndash;rambu dan jalur evakuasi bencana</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['upaya_rambu_evakuasi'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">e. Pembuatan, perawatan, atau normalisasi: sungai, kanal, tanggul, parit, Ada &ndash; 1 Tidak ada &ndash; 2<br>drainase, waduk, pantai, dll.</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['upaya_perawatan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">XII. Asset Desa</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=4 " >
                    <font face=" Arial">1201</font>
                  </td>
                  <td>
                    <font face="Arial">Kepemilikan aset desa:</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Tanah kas desa/ulayat</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['aset_status_tanah_desa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Bangunan milik desa (balai desa, balai rakyat, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['aset_status_bangunan_desa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">c. Aset desa lainnya</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['aset_status_aset_lain'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">1202</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah unit usaha BUMDes:</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_bumdes'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">1203</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah pasar desa (pasar hewan, pelelangan ikan, pelelangan hasil pertanian, dll.)</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_pasar_desa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">1204</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah Tambatan Perahu</font>
                  </td>
                  <td>
                    <font face="Arial"><?=$sdgs_detail['pengisian_podes']['jml_perahu'] ?></font>
                  </td>
                </tr>
                <!-- <tr>
                  <td class="text-right" colspan="3">
                    <font face="Arial">XIII. Keterangan Aparatur Pemerintahan Desa / Kelurahan</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=3 " >
                    <font face=" Arial">1302</font>
                  </td>
                  <td>
                    <font face="Arial">Jumlah aparatur pemerintahan:</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">a. Sekretariat Desa/Kelurahan (kaur keuangan, kaur perencanaan, dll)</font>
                  </td>
                  <td>
                    <font face="Arial">{txt}</font>
                  </td>
                </tr>
                <tr>
                  <td>
                    <font face="Arial">b. Pelaksana Teknis (kasi pemerintahan, kasi kesejahteraan, dll)</font>
                  </td>
                  <td>
                    <font face="Arial">{txt}</font>
                  </td>
                </tr> -->
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>601</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="6" width="64"></colgroup>
                <tr>
                  <td rowspan=13 height="637" align="center" valign=middle sdval="603" sdnum="1033;"><b>
                      <font face="Arial" color="#000000">603</font>
                    </b></td>
                  <td rowspan=2 align="center" valign=middle><b>
                      <font face="Arial" color="#000000">Jenis / Jenjang Pendidikan</font>
                    </b></td>
                  <td colspan=2 align="center" valign=bottom><b>
                      <font face="Arial" color="#000000">Jumlah lembaga pendidikan</font>
                    </b></td>
                  <td colspan=2 align="left" valign=bottom><b>
                      <font face="Arial" color="#000000">Jika tidak ada lembaga pendidikan di desa/kelurahan (kolom (2) dan kolom (3) terisi (0)), jarak dan kemudahan untuk mencapai sarana pendidikan terdekat</font>
                    </b></td>
                </tr>
                <tr>
                  <td ><b>
                      <font face="Arial" color="#000000">Negeri</font>
                    </b></td>
                  <td ><b>
                      <font face="Arial" color="#000000">Swasta</font>
                    </b></td>
                  <td ><b>
                      <font face="Arial" color="#000000">Jarak (km)</font>
                    </b></td>
                  <td ><b>
                      <font face="Arial" color="#000000">Kemudahan untuk mencapai (kode)</font>
                    </b></td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Pos Pendidikan Anak usia Dini (Pos PAUD)</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_paud'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_paud'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_paud'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_paud'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. TK</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_tk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_tk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_tk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_tk'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">c. RA/BA</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_ra'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_ra'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_ra'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_ra'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">d. SD</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_sd'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_sd'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_sd'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_sd'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">e. MI</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_mi'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_mi'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_mi'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_mi'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">f. SMP</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_smp'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_smp'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_smp'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_smp'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">g. MTs</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_mts'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_mts'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_mts'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_mts'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">h. SMA</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_sma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_sma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_sma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_sma'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">i. MA</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_ma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_ma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_ma'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_ma'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">j. SMK</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_smk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_smk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_smk'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_smk'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">k. Akademi/perguruan tinggi</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_negeri_akademik'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jml_swasta_akademik'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_jarak_akademik'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['sp_kemudahan_akademik'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>605</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="5" width="64"></colgroup>
                <tr>
                  <td rowspan=11 height="823" align="center" valign=middle sdval="605" sdnum="1033;">
                    <font face="Arial" color="#000000">605</font>
                  </td>
                  <td rowspan=2 align="left" valign=middle>
                    <font face="Arial" color="#000000">Jenis KLB/ wabah penyakit (KLB: timbulnya atau meningkatnya kejadian kesakitan atau kematian yang bermakna secara epidemiologis pada suatu daerah dalam kurun waktu tertentu, ditetapkan oleh pemerintah)</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Kejadian</font>
                  </td>
                  <td colspan=2 align="left" valign=bottom>
                    <font face="Arial" color="#000000">Jika ada KLB atau wabah (kolom (2) berkode 1)</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">Jumlah penderita</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">Jumlah penderita yang meninggal</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Muntaber/ diare</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_muntaber'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_muntaber'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_muntaber'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Demam berdarah</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_dbd'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_dbd'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_dbd'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">c. Campak</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_campak'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_campak'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_campak'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">d. Malaria</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_malaria'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_malaria'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_malaria'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">e. Flu burung</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_flu_burung'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_flu_burung'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_flu_burung'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">f. Hepatitis E</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_hepatitis'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_hepatitis'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_hepatitis'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">g. Difteri</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_difteri'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_difteri'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_difteri'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">h. Corona/COVID-19</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_corona'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_corona'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_corona'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">i. lainnya</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_kejadian_lainnya'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderita_lainnya'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['klb_penderia_meninggal_lainnya'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>701</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="4" width="64"></colgroup>
                <tr>
                  <td rowspan=13 height="593" align="center" valign=middle sdval="701" sdnum="1033;">
                    <font face="Arial" color="#000000">701</font>
                  </td>
                  <td  align="left" valign=bottom>
                    <font face="Arial" color="#000000">Jenis Olahraga</font>
                  </td>
                  <td  align="left" valign=bottom>
                    <font face="Arial" color="#000000">Fasilitas/lapangan olahraga</font>
                  </td>
                  <td  align="left" valign=bottom>
                    <font face="Arial" color="#000000">Kelompok kegiatan</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Sepak bola</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Bola voli</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bola_voli'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bola_voli'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">c. Bulu tangkis</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bulutangkis'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bulutangkis'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">d. Bola basket</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bola_basket'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bola_basket'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">e. Tenis lapangan</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_tenis_lapangan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_tenis_lapangan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">f. Tenis meja</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_tenis_meja'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_tenis_meja'] ?></font>
                  </td>
                </tr>
                <!-- <tr>
                  <td >
                    <font face="Arial" color="#000000">g. Futsal</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></font>
                  </td>
                </tr> -->
                <tr>
                  <td >
                    <font face="Arial" color="#000000">h. Renang</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_renang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_renang'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">i. Bela diri (pencak silat, karate, dll)</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_bela_diri'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_bela_diri'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">j. Bilyard</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">k. Pusat kebugaran (senam, fitnes, aerobik, dll)</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">i. Lainnya</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_fasilitas_sepak_bola'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ol_kelompok_sepak_bola'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>802</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="8" width="64"></colgroup>
                <tr>
                  <td rowspan=4 height="327" align="center" valign=middle sdval="802" sdnum="1033;">
                    <font face="Arial" color="#000000">802</font>
                  </td>
                  <td rowspan=2 align="left" valign=middle>
                    <font face="Arial" color="#000000">Sarana transportasi yang biasa digunakan oleh sebagian besar penduduk dari kantor kepala desa/lurah ke</font>
                  </td>
                  <td rowspan=2 align="left" valign=middle>
                    <font face="Arial" color="#000000">Sarana transportasi yang biasa digunakan</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Jenis angkutan umum [kode]</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Angkutan umum yang utama [kode]</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Jarak tempuh (km)</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Waktu tempuh (jam: menit)</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Biaya Transportasi (000 Rupiah)</font>
                  </td>
                </tr>
                <tr>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Kantor Camat</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_transportasi_camat'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_jenis_angkutan_camat'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_angkutan_utama_camat'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_jarak_camat'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_waktu_camat'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_biaya_camat'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Kantor bupati/walikota</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_transportasi_bupati'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_jenis_angkutan_bupati'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_angkutan_utama_bupati'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_jarak_bupati'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_waktu_bupati'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['st_biaya_bupati'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>1004</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="4" width="64"></colgroup>
                <tr>
                  <td height="51" align="center" valign=bottom sdval="1004" sdnum="1033;">
                    <font face="Arial" color="#000000">1004</font>
                  </td>
                  <td colspan=3 align="left" valign=bottom>
                    <font face="Arial" color="#000000">Tindak kejahatan yang terjadi di desa/kelurahan selama setahun terakhir :</font>
                  </td>
                </tr>
                <tr>
                  <td  height="120" align="center" valign=middle>
                    <font face="Arial" color="#000000">kode</font>
                  </td>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">Jenis Tindak Kejahatan</font>
                  </td>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">Kejadian</font>
                  </td>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">kecenderungan tindak kejahatan dibanding setahun yang lalu</font>
                  </td>
                </tr>
                <tr>
                  <td  height="104" align="center" valign=bottom sdval="1" sdnum="1033;">
                    <font face="Arial" color="#000000">1</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">Perkosaan/kejahatan terhadap kesusilaan</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kejadian_kesusilaan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kecenderungan_kesusilaan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td  height="70" align="center" valign=bottom sdval="2" sdnum="1033;">
                    <font face="Arial" color="#000000">2</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">Penyalahgunaan/peredaran narkoba</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kejadian_narkoba'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kecenderungan_narkoba'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td  height="70" align="center" valign=bottom sdval="3" sdnum="1033;">
                    <font face="Arial" color="#000000">3</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">Perdagangan orang (trafficking)</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kejadian_trafficking'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kecenderungan_trafficking'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td  height="21" align="center" valign=bottom sdval="4" sdnum="1033;">
                    <font face="Arial" color="#000000">4</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">Korupsi</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kejadian_korupsi'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['tk_kecenderungan_korupsi'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>1006</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="5" width="64"></colgroup>
                <tr>
                  <td rowspan=7 height="288" align="center" valign=middle sdval="1006" sdnum="1033;">
                    <font face="Arial" color="#000000">1006</font>
                  </td>
                  <td colspan=4 align="left" valign=bottom><b>
                      <font face="Arial" color="#000000">Jumlah tempat ibadah di desa/kelurahan:</font>
                    </b></td>
                </tr>
                <tr>
                  <td  align="center" valign=bottom><b>
                      <font face="Arial" color="#000000">Jenis Tempat Ibadah</font>
                    </b></td>
                  <td  align="center" valign=bottom><b>
                      <font face="Arial" color="#000000">Jumlah</font>
                    </b></td>
                  <td  align="center" valign=bottom><b>
                      <font face="Arial" color="#000000">Jenis Tempat ibadah</font>
                    </b></td>
                  <td  align="center" valign=bottom><b>
                      <font face="Arial" color="#000000">Jumlah</font>
                    </b></td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Masjid</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_masjid'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">f. Pura</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_pura'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Surau/Langgar/Musala</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_surau'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">g. Wihara</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_wihara'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">c. Gereja Kristen</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_gereja_kristen'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">h. Kelenteng</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_kelenteng'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">d. Gereja Katolik</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_gereja_katolik'] ?></font>
                  </td>
                  <td rowspan=2 align="left" valign=middle>
                    <font face="Arial" color="#000000">i. Lainnya</font>
                  </td>
                  <td rowspan=2 align="left" valign=middle>
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_lainnya'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">e. Kapel</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['ti_jumlah_kapel'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>1101</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="7" width="64"></colgroup>
                <tr>
                  <td rowspan=13 height="623" align="center" valign=middle sdval="1101" sdnum="1033;">
                    <font face="Arial" color="#000000">1101</font>
                  </td>
                  <td colspan=6 align="left" valign=bottom>
                    <font face="Arial" color="#000000">Kejadian/bencana alam (mengganggu kehidupan dan menyebabkan kerugian bagi masyarakat) yang terjadi:</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Kejadian/bencana alam</font>
                  </td>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Kejadian</font>
                  </td>
                  <td colspan=2 align="center" valign=bottom>
                    <font face="Arial" color="#000000">Tahun Kemarin</font>
                  </td>
                  <td colspan=2 align="center" valign=bottom>
                    <font face="Arial" color="#000000">Tahun ini</font>
                  </td>
                </tr>
                <tr>
                  <td  align="center" valign=bottom>
                    <font face="Arial" color="#000000">Banyak Kejadian</font>
                  </td>
                  <td  align="center" valign=bottom>
                    <font face="Arial" color="#000000">Korban Jiwa</font>
                  </td>
                  <td  align="center" valign=bottom>
                    <font face="Arial" color="#000000">Banyak Kejadian</font>
                  </td>
                  <td  align="center" valign=bottom>
                    <font face="Arial" color="#000000">Korban Jiwa</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Tanah longsor</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_longsor'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_longsor'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_longsor'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_longsor'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_longsor'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Banjir</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_banjir'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">c. Banjir bandang</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_banjir_bandang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_banjir_bandang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_banjir_bandang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_banjir_bandang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_banjir_bandang'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">d. Gempa bumi</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_gempa'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gempa'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_gempa'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gempa'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gempa'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">e. Tsunami</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_tsunami'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_tsunami'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_tsunami'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_tsunami'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_tsunami'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">f. Gelombang pasang laut</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_gelombang_pasang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gelombang_pasang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_gelombang_pasang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gelombang_pasang'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gelombang_pasang'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">g. Angin puyuh/puting beliung/ topan</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_puyuh'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_puyuh'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_puyuh'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_puyuh'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_puyuh'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">h. Gunung meletus</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_gunung_meletus'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_gunung_meletus'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_gunung_meletus'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_gunung_meletus'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_gunung_meletus'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">i. Kebakaran hutan dan lahan</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_kebakaran_hutan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kebakaran_hutan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_kebakaran_hutan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kebakaran_hutan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kebakaran_hutan'] ?></font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">j. Kekeringan (lahan)</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_kekeringan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_lalu_kekeringan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_lalu_kekeringan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_kejadian_sekarang_kekeringan'] ?></font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000"><?=$sdgs_detail['pengisian_podes']['bencana_korban_sekarang_kekeringan'] ?></font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header no-border">
            <h5>1301</h5>
            <ul class="creative-dots">
              <li class="bg-primary big-dot"></li>
              <li class="bg-secondary semi-big-dot"></li>
              <li class="bg-warning medium-dot"></li>
              <li class="bg-info semi-medium-dot"></li>
              <li class="bg-secondary semi-small-dot"></li>
              <li class="bg-primary small-dot"></li>
            </ul>
            <br>
          </div>
          <div class="card-body pt-0">
            <div class="activity-table table-responsive recent-table">
              <table class="table">
                <colgroup span="5" width="64"></colgroup>
                <tr>
                  <td rowspan=5 height="328" align="center" valign=middle sdval="1301" sdnum="1033;">
                    <font face="Arial" color="#000000">1301</font>
                  </td>
                  <td colspan=4 align="left" valign=bottom>
                    <font face="Arial" color="#000000">Keberadaan kepala desa/lurah dan sekretaris kepala desa/lurah</font>
                  </td>
                </tr>
                <tr>
                  <td rowspan=2 align="center" valign=middle>
                    <font face="Arial" color="#000000">Pemerintah desa/kelurahan</font>
                  </td>
                  <td colspan=3 align="left" valign=bottom>
                    <font face="Arial" color="#000000">Jika ada pemerintah desa/kelurahan</font>
                  </td>
                </tr>
                <tr>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">Umur</font>
                  </td>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">Jenis kelamin</font>
                  </td>
                  <td  align="center" valign=middle>
                    <font face="Arial" color="#000000">Pendidikan tertinggi yang<br>ditamatkan</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">a. Kepala Desa/Lurah</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                </tr>
                <tr>
                  <td >
                    <font face="Arial" color="#000000">b. Sekretaris Desa/Sekretaris Kelurahan</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                  <td >
                    <font face="Arial" color="#000000">{txt}</font>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <?php $this->load->view('admin/src/bottom'); ?>
    <script src="<?= base_url(); ?>js/keuangan/dashboard.js" defer></script>