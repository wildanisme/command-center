<div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-lg-6 main-header">
					<h2>Esakip<span>Add</span></h2>
					<h6 class="mb-0">Bogor Comand Center</h6>
				</div>
				<div class="col-lg-6 breadcrumb-right">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
						<li class="breadcrumb-item">Apps </li>
						<li class="breadcrumb-item">Esakip</li>
						<li class="breadcrumb-item active">Esakip</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<form class="form-horizontal form-material" method='post' enctype="multipart/form-data">

								<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
								<?php if (!empty($message)) echo "
                        <div class='alert alert-$message_type'>$message</div>";?>
								<h3 class="text-primary">Data :</h3>


								<div class="form-group">
									<label class="col-md-12">Jumlah Kemiskinan</label>
									<div class="col-md-12">
										<input type="number" name="jml_kemiskinan" value="0" min="0" class="form-control form-control-line">
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-md-12">Rata-rata Perkembangan</label>
									<div class="col-md-12">
										<input type="number" name="sasaran" value="0" min="0" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Total Laki-laki</label>
									<div class="col-md-12">
										<input type="number" name="program" value="0" min="0" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Total Wanita</label>
									<div class="col-md-12">
										<input type="number" name="kegiatan" value="0" min="0" class="form-control form-control-line">
									</div>
								</div> -->
								<div class="form-group">
									<label class="col-md-12">Tahun</label>
									<div class="col-md-12">
										<input type="number" name="tahun" class="form-control form-control-line">
									</div>
								</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12">
								<a href="<?=base_url();?>esakip" class="btn btn-light m-l-10"><span class="icon-angle-left"></span> Batal</a>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12"><button type="submit" class="btn btn-primary m-l-10" style="float: right;"><span class="icon-save"></span> Simpan</button>

							</div>
						</div>
						</form>
					</div>
				</div>