 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Sambutan</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Manage Sambutan</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <a href="<?=base_url()?>manage_company_profile/add_sambutan" class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a>
                    <hr>
                    <table class="table table-striped" id="myTable">
                        <thead>
                            <tr>
                                <th width=70px>#</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th width=60px>Foto</th>
                                <th width=70px>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $num = 1;
                            foreach ($query as $row) {
                                echo"
                                <tr>
                                <td>$num</td>
                                <td>$row->nama</td>
                                <td>$row->jabatan</td>
                                <td>
                                <img src='".base_url()."/data/images/sambutan/$row->foto' style='max-width:50px' />
                                </td>

                                <td>
                                <a href='".base_url()."manage_company_profile/edit_sambutan/$row->id_sambutan' class='btn-xs' title='Edit'>
                                Edit
                                <i class='entypo-pencil'></i>
                                </a>
                                <a href='#' class='btn-xs' title='Delete' onclick='jQuery(\"#confirm\").modal(\"show\");delete_(\"$row->id_sambutan\")'>
                                Hapus
                                <i class='entypo-cancel'></i>
                                </a>

                                </td>
                                </tr>
                                ";

                                $num++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    function delete_(id)
    {
        $('#confirm_title').html('Confirmation');
        $('#confirm_content').html('Are you sure want to delete it?');
        $('#confirm_btn').html('Delete');
        $('#confirm_btn').attr('href',"<?php echo base_url();?>manage_company_profile/delete_sambutan/"+id);
    }
</script>