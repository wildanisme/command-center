 <script type="text/javascript" src="<?php echo base_url()."asset" ;?>/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#isi",
            theme: "modern",
            plugins: [ 
			"advlist autolink link image lists charmap print preview hr anchor pagebreak", 
			"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking", 
			"table contextmenu directionality emoticons paste textcolor filemanager" 
			], 
			image_advtab: true, 
			toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
			 });
    </script>

 <!-- BEGIN: Content-->
 <div class="app-content content">
 	<div class="content-overlay"></div>
 	<div class="header-navbar-shadow"></div>
 	<div class="content-wrapper">
 		<div class="content-header row">
 			<div class="content-header-left col-md-9 col-12 mb-2">
 				<div class="row breadcrumbs-top">
 					<div class="col-12">
 						<h2 class="content-header-title float-left mb-0">Manage Sambutan</h2>
 						<div class="breadcrumb-wrapper col-12">
 							<ol class="breadcrumb">
 								<li class="breadcrumb-item"><a href="<?=base_url();?>manage_company_profile/sambutan">Sambutan</a>
 								</li>
 								<li class="breadcrumb-item active"><a href="#">add</a>
 								</li>

 							</ol>
 						</div>
 					</div>
 				</div>
 			</div>

 		</div>
 		<div class="content-body">
 			<div class="card">
 				<div class="card-content">
 					<div class="card-body">

 						<?php if (!empty($message)) echo "
 						<div class='alert alert-$message_type'>$message</div>";?>
 						<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
 							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
 							<div class="form-group">
 								<label class="control">Nama</label>
 								<input type="text" class="form-control" id='nama' name='nama' value='<?php echo $nama;?>' placeholder="">
 							</div>
 							<div class="form-group">
 								<label class="control">Jabatan</label>
 								<input type="text" class="form-control" name='jabatan' value='<?php echo $jabatan;?>' placeholder="">
 							</div>
 							<div class="form-group">
 								<label class="control">Foto</label>
 								<?php echo"
							<div class='member-img'>
								<img src='".base_url()."data/images/sambutan/$foto' class='img-rounded' style='max-width:200px' />
								
							</div><br>";?>

 								<input type="file" name='userfile' class="form-control file2 btn" data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
 								<p>
 									Max : 500px | 1MB
 								</p>
 								<?php if (!empty($error)) echo "
 								<div class='alert alert-warning'>$error</div>";?>
 							</div>
 							<div class="form-group">
 								<label class="control">Isi sambutan</label>
 								<textarea class="form-control" id='isi' name='isi'><?php echo $isi;?></textarea>
 							</div>
 							<div class="form-group">
 								<div class="col-12">
 									<button type="submit" class="btn btn-primary waves-effect waves-light mb-1"><span class="btn-label"><i class="fa fa-plus"></i></span>Perbaharui</button>	
 								</div>

 							</div>
 						</form>

 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>
