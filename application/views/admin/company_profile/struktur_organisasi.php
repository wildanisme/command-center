<script type="text/javascript" src="<?php echo base_url()."asset" ;?>/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            theme: "modern",
            plugins: [ 
			"advlist autolink link image lists charmap print preview hr anchor pagebreak", 
			"searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking", 
			"table contextmenu directionality emoticons paste textcolor filemanager" 
			], 
			image_advtab: true, 
			toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
			 });
    </script>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Struktur Organisasi</h4>
		</div>
		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

			<ol class="breadcrumb">
				<li>
					<a href="<?php echo base_url();?>admin"><i class="entypo-home"></i>Home</a>
				</li>

				<li class="active">		
					<strong>Struktur Organisasi</strong>
				</li>
			</ol>
		</div>
		<!-- /.col-lg-12 -->
	</div>


<div class="row">
	<div class="col-md-12">
		
			<div class="white-box">
				<?php if (!empty($message)) echo "
				<div class='alert alert-$message_type'>$message</div>";?>
				<form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
					
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control" rows="10" name="isi" id="isi"><?php echo $isi;?></textarea>
						</div>
					</div>
					
						<div class="col-sm-12">
							<button type="submit" class="btn btn-primary">Update</button>	
						</div>
						
					</div>
				</form>
			
			</div>

		</div>
	</div>
</div>
