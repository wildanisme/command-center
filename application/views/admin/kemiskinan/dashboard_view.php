


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

      <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
              <h1>474.700</h1>
			  <h4 class="media-body">			  
			  <span class="m-0">Keluarga Miskin</span>
            </h4><i class="icon-bg" data-feather="user-plus"></i>
          </div>
        </div>
      </div>
    </div>
	
  </div>
  <div class="col-xl-6 col-lg-6 box-col-6">
		<div class="card">
		  <div class="card-header">
			<div class="row">
			  <div class="col-9">
				<h5>Persentase Keluarga Miskin 2022</h5>
			  </div>
			  <div class="col-3 text-right"><i class="text-muted" data-feather="shopping-bag"></i></div>
			</div>
		  </div>
		  <div class="card-body pt-0 r-radial">
			<div id="piechart_persen1"></div>
		  </div>
		</div>
  </div>
  <div class="col-xl-6 col-lg-6 box-col-6">
  <div class="card">
    <div class="card-header">
      <h5>Tren Angka Kemiskinan</h5>
    </div>
    <div class="card-body p-0">
      <div id="colchart_tren"></div>
    </div>
  </div>
  </div>
  
  
<div class="col-sm-12 col-xl-12 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Keluarga Miskin Per Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="colchart_kec"></div>
    </div>
  </div>
</div>


<div class="col-sm-12">


    

<!-- Plugins JS start-->
<!-- Plugins JS start-->
<script type="text/javascript">
var opt_persen1 = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: ['Total Penduduk', 'Keluarga Miskin'],
    series: [5664372, 474700],
    legend: {
        position: 'bottom'
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:[pocoAdminConfig.primary, '#fd517d', '#80cf00', '#06b5dd', '#fd517d']
}

var chart_persen1 = new ApexCharts(
    document.querySelector("#piechart_persen1"),
    opt_persen1
);
chart_persen1.render();
</script>

<script>
// column chart
var opt_colchart_kec = {
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      horizontal: false,
      endingShape: 'rounded',
      columnWidth: '55%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series: [{
    name: 'Jumlah',
    data: [5548,8296,10470,2760,11506,7885,8985,12008,7981,11731,11020,4908,8165,10826,9222,8721,8114,5471,3613,9474,7142,3992,4949,12338,7221,11359,10899,16752,5413,7927,4711,8514,9596,8752,10432,4470,10584,5404,4903,4812]


    }],
    xaxis: {
      categories: ['Babakan Madang','Bojong Gede','Caringin','Cariu','Ciampea','Ciawi','Cibinong','Cibungbulang','Cigombong','Cigudeg','Cijeruk','Cileungsi','Ciomas','Cisarua','Ciseeng','Citeureup','Dramaga','Gunung Putri','Gunung Sindur','Jasinga','Jonggol','Kemang','Klapanunggal','Leuwiliang','Leuwisadeng','Megamendung','Nanggung','Pamijahan','Parung','Parung Panjang','Ranca Bungur','Rumpin','Sukajaya','Sukamakmur','Sukaraja','Tajurhalang','Tamansari','Tanjungsari','Tenjo','Tenjolaya'],
      },
      yaxis: {
        title: {
          text: 'Keluarga Miskin'
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function (val) {
            return "" + val + " Kemiskinan"

          }


        }
      },
      colors:['#80cf00', '#fe80b2', '#80cf00']
    }

    var colchart_kec = new ApexCharts(
      document.querySelector("#colchart_kec"),
      opt_colchart_kec
      );

    colchart_kec.render();
  </script>

<script>
// column chart
var opt_colchart_tren = {
  chart: {
    height: 350,
    type: 'line',
  },
  plotOptions: {
    bar: {
      horizontal: false,
      endingShape: 'rounded',
      columnWidth: '55%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series: [{
    name: 'Jumlah',
    data: [499100,479100,487100,490800,487300,415000,395000,402800,491200,474700]

    }],
    xaxis: {
      categories: ['2013','2014','2015','2016','2017','2018','2019','2020','2021','2022'],
      },
      yaxis: {
        title: {
          text: 'Penduduk Miskin'
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function (val) {
            return "" + val + " Kemiskinan"

          }


        }
      },
      colors:[ '#fe80b2', '#80cf00', '#80cf00']
    }

    var colchart_tren = new ApexCharts(
      document.querySelector("#colchart_tren"),
      opt_colchart_tren
      );

    colchart_tren.render();
  </script>

<script type="text/javascript">
  var mapOptions = {
             center: [-6.821018391805014, 107.94189545895269],
             zoom: 20
            }
            var peta = new L.map('peta', mapOptions);

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 11,
              id: 'mapbox/streets-v11',
              tileSize: 512,
              zoomOffset: -1,
              accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(peta);
            
        <?php foreach ($gerai as $data): ?>



          var marker = new L.Marker([<?php echo $data->lat?>, <?php echo $data->lng?>]);
          marker.bindPopup("<b><?php echo $data->nama?></b>.").openPopup();
          marker.addTo(peta);
        <?php endforeach; ?>
        
        
      </script>



<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>