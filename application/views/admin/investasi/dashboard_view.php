


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

<div class="col-lg-12 xl-100">
  <div class="row">


    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5> APBD OPD</h5>
          <div class="card-header-right">
          <h5>List APBD</h5>
          <div class="card-header-right">
            <?php
            $total_data_tahun_ini = count($data_investasio);
            if ($total_data_tahun_ini >= 1) {

            }
            ?>
            
          </div>  
          </div>
        </div>
        <div class="card-body p-0">

          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_investasi">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama OPD</th>
                  <th scope="col">Apbd</th>
                  <th scope="col">Target Pendapatan</th>
                  <th scope="col">Realisasi Pendapatan</th>
                  <th scope="col">Anggaran Belanja</th>
                  <th scope="col">Realisasi Belanja</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>
              <tbody>
              <?php echo $i=0;?>
                <?php foreach ($data_investasio as $key => $data): ?>
                <tr >
                  <td class="f-w-100"> <?php echo $i++ ?> </td>
                  <td class="f-w-100"> <?php echo $data->nama_opd?> </td>
                  <td class="f-w-100"> <?php echo $data->apbd?> </td>
                  <td class="f-w-100"> <?php echo $data->target_investasio?> </td>
                  <td class="f-w-100"> <?php echo $data->realisasi_investasio?> </td>
                  <td class="f-w-100"> <?php echo $data->agg_bljo?> </td>
                  <td class="f-w-100"> <?php echo $data->real_bljo?> </td>
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('investasi/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
          
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready( function () {
    $('#table_investasi').DataTable({
        "pageLength": 10 // Mengatur jumlah data per halaman menjadi 10
    });
});

</script>






