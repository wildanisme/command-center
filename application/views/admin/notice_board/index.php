 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Pengumuman</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Manage Pengumuman</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <div class="pull-right">
                        <form role="search" class="app-search hidden-xs m-r-10">
                            <input type="text" name="s" placeholder="Search..." class="form-control" style="background:#eee;"> <a href="" class="active"></a>
                        </form>
                    </div>
                    <h3 class="box-title"><a href="<?=base_url()?>manage_notice/add_notice"  class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a></h3>
                    <p class="text-muted m-b-20"></p>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr> 
                                    <th>#</th>
                                    <th>Teks </th>
                                    <th>Tanggal </th>
                                    <th>Status </th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($notice as $n){ ?>
                                    <tr> 
                                        <td><?=$no?></td>
                                        <td><?=$n->text?></td>
                                        <td><?=$n->date?></td>
                                        <td><?=$n->status?></td>
                                        <td>
                                            <a href='<?php echo base_url()."manage_notice/edit_notice/".$n->notice_id."" ?>' class='btn-xs' title='Edit' data-toggle="tooltip" data-original-title="Edit">

                                                <i class="fa fa-pencil text-inverse m-r-10"></i> 
                                            </a>
                                            <a class='btn-xs' title='Delete'  onclick='delete_("<?=$n->notice_id?>")' data-toggle="tooltip" data-original-title="Close">
                                                <i class="fa fa-close text-danger"></i>
                                            </a></td>
                                        </tr>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function delete_(id)
        {
            swal({   
                title: "Apakah anda yakin?",   
                text: "Anda tidak dapat mengembalikan data ini lagi jika sudah terhapus!",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Hapus",
                closeOnConfirm: false 
            }, function(){   
                window.location = "<?php echo base_url();?>manage_notice/delete/"+id;
                swal("Berhasil!", "Data telah terhapus.", "success"); 
            });
        }
    </script>