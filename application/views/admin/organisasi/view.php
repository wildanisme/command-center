	<script>
		$(document).ready(function () {
	// create a tree
	$("#tree-data").jOrgChart({
		chartElement: $("#tree-view"),
		nodeClicked: nodeClicked
	});

	// lighting a node in the selection
	function nodeClicked(node, type) {
		node = node || $(this);
		$('.jOrgChart .selected').removeClass('selected');
		node.addClass('selected');
	}
});
</script>
<style type="text/css">
	.nav-tabs>li{
		width: 50%;
		text-align: center;
		text-transform: uppercase;
	}
	.customtab li.active a, .customtab li.active a:focus, .customtab li.active a:hover {
		border-bottom: 2px solid #6003C8;
		color: #6003C8;
	}
	.nav-tabs>li>a{
		border-radius: 0px;
	}
</style>
<div class="container-fluid">

	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				
					<div class="x_panel">
						<div class="x_content">
							<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
								<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								<label id='status'></label>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="panel panel-default">
										<div class="panel-heading">organisasi</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12 centered">
													<div class="form-group">
														<img src="<?=base_url()?>data/organisasi/<?= ($detail->foto=='') ? 'sumedang.png' : $detail->foto?>" alt="foto" class="img-responsive" style="width: 200px;">
													</div>
												</div>
											</div>
											<a href="" class="fcbtn btn btn-outline btn-primary btn-block" style="margin-left: 7px;" data-toggle="modal" data-target="#editorganisasi" ><i class="ti-pencil"></i>Edit organisasi</a>
											<a class="fcbtn btn btn-outline btn-primary btn-block" style="margin-left: 7px;"  onclick="delete_organisasi_(<?=$detail->id_organisasi;?>)" ><i class="ti-trash"></i>Hapus organisasi</a>
										</div>

										
										
									</div>
									<br>
									<br>

								
									</div>
									<div class="col-md-8">

										<div class="panel panel-default">
											<div class="panel-heading">
												Detail organisasi


											</div>
											<div class="panel-body">
												<div class="row">

													<div class="col-md-12">
														<div class="form-group">
															<label>Kategori</label>
															<p><?=$detail->nama_kategori_organisasi?></p>
														</div>
													</div>



													<div class="col-md-12">
														<div class="form-group">
															<label>Nama organisasi</label>
															<p><?=$detail->nama_organisasi?></p>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label>Deskripsi </label>
															<p><?=$detail->deskripsi?></p>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Alamat </label>
															<p><?=$detail->alamat?></p>
														</div>
													</div>

													<div class="row">
													<div class="col-md-4">
														<div class="form-group">
															<label>Telepon </label>
															<p><?=$detail->telepon?></p>
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label>Email </label>
															<p><?=$detail->email?></p>
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label>Facebook </label>
															<p><?=$detail->facebook?></p>
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label>Instagram </label>
															<p><?=$detail->instagram?></p>
														</div>
													</div>

													<div class="col-md-4">
														<div class="form-group">
															<label>Twitter </label>
															<p><?=$detail->twitter?></p>
														</div>
													</div>

													
												</div>


													


												</div>
											</div>
										</div>

	

									
									</div>
								</div>
							</div>
						</div>

						<div id="editorganisasi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true" style="display: none;">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="panel-heading">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
														<h4 class="modal-title" id="myLargeModalLabel1" style="color:white;">Edit organisasi</h4>
													</div>
													<div class="modal-body">
													<form method='post' enctype="multipart/form-data" >
														<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
															
															<input type="hidden" name="id_organisasi" value="<?=$detail->id_organisasi?>" id="id_organisasi"/>
															
															<div class="row">
																<div class="col-md-5">
																	<div class="panel panel-default">
																		
																		<div class="panel-body">
																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																					<label for="input-file-now">Foto</label>
																						<input type="file" id="input-file-now" name="foto" class="dropify" />
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>

																	<div class="panel panel-default">
																		<div class="panel-body">

																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="exampleInputEmail1">Telepon</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="fa fa-phone"></i></div>
																						<input type="text" class="form-control" id="exampleInputEmail1" value="<?= $telepon;?>" name="telepon">
																					</div>
																				</div>
																			</div>


																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="exampleInputEmail1">Email</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="ti-email"></i></div>
																						<input type="email" class="form-control" name="email" value="<?= $email;?>"  >
																					</div>
																				</div>
																			</div>

																			

																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="exampleInputEmail1">Facebook</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="ti-facebook"></i></div>
																						<input type="text" class="form-control"  name="facebook" value="<?= $facebook;?>" >
																					</div>
																				</div>
																			</div>

																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="exampleInputEmail1">Twitter</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="ti-twitter"></i></div>
																						<input type="text" class="form-control"  name="twitter" value="<?= $twitter;?>" >
																					</div>
																				</div>
																			</div>

																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="exampleInputEmail1">Instagram</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="ti-instagram"></i></div>
																						<input type="text" class="form-control"  name="instagram" value="<?= $instagram;?>" >
																					</div>
																				</div>
																			</div>





																		</div>
																	</div>


																</div>
																<div class="col-md-7">	
																	<div class="panel panel-default">
																		
																		<div class="panel-body">
																			<div class="row">

																				<div class="col-md-12">
																					<div class="form-group">
																						<label>Kategori</label>
																						<select name="id_kategori_organisasi" class="form-control form-control-line select2">
																							<option value="">Pilih</option>
																							<?php 
																							foreach ($kategori as $row) {
																								$selected = (!empty($id_kategori_organisasi) && $id_kategori_organisasi==$row->id_kategori_organisasi) ? "selected" : "";
																								echo "<option $selected value='$row->id_kategori_organisasi'>$row->nama_kategori_organisasi</option>";
																							}
																							?>
																						</select>
																					</div>
																				</div>

																				

																				<div class="col-md-12">
																					<div class="form-group">
																						<label>Nama organisasi </label>
																						<input type="text" name="nama_organisasi" value="<?= $nama_organisasi;?>" class="form-control" placeholder="Masukkan Nama organisasi">
																					</div>
																				</div>
																				<div class="col-md-12">
																					<div class="form-group">
																						<label>Dekripsi organisasi</label>
																						<textarea class="form-control" name="deskripsi"><?= $deskripsi;?></textarea>
																					</div>
																				</div>
																				<div class="col-md-12">
																					<div class="form-group">
																						<label>Alamat</label>
																						<textarea class="form-control" name="alamat"><?= $alamat;?></textarea>
																						<select name="id_desa" class="form-control form-control-line select2">
																							<option value="">Pilih desa</option>
																							<?php 
																							
																							foreach ($desa as $row) {
																								$selected = (!empty($id_desa) && $id_desa==$row->id_desa) ? "selected" : "";
																								echo "<option $selected value='$row->id_desa'>$row->desa, $row->kecamatan</option>";
																							}
																							?>
																						</select>
																					</div>
																				</div>
																				


																				<div class="col-md-12">
																					<div class="form-group">
																						<label>Tag </label>
																						<input type="text" class="form-control" name="tag" value="<?= $tag;?>" placeholder="Dipisah dengan tanda koma (,)"></input>
																					</div>
																				</div>




																			</div>
																		</div>
																	</div>  

																</div>
															
															</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
															<button type="submit" class="btn btn-primary waves-effect text-left">Kirim</button>
														</div>
													</form>
												</div>
												<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										</div>
										

						<script>
											
							
							function delete_organisasi_(id)
							{
								swal({   
									title: "Apakah anda yakin menghapus organisasi?",   
									text: "Data tersebut secara permanen akan terhapus",   
									type: "warning",   
									showCancelButton: true,   
									confirmButtonColor: "#DD6B55",   
									confirmButtonText: "Hapus",
									closeOnConfirm: false 
								}, function(){   
									window.location = "<?php echo base_url();?>manage_organisasi/hapus_organisasi/"+id;
									swal("Berhasil!", "Data telah terhapus.", "success"); 
								});
							}
							
							
							
							
							
							
							
						</script>




