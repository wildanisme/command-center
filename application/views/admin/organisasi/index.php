
<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">organisasi</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<li class="active">organisasi</li>				</ol>
				</div>
				<!-- /.col-lg-12 -->
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="white-box">
						<div class="row">
							<div class="col-md-2 b-r">
								<a href="<?php echo base_url();?>manage_organisasi/add">
									<button class="btn btn-primary m-t-15 btn-block"><i class="ti-plus" data-toggle="modal" data-target="#modal_add_new"></i> Tambah organisasi</button>
								</a>
							</div>
							<form method="POST">
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
								<div class="col-md-3">
									<div class="form-group">
										<label>Nama organisasi </label>
										<input type="text" class="form-control" placeholder="Cari berdasarkan Nama organisasi" name="nama_organisasi" value="<?=($filter) ? $filter_data['nama_organisasi'] : ''?>">
									</div>
								</div>

								<div class="col-md-3">
									<div class="form-group">
										<label>Kategori </label>
										<select class="form-control select2" name="id_kategori_organisasi">
											<option value="">Semua</option>
											<?php 
											foreach ($kategori as $row) {
												$selected = (!empty($id_kategori_organisasi) && $id_kategori_organisasi==$row->id_kategori_organisasi) ? "selected" : "";
												echo "<option $selected value='$row->id_kategori_organisasi'>$row->nama_kategori_organisasi</option>";
											}
											?>

										</select>
									</div>
								</div>


								<div class="col-md-2">
									<div class="form-group">
										<br>
										<button type="submit" class="btn btn-primary m-t-5 btn-outline"><i class="ti-filter"></i> Filter</button>
										<?php 
										if($filter){
											?>
											<a href="" class="btn btn-default m-t-5"><i class="ti-back-left"></i> Reset</a>
											<?php
										}
										?>
									</div>
								</div>

							</form>
						</div>

					</div>
				</div>

			</div>


			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="x_content">

						<?php foreach($organisasi as $l){
							
							?>

							<div class="col-md-4 col-sm-6">
								<div class="white-box">
									<div class="row b-b" style="min-height: 120px;" >
										<div class="col-md-4 col-sm-4 text-center b-r" style="min-height: 120px;padding: 5%">
											<i class="icon-share" style="font-size: 50px;color:#2FBDC8"></i> 
											
										</div>
										<div class="col-md-8 col-sm-8">
											<br>
											<h3 class="box-title m-b-0"><?=$l->nama_organisasi?></h3>
											
											
										</div>
									</div>


									<div class="row b-b">
										<div class="col-md-6 b-r text-center" style="padding-bottom: 20px;">
											<h3 class="box-title m-b-0">Status</h3>
											<div class="label label-table label-danger"><?=$l->status?></div>

										</div>

									
										<div class="col-md-6 text-center">
											<h3 class="box-title m-b-0">Kategori</h3>
											<?=$l->nama_kategori_organisasi?>
										</div>
									</div>

									<div class="row">
										<div class="col-md-12">
											<br>
											<address>
												<a href="<?php echo base_url();?>manage_organisasi/view/<?=$l->id_organisasi?>">
													<button class="fcbtn btn btn-primary btn-outline btn-1b btn-block">Detail organisasi</button>
												</a>
											</address>
										</div>
									</div>

								</div>
							</div>
							<?php } ?>




							<!-- /.col -->
						</div>




						<div class="row">
							<div class="col-md-12 pager">
								<?php 
								if(!$filter){
									echo make_pagination($pages,$current);
								}
								?>
							</div>
						</div>




					</div>

				</div>
