<?php
$url = "https://optimis.bogorkab.go.id/#/commandcenter/" . date("Y-m-d");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Proses formulir pengiriman data ke database
    $data1 = $_POST["data1"];
    $data2 = $_POST["data2"];

    // Validasi dan sanitasi data jika diperlukan

    // Simpan data ke database
    $conn = mysqli_connect("localhost", "username", "password", "nama_database");

    if (!$conn) {
        die("Koneksi gagal: " . mysqli_connect_error());
    }

    $sql = "INSERT INTO nama_tabel (kolom1, kolom2) VALUES ('$data1', '$data2')";

    if (mysqli_query($conn, $sql)) {
        echo "Data berhasil disimpan ke database.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html>
<head>
    <!-- <title>Simpan Data dari URL ke Database</title> -->
</head>
<body>
    <!-- <h1>Simpan Data dari URL ke Database</h1> -->

    <iframe id="frame" src="<?php echo $url; ?>" frameborder="0" gesture="media" allow="encrypted-media" allowtransparency="true" allowfullscreen="true" style="width: 100%; min-height: 720px; height: max-content;"></iframe>

    <script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>
</body>
</html>
