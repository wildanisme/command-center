 <div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2><span>Dashboard Perizinan</span></h2>
          <h6 class="mb-0">Kab. Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Perizinan</li>
            <li class="breadcrumb-item active">Dashboard  </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- Container-fluid starts-->
  <div class="container-fluid">
    <div class="row" id="perizinan-dashboard">
      <div class="col-md-12">
        <div class="loader-box">
          <div class="loader-16"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var dashboard = "perizinan";
</script>