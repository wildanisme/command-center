  <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Jenis Usaha</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Ref. Jenis Usaha</a>
                                    </li>

                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">


                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <table class="table data-list-view">
                            <thead>
                                <tr>

                                    <th>Nama Jenis Usaha</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php
                            foreach ($list as $key=> $row) {
                              ?>
                                <tr>

                                    <td class="product-name"><span id="nama_jenis_<?=$row->id_jenis_usaha;?>"><?=$row->nama_jenis;?></span></td>
                                    <td>
                                        <div class="chip <?=($row->status=="active") ?  'chip-success':'chip-warning';?> ">
                                            <div class="chip-body">
                                                <div class="chip-text" id="status_<?=$row->id_jenis_usaha;?>"><?=$row->status;?></div>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="product-action">
                                        <span class="" onclick="edit(<?=$row->id_jenis_usaha;?>)"><i class="feather icon-edit"></i></span>
                                        <span class="" onclick="hapus(<?=$row->id_jenis_usaha;?>)"><i class="feather icon-trash"></i></span>
                                    </td>
                                </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- DataTable ends -->

                    <!-- add new sidebar starts -->
                    <div class="add-new-data-sidebar">
                        <div class="overlay-bg"></div>
                        <div class="add-new-data">
                          <form method="post">
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                            <input type="hidden" name="id_jenis_usaha" id="id_jenis_usaha" value="" />
                            <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                                <div>
                                    <h4 class="text-uppercase">Ref. Jenis Usaha</h4>
                                </div>
                                <div class="hide-data-sidebar">
                                    <i class="feather icon-x"></i>
                                </div>
                            </div>
                            <div class="data-items pb-3">
                                <div class="data-fields px-2 mt-3">
                                    <div class="row">
                                        <div class="col-sm-12 data-field-col">
                                            <label for="data-name">Nama Jenis Usaha</label>
                                            <input type="text" name="nama_jenis"  class="form-control" id="nama_jenis">
                                        </div>
                                        <div class="col-sm-12 data-field-col">
                                            <label for="data-category"> Status </label>
                                            <select class="form-control select2" id="status" name="status">
                                                <option value="active">Aktif</option>
                                                <option value="not active">Not Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                                <div class="add-data-btn">
                                    <button class="btn btn-primary">Simpan</button>
                                </div>
                                <div class="cancel-data-btn">
                                    <button class="btn btn-outline-danger">Batal</button>
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                    <!-- add new sidebar ends -->
                </section>
                <!-- Data list view end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <script>
      function edit(id)
      {
        //alert(id);
        $("#id_jenis_usaha").val(id);
        var nama_jenis = $("#nama_jenis_"+id).html();
        //alert(nama_jenis);
        var status = $("#status_"+id).html();
        $("#nama_jenis").val(nama_jenis);
        $("#status").val(status).trigger("change");
        $(".add-new-data").addClass("show");
        $(".overlay-bg").addClass("show");
      }
      function hapus(id)
      {
        //alert(id);
        swal({
          title: "Hapus jenis usaha?",
          //icon: "info",
          buttons: true,
          dangerMode: false,
        })
        .then((isConfirm) => {
          if (isConfirm) {

            $.ajax({
                url :"<?php echo base_url("ref_jenisusaha/delete")?>",
                type:'post',
                data:{
                  id:id,
                  "<?=$this->security->get_csrf_token_name();?>" : "<?= $this->security->get_csrf_hash();?>",
                },
                 success    : function(data){
                    console.log(data);

                    swal("Jenis usaha berhasil dihapus", {
                      icon: "success",
                    });

                    setTimeout(function() {
                      window.location.href = "<?=base_url();?>ref_jenisusaha";
                    }, 500);
                 },
                     error: function(xhr, status, error) {
                      //swal("Opps","Error","error");
                      console.log(xhr);
                    }
            });

          }
        });
      }
      function tambah()
      {
        $("#id_jenis_usaha").val("");
          $("#nama_jenis").val("");
          $("#status").val("active").trigger("change");
      }
    </script>
