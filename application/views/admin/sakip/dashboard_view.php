


<style type="text/css">
#peta{
 height:65vh;
 width:100%;

}
</style>

      <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
        <div class="card gradient-secondary o-hidden">
          <div class="b-r-4 card-body">
            <div class="media static-top-widget">
              <div class="align-self-center text-center"><i data-feather="user-plus"></i></div>
              <div class="media-body"><span class="m-0">Jumlah SKPD </span>
                <h4 class="mb-0 counter"> 36
            </h4><i class="icon-bg" data-feather="user-plus"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
    <div class="card gradient-primary o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center"><i data-feather="shopping-bag"></i></div>
          <div class="media-body"><span class="m-0 text-white">Sasaran Strategis</span>
            <h4 class="mb-0 counter">17</h4><i class="icon-bg" data-feather="shopping-bag"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
    <div class="card gradient-warning o-hidden">
      <div class="b-r-4 card-body">
        <div class="media static-top-widget">
          <div class="align-self-center text-center">
            <div class="text-white i" data-feather="database"></div>
          </div>
          <div class="media-body"><span class="m-0 text-white">Sasaran Program</span>
           <h4 class="mb-0 counter text-white"> 419</h4><i class="icon-bg" data-feather="database"></i>
         </div>
       </div>
     </div>
   </div>
 </div>
 <div class="col-sm-6 col-xl-3 col-lg-6 box-col-6">
  <div class="card gradient-info o-hidden">
    <div class="b-r-4 card-body">
      <div class="media static-top-widget">
        <div class="align-self-center text-center">
          <div class="text-white i" data-feather="database"></div>
        </div>
        <div class="media-body"><span class="m-0 text-white">Sasaran Kegiatan</span>
          <h4 class="mb-0 counter text-white">1000</h4><i class="icon-bg" data-feather="database"></i>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- <div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Capaian SKPD </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div>
<div class="col-sm-12 col-xl-6 xl-100">
  <div class="card">
    <div class="card-header">
      <h5>Capaian Kecamatan </h5>
    </div>
    <div class="card-body p-0">
      <div id="column-chart"></div>
    </div>
  </div>
</div> -->
<div class="col-md-6">
  <div class="card">
    <div class="card-header">
      <h5>Capaian SAKIP</h5>
    </div>
    <div class="card-body progress-showcase row">
        <div class="col-md-3">
          <h6>Perangkat Daerah</h6>
        </div>
        <div class="col-md-9">
          perencanaan kinerja
          <div class="progress">
            <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 92.460%" aria-valuenow="92.460" aria-valuemin="0" aria-valuemax="100"> 92.460 %</div>
          </div>
          pengukuran kinerja
          <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 90.000%" aria-valuenow="90.000" aria-valuemin="0" aria-valuemax="100"> 90.000 %</div>
          </div>
          pelaporan kinerja
          <div class="progress">
            <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 69.467%" aria-valuenow="69.467" aria-valuemin="0" aria-valuemax="100"> 69.467 %</div>
          </div>
          evaluasi internal
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 69.500%" aria-valuenow="69.500" aria-valuemin="0" aria-valuemax="100"> 69.500</div>
          </div>
          pencapaian sasaran
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 71.460%" aria-="" valuenow="71.460" aria-valuemin="0" aria-valuemax="100">71.460</div>
          </div>
        </div>
    </div>
    <div class="card-body progress-showcase row">
        <div class="col-md-3">
          <h6>Kecamatan</h6>
        </div>
        <div class="col-md-9">
          perencanaan kinerja
          <div class="progress">
            <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" style="width: 92.460%" aria-valuenow="92.460" aria-valuemin="0" aria-valuemax="100"> 92.460 %</div>
          </div>
          pengukuran kinerja
          <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: 90.000%" aria-valuenow="90.000" aria-valuemin="0" aria-valuemax="100"> 90.000 %</div>
          </div>
          pelaporan kinerja
          <div class="progress">
            <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: 69.467%" aria-valuenow="69.467" aria-valuemin="0" aria-valuemax="100"> 69.467 %</div>
          </div>
          evaluasi internal
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 69.500%" aria-valuenow="69.500" aria-valuemin="0" aria-valuemax="100"> 69.500</div>
          </div>
          pencapaian sasaran
          <div class="progress">
            <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 71.460%" aria-="" valuenow="71.460" aria-valuemin="0" aria-valuemax="100">71.460</div>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="col-md-6">
  <div class="card">
    <div class="card-header">
      <h5>Capaian Indikator Kinerja Utama</h5>
    </div>
    <div class="card-body">
      <div class="dt-ext table-responsive">
        <table class=" datatables" >
          <thead>
            <tr>
              <th>No</th>
              <th>IKU</th>
              <th>Target</th>
              <th>Realisasi</th>
              <th>Capaian</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Indeks Pendidikan</td>
              <td>63,74</td>
              <td>62,52</td>
              <td>98,09</td>
            </tr>
            <tr>
              <td>2</td>
              <td>Rata -rata Lama Sekolah</td>
              <td>8,50</td>
              <td>8,34</td>
              <td>92,12</td>
            </tr>
            <tr>
              <td>3</td>
              <td>Harapan Lama Sekolah</td>
              <td>12,75</td>
              <td>12,5</td>
              <td>98,04</td>
            </tr>
            <tr>
              <td>4</td>
              <td>Indeks Membaca</td>
              <td>61,35</td>
              <td>61,35</td>
              <td>100</td>
            </tr>
            <tr>
              <td>5</td>
              <td>Indeks Kesehatan</td>
              <td>79,05</td>
              <td>79,46</td>
              <td>100,52</td>
            </tr>
            <tr>
              <td>6</td>
              <td>Laju Pertumbuhan Penduduk</td>
              <td>1,63</td>
              <td>1,24</td>
              <td>131,40</td>
            </tr>
            <tr>
              <td>7</td>
              <td>Angka Harapan Hidup</td>
              <td>71,39</td>
              <td>71,65</td>
              <td>100,36</td>
            </tr>
            <tr>
              <td>8</td>
              <td>Skor Pola Pangan Harapan</td>
              <td>81,12</td>
              <td>80,6</td>
              <td>99,36</td>
            </tr>
            <tr>
              <td>9</td>
              <td>Crude Birth Rate</td>
              <td>18,65</td>
              <td>14,60</td>
              <td>121,72</td>
            </tr>
            <tr> 
              <td>10</td>
              <td>Laju Pertumbuhan Ekonomi</td>
              <td>4,31</td>
              <td>5,2</td>
              <td>120,65</td>
            </tr>
            <tr>
              <td>11</td>
              <td>PDRB Perkapita (AHB)</td>
              <td>38,80</td>
              <td>45,72</td>
              <td>117,84</td>
            </tr>
            <tr>
              <td>12</td>
              <td>Tingkat Kemiskinan</td>
              <td>7,38</td>
              <td>7,73</td>
              <td>95,47</td>
            </tr>
            <tr>
              <td>13</td>
              <td>Tingkat Pengangguran Terbuka</td>
              <td>11,64</td>
              <td>10,64</td>
              <td>108,59</td>
            </tr>
            <tr>
              <td>14</td>
              <td>PDRB atas dasar harga berlaku</td>
              <td>244,05</td>
              <td>267,79</td>
              <td>109,73</td>
            </tr>
            <tr>
              <td>15</td>
              <td>Indeks Reformasi Birokrasi</td>
              <td>72</td>
              <td>67,76</td>
              <td>94,11</td>
            </tr>
            <tr>
              <td>16</td>
              <td>Indeks Kepuasan Masyarakat</td>
              <td>87</td>
              <td>97,33</td>
              <td>111,87</td>
            </tr>
            <tr>
              <td>17</td>
              <td>Opini BPK terhadap laporan keuangan pemerintah  daerah</td>
              <td>WTP</td>
              <td>WDP</td>
              <td>-</td>
            </tr>
            <tr>
              <td>18</td>
              <td>Nilai AKIP</td>
              <td>72,00</td>
              <td>68,83</td>
              <td>95,60</td>
            </tr>
            <tr>
              <td>19</td>
              <td>Indeks Inovasi Daerah</td>
              <td>65,00</td>
              <td>87,59</td>
              <td>134,75</td>
            </tr>
            <tr>
              <td>20</td>
              <td>Indeks Ketimpangan Williamson</td>
              <td>0,96</td>
              <td>0,88</td>
              <td>109,9</td>
            </tr>
            <tr>
              <td>21</td>
              <td>Indeks Konektivitas Infrastruktur</td>
              <td>1,47</td>
              <td>1,50</td>
              <td>102,04</td>
                </tr>
            <tr>
              <td>22</td>
              <td>Indeks Desa Membangun</td>
              <td>0,7362</td>
              <td>0,77</td>
              <td>104,59</td>
                </tr>
            <tr>
              <td>23</td>
              <td>Persentase Kawasan Permukiman Layak</td>
              <td>99,86</td>
              <td>99,70</td>
              <td>99,84</td>
                </tr>
            <tr>
              <td>24</td>
              <td>Indeks Kualitas LingkunganHidup</td>
              <td>56,15</td>
              <td>58,91</td>
              <td>104,92</td>
                </tr>
            <tr>
              <td>25</td>
              <td>Indeks Risiko Bencana</td>
              <td>134</td>
              <td>132,3</td>
              <td>101,28</td>
                </tr>
            <tr>
              <td>26</td>
              <td>Indeks Kebahagiaan</td>
              <td>74-72</td>
              <td>74-72</td>
              <td>100</td>
                </tr>
            <tr>
              <td>27</td>
              <td>Indeks Pembangunan Gender</td>
              <td>90</td>
              <td>90,95</td>
              <td>100,95</td>
                </tr>
            <tr>
              <td>28</td>
              <td>Indeks Kerukunan Hidup Beragama</td>
              <td>82,09-82,15</td>
              <td>82,83</td>
              <td>100,4</td>
                </tr>
            <tr>
              <td>29</td>
              <td>Tingkat Kepatuhan Perka dan Perkada Ketertiban Umum</td>
              <td>82,05</td>
              <td>82,05</td>
              <td>100</td>
                </tr>
            <tr>
              <td>30</td>
              <td>Indeks Pemberdayaan Gender</td>
              <td>58,90</td>
              <td>56,16</td>
              <td>95,35</td>
                </tr>
            <tr>
              <td>31</td>
              <td>Persentase Kebudayaan yang dilestarikan</td>
              <td>57,94</td>
              <td>60,75</td>
              <td>104,85</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-lg-12 xl-100">
  <div class="row">

    <!-- <div class="col-sm-6 col-xl-6 col-lg-6 box-col-6">
      <div class="card">
        <div class="card-header">
          <h5>Chart Pemilik Berdasarkan jenis kelamin </h5>
        </div>
        <div class="card-body apex-chart p-0">
          <div id="piechart"></div>
        </div>
      </div>
    </div> -->


    

<!-- Plugins JS start-->
<!-- Plugins JS start-->
<script>
// column chart
var options3 = {
  chart: {
    height: 350,
    type: 'bar',
  },
  plotOptions: {
    bar: {
      horizontal: false,
      endingShape: 'rounded',
      columnWidth: '55%',
    },
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ['transparent']
  },
  series: [{
    name: 'Jumlah',
    data: [<?php foreach ($jumkec as $data): ?>
      <?php echo $data->totalumkm.","?> 
      <?php endforeach; ?>]


    }],
    xaxis: {
      categories: [<?php foreach ($jumkec as $data): ?>
        <?php echo "'" .$data->kecamatan. "',"?> 
        <?php endforeach; ?>],
      },
      yaxis: {
        title: {
          text: 'Jumlah UMKM'
        }
      },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function (val) {
            return "" + val + " UKMM"

          }


        }
      },
      colors:['#80cf00', '#fe80b2', '#80cf00']
    }

    var chart3 = new ApexCharts(
      document.querySelector("#column-chart"),
      options3
      );

    chart3.render();

  </script>

  <script>
// pie chart
var options8 = {
  chart: {
    width: 380,
    type: 'pie',
  },
  labels: ['Perempuan', 'Laki-laki'],
  series: [<?php foreach ($jk as $data): ?>
    <?php echo $data->totalwanita.","?> 
    <?php echo $data->totallaki?>
    <?php endforeach; ?>],
    responsive: [{
      breakpoint: 480,
      options: {
        chart: {
          width: 300
        },
        legend: {
          position: 'bottom'
        }
      }
    }],
    colors:['#fe80b2', '#06b5dd']
  }

  var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
    );

  chart8.render();

</script>

<script type="text/javascript">

  var mapOptions = {
             center: [-6.821018391805014, 107.94189545895269],
             zoom: 20
            }
            var peta = new L.map('peta', mapOptions);

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery Â© <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 11,
              id: 'mapbox/streets-v11',
              tileSize: 512,
              zoomOffset: -1,
              accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(peta);
            
        <?php foreach ($gerai as $data): ?>



          var marker = new L.Marker([<?php echo $data->lat?>, <?php echo $data->lng?>]);
          marker.bindPopup("<b><?php echo $data->nama?></b>.").openPopup();
          marker.addTo(peta);
        <?php endforeach; ?>
        
        
      </script>



<!-- <script src="<?=base_url();?>js/umkm/dashboard.js" defer></script> -->
<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>