<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
				if (!empty($message)){
					?>
					<div class="alert alert-<?= $type;?> alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?= $message;?>
					</div>
					<?php }?>
					<div class="x_panel">
						<form method='post' enctype="multipart/form-data" >
						<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
						<input type="hidden" name="id_user" value="<?=$this->user_id?>">
							<div class="x_content">
								<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
									<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									<label id='status'></label>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="panel panel-default">
											<div class="panel-heading">Foto</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<input type="file" id="input-file-now" name="foto" class="dropify" />
														</div>
													</div>

												</div>
											</div>
										</div>

											<div class="panel panel-default">
											<div class="panel-body">

												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Telepon</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="ti-email"></i></div>
															<input type="text" class="form-control" id="exampleInputEmail1" name="telepon">
														</div>
													</div>
												</div>


												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Email</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="ti-email"></i></div>
															<input type="email" class="form-control" name="email">
														</div>
													</div>
												</div>

											

												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Facebook</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="ti-facebook"></i></div>
															<input type="text" class="form-control"  name="facebook">
														</div>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Twitter</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="ti-twitter"></i></div>
															<input type="text" class="form-control"  name="twitter">
														</div>
													</div>
												</div>

												<div class="col-md-12">
													<div class="form-group">
														<label for="exampleInputEmail1">Instagram</label>
														<div class="input-group">
															<div class="input-group-addon"><i class="ti-instagram"></i></div>
															<input type="text" class="form-control"  name="instagram">
														</div>
													</div>
												</div>





											</div>
										</div>



									</div>
									<div class="col-md-9">	
										<div class="panel panel-default">
											<div class="panel-heading">
												Detail organisasi
											</div>
											<div class="panel-body">
												<div class="row">

													<div class="col-md-12">
														<div class="form-group">
															<label>Kategori</label>
															<select name="id_kategori_organisasi" class="form-control form-control-line select2">
																<?php 
																foreach ($kategori as $row) {
																	echo "<option value='$row->id_kategori_organisasi'>$row->nama_kategori_organisasi</option>";
																}
																?>
															</select>
														</div>
													</div>

													

													<div class="col-md-12">
														<div class="form-group">
															<label>Nama organisasi </label>
															<input type="text" name="nama_organisasi" class="form-control" placeholder="Masukkan Nama organisasi">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label>Dekripsi organisasi</label>
															<textarea class="form-control" name="deskripsi"></textarea>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Alamat organisasi</label>
															<textarea class="form-control" name="alamat"></textarea>
														</div>
													</div>

													

													<div class="col-md-12">
														<div class="form-group">
															<label>Tag</label>
															<textarea class="form-control" name="tag" placeholder="Dipisah dengan tanda koma (,)"></textarea>
														</div>
													</div>

													




												</div>
											</div>
										</div>  





										<button type='submit' class='btn btn-primary pull-right' style="margin-left: 6px;">Submit</button> 
										<a href='<?= base_url();?>manage_organisasi' class='btn btn-default pull-right'>Back</a>
										


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<script>

					function hideMe()
					{$('#pesan').hide();}

					
				</script>