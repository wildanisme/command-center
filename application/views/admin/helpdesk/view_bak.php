<script>
	$(document).ready(function () {
	// create a tree
	$("#tree-data").jOrgChart({
		chartElement: $("#tree-view"),
		nodeClicked: nodeClicked
	});

	// lighting a node in the selection
	function nodeClicked(node, type) {
		node = node || $(this);
		$('.jOrgChart .selected').removeClass('selected');
		node.addClass('selected');
	}
});
</script>
<style type="text/css">
.nav-tabs>li{
	width: 50%;
	text-align: center;
	text-transform: uppercase;
}
.customtab li.active a, .customtab li.active a:focus, .customtab li.active a:hover {
	border-bottom: 2px solid #6003C8;
	color: #6003C8;
}
.nav-tabs>li>a{
	border-radius: 0px;
}
</style>
<div class="container-fluid">

	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
				if (!empty($message)){
					?>
					<div class="alert alert-<?= $type;?> alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?= $message;?>
					</div>
				<?php }?>
				<div class="x_panel">
					<div class="x_content">
						<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
							<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<label id='status'></label>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="panel panel-default">
									<div class="panel-heading">Foto Layanan</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12 centered">
												<div class="form-group">
													<img src="<?=base_url()?>data/foto_layanan/<?= ($detail->foto_layanan=='') ? 'sumedang.png' : $detail->foto_layanan?>" alt="user" class="img-responsive" style="width: 200px;">
												</div>
											</div>
										</div>
										<a href="" class="fcbtn btn btn-outline btn-primary btn-block" style="margin-left: 7px;" data-toggle="modal" data-target="#editSKPD"><i class="ti-pencil"></i>Edit Layanan</a>
										<a href="" class="fcbtn btn btn-outline btn-primary btn-block" style="margin-left: 7px;" data-toggle="modal" data-target="#hapusSKPD"><i class="ti-trash"></i>Hapus Layanan</a>
									</div>
									<div id="editLayanan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true" style="display: none;">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="panel-heading">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h4 class="modal-title" id="myLargeModalLabel1" style="color:white;">Edit Layanan</h4>
												</div>
												<div class="modal-body">
													<form method="POST">

														



														<div class="form-group">
															<label for="exampleInputuname">Nama Layanan</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="ti-user"></i></div>
																<input name="nama_layanan" value="<?=$detail->nama_layanan?>" type="text" class="form-control" id="skpd" placeholder="Nama Layanan">
															</div>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Deskripsi Layanan</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="ti-user"></i></div>
																<textarea name="deskripsi_layanan" class="form-control"><?=$detail->deskripsi_layanan?></textarea>
															</div>
														</div>

														<div class="form-group">
															<label for="exampleInputEmail1">SOP Layanan</label>
															<div class="input-group">
																<div class="input-group-addon"><i class="ti-user"></i></div>
																<textarea name="deskripsi_layanan" class="form-control"><?=$detail->sop?></textarea>
															</div>
														</div>

													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary waves-effect text-left">Kirim</button>
													</div>
												</form>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									<div id="hapusLayanan" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel1" aria-hidden="true" style="display: none;">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="panel-heading">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h4 class="modal-title" id="myLargeModalLabel1" style="color:white;">Hapus Layanan</h4>
												</div>
												<div class="modal-body">
													Apakah anda yakin akan menghapus Layanan ini maka seluruh sub-layanan dan persyaratan juga akan ikut terhapus?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Tidak</button>
													<a style="color: #fff !important" href="<?=base_url('ref_layanan/delete/'.$detail->id_layanan.'')?>" class="btn btn-primary waves-effect text-left">Ya</a>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</div>
								<br>
								<br>

								<div class="white-box">
									<label>Jadwal Layanan</label>
									<div class="table-responsive">
										<table class="table color-table">
											<thead>
												<tr>
													<th style="text-align: center">Hari</th>
													<th style="text-align: center">Status</th>
													<th style="text-align: center">Jam Buka</th>
													<th style="text-align: center">Jam Tutup</th>

												</tr>
											</thead>
											<tbody>

												<tr>
													<td style="text-align: center">Senin</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF" name="senin"></td>
													<td style="text-align: center">
														<input type="text" class="form-control jam_buka" name="jam_buka_senin" placeholder="">
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control jam_tutup" name="jam_tutup_senin" placeholder="" >
													</td>
												</tr>

												<tr>
													<td style="text-align: center">Selasa</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF" name="selasa>"></td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_buka_selasa" placeholder="" >
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_tutup_selasa" placeholder="" >
													</td>
												</tr>



												<tr>
													<td style="text-align: center">Rabu</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF"  name="rabu"></td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_buka_rabu" placeholder="" >
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_tutup_rabu" placeholder="" >
													</td>
												</tr>


												<tr>
													<td style="text-align: center">Kamis</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF"  name="kamis"></td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_buka_kamis" placeholder="" >
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_tutup_kamis" placeholder="" >
													</td>
												</tr>


												<tr>
													<td style="text-align: center">Jumat</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF"  name="jumat"></td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_buka_jumat" placeholder="" >
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_tutup_jumat" placeholder="" >
													</td>
												</tr>

												<tr>
													<td style="text-align: center">Sabtu</td>
													<td style="text-align: center"><input type="checkbox" value="dijadwalkan" class="js-switch" data-color="#7906FE" data-secondary-color="#B7B4AF"  name="sabtu"></td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_buka_sabtu" placeholder="" >
													</td>
													<td style="text-align: center">
														<input type="text" class="form-control" name="jam_tutup_sabtu" placeholder="" >
													</td>
												</tr>


											</tbody>
										</table>
									</div>
								</div>


							</div>
							<div class="col-md-9">
								
								<div class="panel panel-default">
									<div class="panel-heading">
										Detail Layanan


									</div>
									<div class="panel-body">
										<div class="row">

											<div class="col-md-12">
												<div class="form-group">
													<label>SKPD</label>
													<p><?=$detail->nama_skpd?></p>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label>Loket</label>
													<p><?=$detail->loket?></p>
												</div>
											</div>


											<div class="col-md-12">
												<div class="form-group">
													<label>Nama Layanan</label>
													<p><?=$detail->nama_layanan?></p>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>Deskripsi Layanan</label>
													<p><?=$detail->deskripsi_layanan?></p>
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label>SOP</label>
													<p><?=$detail->sop?></p>
												</div>
											</div>
											

											


										</div>
									</div>
								</div>



								<div class="panel panel-default">
									<div class="panel-heading">
										Persyaratan

										<div class="pull-right" ><a href="#"><button class="btn btn-primary " style="margin-bottom: 15px;" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Persyaratan</button></a> </div>

									</div>

									<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="exampleModalLabel1">Tambah Persyaratan</h4>
												</div>
												<div class="modal-body">
												 <form id="data-form"  enctype="multipart/form-data" >

                       								 <input type="hidden" name="id_layanan"  id="id_layanan"/>

														<div class="form-group">
															<label for="recipient-name" class="control-label">Nama Persyaratan</label>
															<input type="text" class="form-control" name="nama_persyaratan">
														</div>
														<div class="form-group">
															<label for="message-text" class="control-label">Keterangan</label>
															<textarea class="form-control" name="keterangan"></textarea>
														</div>

														<div class="form-group">
															<label for="message-text" class="control-label">File / Template</label>
															<input type="file" id="input-file-now" name="file" class="dropify" />
														</div>

													</form>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
													<button type="button" class="btn btn-primary" onclick="simpan_persyaratan()">Simpan</button>
												</div>
											</div>
										</div>
									</div>


									<div class="panel-body">
										<div class="row">

											<table class="table">
												<thead>
													<tr><th>No</th><th>Nama Persyaratan</th><th>Keterangan</th><th>File</th><th>Opsi</th></tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Persyaratan 1</td>
														<td>Keterangan Persyaratan</td>
														<td>file 1</td>
														<td class="text-nowrap">
															<a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
															<a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
														</td>

													</tbody>
												</table>






											</div>
										</div>
									</div>




									<div class="panel panel-default">
										<div class="panel-heading">
											Sub-Layanan

											<div class="pull-right" ><a href="#"><button class="btn btn-primary " style="margin-bottom: 15px;" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Sub-Layanan</button></a> </div>

										</div>

										<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="exampleModalLabel1">Tambah Sub-Layanan</h4>
													</div>
													<div class="modal-body">
														<form >
															<div class="col-md-12">
																<div class="form-group">
																	<label>Nama Layanan </label>
																	<input type="text" name="nama_layanan" class="form-control" placeholder="Masukkan Nama Layanan">
																</div>
															</div>
															<div class="col-md-12">
																<div class="form-group">
																	<label>Dekripsi Layanan</label>
																	<textarea class="form-control" name="deskripsi_layanan"></textarea>
																</div>
															</div>

															<div class="col-md-12">
																<div class="form-group">
																	<label>SOP Layanan</label>
																	<textarea class="form-control" name="deskripsi_layanan"></textarea>
																</div>
															</div>


														</form>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="button" class="btn btn-primary">Simpan</button>
													</div>
												</div>
											</div>
										</div>


										<div class="panel-body">
											<div class="row">

												<table class="table">
													<thead>
														<tr><th>No</th><th>Nama Sub-Layanan</th><th>Opsi</th></tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Persyaratan 1</td>
															<td>Keterangan Persyaratan</td>
															<td>file 1</td>
															<td class="text-nowrap">
																<a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
																<a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a>
															</td>

														</tbody>
													</table>






												</div>
											</div>
										</div>







									</div>
								</div>
							</div>
						</div>




						<script>
						function simpan_persyaratan()
						{
						
						var formData = new FormData();
							formData.append('file_x', input.file[0]);	
						
							$.ajax({
							url:"<?php echo base_url('ref_layanan/add_persyaratan');?>",
							type:"POST",
					        //data: $('#data-form').serialize(),
					        data : formData,
					        async : false,
					        cache : false,
					        contentType : false,
					        processData : false,

					        success:function(resp){
					        	if (resp == true) {
					        		swal("Success!", "Update persyaratan sukses.", "success");
					        		window.location.reload(false); 
					        	} else {
					        		alert('Error Message: '+ resp);
					        		console.log(resp);
					        	}
					        },
					        error:function(event, textStatus, errorThrown) {
					        	alert('Error Message: '+ textStatus + ' , HTTP Error: '+ errorThrown);
					        }
					    })

					
						}
							

						</script>

