
<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">helpdesk</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<li class="active">helpdesk</li>				</ol>
				</div>
				<!-- /.col-lg-12 -->
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="white-box">
						<div class="row">
						
							<form method="POST">
							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama helpdesk </label>
										<input type="text" class="form-control" placeholder="Cari berdasarkan Nama helpdesk" name="judul" value="<?=!empty($filter_data['judul']) ? $filter_data['judul'] : ''?>">
									</div>
								</div>

								<div class="col-md-3">
									<div class="form-group">
										<label>Kategori </label>
										<select class="form-control select2" name="id_kategori_helpdesk">
											<option value="">Semua</option>
											<?php 
											foreach ($kategori as $row) {
												$selected = (!empty($id_kategori_helpdesk) && $id_kategori_helpdesk==$row->id_kategori_helpdesk) ? "selected" : "";
												echo "<option $selected value='$row->id_kategori_helpdesk'>$row->nama_kategori_helpdesk</option>";
											}
											?>

										</select>
									</div>
								</div>


								<div class="col-md-2">
									<div class="form-group">
										<br>
										<button type="submit" class="btn btn-primary m-t-5 btn-outline"><i class="ti-filter"></i> Filter</button>
										<?php 
										if($filter){
											?>
											<a href="<?= base_url();?>manage_helpdesk" class="btn btn-default m-t-5"><i class="ti-back-left"></i> Reset</a>
											<?php
										}
										?>
									</div>
								</div>

							</form>
						</div>

					</div>
				</div>

			</div>

			<div class="row">

			<div class="col-md-12">
			<div class="white-box" style="border-left: solid 3px #2FBDC8">
					<div class="row">
						<div class="col-md-2 col-sm-2 text-center b-r" style="min-height:70px;">
						<i class="icon-flag"style="font-size: 50px;color:#2FBDC8"></i> 
						</div>
						<div class="col-md-10 col-sm-10">
							<div class="row b-b">
							<div class="col-md-12 text-center" style="color: #2FBDC8">
								<b>Status Helpdesk</b>
							</div>
							</div>
						<div class="row">
							<div class="col-md-4 text-center b-r">
								<h3 class="box-title m-b-0"><?= number_format($total) ;?></h3>
								<a style="color: #2FBDC8" href="<?= base_url();?>manage_helpdesk">Total Helpdesk</a>
							</div>
							<div class="col-md-4 text-center b-r">
								<h3 class="box-title m-b-0"><?= number_format($buka) ;?></h3>
								<a style="color: #2FBDC8" href="<?= base_url();?>manage_helpdesk?status=Buka">Buka</a>
							</div>
							<div class="col-md-4 text-center b-r ">
								<h3 class="box-title m-b-0"><?= number_format($tutup) ;?></h3>
								<a style="color: #2FBDC8" href="<?= base_url();?>manage_helpdesk?status=Tutup">Tutup</a>
							</div>
							

						
						</div>
						</div>
					</div>
				</div>
			</div>

			</div>


			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="x_content">

						<?php foreach($helpdesk as $l){
							
							?>

							<div class="col-md-4 col-sm-6">
								<div class="white-box">
									<div class="row b-b" style="min-height: 120px;" >
										<div class="col-md-4 col-sm-4 text-center b-r" style="min-height: 120px;padding: 5%">
											<i class="icon-flag" style="font-size: 50px;color:#2FBDC8"></i> 
											
										</div>
										<div class="col-md-8 col-sm-8">
											<br>
											<h3 class="box-title m-b-0"><?=$l->judul?></h3>
											
											
										</div>
									</div>


									<div class="row b-b">
										<div class="col-md-6 b-r text-center" style="padding-bottom: 20px;">
											<h3 class="box-title m-b-0">Status</h3>
											<?php if ($l->status == "Buka") {
												$status = "success";
											}else{
												$status = "danger";
											} ?>
											<div class="label label-table label-<?=$status?>"><?=$l->status?></div>

										</div>

									
										<div class="col-md-6 text-center">
											<h3 class="box-title m-b-0">Kategori</h3>
											<?=$l->nama_kategori_helpdesk?>
										</div>
									</div>

									<div class="row">
										<div class="col-md-12">
											<br>
											<address>
												<a href="<?php echo base_url();?>manage_helpdesk/view/<?=$l->id_helpdesk?>">
													<button class="fcbtn btn btn-primary btn-outline btn-1b btn-block">Detail helpdesk</button>
												</a>
											</address>
										</div>
									</div>

								</div>
							</div>
							<?php } ?>




							<!-- /.col -->
						</div>




						<div class="row">
							<div class="col-md-12 pager">
								<?php 
								if(!$filter){
									echo make_pagination($pages,$current);
								}
								?>
							</div>
						</div>




					</div>

				</div>
