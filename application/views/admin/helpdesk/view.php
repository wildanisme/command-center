	<script>
		$(document).ready(function () {
	// create a tree
	$("#tree-data").jOrgChart({
		chartElement: $("#tree-view"),
		nodeClicked: nodeClicked
	});

	// lighting a node in the selection
	function nodeClicked(node, type) {
		node = node || $(this);
		$('.jOrgChart .selected').removeClass('selected');
		node.addClass('selected');
	}
});
</script>
<style type="text/css">
	.nav-tabs>li{
		width: 50%;
		text-align: center;
		text-transform: uppercase;
	}
	.customtab li.active a, .customtab li.active a:focus, .customtab li.active a:hover {
		border-bottom: 2px solid #6003C8;
		color: #6003C8;
	}
	.nav-tabs>li>a{
		border-radius: 0px;
	}
</style>
<div class="container-fluid">

	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<?php
				if (!empty($message)){
					?>
					<div class="alert alert-<?= $type;?> alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?= $message;?>
					</div>
					<?php }?> 
				<div class="x_panel">
					<div class="x_content">
						<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
							<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<label id='status'></label>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="panel panel-default">
									<div class="panel-heading">Infromasi Pengadu</div>
									<div class="panel-body">
										<div class="row">
											
											<?php 
											
											if ($detail->anonymous == "Y") { ?>
												ANONYMOUS
											<?php }else{  ?>
																							<div class="col-md-12 centered">
												<div class="form-group">
													<center>
														<img src="<?php echo base_url();?>data/user_picture/useravatar.png" alt="user" class="img-responsive" style="width: 200px;">
													</center>
												</div>
											</div>
											
											<div class="form-group">
												<label for="exampleInputuname">Nama Pengadu</label>
												<div class="input-group">
													<?=$detail->full_name?>
												</div>
											</div>

											<div class="form-group">
												<label for="exampleInputuname">NIK</label>
												<div class="input-group">
													<?=$detail->username?>
												</div>
											</div>

											<div class="form-group">
												<label for="exampleInputuname">Email</label>
												<div class="input-group">
													<?=$detail->email?>
												</div>
											</div>

											<div class="form-group">
												<label for="exampleInputuname">Telepon</label>
												<div class="input-group">
													<?=$detail->phone?>
												</div>
											</div>
											<?php }
											
											?>


										</div>
										
									</div>


								</div>


							</div>
							<div class="col-md-8">
								<?php if($detail->status == "Tutup") { 

									echo "
									<div class='alert alert-danger'>
										Pengaduan ini  <strong>$detail->status</strong> 
									</div>";
								}
								else { 
									echo "
									<div class='alert alert-success'>
										Pengaduan ini  <strong>$detail->status</strong> 
									</div>";
								} ?>

								<div class="panel panel-default">
									<div class="panel-heading">
										Detail Pengaduan


									</div>
									<div class="panel-body">
										<div class="row">

											<div class="col-md-12">
												<div class="form-group">
													<label>Kategori</label>
													<p><?=$detail->nama_kategori_helpdesk?></p>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label>Judul </label>
													<p><?=$detail->judul?></p>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label>Deskripsi </label>
													<p><?=$detail->deskripsi?></p>
												</div>
											</div>

											<div class="col-md-12">
												<div class="form-group">
													<label>Lampiran </label>
													<?php 
													if ($detail->file !=null) { ?>
														<p><a href="<?=base_url('data/helpdesk/'.$detail->file.'');?>" download>Download</a></p>
													<?php }else{
														echo 'Tidak ada lampiran';
													}
													?>
													
												</div>
											</div>

										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
													
					<div class="white-box">
						<div class="chat-right-aside">
                        <div class="chat-main-header">
                            <div class="p-20 b-b">
                                <h3 class="box-title">Respons</h3>
                            </div>
                        </div>
                        <div class="chat-box">
                            <ul class="chat-list slimscroll p-t-30">
							<?php 
							if ($respons == null) {
								echo '<br>Tidak ada respons';
							}
								foreach ($respons as $respon) {
									$odd = "odd";
									if ($respon->id_user != $id_user) {
										$odd = "";
									}
									?>
									<br>
								<li class="<?=$odd?>">
                                    <div class="chat-image"> <img alt="male" src="<?=base_url('data/user_picture/'.$respon->user_picture.'')?>"> </div>
                                    <div class="chat-body">
                                        <div class="chat-text">
                                            <h4>
											<?php 
											if ($detail->anonymous == "Y" && $respon->id_user != $id_user) {
												echo "ANONYOMUS";
											}else{
												echo $respon->full_name;
											}
											?>
											</h4>
                                            <p><?=$respon->isi?></p>
                                            <b><?=tanggal(date('Y-m-d', strtotime($respon->tgl_respon)))?>&nbsp;<?=date('H:i', strtotime($respon->tgl_respon));?></b> 
																					<?php 
										if ($respon->id_user == $id_user && $detail->status != "Tutup") { ?>
										<br>	
										<a href="" data-toggle="modal" data-target="#edit_respons<?=$respon->id_respon?>" style="color:white;"><small>Edit</small> </a> |
										<small><a href="#" onclick="deleterespons_(<?=$detail->id_helpdesk;?>,<?=$respon->id_respon;?>)" style="color:red;"> Delete</a> </small>
										<?php }
										?></div>
                                    </div>
                                </li>
								<div class="modal fade" id="edit_respons<?=$respon->id_respon?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Edit Respons</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form method="post">
										<div class="form-group">
											<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
											<input type="hidden" name="id_respon" value="<?=$respon->id_respon?>">
											<textarea name="isi" id="" class="form-control" cols="30" rows="2"><?=$respon->isi?></textarea>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" name="update_respons" class="btn btn-primary">Simpan</button>
										</form>
									</div>
									</div>
								</div>
								</div>
							<?php	}
							?>
                            </ul>
							<?php 
							if ($detail->status == "Buka") { ?>
							<div class="row send-chat-box">
                                <div class="col-sm-12">
								<form method="post">
								<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                                    <textarea class="form-control" name="isi" placeholder="Type your message" required></textarea>
                                    <div class="custom-send">
									<br>
                                        <button class="btn btn-danger btn-rounded" type=submit" name="submit_respons">Send</button>
                                    </div>
                                </div>
								</form>
                            </div>
							<?php }
							?>

                        </div>
                    </div>
					
                    <!-- .chat-right-panel -->
                </div>
				<?php 
				if ($detail->status != "Tutup") { ?>
				<form method="POST">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
				<button type="submit" name="tutup_laporan" class="btn btn-xs btn-danger btn-rounded" style="float:right;background-color:red;">Tutup Laporan</button>
				</form>
				<?php }
				?>
						  <script>
			function deleterespons_(id_helpdesk,id_respon)
			{
			swal({
				title: "Apakah anda yakin akan menghapus tanggapan?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Ya',
				cancelButtonText: "Tidak",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm){

			if (isConfirm){
				window.location = "<?= base_url();?>manage_helpdesk/deleterespons/"+id_helpdesk+'/'+id_respon;

				} 
			});
			}
		</script>
					




