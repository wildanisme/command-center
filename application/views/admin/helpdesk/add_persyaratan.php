<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title"><?php echo title($title) ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
				<ol class="breadcrumb">
					<?php echo breadcrumb($this->uri->segment_array()); ?>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
				if (!empty($message)){
					?>
					<div class="alert alert-<?= $type;?> alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						</button>
						<?= $message;?>
					</div>
					<?php }?>
					<div class="x_panel">
						<form method='post' enctype="multipart/form-data" >
							<div class="x_content">
								<div class="alert alert-danger alert-dismissible fade in" role="alert" id='pesan' style='display:none'>
									<button type="button" onclick='hideMe()' class="close" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									<label id='status'></label>
								</div>
								<div class="row">
									<div class="col-md-3">
										<div class="panel panel-default">
											<div class="panel-heading">Foto Layanan</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<input type="file" id="input-file-now" name="foto_layanan" class="dropify" />
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
									<div class="col-md-9">	
										<div class="panel panel-default">
											<div class="panel-heading">
												Detail Layanan
											</div>
											<div class="panel-body">
												<div class="row">

													<div class="col-md-12">
														<div class="form-group">
															<label>Nama SKPD</label>
															<select name="id_skpd" class="form-control form-control-line select2">
				                                                     <?php 
				                                                foreach ($skpd as $row) {
				                                                    echo "<option value='$row->id_skpd'>$row->nama_skpd</option>";
				                                                }
				                                            ?>
				                                                </select>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Loket Layanan </label>
															<select name="loket" class="form-control select2">
															 <?php foreach(range('A','Z') as $v){ ?>
															 	<option value="<?=$v?>"><?=$v?> </option>
															 <?php } ?>

															 </select>

														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>Nama Layanan </label>
															<input type="text" name="nama_layanan" class="form-control" placeholder="Masukkan Nama Layanan">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label>Dekripsi Layanan</label>
															<textarea class="form-control" name="deskripsi_layanan"></textarea>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															<label>SOP Layanan</label>
															<textarea class="form-control" name="deskripsi_layanan"></textarea>
														</div>
													</div>




													


												</div>
											</div>
										</div>  

							



										<button type='submit' class='btn btn-primary pull-right' style="margin-left: 6px;">Submit</button> 
										<a href='<?= base_url();?>ref_layanan' class='btn btn-default pull-right'>Back</a>
										


									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<script>

					function hideMe()
					{$('#pesan').hide();}

					
				</script>