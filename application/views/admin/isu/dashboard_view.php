
    <!--buat peta-->
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/ol.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/fontawesome-all.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/ol-layerswitcher.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web.css') ?>">

    <!--buat map-->
    <style>
        #map {
            width: 853px;
            height: 615px;
        }
    </style>

                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(53, 198, 237, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/positif') ?>">
                                                    <h4>Positif</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $positif->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(148, 148, 148, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/netral') ?>">
                                                    <h4>Netral</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $netral->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(255, 77, 77, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/negatif') ?>">
                                                    <h4>Negatif</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $negatif->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl- col-md-8 box-col-8">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fas fa-chart-bar mr-1"></i>
                                    Statistik Sentimen Isu Media
                                </div>
                                <div class="card-body p-0">
                                    <div id="column-chart1"></div>
                                </div>
                                <!-- <div class="card-header">
                                    <i class="fa fa-table"></i>
                                    Top Isu Wilayah
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr>

                                                <th>Kecamatan</th>
                                                <th>Pos</th>
                                                <th>Net</th>
                                                <th>Negatif</th>


                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kec as $kec) : ?>
                                                <tr>

                                                    <td>
                                                        <?php echo $kec->kecamatan ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalpos ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalnet ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalneg ?>
                                                    </td>
                                                <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div> -->
                            </div>
                        </div>

                        <!-- <div class="col-xl-6 col-md-4 box-col-4">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="icofont icofont-ui-map"></i>
                                    Info Grafis Isu Media
                                </div>

                                <div class="map-js-height" id="map">
                                    <div id="popup" class="ol-popup">
                                        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                                        <div id="popup-content"></div>
                                    </div>
                                </div>

                            </div>
                        </div> -->

                        <div class="col-xl-3 col-md-4 box-col-4">
                            <div class="card mb-4" height="615px">
                                <div class="card-header">
                                    <i class="fa fa-table"></i>
                                    Top Kategori Isu Media
                                </div>

                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(53, 198, 237, 1) ;">
                                                <th>Kategori</th>
                                                <th>positif</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategoripos as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(148, 148, 148, 1);">

                                                <th>Kategori</th>
                                                <th>netral</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategorinet as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(255, 77, 77, 1);">

                                                <th>Kategori</th>
                                                <th>negatif</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategorineg as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Perbandingan Asal Berita </h5>
                                </div>
                                <div class="card-body apex-chart p-0">
                                    <div id="piechart1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>perbandingan berita dari media </h5>
                                </div>
                                <div class="card-body apex-chart p-0">
                                    <div id="piechart2"></div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="row">
                        <div class="col-xl-12 xl-80 box-col-12">
                            <div class="card">
                                <div class="card-header no-border">
                                    <h2>Berita Terkini</h2>
                                    
                                </div>
                                   <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head3" title="Copy">
                                            <i class="icofont icofont-copy-alt"></i>
                                        </button>
                                        <pre><code class="language-html" id="example-head3"></code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
$url = "https://pancakarsa.bogorkab.go.id/siber-remote";

$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

$options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
          );

$ch      = curl_init( $url );
curl_setopt_array( $ch, $options );
$content = curl_exec( $ch );
$err     = curl_errno( $ch );
$errmsg  = curl_error( $ch );
$header  = curl_getinfo( $ch );
curl_close( $ch );

$header['errno']   = $err;
$header['errmsg']  = $errmsg;
$header['content'] = $content;
// echo $header['content'];

?>
<script type="text/javascript">
  var token = "MzQ5MzhhNjc1MmI2MDBjOWQyODYyZWUzZjI2OGZhNDUwY2NiMTQxZjdmYjAxZTFjODRlN2RhYWI5MzU5NWQ3";
  var xhr = new XMLHttpRequest();

  xhr.open('GET', 'https://pancakarsa.bogorkab.go.id/siber-remote');
  xhr.onreadystatechange = handler;
  xhr.responseType = 'blob';
  xhr.setRequestHeader('Authorization', 'token_auth ' + token);
  xhr.send();

  function handler() {
    if (this.readyState === this.DONE) {
      if (this.status === 200) {
        // this.response is a Blob, because we set responseType above
        var data_url = URL.createObjectURL(this.response);
        document.querySelector('#frame').src = data_url;
      } else {
        console.error('no pdf :(');
      }
    }
  }
</script>

<iframe id="frame" src="https://pancakarsa.bogorkab.go.id/siber-remote" frameborder="0" gesture="media" allow="encrypted-media" allowtransparency="true" allowfullscreen="true" style="width: 100%; min-height: 720px; height: max-content;"></iframe>

<script src="<?=base_url();?>asset/dashboard/js/hide-on-scroll.js" defer></script>
                    </div>
                </div>
                <!-- Container-fluid Ends-->


    <!--buat peta-->
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web_expressions.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/polyfills.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/functions.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/ol.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/ol-layerswitcher.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/layers/sumedang26_0.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/styles/sumedang26_0_style.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/layers/layers.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/Autolinker.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web.js') ?>"></script>
    <!--buat table peta-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-core.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-service.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-ui.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-mapevents.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/custom.js') ?>"></script>
    <script>
        // column chart
        var options = {
            chart: {
                height: 200,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: 'rounded',
                    columnWidth: '55%',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{
                name: 'Positif',
                data: [<?php echo $positif->num_rows(); ?>]
            }, {
                name: 'netral',
                data: [<?php echo $netral->num_rows(); ?>]
            }, {
                name: 'negatif',
                data: [<?php echo $negatif->num_rows(); ?>]
            }],

            colors: ['#35c6ed', '#949494', '#ff4d4d']
        }

        var chart3 = new ApexCharts(
            document.querySelector("#column-chart1"),
            options
        );

        chart3.render();
    </script>

    <script>
        var size = 0;
        var placement = 'point';

        function categories_sumedang26_0(feature, value, size, resolution, labelText,
            labelFont, labelFill, bufferColor, bufferWidth,
            placement) {
            switch (value.toString()) {
                case '1':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: <?php foreach ($kecsumsel as $kec1) : ?>
                            <?php


                                        if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                            echo "'rgba(53, 198, 237, 1)'";
                                        } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                            echo "'rgba(255, 77, 77, 1)'";
                                        } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } else {
                                            echo "'rgba(255, 255, 255, 1)'";
                                        }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '2':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:


                                <?php foreach ($kecsukasari as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>



                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '3':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: <?php foreach ($kecsurian as $kec1) : ?>
                            <?php


                                        if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                            echo "'rgba(53, 198, 237, 1)'";
                                        } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                            echo "'rgba(255, 77, 77, 1)'";
                                        } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } else {
                                            echo "'rgba(255, 255, 255, 1)'";
                                        }
                            ?>


                        <?php endforeach; ?>



                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '4':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecsumut as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '5':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungmedar as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '6':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungkerta as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '7':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectomo as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '8':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungsari as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '9':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecwado as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '10':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecujungjaya as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '11':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccibugel as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '12':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecbuahdua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '13':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecbuahdua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '14':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccimanggung as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '15':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecconggeang as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '16':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccimalaka as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '17':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatigede as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '18':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccisitu as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '19':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecganeas as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '20':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccisarua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '21':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatinunggal as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '22':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatinangor as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '23':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecpaseh as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '24':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecpamulihan as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '25':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecsituraja as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '26':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecrancakalong as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case 'NULL':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(234,76,176,1.0)'
                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
            }
        };

        var style_sumedang26_0 = function(feature, resolution) {
            var context = {
                feature: feature,
                variables: {}
            };
            var value = feature.get("id");
            var labelText = "";
            size = 0;
            var labelFont = "10px, sans-serif";
            var labelFill = "#000000";
            var bufferColor = "";
            var bufferWidth = 0;
            var textAlign = "left";
            var offsetX = 8;
            var offsetY = 3;
            var placement = 'point';
            if ("" !== null) {
                labelText = String("");
            }

            var style = categories_sumedang26_0(feature, value, size, resolution, labelText,
                labelFont, labelFill, bufferColor,
                bufferWidth, placement);

            return style;
        };
    </script>

    <script>
        // pie chart
        var options8 = {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['Lokal', 'Regional', 'internasional'],
            series: [<?php echo $lokal->num_rows(); ?>, <?php echo $regional->num_rows(); ?>, <?php echo $intern->num_rows(); ?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart8 = new ApexCharts(
            document.querySelector("#piechart1"),
            options8
        );

        chart8.render();
    </script>

    <script>
        // pie chart
        var options8 = {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['internet', 'tv', 'radio'],
            series: [<?php echo $medinternet->num_rows(); ?>, <?php echo $medtv->num_rows(); ?>, <?php echo $medradio->num_rows(); ?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart8 = new ApexCharts(
            document.querySelector("#piechart2"),
            options8
        );

        chart8.render();
    </script>

</body>

</html>