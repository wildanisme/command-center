<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon">
    <title>Isu Media - Tambah Berita</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/fontawesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/icofont.css')?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/themify.css')?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/flag-icon.css')?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/feather-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/animate.css')?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/chartist.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/date-picker.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/prism.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/material-design-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/pe7-icon.css')?>">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/bootstrap.css')?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/style.css')?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('temadmin/tempadmin/assets/css/color-1.css" media="screen')?>">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('temadmin/tempadmin/assets/css/responsive.css')?>">
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>New Era Admin Loading..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
	  <div class="page-main-header">
        <div class="main-header-right">
          <div class="main-header-left text-center">
            <div class="logo-wrapper"><a href="index.html"><img src="../assets/images/logo/logo.png" alt=""></a></div>
          </div>
          <div class="mobile-sidebar">
            <div class="media-body text-right switch-sm">
              <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
            </div>
          </div>
          <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar">               </i></div>
          <div class="nav-right col pull-right right-menu">
            <ul class="nav-menus">
              <li>
                <form class="form-inline search-form" action="#" method="get">
                  <div class="form-group">
                    <div class="Typeahead Typeahead--twitterUsers">
                      <div class="u-posRelative">
                        <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Search Your Product...">
                        <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                      </div>
                      <div class="Typeahead-menu"></div>
                    </div>
                  </div>
                </form>
              </li>
              <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="../assets/images/dashboard/bookmark.png" alt="">
                <div class="onhover-show-div bookmark-flip">
                  <div class="flip-card">
                    <div class="flip-card-inner">
                      <div class="front">
                        <ul class="droplet-dropdown bookmark-dropdown">
                          <li class="gradient-primary text-center">
                            <h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
                          </li>
                          <li>
                            <div class="row">
                              <div class="col-4 text-center"><i data-feather="file-text"></i></div>
                              <div class="col-4 text-center"><i data-feather="activity"></i></div>
                              <div class="col-4 text-center"><i data-feather="users"></i></div>
                              <div class="col-4 text-center"><i data-feather="clipboard"></i></div>
                              <div class="col-4 text-center"><i data-feather="anchor"></i></div>
                              <div class="col-4 text-center"><i data-feather="settings"></i></div>
                            </div>
                          </li>
                          <li class="text-center">
                            <button class="flip-btn" id="flip-btn">Add New Bookmark</button>
                          </li>
                        </ul>
                      </div>
                      <div class="back">
                        <ul>
                          <li>
                            <div class="droplet-dropdown bookmark-dropdown flip-back-content">
                              <input type="text" placeholder="search...">
                            </div>
                          </li>
                          <li>
                            <button class="d-block flip-back" id="flip-back">Back</button>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="../assets/images/dashboard/like.png" alt="">
                <ul class="onhover-show-div droplet-dropdown">
                  <li class="gradient-primary text-center">
                    <h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
                      <div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
                    </div>
                  </li>
                  <li class="text-center">
                    <button class="btn btn-primary btn-air-primary">Follows Up</button>
                  </li>
                </ul>
              </li>
              <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="../assets/images/dashboard/notification.png" alt="">
                <ul class="onhover-show-div notification-dropdown">
                  <li class="gradient-primary">
                    <h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
                  </li>
                  <li>
                    <div class="media">
                      <div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
                      <div class="media-body">
                        <h6>Someone Likes Your Posts</h6>
                        <p class="mb-0"> 2 Hours Ago</p>
                      </div>
                    </div>
                  </li>
                  <li class="pt-0">
                    <div class="media">
                      <div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
                      <div class="media-body">
                        <h6>3 New Comments</h6>
                        <p class="mb-0"> 1 Hours Ago</p>
                      </div>
                    </div>
                  </li>
                  <li class="bg-light txt-dark"><a href="#">All </a> notification</li>
                </ul>
              </li>
              <li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="../assets/images/dashboard/chat.png" alt=""></a></li>
              <li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="../assets/images/dashboard/user.png" alt=""></span>
                <ul class="onhover-show-div profile-dropdown">
                  <li class="gradient-primary">
                    <h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
                  </li>
                  <li><i data-feather="user"> </i>Profile</li>
                  <li><i data-feather="message-square"> </i>Inbox</li>
                  <li><i data-feather="file-text"> </i>Taskboard</li>
                  <li><i data-feather="settings"> </i>Settings            </li>
                </ul>
              </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
          </div>
          <script id="result-template" type="text/x-handlebars-template">
            <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
          <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
        </div>
      </div>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      
        <!-- Page Sidebar Start-->

        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <!-- Right sidebar Ends-->
        <div class="page-body-wrapper">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6 main-header">
                  <h2>Dashboard<span>Isu Media </span></h2>
                  <h6 class="mb-0">Kab. Bogor</h6>
                </div>
                <div class="col-lg-6 breadcrumb-right">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url('adminbaru') ?>"><i class="pe-7s-home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
             <div class="row">
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(53, 198, 237, 1);">
								<div class="align-self-center text-center" >
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Positif</span>
									<h4 class="mb-0  text-white"><?php echo $positif->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(148, 148, 148, 1);">
								<div class="align-self-center text-center">
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Netral</span>
									<h4 class="mb-0  text-white"><?php echo $netral->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(255, 77, 77, 1);">
								<div class="align-self-center text-center">
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Negatif</span>
									<h4 class="mb-0  text-white"><?php echo $negatif->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			 </div>
			 <div class="row">
				<div class="col-xl-4 col-md-6 box-col-4">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
						<div class = "card-header">
							<i class="fas fa-chart-bar mr-1"></i>
                            Statistik Sentimen Isu Media
						</div>
						<div class="card-body p-0">
							<div id="column-chart1"></div>
						</div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="card">
						<div class="card-header">
							<i class="fa fa-table"></i>
                            Top Isu Wilayah
                        </div>
						<div class="table-responsive">
                                            <table class="table" width="100%" cellspacing="0">
												<thead class="big-primary">
													<tr>
                                              
														<th>Kecamatan</th>
														<th>Positif</th>
														<th>Netral</th>
														<th>Negatif</th>
                                                
                                             
													</tr>
												</thead>
                                        
												<tbody>
													<?php foreach ($kec as $kec): ?>
													<tr>
                                                
														<td>
															<?php echo $kec->kecamatan ?>
														</td>
														<td>
															<?php echo $kec->totalpos ?>
														</td>
														<td>
															<?php echo $kec->totalnet ?>
														</td>
														<td>
															<?php echo $kec->totalneg ?>
														</td>
													<?php endforeach; ?>
                                            
												</tbody>
                                            </table>
						</div>
                    </div>
                  </div>
                </div>
			 </div>
				<div class = "col-xl-8 col-md-12">
					
					<div class = "card">
						<div class="card-header">
                        <h5>Tambah berita</h5>
						</div>
						
						<form class="form theme-form" action="" method="post" enctype="multipart/form-data" >
						
									<div class="form-group">
										<label class="col-form-label" for="name">tanggal</label>
										<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
										type= date name="tanggal" placeholder="tanggal" value="<?php echo $berita->tanggal ?>"/>
										<div class="invalid-feedback">
											<?php echo form_error('tanggal') ?>
										</div>
									</div>

									<div class="form-group">
										<label class="col-form-label" for="name">Judul</label>
										<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
										type="text" name="  judul" placeholder="Judul Berita" value="<?php echo $berita->judul ?>" />
										<div class="invalid-feedback">
											<?php echo form_error('judul') ?>
										</div>
									</div>

									<div class="form-group">
										<label class="col-form-label" for="name">Deskripsi</label>
										<textarea class="form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>"
										name="deskripsi" placeholder="Deskripsi Berita"></textarea>
										<div class="invalid-feedback">
											<?php echo form_error('deskripsi') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">Kecamatab</label>
										<select name="kecamatan" id="kecamatan" class="form-control <?php echo form_error('kecamatan') ? 'is-invalid':'' ?>"
										type="text">
											<?php foreach ($kecamatan1 as $kecamatan): ?>
												<option value="<?php echo $kecamatan->nama_kecamatan ?>"><?php echo $kecamatan->nama_kecamatan ?></option>
											<?php endforeach; ?>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('kecamatan') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">KAtegori</label>
										<select name="kategori" id="kategori" class="form-control <?php echo form_error('kategori') ? 'is-invalid':'' ?>"
										type="text">
											<?php foreach ($kategori1 as $kategori): ?>
												<option value=" 
													<?php echo $kategori->kategori ?>
													">
													<?php echo $kategori->kategori ?>
												</option>
                                             <?php endforeach; ?>
										</select>
										<div class="invalid-feedback">
										<?php echo form_error('kategori') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">Sentimen</label>
										<select name="sentimen" id="sentimen" class="form-control <?php echo form_error('sentimen') ? 'is-invalid':'' ?>"
										type="text">
											<option value="positif">Positif</option>
											<option value="netral">Netral</option>
											<option value="negatif">Negatif</option>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('sentimen') ?>
										</div>
									</div>

							<input class="btn btn-primary btn-pill" type="submit" name="btn" value="Save" />
					</form>
					</div>
				</div>
			 </div>
				
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
      
    </div>
    <!-- latest jquery-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/jquery-3.5.1.min.js')?>"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/bootstrap/popper.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/bootstrap/bootstrap.js')?>"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/icons/feather-icon/feather.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/icons/feather-icon/feather-icon.js')?>"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/sidebar-menu.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/config.js')?>"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/typeahead/handlebars.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/typeahead/typeahead.bundle.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/typeahead/typeahead.custom.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/typeahead-search/handlebars.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/typeahead-search/typeahead-custom.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/chartist/chartist.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/chartist/chartist-plugin-tooltip.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/prism/prism.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/clipboard/clipboard.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/counter/jquery.waypoints.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/counter/jquery.counterup.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/counter/counter-custom.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/custom-card/custom-card.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/dashboard/default.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/notify/index.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/datepicker/date-picker/datepicker.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/datepicker/date-picker/datepicker.en.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/datepicker/date-picker/datepicker.custom.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chat-menu.js')?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>
    <!-- login js-->
    <!-- Plugin used-->

	<!-- tabel top-->
	<script src="<?php echo base_url('temadmin/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>
		


	<!-- Theme js-->
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('/assets/js/theme-customizer/customizer.js')?>"></script>

	<!--buat chart-->
	<script src="<?php echo base_url('temadmin/tempadmin/assets/assets/js/chart/chartjs/chart.min.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/assets/js/chart/chartjs/chart.custom.js')?>"></script>
	<script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chart/apex-chart/chart-custom.js')?>"></script>
    <script src="<?php echo base_url('temadmin/tempadmin/assets/js/chat-menu.js')?>"></script>


	<script>
		// column chart
var options = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Positif',
        data: [<?php echo $positif->num_rows();?>]
    }, {
        name: 'Negatif',
        data: [<?php echo $negatif->num_rows();?>]
    }, {
        name: 'netral',
        data: [<?php echo $netral->num_rows();?>]
    }],

    colors:['#35c6ed', '#949494', '#ff4d4d']
}

var chart3 = new ApexCharts(
    document.querySelector("#column-chart1"),
    options
);

chart3.render();
	</script>

  </body>
</html>
