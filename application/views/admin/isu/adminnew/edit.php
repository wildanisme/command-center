<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/favicon.png')?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/favicon.png')?>" type="image/x-icon">
    <title>Poco - Premium Admin Template</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/fontawesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/icofont.css')?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/themify.css')?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/flag-icon.css')?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/feather-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/animate.css')?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/chartist.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/date-picker.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/prism.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/material-design-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css')?>">
    <!-- Plugins css Ends-->

	<!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/datatables.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css')?>">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/bootstrap.css')?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/style.css')?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/color-1.css')?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/responsive.css')?>">

  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>New Era Admin Loading..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
	  <?php $this->load->view("admin/isu/adminnew/partial/header");?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <?php $this->load->view("admin/isu/adminnew/partial/sidebar")?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <?php $this->load->view("admin/isu/adminnew/partial/rightsidebarstart");?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6 main-header">
                  <h2>Isu Media<span>Edit Berita </span></h2>
                  <h6 class="mb-0">Kab. Bogor</h6>
                </div>
                <div class="col-lg-6 breadcrumb-right">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Default  </li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
             <div class="row">
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(53, 198, 237, 1);">
								<div class="align-self-center text-center" >
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Positif</span>
									<h4 class="mb-0  text-white"><?php echo $positif->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(148, 148, 148, 1);">
								<div class="align-self-center text-center">
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Netral</span>
									<h4 class="mb-0  text-white"><?php echo $netral->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="card gradient-warning o-hidden">
						<div class="b-r-4 card-body">
							<div class="media static-top-widget" style="background-color: rgba(255, 77, 77, 1);">
								<div class="align-self-center text-center">
									<div class="text-white i" data-feather="database"></div>
								</div>
								<div class="media-body"><span class="m-0 text-white">Negatif</span>
									<h4 class="mb-0  text-white"><?php echo $negatif->num_rows();?></h4><i class="icon-bg" data-feather="database"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			 </div>
			 <div class="row">
				<div class="col-xl-4 col-md-6 box-col-4">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card">
						<div class = "card-header">
							<i class="fas fa-chart-bar mr-1"></i>
                            Statistik Sentimen Isu Media
						</div>
						<div class="card-body p-0">
							<div id="column-chart1"></div>
						</div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="card">
						<div class="card-header">
							<i class="fa fa-table"></i>
                            Top Isu Wilayah
                        </div>
						<div class="table-responsive">
                                            <table class="table" width="100%" cellspacing="0">
												<thead class="big-primary">
													<tr>
                                              
														<th>Kecamatan</th>
														<th>Positif</th>
														<th>Netral</th>
														<th>Negatif</th>
                                                
                                             
													</tr>
												</thead>
                                        
												<tbody>
													<?php foreach ($kec as $kec): ?>
													<tr>
                                                
														<td>
															<?php echo $kec->kecamatan ?>
														</td>
														<td>
															<?php echo $kec->totalpos ?>
														</td>
														<td>
															<?php echo $kec->totalnet ?>
														</td>
														<td>
															<?php echo $kec->totalneg ?>
														</td>
													<?php endforeach; ?>
                                            
												</tbody>
                                            </table>
						</div>
                    </div>
                  </div>
                </div>
			 </div>
				<div class = "col-xl-8 col-md-12">
					
					<div class = "card">
						<div class="card-header">
                        <h5>Tambah berita</h5>
						</div>
						
						<form class="form theme-form" action="" method="post" enctype="multipart/form-data" >
 							<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
							<input type="hidden" name="id" value="<?php echo $berita->berita_id?>" />
									<div class="form-group">
										<label class="col-form-label" for="name">tanggal</label>
										<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
										type= date name="tanggal" placeholder="tanggal" value="<?php echo $berita->tanggal ?>"/>
										<div class="invalid-feedback">
											<?php echo form_error('tanggal') ?>
										</div>
									</div>

									<div class="form-group">
										<label class="col-form-label" for="name">Judul</label>
										<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
										type="text" name="judul" placeholder="Judul Berita" value="<?php echo $berita->judul ?>"/>
										<div class="invalid-feedback">
											<?php echo form_error('judul') ?>
										</div>
									</div>

									<div class="form-group">
										<label class="col-form-label" for="name">Deskripsi</label>
										<textarea class="form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>"
										name="deskripsi" placeholder="Deskripsi Berita"></textarea>
										<div class="invalid-feedback">
											<?php echo form_error('deskripsi') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">Kecamatab</label>
										<select name="kecamatan" id="kecamatan" class="form-control <?php echo form_error('kecamatan') ? 'is-invalid':'' ?>"
										type="text">
											<?php foreach ($kecamatan1 as $kecamatan): ?>
												<option value="<?php echo $kecamatan->nama_kecamatan ?>"><?php echo $kecamatan->nama_kecamatan ?></option>
											<?php endforeach; ?>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('kecamatan') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">KAtegori</label>
										<select name="kategori" id="kategori" class="form-control <?php echo form_error('kategori') ? 'is-invalid':'' ?>"
										type="text">
											<?php foreach ($kategori1 as $kategori): ?>
												<option value=" 
													<?php echo $kategori->kategori ?>
													">
													<?php echo $kategori->kategori ?>
												</option>
                                             <?php endforeach; ?>
										</select>
										<div class="invalid-feedback">
										<?php echo form_error('kategori') ?>
										</div>
									</div>
                            
									<div class="form-group">
										<label class="col-form-label" for="name">Sentimen</label>
										<select name="sentimen" id="sentimen" class="form-control <?php echo form_error('sentimen') ? 'is-invalid':'' ?>"
										type="text">
											<option value="positif">Positif</option>
											<option value="netral">Netral</option>
											<option value="negatif">Negatif</option>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('sentimen') ?>
										</div>
									</div>

                  <div class="form-group">
										<label class="col-form-label" for="name">Sumber</label>
										<textarea class="form-control <?php echo form_error('sumber') ? 'is-invalid':'' ?>"
										name="sumber" placeholder="Sumber Berita" value="<?php echo $berita->media ?>"></textarea>
										<div class="invalid-feedback">
											<?php echo form_error('deskripsi') ?>
										</div>
									</div>

                  <div class="form-group">
										<label class="col-form-label" for="name">Asal</label>
										<select name="asal" id="asal" class="form-control <?php echo form_error('asal') ? 'is-invalid':'' ?>"
										type="text">
											<option value="lokal">Lokal</option>
											<option value="regional">Regional</option>
											<option value="internasional">Internasional</option>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('sentimen') ?>
										</div>
									</div>

                  <div class="form-group">
										<label class="col-form-label" for="name">Media</label>
										<select name="media" id="media" class="form-control <?php echo form_error('media') ? 'is-invalid':'' ?>"
										type="text">
											<option value="tv">TV</option>
											<option value="internet">Internet</option>
											<option value="radio">Radio</option>
                      <option value>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('sentimen') ?>
										</div>
									</div>

                  <div class="form-group">
										<label class="col-form-label" for="name">status</label>
										<select name="status" id="asal" class="form-control <?php echo form_error('status') ? 'is-invalid':'' ?>"
										type="text">
											<option value="belum diproses">belum diproses</option>
											<option value="sedang diproses">sedang diproses</option>
											<option value="selesai diproses">selesai diproses</option>
                      <option value="ditolak">ditolak</option>
										</select>
										<div class="invalid-feedback">
											<?php echo form_error('sentimen') ?>
										</div>
									</div>

							<input class="btn btn-primary btn-pill" type="submit" name="btn" value="Save" />
					</form>
					</div>
				</div>
			 </div>
				
		  </div>
          <!-- Container-fluid Ends-->
          <div class="welcome-popup modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-body">
                  <div class="modal-header"></div>
                  <div class="contain p-30">
                    <div class="text-center">
                      <h3>Welcome to creative admin</h3>
                      <p>start your project with developer friendly admin </p>
                      <button class="btn btn-primary btn-lg txt-white" type="button" data-dismiss="modal" aria-label="Close">Get Started</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/jquery-3.5.1.min.js')?>"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/popper.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/bootstrap.js')?>"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather-icon.js')?>"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/sidebar-menu.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/config.js')?>"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/handlebars.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.bundle.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/handlebars.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/typeahead-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist-plugin-tooltip.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/prism/prism.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/clipboard/clipboard.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.waypoints.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.counterup.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/counter-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/custom-card/custom-card.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/dashboard/default.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/index.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.en.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>
    <!-- login js-->
    <!-- Plugin used-->
	<!--icon-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icons-notify.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icon-clipart.js')?>"></script>
	<!-- tabel top-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>

	<!--js table bawah-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/datatable.custom.js')?>"></script>
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>

	<!-- Theme js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('/assets/js/theme-customizer/customizer.js')?>"></script>

	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.custom.js')?>"></script>

	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/chart-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>

		<script>

// column chart
var options = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Positif',
        data: [<?php echo $positif->num_rows();?>]
    }, {
        name: 'Negatif',
        data: [<?php echo $negatif->num_rows();?>]
    }, {
        name: 'netral',
        data: [<?php echo $netral->num_rows();?>]
    }],

    colors:['#35c6ed', '#949494', '#ff4d4d']
}

var chart3 = new ApexCharts(
    document.querySelector("#column-chart1"),
    options
);

chart3.render();
	</script>

  </body>
</html>
