<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/favicon.png')?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/favicon.png')?>" type="image/x-icon">
    <title>Aplikasi Isu Media</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/fontawesome.css')?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/icofont.css')?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/themify.css')?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/flag-icon.css')?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/feather-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/animate.css')?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/chartist.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/date-picker.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/prism.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/material-design-icon.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css')?>">
    <!-- Plugins css Ends-->

	<!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/datatables.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css')?>">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/bootstrap.css')?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/style.css')?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/color-1.css')?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/responsive.css')?>">
	<!--buat peta-->
	<link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('/26kec/resources/fontawesome-all.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol-layerswitcher.css')?>">
	<link rel="stylesheet" href="<?php echo base_url('./resources/qgis2web.css')?>">

	<!--buat map-->
	<style>
        #map {
            width: 853px;
            height: 441px;
        }
    </style>

  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="typewriter">
        <h1>New Era Admin Loading..</h1>
      </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
      <!-- Page Header Start-->
	  <?php $this->load->view("admin/isu/adminnew/partial/header");?>
      <!-- Page Header Ends                              -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        <?php $this->load->view("admin/isu/adminnew/partial/sidebar");?>
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
		<?php $this->load->view("admin/isu/adminnew/partial/rightsidebarstart");?>
        <!-- Right sidebar Ends-->
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6 main-header">
                  <h2>Daftar Berita<span>Positif </span></h2>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">

			 <div class="row">
                            
                            <div class="col-xl-12">
								<div class="card">
									<div class="card-body">
										
										<div class="table-responsive">
											<table class="display" id = "basic-1">
												<thead>
													<tr>
														<th>Tanggal</th>
														<th>Judul</th>
														<th>Deskripsi</th>
														<th>Kecamatan</th>
														<th>Kategori</th>
														<th>Sentimen</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($berita as $berita): ?>
														<tr>
															<td width="150">
																<?php echo $berita->tanggal ?>
															</td>
															<td>
																<?php echo $berita->judul ?>
															</td>
															<td>
																<?php echo $berita->deskripsi ?>
															</td>
															<td>
																<?php echo $berita->kecamatan ?>
															</td>
															<td>
																<?php echo $berita->kategori ?>
															</td>
															<td>
																<?php echo $berita->sentimen ?>
															</td>
														</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										</div>
									</div>
							</div>
			 </div>
			</div>
          <!-- Container-fluid Ends-->

        </div>
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright © 2021 Poco. All rights reserved.</p>
              </div>
              <div class="col-md-6">
                <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/jquery-3.5.1.min.js')?>"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/popper.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/bootstrap.js')?>"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather-icon.js')?>"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/sidebar-menu.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/config.js')?>"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/handlebars.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.bundle.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/handlebars.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/typeahead-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist-plugin-tooltip.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/prism/prism.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/clipboard/clipboard.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.waypoints.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.counterup.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/counter-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/custom-card/custom-card.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/dashboard/default.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/index.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.en.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>
    <!-- login js-->
    <!-- Plugin used-->
	<!--icon-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icons-notify.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icon-clipart.js')?>"></script>
	<!-- tabel top-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/theme-customizer/customizer.js')?>"></script>

	<!--js table bawah-->
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/datatable.custom.js')?>"></script>
	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>

	<!-- Theme js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js')?>"></script>
    <script src="<?php echo base_url('/assets/js/theme-customizer/customizer.js')?>"></script>

	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.min.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.custom.js')?>"></script>

	<script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/chart-custom.js')?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js')?>"></script>


  </body>
</html>
