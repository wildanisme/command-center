<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/logo/icon.png') ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/images/logo/icon.png') ?>" type="image/x-icon">
    <title>Aplikasi Isu Media</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/fontawesome.css') ?>">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/icofont.css') ?>">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/themify.css') ?>">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/flag-icon.css') ?>">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/feather-icon.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/animate.css') ?>">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/chartist.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/date-picker.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/prism.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/material-design-icon.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css') ?>">
    <!-- Plugins css Ends-->

    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/datatables.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/pe7-icon.css') ?>">

    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/bootstrap.css') ?>">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/style.css') ?>">
    <link id="color" rel="stylesheet" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/color-1.css') ?>" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/custom/isu/tempadmin/assets/css/responsive.css') ?>">
    <!--buat peta-->
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/ol.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/fontawesome-all.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/ol-layerswitcher.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web.css') ?>">

    <!--buat map-->
    <style>
        #map {
            width: 853px;
            height: 615px;
        }
    </style>

</head>

<body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
        <div class="typewriter">
            <h1>New Era Admin Loading..</h1>
        </div>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper">
        <!-- Page Header Start-->
        <?php $this->load->view("admin/isu/adminnew/partial/header"); ?>
        <!-- Page Header Ends                              -->
        <!-- Page Body Start-->
        <div class="page-body-wrapper">
			<div class="iconsidebar-menu iconbar-mainmenu-close">
                <?php $this->load->view('admin/src/menu'); ?>

            </div>
            <!-- Page Sidebar Start-->
            <?php $this->load->view("admin/isu/adminnew/partial/sidebar"); ?>
            <!-- Page Sidebar Ends-->
            <!-- Right sidebar Start-->
            <?php $this->load->view("admin/isu/adminnew/partial/rightsidebarstart"); ?>
            <!-- Right sidebar Ends-->
            <div class="page-body">
                <div class="container-fluid">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-lg-6 main-header">
                                <h2>Dashboard<span>Isu Media</span></h2>
                                <h6 class="mb-0">admin panel</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid starts-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(53, 198, 237, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/positif') ?>">
                                                    <h4>Positif</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $positif->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(148, 148, 148, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/netral') ?>">
                                                    <h4>Netral</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $netral->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="card gradient-warning o-hidden">
                                <div class="b-r-4 card-body">
                                    <div class="media static-top-widget" style="background-color: rgba(255, 77, 77, 1);">
                                        <div class="align-self-center text-center">
                                            <div class="text-white i" data-feather="database"></div>
                                        </div>
                                        <div class="media-body"><span class="m-0 text-white"><a href="<?php echo site_url('isu/negatif') ?>">
                                                    <h4>Negatif</h4>
                                                </a></span>
                                            <h4 class="mb-0  text-white"><?php echo $negatif->num_rows(); ?></h4><i class="icon-bg" data-feather="database"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3 col-md-4 box-col-4">
                            <div class="card">
                                <div class="card-header">
                                    <i class="fas fa-chart-bar mr-1"></i>
                                    Statistik Sentimen Isu Media
                                </div>
                                <div class="card-body p-0">
                                    <div id="column-chart1"></div>
                                </div>
                                <div class="card-header">
                                    <i class="fa fa-table"></i>
                                    Top Isu Wilayah
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr>

                                                <th>Kecamatan</th>
                                                <th>Pos</th>
                                                <th>Net</th>
                                                <th>Negatif</th>


                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kec as $kec) : ?>
                                                <tr>

                                                    <td>
                                                        <?php echo $kec->kecamatan ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalpos ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalnet ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kec->totalneg ?>
                                                    </td>
                                                <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6 col-md-4 box-col-4">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="icofont icofont-ui-map"></i>
                                    Info Grafis Isu Media
                                </div>

                                <div class="map-js-height" id="map">
                                    <div id="popup" class="ol-popup">
                                        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                                        <div id="popup-content"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-xl-3 col-md-4 box-col-4">
                            <div class="card mb-4" height="615px">
                                <div class="card-header">
                                    <i class="fa fa-table"></i>
                                    Top Kategori Isu Media
                                </div>

                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(53, 198, 237, 1) ;">
                                                <th>Kategori</th>
                                                <th>positif</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategoripos as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(148, 148, 148, 1);">

                                                <th>Kategori</th>
                                                <th>netral</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategorinet as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table" width="100%" cellspacing="0">
                                        <thead class="big-primary">
                                            <tr style="background-color: rgba(255, 77, 77, 1);">

                                                <th>Kategori</th>
                                                <th>negatif</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach ($kategorineg as $kategori) : ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $kategori->kategori ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $kategori->total ?>
                                                    </td>
                                                <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Perbandingan Asal Berita </h5>
                                </div>
                                <div class="card-body apex-chart p-0">
                                    <div id="piechart1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>perbandingan berita dari media </h5>
                                </div>
                                <div class="card-body apex-chart p-0">
                                    <div id="piechart2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 xl-100 box-col-12">
                            <div class="card">
                                <div class="card-header no-border">
                                    <h5>Today's Activity</h5>
                                    <ul class="creative-dots">
                                        <li class="bg-primary big-dot"></li>
                                        <li class="bg-secondary semi-big-dot"></li>
                                        <li class="bg-warning medium-dot"></li>
                                        <li class="bg-info semi-medium-dot"></li>
                                        <li class="bg-secondary semi-small-dot"></li>
                                        <li class="bg-primary small-dot"></li>
                                    </ul>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="icofont icofont-gear fa fa-spin font-primary"></i></li>
                                            <li><i class="view-html fa fa-code font-primary"></i></li>
                                            <li><i class="icofont icofont-maximize full-card font-primary"></i></li>
                                            <li><i class="icofont icofont-minus minimize-card font-primary"></i></li>
                                            <li><i class="icofont icofont-refresh reload-card font-primary"></i></li>
                                            <li><i class="icofont icofont-error close-card font-primary"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="activity-table table-responsive">
                                        <table class="table table-bordernone">
                                            <tbody>
                                                <?php foreach ($beritadash as $beritadash) : ?>
                                                    <tr>
                                                        <td>
                                                            <div class="activity-details">
                                                                <h4><span class="f-14"><?php echo $beritadash->tanggal ?></span></h4>
                                                                <h6><?php echo $beritadash->judul ?></h6>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="activity-time"><span class="font-primary f-w-700"><?php echo $beritadash->kecamatan ?></span><span class="d-block light-text"><?php echo $beritadash->kategori ?></span></div>
                                                        </td>
                                                        <td>
                                                            <?php if ($beritadash->media == "internet") { ?>
                                                                <a href="<?php echo $beritadash->sumber ?>"><button class="btn btn-shadow-primary">Link</button></a>
                                                            <?php } elseif ($beritadash->media == "tv" or $beritadash->media == "radio") { ?>

                                                                <?php echo $beritadash->sumber ?>
                                                            <?php } else {
                                                                echo "belum ada sumber";
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="code-box-copy">
                                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head3" title="Copy">
                                            <i class="icofont icofont-copy-alt"></i>
                                        </button>
                                        <pre><code class="language-html" id="example-head3"></code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Container-fluid Ends-->
                <div class="welcome-popup modal fade" id="loadModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <div class="modal-body">
                                <div class="modal-header"></div>
                                <div class="contain p-30">
                                    <div class="text-center">
                                        <h3>Welcome to creative admin</h3>
                                        <p>start your project with developer friendly admin </p>
                                        <button class="btn btn-primary btn-lg txt-white" type="button" data-dismiss="modal" aria-label="Close">Get Started</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer start-->
            <?php $this->load->view("admin/isu/adminnew/partial/footer"); ?>
        </div>
    </div>


    <!-- latest jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/jquery-3.5.1.min.js') ?>"></script>
    <!-- Bootstrap js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/popper.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/bootstrap/bootstrap.js') ?>"></script>
    <!-- feather icon js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/feather-icon/feather-icon.js') ?>"></script>
    <!-- Sidebar jquery-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/sidebar-menu.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/config.js') ?>"></script>
    <!-- Plugins JS start-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/handlebars.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.bundle.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead/typeahead.custom.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/handlebars.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/typeahead-search/typeahead-custom.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/chartist/chartist-plugin-tooltip.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/prism/prism.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/clipboard/clipboard.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.waypoints.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/jquery.counterup.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/counter/counter-custom.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/custom-card/custom-card.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/dashboard/default.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/index.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.en.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datepicker/date-picker/datepicker.custom.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js') ?>"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/script.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/theme-customizer/customizer.js') ?>"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <!--icon-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/notify/bootstrap-notify.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icons-notify.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/icons/icon-clipart.js') ?>"></script>

    <!--js table bawah-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/datatable/datatables/datatable.custom.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js') ?>"></script>

    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/assets/js/chart/chartjs/chart.custom.js') ?>"></script>

    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/apex-chart.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/stock-prices.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chart/apex-chart/chart-custom.js') ?>"></script>

    <!--buat peta-->
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web_expressions.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/polyfills.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/functions.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/ol.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/ol-layerswitcher.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/layers/sumedang26_0.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/styles/sumedang26_0_style.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/layers/layers.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/Autolinker.min.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/26kec/resources/qgis2web.js') ?>"></script>
    <!--buat table peta-->
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/chat-menu.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-core.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-service.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-ui.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/mapsjs-mapevents.js') ?>"></script>
    <script src="<?php echo base_url('asset/custom/isu/tempadmin/assets/js/map-js/custom.js') ?>"></script>
    <script>
        // column chart
        var options = {
            chart: {
                height: 200,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: 'rounded',
                    columnWidth: '55%',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{
                name: 'Positif',
                data: [<?php echo $positif->num_rows(); ?>]
            }, {
                name: 'netral',
                data: [<?php echo $netral->num_rows(); ?>]
            }, {
                name: 'negatif',
                data: [<?php echo $negatif->num_rows(); ?>]
            }],

            colors: ['#35c6ed', '#949494', '#ff4d4d']
        }

        var chart3 = new ApexCharts(
            document.querySelector("#column-chart1"),
            options
        );

        chart3.render();
    </script>

    <script>
        var size = 0;
        var placement = 'point';

        function categories_sumedang26_0(feature, value, size, resolution, labelText,
            labelFont, labelFill, bufferColor, bufferWidth,
            placement) {
            switch (value.toString()) {
                case '1':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: <?php foreach ($kecsumsel as $kec1) : ?>
                            <?php


                                        if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                            echo "'rgba(53, 198, 237, 1)'";
                                        } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                            echo "'rgba(255, 77, 77, 1)'";
                                        } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } else {
                                            echo "'rgba(255, 255, 255, 1)'";
                                        }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '2':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:


                                <?php foreach ($kecsukasari as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>



                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '3':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: <?php foreach ($kecsurian as $kec1) : ?>
                            <?php


                                        if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                            echo "'rgba(53, 198, 237, 1)'";
                                        } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                            echo "'rgba(255, 77, 77, 1)'";
                                        } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                            echo "'rgba(148, 148, 148, 1)'";
                                        } else {
                                            echo "'rgba(255, 255, 255, 1)'";
                                        }
                            ?>


                        <?php endforeach; ?>



                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '4':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecsumut as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '5':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungmedar as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '6':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungkerta as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '7':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectomo as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '8':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kectanjungsari as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '9':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecwado as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '10':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecujungjaya as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '11':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccibugel as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '12':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecbuahdua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '13':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecbuahdua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '14':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccimanggung as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '15':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecconggeang as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '16':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccimalaka as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '17':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatigede as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '18':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccisitu as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '19':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecganeas as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '20':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($keccisarua as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '21':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatinunggal as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '22':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecjatinangor as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '23':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecpaseh as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '24':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecpamulihan as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '25':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecsituraja as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case '26':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color:

                                <?php foreach ($kecrancakalong as $kec1) : ?>
                            <?php


                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                        echo "'rgba(53, 198, 237, 1)'";
                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                        echo "'rgba(255, 77, 77, 1)'";
                                    } elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                        echo "'rgba(148, 148, 148, 1)'";
                                    } else {
                                        echo "'rgba(255, 255, 255, 1)'";
                                    }
                            ?>


                        <?php endforeach; ?>

                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
                case 'NULL':
                    return [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgba(35,35,35,1.0)',
                            lineDash: null,
                            lineCap: 'butt',
                            lineJoin: 'miter',
                            width: 0
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(234,76,176,1.0)'
                        }),
                        text: createTextStyle(feature, resolution, labelText, labelFont,
                            labelFill, placement, bufferColor,
                            bufferWidth)
                    })];
                    break;
            }
        };

        var style_sumedang26_0 = function(feature, resolution) {
            var context = {
                feature: feature,
                variables: {}
            };
            var value = feature.get("id");
            var labelText = "";
            size = 0;
            var labelFont = "10px, sans-serif";
            var labelFill = "#000000";
            var bufferColor = "";
            var bufferWidth = 0;
            var textAlign = "left";
            var offsetX = 8;
            var offsetY = 3;
            var placement = 'point';
            if ("" !== null) {
                labelText = String("");
            }

            var style = categories_sumedang26_0(feature, value, size, resolution, labelText,
                labelFont, labelFill, bufferColor,
                bufferWidth, placement);

            return style;
        };
    </script>

    <script>
        // pie chart
        var options8 = {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['Lokal', 'Regional', 'internasional'],
            series: [<?php echo $lokal->num_rows(); ?>, <?php echo $regional->num_rows(); ?>, <?php echo $intern->num_rows(); ?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart8 = new ApexCharts(
            document.querySelector("#piechart1"),
            options8
        );

        chart8.render();
    </script>

    <script>
        // pie chart
        var options8 = {
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['internet', 'tv', 'radio'],
            series: [<?php echo $medinternet->num_rows(); ?>, <?php echo $medtv->num_rows(); ?>, <?php echo $medradio->num_rows(); ?>],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
        }

        var chart8 = new ApexCharts(
            document.querySelector("#piechart2"),
            options8
        );

        chart8.render();
    </script>

</body>

</html>