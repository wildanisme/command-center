<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="<?php echo base_url('temadmin/css/styles.css')?>" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('/26kec/resources/fontawesome-all.min.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol-layerswitcher.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('./resources/qgis2web.css')?>">
        <style>
			.search-layer {
				top: 65px;
				left: .5em;
			}
			.ol-touch .search-layer {
				top: 80px;
			}
		</style>
        <style>
        html, body {
            background-color: #ffffff;
        }
        .ol-control button {
            background-color: #f8f8f8 !important;
            color: #000000 !important;
            border-radius: 0px !important;
        }
        .ol-zoom, .geolocate, .gcd-gl-control .ol-control {
            background-color: rgba(255,255,255,.4) !important;
            padding: 3px !important;
        }
        .ol-scale-line {
            background: none !important;
        }
        .ol-scale-line-inner {
            border: 2px solid #f8f8f8 !important;
            border-top: none !important;
            background: rgba(255, 255, 255, 0.5) !important;
            color: black !important;
        }
        </style>
        <style>
        #map {
            width: 1653px;
            height: 541px;
        }
        </style>
        <title></title>
    </head>
    <body class="sb-nav-fixed">
     
     
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard ISu Media</h1>      <?php foreach ($kecsurian as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                                <div class="text-white mb-4" style="background-color: rgba(53, 198, 237, 1);">
                                    <div class="card-body">Respon Positif : <?php echo $positif->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="text-white mb-4 " style="background-color: rgba(148, 148, 148, 1);">
                                    <div class="card-body">Respon Netral : <?php echo $netral->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                       
                            <div class="col-xl-4 col-md-6">
                                <div class="text-white mb-4" style="background-color: rgba(255, 77, 77, 1);">
                                    <div class="card-body">Respon Negatif : <?php echo $negatif->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-xl-3">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Statistik Sentimen Isu Media
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart1" width="100%" height="40"></canvas></div>
                                    </table>
                                    </div>
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Top Isu Wilayah
                                    </div>
                                    <div class="table-responsive">
                                            <table class="table table-bordered"  width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                              
                                                <th>Kecamatan</th>
                                                <th>Positif</th>
                                                <th>Netral</th>
                                                <th>Negatif</th>
                                                
                                             
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach ($kec as $kec): ?>
                                            <tr>
                                                
                                                <td>
                                                    <?php echo $kec->kecamatan ?>
                                                </td>
                                                <td>
                                                    <?php echo $kec->totalpos ?>
                                                </td>
                                                <td>
                                                    <?php echo $kec->totalnet ?>
                                                </td>
                                                <td>
                                                    <?php echo $kec->totalneg ?>
                                                </td>
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                            </table>
                                </div>
                            </div>
                            
                            <div class="col-xl-6 col-md-6">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Info Grafis Isu Media
                                    </div>
                                    
                                                            <div id="map">
                                    <div id="popup" class="ol-popup">
                                        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                                        <div id="popup-content"></div>
                                    </div>
                                </div>

                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Top Kategori Isu Media
                                     
                                </div>
                                <div class="table-responsive">
                                            <table class="table table-bordered"  width="100%" cellspacing="0">
                                        <thead>
                                            <tr style="background-color: rgba(53, 198, 237, 1) ;">
                                              
                                                <th>Kategori</th>
                                                <th>positif</th>
                                                
                                             
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach ($kategoripos as $kategori): ?>
                                            <tr>
                                                
                                                <td>
                                                    <?php echo $kategori->kategori ?>
                                                </td>
                                                <td>
                                                    <?php echo $kategori->total ?>
                                                </td>
                                            
                                    
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                            </table>
                                    </div>
                                    <div class="table-responsive">
                                            <table class="table table-bordered"  width="100%" cellspacing="0">
                                        <thead>
                                        <tr style="background-color: rgba(148, 148, 148, 1);">
                                              
                                                <th>Kategori</th>
                                                <th>netral</th>
                                                
                                             
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach ($kategorinet as $kategori): ?>
                                            <tr>
                                                
                                                <td>
                                                    <?php echo $kategori->kategori ?>
                                                </td>
                                                <td>
                                                    <?php echo $kategori->total ?>
                                                </td>
                                            
                                    
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                            </table>
                                        
                                    </div>
                                    <div class="table-responsive">
                                            <table class="table table-bordered"  width="100%" cellspacing="0">
                                        <thead>
                                        <tr style="background-color: rgba(255, 77, 77, 1);">
                                              
                                                <th>Kategori</th>
                                                <th>negatif</th>
                                                
                                             
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <?php foreach ($kategorineg as $kategori): ?>
                                            <tr>
                                                
                                                <td>
                                                    <?php echo $kategori->kategori ?>
                                                </td>
                                                <td>
                                                    <?php echo $kategori->total ?>
                                                </td>
                                            
                                    
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                            </table>
                                    </div>
                                   
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            
                            <div class="col-xl-12">
                            <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table mr-1"></i>
                                DataTable Example  
                            </div>
                            <div class="card-body">
                            <div class="card mb-3">
					        <div class="card-header">
						    <a href="<?php echo site_url('admin/add') ?>"><i class="fas fa-plus"></i> Add New</a>
					        </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Id Berita</th>
                                                <th>Judul</th>
                                                <th>Deskripsi</th>
                                                <th>Kecamatan</th>
                                                <th>Kategori</th>
                                                <th>Sentimen</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                            <th>Id Berita</th>
                                                <th>Judul</th>
                                                <th>Deskripsi</th>
                                                <th>Kecamatan</th>
                                                <th>Kategori</th>
                                                <th>Sentimen</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
									<?php foreach ($berita as $berita): ?>
									<tr>
										<td width="150">
											<?php echo $berita->berita_id ?>
										</td>
										<td>
											<?php echo $berita->judul ?>
										</td>
										<td>
											<?php echo $berita->deskripsi ?>
										</td>
										<td>
											<?php echo $berita->kecamatan ?>
                                        </td>
                                        <td>
											<?php echo $berita->kategori ?>
                                        </td>
                                        <td>
											<?php echo $berita->sentimen ?>
                                        </td>
                                        <td width="250">
										
											<a href="<?php echo site_url('admin/delete/'.$berita->berita_id) ?>" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
											<a href="<?php echo site_url('admin/update/'.$berita->berita_id) ?>" class="btn btn-small text-danger"><i class="fas fa-trash"></i> update</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                            </div>

                            
                        </div>
                       
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>

                    
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/js/scripts.js')?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/chart-area-demo.js')?>"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/chart-bar-demo.js')?>"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/datatables-demo.js')?>"></script>
        <script src="<?php echo base_url('26kec/resources/qgis2web_expressions.js')?>"></script>
        <script src="<?php echo base_url('26kec/resources/polyfills.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/functions.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/ol.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/ol-layerswitcher.js')?>"></script>
        <script src="<?php echo base_url('26kec/layers/sumedang26_0.js')?>"></script>
        <script src="<?php echo base_url('26kec/styles/sumedang26_0_style.js')?>"></script>
        <script src="<?php echo base_url('./26kec/layers/layers.js')?>" type="text/javascript"></script> 
        <script src="<?php echo base_url('./26kec/resources/Autolinker.min.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/qgis2web.js')?>"></script>
        <script>
        // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart1");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["poritif", "netral", "negatif"],
    datasets: [{
      label: "Jumlah Berita",
      backgroundColor: ["rgba(53, 198, 237, 1)","rgba(148, 148, 148, 1)","rgba(255, 77, 77, 1)"],
      borderColor: ["rgb(30, 144, 255)","rgb(253, 215, 3)","rgb(220, 20, 60)"],
      data: [<?php echo $positif->num_rows();?>,<?php echo $netral->num_rows();?>,<?php echo $negatif->num_rows();?>],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 15,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});</script>
<script>

var size = 0;
var placement = 'point';
function categories_sumedang26_0(feature, value, size, resolution, labelText,
                       labelFont, labelFill, bufferColor, bufferWidth,
                       placement) {
                switch(value.toString()) {case '1':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color:
            <?php foreach ($kecsumsel as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
            
            }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '2':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 


            <?php foreach ($kecsukasari as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

            
            
            }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '3':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color:
            <?php foreach ($kecsurian as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

            
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '4':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color:
             
            <?php foreach ($kecsumut as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '5':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color:
             
            <?php foreach ($kectanjungmedar as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '6':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kectanjungkerta as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '7':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kectomo as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '8':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kectanjungsari as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '9':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecwado as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '10':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecujungjaya as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '11':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($keccibugel as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '12':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($kecbuahdua as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '13':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($kecbuahdua as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '14':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($keccimanggung as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '15':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
            
            <?php foreach ($kecconggeang as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '16':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($keccimalaka as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '17':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($kecjatigede as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '18':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($keccisitu as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '19':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecganeas as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '20':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($keccisarua as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '21':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecjatinunggal as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '22':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($kecjatinangor as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '23':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecpaseh as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '24':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecpamulihan as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '25':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 

            <?php foreach ($kecsituraja as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>

        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case '26':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),
        fill: new ol.style.Fill({color: 
        
            <?php foreach ($kecrancakalong as $kec1): ?>
                                           <?php
                                                    

                                                    if ($kec1->totalpos > $kec1->totalnet && $kec1->totalpos > $kec1->totalneg) {
                                                    echo "'rgba(53, 198, 237, 1)'";
                                                    } elseif ($kec1->totalnet > $kec1->totalpos && $kec1->totalnet > $kec1->totalneg) {
                                                    echo "'rgba(148, 148, 148, 1)'";
                                                    } 
                                                    elseif ($kec1->totalneg > $kec1->totalpos && $kec1->totalneg > $kec1->totalnet) {
                                                        echo "'rgba(255, 77, 77, 1)'";}
                                                    elseif ($kec1->totalneg = $kec1->totalpos or $kec1->totalneg = $kec1->totalnet) {
                                                            echo "'rgba(148, 148, 148, 1)'";}
                                                    else {
                                                    echo "'rgba(255, 255, 255, 1)'";
                                                    }
                                                    ?>           
                               
                                    
                                 <?php endforeach; ?>
        
        }),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;
case 'NULL':
                    return [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'rgba(35,35,35,1.0)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}),fill: new ol.style.Fill({color: 'rgba(234,76,176,1.0)'}),
        text: createTextStyle(feature, resolution, labelText, labelFont,
                              labelFill, placement, bufferColor,
                              bufferWidth)
    })];
                    break;}};

var style_sumedang26_0 = function(feature, resolution){
    var context = {
        feature: feature,
        variables: {}
    };
    var value = feature.get("id");
    var labelText = "";
    size = 0;
    var labelFont = "10px, sans-serif";
    var labelFill = "#000000";
    var bufferColor = "";
    var bufferWidth = 0;
    var textAlign = "left";
    var offsetX = 8;
    var offsetY = 3;
    var placement = 'point';
    if ("" !== null) {
        labelText = String("");
    }
    
var style = categories_sumedang26_0(feature, value, size, resolution, labelText,
                          labelFont, labelFill, bufferColor,
                          bufferWidth, placement);

    return style;
};


</script>

    </body>
</html>
