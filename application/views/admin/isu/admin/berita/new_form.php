<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SB Admin</title>
        <link href="<?php echo base_url('temadmin/css/styles.css')?>" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>

		<script src="<?php echo base_url('temadmin/js/bootstrap-datepicker.js')?>"></script>
		<link rel="stylesheet" href="<?php echo base_url('./temadmin/css/datepicker.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('/26kec/resources/fontawesome-all.min.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('./26kec/resources/ol-layerswitcher.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('./resources/qgis2web.css')?>">

        <style>
.search-layer {
  top: 65px;
  left: .5em;
}
.ol-touch .search-layer {
  top: 80px;
}
</style>
        <style>
        html, body {
            background-color: #ffffff;
        }
        .ol-control button {
            background-color: #f8f8f8 !important;
            color: #000000 !important;
            border-radius: 0px !important;
        }
        .ol-zoom, .geolocate, .gcd-gl-control .ol-control {
            background-color: rgba(255,255,255,.4) !important;
            padding: 3px !important;
        }
        .ol-scale-line {
            background: none !important;
        }
        .ol-scale-line-inner {
            border: 2px solid #f8f8f8 !important;
            border-top: none !important;
            background: rgba(255, 255, 255, 0.5) !important;
            color: black !important;
        }
        </style>
        <style>
        #map {
            width: 1653px;
            height: 541px;
        }
        </style>
        <title></title>
    </head>
    <body class="sb-nav-fixed">
     
     
                <main>
                    <div class="container-fluid">
                        <h1 class="mt-4">Dashboard ISu Media</h1>
                        <ol class="breadcrumb mb-4">
                        <a href="<?php echo site_url('admin') ?>"><i class="fas fa-arrow-left"></i> Dashboard</a>
                  
                        </ol>
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body">Respon Positif : <?php echo $positif->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body">Respon Netral : <?php echo $netral->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                       
                            <div class="col-xl-4 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body">Respon Negatif : <?php echo $negatif->num_rows();?></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small text-white stretched-link" href="#">View Details</a>
                                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-xl-3">
                                <div class="card mb-4">
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Statistik Sentimen Isu Media
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart1" width="100%" height="40"></canvas></div>
                                    </table>
                                    </div>
                                    <div class="card-header">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        Top Isu Wilayah
                                    </div>
                                    <div class="table-responsive">
                                            <table class="table table-bordered"  width="100%" cellspacing="0">
												<thead>
													<tr>
														<th>Kecamatan</th>
														<th>Positif</th>
														<th>Netral</th>
														<th>Negatif</th>
													</tr>
												</thead>
                                        
												<tbody>
													<?php foreach ($kec as $kec): ?>
														<tr>
                                                
															<td>
																<?php echo $kec->kecamatan ?>
															</td>
															<td>
																<?php echo $kec->totalpos ?>
															</td>
															<td>
																<?php echo $kec->totalnet ?>
															</td>
															<td>
																<?php echo $kec->totalneg ?>
															</td>
													<?php endforeach; ?>
												</tbody>
                                            </table>
									</div>
                            </div>
                            <div class="card-body">

						<form action="<?php echo site_url('admin/add') ?>" method="post" enctype="multipart/form-data" >
							<div class="form-group">
								<label for="name">tanggal</label>
								<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
								 type= date name="tanggal" placeholder="tanggal"/>
								<div class="invalid-feedback">
									<?php echo form_error('tanggal') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Judul</label>
								<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
								 type="text" name="judul" placeholder="Judul Berita"/>
								<div class="invalid-feedback">
									<?php echo form_error('judul') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Deskripsi</label>
								<textarea class="form-control <?php echo form_error('deskripsi') ? 'is-invalid':'' ?>"
								 name="deskripsi" placeholder="Deskripsi Berita"></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('deskripsi') ?>
								</div>
                            </div>
                            
                            <div class="form-group">
								<label for="name">Kecamatab</label>
								<select name="kecamatan" id="kecamatan" class="form-control <?php echo form_error('kecamatan') ? 'is-invalid':'' ?>"
								 type="text">
                                 <?php foreach ($kecamatan1 as $kecamatan): ?>
                                    <option value="<?php echo $kecamatan->nama_kecamatan ?>"><?php echo $kecamatan->nama_kecamatan ?></option>
                                              <?php endforeach; ?>
                                    </select>
								<div class="invalid-feedback">
									<?php echo form_error('kecamatan') ?>
								</div>
                            </div>
                            
                            <div class="form-group">
								<label for="name">KAtegori</label>
								<select name="kategori" id="kategori" class="form-control <?php echo form_error('kategori') ? 'is-invalid':'' ?>"
								 type="text">
                                 <?php foreach ($kategori1 as $kategori): ?>
                                    <option value=" 
                                              <?php echo $kategori->kategori ?>
                                               ">
                                              <?php echo $kategori->kategori ?>
                                              </option>
                                              <?php endforeach; ?>
                                    </select>
                                 
								<div class="invalid-feedback">
									<?php echo form_error('kategori') ?>
								</div>


                            </div>
                            
                            <div class="form-group">
								<label for="name">Sentimen</label>
								<select name="sentimen" id="sentimen" class="form-control <?php echo form_error('sentimen') ? 'is-invalid':'' ?>"
								 type="text">
                                    <option value="positif">Positif</option>
                                    <option value="netral">Netral</option>
                                    <option value="negatif">Negatif</option>
                                    </select>
								<div class="invalid-feedback">
									<?php echo form_error('sentimen') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>

                    
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/js/scripts.js')?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/chart-area-demo.js')?>"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/chart-bar-demo.js')?>"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('temadmin/assets/demo/datatables-demo.js')?>"></script>
        <script src="<?php echo base_url('26kec/resources/qgis2web_expressions.js')?>"></script>
        <script src="<?php echo base_url('26kec/resources/polyfills.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/functions.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/ol.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/ol-layerswitcher.js')?>"></script>
        <script src="<?php echo base_url('26kec/layers/sumedang26_0.js')?>"></script>
        <script src="<?php echo base_url('26kec/styles/sumedang26_0_style.js')?>"></script>
        <script src="<?php echo base_url('./26kec/layers/layers.js')?>" type="text/javascript"></script> 
        <script src="<?php echo base_url('./26kec/resources/Autolinker.min.js')?>"></script>
        <script src="<?php echo base_url('./26kec/resources/qgis2web.js')?>"></script>

		// srip date and time
		<script = type = "text/javascript" src "assets/plugin/js/bootsrap-date-picker.min.js"></script> 
        <script>
        // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart1");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["poritif", "netral", "negatif"],
    datasets: [{
      label: "Jumlah Berita",
      backgroundColor: ["rgb(30, 144, 255)","rgb(253, 215, 3)","rgb(220, 20, 60)"],
      borderColor: ["rgb(30, 144, 255)","rgb(253, 215, 3)","rgb(220, 20, 60)"],
      data: [<?php echo $positif->num_rows();?>,<?php echo $netral->num_rows();?>,<?php echo $negatif->num_rows();?>],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 15,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});</script>
    </body>
</html>
