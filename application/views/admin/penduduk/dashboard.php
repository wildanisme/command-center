<div class="page-body">
  <div class="container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col-lg-6 main-header">
          <h2>Dashboard Pendapatan</h2>
          <h6 class="mb-0">Kabupaten Bogor</h6>
        </div>
        <div class="col-lg-6 breadcrumb-right">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="pe-7s-home"></i></a></li>
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item active">Pendapatan</li>
          </ol>
        </div>
      </div>
    </div>
    <br>

    <div class="row">
      <!-- New taller cards -->
      <div class="col-xl-4 xl-10 col-md-6 box-col-3">
        <div class="card gradient-secondary o-hidden">
          <div class="card-body tag-card">
            <div class="ecommerce-chart">
              <div class="media ecommerce-small-chart">
                <div class="small-bar"></div>
                <div class="sale-chart">
                  <div class="media-body m-l-70">
                    <h6 class="f-w-700 m-l-10">Pendapatan Asli Daerah</h6>
                    <h4 class="mb-0 f-w-700 m-l-10">8.765.432.109.876.00</h4>
                  </div>
                </div>
              </div>
            </div>
            <span class="tag-hover-effect"><!-- dots here --></span>
          </div>
        </div>
      </div>

      <div class="col-xl-4 xl-10 col-md-6 box-col-3">
        <div class="card gradient-info o-hidden">
          <div class="card-body tag-card">
            <div class="ecommerce-chart">
              <div class="media ecommerce-small-chart">
                <div class="small-bar"></div>
                <div class="sale-chart">
                  <div class="media-body m-l-70">
                    <h6 class="f-w-700 m-l-10">Pendapatan Transfer</h6>
                    <h4 class="mb-0 f-w-700 m-l-10">9.876.543.210.987.00</h4>
                  </div>
                </div>
              </div>
            </div>
            <span class="tag-hover-effect"><!-- dots here --></span>
          </div>
        </div>
      </div>

      <div class="col-xl-4 xl-10 col-md-6 box-col-3">
        <div class="card gradient-warning o-hidden">
          <div class="card-body tag-card">
            <div class="ecommerce-chart">
              <div class="media ecommerce-small-chart">
                <div class="small-bar"></div>
                <div class="sale-chart">
                  <div class="media-body m-l-70">
                    <h6 class="f-w-700 m-l-10">Lain-lain PAD SAH</h6>
                    <h4 class="mb-0 f-w-700 m-l-10">7.654.321.098.765.00</h4>
                  </div>
                </div>
              </div>
            </div>
            <span class="tag-hover-effect"><!-- dots here --></span>
          </div>
        </div>
      </div>
      <!-- End of new taller cards -->

      <!-- Existing cards -->
      <div class="col-xl-4 xl-7 col-md-3 box-col-3">
        <!-- Card 1 -->
        <!-- ... (existing card code) ... -->
      </div>
      
      <div class="col-xl-4 xl-7 col-md-3 box-col-3">
        <!-- Card 2 -->
        <!-- ... (existing card code) ... -->
      </div>
      
      <div class="col-xl-4 xl-7 col-md-3 box-col-3">
        <!-- Card 3 -->
        <!-- ... (existing card code) ... -->
      </div>
      <!-- ... (continue with the rest of the HTML code) ... -->

    </div>
  </div>
<!-- ... (previous HTML code) ... -->

<!-- New small card -->
      <span class="tag-hover-effect"><!-- dots here --></span>
    </div>
  </div>
</div>

<!-- ... (continue with the rest of the HTML code) ... -->

</div>
