<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Poco admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
  <meta name="keywords" content="admin template, Poco admin template, dashboard template, flat admin template, responsive admin template, web app">
  <meta name="author" content="pixelstrap">
  <link rel="icon" href="<?= base_url(); ?>assets/images/favicon.png" type="image/x-icon">
  <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.png" type="image/x-icon">
  <title>Dinas Pendidikan - Kab. Bogor</title>
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Font Awesome-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/fontawesome.css">
  <!-- ico-font-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/icofont.css">
  <!-- Themify icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/themify.css">
  <!-- Flag icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/flag-icon.css">
  <!-- Feather icon-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/feather-icon.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/animate.css">
  <!-- Plugins css start-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/pe7-icon.css">
  <!-- Plugins css Ends-->
  <!-- Bootstrap css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/bootstrap.css">
  <!-- App css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">
  <link id="color" rel="stylesheet" href="<?= base_url(); ?>assets/css/color-1.css" media="screen">
  <!-- Responsive css-->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/responsive.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/datatables.css">

</head>

<body>
  <!-- Loader starts-->
  <div class="loader-wrapper">
    <div class="typewriter">
      <h1>New Era Admin Loading..</h1>
    </div>
  </div>
  <!-- Loader ends-->
  <!-- page-wrapper Start-->
  <div class="page-wrapper">
    <!-- Page Header Start-->
    <div class="page-main-header">
      <div class="main-header-right">
        <div class="main-header-left text-center">
          <div class="logo-wrapper"><a href="index.html"><img src="<?= base_url(); ?>assets/images/logo/logo.png" alt="" style="width: 150px"></a></div>
        </div>
        <div class="mobile-sidebar">
          <div class="media-body text-right switch-sm">
            <label class="switch ml-3"><i class="font-primary" id="sidebar-toggle" data-feather="align-center"></i></label>
          </div>
        </div>
        <div class="vertical-mobile-sidebar"><i class="fa fa-bars sidebar-bar"> </i></div>
        <div class="nav-right col pull-right right-menu">
          <ul class="nav-menus">
            <li>
              <form class="form-inline search-form" action="#" method="get">
                <div class="form-group">
                  <div class="Typeahead Typeahead--twitterUsers">
                    <div class="u-posRelative">
                      <input class="Typeahead-input form-control-plaintext" id="demo-input" type="text" name="q" placeholder="Search Your Product...">
                      <div class="spinner-border Typeahead-spinner" role="status"><span class="sr-only">Loading...</span></div><span class="d-sm-none mobile-search"><i data-feather="search"></i></span>
                    </div>
                    <div class="Typeahead-menu"></div>
                  </div>
                </div>
              </form>
            </li>
            <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/bookmark.png" alt="">
              <div class="onhover-show-div bookmark-flip">
                <div class="flip-card">
                  <div class="flip-card-inner">
                    <div class="front">
                      <ul class="droplet-dropdown bookmark-dropdown">
                        <li class="gradient-primary text-center">
                          <h5 class="f-w-700">Bookmark</h5><span>Bookmark Icon With Grid</span>
                        </li>
                        <li>
                          <div class="row">
                            <div class="col-4 text-center"><i data-feather="file-text"></i></div>
                            <div class="col-4 text-center"><i data-feather="activity"></i></div>
                            <div class="col-4 text-center"><i data-feather="users"></i></div>
                            <div class="col-4 text-center"><i data-feather="clipboard"></i></div>
                            <div class="col-4 text-center"><i data-feather="anchor"></i></div>
                            <div class="col-4 text-center"><i data-feather="settings"></i></div>
                          </div>
                        </li>
                        <li class="text-center">
                          <button class="flip-btn" id="flip-btn">Add New Bookmark</button>
                        </li>
                      </ul>
                    </div>
                    <div class="back">
                      <ul>
                        <li>
                          <div class="droplet-dropdown bookmark-dropdown flip-back-content">
                            <input type="text" placeholder="search...">
                          </div>
                        </li>
                        <li>
                          <button class="d-block flip-back" id="flip-back">Back</button>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-secondary" src="<?= base_url(); ?>assets/images/dashboard/like.png" alt="">
              <ul class="onhover-show-div droplet-dropdown">
                <li class="gradient-primary text-center">
                  <h5 class="f-w-700">Grid Dashboard</h5><span>Easy Grid inside dropdown</span>
                </li>
                <li>
                  <div class="row">
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="file-text"></i><span class="d-block">Content</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="activity"></i><span class="d-block">Activity</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="users"></i><span class="d-block">Contacts</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="clipboard"></i><span class="d-block">Reports</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="anchor"></i><span class="d-block">Automation</span></div>
                    <div class="col-sm-4 col-6 droplet-main"><i data-feather="settings"></i><span class="d-block">Settings</span></div>
                  </div>
                </li>
                <li class="text-center">
                  <button class="btn btn-primary btn-air-primary">Follows Up</button>
                </li>
              </ul>
            </li>
            <li class="onhover-dropdown"><img class="img-fluid img-shadow-warning" src="<?= base_url(); ?>assets/images/dashboard/notification.png" alt="">
              <ul class="onhover-show-div notification-dropdown">
                <li class="gradient-primary">
                  <h5 class="f-w-700">Notifications</h5><span>You have 6 unread messages</span>
                </li>
                <li>
                  <div class="media">
                    <div class="notification-icons bg-success mr-3"><i class="mt-0" data-feather="thumbs-up"></i></div>
                    <div class="media-body">
                      <h6>Someone Likes Your Posts</h6>
                      <p class="mb-0"> 2 Hours Ago</p>
                    </div>
                  </div>
                </li>
                <li class="pt-0">
                  <div class="media">
                    <div class="notification-icons bg-info mr-3"><i class="mt-0" data-feather="message-circle"></i></div>
                    <div class="media-body">
                      <h6>3 New Comments</h6>
                      <p class="mb-0"> 1 Hours Ago</p>
                    </div>
                  </div>
                </li>
                <li class="bg-light txt-dark"><a href="#">All </a> notification</li>
              </ul>
            </li>
            <li><a class="right_side_toggle" href="#"><img class="img-fluid img-shadow-success" src="<?= base_url(); ?>assets/images/dashboard/chat.png" alt=""></a></li>
            <li class="onhover-dropdown"> <span class="media user-header"><img class="img-fluid" src="<?= base_url(); ?>assets/images/dashboard/user.png" alt=""></span>
              <ul class="onhover-show-div profile-dropdown">
                <li class="gradient-primary">
                  <h5 class="f-w-600 mb-0">Elana Saint</h5><span>Web Designer</span>
                </li>
                <li><i data-feather="user"> </i>Profile</li>
                <li><i data-feather="message-square"> </i>Inbox</li>
                <li><i data-feather="file-text"> </i>Taskboard</li>
                <li><i data-feather="settings"> </i>Settings </li>
              </ul>
            </li>
          </ul>
          <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
        <script id="result-template" type="text/x-handlebars-template">
          <div class="ProfileCard u-cf">                        
            <div class="ProfileCard-avatar"><i class="pe-7s-home"></i></div>
            <div class="ProfileCard-details">
            <div class="ProfileCard-realName">{{name}}</div>
            </div>
            </div>
          </script>
        <script id="empty-template" type="text/x-handlebars-template"><div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div></script>
      </div>
    </div>

    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
		<div class="iconsidebar-menu iconbar-mainmenu-close">
                <?php $this->load->view('admin/src/menu'); ?>

            </div>
      <!-- Page Sidebar Start-->
      

      <!-- Page Sidebar Ends-->

      <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6 main-header">
                <h2>Dinas Pendidikan</h2>
                <h6 class="mb-0">Kabupaten Bogor</h6>
              </div>
              <div class="col-lg-6 breadcrumb-right">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
                  <li class="breadcrumb-item">Home</li>
                  <li class="breadcrumb-item active">Halaman Data</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
          	
          	<!-- ////////////////// -->

          	<div class="row ecommerce-chart-card">
  <div class="col-md-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-primary o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart1 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  275
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah TK Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>

  <div class="col-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-secondary o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart2 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  639
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah KB Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>

  <div class="col-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-warning o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart2 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  74
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah PKBM Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>

  <div class="col-md-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-warning o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart1 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  604
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah SD Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>

  <div class="col-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-secondary o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart2 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  123
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah SMP Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>

  <div class="col-4 xl-20 col-md-4 box-col-4">
    <div class="card gradient-primary o-hidden">
      <div class="card-body tag-card">
        <div class="ecommerce-chart">
          <div class="media ecommerce-small-chart">
            <div class="small-bar">
              <div class="small-chart2 flot-chart-container"></div>
            </div>
            <div class="sale-chart">
              <div class="media-body m-l-40">
                <h3 class="f-w-40 m-l-10">
                  27
                </h3>
                <h6 class="mb-0 f-w-700 m-l-10">Jumlah SMA Negeri dan Swasta</h6>
              </div>
            </div>
          </div>
        </div><span class="tag-hover-effect"><span class="dots-group"><span class="dots dots1"></span><span class="dots dots2 dot-small"></span><span class="dots dots3 dot-small"></span><span class="dots dots4 dot-medium"></span><span class="dots dots5 dot-small"></span><span class="dots dots6 dot-small"></span><span class="dots dots7 dot-small-semi"></span><span class="dots dots8 dot-small-semi"></span><span class="dots dots9 dot-small"> </span></span></span>
      </div>
    </div>
  </div>



      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik TK Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chart"></div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik KB Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chartKB"></div>
          </div>
        </div>
      </div>
      <div class="w-100"></div>
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik PKBM Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chartPKBM"></div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik SD Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chartSD"></div>
          </div>
        </div>
      </div>

      <div class="w-100"></div>
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik SMP Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chartSMP"></div>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>
              <center>Data Peserta didik SMA Kabupaten Bogor per Kecamatan</center>
            </h5>
          </div>
          <div class="card-body p-0">
            <div id="column-chartSMA"></div>
          </div>
        </div>
      </div>

    <div class="w-100"></div>

      <div class="col">
        <div class="card">
          <div class="card-header">
            <h5>Jumlah Guru / Tenaga Pendidik</h5>
          </div>
          <div class="card-body apex-chart p-0">
            <div id="piechart"></div>
          </div>
        </div>
      </div>


  <!--  -->

          
       </div>
     </div></div></div>
      <!-- footer start-->
      <footer class="footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6 footer-copyright">
              <p class="mb-0">Copyright © Dinas Pendidikan Kab. Bogor.</p>
            </div>
            <div class="col-md-6">
              <p class="pull-right mb-0">Hand-crafted & made with<i class="fa fa-heart"></i></p>
            </div>
          </div>
        </div>
      </footer>

    </div>
  </div>


  <!-- latest jquery-->
  <script src="<?= base_url(); ?>assets/js/jquery-3.5.1.min.js"></script>
  <!-- Bootstrap js-->
  <script src="<?= base_url(); ?>assets/js/bootstrap/popper.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/bootstrap/bootstrap.js"></script>
  <!-- feather icon js-->
  <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/icons/feather-icon/feather-icon.js"></script>
  <!-- Sidebar jquery-->
  <script src="<?= base_url(); ?>assets/js/sidebar-menu.js"></script>
  <script src="<?= base_url(); ?>assets/js/config.js"></script>
  <!-- Plugins JS start-->
  <script src="<?= base_url(); ?>assets/js/chart/apex-chart/apex-chart.js"></script>
  <script src="<?= base_url(); ?>assets/js/chart/apex-chart/stock-prices.js"></script>
  <script src="<?= base_url(); ?>assets/js/chat-menu.js"></script>
  <!-- Plugins JS Ends-->
  <!-- Theme js-->
  <script src="<?= base_url(); ?>assets/js/script.js"></script>
  <script src="<?= base_url(); ?>assets/js/theme-customizer/customizer.js"></script>
  <script src="<?= base_url(); ?>assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url(); ?>assets/js/datatable/datatables/datatable.custom.js"></script>
  <!-- login js-->
  <!-- Plugin used-->
  <script>
    // basic area chart
    var options = {
      chart: {
        height: 350,
        type: 'area',
        zoom: {
          enabled: false
        }

      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'straight'
      },
      series: [{
        name: "JUMLAH",
        data: [6, 104, 40, 15, 112, 38865, 5692, 20, 10, 5, 34, 1, 6, 2253, 8221, 33411, 2770]
      }],
      title: {
        text: 'Jumlah PMKS',
        align: 'left'
      },
      subtitle: {
        text: '91.565',
        align: 'left'
      },
      labels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'],
      xaxis: {
        type: 'text',
      },
      yaxis: {
        opposite: true
      },
      legend: {
        horizontalAlign: 'left'
      },
      colors: ['#7e37d8']

    }

    var chart = new ApexCharts(
      document.querySelector("#basic-apex"),
      options
    );

    chart.render();
  </script>

  <script>
    
var dataSeries = [
    [{
        "date": "2014-01-01",
        "value": 20000000
    },
        {
            "date": "2014-01-02",
            "value": 10379978
        },
        {
            "date": "2014-01-03",
            "value": 30493749
        },
        {
            "date": "2014-01-04",
            "value": 10785250
        },
        {
            "date": "2014-01-05",
            "value": 33901904
        },
        {
            "date": "2014-01-06",
            "value": 11576838
        },
        {
            "date": "2014-01-07",
            "value": 14413854
        },
        {
            "date": "2014-01-08",
            "value": 15177211
        },
        {
            "date": "2014-01-09",
            "value": 16622100
        },
        {
            "date": "2014-01-10",
            "value": 17381072
        },
        {
            "date": "2014-01-11",
            "value": 18802310
        },
        {
            "date": "2014-01-12",
            "value": 15531790
        },
        {
            "date": "2014-01-13",
            "value": 15748881
        },
        {
            "date": "2014-01-14",
            "value": 18706437
        },
        {
            "date": "2014-01-15",
            "value": 19752685
        },
        {
            "date": "2014-01-16",
            "value": 21016418
        },
        {
            "date": "2014-01-17",
            "value": 25622924
        },
        {
            "date": "2014-01-18",
            "value": 25337480
        },
        {
            "date": "2014-01-19",
            "value": 22258882
        },
        {
            "date": "2014-01-20",
            "value": 23829538
        },
        {
            "date": "2014-01-21",
            "value": 24245689
        },
        {
            "date": "2014-01-22",
            "value": 26429711
        },
        {
            "date": "2014-01-23",
            "value": 26259017
        },
        {
            "date": "2014-01-24",
            "value": 25396183
        },
        {
            "date": "2014-01-25",
            "value": 23107346
        },
        {
            "date": "2014-01-26",
            "value": 28659852
        },
        {
            "date": "2014-01-27",
            "value": 25270783
        },
        {
            "date": "2014-01-28",
            "value": 26270783
        },
        {
            "date": "2014-01-29",
            "value": 27270783
        },
        {
            "date": "2014-01-30",
            "value": 28270783
        },
        {
            "date": "2014-01-31",
            "value": 29270783
        },
        {
            "date": "2014-02-01",
            "value": 30270783
        },
        {
            "date": "2014-02-02",
            "value": 31270783
        },
        {
            "date": "2014-02-03",
            "value": 32270783
        },
        {
            "date": "2014-02-04",
            "value": 33270783
        },
        {
            "date": "2014-02-05",
            "value": 28270783
        },
        {
            "date": "2014-02-06",
            "value": 27270783
        },
        {
            "date": "2014-02-07",
            "value": 35270783
        },
        {
            "date": "2014-02-08",
            "value": 34270783
        },
        {
            "date": "2014-02-09",
            "value": 28270783
        },
        {
            "date": "2014-02-10",
            "value": 35270783
        },
        {
            "date": "2014-02-11",
            "value": 36270783
        },
        {
            "date": "2014-02-12",
            "value": 34127078
        },
        {
            "date": "2014-02-13",
            "value": 33124078
        },
        {
            "date": "2014-02-14",
            "value": 36227078
        },
        {
            "date": "2014-02-15",
            "value": 37827078
        },
        {
            "date": "2014-02-16",
            "value": 36427073
        },
        {
            "date": "2014-02-17",
            "value": 37570783
        },
        {
            "date": "2014-02-18",
            "value": 38627073
        },
        {
            "date": "2014-02-19",
            "value": 37727078
        },
        {
            "date": "2014-02-20",
            "value": 38827073
        },
        {
            "date": "2014-02-21",
            "value": 40927078
        },
        {
            "date": "2014-02-22",
            "value": 41027078
        },
        {
            "date": "2014-02-23",
            "value": 42127073
        },
        {
            "date": "2014-02-24",
            "value": 43220783
        },
        {
            "date": "2014-02-25",
            "value": 44327078
        },
        {
            "date": "2014-02-26",
            "value": 40427078
        },
        {
            "date": "2014-02-27",
            "value": 41027078
        },
        {
            "date": "2014-02-28",
            "value": 45627078
        },
        {
            "date": "2014-03-01",
            "value": 44727078
        },
        {
            "date": "2014-03-02",
            "value": 44227078
        },
        {
            "date": "2014-03-03",
            "value": 45227078
        },
        {
            "date": "2014-03-04",
            "value": 46027078
        },
        {
            "date": "2014-03-05",
            "value": 46927078
        },
        {
            "date": "2014-03-06",
            "value": 47027078
        },
        {
            "date": "2014-03-07",
            "value": 46227078
        },
        {
            "date": "2014-03-08",
            "value": 47027078
        },
        {
            "date": "2014-03-09",
            "value": 48027078
        },
        {
            "date": "2014-03-10",
            "value": 47027078
        },
        {
            "date": "2014-03-11",
            "value": 47027078
        },
        {
            "date": "2014-03-12",
            "value": 48017078
        },
        {
            "date": "2014-03-13",
            "value": 48077078
        },
        {
            "date": "2014-03-14",
            "value": 48087078
        },
        {
            "date": "2014-03-15",
            "value": 48017078
        },
        {
            "date": "2014-03-16",
            "value": 48047078
        },
        {
            "date": "2014-03-17",
            "value": 48067078
        },
        {
            "date": "2014-03-18",
            "value": 48077078
        },
        {
            "date": "2014-03-19",
            "value": 48027074
        },
        {
            "date": "2014-03-20",
            "value": 48927079
        },
        {
            "date": "2014-03-21",
            "value": 48727071
        },
        {
            "date": "2014-03-22",
            "value": 48127072
        },
        {
            "date": "2014-03-23",
            "value": 48527072
        },
        {
            "date": "2014-03-24",
            "value": 48627027
        },
        {
            "date": "2014-03-25",
            "value": 48027040
        },
        {
            "date": "2014-03-26",
            "value": 48027043
        },
        {
            "date": "2014-03-27",
            "value": 48057022
        },
        {
            "date": "2014-03-28",
            "value": 49057022
        },
        {
            "date": "2014-03-29",
            "value": 50057022
        },
        {
            "date": "2014-03-30",
            "value": 51057022
        },
        {
            "date": "2014-03-31",
            "value": 52057022
        },
        {
            "date": "2014-04-01",
            "value": 53057022
        },
        {
            "date": "2014-04-02",
            "value": 54057022
        },
        {
            "date": "2014-04-03",
            "value": 52057022
        },
        {
            "date": "2014-04-04",
            "value": 55057022
        },
        {
            "date": "2014-04-05",
            "value": 58270783
        },
        {
            "date": "2014-04-06",
            "value": 56270783
        },
        {
            "date": "2014-04-07",
            "value": 55270783
        },
        {
            "date": "2014-04-08",
            "value": 58270783
        },
        {
            "date": "2014-04-09",
            "value": 59270783
        },
        {
            "date": "2014-04-10",
            "value": 60270783
        },
        {
            "date": "2014-04-11",
            "value": 61270783
        },
        {
            "date": "2014-04-12",
            "value": 62270783
        },
        {
            "date": "2014-04-13",
            "value": 63270783
        },
        {
            "date": "2014-04-14",
            "value": 64270783
        },
        {
            "date": "2014-04-15",
            "value": 65270783
        },
        {
            "date": "2014-04-16",
            "value": 66270783
        },
        {
            "date": "2014-04-17",
            "value": 67270783
        },
        {
            "date": "2014-04-18",
            "value": 68270783
        },
        {
            "date": "2014-04-19",
            "value": 69270783
        },
        {
            "date": "2014-04-20",
            "value": 70270783
        },
        {
            "date": "2014-04-21",
            "value": 71270783
        },
        {
            "date": "2014-04-22",
            "value": 72270783
        },
        {
            "date": "2014-04-23",
            "value": 73270783
        },
        {
            "date": "2014-04-24",
            "value": 74270783
        },
        {
            "date": "2014-04-25",
            "value": 75270783
        },
        {
            "date": "2014-04-26",
            "value": 76660783
        },
        {
            "date": "2014-04-27",
            "value": 77270783
        },
        {
            "date": "2014-04-28",
            "value": 78370783
        },
        {
            "date": "2014-04-29",
            "value": 79470783
        },
        {
            "date": "2014-04-30",
            "value": 80170783
        }
    ],
    [{
        "date": "2014-01-01",
        "value": 150000000
    },
        {
            "date": "2014-01-02",
            "value": 160379978
        },
        {
            "date": "2014-01-03",
            "value": 170493749
        },
        {
            "date": "2014-01-04",
            "value": 160785250
        },
        {
            "date": "2014-01-05",
            "value": 167391904
        },
        {
            "date": "2014-01-06",
            "value": 161576838
        },
        {
            "date": "2014-01-07",
            "value": 161413854
        },
        {
            "date": "2014-01-08",
            "value": 152177211
        },
        {
            "date": "2014-01-09",
            "value": 140762210
        },
        {
            "date": "2014-01-10",
            "value": 144381072
        },
        {
            "date": "2014-01-11",
            "value": 154352310
        },
        {
            "date": "2014-01-12",
            "value": 165531790
        },
        {
            "date": "2014-01-13",
            "value": 175748881
        },
        {
            "date": "2014-01-14",
            "value": 187064037
        },
        {
            "date": "2014-01-15",
            "value": 197520685
        },
        {
            "date": "2014-01-16",
            "value": 210176418
        },
        {
            "date": "2014-01-17",
            "value": 196122924
        },
        {
            "date": "2014-01-18",
            "value": 207337480
        },
        {
            "date": "2014-01-19",
            "value": 200258882
        },
        {
            "date": "2014-01-20",
            "value": 186829538
        },
        {
            "date": "2014-01-21",
            "value": 192456897
        },
        {
            "date": "2014-01-22",
            "value": 204299711
        },
        {
            "date": "2014-01-23",
            "value": 192759017
        },
        {
            "date": "2014-01-24",
            "value": 203596183
        },
        {
            "date": "2014-01-25",
            "value": 208107346
        },
        {
            "date": "2014-01-26",
            "value": 196359852
        },
        {
            "date": "2014-01-27",
            "value": 192570783
        },
        {
            "date": "2014-01-28",
            "value": 177967768
        },
        {
            "date": "2014-01-29",
            "value": 190632803
        },
        {
            "date": "2014-01-30",
            "value": 203725316
        },
        {
            "date": "2014-01-31",
            "value": 218226177
        },
        {
            "date": "2014-02-01",
            "value": 210698669
        },
        {
            "date": "2014-02-02",
            "value": 217640656
        },
        {
            "date": "2014-02-03",
            "value": 216142362
        },
        {
            "date": "2014-02-04",
            "value": 201410971
        },
        {
            "date": "2014-02-05",
            "value": 196704289
        },
        {
            "date": "2014-02-06",
            "value": 190436945
        },
        {
            "date": "2014-02-07",
            "value": 178891686
        },
        {
            "date": "2014-02-08",
            "value": 171613962
        },
        {
            "date": "2014-02-09",
            "value": 157579773
        },
        {
            "date": "2014-02-10",
            "value": 158677098
        },
        {
            "date": "2014-02-11",
            "value": 147129977
        },
        {
            "date": "2014-02-12",
            "value": 151561876
        },
        {
            "date": "2014-02-13",
            "value": 151627421
        },
        {
            "date": "2014-02-14",
            "value": 143543872
        },
        {
            "date": "2014-02-15",
            "value": 136581057
        },
        {
            "date": "2014-02-16",
            "value": 135560715
        },
        {
            "date": "2014-02-17",
            "value": 122625263
        },
        {
            "date": "2014-02-18",
            "value": 112091484
        },
        {
            "date": "2014-02-19",
            "value": 98810329
        },
        {
            "date": "2014-02-20",
            "value": 99882912
        },
        {
            "date": "2014-02-21",
            "value": 94943095
        },
        {
            "date": "2014-02-22",
            "value": 104875743
        },
        {
            "date": "2014-02-23",
            "value": 116383678
        },
        {
            "date": "2014-02-24",
            "value": 125028841
        },
        {
            "date": "2014-02-25",
            "value": 123967310
        },
        {
            "date": "2014-02-26",
            "value": 133167029
        },
        {
            "date": "2014-02-27",
            "value": 128577263
        },
        {
            "date": "2014-02-28",
            "value": 115836969
        },
        {
            "date": "2014-03-01",
            "value": 119264529
        },
        {
            "date": "2014-03-02",
            "value": 109363374
        },
        {
            "date": "2014-03-03",
            "value": 113985628
        },
        {
            "date": "2014-03-04",
            "value": 114650999
        },
        {
            "date": "2014-03-05",
            "value": 110866108
        },
        {
            "date": "2014-03-06",
            "value": 96473454
        },
        {
            "date": "2014-03-07",
            "value": 104075886
        },
        {
            "date": "2014-03-08",
            "value": 103568384
        },
        {
            "date": "2014-03-09",
            "value": 101534883
        },
        {
            "date": "2014-03-10",
            "value": 115825447
        },
        {
            "date": "2014-03-11",
            "value": 126133916
        },
        {
            "date": "2014-03-12",
            "value": 116502109
        },
        {
            "date": "2014-03-13",
            "value": 130169411
        },
        {
            "date": "2014-03-14",
            "value": 124296886
        },
        {
            "date": "2014-03-15",
            "value": 126347399
        },
        {
            "date": "2014-03-16",
            "value": 131483669
        },
        {
            "date": "2014-03-17",
            "value": 142811333
        },
        {
            "date": "2014-03-18",
            "value": 129675396
        },
        {
            "date": "2014-03-19",
            "value": 115514483
        },
        {
            "date": "2014-03-20",
            "value": 117630630
        },
        {
            "date": "2014-03-21",
            "value": 122340239
        },
        {
            "date": "2014-03-22",
            "value": 132349091
        },
        {
            "date": "2014-03-23",
            "value": 125613305
        },
        {
            "date": "2014-03-24",
            "value": 135592466
        },
        {
            "date": "2014-03-25",
            "value": 123408762
        },
        {
            "date": "2014-03-26",
            "value": 111991454
        },
        {
            "date": "2014-03-27",
            "value": 116123955
        },
        {
            "date": "2014-03-28",
            "value": 112817214
        },
        {
            "date": "2014-03-29",
            "value": 113029590
        },
        {
            "date": "2014-03-30",
            "value": 108753398
        },
        {
            "date": "2014-03-31",
            "value": 99383763
        },
        {
            "date": "2014-04-01",
            "value": 100151737
        },
        {
            "date": "2014-04-02",
            "value": 94985209
        },
        {
            "date": "2014-04-03",
            "value": 82913669
        },
        {
            "date": "2014-04-04",
            "value": 78748268
        },
        {
            "date": "2014-04-05",
            "value": 63829135
        },
        {
            "date": "2014-04-06",
            "value": 78694727
        },
        {
            "date": "2014-04-07",
            "value": 80868994
        },
        {
            "date": "2014-04-08",
            "value": 93799013
        },
        {
            "date": "2014-04-09",
            "value": 99042416
        },
        {
            "date": "2014-04-10",
            "value": 97298692
        },
        {
            "date": "2014-04-11",
            "value": 83353499
        },
        {
            "date": "2014-04-12",
            "value": 71248129
        },
        {
            "date": "2014-04-13",
            "value": 75253744
        },
        {
            "date": "2014-04-14",
            "value": 68976648
        },
        {
            "date": "2014-04-15",
            "value": 71002284
        },
        {
            "date": "2014-04-16",
            "value": 75052401
        },
        {
            "date": "2014-04-17",
            "value": 83894030
        },
        {
            "date": "2014-04-18",
            "value": 90236528
        },
        {
            "date": "2014-04-19",
            "value": 99739114
        },
        {
            "date": "2014-04-20",
            "value": 96407136
        },
        {
            "date": "2014-04-21",
            "value": 108323177
        },
        {
            "date": "2014-04-22",
            "value": 101578914
        },
        {
            "date": "2014-04-23",
            "value": 115877608
        },
        {
            "date": "2014-04-24",
            "value": 112088857
        },
        {
            "date": "2014-04-25",
            "value": 112071353
        },
        {
            "date": "2014-04-26",
            "value": 101790062
        },
        {
            "date": "2014-04-27",
            "value": 115003761
        },
        {
            "date": "2014-04-28",
            "value": 120457727
        },
        {
            "date": "2014-04-29",
            "value": 118253926
        },
        {
            "date": "2014-04-30",
            "value": 117956992
        }
    ],
    [{
        "date": "2014-01-01",
        "value": 50000000
    },
        {
            "date": "2014-01-02",
            "value": 60379978
        },
        {
            "date": "2014-01-03",
            "value": 40493749
        },
        {
            "date": "2014-01-04",
            "value": 60785250
        },
        {
            "date": "2014-01-05",
            "value": 67391904
        },
        {
            "date": "2014-01-06",
            "value": 61576838
        },
        {
            "date": "2014-01-07",
            "value": 61413854
        },
        {
            "date": "2014-01-08",
            "value": 82177211
        },
        {
            "date": "2014-01-09",
            "value": 103762210
        },
        {
            "date": "2014-01-10",
            "value": 84381072
        },
        {
            "date": "2014-01-11",
            "value": 54352310
        },
        {
            "date": "2014-01-12",
            "value": 65531790
        },
        {
            "date": "2014-01-13",
            "value": 75748881
        },
        {
            "date": "2014-01-14",
            "value": 47064037
        },
        {
            "date": "2014-01-15",
            "value": 67520685
        },
        {
            "date": "2014-01-16",
            "value": 60176418
        },
        {
            "date": "2014-01-17",
            "value": 66122924
        },
        {
            "date": "2014-01-18",
            "value": 57337480
        },
        {
            "date": "2014-01-19",
            "value": 100258882
        },
        {
            "date": "2014-01-20",
            "value": 46829538
        },
        {
            "date": "2014-01-21",
            "value": 92456897
        },
        {
            "date": "2014-01-22",
            "value": 94299711
        },
        {
            "date": "2014-01-23",
            "value": 62759017
        },
        {
            "date": "2014-01-24",
            "value": 103596183
        },
        {
            "date": "2014-01-25",
            "value": 108107346
        },
        {
            "date": "2014-01-26",
            "value": 66359852
        },
        {
            "date": "2014-01-27",
            "value": 62570783
        },
        {
            "date": "2014-01-28",
            "value": 77967768
        },
        {
            "date": "2014-01-29",
            "value": 60632803
        },
        {
            "date": "2014-01-30",
            "value": 103725316
        },
        {
            "date": "2014-01-31",
            "value": 98226177
        },
        {
            "date": "2014-02-01",
            "value": 60698669
        },
        {
            "date": "2014-02-02",
            "value": 67640656
        },
        {
            "date": "2014-02-03",
            "value": 66142362
        },
        {
            "date": "2014-02-04",
            "value": 101410971
        },
        {
            "date": "2014-02-05",
            "value": 66704289
        },
        {
            "date": "2014-02-06",
            "value": 60436945
        },
        {
            "date": "2014-02-07",
            "value": 78891686
        },
        {
            "date": "2014-02-08",
            "value": 71613962
        },
        {
            "date": "2014-02-09",
            "value": 107579773
        },
        {
            "date": "2014-02-10",
            "value": 58677098
        },
        {
            "date": "2014-02-11",
            "value": 87129977
        },
        {
            "date": "2014-02-12",
            "value": 51561876
        },
        {
            "date": "2014-02-13",
            "value": 51627421
        },
        {
            "date": "2014-02-14",
            "value": 83543872
        },
        {
            "date": "2014-02-15",
            "value": 66581057
        },
        {
            "date": "2014-02-16",
            "value": 65560715
        },
        {
            "date": "2014-02-17",
            "value": 62625263
        },
        {
            "date": "2014-02-18",
            "value": 92091484
        },
        {
            "date": "2014-02-19",
            "value": 48810329
        },
        {
            "date": "2014-02-20",
            "value": 49882912
        },
        {
            "date": "2014-02-21",
            "value": 44943095
        },
        {
            "date": "2014-02-22",
            "value": 104875743
        },
        {
            "date": "2014-02-23",
            "value": 96383678
        },
        {
            "date": "2014-02-24",
            "value": 105028841
        },
        {
            "date": "2014-02-25",
            "value": 63967310
        },
        {
            "date": "2014-02-26",
            "value": 63167029
        },
        {
            "date": "2014-02-27",
            "value": 68577263
        },
        {
            "date": "2014-02-28",
            "value": 95836969
        },
        {
            "date": "2014-03-01",
            "value": 99264529
        },
        {
            "date": "2014-03-02",
            "value": 109363374
        },
        {
            "date": "2014-03-03",
            "value": 93985628
        },
        {
            "date": "2014-03-04",
            "value": 94650999
        },
        {
            "date": "2014-03-05",
            "value": 90866108
        },
        {
            "date": "2014-03-06",
            "value": 46473454
        },
        {
            "date": "2014-03-07",
            "value": 84075886
        },
        {
            "date": "2014-03-08",
            "value": 103568384
        },
        {
            "date": "2014-03-09",
            "value": 101534883
        },
        {
            "date": "2014-03-10",
            "value": 95825447
        },
        {
            "date": "2014-03-11",
            "value": 66133916
        },
        {
            "date": "2014-03-12",
            "value": 96502109
        },
        {
            "date": "2014-03-13",
            "value": 80169411
        },
        {
            "date": "2014-03-14",
            "value": 84296886
        },
        {
            "date": "2014-03-15",
            "value": 86347399
        },
        {
            "date": "2014-03-16",
            "value": 31483669
        },
        {
            "date": "2014-03-17",
            "value": 82811333
        },
        {
            "date": "2014-03-18",
            "value": 89675396
        },
        {
            "date": "2014-03-19",
            "value": 95514483
        },
        {
            "date": "2014-03-20",
            "value": 97630630
        },
        {
            "date": "2014-03-21",
            "value": 62340239
        },
        {
            "date": "2014-03-22",
            "value": 62349091
        },
        {
            "date": "2014-03-23",
            "value": 65613305
        },
        {
            "date": "2014-03-24",
            "value": 65592466
        },
        {
            "date": "2014-03-25",
            "value": 63408762
        },
        {
            "date": "2014-03-26",
            "value": 91991454
        },
        {
            "date": "2014-03-27",
            "value": 96123955
        },
        {
            "date": "2014-03-28",
            "value": 92817214
        },
        {
            "date": "2014-03-29",
            "value": 93029590
        },
        {
            "date": "2014-03-30",
            "value": 108753398
        },
        {
            "date": "2014-03-31",
            "value": 49383763
        },
        {
            "date": "2014-04-01",
            "value": 100151737
        },
        {
            "date": "2014-04-02",
            "value": 44985209
        },
        {
            "date": "2014-04-03",
            "value": 52913669
        },
        {
            "date": "2014-04-04",
            "value": 48748268
        },
        {
            "date": "2014-04-05",
            "value": 23829135
        },
        {
            "date": "2014-04-06",
            "value": 58694727
        },
        {
            "date": "2014-04-07",
            "value": 50868994
        },
        {
            "date": "2014-04-08",
            "value": 43799013
        },
        {
            "date": "2014-04-09",
            "value": 4042416
        },
        {
            "date": "2014-04-10",
            "value": 47298692
        },
        {
            "date": "2014-04-11",
            "value": 53353499
        },
        {
            "date": "2014-04-12",
            "value": 71248129
        },
        {
            "date": "2014-04-13",
            "value": 75253744
        },
        {
            "date": "2014-04-14",
            "value": 68976648
        },
        {
            "date": "2014-04-15",
            "value": 71002284
        },
        {
            "date": "2014-04-16",
            "value": 75052401
        },
        {
            "date": "2014-04-17",
            "value": 83894030
        },
        {
            "date": "2014-04-18",
            "value": 50236528
        },
        {
            "date": "2014-04-19",
            "value": 59739114
        },
        {
            "date": "2014-04-20",
            "value": 56407136
        },
        {
            "date": "2014-04-21",
            "value": 108323177
        },
        {
            "date": "2014-04-22",
            "value": 101578914
        },
        {
            "date": "2014-04-23",
            "value": 95877608
        },
        {
            "date": "2014-04-24",
            "value": 62088857
        },
        {
            "date": "2014-04-25",
            "value": 92071353
        },
        {
            "date": "2014-04-26",
            "value": 81790062
        },
        {
            "date": "2014-04-27",
            "value": 105003761
        },
        {
            "date": "2014-04-28",
            "value": 100457727
        },
        {
            "date": "2014-04-29",
            "value": 98253926
        },
        {
            "date": "2014-04-30",
            "value": 67956992
        }
    ]
]
// basic area chart
var options = {
    chart: {
        height: 350,
        type: 'area',
        zoom: {
            enabled: false
        }

    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    series: [{
        name: "STOCK ABC",
        data: series.monthDataSeries1.prices
    }],
    title: {
        text: 'Fundamental Analysis of Stocks',
        align: 'left'
    },
    subtitle: {
        text: 'Price Movements',
        align: 'left'
    },
    labels: series.monthDataSeries1.dates,
    xaxis: {
        type: 'datetime',
    },
    yaxis: {
        opposite: true
    },
    legend: {
        horizontalAlign: 'left'
    },
    colors:['#7e37d8']

}

var chart = new ApexCharts(
    document.querySelector("#basic-apex"),
    options
);

chart.render();

// area spaline chart
var options1 = {
    chart: {
        height: 350,
        type: 'area',
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'smooth'
    },
    series: [{
        name: 'series1',
        data: [31, 40, 28, 51, 42, 109, 100]
    }, {
        name: 'series2',
        data: [11, 32, 45, 32, 34, 52, 41]
    }],

    xaxis: {
        type: 'datetime',
        categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],
    },
    tooltip: {
        x: {
            format: 'dd/MM/yy HH:mm'
        },
    },
    colors:['#7e37d8', '#fd517d']
}

var chart1 = new ApexCharts(
    document.querySelector("#area-spaline"),
    options1
);

chart1.render();

// basic bar chart
var options2 = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        data: [400, 430, 448, 470, 540, 580, 690, 1100, 1200, 1380]
    }],
    xaxis: {
        categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy', 'France', 'Japan', 'United States', 'China', 'Germany'],
    },
    colors:['#7e37d8']
}

var chart2 = new ApexCharts(
    document.querySelector("#basic-bar"),
    options2
);

chart2.render();

// column chart
var options3 = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'TK',
        data: [1141, 728, 705, 443, 781, 350, 610, 422, 223,199, 287, 162, 268, 192,410,218,236,212,146,141,110,166,84,296,203,91]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#7e37d8', '#fe80b2', '#80cf00']
}

var chart3 = new ApexCharts(
    document.querySelector("#column-chart"),
    options3
);

chart3.render();


// column chart
var optionsKB = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'KB',
        data: [865, 1238, 709, 866, 1164, 375, 679, 1011, 1389,868, 377, 725, 744, 387,352,456,257,494,453,775,290,193,440,86,622,363]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#fe80b2', '#fe80b2', '#80cf00']
}

var chartKB = new ApexCharts(
    document.querySelector("#column-chartKB"),
    optionsKB
);

chartKB.render();

// column chart
var optionsPKBM = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'PKBM',
        data: [394, 233, 507, 249, 558, 743, 223, 489, 285,278, 0, 255, 286, 0,350,313,536,401,352,0,506,0,0,244,0,0]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#80cf00', '#fe80b2', '#80cf00']
}

var chartPKBM = new ApexCharts(
    document.querySelector("#column-chartPKBM"),
    optionsPKBM
);

chartPKBM.render();


// column chart
var optionsSD = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'SD',
        data: [7967, 8685, 7320, 9139, 5936, 3715, 4749, 5410, 3961,3782, 3152, 3546, 2439, 2499,2913,2595,2490,2378,2018,2372,2185,1388,1670,1809,1365,950]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#32a891', '#fe80b2', '#80cf00']
}

var chartsSD = new ApexCharts(
    document.querySelector("#column-chartSD"),
    optionsSD
);

chartsSD.render();

// column chart
var optionsSMP = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'SMP',
        data: [2884, 3152, 5432, 3386, 3700, 1515, 1953, 1571, 1394,1495, 1467, 1501, 1149, 1156,936,1149,232,640,905,779,529,820,764,346,314,215]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#e5fa4b', '#fe80b2', '#80cf00']
}

var chartsSMP = new ApexCharts(
    document.querySelector("#column-chartSMP"),
    optionsSMP
);

chartsSMP.render();

// column chart
var optionsSMA = {
    chart: {
        height: 350,
        type: 'bar',
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'SMA',
        data: [2884, 3152, 5432, 3386, 3700, 1515, 1953, 1571, 1394,1495, 1467, 1501, 1149, 1156,936,1149,232,640,905,779,529,820,764,346,314,215]
    // }, {
    //     name: 'KB',
    //     data: [865, 1238, 101, 98, 87, 105, 91, 114, 94,76, 85, 101, 98, 87]
    // }, {
    //     name: 'TPA',
    //     data: [0, 0, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SPS',
    //     data: [40, 22, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'PKBM',
    //     data: [394, 233, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SKB',
    //     data: [0, 180, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SD',
    //     data: [7967, 8685, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    // },{
    //     name: 'SMP',
    //     data: [2884, 3152, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMA',
    //     data: [1919, 1811, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SMK',
    //     data: [1049, 3646, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    //  },{
    //     name: 'SLB',
    //     data: [140, 148, 36, 26, 45, 48, 52, 53, 41,76, 85, 101, 98, 87]
    }
    ],
    xaxis: {
        categories: ['Kec. Bogor Utara', 'Kec. Tanjungsari', 'Kec. Jatinangor', 'Kec. Cimanggung', 'Kec. Bogor Selatan', 'Kec. Situraja', 'Kec. Cimalaka', 'Kec. Pamulihan', 'Kec. Jatinunggal','Kec. Rancakalong','Kec. Darmaraja','Kec. Wado','Kec. Tanjungkerta','Kec. Buahdua','Kec. Paseh','Kec. Cisitu','Kec. Ujungjaya','Kec. Sukasari','Kec. Conggeang','Kec. Cibugel','Kec. Ganeas','Kec. Tomo','Kec. Jatigede','Kec. Cisarua','Kec. Tanjungmedar','Kec. Surian'],
    },
    yaxis: {
        title: {
            text: ''
        }
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return "" + val + " "
            }
        }
    },
    colors:['#585df5', '#fe80b2', '#80cf00']
}

var chartsSMA = new ApexCharts(
    document.querySelector("#column-chartSMA"),
    optionsSMA
);

chartsSMA.render();




// 3d bubble chart

function generateData(baseval, count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
        //var x =Math.floor(Math.random() * (750 - 1 + 1)) + 1;;
        var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
        var z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;

        series.push([baseval, y, z]);
        baseval += 86400000;
        i++;
    }
    return series;
}


var options = {
    chart: {
        height: 350,
        type: 'bubble',
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        name: 'Product1',
        data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
            min: 10,
            max: 60
        })
    },
        {
            name: 'Product2',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'Product3',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'Product4',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        }
    ],
    fill: {
        type: 'gradient',
    },
    title: {
        text: '3D Bubble Chart'
    },
    xaxis: {
        tickAmount: 12,
        type: 'datetime',

        labels: {
            rotate: 0,
        }
    },
    yaxis: {
        max: 70
    },
    theme: {
        palette: 'palette2'
    },
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd']
}

var chart = new ApexCharts(
    document.querySelector("#chart-bubble"),
    options
);

chart.render();

// candlestick chart
var options4 = {
    chart: {
        height: 350,
        type: 'candlestick',
    },
    plotOptions: {
        candlestick: {
            colors: {
                upward: '#7e37d8',
                downward: '#fe80b2'
            }
        }
    },
    series: [{
        data: [{
            x: new Date(1538778600000),
            y: [6629.81, 6650.5, 6623.04, 6633.33]
        },
            {
                x: new Date(1538780400000),
                y: [6632.01, 6643.59, 6620, 6630.11]
            },
            {
                x: new Date(1538782200000),
                y: [6630.71, 6648.95, 6623.34, 6635.65]
            },
            {
                x: new Date(1538784000000),
                y: [6635.65, 6651, 6629.67, 6638.24]
            },
            {
                x: new Date(1538785800000),
                y: [6638.24, 6640, 6620, 6624.47]
            },
            {
                x: new Date(1538787600000),
                y: [6624.53, 6636.03, 6621.68, 6624.31]
            },
            {
                x: new Date(1538789400000),
                y: [6624.61, 6632.2, 6617, 6626.02]
            },
            {
                x: new Date(1538791200000),
                y: [6627, 6627.62, 6584.22, 6603.02]
            },
            {
                x: new Date(1538793000000),
                y: [6605, 6608.03, 6598.95, 6604.01]
            },
            {
                x: new Date(1538794800000),
                y: [6604.5, 6614.4, 6602.26, 6608.02]
            },
            {
                x: new Date(1538796600000),
                y: [6608.02, 6610.68, 6601.99, 6608.91]
            },
            {
                x: new Date(1538798400000),
                y: [6608.91, 6618.99, 6608.01, 6612]
            },
            {
                x: new Date(1538800200000),
                y: [6612, 6615.13, 6605.09, 6612]
            },
            {
                x: new Date(1538802000000),
                y: [6612, 6624.12, 6608.43, 6622.95]
            },
            {
                x: new Date(1538803800000),
                y: [6623.91, 6623.91, 6615, 6615.67]
            },
            {
                x: new Date(1538805600000),
                y: [6618.69, 6618.74, 6610, 6610.4]
            },
            {
                x: new Date(1538807400000),
                y: [6611, 6622.78, 6610.4, 6614.9]
            },
            {
                x: new Date(1538809200000),
                y: [6614.9, 6626.2, 6613.33, 6623.45]
            },
            {
                x: new Date(1538811000000),
                y: [6623.48, 6627, 6618.38, 6620.35]
            },
            {
                x: new Date(1538812800000),
                y: [6619.43, 6620.35, 6610.05, 6615.53]
            },
            {
                x: new Date(1538814600000),
                y: [6615.53, 6617.93, 6610, 6615.19]
            },
            {
                x: new Date(1538816400000),
                y: [6615.19, 6621.6, 6608.2, 6620]
            },
            {
                x: new Date(1538818200000),
                y: [6619.54, 6625.17, 6614.15, 6620]
            },
            {
                x: new Date(1538820000000),
                y: [6620.33, 6634.15, 6617.24, 6624.61]
            },
            {
                x: new Date(1538821800000),
                y: [6625.95, 6626, 6611.66, 6617.58]
            },
            {
                x: new Date(1538823600000),
                y: [6619, 6625.97, 6595.27, 6598.86]
            },
            {
                x: new Date(1538825400000),
                y: [6598.86, 6598.88, 6570, 6587.16]
            },
            {
                x: new Date(1538827200000),
                y: [6588.86, 6600, 6580, 6593.4]
            },
            {
                x: new Date(1538829000000),
                y: [6593.99, 6598.89, 6585, 6587.81]
            },
            {
                x: new Date(1538830800000),
                y: [6587.81, 6592.73, 6567.14, 6578]
            },
            {
                x: new Date(1538832600000),
                y: [6578.35, 6581.72, 6567.39, 6579]
            },
            {
                x: new Date(1538834400000),
                y: [6579.38, 6580.92, 6566.77, 6575.96]
            },
            {
                x: new Date(1538836200000),
                y: [6575.96, 6589, 6571.77, 6588.92]
            },
            {
                x: new Date(1538838000000),
                y: [6588.92, 6594, 6577.55, 6589.22]
            },
            {
                x: new Date(1538839800000),
                y: [6589.3, 6598.89, 6589.1, 6596.08]
            },
            {
                x: new Date(1538841600000),
                y: [6597.5, 6600, 6588.39, 6596.25]
            },
            {
                x: new Date(1538843400000),
                y: [6598.03, 6600, 6588.73, 6595.97]
            },
            {
                x: new Date(1538845200000),
                y: [6595.97, 6602.01, 6588.17, 6602]
            },
            {
                x: new Date(1538847000000),
                y: [6602, 6607, 6596.51, 6599.95]
            },
            {
                x: new Date(1538848800000),
                y: [6600.63, 6601.21, 6590.39, 6591.02]
            },
            {
                x: new Date(1538850600000),
                y: [6591.02, 6603.08, 6591, 6591]
            },
            {
                x: new Date(1538852400000),
                y: [6591, 6601.32, 6585, 6592]
            },
            {
                x: new Date(1538854200000),
                y: [6593.13, 6596.01, 6590, 6593.34]
            },
            {
                x: new Date(1538856000000),
                y: [6593.34, 6604.76, 6582.63, 6593.86]
            },
            {
                x: new Date(1538857800000),
                y: [6593.86, 6604.28, 6586.57, 6600.01]
            },
            {
                x: new Date(1538859600000),
                y: [6601.81, 6603.21, 6592.78, 6596.25]
            },
            {
                x: new Date(1538861400000),
                y: [6596.25, 6604.2, 6590, 6602.99]
            },
            {
                x: new Date(1538863200000),
                y: [6602.99, 6606, 6584.99, 6587.81]
            },
            {
                x: new Date(1538865000000),
                y: [6587.81, 6595, 6583.27, 6591.96]
            },
            {
                x: new Date(1538866800000),
                y: [6591.97, 6596.07, 6585, 6588.39]
            },
            {
                x: new Date(1538868600000),
                y: [6587.6, 6598.21, 6587.6, 6594.27]
            },
            {
                x: new Date(1538870400000),
                y: [6596.44, 6601, 6590, 6596.55]
            },
            {
                x: new Date(1538872200000),
                y: [6598.91, 6605, 6596.61, 6600.02]
            },
            {
                x: new Date(1538874000000),
                y: [6600.55, 6605, 6589.14, 6593.01]
            },
            {
                x: new Date(1538875800000),
                y: [6593.15, 6605, 6592, 6603.06]
            },
            {
                x: new Date(1538877600000),
                y: [6603.07, 6604.5, 6599.09, 6603.89]
            },
            {
                x: new Date(1538879400000),
                y: [6604.44, 6604.44, 6600, 6603.5]
            },
            {
                x: new Date(1538881200000),
                y: [6603.5, 6603.99, 6597.5, 6603.86]
            },
            {
                x: new Date(1538883000000),
                y: [6603.85, 6605, 6600, 6604.07]
            },
            {
                x: new Date(1538884800000),
                y: [6604.98, 6606, 6604.07, 6606]
            },
        ]
    }],
    title: {
        text: 'CandleStick Chart',
        align: 'left'
    },
    xaxis: {
        type: 'datetime'
    },
    yaxis: {
        tooltip: {
            enabled: true
        }
    },
    colors:['#000000']
}

var chart4 = new ApexCharts(
    document.querySelector("#candlestick"),
    options4
);

chart4.render();

// stepline chart
var ts2 = 1484418600000;
var data = [];
var spikes = [5, -5, 3, -3, 8, -8]
for (var i = 0; i < 30; i++) {
    ts2 = ts2 + 86400000;
    var innerArr = [ts2, dataSeries[1][i].value];
    data.push(innerArr)
}

var options5 = {
    chart: {
        type: 'line',
        height: 350
    },
    stroke: {
        curve: 'stepline',
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        data: [34, 44, 54, 21, 12, 43, 33, 23, 66, 66, 58]
    }],
    title: {
        text: 'Stepline Chart',
        align: 'left'
    },
    markers: {
        hover: {
            sizeOffset: 4
        }
    },
    colors:['#7e37d8']

}

var chart5 = new ApexCharts(
    document.querySelector("#stepline"),
    options5
);

chart5.render();

// annotation chart
var options6 = {
    annotations: {
        yaxis: [{
            y: 8200,
            borderColor: '#00E396',
            label: {
                borderColor: '#00E396',
                style: {
                    color: '#fff',
                    background: '#00E396',
                },
                text: 'Support',
            }
        }, {
            y: 8600,
            y2: 9000,
            borderColor: '#000',
            fillColor: '#FEB019',
            opacity: 0.2,
            label: {
                borderColor: '#333',
                style: {
                    fontSize: '10px',
                    color: '#333',
                    background: '#FEB019',
                },
                text: 'Y-axis range',
            }
        }],
        xaxis: [{
            x: new Date('23 Nov 2017').getTime(),
            strokeDashArray: 0,
            borderColor: '#775DD0',
            label: {
                borderColor: '#775DD0',
                style: {
                    color: '#fff',
                    background: '#775DD0',
                },
                text: 'Anno Test',
            }
        }, {
            x: new Date('26 Nov 2017').getTime(),
            x2: new Date('28 Nov 2017').getTime(),
            fillColor: '#fe80b2',
            opacity: 0.4,
            label: {
                borderColor: '#B3F7CA',
                style: {
                    fontSize: '10px',
                    color: '#fff',
                    background: '#00E396',
                },
                offsetY: -10,
                text: 'X-axis range',
            }
        }],
        points: [{
            x: new Date('01 Dec 2017').getTime(),
            y: 8607.55,
            marker: {
                size: 8,
                fillColor: '#fff',
                strokeColor: 'red',
                radius: 2,
                cssClass: 'apexcharts-custom-class'
            },
            label: {
                borderColor: '#FF4560',
                offsetY: 0,
                style: {
                    color: '#fff',
                    background: '#FF4560',
                },

                text: 'Point Annotation',
            }
        }]
    },
    chart: {
        height: 350,
        type: 'line',
        id: 'areachart-2'
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    grid: {
        padding: {
            right: 30,
            left: 20
        }
    },
    series: [{
        data: series.monthDataSeries1.prices
    }],
    title: {
        text: 'Line with Annotations',
        align: 'left'
    },
    labels: series.monthDataSeries1.dates,
    xaxis: {
        type: 'datetime',
    },
    colors:['#7e37d8']
}

var chart6 = new ApexCharts(
    document.querySelector("#annotationchart"),
    options6
);

chart6.render();

// mixed chart
var options7 = {
    chart: {
        height: 350,
        type: 'line',
        stacked: false,
    },
    stroke: {
        width: [0, 2, 5],
        curve: 'smooth'
    },
    plotOptions: {
        bar: {
            columnWidth: '50%'
        }
    },
    colors: ['#3A5794', '#A5C351', '#E14A84'],
    series: [{
        name: 'Column',
        type: 'column',
        data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30]
    }, {
        name: 'Area',
        type: 'area',
        data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43]
    }, {
        name: 'Line',
        type: 'line',
        data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39]
    }],
    fill: {
        opacity: [0.85,0.25,1],
        gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
        }
    },
    labels: ['01/01/2003', '02/01/2003','03/01/2003','04/01/2003','05/01/2003','06/01/2003','07/01/2003','08/01/2003','09/01/2003','10/01/2003','11/01/2003'],
    markers: {
        size: 0
    },
    xaxis: {
        type:'datetime'
    },
    yaxis: {
        min: 0
    },
    tooltip: {
        shared: true,
        intersect: false,
        y: {
            formatter: function (y) {
                if(typeof y !== "undefined") {
                    return  y.toFixed(0) + " views";
                }
                return y;

            }
        }
    },
    legend: {
        labels: {
            useSeriesColors: true
        },
    },
    colors:['#7e37d8','#fd517d','#158df7']
}

var chart7 = new ApexCharts(
    document.querySelector("#mixedchart"),
    options7
);

chart7.render();

// pie chart
var options8 = {
    chart: {
        width: 600,
        type: 'pie',
    },
    labels: ['Taman Kanak Kanak', 'Kelompok Belajar', 'PKBM', 'Sekolah Dasar', 'SMP', 'SMA'],
    series: [565, 1263, 139, 5924, 2549, 872],
    responsive: [{
        breakpoint: 100,
        options: {
            chart: {
                width: 600
            },
            legend: {
                position: 'center'
            }
        }
    }],
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d','#eb4034']
}

var chart8 = new ApexCharts(
    document.querySelector("#piechart"),
    options8
);

chart8.render();

// donut chart
var options9 = {
    chart: {
        width: 380,
        type: 'donut',
    },
    series: [10, 55, 41, 17, 15],
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d']
}

var chart9 = new ApexCharts(
    document.querySelector("#donutchart"),
    options9
);

chart9.render();

// radar chart
var options10 = {
    chart: {
        height: 350,
        type: 'radar',
    },
    series: [{
        name: 'Series 1',
        data: [20, 100, 40, 30, 50, 80, 33],
    }],
    labels: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    plotOptions: {
        radar: {
            size: 140,
            polygons: {
                strokeColor: '#e9e9e9',
                fill: {
                    colors: ['rgba(126,55,216,0.1)', '#fff'],
                }
            }
        }
    },
    title: {
        text: 'Radar with Polygon Fill'
    },
    colors: ['#fe80b2'],
    markers: {
        size: 4,
        colors: ['#fe80b2'],
        strokeColor: '#fe80b2',
        strokeWidth: 2,
    },
    tooltip: {
        y: {
            formatter: function(val) {
                return val
            }
        }
    },
    yaxis: {
        tickAmount: 7,
        labels: {
            formatter: function(val, i) {
                if(i % 2 === 0) {
                    return val
                } else {
                    return ''
                }
            }
        }
    },
    colors:['#fe80b2', '#000000']
}

var chart10 = new ApexCharts(
    document.querySelector("#radarchart"),
    options10
);

chart10.render();

// circle chart
var options11 = {
    chart: {
        height: 350,
        type: 'radialBar',
    },
    plotOptions: {
        radialBar: {
            dataLabels: {
                name: {
                    fontSize: '22px',
                },
                value: {
                    fontSize: '16px',
                },
                total: {
                    show: true,
                    label: 'Total',
                    formatter: function (w) {
                        return 249
                    }
                }
            }
        }
    },
    series: [44, 55, 67, 83],
    labels: ['Apples', 'Oranges', 'Bananas', 'Berries'],
    colors:['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd']


}

var chart11 = new ApexCharts(
    document.querySelector("#circlechart"),
    options11
);

chart11.render();


  </script>

</body>

</html>