 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Header</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url();?>manage_media/img_header">Manage Header</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">add</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <?php if (!empty($message)) echo "
                    <div class='alert alert-$message_type'>$message</div>";?>
                    <form role="form" class="form-horizontal " method='post' enctype="multipart/form-data">
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" />
                        <div class="form-group">
                            <label class="control">Judul</label>
                            <input type="text" class="form-control" name='judul' placeholder="" value="<?php echo $judul;?>">
                        </div>

                        <div class="form-group">
                            <label class="control">Deskripsi</label>
                            <textarea class="form-control" rows="4" name="deskripsi" id="deskripsi"><?php echo $deskripsi;?></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control">Link</label>
                            <input type="text" class="form-control" name='link' placeholder="" value="<?php echo $link;?>">
                        </div>
                                                
                        <div class="form-group">
                            <label class="control">Status</label>
                            <select name="status" class="form-control selectboxit" data-first-option="false">
                                <?php
									if ($status=="Active"){
										echo "
											<option value='Active' selected>Active</option>
											<option value='Not Active'>Not Active</option>
										";
									}
									else{
										echo "
											<option value='Active' >Active</option>
											<option value='Not Active' selected>Not Active</option>
										";
									}
								?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control">Gambar Header</label>                                                        
                            <?php echo"
							<div class='member-img'>
								<img src='".base_url()."data/images/header/$gbr_header' class='img-rounded' style='max-width:600px' />
								
							</div><br>
							";?>
							<input type="file" name='userfile'  id="input-file-now-custom-3" class="dropify" data-label="<i class='glyphicon glyphicon-file'></i> Browse" />
							<p>
								Max : 2000px | 2MB
							</p>
                        </div>
                        <div class="form-group">

                            <div class="col-12">
                                <button type="submit" class="btn btn-primary waves-effect waves-light pull-right mb-1" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Perbaharui</button>

                            </div>

                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
