 <!-- BEGIN: Content-->
 <div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Manage Header</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Manage Header</a>
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">
          <div class="card">
            <div class="card-content">
                <div class="card-body">

                	<div class="pull-right">
					<form role="search" class="app-search hidden-xs m-r-10">
						<input type="text" name="s" placeholder="Search..." class="form-control" style="background:#eee;"> <a href="" class="active"></a>
					</form>
				</div>
				<h3 class="box-title"><a href="<?php echo base_url();?>manage_media/add_img_header" class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Tambah Baru</a></h3>
				<p class="text-muted m-b-20"></p>
				<div class="table-responsive">
					
					<table class="table table-striped datatable" id="data">
						<thead>
							<tr>
								<th>#</th>
								<th>Judul</th>
								<th>Gambar</th>
								<th>Status</th>
								<th width=70px>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$num = 1;
							foreach ($query as $row) {
								echo"
								<tr>
								<td>$num</td>
								<td>$row->judul</td>
								
								<td><a href='".base_url()."data/images/header/{$row->gbr_header}' class='image-popup-no-margins'><img src='".base_url()."data/images/header/{$row->gbr_header}' class='img-responsive thumbnail m-r-15' style='max-height:200px;'></a></td>
								<td>$row->status</td>
								<td>
								<a href='".base_url()."manage_media/edit_img_header/$row->id_header' class='btn-xs' title='Edit' data-toggle=\"tooltip\" data-original-title=\"Edit\">
								
								<i class=\"fa fa-pencil text-inverse m-r-10\"></i> 
								</a>
								<a class='btn-xs' title='Delete'  onclick='delete_(\"$row->id_header\")' data-toggle=\"tooltip\" data-original-title=\"Close\">
								<i class=\"fa fa-close text-danger\"></i>
								</a>
								
								</td>
								</tr>
								";

								$num++;
							}
							?>
						</tbody>
					</table>

				</div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
