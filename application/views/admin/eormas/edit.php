 <div class="page-body">
	<div class="container-fluid">
		<div class="page-header">
			<div class="row">
				<div class="col-lg-6 main-header">
					<h2>Ormas<span>Edit</span></h2>
					<h6 class="mb-0">Bogor Comand Center</h6>
				</div>
				<div class="col-lg-6 breadcrumb-right">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html"><i class="pe-7s-home"></i></a></li>
						<li class="breadcrumb-item">Apps </li>
						<li class="breadcrumb-item">Ormas</li>
						<li class="breadcrumb-item active">Ormas</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- Container-fluid starts-->
	<div class="container-fluid">
		<div class="card">
			<div class="card-content">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<form class="form-horizontal form-material" method='post' autocomplete="off">

								<!-- <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>" /> -->
								<?php if (!empty($message)) echo "
                        <div class='alert alert-$message_type'>$message</div>";?>
								<h3 class="text-primary">Data :</h3>
								<div class="form-group">
									<label class="col-md-12">Nama Ormas</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="nama_orkesmas" value=" <?= $data_eormas->nama_orkesmas; ?>" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Jenis Orkesmas</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="jns_orkesmas" value=" <?= $data_eormas->jns_orkesmas; ?>" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Alamat</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="alamat" value=" <?= $data_eormas->alamat; ?>" class="form-control form-control-line">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Bidang Kegiatan</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="bidang_kgt" value=" <?= $data_eormas->bidang_kgt; ?>" class="form-control form-control-line">
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-md-12">Tgl Pencatatan</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="tgl_pencatatan" value=" <?= $data_eormas->tgl_pencatatan; ?>" class="form-control form-control-line">
									</div>
								</div> -->
								<div class="form-group">
									<label class="col-md-12">No Telp</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="no_tlp" value=" <?= $data_eormas->no_tlp; ?>" class="form-control form-control-line">
									</div>
								<!-- </div>
								<div class="form-group">
									<label class="col-md-12">Total Ormas</label>
									<div class="col-md-12">
									<input type="hidden" class="form-control" id="id" name="id" value=" <?= $data_eormas->id; ?>">
										<input type="text" name="total_ormas" value=" <?= $data_eormas->total_ormas; ?>" class="form-control form-control-line">
									</div>
								</div> -->
							
								<div class="form-group">
									<label class="col-md-12">Tahun</label>
									<div class="col-md-12">
										<input type="text" name="tahun" class="form-control form-control-line" value=" <?= $data_eormas->tahun; ?>">
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-md-12">Desa</label>
									<div class="col-md-12">
										<input type="text" name="desa" class="form-control form-control-line" value=" <?= $data_eormas->desa; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Kecamatan</label>
									<div class="col-md-12">
										<input type="text" name="kecamatan" class="form-control form-control-line" value=" <?= $data_eormas->kecamatan; ?>">
									</div>
								</div> -->

						<div class="form-group">
							<div class="col-md-12 offset-md-12">
								<a href="<?=base_url();?>eormas" class="btn btn-light m-l-10"><span class="icon-angle-left"></span> Batal</a>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-12 offset-md-12"><button type="submit" class="btn btn-primary m-l-10" style="float: right;"><span class="icon-save"></span> Simpan</button>

							</div>
						</div>
						</form>
					</div>
				</div>