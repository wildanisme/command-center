<style type="text/css">
  #peta {
    height: 65vh;
    width: 100%;
  }
</style>

<div class="col-lg-12 xl-100">
  <div class="row">
    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5>List Ormas</h5>
          <div class="card-header-right">
            <?php
            $total_data_tahun_ini = count($data_eormas);
            if ($total_data_tahun_ini >= 1) {

            }
            ?>
            <a href="<?= $total_data_tahun_ini >= 1 ? '#' : site_url('eormas/add/') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus text-white"></i> </a>
          </div>
        </div>
        <div class="card-body p-0">
          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_card_">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama Ormas</th>
                  <th scope="col">Jenis</th>
                  <th scope="col">Bidang Kegiatan</th>
                  <th scope="col">Alamat</th>
                  <th scope="col">No Telp</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data_eormas as $key => $data): ?>
                <tr class="<?= $key >= 10 ? 'd-none' : '' ?>">
                  <td class="f-w-600"> <?php echo $data->id?> </td>
                  <td class="f-w-600"> <?php echo $data->nama_orkesmas?> </td>
                  <td class="f-w-600"> <?php echo $data->jns_orkesmas?> </td>
                  <td class="f-w-600"> <?php echo $data->bidang_kgt?> </td>
                  <td class="f-w-600"> <?php echo $data->alamat?> </td>
                  <td class="f-w-600"> <?php echo $data->no_tlp?> </td>
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('eormas/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <div id="pagination" class="mt-3">
              <button id="prev" class="btn btn-secondary btn-sm">Previous</button>
              <button id="next" class="btn btn-secondary btn-sm">Next</button>
              <select id="pageSelect" class="form-control form-control-sm">
                <!-- Options for selecting a page -->
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var currentPage = 0;
  var pageSize = 10;
  var rows = document.querySelectorAll("#table_card_ tbody tr");

  function showPage(page) {
    for (var i = 0; i < rows.length; i++) {
      if (i >= page * pageSize && i < (page + 1) * pageSize) {
        rows[i].classList.remove("d-none");
      } else {
        rows[i].classList.add("d-none");
      }
    }
  }

  function populatePageSelect() {
    var pageSelect = document.getElementById("pageSelect");
    pageSelect.innerHTML = "";
    for (var i = 0; i < rows.length / pageSize; i++) {
      var option = document.createElement("option");
      option.value = i;
      option.text = "Page " + (i + 1);
      pageSelect.appendChild(option);
    }
    pageSelect.value = currentPage;
  }

  document.getElementById("next").addEventListener("click", function () {
    if (currentPage < rows.length / pageSize - 1) {
      currentPage++;
      showPage(currentPage);
      populatePageSelect();
    }
  });

  document.getElementById("prev").addEventListener("click", function () {
    if (currentPage > 0) {
      currentPage--;
      showPage(currentPage);
      populatePageSelect();
    }
  });

  document.getElementById("pageSelect").addEventListener("change", function () {
    currentPage = parseInt(this.value);
    showPage(currentPage);
  });

  showPage(currentPage);
  populatePageSelect();
</script>
