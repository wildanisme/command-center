<style type="text/css">
  #peta {
    height: 65vh;
    width: 100%;
  }
</style>

<div class="col-lg-12 xl-100">
  <div class="row">
    <div class="col-sm-12 col-xl-12 col-lg-12 box-col-12">
      <div class="card">
        <div class="card-header">
          <h5>List Ormas</h5>
          <div class="card-header-right">
            <?php
            $total_data_tahun_ini = count($data_eormas);
            if ($total_data_tahun_ini >= 1) {

            }
            ?>
           <a href="<?= $total_data_tahun_ini >= 1 ? site_url('eormas/add/') : '#' ?>" class="btn btn-primary btn-sm">
    <i class="fa fa-plus text-white"></i> Tambah Data Ormas Baru
</a>

          </div>
        </div>
        <div class="card-body p-0">
          <div class="user-status cart-table table-responsive">
            <table class="table table-bordernone" id="table_card_">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Nama Ormas</th>
                  <th scope="col">Jenis</th>
                  <th scope="col">Bidang Kegiatan</th>
                  <th scope="col">Alamat</th>
                  <th scope="col">No Telp</th>
                  <th scope="col">Tahun</th>
                  <th scope="col">Opsi</th>
                </tr>
              </thead>
              <tbody>
                <?php echo $i=1;?>
                <?php foreach ($data_eormas as $key => $data): ?>
                <tr >
                  <td class="f-w-600"> <?php echo $i++ ?> </td>
                  <td class="f-w-600"> <?php echo $data->nama_orkesmas?> </td>
                  <td class="f-w-600"> <?php echo $data->jns_orkesmas?> </td>
                  <td class="f-w-600"> <?php echo $data->bidang_kgt?> </td>
                  <td class="f-w-600"> <?php echo $data->alamat?> </td>
                  <td class="f-w-600"> <?php echo $data->no_tlp?> </td>
                  <td class="digits">
                    <?php echo $data->tahun ?>
                  </td>
                  <td>
                    <a href="<?= site_url('eormas/edit/' . $data->id) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> </a>
                    <a href="javascript:void(0);" data="<?= $data->id ?>" class="btn btn-danger btn-sm item-delete"><i class="fa fa-trash"></i> </a>
                  </td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready( function () {
    $('#table_card_').DataTable();
} );
</script>
<!-- 
<script>
  var currentPage = 0;
  var pageSize = 10;
  var rows = document.querySelectorAll("#table_card_ tbody tr");

  function showPage(page) {
    for (var i = 0; i < rows.length; i++) {
      if (i >= page * pageSize && i < (page + 1) * pageSize) {
        rows[i].classList.remove("d-none");
      } else {
        rows[i].classList.add("d-none");
      }
    }
  }

  function populatePagination() {
    var pagination = document.getElementById("pagination");
    var ul = pagination.querySelector("ul");

    // Remove existing page numbers
    ul.innerHTML = "";

    var totalPages = Math.ceil(rows.length / pageSize);

    // Add Previous button
    var prevLi = document.createElement("li");
    prevLi.className = "page-item";
    prevLi.id = "prev";
    var prevLink = document.createElement("a");
    prevLink.className = "page-link";
    prevLink.href = "#";
    prevLink.textContent = "Previous";
    prevLi.appendChild(prevLink);
    ul.appendChild(prevLi);

    // Add page numbers
    for (var i = 1; i <= totalPages; i++) {
      var li = document.createElement("li");
      li.className = "page-item";
      var link = document.createElement("a");
      link.className = "page-link";
      link.href = "#";
      link.textContent = i;
      li.appendChild(link);
      ul.appendChild(li);
    }

    // Add Next button
    var nextLi = document.createElement("li");
    nextLi.className = "page-item";
    nextLi.id = "next";
    var nextLink = document.createElement("a");
    nextLink.className = "page-link";
    nextLink.href = "#";
    nextLink.textContent = "Next";
    nextLi.appendChild(nextLink);
    ul.appendChild(nextLi);

    ul.addEventListener("click", function (e) {
      if (e.target.tagName === "A") {
        e.preventDefault();
        var text = e.target.textContent;
        if (text === "Previous" && currentPage > 0) {
          currentPage--;
        } else if (text === "Next" && currentPage < totalPages - 1) {
          currentPage++;
        } else {
          currentPage = parseInt(text) - 1;
        }
        showPage(currentPage);
        populatePagination();
      }
    });
  }

  showPage(currentPage);
  populatePagination();
</script> -->
