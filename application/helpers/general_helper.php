<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function curlEofficedesa($url, $postData = '')
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://e-officedesa.sumedangkab.go.id/api_cc/' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  FALSE);
    // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    if (!empty($postData)) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    }

    // $headers = array();
    // $headers[] = 'Accept: */*';
    // $headers[] = 'Content-Type: multipart/form-data';
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
        die;
    }
    curl_close($ch);
    return $result;
}

function input_csrf()
{
    $CI =& get_instance();
    $csrf = array(
        'name' => $CI->security->get_csrf_token_name(),
        'hash' => $CI->security->get_csrf_hash()
    );
    $input = '<input type="hidden" name="'.$csrf['name'].'" value="'.$csrf['hash'].'" />';
    return $input;
}

function js_csrf()
{
    $CI =& get_instance();
    $csrf = array(
        'name' => $CI->security->get_csrf_token_name(),
        'hash' => $CI->security->get_csrf_hash()
    );
    $input = '\''.$csrf['name'].'\' : \''.$csrf['hash'].'\'';
    return $input;
}

function js_csrf_hash()
{
    $CI =& get_instance();
    $csrf = array(
        'name' => $CI->security->get_csrf_token_name(),
        'hash' => $CI->security->get_csrf_hash()
    );
    $input = '\''.$csrf['name'].'\' : \''.$csrf['hash'].'\'';
    return $csrf['hash'];
}


if ( ! function_exists('bulan'))
{
    function bulan($bln)
    {
        switch ($bln)
        {
            case 1:
            return "Januari";
            break;
            case 2:
            return "Februari";
            break;
            case 3:
            return "Maret";
            break;
            case 4:
            return "April";
            break;
            case 5:
            return "Mei";
            break;
            case 6:
            return "Juni";
            break;
            case 7:
            return "Juli";
            break;
            case 8:
            return "Agustus";
            break;
            case 9:
            return "September";
            break;
            case 10:
            return "Oktober";
            break;
            case 11:
            return "November";
            break;
            case 12:
            return "Desember";
            break;
        }
    }
} if ( ! function_exists('bulann'))
{
    function bulann($bln)
    {
        switch ($bln)
        {
            case 1:
            return "Jan";
            break;
            case 2:
            return "Feb";
            break;
            case 3:
            return "Mar";
            break;
            case 4:
            return "Apr";
            break;
            case 5:
            return "Mei";
            break;
            case 6:
            return "Jun";
            break;
            case 7:
            return "Jul";
            break;
            case 8:
            return "Agu";
            break;
            case 9:
            return "Sep";
            break;
            case 10:
            return "Okt";
            break;
            case 11:
            return "Nov";
            break;
            case 12:
            return "Des";
            break;
        }
    }
}
if ( ! function_exists('poee'))
{
    function poee($day)
    {
        switch ($day)
        {
            case 1:
            return "Senin";
            break;
            case 2:
            return "Selasa";
            break;
            case 3:
            return "Rabu";
            break;
            case 4:
            return "Kamis";
            break;
            case 5:
            return "Jum'at";
            break;
            case 6:
            return "Sabtu";
            break;
            case 7:
            return "Minggu";
            break;
        }
    }
}

//format tanggal yyyy-mm-dd
if ( ! function_exists('tanggal'))
{
    function tanggal($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);  //memecah variabel berdasarkan -
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal.' '.$bulan.' '.$tahun; //hasil akhir
    }
}
if ( ! function_exists('thn_hungkul'))
{
    function thn_hungkul($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);  //memecah variabel berdasarkan -
        $tahun = $pecah[0];
        return $tahun; //hasil akhir
    }
}
if ( ! function_exists('bln_hungkul'))
{
    function bln_hungkul($tanggal)
    {
        $bln = substr($tanggal,5,2);
        $bulan = bulann($bln);
        return $bulan; //hasil akhir
    }
}
if ( ! function_exists('tgl_hungkul'))
{
    function tgl_hungkul($tanggal)
    {
        $tgl = substr($tanggal,8,2);
        return $tgl; //hasil akhir
    }
}
if ( ! function_exists('sdate'))
{
    function sdate($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);  //memecah variabel berdasarkan -
        $tanggal = $pecah[2];
        $bulan = bulann($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal.' '.$bulan; //hasil akhir
    }
}
if ( ! function_exists('g_month'))
{
    function g_month($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);  //memecah variabel berdasarkan -
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $bulan.' '.$tahun; //hasil akhir
    }
}
if ( ! function_exists('stime'))
{
    function stime($time)
    {
        $t = substr($time,0,5);
        return $t;
    }
}
if ( ! function_exists('poe'))
{
    function poe($day)
    {
        $poe = poee($day);
        return $poe; //hasil akhir
    }
}

//format tanggal timestamp
if( ! function_exists('tgl_indo_timestamp')){

    function tgl_indo_timestamp($tgl)
    {
    $inttime=date('Y-m-d H:i:s',$tgl); //mengubah format menjadi tanggal biasa
    $tglBaru=explode(" ",$inttime); //memecah berdasarkan spaasi

    $tglBaru1=$tglBaru[0]; //mendapatkan variabel format yyyy-mm-dd
    $tglBaru2=$tglBaru[1]; //mendapatkan fotmat hh:ii:ss
    $tglBarua=explode("-",$tglBaru1); //lalu memecah variabel berdasarkan -

    $tgl=$tglBarua[2];
    $bln=$tglBarua[1];
    $thn=$tglBarua[0];

    $bln=bulan($bln); //mengganti bulan angka menjadi text dari fungsi bulan
    $ubahTanggal="$tgl $bln $thn | $tglBaru2 "; //hasil akhir tanggal

    return $ubahTanggal;
}
}

//format tanggal timestamp
if( ! function_exists('convert_tgl')){

    function convert_tgl($tgl)
    {
        return date("Y-m-d", strtotime($tgl));
    }
}
if( ! function_exists('rupiah')){

    function rupiah($angka)
    {
       $jadi="Rp".number_format($angka,2,',','.');
       return $jadi;
   }
}

if( ! function_exists('dot')){

    function dot($angka,$digit=0)
    {
       $jadi=number_format($angka,$digit,',','.');
       return $jadi;
   }
}

if( ! function_exists('sel_jam')){

    function sel_jam($awal,$akhir)
    {
        $start  = date_create($awal);
    $end    = date_create($akhir); // Current time and date
    $diff   = date_diff( $start, $end );
    // $a = explode(':', $awal);
    // $b = explode(':', $akhir);
    // $jam_start = $a['0'];
    // $menit_start = $a['1'];
    // $jam_end = $b['0'];
    // $menit_end = $b['1'];
    // $hasil = (intVal($jam_end) - intVal($jam_start)) * 60 + (intVal($menit_end) - intVal($menit_start));
    // $hasil = $hasil / 60;
    // $hasil = number_format($hasil,2);
    $jam = $diff->h;
    $men = $diff->i;
    if($jam==0){
        $hasil = $men." Menit";
    }elseif($men==0){
        $hasil = $jam." Jam";
    }elseif($jam==0&&$men==0){
        $hasil = 'Sekejap Mata';
    }else{
        $hasil=$jam." Jam ".$men." Menit";
    }
    return $hasil;
}
}

if( ! function_exists('randomKey')){

    function randomKey($length)
    {
       $pool = array_merge(range(0,9),range('A', 'Z'));
       $key = '';
       for($i=0; $i < $length; $i++) {
        $key .= $pool[mt_rand(0, count($pool) - 1)];
    }
    return $key;
}
}

if( ! function_exists('status_verifikasi')){

    function status_verifikasi($status)
    {
        if($status=='terima'){
            $render = '<span class="label label-success">Diterima</span>';
        }elseif($status=='tolak'){
            $render = '<span class="label label-danger">Ditolak</span>';
        }else{
            $render = '<span class="label label-warning">Belum Diverifikasi</span>';
        }
        return $render;
    }
}



if( ! function_exists('status_loker')){

    function status_loker($status)
    {
        if($status==1){
            $render = '<span class="label label-success">Dibuka</span>';
        }else{
            $render = '<span class="label label-danger">Ditutup</span>';
        }
        return $render;
    }
}

if( ! function_exists('cekForm')){
    function cekForm($array) {
        if(count(array_filter($array)) == count($array)) {
            return false;
        } else {
            return true;
        }
    }
}



if( ! function_exists('isFormEmpty')){
    function isFormEmpty($list_not_required,$list_array) {
        foreach($list_not_required as $l){
            foreach($list_array as $k => $v){
                if($l==$k){
                    unset($list_array[$k]);
                }
            }
        }
        if(count(array_filter($list_array)) == count($list_array)) {
            return false;
        } else {
            return true;
        }
    }
}


if( ! function_exists('status_pekerjaan')){

    function status_pekerjaan($status)
    {
        if($status=='approved'){
            $render = '<span class="label label-info m-l-5 ">Di Setujui</span>';
        }elseif($status=='declined'){
            $render = '<span class="label label-danger m-l-5 ">Di Tolak</span>';
        }else{
            $render = '<span class="label label-warning m-l-5 ">Proses Verfikasi</span>';
        }
        return $render;
    }
}
if( ! function_exists('breadcrumb')){

    function breadcrumb($segments)
    {

        $urls = array();
        $render = '';
        foreach($segments as $key=>$segment){
            $urls[] = array(site_url(implode( '/',array_slice($segments,0,$key))),$segment);
        }
        $q=1;
        if(!empty($urls)){
            foreach($urls as $key => $value){
                if($value[1]=='admin'){
                    $render.= '<li class="active">Dashboard</li>';
                }else{
                    if($q==count($urls)){
                        $render.= '<li class="active">'.ucwords(str_replace('_', ' ', $value[1])).'</li>';
                    }else{
                        $render.= '<li>'.anchor($value[0], ucwords(str_replace('_', ' ', $value[1]))).'</li>';
                    }
                }
                $q++;
            }
        }else{
            $render.= '<li class="active">Dashboard</li>';
        }
        return $render;
    }
}
if( ! function_exists('title')){

    function title($title)
    {

        $title = explode('-', $title);
        $render = trim($title[0]);
        return $render;
    }
}

if( ! function_exists('warna')){

    function warna($jumlah)
    {
        if($jumlah>=100){
            $render = '008000';
        }elseif($jumlah>=80 & $jumlah<100){
            $render = 'FFD700';
        }elseif($jumlah<80){
            $render = 'FF0000';
        }else{
            $render = 'fff';
        }
        return $render;
    }
}

if( ! function_exists('rc')){

    function round_c($n,$x=5)
    {
        $n = number_format($n);
        return (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
    }
}



if( ! function_exists('status_realisasi')){

    function status_realisasi($status)
    {
        if($status=='disetujui'){
            $render = '<span class="label label-success m-l-5 pull-right">Di Setujui</span>';
        }elseif($status=='menunggu_verifikasi'){
            $render = '<span class="label label-info m-l-5 pull-right">Menunggu Verifikasi</span>';
        }elseif($status=='ditolak'){
            $render = '<span class="label label-danger m-l-5 pull-right">Ditolak</span>';
        }elseif($status=='belum_dikerjakan'){
            $render = '<span class="label label-default m-l-5 pull-right">Belum Dikerjakan</span>';
        }
        return $render;
    }
}

if( ! function_exists('make_pagination')){

    function make_pagination($pages,$current,$range=3){
        $res = '';

        $start = $current-$range;
        $end = $current+$range;

        if ($start>1) {
         $res.='<a href="?page=1" class="btn btn-primary">Awal</a> ';
     }

     if ($current>1) {
         $res.='<a href="?page='.($current-1).'" class="btn btn-primary">Sebelumnya</a> ';
     }

     for ($i=$start; $i<=$end ; $i++){
        if($current==$i){
            $disabled = 'disabled';
        }else{
            $disabled = '';
        }

        if ($i>=1 AND $i<=$pages) {
         $res.='<a href="?page='.$i.'" class="btn btn-primary '.$disabled.'">'.$i.'</a> ';
     }
 }

 if (($pages-$current)>0) {
     $res.='<a href="?page='.($current+1).'" class="btn btn-primary">Selanjutnya</a> ';
 }

 if (($pages-$end)>0) {
     $res.='<a href="?page='.$pages.'" class="btn btn-primary">Akhir</a> ';
 }

 return $res;
}
}

if( ! function_exists('normal_string')){

    function normal_string($string)
    {
        $string = str_replace('_', ' ', $string);
        $string = ucwords($string);
        return $string;
    }
}
if( ! function_exists('code_string')){

    function code_string($string)
    {
        $string = str_replace(' ', '_', $string);
        $string = strtolower($string);
        return $string;
    }
}


if( ! function_exists('status_reviu')){

    function status_reviu($status)
    {
        if($status=='kosong_semua'){
            $icon = 'ti-time';
        }elseif($status=='diterima_semua'){
            $icon = 'ti-star';
        }else{
            $icon = 'ti-alert';
        }
        return $icon;
    }
}

if( ! function_exists('status_reviu_skpd')){

    function status_reviu_skpd($status)
    {
        if($status=='sudah_direviu'){
            $icon = ' <div class="white-box-header primary">
            <i class="ti-check"></i> SUDAH DIREVIU
            </div>';
        }elseif($status=='belum_direviu'){
            $icon = '
            <div class="white-box-header danger">
            <i class="ti-close"></i> BELUM DIREVIU
            </div>';
        }elseif($status=='belum_ada_direviu'){
            $icon = '
            <div class="white-box-header danger">
            <i class="ti-close"></i> BELUM ADA INDIKATOR
            </div>';
        }else{
            $icon = '
            <div class="white-box-header warning">
            <i class="ti-time"></i> MASIH PERLU TANGGAPAN
            </div>';
        }
        return $icon;
    }
}
if( ! function_exists('generate_hash')){

    function generate_hash($string)
    {
        return substr(strtoupper(base64_encode(sha1($string))),0,8);
    }
}
if( ! function_exists('refresh_url')){
    function refresh_url($url)
    {
        $string = '<script type="text/javascript">';
        $string .= 'window.location = "' . $url . '"';
        $string .= '</script>';

        return $string;
    }
}

if(!function_exists('generate_qr')){
    function generate_qr($id){
       $CI =& get_instance();
       $CI->load->library('Ci_qr_code');
       $CI->config->load('qr_code');

       $qr_code_config = array();
       $qr_code_config['cacheable'] = $CI->config->item('cacheable');
       $qr_code_config['cachedir'] = $CI->config->item('cachedir');
       $qr_code_config['imagedir'] = $CI->config->item('imagedir');
       $qr_code_config['errorlog'] = $CI->config->item('errorlog');
       $qr_code_config['ciqrcodelib'] = $CI->config->item('ciqrcodelib');
       $qr_code_config['quality'] = $CI->config->item('quality');
       $qr_code_config['size'] = $CI->config->item('size');
       $qr_code_config['black'] = $CI->config->item('black');
       $qr_code_config['white'] = $CI->config->item('white');
       $CI->ci_qr_code->initialize($qr_code_config);

       $image_name = $id . ".png";

       $servername=filter_input(INPUT_SERVER, 'SERVER_NAME');
       $port=filter_input(INPUT_SERVER, 'SERVER_PORT');
       $codeContents = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && ! in_array(strtolower($_SERVER['HTTPS']), array( 'off', 'no' ))) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/sakip/verifikasi_surat/id/'.$id;

       $params['data'] = $codeContents;
       $params['level'] = 'H';
       $params['size'] = 2;

       $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
       $CI->ci_qr_code->generate($params);

       $CI->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;
       return $file = $params['savename'];
   }
}

if(!function_exists('get_qr_code')){
    function get_qr_code($imagepath){

        $za = new ZipArchive();

        $za->open($imagepath);

        $image_files = array();
        for( $i = 0; $i < $za->numFiles; $i++ ){
            $stat = $za->statIndex( $i );
            $filename = basename( $stat['name'] ) . PHP_EOL;
            $ex = explode('.', $filename);
            if(isset($ex[1])){
                $ext = trim($ex[1]);
                if($ext == 'png'){
                    $image_files[] = $filename;
                }
            }
        }
        return trim(end($image_files));
    }
}

if(!function_exists('get_capaian')){
    function get_capaian($target,$realisasi,$pola){
        if($target==0&&$realisasi==0){
            $capaian=0;
        }else{
            if($pola == "Maximaze" ){
              $capaian = ($realisasi/$target)*100; 
          }elseif($pola == "Minimaze" ){
              $capaian = (1+(1-$realisasi/$target))*100;
          }elseif($pola == "Stabilize" ){
              if($realisasi > $target){
        //salah
                $ca = $realisasi/$target*100;
                $capaian = (100-($ca-100));
            }elseif($realisasi < $target){
        //salah
                $capaian = $capaian = $realisasi/$target*100;
            }else{
                $capaian = 100;
            }
        } else {
          $capaian =0;
      }
  }
  return round($capaian,2);
}
}

if(!function_exists('flashdata_notif')){
    function flashdata_notif(){
        $CI =& get_instance();
        $msg = "";
        $current_status = "primary";
        if (($CI->session->flashdata('primary'))) {
            $msg = '<div class="alert alert-primary dark alert-dismissible fade show" role="alert"><i data-feather="bell"></i>
            <p> '.$CI->session->flashdata('primary').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "primary";
        }

        if (($CI->session->flashdata('secondary'))) {
            $msg = '<div class="alert alert-secondary dark alert-dismissible fade show" role="alert"><i data-feather="heart"></i>
            <p> '.$CI->session->flashdata('secondary').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "secondary";
        }

        if (($CI->session->flashdata('success'))) {
            $msg = '<div class="alert alert-success dark alert-dismissible fade show" role="alert"><i data-feather="thumbs-up"></i>
            <p> '.$CI->session->flashdata('success').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "success";
        }

        if (($CI->session->flashdata('info'))) {
            $msg = '<div class="alert alert-info dark alert-dismissible fade show" role="alert"><i data-feather="info"></i>
            <p> '.$CI->session->flashdata('info').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "info";
        }

        if (($CI->session->flashdata('warning'))) {
            $msg = '<div class="alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="alert-triangle"></i>
            <p> '.$CI->session->flashdata('warning').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "warning";
        }

        if (($CI->session->flashdata('danger'))) {
            $msg = '<div class="alert alert-danger dark alert-dismissible fade show" role="alert"><i data-feather="slash"></i>
            <p> '.$CI->session->flashdata('danger').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "danger";
        }

        if (($CI->session->flashdata('light'))) {
            $msg = '<div class="alert alert-light dark alert-dismissible fade show" role="alert"><i data-feather="help-circle"></i>
            <p> '.$CI->session->flashdata('light').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "light";
        }

        if (($CI->session->flashdata('dark'))) {
            $msg = '<div class="alert alert-dark dark alert-dismissible fade show" role="alert"><i data-feather="alert-circle"></i>
            <p> '.$CI->session->flashdata('dark').'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
            $current_status = "dark";
        }

        if ((validation_errors())) {
            $msg .= '<div class="alert alert-'.$current_status.' outline dark alert-dismissible fade show" role="alert">
            <p> Keterangan: '.validation_errors().'</p>
            <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>';
        }
        return $msg;
        
    }
}



if( ! function_exists('cari_array_result')){

    function cari_array_result($value,$key,$array)
    {
        return array_search($value, array_column($array, $key));
    }
}


if( ! function_exists('random_color')){

    function random_color($index=null)
    {
        $array_color = array('primary','success','info','warning','secondary','danger','dark');
        $index = ($index) ? $index : rand(0,count($array_color)-1);
        return $array_color[$index];
    }
}

if( ! function_exists('abjad_sakip')){
    function abjad_sakip($nilai)
    {
        $nilai = (int) $nilai;
        if (is_numeric($nilai)) {
            if ($nilai>90) {
                $abjad = "AA";
            } elseif ($nilai>80) {
                $abjad = "A";
            } elseif ($nilai>70) {
                $abjad = "BB";
            } elseif ($nilai>60) {
                $abjad = "B";
            } elseif ($nilai>50) {
                $abjad = "CC";
            } elseif ($nilai>30) {
                $abjad = "C";
            } else {
                $abjad = "D";
            }
        } else {
            $abjad = $nilai;
        }
        return $abjad;
    }
}

if( ! function_exists('color_sakip')){
    function color_sakip($abjad)
    {
        if ($abjad == "AA") {
            $color = 0;
        } elseif ($abjad == "A") {
            $color = 1;
        } elseif ($abjad == "BB") {
            $color = 2;
        } elseif ($abjad == "B") {
            $color = 3;
        } elseif ($abjad == "CC") {
            $color = 4;
        } elseif ($abjad == "C") {
            $color = 5;
        } else {
            $color = 6;
        }
        return $color;
    }
}

function roundUpToAny($n,$x=5) {
    return (round($n)%$x === 0) ? round($n) : round(($n+$x/2)/$x)*$x;
}
