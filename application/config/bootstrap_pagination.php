<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// application/config/bootstrap_pagination.php
$config['pagination']['full_tag_open'] = '<ul>';
$config['pagination']['full_tag_close'] = '</ul>';

$config['pagination']['first_tag_open'] = '<li>';
$config['pagination']['first_tag_close'] = '</li>';

$config['pagination']['last_tag_open'] = '<li>';
$config['pagination']['last_tag_close'] = '</li>';

$config['pagination']['next_tag_open'] = '<li>';
$config['pagination']['next_tag_close'] = '</li>';

$config['pagination']['prev_tag_open'] = '<li>';
$config['pagination']['prev_tag_close'] = '</li>';

$config['pagination']['cur_tag_open'] = '<li><a href="#!" class="current-page">';
$config['pagination']['cur_tag_close'] = '</a></li>';

$config['pagination']['num_tag_open'] = '<li>';
$config['pagination']['num_tag_close'] = '</li>';

$config['pagination']['first_link'] = '<i class="sl sl-icon-arrow-left"></i>';
$config['pagination']['last_link'] = '<i class="sl sl-icon-arrow-right"></i>';

$config['pagination']['prev_link'] = '<i class="sl sl-icon-arrow-left"></i>';
$config['pagination']['next_link'] = '<i class="sl sl-icon-arrow-right"></i>';