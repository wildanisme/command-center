<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_public extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('public_model');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes'); 
	}
	
	
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Public - ". app_name;
			$data['content']	= "publikasi/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$this->load->model('public_model');
			$data['query']		= $this->public_model->get_all();
			$data['active_menu'] = "public";
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}

	public function add_publikasi()
	{
		if ($this->user_id)
		{
			$this->load->model('public_model');
			$data['title']		= "Add new file ` - ". app_name;
			$data['content']	= "publikasi/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			//load kategori
			$this->load->model('category_model_public');
			$this->category_model_public->category_status = "Active";
			$data['categories']	= $this->category_model_public->get_all();
			//
			$data['active_menu'] = "public";
			$this->load->helper('form');
			if (!empty($_POST))
			{
				if ($_POST['judul'] !="" )
				{
						$config['upload_path']          = './data/publikasi/';
			            $config['allowed_types']        = '*';
			            $config['max_size']             = 200000;
			            //$config['max_width']            = 2000;
			            //$config['max_height']           = 2000;
			            $error=false;
			            $url_video = $_POST['url'];
			            $this->load->library('upload', $config);
			            if ( ! $this->upload->do_upload())
		                {
		                   
		                    
    						$new_url = str_replace("watch?v=", "embed/", $url_video);
    						$this->public_model->url = $new_url;
    						$this->public_model->type = 'youtube';

    						if ($url_video == '') {
    						$data['message_type'] = "warning";
		                    $data['message']			= $this->upload->display_errors();
							$error = true;
    						}
    						
		                }
		                else {
							$this->public_model->url = '';

		                }

		               	if ($error == false) {

		                	$this->load->model('public_model');
		                	$this->public_model->nama_file = $this->upload->data('file_name');
		                	$this->public_model->judul = $_POST['judul'];
		                	$this->public_model->category_id = $_POST['category_id'];
		                	$this->public_model->tgl_posting = $_POST['tgl_posting'];
		                	$this->public_model->publish = $_POST['publish'];
							if($url_video != ''){
								$this->public_model->type = 'youtube';
							}
							else {
								$this->public_model->type = $this->upload->data('file_type');
							}		               
		                	
			                $this->public_model->insert();
			                $data['message_type'] = "success";
			                $data['message']		= "File download has been added successfully.";
			           
						}
						else {
							$data['message_type'] = "warning";
			                $data['message']		= "data belum lengkap";
						}		                

		               
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}
			$this->load->view('admin/index',$data);
			
		}
		else
		{
			redirect('admin');	
		}
	}
	public function edit_publikasi($id_publikasi)
	{
		if ($this->user_id)
		{
			$this->load->model('public_model');
			$this->public_model->id_publikasi = $id_publikasi;
			$data['title']		= "Edit publikasi - ". app_name;
			$data['content']	= "publikasi/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "public";
			$this->load->helper('form');
			if (!empty($_POST))
			{
				if ($_POST['judul'] !="" )
				{
						$config['upload_path']          = './data/publikasi/';
			            $config['allowed_types']        = '*';
			            $config['max_size']             = 200000;
			            //$config['max_width']            = 2000;
			            //$config['max_height']           = 2000;
			            $error=false;
			            $url_video = $_POST['url'];
			            $this->load->library('upload', $config);
			            if ( ! $this->upload->do_upload())
		                {
		                   
		                    
    						$new_url = str_replace("watch?v=", "embed/", $url_video);
    						$this->public_model->url = $new_url;
    						$this->public_model->type = 'youtube';

    						if ($url_video == '') {
    						$data['message_type'] = "warning";
		                    $data['message']			= $this->upload->display_errors();
							$error = true;
    						}
    						
		                }
		                else {
							$this->public_model->url = '';

		                }

		               	if ($error == false) {

		                	$this->load->model('public_model');
		                	$this->public_model->nama_file = $this->upload->data('file_name');
		                	$this->public_model->judul = $_POST['judul'];
		                	$this->public_model->category_id = $_POST['category_id'];
		                    $this->public_model->publish = $_POST['publish'];

							if($url_video != ''){
								$this->public_model->type = 'youtube';
							}
							else {
								$this->public_model->type = $this->upload->data('file_type');
							}		               
		                	
			                $this->public_model->update();
			                $data['message_type'] = "success";
			                $data['message']		= "File download has been added successfully.";
			           
						}
						else {
							$data['message_type'] = "warning";
			                $data['message']		= "data belum lengkap";
						}		                

		               
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}
			$this->public_model->set_by_id();
			$data['id_publikasi']	= $this->public_model->id_publikasi;
			$data['category_id']	= $this->public_model->category_id;
			$data['judul']			= $this->public_model->judul;
			$data['nama_file']	= $this->public_model->nama_file;
			$data['tgl_posting']	= $this->public_model->tgl_posting;
			$data['type']	= $this->public_model->type;
			$data['url']	= $this->public_model->url;
			$data['publish']	= $this->public_model->publish;

			//ambil dari table category
			$this->load->model('category_model_public');
			$data['categories']	= $this->category_model_public->get_all();


			$this->load->view('admin/index',$data);
			
		}
		else
		{
			redirect('admin');	
		}
	}

	public function delete($id_publikasi)
	{
		if ($this->user_id)
		{
			$this->load->model('public_model');
			$this->public_model->id_publikasi = $id_publikasi;
			$this->public_model->set_by_id();
			if ($this->public_model->nama_file !="") unlink('./data/publikasi/'.$this->public_model->nama_file);
			$this->public_model->delete();
			redirect('manage_public');
		}
		else
		{
			redirect('home');
		}
	}
}
?>