<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_helpdesk extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		$this->load->model('helpdesk_model');
		$id_helpdesk = $this->uri->segment(3);
		if($this->session->userdata("user_level")>1){
			redirect("home");
		}
	}
	
	
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "helpdesk - Admin ";
			$data['content']	= "helpdesk/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "helpdesk";

			$hal = 6;
			$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
			$mulai = ($page>1) ? ($page * $hal) - $hal : 0;
			$total = count($this->helpdesk_model->get_all());
			$data['pages'] = ceil($total/$hal);
			$data['current'] = $page;
			if(!empty($_POST)){
				$filter = $_POST;
				$data['filter'] = true;
				$data['filter_data'] = $_POST;
				$data['id_kategori_helpdesk'] = $_POST['id_kategori_helpdesk'];
			}
			else if(!empty($_GET)){
				$filter = $_GET;
				$data['filter'] = true;
				$data['filter_data'] = $_GET;
			}else{
				$filter = '';
				$data['filter'] = false;
			}
			$data['helpdesk'] = $this->helpdesk_model->get_for_page($mulai,$hal,$filter);
			$data['status_buka'] = $this->helpdesk_model->get_buka();
			$data['status_tutup'] = $this->helpdesk_model->get_tutup();

			$this->load->model('kategori_helpdesk_model');
			$data['kategori']		= $this->kategori_helpdesk_model->get_all();
			
			$data['total'] = $total;
			$data['buka'] = $this->helpdesk_model->get_total_row("",null,['status' => 'Buka']);
			$data['tutup'] = $this->helpdesk_model->get_total_row("",null,['status' => 'Tutup']);
			
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah helpdesk - Admin ";
			$data['content']	= "helpdesk/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_helpdesk";

			if(!empty($_POST)){
				$cek = $_POST;
				unset($cek['nama_helpdesk']);
				unset($cek['id_kategori_helpdesk']);
				if(cekForm($cek)){
					$data['message'] = 'Masih ada form yang kosong';
					$data['type'] = 'warning';
				}else{
					$insert = $_POST;
					unset($insert['foto']);
					$insert['foto'] = "sumedang.png";

					$config['upload_path']          = './data/helpdesk/';
					$config['allowed_types']        = 'gif|jpg|png';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('foto')){
						$tmp_name = $_FILES['foto']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$insert['foto'] = $this->upload->data('file_name');
					}

					$in = $this->helpdesk_model->insert($insert);
					$data['message'] = 'helpdesk berhasil ditambahkan';
					$data['type'] = 'success';
				}
			}

			$this->load->model('kategori_helpdesk_model');
			$data['kategori']		= $this->kategori_helpdesk_model->get_all();

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	



	public function view($id_helpdesk)
	{
		if ($this->user_id)
		{
			$data['title']		= "Detail helpdesk - Admin ";
			$data['content']	= "helpdesk/view" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_helpdesk";
			$data['id_user'] = $this->user_id;
			$id_user = $this->user_id;
			// if(!empty($_POST)){
			// 	//echo "<pre>";print_r($_POST);die;
			// 	$html_escape = html_escape($_POST);
			// 	$clean = array();
			// 	foreach($html_escape as $key=>$value)
			// 	{
			// 		$clean[$key] = $this->security->xss_clean($value);
			// 	}

			// 	$update = array(
			// 		'catatan'		=> $clean['catatan'],
			// 		'tgl_respon'	=> date('Y-m-d H:i:s'),
			// 	);
			// 	$data['message'] = "";
			// 	$data['type'] ="success";

			// 	$detail = $this->helpdesk_model->get_by_id($id_helpdesk);

			// 	$insert_log = array(
			// 		'user_id'	=> $detail->id_user,
			// 		'category'	=> 'helpdesk',
			// 		'time'		=> date('Y-m-d H:i:s'),
			// 		'description'	=> $update['catatan'],
			// 	);
			// 	if(!empty($clean['verifikasi'])){
			// 		$update['status']	= 'Sudah diverifikasi';
			// 		$data['message'] = 'helpdesk berhasil diverifikasi';
			// 		$insert_log['activity'] = "helpdesk anda untuk <b>".$detail->nama_helpdesk."</b> telah diverifikasi.";
			// 	}
			// 	else if(!empty($clean['tolak'])){
			// 		$update['status']	= 'Ditolak';
			// 		$data['message'] = 'helpdesk telah ditolak';
			// 		$insert_log['activity'] = "helpdesk anda untuk <b>".$detail->nama_helpdesk."</b> telah ditolak.";
			// 	}
			// 	$this->helpdesk_model->update($update,$id_helpdesk);
			// 	$this->db->insert("logs",$insert_log);
			// }

				$this->load->library('form_validation');

			if(isset($_POST['submit_respons'])){
					$this->form_validation->set_rules("isi","Isi Tanggapan","required",["required" => "%s harus diisi"]);
					if ($this->form_validation->run() == true) {
						$this->helpdesk_model->insert_respons($id_helpdesk, $id_user);
						$this->session->set_flashdata("respons_success","Tanggapan berhasil ditambah");
						echo '<script>javascript:alert("Tanggapan berhasil ditambahkan");window.location = window.location.href;</script>';
					}else{
						$this->session->set_flashdata("respons_error","Ada kesalah");
						$data = array_merge($data,$_POST);
					}
				}

			if (isset($_POST['tutup_laporan'])) {
						$this->helpdesk_model->tutup_laporan($id_helpdesk);
						// $this->session->set_flashdata("respons_success","Tanggapan berhasil ditambah");
						echo '<script>javascript:alert("Pengaduan berhasil ditutup");window.location = window.location.href;</script>';
				}

			if(isset($_POST['update_respons'])){
						$this->helpdesk_model->update_respons($this->input->post('id_respon'));
						// $this->session->set_flashdata("respons_success","Tanggapan berhasil ditambah");
						echo '<script>javascript:alert("Respons berhasil diupdate");window.location = window.location.href;</script>';
			}	
			
				
			$data['respons'] = $this->helpdesk_model->respons_by_id($id_helpdesk);
			$data['detail'] = $this->helpdesk_model->get_by_id($id_helpdesk);
		

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


	public function hapus_helpdesk($id_helpdesk)
	{
		if ($this->user_id)
		{
			$listing = $this->helpdesk_model->get_by_id($id_helpdesk);
			$this->helpdesk_model->hapus_helpdesk($id_helpdesk);
			
			redirect('manage_helpdesk');
		}
		else
		{
			redirect('admin');
		}
	}

	public function deleterespons($id_helpdesk=null,$id_respons=null)
	{
		if($this->user_id && $id_respons!=null && $id_helpdesk!=null){
			$this->helpdesk_model->user_id = $this->user_id;
			// print_r($id_helpdesk);
			// die;
			$success = $this->helpdesk_model->hapus_helpdeskrespons($id_respons);
			if($success)
				echo '<script>javascript:alert("Tanggapan berhasil dihapus");window.location = window.location.href;</script>';
			else
				$this->session->set_flashdata("message_error","Tanggapan gagal dihapus.");
			
				redirect('manage_helpdesk/view/'.$id_helpdesk.'');
		}
		
	}

	

	

	
}
?>