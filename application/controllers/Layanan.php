<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');
		$this->load->model('ref_layanan_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		$data['title'] = "Daftar Layanan - " .$this->company_profile_model->nama;
		$data['active_menu'] = "layanan";
		//$this->load->model('potensi_model');
		//$data['query']		= $this->potensi_model->get_all_join_category();

		$hal = 10;
		$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;

		$offset = ($page*$hal) - $hal;
		$cari = "";
		$total = 0;

		$layanan = null;
		
		$post = array();
		if(!empty($_POST)){
			$offset = 0;
			$page = 1;
			$html_escape = html_escape($_POST);
			
			foreach($html_escape as $key=>$value)
			{
				$post[$key] = $this->security->xss_clean($value);
			}
			$data = array_merge($post);
		}

		require_once(APPPATH."libraries/WebserviceMPP.php");
		$MPP = new WebserviceMPP();
		$api_key = $this->config->item("app_key_mpp");
		$param = array(
			'offset'	=> $offset,
			'limit'		=> $hal,
			'cari'		=> !empty($post['cari'] ) ? $post['cari'] : '',
			'tags'		=> !empty($post['tags']) ? $post['tags'] : '',
		);

		// var_dump($param);die;
		$result = $MPP->get_layanan($api_key,$param);
		
		$layanan = json_decode($result);
		$data['tag'] = array();
		if(!$layanan->error){
			$total = $layanan->total_rows;
			
			$data['tag'] = $layanan->tag;
			$layanan = $layanan->layanan;
		}
		else{
			
		}
		
		$data['layanan'] = $layanan;
		
		$data['title'] = "Layanan publik - " .$this->company_profile_model->nama;
		$data['active_menu_cityzen'] ="layanan";


		$data['pages'] = ceil($total/$hal);
		$data['current'] = $page;
		$data['per_halaman'] = $hal;

		//echo "<pre>";print_r($data['tag']);die;
		

		$this->load->view('blog/layanan',$data);
	}
	
	
	public function detail($id_layanan=null)
	{
		$data['title'] = "Detail Layanan - " .$this->company_profile_model->nama;
		$data['active_menu'] = "layanan";
		

		if($id_layanan==null)
		{
			redirect("/layanan");
		}
		require_once(APPPATH."libraries/WebserviceMPP.php");
		$MPP = new WebserviceMPP();
		$api_key = $this->config->item("app_key_mpp");
		$param = array(
			'id_layanan'	=> $id_layanan,
		);

		//var_dump($param);die;
		$result = $MPP->get_layanan_detail($api_key,$param);
		$layanan = json_decode($result);
		$detail = null;
		if(!$layanan->error){
			$detail = $layanan->detail;
		}
		else{
			
		}
		if($detail==null)
		{
			redirect("/layanan");
		}
		
		$data['detail'] = $detail;
		$data['id_layanan'] = $id_layanan;

		$data['title'] = "Detail Layanan - " .$this->company_profile_model->nama;
		$data['active_menu_cityzen'] ="layanan";


		
		//echo "<pre>";print_r($detail);die;
		
		$this->load->view('blog/detail_layanan',$data);
	}


}
?>