<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_sarana extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		$this->load->model('sarana_model');
		$id_sarana = $this->uri->segment(3);
		if (!in_array($this->session->userdata("user_level"),[1,3,4])) redirect("cityzen");
	}
	
	
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "sarana - Admin ";
			$data['content']	= "sarana/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "sarana";

			$hal = 6;
			$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
			$mulai = ($page>1) ? ($page * $hal) - $hal : 0;
			$total = count($this->sarana_model->get_all());
			$data['pages'] = ceil($total/$hal);
			$data['current'] = $page;
			if(!empty($_POST)){
				$filter = $_POST;
				$data['filter'] = true;
				$data['filter_data'] = $_POST;
				$data['id_kategori_sarana'] = $_POST['id_kategori_sarana'];
			}else{
				$filter = '';
				$data['filter'] = false;
			}
			$data['list'] = $this->sarana_model->get_for_page($mulai,$hal,$filter);

			$this->load->model('kategori_sarana_model');
			$data['kategori']		= $this->kategori_sarana_model->get_all();

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah sarana - Admin ";
			$data['content']	= "sarana/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "sarana";

			$this->load->model("alamat_model");
			$data['desa'] = $this->alamat_model->get_desa();


			if(!empty($_POST)){
				$cek = $_POST;
				unset($cek['nama_sarana']);
				unset($cek['id_kategori_sarana']);
				if(cekForm($cek)){
					$data['message'] = 'Masih ada form yang kosong';
					$data['type'] = 'warning';
				}else{
					$insert = $_POST;
					unset($insert['banner']);
					unset($insert['_wysihtml5_mode']);
					$insert['banner'] = "sumedang.png";

					$config['upload_path']          = './data/sarana/';
					$config['allowed_types']        = 'gif|jpg|png';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('banner')){
						$tmp_name = $_FILES['banner']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$insert['banner'] = $this->upload->data('file_name');
					}


					if(!empty($insert['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($insert['id_desa']);
						$insert['id_kecamatan'] = $desa->id_kecamatan;
					}

					$in = $this->sarana_model->insert($insert);
					$data['message'] = 'sarana berhasil ditambahkan';
					$data['type'] = 'success';
				}
			}

			$this->load->model('kategori_sarana_model');
			$data['kategori']		= $this->kategori_sarana_model->get_all();

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	



	public function view($id_sarana)
	{
		if ($this->user_id && $this->sarana_model->is_authorize($id_sarana))
		{
			$data['title']		= "Detail sarana - Admin ";
			$data['content']	= "sarana/view" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "sarana";

			$this->load->model("alamat_model");
			$data['desa'] = $this->alamat_model->get_desa();


			if(!empty($_POST)){

				$insert = $_POST;

					unset($insert['_wysihtml5_mode']);

					unset($insert['poster']);
					

					
					$config['upload_path']          = './data/sarana/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;
					//$config['file_name']			= uniqid().".".$file_ext;
					$config['encrypt_name']			= true;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('banner')){
						
						$tmp_name = isset($_FILES['banner']['tmp_name']) ? $_FILES['banner']['tmp_name'] :"";
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						
						$insert['banner'] = $this->upload->data('file_name');
						
						$sarana = $this->sarana_model->get_by_id($id_sarana);
						if($sarana && $sarana->banner!="")
						{
							if(file_exists("./data/sarana/".$sarana->banner))
								unlink("./data/sarana/".$sarana->banner);
						}
					}

					if(!empty($insert['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($insert['id_desa']);
						$insert['id_kecamatan'] = $desa->id_kecamatan;
						
					}

					unset($insert['id_sarana']);
					
					
					$update = $this->sarana_model->update($insert,$id_sarana);
				if($update){
					//echo "<pre>";print_r($insert);die;
					$data['message'] = 'sarana berhasil diperbarui';
					$data['type'] = 'success';

			}
			}
			
			
			$data['foto_sarana'] = $this->sarana_model->get_foto($id_sarana);
			$data['detail'] = $this->sarana_model->get_by_id($id_sarana);
		
			//echo "<pre>";print_r($data['detail']);die;
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


	public function hapus_sarana($id_sarana)
	{
		if ($this->user_id && $this->sarana_model->is_authorize($id_sarana))
		{
			
			$this->sarana_model->hapus_sarana($id_sarana);
			
			redirect('manage_sarana');
		}
		else
		{
			redirect('admin');
		}
	}

	public function hapus_foto($id_sarana,$id_foto)
	{	 
		
		//$id_layanan = $this->uri->segment('3');
		if ($this->user_id && $this->sarana_model->is_authorize($id_sarana))
		{
			$this->sarana_model->hapus_foto($id_foto);
			redirect('manage_sarana/view/'.$id_sarana);
		}
		else
		{
			redirect('admin');
		}
	}




	public function add_foto()
	{
		if ($this->user_id)
		{
			$this->load->model("sarana_model");
			$result = true;
			$insert = $_POST;
					unset($insert['foto']);
			
					
			if(!$this->sarana_model->is_authorize($insert['id_sarana'])){
				show_404();
			}
			else{
				
					$config['upload_path']          = './data/sarana/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;
					//$config['file_name']			= uniqid().".".$file_ext;
					$config['encrypt_name']			= true;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('foto')){
						$tmp_name = $_FILES['foto']['tmp_name'];
						if ($tmp_name!=""){
							$result = $this->upload->display_errors();
							die($result);
						}
					}else{
						
						$insert['foto'] = $this->upload->data('file_name');
					}

				
				
					$this->sarana_model->insert_foto($insert);
				

				redirect('manage_sarana/view/'.$insert['id_sarana']);
			}
		}
	}




	

	
}
?>