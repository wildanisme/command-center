<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_mentor extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		if (!$this->user_id)
		{
			redirect("admin/login");
		}

		if($this->user_level!="Administrator")
		{
			show_404();
		}

		$this->load->model('Mentor_model');

		$this->file_max_size = 1000; // 1mb
		$this->file_type_allowed = ['jpg','jpeg','png'];
	}


	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Ref. Mentor";
			$data['content']	= "ref_mentor/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";

			$this->load->model("Kelompok_model");
			$data['dt_kelompok'] = $this->Kelompok_model->get()->result();


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function get_list($rowno=1)
	{
			if($this->input->is_ajax_request())
			{
					// Row per page
					$rowperpage = 6;
					$offset = ($rowno-1) * $rowperpage;

					$param = array();
					$param['limit']     = $rowperpage;
					$param['offset']    = $offset;
					$data = array();

					if($this->input->post("search"))
					{
							$param['search'] = $this->input->post("search");
					}

					if($this->input->post("id_kelompok"))
					{
							$param['id_kelompok'] = $this->input->post("id_kelompok");
					}


					$query = $this->Mentor_model->get($param);
					$result = $query->result();

					$param_total = $param;
					unset($param_total['limit']);
					unset($param_total['offset']);
					$query_total = $this->Mentor_model->get($param_total);
					$total_rows = $query_total->num_rows();


					// Pagination Configuration
					$config['base_url'] = "#";
					$config['use_page_numbers'] = TRUE;
					$config['total_rows'] = $total_rows;
					$config['per_page'] = $rowperpage;
					//$config['attributes'] = array('class' => 'btn btn-default btn-outline btn-xm');

					$config['cur_tag_open']     = '<li class="page-item active" aria-current="page"><a style="padding:10px 15px"  class=" page-link">';
					$config['cur_tag_close']    = '<a class="sr-only">(current)</a></a></li>';

					// Initialize
					$this->load->library('pagination');
					$this->pagination->initialize($config);

					$link = $this->pagination->create_links();
					//$link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled' >", $link);
					//$link = str_replace("</strong>", "</button>", $link);
					$link = str_replace("<li>",'<li class="page-item">',$link);
					$link = str_replace('data-ci-pagination-page','class="page-link" data-ci-pagination-page',$link);

					// Initialize $data Array
					$data['pagination'] = $link;
					$data['result']     = $result;
					$data['row']        = $offset;
					$data['csrf_hash']  = $this->security->get_csrf_hash();
					$data['row_content'] = $this->rowContent($result);


					echo json_encode($data);

			}
	}

	private function rowContent($data)
	{
			$contents = "";
			foreach ($data as $row) {
				$email = ($row->email) ? $row->email : "-";
				$telepon = ($row->telepon) ? $row->telepon : "-";
					$contents .= '

					<div class="col-xl-4 col-md-6 col-sm-12 profile-card-1">
							<div class="card with-dropdown">
									<div class="btn-group mb-1 card-dropdown">
											<div class="dropdown">
													<button class="btn rounded-circle btn-primary mr-1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i style="margin-right:0px" class="feather icon-more-horizontal"></i>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<a class="dropdown-item" href="'.base_url().'ref_mentor/edit/'.$row->id_mentor.'"><i class="feather icon-edit"></i> Edit</a>
															<a class="dropdown-item" href="javascript:void(0)" onclick="hapus('.$row->id_mentor.')"><i class="feather icon-trash"></i> Hapus</a>
													</div>
											</div>
									</div>

									<a href="'.base_url().'ref_mentor/detail/'.$row->id_mentor.'">
									<div class="card-content">
											<div class="media user-list">
													<img class="align-self-center mr-2 user-img" src="'.base_url().'data/mentor/'.$row->foto.'" alt="">
													<div class="media-body">
															<h5 class="mt-0 mb-0 font-weight-bold">'.$row->nama_mentor.'</h5>
															<span class="user-role text-primary">'.$telepon.'</span>
															 <hr style="border-top: 1px solid rgb(0 0 0 / 15%);">
															<p class="mb-0 mt-0"><i class="feather icon-mail"></i> '.$email.'</p>
													</div>
											</div>
									</div>
							</a>

							</div>
					</div>
									';
			}

			if($contents=="")
			{
					$contents = "
					<div class='col-md-12'>
					<p class='text-center mt-5'>- Tidak ada data -</p>
					</div>
					";
			}

			return $contents;
	}

	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah Mentor ";
			$data['content']	= "ref_mentor/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";

			$data['file_max_size'] = $this->file_max_size;
			$data['file_type_allowed'] = implode(",",$this->file_type_allowed);


			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}

	public function save()
	{
			if($this->input->is_ajax_request() )
			{
					if($_POST)
					{
							$data['status'] = true;
							$data['errors'] = array();
							$html_escape = html_escape($_POST);
							$postdata = array();
							foreach($html_escape as $key=>$value)
							{
									$postdata[$key] = $this->security->xss_clean($value);
							}


							$this->load->library('form_validation');

							$this->form_validation->set_data( $postdata );




									$validation_rules = [
											[
													'field' => 'nama_mentor',
													'label' => 'Nama',
													'rules' => 'required|regex_match[/^[a-z\d\-_\s\'\ "]+$/i]|max_length[50]',
													//'rules' => 'required|max_length[255]',
													'errors' => [
															'required' => '%s diperlukan',
															'max_length' => 'Maximal 50 Char',
															'regex_match' => '%s tidak valid',
													]
											],
											[
													'field' => 'telepon',
													'label' => 'Telepon',
													'rules' => 'required|numeric|max_length[15]',
													'errors' => [
															'required' => '%s diperlukan',
															'max_length' => 'Maximal 15 Char',
															'numeric' => '%s harus angka',
													]
											],
											[
													'field' => 'alamat',
													'label' => 'Alamat',
													'rules' => 'required',
													'errors' => [
															'required' => '%s diperlukan',
													]
											],
											[
													'field' => 'status',
													'label' => 'Status',
													'rules' => 'required',
													'errors' => [
															'required' => '%s diperlukan',
													]
											],
											[
													'field' => 'nik',
													'label' => 'No KTP',
													'rules' => 'numeric',
													'errors' => [
															'numeric' => '%s harus angka',
													]
											],

									];

							if($this->input->post("action")=="add")
							{
								$validation_rules[] = [
										'field' => 'email',
										'label' => 'Email',
										'rules' => 'required|valid_email|is_unique[mentor.email]',
										'errors' => [
												'required' => '%s diperlukan',
												'valid_email' => '%s tidak valid',
												'is_unique' => '%s sudah digunakan',
										]
								];
							}
							else{
								$mentor = $this->db->where("id_mentor",$postdata['id'])->get("mentor")->row();
								if($mentor->email && $mentor->email != $postdata['email'])
								{
									$validation_rules[] =
									[
											'field' => 'email',
											'label' => 'Email',
											'rules' => 'required|valid_email|is_unique[mentor.email]',
											'errors' => [
													'required' => '%s diperlukan',
													'valid_email' => '%s tidak valid',
													'is_unique' => '%s sudah digunakan',
											]
									];
								}
							}


							$this->form_validation->set_rules( $validation_rules );

							if( $this->form_validation->run() )
							{


									$dt = array();
									$dt['nama_mentor']    = $postdata['nama_mentor'];
									$dt['email']    = $postdata['email'];
									$dt['telepon']        = $postdata['telepon'];
									$dt['alamat']        = $postdata['alamat'];
									$dt['nik']        = $postdata['nik'];
									$dt['status']        = $postdata['status'];

									// upload picture
									$config['upload_path']="./data/mentor/";
							        $config['allowed_types']=implode("|", $this->file_type_allowed);
							        $config['encrypt_name'] = TRUE;
							        $config['max_size']     = $this->file_max_size;

							        $this->load->library('upload',$config);
							        if($this->upload->do_upload("foto")){
							            $dt['foto']= $this->upload->data('file_name');
							            if(!empty($mentor) && $mentor->foto!="default.png" && $this->input->post("action")=="edit"){
							            	unlink($config['upload_path'].$mentor->foto);
							            }
							        }
							        else if (!empty($_FILES['foto']['tmp_name'])){
							        	$error_upload = $this->upload->display_errors();
							        }
									if(!empty($error_upload)){
										$data['errors'] = array('foto' => $error_upload);
										$data['status'] = FALSE;
							    }
									else if($this->input->post("action")=="add"){
										$this->db->insert("mentor",$dt);
										$data['message'] = "Mentor berhasil disimpan";
									}
									else if($this->input->post("action")=="edit")
									{
										$this->db->where("id_mentor",$postdata['id'])->update("mentor",$dt);
										$data['message'] = "Mentor berhasil diubah";
									}

							}
							else{
									$errors = $this->form_validation->error_array();
									$data['status'] = FALSE;
									$data['errors'] = $errors;
							}

							$data['csrf_hash']	= $this->security->get_csrf_hash();

							echo json_encode($data);
					}
			}
	}

	public function delete()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post("id");
			if($_POST && $id)
			{
				$data['status'] = true;

				// delete lampiran
				$dt = $this->db->where("id_mentor",$id)->get("mentor")->row();
				if($dt && $dt->foto && $dt->foto!="default.png")
				{
					$path = "./data/mentor/".$dt->foto;
					if(file_exists($path))
					{
						unlink($path);
					}
				}

				$status = $this->db->where("id_mentor",$id)->delete("mentor");

				if($status==true){
					$data['message'] = "Mentor berhasil dihapus";
				}
				else
				{
					$data['status'] = false;
					$data['message'] = "Mentor gagal dihapus";
				}

				$data['csrf_hash']	= $this->security->get_csrf_hash();
				echo json_encode($data);
			}
		}
	}




	public function detail($id_mentor=null)
	{
		if ($this->user_id)
		{
			$data['title']		= "Ref_mentor";
			$data['content']	= "ref_mentor/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";
			$data['id_user'] = $this->user_id;
			$id_user = $this->user_id;

			$detail = $this->db->where("id_mentor",$id_mentor)->get("mentor")->row();
			if(!$detail || $id_mentor==null)
			{
				show_404();
			}

			$data['detail'] = $detail;


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function edit($id_mentor=null)
	{
		if ($this->user_id)
		{
			$data['title']		= "Ref_mentor";
			$data['content']	= "ref_mentor/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";
			$data['id_user'] = $this->user_id;
			$id_user = $this->user_id;

			$detail = $this->db->where("id_mentor",$id_mentor)->get("mentor")->row();
			if(!$detail || $id_mentor==null)
			{
				show_404();
			}

			$data['detail'] = $detail;

			$data['file_max_size'] = $this->file_max_size;
			$data['file_type_allowed'] = implode(",",$this->file_type_allowed);

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


}
?>
