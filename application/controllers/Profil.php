<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		$data['title'] = "Daftar Layanan - " .$this->company_profile_model->nama;
		$data['active_menu'] = "profil";
	
			//sambutan
		$this->load->model('company_profile_model');
		$data['sambutan'] = $this->company_profile_model->get_all_sambutan();
		$data['visimisi'] = $this->company_profile_model->get_all_vm();


		$this->load->view('blog/profil',$data);
	}

	


}
?>