<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_bantuan extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		//$this->load->model('helpdesk_model');
		$id_helpdesk = $this->uri->segment(3);
		if($this->session->userdata("user_level")>1){
			redirect("home");
		}
	}

	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Manage bantuan";
			$data['content']	= "manage_bantuan/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_bantuan";

			

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah bantuan ";
			$data['content']	= "manage_bantuan/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_bantuan";



			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	

	public function detail()
	{
		if ($this->user_id)
		{
			$data['title']		= "Detail bantuan";
			$data['content']	= "manage_bantuan/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_helpdesk";
			$data['id_user'] = $this->user_id;
			$id_user = $this->user_id;
			


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


	public function hapus_bantuan()
	{
		if ($this->user_id)
		{
			//$listing = $this->helpdesk_model->get_by_id($id_helpdesk);
			//$this->helpdesk_model->hapus_helpdesk($id_helpdesk);
			
			//redirect('manage_helpdesk');
		}
		else
		{
			redirect('admin');
		}
	}

	

	

	

	
}
?>