<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Create By ARYO
 * Youtube : Aryo Coding
 */
class Userlevel extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model(array('Mod_menu','user_model','Mod_userlevel'));
        $this->user_model->user_id = $this->user_id;
        $this->user_model->set_user_by_user_id();
        $this->user_picture = $this->user_model->user_picture;
        $this->full_name    = $this->user_model->full_name;
        $this->user_level   = $this->user_model->level;
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->library('session');
        $this->load->model('tank_auth/users');

        $this->load->helper('text');
        $this->load->helper('typography');
        $this->load->helper('file');

        $this->bulan = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'Nopember',
            12 => 'Desember',
        );


        if ($this->tank_auth->is_logged_in()) {
            redirect ('welcome'); 
        }
    }

    public function index()
    {
       if ($this->user_id)
       {
        $data['title']      = app_name;
        $data['content']    = "userlevel/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;

        $data['user_level']     = $this->user_level;

        $data['active_menu'] = "Userlevel";


        $this->load->view('admin/index',$data);

    }
    else
    {
     redirect('admin/login');
 }
}


public function dashboard()
{
    $data["level"] = $this->Mod_userlevel->getAll()->result();
    $this->load->view('admin/userlevel/dashboard_view',$data);
}


public function add()
{
    $validation = $this->form_validation; 
    $validation->set_rules($this->rules()); 
    if ($validation->run()) {
        $data['title']      = app_name;
        $data['content']    = "userlevel/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "userlevel";
        $this->insert();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User Level berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect("userlevel");
    } else {
        $data['title']      = 'Tambah User Level ' . app_name;
        $data['content']    = "userlevel/add" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "userlevel";
        $this->load->view('admin/index',$data);
    }
}


public function insert()
{


    $save  = array(
        'level' => $this->input->post('level')
    );

    $this->Mod_userlevel->insertlevel("user_level", $save);
    $id =$this->db->insert_id();

    $menus = $this->Mod_userlevel->getMenu()->result();
        // Insert Akses Menu
    foreach($menus as $key) {
     $idmenu= $key->id_menu;
     $datamenu =array(
        'id_level'=> $id,
        'id_menu'=> $idmenu,
    );
     $this->Mod_userlevel->insert_akses_menu('tbl_akses_menu', $datamenu);
 }

 $submenus = $this->db->get('tbl_submenu')->result();
 foreach ($submenus as $submenu) {
    $datasubmenu = array(
        'id_level'  => $id,
        'id_submenu'   => $submenu->id_submenu,
    );
    $this->db->insert('tbl_akses_submenu', $datasubmenu);
}
}

public function edit($id)
{
   $data['level'] = $this->Mod_userlevel->getUserlevel($id);
   $validation = $this->form_validation; 
   $validation->set_rules($this->rules()); 
   if ($validation->run()) {
    $data['title']      = app_name;
    $data['content']    = "userlevel/dashboard" ;
    $data['user_picture'] = $this->user_picture;
    $data['full_name']      = $this->full_name;
    $data['user_level']     = $this->user_level;
    $data['active_menu'] = "userlevel";
    $this->update();
    $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Menu berhasil disimpan. 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>');
    redirect("userlevel");
} else {
    $data['title']      = 'Tambah User Level ' . app_name;
    $data['content']    = "userlevel/edit" ;
    $data['user_picture'] = $this->user_picture;
    $data['full_name']      = $this->full_name;
    $data['user_level']     = $this->user_level;
    $data['active_menu'] = "userlevel";
    $this->load->view('admin/index',$data);
}
    // $this->load->view('admin/mainmenu/edit');
}


public function update()
{

    $id = $this->input->post('level_id');
    $save  = array(
        'level' => $this->input->post('level')
    );

    $this->Mod_userlevel->update($id, $save);

}

public function delete(){
    $id = $this->input->post('level_id');
    $this->Mod_userlevel->delete($id, 'user_level');
    $data['status'] = TRUE;
    echo json_encode($data);
}



public function view_akses_menu($id)
{
    $data['id_level'] = $id;
    $data['data_menu'] = $this->Mod_userlevel->view_akses_menu()->result();
    $data['data_submenu'] = $this->Mod_userlevel->akses_submenu()->result();
    $data['title']      = 'Hak AKses' . app_name;
    $data['content']    = "userlevel/view_akses_menu" ;
    $data['user_picture'] = $this->user_picture;
    $data['full_name']      = $this->full_name;
    $data['user_level']     = $this->user_level;
    $data['active_menu'] = "userlevel";
    $this->load->view('admin/index',$data);
    // $this->load->view('admin/userlevel/view_akses_menu', $data);
}

public function update_akses_menu()
{
    $view =$this->input->post('view');
    $id =$this->input->post('id');
    $idmenu =$this->input->post('id_menu');
    $id_level = $this->input->post('level');
    if ($id!='0') {
        $up = array(
            'view' => $view
        );
        $this->Mod_userlevel->update_aksesmenu($id, $up);
    } else {
        $datamenu =array(
        'id_level'=> $id_level,
        'id_menu'=> $idmenu,
        'view' => $view
    );
     $this->Mod_userlevel->insert_akses_menu('tbl_akses_menu', $datamenu);
    }
    
    
    echo json_encode(array("status" => TRUE));
}

public function view_akses_submenu()
{
    $id = $this->input->post('id');
    $data['data_submenu'] = $this->Mod_userlevel->akses_submenu($id)->result();
    $this->load->view('admin/view_akses_submenu', $data);

}
public function update_akses_submenu()
{
    $view =$this->input->post('view');
    $id =$this->input->post('id');
    $id_submenu =$this->input->post('id_submenu');
    $id_level = $this->input->post('level');
    if ($id!='0') {
        $up = array(
            'view' => $view
        );

        $this->Mod_userlevel->update_akses_submenu($id, $up);
    }else{
        $datasubmenu = array(
            'id_level'  => $id_level,
            'id_submenu'   => $id_submenu,
            'view' => $view
        );
        $this->db->insert('tbl_akses_submenu', $datasubmenu);
    }
    
    echo json_encode(array("status" => TRUE));
}
public function rules()
{
    return [
        [
            'field' => 'level',
            'label' => 'Level',
            'rules' => 'trim|required'
        ],
    ];
}
}