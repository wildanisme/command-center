<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_kelompok extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		if (!$this->user_id)
		{
			redirect("admin/login");
		}

		if($this->user_level!="Administrator")
		{
			show_404();
		}

		$this->load->model('Kelompok_model');

		$this->file_max_size = 1000; // 1mb
		$this->file_type_allowed = ['jpg','jpeg','png'];
	}


	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Manage Kelompok";
			$data['content']	= "manage_kelompok/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";

			$data['dt_kecamatan'] = $this->db->where("id_kabupaten",3211)
			->order_by("kecamatan","ASC")
			->get("kecamatan")->result();

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function get_list($rowno=1)
	{
			if($this->input->is_ajax_request())
			{
					// Row per page
					$rowperpage = 6;
					$offset = ($rowno-1) * $rowperpage;

					$param = array();
					$param['limit']     = $rowperpage;
					$param['offset']    = $offset;
					$data = array();

					if($this->input->post("search"))
					{
							$param['search'] = $this->input->post("search");
					}

					if($this->input->post("id_kecamatan"))
					{
							$param['where']['kelompok.id_kecamatan'] = $this->input->post("id_kecamatan");
					}


					$query = $this->Kelompok_model->get($param);
					$result = $query->result();

					$param_total = $param;
					unset($param_total['limit']);
					unset($param_total['offset']);
					$query_total = $this->Kelompok_model->get($param_total);
					$total_rows = $query_total->num_rows();


					// Pagination Configuration
					$config['base_url'] = "#";
					$config['use_page_numbers'] = TRUE;
					$config['total_rows'] = $total_rows;
					$config['per_page'] = $rowperpage;
					//$config['attributes'] = array('class' => 'btn btn-default btn-outline btn-xm');

					$config['cur_tag_open']     = '<li class="page-item active" aria-current="page"><a style="padding:10px 15px"  class=" page-link">';
					$config['cur_tag_close']    = '<a class="sr-only">(current)</a></a></li>';

					// Initialize
					$this->load->library('pagination');
					$this->pagination->initialize($config);

					$link = $this->pagination->create_links();
					//$link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm disabled' >", $link);
					//$link = str_replace("</strong>", "</button>", $link);
					$link = str_replace("<li>",'<li class="page-item">',$link);
					$link = str_replace('data-ci-pagination-page','class="page-link" data-ci-pagination-page',$link);

					// Initialize $data Array
					$data['pagination'] = $link;
					$data['result']     = $result;
					$data['row']        = $offset;
					$data['csrf_hash']  = $this->security->get_csrf_hash();
					$data['row_content'] = $this->rowContent($result);


					echo json_encode($data);

			}
	}

	private function rowContent($data)
	{
			$contents = "";
			foreach ($data as $row) {
					$jumlah_anggota = $this->db->where("id_kelompok",$row->id_kelompok)->get("anggota")->num_rows();
					$contents .= '

					<div class="col-xl-4 col-md-6 col-sm-12 profile-card-1">
							<div class="card">
									<div class="card-header mx-auto">
											<div class="avatar-xl" >
													<img class="img-fluid" src="'.base_url().'data/images/avatar.png" alt="Kelompok" style="width: 220px">
											</div>
									</div>

									<div class="card-content">
											<div class="card-body text-center">
													<h4>'.$row->nama_kelompok.'</h4>
													<div class="badge badge-success">'.$row->nama_jenis_usaha.'</div>

													<div class="d-flex justify-content-between">
															<div class="float-left">
																	<i class="feather icon-star text-warning mr-50"></i> '.$row->nama_kecamatan.'
															</div>
															<div class="float-right">
																	<i class="feather icon-briefcase text-primary mr-50"></i> '.$jumlah_anggota.' Anggota
															</div>
													</div>
													<hr style="border-top: 1px solid rgb(0 0 0 / 15%);">
													<div class="d-flex justify-content-between">
														 <a href="'.base_url().'manage_kelompok/detail/'.$row->id_kelompok.'" class="btn btn-outline-primary waves-effect waves-light btn-block">Detail</a>
												 </div>

										 </div>
								 </div>

						 </div>
				 </div>
									';
			}

			if($contents=="")
			{
					$contents = "
					<div class='col-md-12'>
					<p class='text-center mt-5'>- Tidak ada data -</p>
					</div>
					";
			}

			return $contents;
	}

	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah Kelompok ";
			$data['content']	= "manage_kelompok/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";
			$data['map'] = true;

			$data['dt_jenis_usaha'] = $this->db->where("status","active")->get("ref_jenis_usaha")->result();
			$data['dt_mentor'] = $this->db->where("status","active")->get("mentor")->result();

			$data['dt_kecamatan'] = $this->db->where("id_kabupaten",3211)
			->order_by("kecamatan","ASC")
			->get("kecamatan")->result();

			$data['dt_ketua'] = $this->db->where("user_status","Active")
			->where("user_level",3)//ketua
			->where("id_kelompok",null)
			->get("user")->result();

			if($this->input->post("id_kecamatan"))
			{
				$data['dt_desa'] = $this->db->where("id_kecamatan",$this->input->post("id_kecamatan"))
				->order_by("desa","ASC")
				->get("desa")->result();
			}

			$this->load->library('form_validation');

			if($_POST)
			{
				//echo "<pre>";print_r($_POST);die;
				$html_escape = html_escape($_POST);
				$postdata = array();
				foreach($html_escape as $key=>$value)
				{
						$postdata[$key] = $this->security->xss_clean($value);
				}

				$this->form_validation->set_data( $postdata );
				$validation_rules = [
						[
								'field' => 'nama_kelompok',
								'label' => 'Nama',
								'rules' => 'required|regex_match[/^[a-z\d\-_\s\'\ "]+$/i]|max_length[50]',
								//'rules' => 'required|max_length[255]',
								'errors' => [
										'required' => '%s diperlukan',
										'max_length' => 'Maximal 50 Char',
										'regex_match' => '%s tidak valid',
								]
						],
						[
								'field' => 'id_jenis_usaha',
								'label' => 'Jenis Usaha',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'id_mentor',
								'label' => 'Mentor',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],

						[
								'field' => 'id_ketua',
								'label' => 'Ketua kelompok',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'telepon',
								'label' => 'Telepon',
								'rules' => 'required|numeric|max_length[15]',
								'errors' => [
										'required' => '%s diperlukan',
										'max_length' => 'Maximal 15 Char',
										'numeric' => '%s harus angka',
								]
						],
						[
								'field' => 'id_kecamatan',
								'label' => 'Kecamatan',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'id_desa',
								'label' => 'Desa',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'alamat',
								'label' => 'Alamat',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'alamat',
								'label' => 'Alamat',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'status',
								'label' => 'Status',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'latitude',
								'label' => 'Latitude',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'longitude',
								'label' => 'Longitude',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],

				];

				$this->form_validation->set_rules( $validation_rules );

				if( $this->form_validation->run() )
				{
					$dt = array(
						'nama_kelompok'		=> $postdata['nama_kelompok'],
						'id_ketua'				=> $postdata['id_ketua'],
						'id_desa'					=> $postdata['id_desa'],
						'id_kabupaten'		=> '3211', // sumedang
						'id_kecamatan'		=> $postdata['id_kecamatan'],
						'alamat'					=> $postdata['alamat'],
						'telepon'					=> $postdata['telepon'],
						'id_mentor'				=> $postdata['id_mentor'],
						'id_jenis_usaha'	=> $postdata['id_jenis_usaha'],
						'latitude'				=> $postdata['latitude'],
						'longitude'				=> $postdata['longitude'],
						'status'					=> $postdata['status'],
						'facebook'				=> $postdata['facebook'],
						'instagram'				=> $postdata['instagram'],
						'tokopedia'				=> $postdata['tokopedia'],
						'shopee'					=> $postdata['shopee'],
						'bukalapak'				=> $postdata['bukalapak'],
					);
					$this->db->insert("kelompok",$dt);
					$id = $this->db->insert_id();

					$this->db->set("id_kelompok",$id)
					->where("user_id",$dt['id_ketua'])
					->update("user");

					$this->session->set_flashdata("success","Data berhasil disimpan");
					redirect("manage_kelompok/edit/".$id);
				}
				else{
					foreach ($postdata as $key => $value) {
						$data[$key] = $value;
					}

				}

			}

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}

	public function edit($id_kelompok=null)
	{
		if ($this->user_id)
		{

			$param['where']['kelompok.id_kelompok'] = $id_kelompok;
			$detail = $this->Kelompok_model->get($param)->row();
			if(!$detail || !$id_kelompok)
			{
				show_404();
			}
			foreach ($detail as $key => $value) {
				$data[$key] = $value;
			}

			$this->load->model("Anggota_model");
			$data['dt_anggota'] = $this->Anggota_model->get($param)->result();

			$data['title']		= "Edit Kelompok ";
			$data['content']	= "manage_kelompok/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";
			$data['map'] = true;
			$data['datepicker'] = true;

			$data['dt_jenis_usaha'] = $this->db->where("status","active")->get("ref_jenis_usaha")->result();
			$data['dt_mentor'] = $this->db->where("status","active")->get("mentor")->result();

			$data['dt_kecamatan'] = $this->db->where("id_kabupaten",3211)
			->order_by("kecamatan","ASC")
			->get("kecamatan")->result();

			$data['dt_ketua'] = $this->db->where("user_status","Active")
			->where("user_level",3)//ketua
			->where("(id_kelompok is null OR id_kelompok = '$detail->id_kelompok' )")
			->get("user")->result();

			if($this->input->post("id_kecamatan"))
			{
				$id_kecamatan = $this->input->post("id_kecamatan");
			}
			else{
				$id_kecamatan = $detail->id_kecamatan;
			}
			$data['dt_desa'] = $this->db->where("id_kecamatan",$id_kecamatan)
			->order_by("desa","ASC")
			->get("desa")->result();

			$this->load->library('form_validation');

			if($_POST)
			{
				//echo "<pre>";print_r($_POST);die;
				$html_escape = html_escape($_POST);
				$postdata = array();
				foreach($html_escape as $key=>$value)
				{
						$postdata[$key] = $this->security->xss_clean($value);
				}

				$this->form_validation->set_data( $postdata );
				$validation_rules = [
						[
								'field' => 'nama_kelompok',
								'label' => 'Nama',
								'rules' => 'required|regex_match[/^[a-z\d\-_\s\'\ "]+$/i]|max_length[50]',
								//'rules' => 'required|max_length[255]',
								'errors' => [
										'required' => '%s diperlukan',
										'max_length' => 'Maximal 50 Char',
										'regex_match' => '%s tidak valid',
								]
						],
						[
								'field' => 'id_jenis_usaha',
								'label' => 'Jenis Usaha',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'id_mentor',
								'label' => 'Mentor',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],

						[
								'field' => 'id_ketua',
								'label' => 'Ketua kelompok',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'telepon',
								'label' => 'Telepon',
								'rules' => 'required|numeric|max_length[15]',
								'errors' => [
										'required' => '%s diperlukan',
										'max_length' => 'Maximal 15 Char',
										'numeric' => '%s harus angka',
								]
						],
						[
								'field' => 'id_kecamatan',
								'label' => 'Kecamatan',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'id_desa',
								'label' => 'Desa',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'alamat',
								'label' => 'Alamat',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'alamat',
								'label' => 'Alamat',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'status',
								'label' => 'Status',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'latitude',
								'label' => 'Latitude',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],
						[
								'field' => 'longitude',
								'label' => 'Longitude',
								'rules' => 'required',
								'errors' => [
										'required' => '%s diperlukan',
								]
						],

				];

				$this->form_validation->set_rules( $validation_rules );

				if( $this->form_validation->run() )
				{
					$dt = array(
						'nama_kelompok'		=> $postdata['nama_kelompok'],
						'id_ketua'				=> $postdata['id_ketua'],
						'id_desa'					=> $postdata['id_desa'],
						'id_kabupaten'		=> '3211', // sumedang
						'id_kecamatan'		=> $postdata['id_kecamatan'],
						'alamat'					=> $postdata['alamat'],
						'telepon'					=> $postdata['telepon'],
						'id_mentor'				=> $postdata['id_mentor'],
						'id_jenis_usaha'	=> $postdata['id_jenis_usaha'],
						'latitude'				=> $postdata['latitude'],
						'longitude'				=> $postdata['longitude'],
						'status'					=> $postdata['status'],
						'facebook'				=> $postdata['facebook'],
						'instagram'				=> $postdata['instagram'],
						'tokopedia'				=> $postdata['tokopedia'],
						'shopee'					=> $postdata['shopee'],
						'bukalapak'				=> $postdata['bukalapak'],
					);
					$this->db->where("id_kelompok",$id_kelompok);
					$this->db->update("kelompok",$dt);

					if($detail->id_ketua != $postdata['id_ketua'])
					{
						$this->db->set("id_kelompok",null)
						->where("user_id",$detail->id_ketua)
						->update("user");
					}


					$this->db->set("id_kelompok",$id_kelompok)
					->where("user_id",$dt['id_ketua'])
					->update("user");

					$this->session->set_flashdata("success","Data berhasil diubah");
					redirect("manage_kelompok/edit/".$id_kelompok);
				}
				else{
					foreach ($postdata as $key => $value) {
						$data[$key] = $value;
					}

				}

			}

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	public function get_desa($id_kecamatan = false)
	{
			$data['dt_desa'] = array();
			if($this->input->is_ajax_request() )
			{
				if($id_kecamatan)
				{
					$data['dt_desa'] = $this->db->where("id_kecamatan",$id_kecamatan)
					->order_by("desa","ASC")
					->get("desa")->result();
				}
			}
			echo json_encode($data);
	}


	public function save_anggota()
	{
			if($this->input->is_ajax_request() )
			{
					if($_POST)
					{
							$data['status'] = true;
							$data['errors'] = array();
							$html_escape = html_escape($_POST);
							$postdata = array();
							foreach($html_escape as $key=>$value)
							{
									$postdata[$key] = $this->security->xss_clean($value);
							}


							$this->load->library('form_validation');

							$this->form_validation->set_data( $postdata );




									$validation_rules = [
											[
													'field' => 'nama',
													'label' => 'Nama',
													'rules' => 'required|regex_match[/^[a-z\d\-_\s\'\ "]+$/i]|max_length[50]',
													//'rules' => 'required|max_length[255]',
													'errors' => [
															'required' => '%s diperlukan',
															'max_length' => 'Maximal 50 Char',
															'regex_match' => '%s tidak valid',
													]
											],
											[
													'field' => 'no_hp',
													'label' => 'Telepon',
													'rules' => 'numeric|max_length[15]',
													'errors' => [
															'max_length' => 'Maximal 15 Char',
															'numeric' => '%s harus angka',
													]
											],
											[
													'field' => 'alamat',
													'label' => 'Alamat',
													'rules' => 'required',
													'errors' => [
															'required' => '%s diperlukan',
													]
											],
											[
													'field' => 'nik',
													'label' => 'No KTP',
													'rules' => 'required|numeric',
													'errors' => [
															'required' => '%s diperlukan',
															'numeric' => '%s harus angka',
													]
											],
											[
													'field' => 'id_kelompok',
													'label' => 'Kelompok',
													'rules' => 'required',
													'errors' => [
															'required' => '%s diperlukan',
													]
											],
											[
													'field' => 'status',
													'label' => 'Status',
													'rules' => 'required',
													'errors' => [
															'required' => '%s diperlukan',
													]
											],
									];



							$this->form_validation->set_rules( $validation_rules );

							if( $this->form_validation->run() )
							{
								if($this->input->post("action")=="edit"){
									$anggota = $this->db->where("id_anggota",$postdata['id_anggota'])->get("anggota")->row();
								}


									$dt = array();
									$dt['nama']    = $postdata['nama'];
									$dt['no_dtks']    = $postdata['no_dtks'];
									$dt['no_kk']        = $postdata['no_kk'];
									$dt['alamat']        = $postdata['alamat'];
									$dt['nik']        = $postdata['nik'];
									$dt['jenis_kelamin']        = $postdata['jenis_kelamin'];
									$dt['tgl_lahir']        = $postdata['tgl_lahir'];
									$dt['jabatan']        = $postdata['jabatan'];
									$dt['id_kelompok']        = $postdata['id_kelompok'];
									$dt['status']        = $postdata['status'];
									$dt['no_hp']        = $postdata['no_hp'];

									// upload picture
									$config['upload_path']="./data/anggota/";
							        $config['allowed_types']=implode("|", $this->file_type_allowed);
							        $config['encrypt_name'] = TRUE;
							        $config['max_size']     = $this->file_max_size;

							        $this->load->library('upload',$config);
							        if($this->upload->do_upload("foto")){
							            $dt['foto']= $this->upload->data('file_name');

							            if(!empty($anggota) && $anggota->foto!="default.png" && $this->input->post("action")=="edit"){
														$path = $config['upload_path'].$anggota->foto;
														if(file_exists($path)){
															unlink($path);
														}
							            }
							        }
							        else if (!empty($_FILES['foto']['tmp_name'])){
							        	$error_upload = $this->upload->display_errors();
							        }
									if(!empty($error_upload)){
										$data['errors'] = array('foto' => $error_upload);
										$data['status'] = FALSE;
							    }
									else if($this->input->post("action")=="add"){
										$this->db->insert("anggota",$dt);
										$data['message'] = "Anggota berhasil disimpan";
									}
									else if($this->input->post("action")=="edit")
									{
										$this->db->where("id_anggota",$postdata['id_anggota'])->update("anggota",$dt);
										$data['message'] = "Anggota berhasil diubah";
									}

							}
							else{
									$errors = $this->form_validation->error_array();
									$data['status'] = FALSE;
									$data['errors'] = $errors;
							}

							$data['csrf_hash']	= $this->security->get_csrf_hash();

							echo json_encode($data);
					}
			}
	}


	public function delete_anggota()
	{
		if($this->input->is_ajax_request())
		{
			$id = $this->input->post("id");
			if($_POST && $id)
			{
				$data['status'] = true;

				// delete lampiran
				$dt = $this->db->where("id_anggota",$id)->get("anggota")->row();
				if($dt && $dt->foto && $dt->foto!="default.png")
				{
					$path = "./data/anggota/".$dt->foto;
					if(file_exists($path))
					{
						unlink($path);
					}
				}

				$status = $this->db->where("id_anggota",$id)->delete("anggota");

				if($status==true){
					$data['message'] = "Anggota berhasil dihapus";
				}
				else
				{
					$data['status'] = false;
					$data['message'] = "Anggota gagal dihapus";
				}

				$data['csrf_hash']	= $this->security->get_csrf_hash();
				echo json_encode($data);
			}
		}
	}

	public function detail($id_kelompok=null)
	{
		if ($this->user_id)
		{
			$data['title']		= "Detail Kelompok";
			$data['content']	= "manage_kelompok/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";
			$data['map'] = true;



			$param['where']['kelompok.id_kelompok'] = $id_kelompok;
			$detail = $this->Kelompok_model->get($param)->row();
			if(!$detail || !$id_kelompok)
			{
				show_404();
			}
			foreach ($detail as $key => $value) {
				$data[$key] = $value;
			}

			$this->load->model("Anggota_model");
			$data['dt_anggota'] = $this->Anggota_model->get($param)->result();

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

		public function detail_anggota($id_anggota=null)
		{
			if ($this->user_id)
			{
				$data['title']		= "Detail Anggota";
				$data['content']	= "manage_kelompok/detail_anggota" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "ref";
				$data['id_user'] = $this->user_id;
				$id_user = $this->user_id;

				$param['where']['anggota.id_anggota'] = $id_anggota;
				$detail = $this->Anggota_model->get($param)->row();
				if(!$detail || !$id_anggota)
				{
					show_404();
				}
				foreach ($detail as $key => $value) {
					$data[$key] = $value;
				}


				$this->load->view('admin/index',$data);


			}
			else
			{
				redirect('admin');
			}
		}


		public function detail_produk()
		{
			if ($this->user_id)
			{
				$data['title']		= "Detail Produk";
				$data['content']	= "manage_kelompok/detail_produk" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "ref";
				$data['id_user'] = $this->user_id;
				$id_user = $this->user_id;



				$this->load->view('admin/index',$data);


			}
			else
			{
				redirect('admin');
			}
		}




		public function hapus_kelompok()
		{
			if ($this->user_id)
			{
			//$listing = $this->helpdesk_model->get_by_id($id_helpdesk);
			//$this->helpdesk_model->hapus_helpdesk($id_helpdesk);

			//redirect('manage_helpdesk');
			}
			else
			{
				redirect('admin');
			}
		}








	}
	?>
