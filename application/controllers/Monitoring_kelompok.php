<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring_kelompok extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		//$this->load->model('helpdesk_model');
		$id_helpdesk = $this->uri->segment(3);
		if($this->session->userdata("user_level")>1){
			redirect("home");
		}
	}
	
	
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Monitoring Kelompok";
			$data['content']	= "monitoring_kelompok/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "monitoring_kelompok";

			

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	

	public function detail()
	{
		if ($this->user_id)
		{
			$data['title']		= "Detail Kelompok";
			$data['content']	= "monitoring_kelompok/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "monitoring_helpdesk";
			$data['id_user'] = $this->user_id;
			$id_user = $this->user_id;
			


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}



}
?>