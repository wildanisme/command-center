<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/integration/PTSP_service.php');
require(APPPATH.'libraries/integration/Mpp_service.php');

class Admin extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->load->model("Investasi_model");
		$this->load->model("Investasiopd_model");
		$this->load->model("PendapatanPajak_model");
		$this->load->model("BelanjaLangsung_model");
		$this->load->model("BelanjaTidakLangsung_model");
		$this->user_level	= $this->user_model->level;
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');

		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);


		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index()
	{
		
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$tahunSaatIni = date("Y");
			$Investasi = $this->Investasi_model; 
			$Investasio = $this->Investasiopd_model; 
			$PendapatanPajak = $this->PendapatanPajak_model;
			$BelanjaLangsung = $this->BelanjaLangsung_model;
			$BelanjaTidakLangsung = $this->BelanjaTidakLangsung_model;
			$data["data_investasi"] = $Investasi->getByTahun($tahunSaatIni);
			$data["data_investasio"] = $Investasio->getRealisasiPendapatanByTahun($tahunSaatIni);
			$data["RealisasiPendapatan"] = $Investasio->getRealisasiBelanjaByTahun($tahunSaatIni);	
			$data["TargetInvestasio"] = $Investasio->getTargetInvestasioByTahun($tahunSaatIni);	
			$data["RealisasiInvestasio"] = $Investasio->getRealisasiInvestasioByTahun($tahunSaatIni);
			$data["Apbd"] = $Investasio->getApbdByTahun($tahunSaatIni);			
			$data['data_pendapatan_pajak'] =$PendapatanPajak->getByTahun($tahunSaatIni);
			$data['data_belanja_langsung'] =$BelanjaLangsung->getByTahun($tahunSaatIni);
			$data['data_belanja_tidak_langsung'] =$BelanjaTidakLangsung->getByTahun($tahunSaatIni);
			$data['title']		= app_name;
			$data['content']	= "dashboard" ; 
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "dashboard";
			$data['page'] = "dashboard";
			$data["jumkec"]=$this->db->query('SELECT kecamatan,count(nama_umkm) as totalumkm,sum(asset) 
		as totalaset,sum(hasil_usaha) as totalhasil FROM umkm group by kecamatan')->result();

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			$Ptsp  = new PTSP_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $data['tahun'] = $tahun;
            $param = array(
                'tahun' => $tahun
            );
            $end_point = "integration/commandcenter/init_v2";

            $rs = json_decode($Ptsp->sendRequest($param,$end_point));
            
            if($rs->error == false){
				//die;
                //echo "<pre>";print_r($rs->data);die;
                $data['data_perizinan'] = $rs->data;                
                // $data['data_perizinan'] = array();                
			}
			
			$Mpp  = new Mpp_service();
            $rs_mpp = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs_mpp->error == false){
                //echo "<pre>";print_r($rs->data);die;
                $data['data_mpp'] = $rs_mpp->data;
                // $data['data_mpp'] = array();

                
			}
			
			
			$this->load->view('admin/index',$data);
		}
		else{
			redirect('admin/login');
			
		}
	}

	public function dashboard()
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			$data['title']		= app_name;
			$tahunSaatIni = date("Y");
			$Investasi = $this->Disduk_model; 
			$data["data_investasi"] = $Disduk->getByTahun($tahunSaatIni);
			$data['content']	= "dashboard_user" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "dashboard";
			$this->load->model('dashboard_model');

			
			$data['user']	= $this->dashboard_model->get_user();
			$data['post']	= $this->dashboard_model->get_post();


			//logs
			$this->load->model('logs_model');
			$data['logs']	= $this->logs_model->get_some();


			//logs
			$this->load->model('logs_model');
			$data['logs']	= $this->logs_model->get_some_id($this->user_id);

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();		


			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('home');
		}
	}

	


	public function login()
	{
		
		if ($this->user_id)
		{
			redirect('admin');
		}

		else
		{
			
			$data['title']		= "Admin - Login".$this->session->userdata('user_id');
			if (!empty($_POST))
			{
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
				if ($this->form_validation->run() == FALSE)
				{
					$data['pesan'] ="Login gagal.<br>username atau password tidak tepat";
				}

				elseif ($_POST['username'] !="" && $_POST['password'] !="")
				{
					$this->load->model('user_model');
					$this->user_model->username = $_POST['username'];
					$this->user_model->password = $_POST['password'];
					$valid = $this->user_model->validasi_login();
					if ($valid)
					{
						$login = $this->user_model->cek_status_user();
						if ($login)
						{
							 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
							$this->logs_model->activity = "has login";
							$this->logs_model->category = "login";
							$desc = $_POST['username'];
							$this->logs_model->description = "with username ".$desc;
							$this->logs_model->insert();
							
							redirect('admin');

							

						}
						else
						{
							$data['pesan']	= "Login gagal.<br>Akun anda tidak aktif.";
						}
					}
					else
					{
						$data['pesan']	= "Login gagal.<br>username atau password tidak tepat";
					}
				}
				else
				{
					//echo"<script type='javascript'>alert('username atau Password tidak boleh kosong)');</script>";
					$data['pesan']	= "username atau Password tidak boleh kosong";
				}
			}
			$this->load->view('admin/login',$data);
		}
	}

	

	public function logout()
	{
		$CI=& get_instance();
		$CI->session->sess_destroy();
		redirect('admin/login');
	}
	public function page_not_found()
	{
		if ($this->user_id)
		{
			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes'); 
			$data['title']		= app_name;
			$data['content']	= "404" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "";
			$this->load->view('admin/index',$data);
		}

		else
		{
			redirect('admin/login');
		}
	}

	public function blank()
	{
		$this->load->view('admin/blank');
	}


	// public function index()
	// {
		
	// 	if ($this->user_id && $this->session->userdata("user_level")==1)
	// 	{
	// 		//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
	// 		//if ($this->user_level=="User") redtirect ('admin/dashboard'); 
	// 		$tahunSaatIni = date("Y");
	// 		$Investasi = $this->Disduk_model; 
	// 		$PendapatanPajak = $this->PendapatanPajak_model;
	// 		$BelanjaLangsung = $this->BelanjaLangsung_model;
	// 		$BelanjaTidakLangsung = $this->BelanjaTidakLangsung_model;
	// 		$data["data_investasi"] = $Investasi->getByTahun($tahunSaatIni);
	// 		$data['data_pendapatan_pajak'] =$PendapatanPajak->getByTahun($tahunSaatIni);
	// 		$data['data_belanja_langsung'] =$BelanjaLangsung->getByTahun($tahunSaatIni);
	// 		$data['data_belanja_tidak_langsung'] =$BelanjaTidakLangsung->getByTahun($tahunSaatIni);
	// 		$data['title']		= app_name;
	// 		$data['content']	= "dashboard" ;
	// 		$data['user_picture'] = $this->user_picture;
	// 		$data['full_name']		= $this->full_name;
	// 		$data['user_level']		= $this->user_level;
	// 		$data['active_menu'] = "dashboard";
	// 		$data['page'] = "dashboard";
	// 		$data["jumkec"]=$this->db->query('SELECT kecamatan,count(nama_umkm) as totalumkm,sum(asset) 
	// 	as totalaset,sum(hasil_usaha) as totalhasil FROM umkm group by kecamatan')->result();

	// 		$this->user_model->user_id = $this->session->userdata('user_id');
	// 		$this->user_model->set_user_by_user_id();

	// 		$Ptsp  = new PTSP_service();

    //         $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
    //         $data['tahun'] = $tahun;
    //         $param = array(
    //             'tahun' => $tahun
    //         );
    //         $end_point = "integration/commandcenter/init_v2";

    //         $rs = json_decode($Ptsp->sendRequest($param,$end_point));
            
    //         if($rs->error == false){
	// 			//die;
    //             //echo "<pre>";print_r($rs->data);die;
    //             $data['data_perizinan'] = $rs->data;                
    //             // $data['data_perizinan'] = array();                
	// 		}
			
	// 		$Mpp  = new Mpp_service();
    //         $rs_mpp = json_decode($Mpp->sendRequest($param,$end_point));
            
    //         if($rs_mpp->error == false){
    //             //echo "<pre>";print_r($rs->data);die;
    //             $data['data_mpp'] = $rs_mpp->data;
    //             // $data['data_mpp'] = array();

                
	// 		}
			
			
	// 		$this->load->view('admin/index',$data);
	// 	}
	// 	else{
	// 		redirect('admin/login');
			
	// 	}
	// }


	
}
