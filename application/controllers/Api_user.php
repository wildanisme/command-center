<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;
class Api_user extends REST_Controller {
	function login_post()
	{
		$headers = $this->input->request_headers();
		$authorization = (!empty($headers['app_key'])) ? $headers['app_key'] : null;
		//var_dump($headers);
		//var_dump($this->config->item('authorization'));
		//die($this->config->item('authorization'));
		$username = $this->post('username');
		$password = $this->post('password');
		if($authorization!=null && $authorization== $this->config->item('authorization') && !empty($username) && !empty($password))
		{
			
			$data = $this->db->where('username',$username)->where("password",md5($password))->get("user")->result();
			if(!empty($data[0]))
			{
				$this->db->select("user.user_id,user.nip,user.id_skpd, user.username, user.full_name, concat('".base_url()."/data/user_picture/',user.user_picture) as 'user_picture',ref_skpd.nama_skpd, user.loket");
				$this->db->join('ref_skpd', 'user.id_skpd = ref_skpd.id_skpd','left');
				$data = $this->db->where("user_id",$data[0]->user_id)->get("user")->result();

				// layanan
				if(!empty($data[0])){
					$layanan=$this->db
									//->where('id_skpd',$data[0]->id_skpd)
									->where("loket",$data[0]->loket)
									->get('ref_layanan')->result();
					if($layanan){
						$arrLayanan = array();
						foreach ($layanan as $r) {
							$arrLayanan[]=$r->nama_layanan;
						}
						$data[0]->layanan = implode(",", $arrLayanan);
					}
					$cek = $this->db->where("id_skpd",$data[0]->id_skpd)->where("loket",$data[0]->loket)->get("ref")->num_rows();
		    		$update=array("dashboard"=>1);
		    		$where["loket"]=$data[0]->loket;
		    		$where["id_skpd"]=$data[0]->id_skpd;
		    		if($cek){
		    			$this->db->update("ref",$update,$where);
		    		}
		    		else{
		    			$insert = array_merge($where,$update);
		    			$this->db->insert("ref",$insert);
		    		}
				}
				$response = array('error' => false,'data' => !empty($data[0]) ? $data[0] : null);
			}
			else{
				$response = [
	    			'error'	=> true,
	    			'message' => 'Username / password salah',
	    		];
			}
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid data :',
    		];
		}
		
		
		
		$this->response($response);
	}

	function logout_post()
	{
		$headers = $this->input->request_headers();
		$api_key = (!empty($headers['api_key'])) ? $headers['api_key'] : null;
		$cekApi = $this->user_model->checkApiKey($api_key);

		if($api_key!=null && $cekApi){
			
			$this->db->update('user',array('app_token' => null),array('api_key' => $api_key));

			$response = array(
				'error'		=> false,
				'message'	=> "Logout berhasil",
			);
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid credential',
    		];
		}

		

		$this->response($response);
	}

	function change_password_post()
	{
		$headers = $this->input->request_headers();
		$api_key = (!empty($headers['api_key'])) ? $headers['api_key'] : null;
		$cekApi = $this->user_model->checkApiKey($api_key);
		
		$password_confirmation = $this->post('password_confirmation');
		$password = $this->post('password');
		$current_password = $this->post('current_password');
		if($api_key!=null && $cekApi)
		{
			if(!empty($password_confirmation) && !empty($password) && !empty($current_password)){
			
				if($password_confirmation!= $password){
					$response = [
		    			'error'	=> true,
		    			'message' => 'Konfirmasi password salah',
		    		];
				}
				else if(md5($current_password)  !=  $cekApi->password){
					$response = [
		    			'error'	=> true,
		    			'message' => 'Password salah',
		    		];
				}
				else{
					
					$this->db->update('user',array('password' => md5($password)),array('user_id' => $cekApi->user_id));

					$response = array(
						'error'		=> false,
					);
				}
				
			}
			else{
				$response = [
	    			'error'	=> true,
	    			'message' => 'Data tidak lengkap',
	    		];
			}
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid credential',
    		];
		}

		

		$this->response($response);
	}

	function get_notification_post()
	{
		$headers = $this->input->request_headers();
		$api_key = (!empty($headers['api_key'])) ? $headers['api_key'] : null;
		$cekApi = $this->user_model->checkApiKey($api_key);

		
		if($api_key!=null && $cekApi){
			
			$this->db->where('user_id', $cekApi->user_id);
			
			$this->db->order_by("ndate","DESC");
			$this->db->order_by("ntime","DESC");
			$data = $this->db->get('notification')->result();

			$response = array(
				'error'		=> false,
				'data'		=> $data,
			);
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid credential',
    		];
		}

		

		$this->response($response);
	}

	function get_pengumuman_post()
	{
		$headers = $this->input->request_headers();
		$api_key = (!empty($headers['api_key'])) ? $headers['api_key'] : null;
		$cekApi = $this->user_model->checkApiKey($api_key);

		
		if($api_key!=null && $cekApi){
			
			$this->db->where('tanggal', date('Y-m-d'));
			$data = $this->db->get('pengumuman')->result();

			$response = array(
				'error'		=> false,
				'data'		=> $data,
			);
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid credential',
    		];
		}

		

		$this->response($response);
	}

}