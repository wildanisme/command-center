<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk extends CI_Controller {

	public function __construct() {
        parent::__construct();

		$this->load->model("Penduduk_Model");
    }

	public function index() {
		$data = [];
		$data['title']		= app_name;
			$data['content']	= "dashboard" ;
			// $data['user_picture'] = $this->user_picture;
			// $data['full_name']		= $this->full_name;
			// $data['user_level']		= $this->user_level;
			$data['active_menu'] = "dashboard";
			$data['page'] = "dashboard";

		$data['total_penduduk'] = $this->Penduduk_Model->getTotalPenduduk();
		$data['rata_tembuh'] = $this->Penduduk_Model->getRataTembuh();
		$data['total_laki'] = $this->Penduduk_Model->getTotalLaki();
		$data['total_perempuan'] = $this->Penduduk_Model->getTotalPerempuan();
		$data['jml_penduduk'] = $this->Penduduk_Model->getJmlPenduduk();
		$data['status_kawin'] = $this->Penduduk_Model->getStatusKawin();
		
		$dataPerkecamatan = $this->Penduduk_Model->getPerkecamatan();
		// $dataKec = "";
		$dataCowo = "";
		$dataCewe = "";
		for ($i=0; $i < count($dataPerkecamatan); $i++) { 
			if($i==0){
				// $dataKec = $dataPerkecamatan[$i]->kec;
				$dataCowo = intval($dataPerkecamatan[$i]->cowo);
				$dataCewe = intval($dataPerkecamatan[$i]->cewe);
			} else {
				// $dataKec .= ",".$dataPerkecamatan[$i]->kec;
				$dataCowo .= ",".intval($dataPerkecamatan[$i]->cowo);
				$dataCewe .= ",".intval($dataPerkecamatan[$i]->cewe);
			}
			
		}
		// $data["perKecamatanKec"] = $dataKec;
		$data["perKecamatanCowo"] = $dataCowo;
		$data["perKecamatanCewe"] = $dataCewe;

		$dataKtpKia = $this->Penduduk_Model->getKtpKia();
		$dataKec = "";
		$dataKtpL = "";
		$dataKtpP = "";
		$dataKiaL = "";
		$dataKiaP = "";
		for ($i=0; $i < count($dataKtpKia); $i++) { 
			if($i==0){
				$dataKec = $dataKtpKia[$i]->kec;
				$dataKtpL = intval($dataKtpKia[$i]->KTPL);
				$dataKtpP = intval($dataKtpKia[$i]->KTPP);
				$dataKiaL = intval($dataKtpKia[$i]->KIAL);
				$dataKiaP = intval($dataKtpKia[$i]->KIAP);
			} else {
				$dataKec .= ",".$dataKtpKia[$i]->kec;
				$dataKtpL .= ",".intval($dataKtpKia[$i]->KTPL);
				$dataKtpP .= ",".intval($dataKtpKia[$i]->KTPL);
				$dataKiaL .= ",".intval($dataKtpKia[$i]->KIAL);
				$dataKiaP .= ",".intval($dataKtpKia[$i]->KIAP);
			}
			
		}
		$data["data_kec"] = $dataKec;
		$data["data_ktp_l"] = $dataKtpL;
		$data["data_ktp_p"] = $dataKtpP;
		$data["data_kia_l"] = $dataKiaL;
		$data["data_kia_p"] = $dataKiaP;
		
		$data["jmlPenduduk"] = $this->Penduduk_Model->getJmlPenduduk();
		// $data["perKecamatan"] = $this->Penduduk_Model->getPerKecamatan();
		$data["perAgama"] = $this->Penduduk_Model->getPerAgama();
		$data["perGoldarah"] = $this->Penduduk_Model->getPerGoldarah();
		$data["pendidikan"] = $this->Penduduk_Model->getPendidikan();
		//$this->template->admin('penduduk/index', $data);
		$this->load->view("admin/penduduk/index", $data);
	}
	
}
