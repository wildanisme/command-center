<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->load->model('helpdesk_model','helpdesk_m');
			$data['item'] = $this->helpdesk_m->get_all();

		if ($this->user_level=="Admin Web"); 


	
	}
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Helpdesk- Admin ";
			$data['content']	= "helpdesk/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "helpdesk";

			$this->load->model('helpdesk_model','helpdesk_m');
			$data['item'] = $this->helpdesk_m->get_all();

			//load kategori helpdesk
			$this->load->model('ref_kategori_helpdesk_model','kategori_helpdesk_m');
			$this->kategori_helpdesk_m->status = "Y";
			$data['kategori']	= $this->kategori_helpdesk_m->get_all();
			//

			$this->load->view('pemohon/index',$data);

		}
		else
		{
			redirect('pemohon/login');
		}
	}

	public function add(){
		$data = $this->input->post();
		$this->helpdesk_m->insert($data);
		redirect(base_url('helpdesk'));
	}

	public function add_respons(){
		$data = $this->input->post();
		$id = $this->uri->segment(3);
		$this->helpdesk_m->insert_respons($data);
		redirect('helpdesk/view/'.$id);
	}


	public function view()
	{
		if ($this->user_id)
		{
			
			$data['title']		= "Helpdesk ";
			$data['content']	= "helpdesk/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref_jalan";
			$id = $this->uri->segment(3);

			$this->load->model('helpdesk_model');
	        $data['item'] = $this->helpdesk_model->select_by_id($id);

	        if (empty($data['item']) or empty($id)) redirect(base_url('helpdesk'));

	        $this->load->model('helpdesk_model');
	        $data['respons'] = $this->helpdesk_model->select_by_id_respons($id);

			$this->load->view('pemohon/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function edit()
	{
		if ($this->user_id)
		{
			$id = $this->uri->segment(3);
	        if(empty($id)){
	            redirect(base_url('ref_jalan'));
	        }
	        $this->load->model('ref_jalan_model');
	        $data['item'] = $this->ref_jalan_model->select_by_id($id);
	        
	        if(empty($data['item'])){
	            redirect(base_url('ref_jalan'));
	        }

			$data['title']		= "ref jalan - Admin ";
			$data['content']	= "ref_jalan/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref_jalan";
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}

	public function update(){
		$id = $this->uri->segment(3);
		if(empty($id)){
	            redirect(base_url('ref_jalan'));
	        }
		$data = $this->input->post();
		$this->jalan_m->update($data,$id);
		redirect(base_url('ref_jalan'));
	}


	public function delete_respons($id)
	{
		if ($this->user_id)
		{
			$this->load->model('helpdesk_model');
			$id_helpdesk = $this->uri->segment(4);
			$this->helpdesk_model->id_respons = $id;
			$this->helpdesk_model->delete_respons();
			$data['message_type'] = "success";
			$data['message']	= "Record Ref jalan  berhasil dihapus.";
		
			redirect('helpdesk/view/'.$id_helpdesk);
			
		}
		else
		{
			redirect('home');
		}
	}

	public function delete($id)
	{
		if ($this->user_id)
		{
			$this->load->model('helpdesk_model');
			$id_helpdesk = $this->uri->segment(3);
			$this->helpdesk_model->id_helpdesk = $id;
			$this->helpdesk_model->delete();
			$data['message_type'] = "success";
			$data['message']	= "Record Ref jalan  berhasil dihapus.";
		
			redirect('helpdesk');
			
		}
		else
		{
			redirect('home');
		}
	}

	public function open_help($id)
	{
		if ($this->user_id)
		{
			$this->load->model('helpdesk_model');
			$id_helpdesk = $this->uri->segment(3);
			$this->helpdesk_model->id_helpdesk = $id;
			$this->helpdesk_model->open_help();

			$data['message_type'] = "success";
			$data['message']	= "Record Ref jalan  berhasil dihapus.";
		
			redirect('helpdesk/view/'.$id_helpdesk);
			
		}
		else
		{
			redirect('home');
		}
	}

	public function close_help($id)
	{
		if ($this->user_id)
		{
			$this->load->model('helpdesk_model');
			$id_helpdesk = $this->uri->segment(3);
			$this->helpdesk_model->id_helpdesk = $id;
			$this->helpdesk_model->close_help();

			$data['message_type'] = "success";
			$data['message']	= "Record Ref jalan  berhasil dihapus.";
		
			redirect('helpdesk');
			
		}
		else
		{
			redirect('home');
		}
	}

	public function sisipkan()
	{
		$id = $this->uri->segment(3);
		$config['upload_path']          = "./../global/helpdesk/{$id}/";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 5000;
        $config['max_height']           = 5000;
		
		$dest = $config['upload_path'];
		if (!file_exists($dest)) {
		    mkdir($dest, 0777, true);
		}

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('file'))
        {
            $data['respons_text'] = "<i>{$this->upload->display_errors()}</i>";
        }
        else
        {
        	$servername = filter_input(INPUT_SERVER, 'SERVER_NAME');
        	$src = "http://{$servername}/global/helpdesk/{$id}/{$this->upload->data('file_name')}";
        	$data['respons_text'] = "<a href='{$src}' target='_blank'><img src='{$src}' style='max-width:100%;'></img></a>";
        }

        $data['id_helpdesk'] = $id;


		$this->helpdesk_m->insert_respons($data);
	}
}
?>