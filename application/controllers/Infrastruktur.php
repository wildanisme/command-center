<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require_once APPPATH.'third_party/Lcobucci/JWT/Signer/Hmac/Sha256.php';
// require_once APPPATH.'third_party/Lcobucci/JWT/Builder.php';

class Infrastruktur extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');
		$this->load->model('keuangan_model');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');

		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);


		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "infrastruktur/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "infrastruktur";
			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			// $data['query'] = $this->keuangan_model->get_keuangan_view();
			$data = array();
			
			$this->load->view('admin/kesehatan/dashboard_view',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function rsud(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "kesehatan/dashboard_rsud" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "kesehatan";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard_rsud(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			// $data['query'] = $this->keuangan_model->get_keuangan_view();
			$data = array();
			
			$this->load->view('admin/kesehatan/dashboard_rsud_view',$data);
		} else {
			redirect('admin/login');
			
		}
	}



	public function check()
	{

		$url = "https://dashboard.epuskesmas.id/index.php?token_auth=MzQ5MzhhNjc1MmI2MDBjOWQyODYyZWUzZjI2OGZhNDUwY2NiMTQxZjdmYjAxZTFjODRlN2RhYWI5MzU5NWQ3";
		$header = get_headers($url, 1);
		echo "<pre>";
		print_r($header);
	}




}
