<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Parkir extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');

		$this->load->helper('text');
		$this->load->helper('typography');
        $this->load->helper('file');

		$this->load->model('parkir_model');

		

		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= "Parkir Berlangganan - ".app_name;
			$data['content']	= "parkir/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "parkir";
			$data['page'] = "parkir";
			$data['tahun'] = date('Y');

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			$parkir = $this->parkir_model->get_all();
			$parkir_hari_ini = $this->parkir_model->get_all(date('Y-m-d'));

			$j_parkir = 0;
			foreach($parkir as $p){
				$j_parkir += $p->total_bayar;
			}

			$j_parkir_hari_ini = 0;
			foreach($parkir_hari_ini as $p){
				$j_parkir_hari_ini += $p->total_bayar;
			}
			$data['total_transaksi'] = count($parkir);
			$data['total_retribusi'] = $j_parkir;
			$data['retribusi_hari_ini'] = $j_parkir_hari_ini;

			$data['laporan_parkir'] = $parkir_hari_ini;


			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}




}
