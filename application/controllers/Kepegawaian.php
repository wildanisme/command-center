<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Kepegawaian extends CI_Controller
{
	public $user_id;
	
	function __construct()
	{
		parent ::__construct();

		$this->user_id 		= '';
		$this->user_picture = '';
		$this->full_name	= '';
		$this->user_level	= '';
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('session');
		//$this->load->model('kepegawaian_model');
		//$this->load->model('keuangan_model');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');
	}

	public function index(){
		$data['title']		= 'Kepegawaian';
		$data['content']	= 'kepegawaian/dashboard';
		//$data['user_picture'] = $this->user_picture;
		//$data['full_name']		= $this->full_name;
		//$data['user_level']		= $this->user_level;
		$data['active_menu'] = "kepegawaian";

		$this->load->view('admin/index',$data);
	}

	// function create_view()
	// {
	// 	$return = $this->kepegawaian_model->create_view();
	// 	if ($return) {
	// 		redirect('kepegawaian');
	// 	}
	// }

	public function dashboard(){
		// $data['query'] = $this->kepegawaian_model->get_pegawai_view();

		// if (!empty($data['query'])) {
			$this->load->view('admin/kepegawaian/dashboard_view');
		// } else {
		// 	if (!$this->db->table_exists('pegawai_view'));
		// 	{
		// 		$this->create_view();
		// 	}
		// 	$this->load->view('admin/404',$data);
		// }
	} 

// 	public function mutasi(){
// 		$data['title']		= 'dashboard';
// 		$data['content']	= "kepegawaian/mutasi" ;
// 		$data['user_picture'] = $this->user_picture;
// 		$data['full_name']		= $this->full_name;
// 		$data['user_level']		= $this->user_level;
// 		$data['active_menu'] = "mutasi";


// 		$data['query'] = $this->kepegawaian_model->get_all_mutasi();
// 		$this->load->view('admin/index',$data);
// 	}

// 	public function jabatan(){
// 		$data['title']		= 'dashboard';
// 		$data['content']	= "kepegawaian/jabatan" ;
// 		$data['user_picture'] = $this->user_picture;
// 		$data['full_name']		= $this->full_name;
// 		$data['user_level']		= $this->user_level;
// 		$data['active_menu'] = "jabatan";


// 		$data['query'] = $this->kepegawaian_model->get_all_jabatan();
// 		$this->load->view('admin/index',$data);
// 	}

// public function addmutasi(){
// 		$data['title'] = 'Tambah mutasi';
//         if (isset($_POST['submit'])) {
//             $this->mutasi->input();
//             $this->session->set_flashdata('message', '<div class="alert alert-success dark alert-dismissible fade show" role="alert"><i data-feather="clock"></i><p>Data Berhasil Diinput !.</p>
//             <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
//             redirect('mutasi');
//         }
//         $this->template->load('admin', 'mutasi/add', $data);
// 	}
// public function editmutasi()
//     {
//         $data['title'] = 'Edit mutasi';
//         if (isset($_POST['submit'])) {
//             $this->mutasi->edit();
//             $this->session->set_flashdata('message', '<div class="alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="clock"></i><p>Data Berhasil Diupdate !.</p>
//             <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
//             redirect('mutasi');
//         } else {
//             $id = $this->uri->segment(3);
//             $data['x'] = $this->m_tipe_dokumen->get_one($id)->row_array();
//             $this->template->load('admin', 'mutasi/edit', $data);
//         }
//     }

//     public function deletemutasi()
//     {
//         $id = $this->uri->segment(3);
//         $this->mutasi->delete($id);
//         $this->session->set_flashdata('message', '<div class="alert alert-danger dark alert-dismissible fade show" role="alert"><i data-feather="clock"></i><p>Data Berhasil Dihapus !.</p>
//             <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
//         redirect('mutasi');
//     }

//     public function add(){
// 		$data['title'] = 'Tambah Pegawai';
//         if (isset($_POST['submit'])) {
//         	$html_escape = html_escape($_POST);
// 			$postdata = array();
// 			foreach($html_escape as $key=>$value)
// 			{
// 				$postdata[$key] = $value;
// 			}
//             $dt = array(
// 				'id_pegawai'	=> $postdata['id_pegawai'],
// 				'nip'	=> $postdata['nip'],
// 				'nama'	=> $postdata['nama'],
// 				'id_gol'	=> $postdata['id_gol'],
// 				'id_jab'		=> $postdata['id_jab'],
// 				'jen_kel'		=> $postdata['jen_kel'],

// 			);
// 			$this->db->insert("Pegawai",$dt);
// 			$this->db->insert_id();//KeurNaonieu


// 			/*$uj = array('id_jab') => $postdata['id_jab']);
// 			$this->db->where('id_pegawai', $postdata['id_pegawai']);
// 			$this->db->update("pegawai_jabatan",$uj);

// 			$up = array('id_jab' => $postdata['id_jab']);
// 			$this->db->where('id_pegawai', $postdata['id_pegawai']);
// 			$this->db->update("pegawai_view",$up);*/
					
//         }
//         $data['content'] = "kepegawaian/add" ;
//         $data['jabatan'] = $this->kepegawaian_model->get_pegawai_view();
//         $this->load->view('admin/index',$data);
// 	}
// 	public function edit($id)
//     {
//         $data['title'] = 'Edit mutasi';
//         if (isset($_POST['submit'])) {
//             $this->mutasi_model->edit();
//             $this->session->set_flashdata('message', '<div class="alert alert-warning dark alert-dismissible fade show" role="alert"><i data-feather="clock"></i><p>Data Berhasil Diupdate !.</p>
//             <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
//             redirect('mutasi');
//         }

//         $id = $this->uri->segment(3);
//         $data['jabatan'] = $this->kepegawaian_model->get_all_jabatan();
//          $data['content'] = "kepegawaian/mutasi/edit" ;
//         $data['x'] = $this->mutasi_model->get_one_mutasi($id)->row_array();
//          $this->load->view('admin/index',$data);
//     }

//     public function delete()
//     {
//         $id = $this->uri->segment(3);
//         $this->mutasi_model->del_mutasi($id);
//         $this->session->set_flashdata('message', '<div class="alert alert-danger dark alert-dismissible fade show" role="alert"><i data-feather="clock"></i><p>Data Berhasil Dihapus !.</p>
//             <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div>');
//         redirect('mutasi');
//     }


}
