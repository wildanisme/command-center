<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Menu extends CI_Controller {
    public $user_id;
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model(array('Mod_menu','user_model','Mod_userlevel'));
        $this->user_model->user_id = $this->user_id;
        $this->user_model->set_user_by_user_id();
        $this->user_picture = $this->user_model->user_picture;
        $this->full_name    = $this->user_model->full_name;
        $this->user_level   = $this->user_model->level;
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->library('session');
        $this->load->model('tank_auth/users');

        $this->load->helper('text');
        $this->load->helper('typography');
        $this->load->helper('file');

        $this->bulan = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'Nopember',
            12 => 'Desember',
        );


        if ($this->tank_auth->is_logged_in()) {
            redirect ('welcome'); 
        }
    }

    public function index()
    {
        if ($this->user_id)
        {
            $data['title']      = app_name;
            $data['content']    = "mainmenu/dashboard" ;
            $data['user_picture'] = $this->user_picture;
            $data['full_name']      = $this->full_name;

            $data['user_level']     = $this->user_level;
            
            $data['active_menu'] = "menu";


            $this->load->view('admin/index',$data);

        }
        else
        {
         redirect('admin/login');
     }
 }

 public function dashboard()
 {
    $data["menu"] = $this->Mod_menu->getAll()->result();

    $this->load->view('admin/mainmenu/dashboard_view',$data);
}

public function add()
{
    $validation = $this->form_validation; 
    $validation->set_rules($this->rules()); 
    if ($validation->run()) {
        $data['title']      = app_name;
        $data['content']    = "mainmenu/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "menu";
        $this->insert();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Menu berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect("menu");
    } else {
        $data['title']      = 'Tambah Menu ' . app_name;
        $data['content']    = "mainmenu/add" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "menu";
        $this->load->view('admin/index',$data);
    }
    // $this->load->view('admin/mainmenu/add');
}

public function edit($id)
{
    $data['menu'] = $this->Mod_menu->get_menu($id);
    $validation = $this->form_validation; 
    $validation->set_rules($this->rules()); 
    if ($validation->run()) {
        $data['title']      = app_name;
        $data['content']    = "mainmenu/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "menu";
        $this->update();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Menu berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect("menu");
    } else {
        $data['title']      = 'Tambah Menu ' . app_name;
        $data['content']    = "mainmenu/edit" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "menu";
        $this->load->view('admin/index',$data);
    }
    // $this->load->view('admin/mainmenu/edit');
}

public function insert()
{
    $save  = array(
        'nama_menu' => ucwords($this->input->post('nama_menu')),
        'link'      => $this->input->post('link'),
        'icon'      => $this->input->post('icon'),
        'urutan'    => $this->input->post('urutan'),
        'is_active' => $this->input->post('is_active')
    );
    $this->Mod_menu->insertMenu("tbl_menu", $save);

    $id_menu =$this->db->insert_id();
    $levels = $this->Mod_userlevel->getAll()->result();
    foreach ($levels as $row) {
        $data = array(
            'id_menu'   => $id_menu,
            'id_level'  => $row->level_id,
        );
                //insert ke akses menu
        $this->Mod_menu->insertaksesmenu("tbl_akses_menu", $data);
    }

}

public function update()
{
    $id_menu      = $this->input->post('id_menu');
    $save  = array(
        'nama_menu' => ucwords($this->input->post('nama_menu')),
        'link'      => $this->input->post('link'),
        'icon'      => $this->input->post('icon'),
        'urutan'    => $this->input->post('urutan'),
        'is_active' => $this->input->post('is_active')
    );
    $this->Mod_menu->updateMenu($id_menu, $save);
    echo json_encode(array("status" => TRUE));
}
public function delete()
{
    $id_menu = $this->input->post('id_menu');
    $this->Mod_menu->deleteMenu($id_menu, 'tbl_menu');
    
    echo json_encode(array("status" => TRUE));
}

public function rules()
    {
        return [
            [
                'field' => 'nama_menu',
                'label' => 'Nama Menu',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'urutan',
                'label' => 'Urutan',
                'rules' => 'trim|required|numeric'
            ],
        ];
    }

}