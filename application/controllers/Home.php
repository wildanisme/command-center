<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();

		$this->bulan = array(
							1 => 'Januari',
							2 => 'Februari',
							3 => 'Maret',
							4 => 'April',
							5 => 'Mei',
							6 => 'Juni',
							7 => 'Juli',
							8 => 'Agustus',
							9 => 'September',
							10 => 'Oktober',
							11 => 'Nopember',
							12 => 'Desember',
						);

	}

	public function index()
	{
		$data['active_menu'] = "home";
		$data['title'] = $this->company_profile_model->nama;

		

		//header
		$this->load->model('img_header_model');
		$data['header'] = $this->img_header_model->get_all();
		//var_dump($data);die;

		//sambutan
		$this->load->model('company_profile_model');
		$data['sambutan'] = $this->company_profile_model->get_all_sambutan();

		//download
		$this->load->model('download_model');
		$data['download'] = $this->download_model->get_some_download();


		//download
		$this->load->model('banner_model');
		$data['banner'] = $this->banner_model->get_all();


		//sambutan
		$this->load->model('company_profile_model');
		$data['sambutan'] = $this->company_profile_model->get_all_sambutan();


		
		//video
		$this->load->model('video_model');
		$data['video'] = $this->video_model->get_limit($limit=1);


		

		//berita
		$this->load->model('post_model');
		if (!empty($_GET['s'])) {
			$this->post_model->search = $_GET['s'];
			$data['search'] = $_GET['s'];
		}




		//grafik
		$this->load->model('dashboard_model');

		if ($this->input->get('tahun')) {
			$tahun = $this->input->get('tahun');
		}
		$tahun = (empty($tahun)) ? date("Y") : $tahun;
		
		$data['tahun'] = $tahun;
		$data['bulan'] = $this->bulan;


		



		$this->post_model->external = 'true';
		$this->post_model->post_status = "Publish";
		$data['per_page']	= 3;
		$data['total_rows']	= $this->post_model->get_total_row();
		$offset = 0;
		if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
		$data['posts']		= $this->post_model->get_for_page($data['per_page'],$offset);
		$this->post_model->channel_id = "2";
		$data['posts2']		= $this->post_model->get_for_page($data['per_page'],$offset);
		$this->post_model->channel_id = "3";
		$data['posts3']		= $this->post_model->get_for_page($data['per_page'],$offset);
		$this->load->model('tag_model');
		$data['Qtag']	= $this->tag_model->get_all();
		$this->load->model('category_model');
		$this->category_model->category_status = "Active";
		$data['categories']	= $this->category_model->get_all();
		$data['tags_']	= $this->tag_model->get_all();
		$data['popular']	= $this->post_model->get_popular();


		



		
		$this->load->model('listing_model');
		$data['hitung_by_kategori'] = $this->listing_model->hitung_by_kategori();



		$this->load->model('listing_model');
		$data['rekomendasi'] = $this->listing_model->get_rekomendasi();

		$this->load->model('listing_model');
		$data['umkm'] = $this->listing_model->get_umkm();

		$data['total_ukm'] = $this->listing_model->get_total_umkm();

		$this->load->model("user_model");
		$this->user_model->user_level_ = 2;
		$data['total_masyarakat'] = $this->user_model->get_total_row();


		require_once(APPPATH."libraries/WebserviceMPP.php");
		$MPP = new WebserviceMPP();
		$api_key = $this->config->item("app_key_mpp");
		$param = array(
			'offset'	=> 0,
			'limit'		=> 10,
			'cari'		=>  '',
			'tags'		=> '',
		);

		// var_dump($param);die;
		$result = $MPP->get_layanan($api_key,$param);
		
		$layanan = json_decode($result);
		$total = 0;
		if(!$layanan->error){
			$total = $layanan->total_rows;
		}
		
		$data['total_layanan'] = $total;
		

		//view
		
		$this->load->view('blog/home',$data);
	}

	
	
	
}
?>