<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publikasi extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		$data['title'] = "Publikasi - " .$this->company_profile_model->nama;
		$data['active_menu'] ="publikasi";
		$this->load->model('public_model');
		$data['public'] = $this->public_model->get_all();

		$this->load->model('Category_model_public');
		$data['kategori'] = $this->Category_model_public->get_all();

		$this->load->view('blog/publikasi',$data);
	}

	public function detail($id_publikasi)
	{

		$data['active_menu'] = "public_model";
		$this->load->model('public_model');
		$this->public_model->id_publikasi = $id_publikasi;
		$this->public_model->set_by_id();
		$data['category_id']	= $this->public_model->category_id;
		$data['judul']	= $this->public_model->judul;
		$data['nama_file']	= $this->public_model->nama_file;
		$data['tgl_posting']	= $this->public_model->tgl_posting;
		$data['type']	= $this->public_model->type;
		$data['url']	= $this->public_model->url;
		$title = 'Detail Publikasi';
		$this->load->view('blog/detail_publikasi',$data,$title);
	}
	
}
?>