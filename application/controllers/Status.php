<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		redirect('home');
	}

	public function antrian($hash)
	{
		$data['title'] = "Status Layanan - " .$this->company_profile_model->nama;
		$data['active_menu'] = "status";
		//$this->load->model('potensi_model');
		//$data['query']		= $this->potensi_model->get_all_join_category();
		$hash = $this->uri->segment(3);
		$this->load->model('ref_layanan_model');
		$data['status'] = $this->ref_layanan_model->status_antrian($hash);
		$data['sisa_antrian'] = $this->ref_layanan_model->getSisaAntrian($data['status']->loket,$data['status']->id_antrian);
		
		//var_dump($data);die;
		
		$this->load->view('blog/status',$data);
	}


}
?>