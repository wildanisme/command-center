<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->load->helper('text');
		$this->load->helper('typography');
        $this->load->helper('file');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		$data['title'] = "E-Modul - " .$this->company_profile_model->nama;
		$data['active_menu'] ="download";
		$this->load->model('download_model');
		if (!empty($_GET['s'])) {
			$this->download_model->search = $_GET['s'];
			$data['search'] = $_GET['s'];
		}
		if (!empty($_GET['c'])) {
			$this->download_model->search_c = $_GET['c'];
			$data['search_c'] = $_GET['c'];
		}
		$data['per_page']	= 6;
		$data['total_rows']	= $this->download_model->get_total_row();
		$offset = 0;
		if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
		$data['download'] = $this->download_model->get_for_page($data['per_page'],$offset);
		//banner
		$this->load->model('banner_model');
		$data['banner'] = $this->banner_model->get_all();

		$this->load->model('category_download_model');
		$this->category_download_model->category_status = "Active";
		$data['categories']	= $this->category_download_model->get_all();
		
		$this->load->view('blog/src/header',$data);
		$this->load->view('blog/src/top_nav',$data);
		$this->load->view('blog/modul',$data);
		$this->load->view('blog/src/footer',$data);
	}

	public function detail($id)
	{
		if ($id) {
			$this->load->model('download_model');
			if (!empty($_GET['s'])) {
				$this->download_model->search = $_GET['s'];
				$data['search'] = $_GET['s'];
			}
			if (!empty($_GET['c'])) {
				$this->download_model->search_c = $_GET['c'];
				$data['search_c'] = $_GET['c'];
			}
			$this->download_model->id_download = $id;
			$data['download'] = $this->download_model->get_by_id();

			$data['title'] = "E-Modul - " .$data['download'][0]->judul;
			$data['active_menu'] ="download";
			//banner
			$this->load->model('banner_model');
			$data['banner'] = $this->banner_model->get_all();

			$this->load->model('category_download_model');
			$this->category_download_model->category_status = "Active";
			$data['categories']	= $this->category_download_model->get_all();
			
			$this->load->view('blog/src/header',$data);
			$this->load->view('blog/src/top_nav',$data);
			$this->load->view('blog/modul_detail',$data);
			$this->load->view('blog/src/footer',$data);
		} else {
			redirect(base_url()."modul?msg=404");
		}
	}

	public function download($id)
	{
		if ($id) {
			$this->load->model('download_model');
			$this->download_model->id_download = $id;
			$download = $this->download_model->get_by_id();

			if ($download) {
				$this->download_model->hits();
				redirect($download[0]->link);
			} else {
				redirect(base_url()."modul?msg=404");
			}
		} else {
			redirect(base_url()."modul?msg=404");
		}
	}
	
}
?>