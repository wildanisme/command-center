<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_kategoriproduk extends CI_Controller {
	public $user_id;
	public function __construct()
	{
			parent::__construct();
			$this->user_id = $this->session->userdata('user_id');
			$this->load->model('user_model');
			$this->user_model->user_id = $this->user_id;
			$this->user_model->set_user_by_user_id();
			$this->user_picture = $this->user_model->user_picture;
			$this->full_name    = $this->user_model->full_name;
			$this->user_level   = $this->user_model->level;
			$this->load->model('ref_kategori_produk_model');
			if (!$this->user_id)
			{
				redirect("admin/login");
			}

			if($this->user_level!="Administrator")
			{
				show_404();
			}
	}

	public function index()
	{
			$data['title']     = "Ref Kategori Produk ";
					$data['content']    = "ref_kategori_produk/index" ;
					$data['user_picture'] = $this->user_picture;
					$data['full_name']      = $this->full_name;
					$data['user_level']     = $this->user_level;
					$data['active_menu'] = "ref";

			if (!empty($_POST)) {
				//echo "<pre>";print_r($_POST);die;
				$dt = array(
					'nama_kategori_produk'  => $this->input->post("nama_kategori_produk"),
					'id_induk'  => $this->input->post("id_induk"),
					'status'  => $this->input->post("status"),
				);
				$id_kategori_produk = $this->input->post("id_kategori_produk");
				if($id_kategori_produk=="")
				{
					$this->db->insert("ref_kategori_produk",$dt);
				}
				else{
					$this->db->where("id_kategori_produk",$id_kategori_produk)
					->update("ref_kategori_produk",$dt);
				}
				$this->session->set_flashdata("success","Kategori produk berhasil disimpan");
				redirect("ref_kategoriproduk");
			}

			$data['list'] = $this->ref_kategori_produk_model->get()->result();

			$param_induk['where']['ref_kategori_produk.id_induk'] = null;
			$data['dt_induk'] = $this->ref_kategori_produk_model->get($param_induk)->result();

			$this->load->view('admin/index', $data);
	}

	public function delete()
	{
			if($this->input->is_ajax_request() )
			{
					$dt=$this->db->where("id_kategori_produk",$this->input->post("id"))->get("ref_kategori_produk")->row();

					$data['status'] = FALSE;

					if($this->input->post("id") && $dt){
							$this->db->delete("ref_kategori_produk",['id_kategori_produk' => $this->input->post("id")]);

							if( $this->db->affected_rows() == 1 ){
									$data['status'] = true ;
							}
					}

					echo json_encode($data);
			}
	}

}
?>
