<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata("user_id");
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function send_email()
	{
		
		$to = "dieansh@gmail.com";
		$subject = "Reset Password";
		
		$template = file_get_contents("./data/template_email/reset_password.html");

		$APP_NAME = "WISATA SUMEDANG";
		$URL = "http://202.93.229.205/wisata/reset-password?user=jj&token=888";
		$NAME = "Arif";
		
		$template = str_replace("{APP_NAME}",$APP_NAME,$template);
		$template = str_replace("{URL}",$URL,$template);
		$template = str_replace("{NAME}",$NAME,$template);

		$config = $this->config->item("email");
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config["smtp_user"]);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($template);

		if($this->email->send())
		{
			echo 'Email sent.';
		}
		else
		{
			show_error($this->email->print_debugger());
		}
		
		

		
	}
	
	public function register()
	{
		if(!empty($_POST))
		{
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules("username","NIK",
				"trim|required|numeric|is_unique[user.username]|exact_length[16]",
				array(
					"required" 		=> "%s harus diisi",
					'numeric'		=> '%s harus berupa angka',
					'is_unique'		=> '%s sudah digunakan',
					'exact_length'	=> '%s harus 16 karakter',
				)
			);
			
			$this->form_validation->set_rules("full_name","Nama",
				"trim|required|alpha_numeric_spaces|min_length[5]",
				array(
					"required" 				=> "%s harus diisi",
					'alpha_numeric_spaces'	=> '%s harus berupa huruf, angka, spasi',
					'min_length'			=> '%s minimal 5 karakter'
				)
			);
			
			$this->form_validation->set_rules("email","Alamat Email",
				"trim|required|valid_email|is_unique[user.email]",
				array(
					"required" 		=> "%s harus diisi",
					'valid_email'		=> '%s tidak valid',
					'is_unique'		=> '%s sudah digunakan'
				)
			);
			
			$this->form_validation->set_rules("password","Password",
				"trim|required|alpha_numeric|min_length[8]",
				array(
					"required" 		=> "%s harus diisi",
					'alpha_numeric'	=> '%s harus berupa huruf, angka',
					'min_length'		=> '%s minimal 8 karakter'
				)
			);
			
			$this->form_validation->set_rules("password_confirmation","Password",
				"trim|required|alpha_numeric|min_length[8]|matches[password]",
				array(
					"required" 		=> "%s harus diisi",
					'alpha_numeric'	=> '%s harus berupa huruf, angka',
					'min_length'		=> '%s minimal 8 karakter',
					'matches'			=> '%s tidak sesuai'
				)
			);
			
			if($this->form_validation->run() ==true) {
				
				$this->load->model("user_model");
				
				$html_escape = html_escape($_POST);
				$data = array();
				foreach($html_escape as $key=>$value)
				{
					$data[$key] = $this->security->xss_clean($value);
				}
				
				$this->user_model->username 	= $data['username'];
				$this->user_model->full_name 	= $data['full_name'];
				$this->user_model->email 		= $data['email'];
				$this->user_model->password 	= $data['password'];
				$this->user_model->user_status 	= "Active";
				$this->user_model->user_level	= 2; // User
				
				
				$this->user_model->insert();
				
				$this->user_model->set_user_by_username();
				$this->user_model->login_sukses();
				
				$response = [
					'error'	=> false,
					//'data'	=> $data,
				];
			}
			else{
				$error_message	 = $this->form_validation->error_array();
				
				$response = [
					'error'			=> true,
					'error_message'	=> $error_message,
					'csrf_token'	=> $this->security->get_csrf_hash(),
				];
			}
			
			
			
			echo json_encode($response);
		}
		else{
			$data['title']		= "404 Halaman tidak ditemukan";
			$this->load->view("blog/404",$data);
		}
	}
	
	public function login()
	{
		if(!empty($_POST))
		{
			$this->load->library('form_validation');
			
			
			
			$this->form_validation->set_rules("username","NIK",
				"trim|required|numeric|exact_length[16]",
				array(
					"required" 		=> "%s harus diisi",
					'numeric'		=> '%s harus berupa angka',
					'exact_length'	=> '%s harus 16 karakter',
				)
			);
			
			$this->form_validation->set_rules("password","Password",
				"trim|required|alpha_numeric",
				array(
					"required" 		=> "%s harus diisi",
					'alpha_numeric'	=> '%s harus berupa huruf, angka',
				)
			);
			
			
			
			if($this->form_validation->run() ==true) {
				
				$this->load->model("user_model");
				
				$html_escape = html_escape($_POST);
				$data = array();
				foreach($html_escape as $key=>$value)
				{
					$data[$key] = $this->security->xss_clean($value);
				}
				
				
				$this->user_model->username		= $data['username'];
				$this->user_model->password 	= $data['password'];
				
				
					$valid = $this->user_model->validasi_login();
					if ($valid)
					{
						$login = $this->user_model->cek_status_user();
						if ($login)
						{
							 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has login";
		                	$this->logs_model->category = "login";
		                	$desc = $this->user_model->username;
		                	$this->logs_model->description = "with username ".$desc;
							$this->logs_model->insert();
							
							

							$response = [
								'error'	=> false,
								//'data'	=> $data,
							];
							

						}
						else
						{
							$error_message['notification'] = "Akun anda tidak aktif.";
							$response = [
								'error'			=> true,
								'error_message'	=> $error_message,
								'csrf_token'	=> $this->security->get_csrf_hash(),
							];
							
						}
					}
					else
					{
						$error_message['notification'] = "NIK atau password tidak tepat";
						$response = [
							'error'			=> true,
							'error_message'	=> $error_message,
							'csrf_token'	=> $this->security->get_csrf_hash(),
						];

					}
			}
			else{
				$error_message	 = $this->form_validation->error_array();
				
				$response = [
					'error'			=> true,
					'error_message'	=> $error_message,
					'csrf_token'	=> $this->security->get_csrf_hash(),
				];
			}
			
			
			
			echo json_encode($response);
		}
		else{
			$data['title']		= "404 Halaman tidak ditemukan";
			$this->load->view("blog/404",$data);
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/cityzen/access');
	}
	
	public function forget_password()
	{
		$this->load->library('form_validation');
		$data['title']		= "Lupa Password";
		if(!empty($_POST))
		{
			$this->form_validation->set_rules("email","Alamat Email",
				"trim|required|valid_email",
				array(
					"required" 		=> "%s harus diisi",
					'valid_email'		=> '%s tidak valid',
				)
			);	

			if($this->form_validation->run() ==true) {
					
				$this->load->model("user_model");
				
				$html_escape = html_escape($_POST);
				$clean_data = array();
				foreach($html_escape as $key=>$value)
				{
					$clean_data[$key] = $this->security->xss_clean($value);
				}
				
				$param = array(
					'user.email'					=> $clean_data["email"],
				);
				$user = $this->user_model->get($param);

				if($user)
				{
					$forget_password_token = md5(uniqid(rand(), true));

					$this->user_model->user_id = $user[0]->user_id;
					$this->user_model->forget_password_token = $forget_password_token;
					$this->user_model->update();

					$URL = base_url()."reset-password?user=".$user[0]->user_hash."&token=".$forget_password_token;

					$to = $clean_data["email"];
					$subject = "Reset Password";
					
					$template = file_get_contents("./data/template_email/reset_password.html");

					$APP_NAME = $this->company_profile_model->nama;
					
					$NAME = $user[0]->full_name;
					
					$template = str_replace("{APP_NAME}",$APP_NAME,$template);
					$template = str_replace("{URL}",$URL,$template);
					$template = str_replace("{NAME}",$NAME,$template);
					

					$config = $this->config->item("email");
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from($config["smtp_user"]);
					$this->email->to($to);
					$this->email->subject($subject);
					$this->email->message($template);

					if($this->email->send())
					{
						$data["message"] = "Email terkirim. Silahkan periksa email anda.";
						$data["type"] 	= "success";
					}
					else
					{
						//show_error($this->email->print_debugger());
						$data["message"] = "Email gagal terkirim. Silahkan hubungi administrator.";
						$data["type"] 	= "error";
					}
					

					
				}
				else{
					$data["message"] = "Alamat email belum terdaftar.";
					$data["type"] 	= "error";
				}
				
				
			}
		}
		$this->load->view("blog/forget_password",$data);
	}
	public function reset_password()
	{
		$get_escape = html_escape($_GET);
		$get_data = array();
		foreach($get_escape as $key=>$value)
		{
			$get_data[$key] = $this->security->xss_clean($value);
		}
		if(!empty($get_data)){
			$this->load->model("user_model");
			$param = array(
				'user.user_hash'				=> $get_data["user"],
				'user.forget_password_token'	=> $get_data["token"],
			);
			$user = $this->user_model->get($param);
		}
		if(!empty($get_data) && !empty($user) && !$this->user_id){
			
		
			$data['title']		= "Reset Password";
			$this->load->library('form_validation');
			if(!empty($_POST))
			{
				
				
				$this->form_validation->set_rules("password","Password",
					"trim|required|alpha_numeric|min_length[8]",
					array(
						"required" 		=> "%s harus diisi",
						'alpha_numeric'	=> '%s harus berupa huruf, angka',
						'min_length'		=> '%s minimal 8 karakter'
					)
				);
				
				$this->form_validation->set_rules("password_confirmation","Password",
					"trim|required|alpha_numeric|min_length[8]|matches[password]",
					array(
						"required" 		=> "%s harus diisi",
						'alpha_numeric'	=> '%s harus berupa huruf, angka',
						'min_length'		=> '%s minimal 8 karakter',
						'matches'			=> '%s tidak sesuai'
					)
				);
				
				if($this->form_validation->run() ==true) {
					
					$this->load->model("user_model");
					
					$html_escape = html_escape($_POST);
					$clean_data = array();
					foreach($html_escape as $key=>$value)
					{
						$clean_data[$key] = $this->security->xss_clean($value);
					}
					
					$this->user_model->change_password($user[0]->user_id,$clean_data["password"]);

					$data["message"] = "Reset password  berhasil. Password anda telah diubah";
					$data["type"] 	= "success";
					
				}
				
			}
			$this->load->view("blog/reset_password",$data);
		}
		else{
			$data['title']		= "404 Halaman tidak ditemukan";
			$this->load->view("blog/404",$data);
		}
	}

	public function verification()
	{
		$get_escape = html_escape($_GET);
		$get_data = array();
		foreach($get_escape as $key=>$value)
		{
			$get_data[$key] = $this->security->xss_clean($value);
		}
		if(!empty($get_data)){
			$this->load->model("user_model");
			$param = array(
				'user.user_hash'				=> $get_data["user"],
				'user.email_verification_token'	=> $get_data["token"],
			);
			$user = $this->user_model->get($param);
			
		}
		if(!empty($get_data) && !empty($user)){
			$this->user_model->user_id = $user[0]->user_id;
			$this->user_model->email_verification_status = "Y";
			$this->user_model->update();

			//$this->session->sess_destroy();

			$this->user_model->username = $user[0]->username;
			$this->user_model->set_user_by_username();
			$this->user_model->login_sukses();
			//var_dump($this->user_model->full_name);
			redirect("cityzen");
		}
		else{
			$data['title']		= "404 Halaman tidak ditemukan";
			$this->load->view("blog/404",$data);
		}
	}
	
	public function send_email_verification()
	{
		$app_key = !empty($_GET['token']) ? $_GET['token'] : null;
		//var_dump(base_url());
		
		if($app_key!=null && $app_key== $this->config->item('authorization'))
		{
			$this->load->model("user_model");
			$param = array("user.email_verification"=>"N");
			$user = $this->user_model->get($param);

			$success = 0;
			$failed = 0 ;
			//var_dump($user);
			foreach($user as $row)
			{
				$this->user_model->user_id = $row->user_id;
				$this->user_model->email_verification = "Y";
				$this->user_model->update();

				if($row->email!=""){
					$URL = base_url()."verification?user=".$row->user_hash."&token=".$row->email_verification_token;

					$to = $row->email;
					$subject = "Verifikasi Akun";
					
					$template = file_get_contents("./data/template_email/verifikasi_akun.html");

					$APP_NAME = $this->company_profile_model->nama;
					
					$NAME = $row->full_name;
					
					$template = str_replace("{APP_NAME}",$APP_NAME,$template);
					$template = str_replace("{URL}",$URL,$template);
					$template = str_replace("{NAME}",$NAME,$template);

					$config = $this->config->item("email");
					$this->load->library('email', $config);
					$this->email->set_newline("\r\n");
					$this->email->from($config["smtp_user"]);
					$this->email->to($to);
					$this->email->subject($subject);
					$this->email->message($template);

					if($this->email->send())
					{
						$success++;
					}
					else
					{
						$failed++;
					}
				}
			}
			$response = [
    			'error'	=> false,
    			'data' => $success." email terkirim. ".$failed." email gagal dikirim.",
    		];
		}
		else{
			$response = [
    			'error'	=> true,
    			'message' => 'Invalid data',
    		];
		}
		
		echo json_encode($response);
	}
}