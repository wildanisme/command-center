<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Ref_jenisusaha extends CI_Controller
{
    public $user_id;
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('user_model');
        $this->user_model->user_id = $this->user_id;
        $this->user_model->set_user_by_user_id();
        $this->user_picture = $this->user_model->user_picture;
        $this->full_name    = $this->user_model->full_name;
        $this->user_level   = $this->user_model->level;
        $this->load->model('ref_jenisusaha_model');

        if (!$this->user_id)
  			{
  				redirect("admin/login");
  			}

        if($this->user_level!="Administrator")
        {
          show_404();
        }

    }

    public function index()
    {
        $data['title']     = "Ref Jenis Usaha ";
            $data['content']    = "ref_jenis_usaha/index" ;
            $data['user_picture'] = $this->user_picture;
            $data['full_name']      = $this->full_name;
            $data['user_level']     = $this->user_level;
            $data['active_menu'] = "ref";

        if (!empty($_POST)) {
          $dt = array(
            'nama_jenis'  => $this->input->post("nama_jenis"),
            'status'  => $this->input->post("status"),
          );
          $id_jenis_usaha = $this->input->post("id_jenis_usaha");
          if($id_jenis_usaha=="")
          {
            $this->db->insert("ref_jenis_usaha",$dt);
          }
          else{
            $this->db->where("id_jenis_usaha",$id_jenis_usaha)
            ->update("ref_jenis_usaha",$dt);
          }
          $this->session->set_flashdata("success","Jenis usaha berhasil disimpan");
          redirect("ref_jenisusaha");
        }

        $data['list'] = $this->ref_jenisusaha_model->get_all();

        $this->load->view('admin/index', $data);
    }

    public function delete()
    {
        if($this->input->is_ajax_request() )
        {
            $dt=$this->db->where("id_jenis_usaha",$this->input->post("id"))->get("ref_jenis_usaha")->row();

            $data['status'] = FALSE;

            if($this->input->post("id") && $dt){
                $this->db->delete("ref_jenis_usaha",['id_jenis_usaha' => $this->input->post("id")]);

                if( $this->db->affected_rows() == 1 ){
                    $data['status'] = true ;
                }
            }

            echo json_encode($data);
        }
    }


}
