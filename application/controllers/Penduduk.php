<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk extends CI_Controller {

	public function __construct() {
        parent::__construct();

		$this->load->model("Penduduk_Model");
    }

	public function index() {
		$data = [];
		$data['title']		= app_name;
			$data['content']	= "dashboard" ;
			$data['active_menu'] = "dashboard";
			$data['page'] = "dashboard";

		$data['pjk_drh'] = $this->Penduduk_Model->getPjkDrh();
		$data['ret_drh'] = $this->Penduduk_Model->getRetDrh();
		$data['kd'] = $this->Penduduk_Model->getKd();
		$data['llpads'] = $this->Penduduk_Model->getLlpads();
		$data['padt'] = $this->Penduduk_Model->getPadt();
		$data['td'] = $this->Penduduk_Model->getTd();
		
		$this->load->view("admin/penduduk/index", $data);
	}
	
}
