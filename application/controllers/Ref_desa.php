<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_desa extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('alamat_model');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->user_privileges	= $this->user_model->user_privileges;
		$array_privileges = explode(';', $this->user_privileges);


		if (!in_array($this->session->userdata("user_level"),[1,3,4])) redirect("cityzen");
	}
	public function index()
	{
		// khusus admin dan operator kecamatan
		if ($this->user_id && in_array($this->session->userdata("user_level"),[1,4]))
		{
			$data['title']		= "Daftar desa - ". app_name;
			$data['content']	= "ref_desa/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			
			$data['query']		= $this->alamat_model->get_desa();
			$data['active_menu'] = "ref_desa";
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	
	public function profil($id_desa=null)
	{
		if ($this->user_id)
		{
			
			$data['title']		= "Profil desa - ". app_name;
			$data['content']	= "ref_desa/profil" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;


			
			
			
			$data['active_menu'] = "ref_desa";
			
			$dt_desa = $this->alamat_model->get_desa_by_id($id_desa);
			if(!$dt_desa || $id_desa==null)
				show_404();


			if($this->session->userdata("user_level")==3 && $dt_desa->id_desa != $this->session->userdata("id_desa"))
			{
				show_404();
			}
			

			if (!empty($_POST))
			{
				$profil_desa = $this->alamat_model->get_profil_desa($id_desa);


				

				$dt = array(
					'id_kecamatan'		=> $dt_desa->id_kecamatan,
					'id_desa'			=> $dt_desa->id_desa,
					'alamat'			=> $this->input->post("alamat"),
					'email'				=> $this->input->post("email"),
					'telp'				=> $this->input->post("telp"),
					'kepala_desa'		=> $this->input->post("kepala_desa"),
					'visi'				=> $this->input->post("visi"),
					'misi'				=> $this->input->post("misi"),
					'program_kerja'		=> $this->input->post("program_kerja"),
					'longitude'			=> $this->input->post("longitude"),
					'latitude'			=> $this->input->post("latitude"),
					'anggaran'			=> $this->input->post("anggaran"),
				);


					$config['upload_path']          = './data/desa/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 1000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;
					//$config['file_name']			= uniqid().".".$file_ext;
					$config['encrypt_name']			= true;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('foto')){
						$tmp_name = $_FILES['foto']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
						//echo $this->upload->display_errors();die;
					}else{
						$dt['foto'] = $this->upload->data('file_name');
					}

					if ( ! $this->upload->do_upload('struktur_organisasi')){
						$tmp_name = $_FILES['struktur_organisasi']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$dt['struktur_organisasi'] = $this->upload->data('file_name');
					}

				if($profil_desa)
				{

					if(!empty($dt['foto']) && $profil_desa)
					{
						$path = "./data/desa/".$profil_desa->foto;
						if(file_exists($path) && $profil_desa->foto!="")
						{
							unlink($path);
						}
					}

					if(!empty($dt['struktur_organisasi']) && $profil_desa)
					{
						$path = "./data/desa/".$profil_desa->struktur_organisasi;
						if(file_exists($path) && $profil_desa->struktur_organisasi!="")
						{
							unlink($path);
						}
					}

					$this->db->where("id_desa",$dt_desa->id_desa);
					$this->db->update("profil_desa",$dt);
				}
				else{
					$this->db->insert("profil_desa",$dt);
				}
		        $data['message_type'] = "success";
		        $data['message']		= "Profile desa berhasil diperbarui.";

	           	
			}
			
			$profil_desa = $this->alamat_model->get_profil_desa($id_desa);

			$data['profil_desa'] = $profil_desa;
			$data['dt_desa']  = $dt_desa;
			
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	
}
?>