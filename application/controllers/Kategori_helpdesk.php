<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_helpdesk extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('kategori_helpdesk_model');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->user_privileges	= $this->user_model->user_privileges;
		$array_privileges = explode(';', $this->user_privileges);


		if (($this->user_level!="Administrator" && $this->user_level!="Admin Web") && !in_array('blog_category', $array_privileges)) redirect ('welcome');
	}
	public function index()
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			$data['title']		= "Kategori - ". app_name;
			$data['content']	= "kategori_helpdesk/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";

			$data['query']		= $this->kategori_helpdesk_model->get_all();
			
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	public function add()
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{

			$data['title']		= "Tambah Kategori - ". app_name;
			$data['content']	= "kategori_helpdesk/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";


			if (!empty($_POST))
			{
				if ($_POST['nama_kategori_helpdesk'] !="")
				{

		                $this->kategori_helpdesk_model->nama_kategori_helpdesk = $_POST['nama_kategori_helpdesk'];
		                $this->kategori_helpdesk_model->kategori_slug = $_POST['kategori_slug'];
		                $this->kategori_helpdesk_model->status = $_POST['status'];
		                $this->kategori_helpdesk_model->insert();
		                $data['message_type'] = "success";
		                $data['message']		= "Kategori telah berhasil ditambahkan.";
		                redirect('kategori_helpdesk');

				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data belum lengkap. silahkan lengkapi dulu!";
				}
			}
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}

	public function edit($id_kategori_helpdesk)
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{

			$data['title']		= "Edit Kategori - ". app_name;
			$data['content']	= "kategori_helpdesk/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "ref";



			$this->kategori_helpdesk_model->id_kategori_helpdesk = $id_kategori_helpdesk;
			if (!empty($_POST))
			{
				if ($_POST['nama_kategori_helpdesk'] !=""  )
				{

		                $this->kategori_helpdesk_model->nama_kategori_helpdesk = $_POST['nama_kategori_helpdesk'];
		                $this->kategori_helpdesk_model->kategori_slug = $_POST['kategori_slug'];
		                $this->kategori_helpdesk_model->status = $_POST['status'];
		                $this->kategori_helpdesk_model->update();
		                $data['message_type'] = "success";
		                $data['message']		= "Kategori berhasil diperbarui.";


				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data belum lengkap. silahkan lengkapi dulu!";
				}
			}
			$this->kategori_helpdesk_model->set_by_id();
			$data['nama_kategori_helpdesk'] = $this->kategori_helpdesk_model->nama_kategori_helpdesk;
			$data['kategori_slug'] = $this->kategori_helpdesk_model->nama_kategori_helpdesk;
			$data['status'] = $this->kategori_helpdesk_model->status;
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	public function delete()
	{
			if($this->input->is_ajax_request() )
			{
					$dt=$this->db->where("id_kategori_helpdesk",$this->input->post("id"))->get("kategori_helpdesk")->row();

					$data['status'] = FALSE;

					if($this->input->post("id") && $dt){
							$this->db->delete("kategori_helpdesk",['id_kategori_helpdesk' => $this->input->post("id")]);

							if( $this->db->affected_rows() == 1 ){
									$data['status'] = true ;
							}
					}

					echo json_encode($data);
			}
	}
}
?>
