<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);
	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			
			$data['title']		= app_name;
			$data['content']	= "desa/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "desa";
			$data['page'] = "desa";

			$skpd = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs/skpd", ['key' => 'berkary4']);
			$data['skpd'] = json_decode($skpd, TRUE);

			$sdgs = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs", ['key' => 'berkary4']);
			$data['sdgs'] = json_decode($sdgs, TRUE);

			$json = file_get_contents('http://124.158.169.179/jsonJumlah/3211.json');

			$data['disduk'] = json_decode($json);


			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function kecamatan($id){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			
			$data['title']		= app_name;
			$data['content']	= "desa/dashboard_kecamatan" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "desa";
			$data['page'] = "desa";

			$skpd = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs/skpd", ['key' => 'berkary4', 'skpd_induk' => $id]);
			$data['skpd'] = json_decode($skpd, TRUE);

			$sdgs = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs", ['key' => 'berkary4', 'skpd_induk' => $id]);
			$data['sdgs'] = json_decode($sdgs, TRUE);

			$json = file_get_contents('http://124.158.169.179/jsonJumlah/'.$data['skpd']['skpd_detail'][0]['id_kecamatan_disduk'].'.json');

			$data['disduk'] = json_decode($json);

			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function detail($id = null){
		
		if ($this->user_id && $this->session->userdata("user_level")==1 && $id != null){
			
			$data['title']		= app_name;
			$data['content']	= "desa/detail" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "desa";
			$data['page'] = "desa";

			// $skpd = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs/skpd", ['key' => 'berkary4', 'id_skpd' => $id]);
			// $data['skpd'] = json_decode($skpd, TRUE);

			$sdgs_detail = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs/detail_tahun", ['key' => 'berkary4', 'id_simpatik_kuesioner_periode' => $id]);
			$data['sdgs_detail'] = json_decode($sdgs_detail, TRUE);

			$sdgs = $this->http_request("https://e-officedesa.sumedangkab.go.id/api/sdgs", ['key' => 'berkary4']);
			$data['sdgs'] = json_decode($sdgs, TRUE);

			$json = file_get_contents('http://124.158.169.179/jsonJumlah/'.$data['sdgs_detail']['detail_skpd']['id_desa_disduk'].'.json');
			$data['disduk'] = json_decode($json);
			
			// var_dump($data['skpd']);die;

			// print_r($data['sdgs_detail']);die;

			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	function http_request($url, $data = ''){
		// persiapkan curl
		$ch = curl_init(); 
	
		// set url 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
		//
		curl_setopt($ch, CURLOPT_POST, 1);

		// set params
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	
		// return the transfer as a string 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	
		// $output contains the output string 
		$output = curl_exec($ch); 
	
		// tutup curl 
		curl_close($ch);      
	
		// mengembalikan hasil curl
		return $output;
	}

}
