<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sie extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		$ambiltahap=$this->input->post('tahap');
		$ambiltahun=$this->input->post('tahun');
        if (empty($ambiltahap) or empty($ambiltahun)) {
			$data['tahap']='2';
			$data ['tahun']='2021';
		} else{
		$data['tahap'] = $this->input->post('tahap');
        $data['tahun'] = $this->input->post('tahun');

		}

		
		$this->load->view('admin/sie/dashboard',$data);
	}

	public function tes()
	{
		
        $data['tahap'] = $this->input->post('tahap');
        $data['tahun'] = $this->input->post('tahun');
		$this->load->view('admin/sie/tes',$data);
	}
}
