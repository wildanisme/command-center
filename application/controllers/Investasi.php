<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Investasi extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');
		$this->load->model('keuangan_model');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');

		$this->load->model("Investasi_model");


		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);


		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "investasi/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "investasi";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard()
	{
	
		$data["data_investasio"] = $this->Investasi_model->getAll();

		$this->load->view('admin/investasi/dashboard_view',$data);
	}

	
	public function add() 
	{

			$Investasi = $this->Investasi_model; 
			$validation = $this->form_validation; 
			$validation->set_rules($Investasi->rules()); 
			if ($validation->run()) {
				$data['title']		= app_name;
				$data['content']	= "investasi/dashboard" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
					$Investasi->save();
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data Investasi berhasil disimpan. 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
				redirect("investasi");
			} else {
				$data['title']		= 'Tambah Investasi ' . app_name;
				$data['content']	= "investasi/add" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
				$this->load->view('admin/index',$data);
			}
	}



	


	public function edit($id = null)
	{

			$Investasio = $this->Investasi_model; 
			$validation = $this->form_validation; 
			$validation->set_rules($Investasio->rules()); 
			
			if ($validation->run()) {
					$data['title']		= app_name;
					$data['content']	= "investasi/dashboard" ;
					$data['user_picture'] = $this->user_picture;
					$data['full_name']		= $this->full_name;
					$data['user_level']		= $this->user_level;
					$data['active_menu'] = "investasi";
					$Investasio->update();
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data investasi berhasil disimpan.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
				redirect("investasi");
			} else {
				$data['title']		= 'Edit Investasi ' . app_name;
				$data['content']	= "investasi/edit" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
				$data["data_investasio"] = $Investasio->getById($id);
				if (!$data["data_investasio"]) show_404();
				$this->load->view('admin/index',$data);
			}
			
	}

	public function total_apbd() {
		$data["jumrelp"]=$this->db->query('SELECT investasio,count(nama_opd) as jumrelp,sum(target_investasio) 
		as jumrelp,sum(hasil_usaha) as jumrealp FROM umkm group by investasio')->result();
			}














	

	public function delete()
	{
			$id = $this->input->get('id');
			if (!isset($id)) show_404();
			$this->Investasi_model->delete($id);
			$msg['success'] = true;
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
			Data Mahasiswa berhasil dihapus.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
			$this->output->set_output(json_encode($msg));
	}

}
