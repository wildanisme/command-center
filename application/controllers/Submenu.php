<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Submenu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model(array('Mod_submenu','user_model','Mod_userlevel','Mod_menu'));
        $this->user_model->user_id = $this->user_id;
        $this->user_model->set_user_by_user_id();
        $this->user_picture = $this->user_model->user_picture;
        $this->full_name    = $this->user_model->full_name;
        $this->user_level   = $this->user_model->level;
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->library('session');
        $this->load->model('tank_auth/users');

        $this->load->helper('text');
        $this->load->helper('typography');
        $this->load->helper('file');

        $this->bulan = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'Nopember',
            12 => 'Desember',
        );


        if ($this->tank_auth->is_logged_in()) {
            redirect ('welcome'); 
        }
    }

    public function index()
    {
        if ($this->user_id)
        {
            $data['title']      = app_name;
            $data['content']    = "submenu/dashboard" ;
            $data['user_picture'] = $this->user_picture;
            $data['full_name']      = $this->full_name;

            $data['user_level']     = $this->user_level;
            
            $data['active_menu'] = "submenu";


            $this->load->view('admin/index',$data);

        }
        else
        {
         redirect('admin/login');
     }
 }


 public function dashboard()
 {
    $data["submenu"] = $this->Mod_submenu->getAll()->result();

    $this->load->view('admin/submenu/dashboard_view',$data);
}

public function add()
{
    $data['menu'] = $this->Mod_menu->getAll()->result();
    $validation = $this->form_validation; 
    $validation->set_rules($this->rules()); 
    if ($validation->run()) {
        $data['title']      = app_name;
        $data['content']    = "submenu/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "submenu";
        $this->insert();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sub Menu berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect("submenu");
    } else {
        $data['title']      = 'Tambah Sub Menu ' . app_name;
        $data['content']    = "submenu/add" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "submenu";
        $this->load->view('admin/index',$data);
    }
    // $this->load->view('admin/submenu/add');
}


public function edit($id)
{
    $data['menu'] = $this->Mod_menu->getAll()->result();
    $data['submenu'] = $this->Mod_submenu->get_submenu($id);
    $validation = $this->form_validation; 
    $validation->set_rules($this->rules()); 
    if ($validation->run()) {
        $data['title']      = app_name;
        $data['content']    = "submenu/dashboard" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "submenu";
        $this->update();
        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Sub Menu berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
        redirect("submenu");
    } else {
        $data['title']      = 'Edit Sub  Menu ' . app_name;
        $data['content']    = "submenu/edit" ;
        $data['user_picture'] = $this->user_picture;
        $data['full_name']      = $this->full_name;
        $data['user_level']     = $this->user_level;
        $data['active_menu'] = "menu";
        $this->load->view('admin/index',$data);
    }
    // $this->load->view('admin/submenu/edit');
    
}

public function insert()
{
   $save  = array(
    'nama_submenu'  => ucwords($this->input->post('nama_submenu')),
    'link'      => $this->input->post('link'),
    'icon'      => $this->input->post('icon'),
    'id_menu'   => $this->input->post('id_menu'),
    'is_active' => $this->input->post('is_active'),
    'header'   => $this->input->post('header'),
    'urutan'   => $this->input->post('urutan'),
);
   $this->Mod_submenu->insertsubmenu("tbl_submenu", $save);
   $insert_id = $this->db->insert_id();
   $levels = $this->Mod_userlevel->getAll()->result();
   foreach ($levels as $row) {
    $data = array(
        'id_submenu' => $insert_id,
        'id_level'   => $row->level_id,
    );
    $this->Mod_submenu->insert_akses_submenu("tbl_akses_submenu",$data);
}


}

public function update()
{

    $id = $this->input->post('id_submenu');
    $data  = array(
        'nama_submenu' => ucwords($this->input->post('nama_submenu')),
        'link'      => $this->input->post('link'),
        'icon'      => $this->input->post('icon'),
        'id_menu'    => $this->input->post('id_menu'),
        'is_active' => $this->input->post('is_active'),
        'header'   => $this->input->post('header'),
        'urutan'   => $this->input->post('urutan'),
    );
    $this->Mod_submenu->updatesubmenu($id, $data);
    
}
public function delete()
{
    $id_submenu = $this->input->post('id_submenu');
    $this->Mod_submenu->deletesubmenu($id_submenu, 'tbl_submenu');
    $data['status'] = TRUE;
    echo json_encode($data);
    
}


public function rules()
    {
        return [
            [
                'field' => 'id_menu',
                'label' => 'Menu',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_submenu',
                'label' => 'Nama Sub Menu',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'urutan',
                'label' => 'Urutan',
                'rules' => 'trim|required|numeric'
            ],
        ];
    }
}