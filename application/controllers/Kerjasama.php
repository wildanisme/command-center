<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kerjasama extends CI_Controller
{
    public function __construct()
    {
        parent ::__construct(); 
        $this->user_id = $this->session->userdata('user_id');
        $this->load->model('user_model');
        $this->user_model->user_id = $this->user_id;
        $this->user_model->set_user_by_user_id();
        $this->user_picture = $this->user_model->user_picture;
        $this->full_name    = $this->user_model->full_name;
        $this->user_level   = $this->user_model->level;
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
        $this->load->library('tank_auth');
        $this->load->library('session');
        $this->load->model('tank_auth/users');

        $this->load->helper('text');
        $this->load->helper('typography');
        $this->load->helper('file');

        $this->bulan = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'Nopember',
            12 => 'Desember',
        );


        if ($this->tank_auth->is_logged_in()) {
            redirect ('welcome'); 
        }


        //echo $this->session->userdata('employee_id');exit();
        $this->load->model("kerjasama_model");
        $this->load->model("kerjasama/bidang_model");
        $this->load->model("kerjasama/status_model");
        $this->load->model("kerjasama/skpd_model");
        $this->load->helper('download');
    }

    public function index(){

        if ($this->user_id && $this->session->userdata("user_level")==1){
            //if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
            //if ($this->user_level=="User") redirect ('admin/dashboard'); 
            $data['title']      = app_name;
            $data['content']    = "kerjasama/dashboard" ;
            $data['user_picture'] = $this->user_picture;
            $data['full_name']      = $this->full_name;
            $data['user_level']     = $this->user_level;
            $data['active_menu'] = "kerjasama";

            
            $this->load->view('admin/index',$data);
        } else {
            redirect('admin/login');
            
        }
    }

    public function dashboard(){

        if ($this->user_id && $this->session->userdata("user_level")==1){
            $data = $this->getStatistikAll();
            $data["tbl_kerjasama"] = $this->kerjasama_model->getAll();
            $data["filter_tahun"] = $this->db->query('SELECT DISTINCT(YEAR(tanggal)) as year FROM kerjasama')->result();
            $data["filter_status"] = $this->db->query('SELECT nama_status FROM kerjasama_status')->result();
            $data["pilih_tahun"] = "";
            $data["pilih_status"] = "";

            $data["galeri_new"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama ORDER BY tanggal DESC LIMIT 10')->result();
            $data["galeri_finish"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama WHERE status = "Finish" ORDER BY tanggal DESC LIMIT 10')->result();

            if (!empty($data)) {
                $this->load->view('admin/kerjasama/dashboard_view',$data);
            } else {
                $this->load->view('admin/404',$data);
            }
            
        } else {
            redirect('admin/login');
            
        }
    }

    public function indexs()
    {

        $data = $this->getStatistikAll();
        $data["tbl_kerjasama"] = $this->kerjasama_model->getAll();
        $data["filter_tahun"] = $this->db->query('SELECT DISTINCT(YEAR(tanggal)) as year FROM kerjasama')->result();
        $data["filter_status"] = $this->db->query('SELECT nama_status FROM kerjasama_status')->result();
        $data["pilih_tahun"] = "";
        $data["pilih_status"] = "";

        $data["galeri_new"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama ORDER BY tanggal DESC LIMIT 10')->result();
        $data["galeri_finish"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama WHERE status = "Finish" ORDER BY tanggal DESC LIMIT 10')->result();

        $this->load->view("admin/kerjasama/Dashboards", $data);
    }

    public function filter()
    {

        $post = $this->input->post();
        $get_year = $post["tahun"];
        $get_status = $post["status"];


        if ($get_year == "Semua") {
            if ($get_status == "Semua") {
                $data = $this->getStatistikAll();
                $data["tbl_kerjasama"] = $this->kerjasama_model->getAll();
            } else {
                $data = $this->getStatistikByStatus($get_status);
                $data["tbl_kerjasama"] = $this->db->query("SELECT * from kerjasama where status = '$get_status'")->result();
            }
        } else {
            if ($get_status == "Semua") {
                $data = $this->getStatistikByYear($get_year);
                $data["tbl_kerjasama"] = $this->db->query("SELECT * from kerjasama where YEAR(tanggal) = '$get_year'")->result();
            } else {
                $data = $this->getStatistikByYearAndStatus($get_year, $get_status);
                $data["tbl_kerjasama"] = $this->db->query("SELECT * from kerjasama where YEAR(tanggal) = '$get_year' AND status ='$get_status'")->result();
            }
        }


        $data["pilih_tahun"] = $get_year;
        $data["pilih_status"] = $get_status;

        $data["filter_tahun"] = $this->db->query('SELECT DISTINCT(YEAR(tanggal)) as year FROM kerjasama')->result();
        $data["filter_status"] = $this->db->query('SELECT nama_status FROM kerjasama_status')->result();
        $data["galeri_new"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama ORDER BY tanggal DESC LIMIT 10')->result();
        $data["galeri_finish"] = $this->db->query('SELECT no_surat_ket, judul_kerjasama, pihak_pertama, galeri FROM kerjasama WHERE status = "Finish" ORDER BY tanggal DESC LIMIT 10')->result();

        $this->load->view("user/dashboard", $data);
    }

    public function getStatistikAll()
    {
        $data["status"] = $this->db->query(

            'SELECT 
            bb.nama_status AS status,
            CASE WHEN aa.total IS NULL THEN 0 ELSE aa.total
            END AS total
            FROM
            (
            SELECT
            a.status,
            COUNT(*) AS total
            FROM
            kerjasama AS a,
            kerjasama_status AS b
            WHERE
            a.status = b.nama_status
            GROUP BY
            a.status
            ) aa
            RIGHT JOIN kerjasama_status AS bb
            ON
            aa.status = bb.nama_status'

        )->result();

        $data["bidang"] = $this->db->query(
            'SELECT
            bb.nama_bidang AS bidang,
            CASE WHEN aa.total IS NULL THEN 0 ELSE aa.total
            END AS total
            FROM
            (
            SELECT
            a.bidang,
            COUNT(*) AS total
            FROM
            kerjasama AS a,
            kerjasama_bidang AS b
            WHERE
            a.bidang = b.nama_bidang
            GROUP BY
            a.bidang
            ) aa
            RIGHT JOIN kerjasama_bidang AS bb
            ON
            aa.bidang = bb.nama_bidang'
        )->result();

        $data["skpd"] = $this->db->query(
            'SELECT
            a.pihak_pertama AS skpd,
            COUNT(*) AS total
            FROM
            kerjasama AS a,
            kerjasama_skpd AS b
            WHERE
            a.pihak_pertama = b.nama_skpd
            GROUP BY
            a.pihak_pertama'
        )->result();

        return $data;
    }

    public function getStatistikByYear($tahun)
    {

        $data["status"] = $this->db->query("SELECT COUNT(*) total, STATUS status FROM kerjasama WHERE YEAR(tanggal) = '$tahun'  GROUP BY STATUS")->result();

        $data["bidang"] = $this->db->query("SELECT COUNT(*) total, bidang FROM kerjasama WHERE YEAR(tanggal) = '$tahun' GROUP BY bidang")->result();

        $data["skpd"] = $this->db->query("SELECT COUNT(*) total, pihak_pertama skpd FROM kerjasama WHERE YEAR(tanggal) = '$tahun' GROUP BY pihak_pertama")->result();

        return $data;
    }

    public function getStatistikByStatus($status)
    {

        $data["status"] = $this->db->query("SELECT COUNT(*) total, STATUS status FROM kerjasama WHERE status= '$status'  GROUP BY STATUS")->result();

        $data["bidang"] = $this->db->query("SELECT COUNT(*) total, bidang FROM kerjasama WHERE status= '$status'  GROUP BY bidang")->result();

        $data["skpd"] = $this->db->query("SELECT COUNT(*) total, pihak_pertama skpd FROM kerjasama WHERE status= '$status'  GROUP BY pihak_pertama")->result();

        return $data;
    }

    public function getStatistikByYearAndStatus($tahun, $status)
    {

        $data["status"] = $this->db->query("SELECT COUNT(*) total, STATUS status FROM kerjasama WHERE YEAR(tanggal) = '$tahun' AND status= '$status'  GROUP BY STATUS")->result();

        $data["bidang"] = $this->db->query("SELECT COUNT(*) total, bidang FROM kerjasama WHERE YEAR(tanggal) = '$tahun' AND status= '$status' GROUP BY bidang")->result();

        $data["skpd"] = $this->db->query("SELECT COUNT(*) total, pihak_pertama skpd FROM kerjasama WHERE YEAR(tanggal) = '$tahun' AND status= '$status' GROUP BY pihak_pertama")->result();

        return $data;
    }

    public function downloadFileKerjasama($fileName = NULL)
    {
        if ($fileName) {
            $file = realpath("uploads/kerjasama") . "\\" . $fileName;
            // check file exists    
            if (file_exists($file)) {
                // get file content
                $data = file_get_contents($file);
                //force download
                force_download($fileName, $data);
            } else {
                // Redirect to base url
                redirect(base_url());
            }
        }
    }

    //GALLERY
    public function gallery()
    {

        $data["tbl_kerjasama"] = $this->kerjasama_model->getAll();


        $this->load->view("admin/kerjasama/gallery", $data);
    }

    public function search()
    {

        $post = $this->input->post();
        $search = $post["search"];

        $data["tbl_kerjasama"] = $this->db->query("SELECT * FROM kerjasama a WHERE a.judul_kerjasama LIKE '%$search%' OR a.judul_kerjasama LIKE '%$search' OR a.judul_kerjasama LIKE '%$search'")->result();

        $this->load->view("admin/kerjasama/gallery", $data);
    }


    function detail($id)
    {
        $data['DetailKerjasama']    = $this->kerjasama_model->DetailData($id);
        $data['page']    = 'DetailKerjasama';


        $this->load->view("admin/kerjasama/example_gallery", $data);
        $this->load->view('admin/kerjasama/_partials/logout_modal');
    }
	
	
	//ADMIN
	public function admin()
    {

        $data["tbl_kerjasama"] = $this->kerjasama_model->getAll();
        $data["bidang"] = $this->db->query('SELECT bb.nama_bidang AS bidang, CASE WHEN aa.total IS NULL THEN 0 ELSE aa.total END AS total 
                                            FROM (SELECT a.bidang, COUNT(*) AS total
                                                FROM kerjasama AS a,
                                                      kerjasama_bidang AS b
                                                WHERE a.bidang = b.nama_bidang
                                                GROUP BY a.bidang) aa
                                            RIGHT JOIN kerjasama_bidang AS bb
                                            ON aa.bidang = bb.nama_bidang')->result();

        $this->load->view("admin/kerjasama/admin/kerjasama/list", $data);
    }


    public function addKerjasama()
    {
        $data["data_bidang"] = $this->bidang_model->getAll();
        $data["data_status"] = $this->status_model->getAll();
        $data["data_skpd"] = $this->skpd_model->getAll();

        $this->load->view("admin/kerjasama/admin/kerjasama/new_form", $data);
    }

    public function saveKerjasama()
    {

        $kerjasama = $this->kerjasama_model;
        $kerjasama->save();
        $this->session->set_flashdata('success', 'Data Berhasil di Simpan');

        $this->addKerjasama();
    }


    public function editKerjasama($id = null)
    {
        $data["data_bidang"] = $this->bidang_model->getAll();
        $data["data_status"] = $this->status_model->getAll();
        $data["data_skpd"] = $this->skpd_model->getAll();

        if (!isset($id)) redirect('admin/kerjasama/admin');

        $kerjasama = $this->kerjasama_model;

        $data["tbl_kerjasama"] = $kerjasama->getById($id);
        if (!$data["tbl_kerjasama"]) show_404();

        $this->load->view("admin/kerjasama/admin/kerjasama/edit_form", $data);
    }

    public function updateKerjasama()
    {
        $kerjasama = $this->kerjasama_model;
        $kerjasama->update();
        $this->session->set_flashdata('success', 'Data Berhasil di Ubah');
        $this->editKerjasama($kerjasama->id_surat);
    }

    public function deleteKerjasama($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->kerjasama_model->delete($id)) {
            redirect(site_url('admin/kerjasama/admin'));
        }
    }
	
	//PARAMETER SKPD
	public function adminParameterSkpd()
    {

        $data["skpd"] = $this->skpd_model->getAll();

        $this->load->view("admin/kerjasama/admin/parameter/skpd/list_skpd", $data);
    }

    public function addParameterSkpd()
    {
        $this->load->view("admin/kerjasama/admin/parameter/skpd/new_form_skpd");
    }

    public function saveParameterSkpd()
    {
        $skpd = $this->skpd_model;
        $skpd->save();
        $this->session->set_flashdata('success', 'Data Berhasil di Simpan');
        $this->addParameterSkpd();
    }

    public function editParameterSkpd($id = null)
    {
        if (!isset($id)) redirect('kerjasama/adminParameterSkpd');

        $skpd = $this->skpd_model;

        $data["skpd"] = $skpd->getById($id);
        if (!$data["skpd"]) show_404();

        $this->load->view("admin/kerjasama/admin/parameter/skpd/edit_form_skpd", $data);
    }

    public function updateParameterSkpd()
    {
        $skpd = $this->skpd_model;

        $skpd->update();
        $this->session->set_flashdata('success', 'Data Berhasil di Ubah');
        $this->editParameterSkpd($skpd->id_skpd);
    }

    public function deleteParameterSkpd($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->skpd_model->delete($id)) {
            redirect(site_url('kerjasama/adminParameterSkpd'));
        }
    }
	
	//Parameter Bidang
	 public function adminParameterBidang()
    {

        $data["bidang"] = $this->bidang_model->getAll();

        $this->load->view("admin/kerjasama/admin/parameter/bidang/list_bidang", $data);
    }

    public function addParameterBidang()
    {
        $this->load->view("admin/kerjasama/admin/parameter/bidang/new_form_bidang");
    }

    public function saveParameterBidang()
    {
        $bidang = $this->bidang_model;
        $bidang->save();
        $this->session->set_flashdata('success', 'Data Berhasil di Simpan');
        $this->add();
    }

    public function editParameterBidang($id = null)
    {
        if (!isset($id)) redirect('admin/parameter/bidang');

        $bidang = $this->bidang_model;

        $data["bidang"] = $bidang->getById($id);
        if (!$data["bidang"]) show_404();

        $this->load->view("admin/kerjasama/admin/parameter/bidang/edit_form_bidang", $data);
    }

    public function updateParameterBidang()
    {
        $bidang = $this->bidang_model;

        $bidang->update();
        $this->session->set_flashdata('success', 'Data Berhasil di Ubah');
        $this->editParameterBidang($bidang->id_bidang);
    }

    public function deleteParameterBidang($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->bidang_model->delete($id)) {
            redirect(site_url('kerjasama/adminParameterBidang'));
        }
    }
}
