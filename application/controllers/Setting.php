<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('setting_model');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->user_privileges	= $this->user_model->user_privileges;
		$array_privileges = explode(';', $this->user_privileges);

			if (($this->user_level!="Administrator" && $this->user_level!="Admin Web") && !in_array('company', $array_privileges)) redirect ('welcome');
	}

	public function index()
	{
		if ($this->user_id)
		{
			
			$data['title']		= "Setting - ". app_name;
			$data['content']	= "setting/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			
			if (!empty($_POST))
			{
				if ($_POST['header'] !="" &&
					$_POST['footer'] !="" )
					
				
				{
					
						$config['upload_path']          = './data/logo/antrian/';
			            $config['allowed_types']        = 'gif|jpg|png';
			            $config['max_size']             = 2000;
			            $config['max_width']            = 2000;
			            $config['max_height']           = 2000;

			           $this->load->library('upload', $config);
			           if ( ! $this->upload->do_upload())
		               {
		                    $this->setting_model->logo 	= "";
		                    $tmp_name				= $_FILES['userfile']['tmp_name'];
		                    if ($tmp_name!="")
		                    {
		                    	$data['error']			= $this->upload->display_errors();
		                    }
		                }
		                else
		                {
		                	$this->setting_model->set_setting();
		                	if ($this->setting_model->logo !="") unlink('./data/logo/antrian/'.$this->setting_model->logo);
		                	$this->setting_model->logo = $this->upload->data('file_name');
		                }

		                $this->setting_model->header = $_POST['header'];
		                $this->setting_model->header2 = $_POST['header2'];
						$this->setting_model->header3 = $_POST['header3'];
		                $this->setting_model->tagline = $_POST['tagline'];
		                $this->setting_model->footer= $_POST['footer'];
		                $this->setting_model->footer2= $_POST['footer2'];

		                $this->setting_model->update_setting();
		                $data['message_type'] = "success";
		                $data['message']		= "setting berhasil diperbarui.";
		            
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data belum lengkap, silahkan lengkapi dulu!";
				}
			}
			$this->setting_model->set_setting();
			$data['header']		= $this->setting_model->header;
			$data['header2']		= $this->setting_model->header2;
			$data['header3']		= $this->setting_model->header3;
			$data['logo']		= $this->setting_model->logo;
			$data['tagline']		= $this->setting_model->tagline;
			$data['footer']		= $this->setting_model->footer;
			$data['footer2']		= $this->setting_model->footer2;
			$data['active_menu'] = "setting";
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}
	
	

}
?>