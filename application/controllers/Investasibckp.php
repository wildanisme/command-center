<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Investasi extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');
		$this->load->model('keuangan_model');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');

		$this->load->model("Investasi_model");

		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);


		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "investasi/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "investasi";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard()
	{
		$data["data_investasi"] = $this->Investasi_model->getAll();

		$this->load->view('admin/investasi/dashboard_view',$data);
	}

	public function konten(){
		
		$data['title']		= 'dashboard';
		$data['content']	= "umkm/konten" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;

		$data["jumumkm"]=$this->db->query('SELECT COUNT( nama_umkm ) as total FROM umkm')->result();
		$data["jumusaha"]=$this->db->query('SELECT count(DISTINCT jenis) as total FROM umkm')->result();
		$data["jumaset"]=$this->db->query('SELECT sum(asset) as total FROM umkm')->result();
		$data["jumahasil"]=$this->db->query('SELECT sum(hasil_usaha) as total FROM umkm')->result();
		$data["jumkec"]=$this->db->query('SELECT kecamatan,count(nama_umkm) as totalumkm,sum(asset) 
		as totalaset,sum(hasil_usaha) as totalhasil FROM umkm group by kecamatan')->result();
		$data["jk"]=$this->db->query('SELECT sum(if(jk_pemilik="perempuan",1,0)) as totalwanita,
		sum(if(jk_pemilik="Laki-laki",1,0)) as totallaki from umkm')->result();
		$data["kategori"]=$this->db->query('SELECT jenis,count(jenis)as total 
		FROM umkm group by jenis order by total DESC limit 5')->result();
		$data["asset"]=$this->db->query('SELECT nama_umkm,jenis,asset FROM umkm  order by asset DESC limit 5')->result();
		$data["hasil"]=$this->db->query('SELECT nama_umkm,jenis,hasil_usaha FROM umkm  order by hasil_usaha DESC limit 5')->result();
		$data["gerai"]=$this->db->query('SELECT * from umkm_gerai')->result();

		$this->load->view('admin/umkm/konten',$data);
	}


	public function add() 
	{

			$Investasi = $this->Investasi_model; 
			$validation = $this->form_validation; 
			$validation->set_rules($Investasi->rules()); 
			if ($validation->run()) {
				$data['title']		= app_name;
				$data['content']	= "investasi/dashboard" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
					$Investasi->save();
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data Investasi berhasil disimpan. 
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
				redirect("investasi");
			} else {
				$data['title']		= 'Tambah Investasi ' . app_name;
				$data['content']	= "investasi/add" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
				$this->load->view('admin/index',$data);
			}
	}

	public function edit($id = null)
	{

			$Investasi = $this->Investasi_model; 
			$validation = $this->form_validation; 
			$validation->set_rules($Investasi->rules()); 
			
			if ($validation->run()) {
					$data['title']		= app_name;
					$data['content']	= "investasi/dashboard" ;
					$data['user_picture'] = $this->user_picture;
					$data['full_name']		= $this->full_name;
					$data['user_level']		= $this->user_level;
					$data['active_menu'] = "investasi";
					$Investasi->update();
					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
					Data investasi berhasil disimpan.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
				redirect("investasi");
			} else {
				$data['title']		= 'Edit Investasi ' . app_name;
				$data['content']	= "investasi/edit" ;
				$data['user_picture'] = $this->user_picture;
				$data['full_name']		= $this->full_name;
				$data['user_level']		= $this->user_level;
				$data['active_menu'] = "investasi";
				$data["data_investasi"] = $Investasi->getById($id);
				if (!$data["data_investasi"]) show_404();
				$this->load->view('admin/index',$data);
			}
			
	}

	public function delete()
	{
			$id = $this->input->get('id');
			if (!isset($id)) show_404();
			$this->Investasi_model->delete($id);
			$msg['success'] = true;
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
			Data Mahasiswa berhasil dihapus.
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button></div>');
			$this->output->set_output(json_encode($msg));
	}

}
