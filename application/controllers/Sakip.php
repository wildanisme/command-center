<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sakip extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');
		$this->load->model('keuangan_model');

		$this->load->helper('text');
		$this->load->helper('typography');
		$this->load->helper('file');

		$this->bulan = array(
			1 => 'Januari',
			2 => 'Februari',
			3 => 'Maret',
			4 => 'April',
			5 => 'Mei',
			6 => 'Juni',
			7 => 'Juli',
			8 => 'Agustus',
			9 => 'September',
			10 => 'Oktober',
			11 => 'Nopember',
			12 => 'Desember',
		);


		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "sakip/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "sakip";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard(){
		
		$data["jumumkm"]=$this->db->query('SELECT COUNT( nama_umkm ) as total FROM umkm')->result();
		$data["jumusaha"]=$this->db->query('SELECT count(DISTINCT jenis) as total FROM umkm')->result();
		$data["jumaset"]=$this->db->query('SELECT sum(asset) as total FROM umkm')->result();
		$data["jumahasil"]=$this->db->query('SELECT sum(hasil_usaha) as total FROM umkm')->result();
		$data["jumkec"]=$this->db->query('SELECT kecamatan,count(nama_umkm) as totalumkm,sum(asset) 
		as totalaset,sum(hasil_usaha) as totalhasil FROM umkm group by kecamatan')->result();
		$data["jk"]=$this->db->query('SELECT sum(if(jk_pemilik="perempuan",1,0)) as totalwanita,
		sum(if(jk_pemilik="Laki-laki",1,0)) as totallaki from umkm')->result();
		$data["kategori"]=$this->db->query('SELECT jenis,count(jenis)as total 
		FROM umkm group by jenis order by total DESC limit 5')->result();
		$data["asset"]=$this->db->query('SELECT nama_umkm,jenis,asset FROM umkm  order by asset DESC limit 5')->result();
		$data["hasil"]=$this->db->query('SELECT nama_umkm,jenis,hasil_usaha FROM umkm  order by hasil_usaha DESC limit 5')->result();
		$data["gerai"]=$this->db->query('SELECT * from umkm_gerai')->result();

		$this->load->view('admin/sakip/dashboard_view',$data);
	}

	public function konten(){
		
		$data['title']		= 'dashboard';
		$data['content']	= "sakip/konten" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;

		$data["jumumkm"]=$this->db->query('SELECT COUNT( nama_umkm ) as total FROM umkm')->result();
		$data["jumusaha"]=$this->db->query('SELECT count(DISTINCT jenis) as total FROM umkm')->result();
		$data["jumaset"]=$this->db->query('SELECT sum(asset) as total FROM umkm')->result();
		$data["jumahasil"]=$this->db->query('SELECT sum(hasil_usaha) as total FROM umkm')->result();
		$data["jumkec"]=$this->db->query('SELECT kecamatan,count(nama_umkm) as totalumkm,sum(asset) 
		as totalaset,sum(hasil_usaha) as totalhasil FROM umkm group by kecamatan')->result();
		$data["jk"]=$this->db->query('SELECT sum(if(jk_pemilik="perempuan",1,0)) as totalwanita,
		sum(if(jk_pemilik="Laki-laki",1,0)) as totallaki from umkm')->result();
		$data["kategori"]=$this->db->query('SELECT jenis,count(jenis)as total 
		FROM umkm group by jenis order by total DESC limit 5')->result();
		$data["asset"]=$this->db->query('SELECT nama_umkm,jenis,asset FROM umkm  order by asset DESC limit 5')->result();
		$data["hasil"]=$this->db->query('SELECT nama_umkm,jenis,hasil_usaha FROM umkm  order by hasil_usaha DESC limit 5')->result();
		$data["gerai"]=$this->db->query('SELECT * from umkm_gerai')->result();

		$this->load->view('admin/umkm/konten',$data);
	}

	function create_view()
	{
		$return = $this->keuangan_model->create_view();
		if ($return) {
			redirect('contoh');
		}
	}

	public function dashboard_(){
		
		$data['query'] = $this->keuangan_model->get_keuangan_view();

		if (!empty($data['query'])) {
			$this->load->view('admin/keuangan/dashboard_view',$data);
		} else {
			if (!$this->db->table_exists('keuangan_view'));
			{
				$this->create_view();
			}
			$this->load->view('admin/404',$data);
		}
		
	}


	public function sumber(){
		

		$data['title']	= "dashboard" ;
		$data['content']	= "keuangan/sumber" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;
		$data['active_menu'] = "keuangan";

		$this->load->library('form_validation');
		if($_POST)
		{

			$html_escape = html_escape($_POST);
			$postdata = array();
			foreach($html_escape as $key=>$value)
			{
				$postdata[$key] = $this->security->xss_clean($value);
			}

			$this->form_validation->set_data( $postdata );

			if ($postdata['save_method'] == "update_perencanaan") {
				$validation_rules = [
					[
						'field' => 'id_keuangan',
						'rules' => 'trim|required|numeric',
					],
					[
						'field' => 'apbd_murni',
						'label' => 'APBD Murni',
						'rules' => 'numeric',
						'errors' => [
							'numeric' => '%s nominal tidak diperbolehkan',
						]
					],
					[
						'field' => 'apbd_perubahan',
						'label' => 'APBD Perubahan',
						'rules' => 'numeric',
						'errors' => [
							'numeric' => '%s nominal tidak diperbolehkan',
						]
					],

				];
			} elseif ($postdata['save_method'] == "update_realisasi") {
				$validation_rules = [
					[
						'field' => 'id_realisasi',
						'rules' => 'trim|required|numeric',
					],
					[
						'field' => 'jenis_realisasi',
						'label' => 'Jenis Realisasi',
						'rules' => 'trim|required',
						'errors' => [
							'required' => '%s diperlukan',
						]
					],
					[
						'field' => 'anggaran_realisasi',
						'label' => 'Anggaran Realisasi',
						'rules' => 'numeric',
						'errors' => [
							'numeric' => '%s nominal tidak diperbolehkan',
						]
					],
					[
						'field' => 'tgl_realisasi',
						'label' => 'Tanggal Realisasi',
						'rules' => 'required',
						'errors' => [
							'required' => '%s diperlukan',
						]
					],

				];
			}

			$this->form_validation->set_rules( $validation_rules );

			if( $this->form_validation->run() )
			{

					//echo "<pre>";print_r($dt);die;
				if ($postdata['save_method'] == "update_perencanaan") {
					$dt = array(
						'apbd_murni'			=> $postdata['apbd_murni'],
						'apbd_perubahan'		=> $postdata['apbd_perubahan'],
					);
					$this->db->where('id_keuangan', $postdata['id_keuangan']);
					$this->db->update("keuangan_perencanaan",$dt);
					$id_ref = $this->db->insert_id();
				} elseif ($postdata['save_method'] == "update_realisasi") {
					$dt = array(
						'jenis_realisasi'		=> $postdata['jenis_realisasi'],
						'anggaran_realisasi'	=> $postdata['anggaran_realisasi'],
						'tgl_realisasi'			=> $postdata['tgl_realisasi'],
					);
					$this->db->where('id_realisasi', $postdata['id_realisasi']);
					$this->db->update("keuangan_realisasi",$dt);
					$id_ref = $this->db->insert_id();
				}

				$this->session->set_flashdata("success","Referensi berhasil disimpan");
				redirect($this->router->fetch_class()."/".$this->router->fetch_method());
			}
			else{
				$this->session->set_flashdata("danger","Referensi gagal disimpan ");
			}
		}

		$data['query'] = $this->keuangan_model->get_all_perencanaan();
		$data['query2'] = $this->keuangan_model->get_all_realisasi();

		$this->load->view('admin/index',$data);
	}



	public function sumber_perencanaan_add(){
		
		$data['title']		= 'dashboard';
		$data['content']	= "keuangan/sumber_perencanaan_add" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;
		$data['active_menu'] = "keuangan";

		$this->load->library('form_validation');
		if($_POST)
		{

			$html_escape = html_escape($_POST);
			$postdata = array();
			foreach($html_escape as $key=>$value)
			{
				$postdata[$key] = $this->security->xss_clean($value);
			}

			foreach ($postdata['id_ref_keuangan'] as $k => $v) {$check_data[$v][] = $postdata['tahun'][$k];}

			$this->form_validation->set_data( $postdata );
			$validation_rules = [
				[
					'field' => 'id_ref_keuangan[]',
					'label' => 'Kode Rekening',
					'rules' => 'required|numeric|callback_check_kode_rekening_perencanaan['.json_encode($check_data).']',
					'errors' => [
						'required' => '%s diperlukan',
						'numeric' => '%s diperlukan',
					]
				],
				[
					'field' => 'apbd_murni[]',
					'label' => 'APBD Murni',
					'rules' => 'numeric',
					'errors' => [
						'numeric' => '%s nominal tidak diperbolehkan',
					]
				],
				[
					'field' => 'apbd_perubahan[]',
					'label' => 'APBD Perubahan',
					'rules' => 'numeric',
					'errors' => [
						'numeric' => '%s nominal tidak diperbolehkan',
					]
				],
				[
					'field' => 'tahun[]',
					'label' => 'Tahun',
					'rules' => 'required',
					'errors' => [
						'required' => '%s diperlukan',
					]
				],

			];

			$this->form_validation->set_rules( $validation_rules );

			if( $this->form_validation->run() )
			{

				foreach ($postdata['id_ref_keuangan'] as $key => $value) {
					$dt = array(
						'id_ref_keuangan'	=> $postdata['id_ref_keuangan'][$key],
						'apbd_murni'		=> $postdata['apbd_murni'][$key],
						'apbd_perubahan'	=> $postdata['apbd_perubahan'][$key],
						'tahun'				=> $postdata['tahun'][$key],
					);
					$this->db->insert("keuangan_perencanaan",$dt);
					$id_ref = $this->db->insert_id();
				}

				$this->session->set_flashdata("success","Referensi berhasil disimpan");
				redirect($this->router->fetch_class()."/".$this->router->fetch_method());
			}
			else{
				$this->session->set_flashdata("danger","Referensi gagal disimpan ");
				foreach ($postdata as $key => $value) {
					$data[$key] = $value;
				}
			}
		}


		$data['ref'] = $this->keuangan_model->get_all_ref();
		$this->load->view('admin/index',$data);
	}

	function fetch_perencanaan($id){
		$data = $this->keuangan_model->select_by_id_perencanaan($id);
		echo json_encode($data);
	}

	function delete_perencanaan($id){
		if ($id) {
			$this->keuangan_model->delete_perencanaan($id);
			echo json_encode(array('status'=>true));
		}
	}

	public function sumber_realisasi_add(){
		
		$data['title']		= 'dashboard';
		$data['content']	= "keuangan/sumber_realisasi_add" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;
		$data['active_menu'] = "keuangan";


		$this->load->library('form_validation');
		if($_POST)
		{

			$html_escape = html_escape($_POST);
			$postdata = array();
			foreach($html_escape as $key=>$value)
			{
				$postdata[$key] = $this->security->xss_clean($value);
			}

			$this->form_validation->set_data( $postdata );
			$validation_rules = [
				[
					'field' => 'id_keuangan[]',
					'label' => 'Kode Rekening',
					'rules' => 'required|numeric|trim',
					'errors' => [
						'required' => '%s diperlukan',
						'numeric' => '%s diperlukan',
					]
				],
				[
					'field' => 'jenis_realisasi[]',
					'label' => 'Jenis Realisasi',
					'rules' => 'trim|required',
					'errors' => [
						'required' => '%s diperlukan',
					]
				],
				[
					'field' => 'anggaran_realisasi[]',
					'label' => 'Anggaran Realisasi',
					'rules' => 'numeric',
					'errors' => [
						'numeric' => '%s nominal tidak diperbolehkan',
					]
				],
				[
					'field' => 'tgl_realisasi[]',
					'label' => 'Tanggal Realisasi',
					'rules' => 'required',
					'errors' => [
						'required' => '%s diperlukan',
					]
				],

			];

			$this->form_validation->set_rules( $validation_rules );

			if( $this->form_validation->run() )
			{

				foreach ($postdata['id_keuangan'] as $key => $value) {
					$dt = array(
						'id_keuangan'			=> $postdata['id_keuangan'][$key],
						'jenis_realisasi'			=> $postdata['jenis_realisasi'][$key],
						'anggaran_realisasi'	=> $postdata['anggaran_realisasi'][$key],
						'tgl_realisasi'		=> date('Y-m-d',strtotime($postdata['tgl_realisasi'][$key])),
					);
					$this->db->insert("keuangan_realisasi",$dt);
					$id_ref = $this->db->insert_id();
				}

				$this->session->set_flashdata("success","Referensi berhasil disimpan");
				redirect($this->router->fetch_class()."/".$this->router->fetch_method());
			}
			else{
				$this->session->set_flashdata("danger","Referensi gagal disimpan ");
				foreach ($postdata as $key => $value) {
					$data[$key] = $value;
				}
			}
		}


		$data['perencanaan'] = $this->keuangan_model->get_all_perencanaan();
		$this->load->view('admin/index',$data);
	}

	function fetch_detail_realisasi($id){
		$query = $this->keuangan_model->select_by_id_detail_realisasi($id);
		$data['table'] = "";
		foreach ($query as $key => $value) {
			$id_realisasi 		= $value->id_realisasi;;
			$no 				= $key+1;
			$tgl_realisasi 		= tanggal($value->tgl_realisasi);
			$jenis_realisasi 	= $value->jenis_realisasi;
			$anggaran_realisasi = rupiah($value->anggaran_realisasi);

			$data['table'] .= "<tr>";
			$data['table'] .= "<td>{$no}.</td>";
			$data['table'] .= "<td>{$tgl_realisasi}</td>";
			$data['table'] .= "<td>{$jenis_realisasi}</td>";
			$data['table'] .= "<td class='text-right'>{$anggaran_realisasi}</td>";
			$data['table'] .= "<td><a href=\"javascript:editRef2('{$id_realisasi}')\" class=\"btn btn-outline-warning btn-sm\"> <span class=\"icon-pencil\"></span> Edit</a><a href=\"javascript:deleteRef2('{$id_realisasi}')\" class=\"btn btn-outline-danger btn-sm m-l-10\"> <span class=\"icon-trash\"></span> Hapus</a></td>";
			$data['table'] .= "</tr>";
		}
		echo json_encode($data);
	}

	function fetch_realisasi($id){
		$data = $this->keuangan_model->select_by_id_realisasi($id);
		// $data->tgl_realisasi = date('d F Y',strtotime($data->tgl_realisasi));
		echo json_encode($data);
	}

	function delete_realisasi($id){
		if ($id) {
			$this->keuangan_model->delete_realisasi($id);
			echo json_encode(array('status'=>true));
		}
	}


	public function ref(){
		
		$data['title']		= 'dashboard';
		$data['content']	= "keuangan/ref" ;
		$data['user_picture'] = $this->user_picture;
		$data['full_name']		= $this->full_name;
		$data['user_level']		= $this->user_level;
		$data['active_menu'] = "keuangan";


		$this->load->library('form_validation');
		if($_POST)
		{

			$html_escape = html_escape($_POST);
			$postdata = array();
			foreach($html_escape as $key=>$value)
			{
				$postdata[$key] = $this->security->xss_clean($value);
			}

			$this->form_validation->set_data( $postdata );
			$validation_rules = [
				[
					'field' => 'uraian',
					'label' => 'Uraian',
					'rules' => 'required',
					'errors' => [
						'required' => '%s diperlukan',
					]
				],
				[
					'field' => 'ket',
					'label' => 'Keterangan',
					'rules' => '',
				],

			];

			if ($postdata['save_method'] == "add") {
				$validation_rules[] = [
					'field' => 'kode_rekening',
					'label' => 'Kode Rekening',
					'rules' => 'trim|required|max_length[20]|is_unique[keuangan_ref.kode_rekening]',
					'errors' => [
						'required' => '%s diperlukan',
						'max_length' => 'Maximal 20 Char',
						'is_unique' => '%s sudah dipakai',
					]
				];
			} elseif ($postdata['save_method'] == "update") {
				if ($postdata['kode_rekening'] == $postdata['kode_rekening_old']) {
					$validation_rules[] = [
						'field' => 'kode_rekening',
						'label' => 'Kode Rekening',
						'rules' => 'trim|required|max_length[20]',
						'errors' => [
							'required' => '%s diperlukan',
							'max_length' => 'Maximal 20 Char',
							'is_unique' => '%s sudah dipakai',
						]
					];
				} else {
					$validation_rules[] = [
						'field' => 'kode_rekening',
						'label' => 'Kode Rekening',
						'rules' => 'trim|required|max_length[20]|is_unique[keuangan_ref.kode_rekening]',
						'errors' => [
							'required' => '%s diperlukan',
							'max_length' => 'Maximal 20 Char',
							'is_unique' => '%s sudah dipakai',
						]
					];
				}
				$validation_rules[] = [
					'field' => 'id_ref_keuangan',
					'rules' => 'trim|required|numeric',
				];
			}

			$this->form_validation->set_rules( $validation_rules );

			if( $this->form_validation->run() )
			{

				$dt = array(
					'kode_rekening'		=> $postdata['kode_rekening'],
					'uraian'			=> $postdata['uraian'],
					'ket'				=> $postdata['ket'],
				);
					//echo "<pre>";print_r($dt);die;
				if ($postdata['save_method'] == "add") {
					$this->db->insert("keuangan_ref",$dt);
					$id_ref = $this->db->insert_id();
				} elseif ($postdata['save_method'] == "update") {
					$this->db->where('id_ref_keuangan', $postdata['id_ref_keuangan']);
					$this->db->update("keuangan_ref",$dt);
					$id_ref = $this->db->insert_id();
				}

				$this->session->set_flashdata("success","Referensi berhasil disimpan");
				redirect($this->router->fetch_class()."/".$this->router->fetch_method());
			}
			else{
				$this->session->set_flashdata("danger","Referensi gagal disimpan ");
			}
		}

		$data['query'] = $this->keuangan_model->get_all_ref();


		$this->load->view('admin/index',$data);
	}

	function fetch_ref($id){
		$data = $this->keuangan_model->select_by_id_ref($id);
		echo json_encode($data);
	}

	function delete_ref($id){
		if ($id) {
			$this->keuangan_model->delete_ref($id);
			echo json_encode(array('status'=>true));
		}
	}

	function check_kode_rekening() {
		$first_name = $this->input->post('firstname');// get fiest name
		$last_name = $this->input->post('lastname');// get last name
		$this->db->select('user_id');
		$this->db->from('student');
		$this->db->where('firstname', $first_name);
		$this->db->where('lastname', $lase_name);
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	function check_kode_rekening_perencanaan($key,$check_data) {
		$check_data = json_decode($check_data,true);
		$return = TRUE;
		if ($key > 0) {
			foreach ($check_data[$key] as $value) {
				$this->db->select('id_keuangan');
				$this->db->from('keuangan_perencanaan');
				$this->db->where('id_ref_keuangan', $key);
				$this->db->where('tahun', $value);
				$query = $this->db->get();
				$num = $query->num_rows();
				$count = array_count_values($check_data[$key]);
				if (($num > 0 OR $count[$value] > 1)) {
					$query = $this->keuangan_model->select_by_id_ref($key);
					$this->form_validation->set_message('check_kode_rekening_perencanaan', "%s <b>{$query->kode_rekening} {$query->uraian} ({$value})</b> sudah dipakai");
					$return = FALSE;
				} 
			}
		} else {
			$this->form_validation->set_message('check_kode_rekening_perencanaan', "%s belum dipilih");
			$return = FALSE;
		}
		return $return;
	}



}
