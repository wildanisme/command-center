<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
	}

	public function index()
	{
		$this->load->model('contact_model');
		$this->load->library('form_validation');
		$this->load->model('company_profile_model');
		$this->company_profile_model->set_identity();
		$data['nama']	= $this->company_profile_model->nama;
		$data['alamat']	= $this->company_profile_model->alamat;
		$data['email']	= $this->company_profile_model->email;
		$data['telepon']	= $this->company_profile_model->telepon;
		$validation = $this->form_validation;
		if(!empty($_POST)){
			$validation->set_rules('name','Name','required|min_length[3]|xss_clean');
			$validation->set_rules('subject','Subject','required|min_length[5]|xss_clean');
			$validation->set_rules('message','Message','required|min_length[5]|xss_clean');
			$validation->set_rules('email','Email','required|valid_email|xss_clean');
			if ($validation->run() == true)
			{
			$this->contact_model->name= $_POST['name'];
			$this->contact_model->email= $_POST['email'];
			$this->contact_model->subject= $_POST['subject'];
			$this->contact_model->message= $_POST['message'];	
			}
			
		}

		$this->load->view('blog/contact',$data);
	}

	public function insert_contact()
	{
		$this->load->model('contact_model');
		$this->load->library('form_validation');
		$validation = $this->form_validation;
			$validation->set_rules('subject','Subject','required|min_length[5]');
			$validation->set_rules('message','Message','required|min_length[5]');
			if ($validation->run() == true)
			{
			$this->contact_model->name= $this->session->userdata('full_name');
			$this->contact_model->email= $this->session->userdata('email');
			$this->contact_model->subject= $_POST['subject'];
			$this->contact_model->message= $_POST['message'];	
			$query = $this->contact_model->insert();
			redirect(base_url('contact?status=sent'));
			}
			else {
				$this->load->model('company_profile_model');
				$this->company_profile_model->set_identity();
				$data['nama']	= $this->company_profile_model->nama;
				$data['alamat']	= $this->company_profile_model->alamat;
				$data['email']	= $this->company_profile_model->email;
				$data['telepon']	= $this->company_profile_model->telepon;
				$this->load->view('blog/contact',$data);
			}
		
	}
	
}
?>