<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_Pegawai extends CI_Controller {
	public $user_id;
	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->load->model('pegawai_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		$this->level_id	= $this->user_model->level_id;
		$this->kd_skpd	= $this->user_model->kd_skpd;
		$this->default_data = array(
					'nip_lama' =>'',
					'nip_baru' =>'',
					'karpeg' =>'',
					'id_gelardepan' =>'',
					'nama_lengkap' =>'',
					'id_gelarbelakang' =>'',
					'tgl_lahir' =>'',
					'tempat_lahir' =>'',
					'id_agama' =>'',
					'jenis_kelamin' =>'1',
					'kedudukan_pegawai' =>'',
					'status_pegawai' =>'1',
					'alamat' =>'',
					'RT' =>'',
					'RW' =>'',
					'id_desa' =>'',
					'id_kecamatan' =>'',
					'id_kabupaten' =>'',
					'id_provinsi' =>'',
					'kode_pos' =>'',
					'telepon' =>'',
					'kartu_askes' =>'',
					'kartu_taspen' =>'',
					'karis_karsu' =>'',
					'npwp' =>'',
					'id_statusmenikah' =>'',
					'jml_tanggungan_anak' =>'',
					'jml_seluruh_anak' =>'',
					'kabupaten' => '',
					'kecamatan' => '',
					'desa' => '',
					'cpns_tmt' => '',
					'cpns_no_sk' => '',
					'cpns_no_bakn' => '',
					'cpns_pejabat' => '',
					'cpns_tahun_pendidikan' => '',
					'pns_tmt' => '',
					'pns_pejabat' => '',
					'pns_no_sk' => '',
					'foto' => 'user_default.png',
					'kd_skpd' => $this->kd_skpd
				);
		$this->arrStatus = array('belum diverifikasi','proses verifikasi','aktif','non aktif');
		$this->arrJK= array('Laki -Laki','Perempuan');
		$this->arrStatusRiwayat = array(
			0 => 'Belum diverifikasi',
			1 => 'Sudah diverifikasi',
			2 => 'Ditolak'
		);
		$this->max_file_size = 2000; // dalam kB
	}
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Master_Pegawai - Admin ";
			$data['content']	= "master_pegawai/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu']		= 'master_pegawai';
			$this->pegawai_model->kd_skpd = $this->kd_skpd;
			
			if (!empty($_POST)){
				// $this->pegawai_model->nip = $_POST['nip'];
				$this->pegawai_model->nama_lengkap = $_POST['nama_lengkap'];
				$this->pegawai_model->id_unit_kerja = $_POST['id_unit_kerja'];
				// $this->pegawai_model->id_jabatan = $_POST['id_jabatan'];
				$data = array_merge($data,$_POST);
				
			}
			else{
				$data['nip_baru'] = "";
				$data['nama_lengkap']="";
				$data['kd_skpd'] = 0;
				$data['status'] ="";
				$data['jenis_kelamin']=0;
			}
			$offset = 0;
			$data['per_page']	= 9;
			if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
			
			$data['query']		= $this->pegawai_model->get_for_page($data['per_page'],$offset);
			
			$data['total_rows']	= count($this->pegawai_model->get_for_page());
			$data['offset']	= $offset;
			
			$data['arrStatus'] = $this->arrStatus;
			$data['arrJK'] = $this->arrJK;
			
			$this->load->model('skpd_model');
			$this->skpd_model->kd_skpd = $this->kd_skpd;
			$skpd_all = $this->skpd_model->get_all();
			$data['all_skpd'] = array();
			foreach ($skpd_all as $row)
			{
				$data['all_skpd'][$row->kd_skpd] = $row->nama_skpd;
			}
			$data['skpd'] = $this->skpd_model->get_skpd_by_induk(0);
			
			$this->load->model('ref_unit_kerja_model');
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			
			$this->load->model('ref_jabatan_model');
			$data['jabatan'] = $this->ref_jabatan_model->get_all();
			
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function get_kabupaten($parent_id){
		$obj = '<option value="">Pilih</option>';
		$this->load->model('ref_wilayah_model');
		$id = explode("-",$parent_id);
		$data = $this->ref_wilayah_model->get_kabupaten(null,$id[0]);
		foreach($data as $row){
			$obj .= "<option value='".$row->id_kabupaten."-".$row->kabupaten."'>$row->kabupaten</option>";
		}
		die ($obj);
	}
	public function get_kecamatan($parent_id){
		$obj = '<option value="">Pilih</option>';
		$this->load->model('ref_wilayah_model');
		$id = explode("-",$parent_id);
		$data = $this->ref_wilayah_model->get_kecamatan(null,$id[0]);
		foreach($data as $row){
			$obj .= "<option value='".$row->id_kecamatan."-".$row->kecamatan."'>$row->kecamatan</option>";
		}
		die ($obj);
	}
	public function get_desa($parent_id){
		$obj = '<option value="">Pilih</option>';
		$this->load->model('ref_wilayah_model');
		$id = explode("-",$parent_id);
		$data = $this->ref_wilayah_model->get_desa(null,$id[0]);
		foreach($data as $row){
			$obj .= "<option value='".$row->id_desa."-".$row->desa."'>$row->desa</option>";
		}
		die ($obj);
	}


	public function get_level_jabatan($id_unit_kerja){
		$obj = '<option value="">Pilih</option>';
		$this->load->model('ref_jabatan_model');
		$data = $this->ref_jabatan_model->arr_level_jabatan;
		for($i=1; $i <= count($data); $i++)
		{
			$obj .= "<option value='".$i."'>$data[$i]</option>";
		}
		die ($obj);
	}


	public function get_jabatan($id_unit_kerja, $level_jabatan){
		$obj = '<option value="">Pilih</option>';
		$this->load->model('ref_jabatan_model');
		$data = $this->ref_jabatan_model->get_all($level_jabatan,null,$id_unit_kerja);
		foreach($data as $row){
			$obj .= "<option value='".$row->id_jabatan."'>$row->nama_jabatan</option>";
		}
		die ($obj);
	}
	
	public function add()
	{
		if ($this->user_id)
		{
			
			$data['title']		= "Master Pegawai - Admin ";
			$data['content']	= "master_pegawai/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['active_menu']		= 'master_pegawai';
			$data['user_level']		= $this->user_level;
			
			$this->load->model('ref_gelardepan_model');
			$data['gelardepan'] = $this->ref_gelardepan_model->get_all();
			
			$this->load->model('ref_gelarbelakang_model');
			$data['gelarbelakang'] = $this->ref_gelarbelakang_model->get_all();
			
			$this->load->model('ref_agama_model');
			$data['agama'] = $this->ref_agama_model->get_all();
			
			$this->load->model('ref_wilayah_model');
			$data['provinsi'] = $this->ref_wilayah_model->get_provinsi();
			
			$this->load->model('ref_unit_kerja_model');
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			
			$this->load->model('ref_jabatan_model');
			$data['jabatan'] = $this->ref_jabatan_model->get_all();

			$this->load->model('ref_statusmenikah_model');
			$data['statusmenikah'] = $this->ref_statusmenikah_model->get_all();
			$data['jenjangpendidikan'] = $this->pegawai_model->get_jenjangpendidikan();
			$data['golongan'] = $this->pegawai_model->get_golongan();
			$data['max_size']             = 1000;
			$data['max_width']            = 500;
			$data['max_height']           = 500;
			if (!empty($_POST)){					
				if (
					$_POST['nip']!='' && 
					$_POST['nama_lengkap']!='' && 
					$_POST['id_unit_kerja']!='' && 
					$_POST['id_jabatan']!='' && 
					$_POST['eselon']!=''
				){
					$avaliable = $this->pegawai_model->cek_nip($_POST['nip'])	;
					if ($avaliable){
						$config['upload_path']          = './data/user_picture/';
			            $config['allowed_types']        = 'jpg|png';
			            $config['max_size']             = $data['max_size'];
			            $config['max_width']            = $data['max_width'];
			            $config['max_height']           = $data['max_height'];

			            $this->load->library('upload', $config);
			            if ( ! $this->upload->do_upload())
		                {
		                    $_POST['foto'] 	= "user_default.png";
		                    $tmp_name				= $_FILES['userfile']['tmp_name'];
		                    if ($tmp_name!="")
		                    {
		                    	$data['error'] 		= true;
		                    	$data['message'] 	= "Foto : ". $this->upload->display_errors();
		                    }
		                }
		                else
		                {
		                	$_POST['foto'] = $this->upload->data('file_name');
		                }
						if (empty($data['error'])){
							// $_POST['status'] = "aktif";
							// $_POST['kd_skpd'] = $this->kd_skpd;
							$this->pegawai_model->insert($_POST);
							$data['message'] = "Tambah data berhasil";
							$data = array_merge($data,$this->default_data);
						}
						else{
							$data = array_merge($data,$_POST);
						}
					}
					else{
						$data['error'] = true;
		                $data['message']= "NIP tidak tersedia";
						$data = array_merge($data,$_POST);
					}
				}
				else{
					$data['error'] = true;
					$data['message'] = "Data belum lengkap.";
					$data = array_merge($data,$_POST);
				}

			}
			else{
				
				$data = array_merge($data,$this->default_data);
			}
			
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}
	
	public function getFromMaster(){
		$nip_master = $_POST['nip_master'];
		$data = $this->pegawai_model->getFromMaster($nip_master);
		print(json_encode($data));
	}
	public function get_pegawai_for_pengajuan(){
		$nip_baru = $_POST['nip_baru'];
		$data = $this->pegawai_model->get_pegawai_for_pengajuan($nip_baru);
		print(json_encode($data));
	}
	public function view($id_pegawai=0)
	{
		if ($this->user_id && !empty($id_pegawai) && $id_pegawai>0)
		{
			
			$data['title']		= "Master Pegawai - Admin ";
			$data['content']	= "master_pegawai/view" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu']		= 'master_pegawai';
			$data['level_id']		= $this->level_id;
			
			$result = $this->pegawai_model->get($id_pegawai);
			$this->load->model('ref_wilayah_model');
			$data['kabupaten'] = $this->ref_wilayah_model->get_kabupaten($result[0]['id_kabupaten']);
			$data['kecamatan'] = $this->ref_wilayah_model->get_kecamatan($result[0]['id_kecamatan']);
			$data['desa'] = $this->ref_wilayah_model->get_desa($result[0]['id_desa']);
			$data = array_merge($data,$result[0]);
			$data['arrStatusRiwayat'] = $this->arrStatusRiwayat;
			$data['max_file_size'] = $this->max_file_size;
			$data['golongan'] = $this->pegawai_model->get_golongan();
			$this->load->model('ref_jabatan_model');
			$data['jabatan'] = $this->ref_jabatan_model->getAll(1);
			$data['jab_level1'] = $this->ref_jabatan_model->get_all(1);
			$data['arr_jenis_jabatan'] = $this->ref_jabatan_model->arr_jenis_jabatan;
			$data['eselon'] = $this->ref_jabatan_model->get_eselon();
			
			$arrJab = $this->ref_jabatan_model->get_all();
			$data['arrJab'] = array();
			foreach($arrJab as $row){
				$data['arrJab'][$row->id_jabatan] = $row->nama_jabatan;
			}
			//var_dump($data['arrJab']);die;
			$data['jenjangpendidikan'] = $this->pegawai_model->get_jenjangpendidikan();
			//$data['tempatpendidikan'] = $this->pegawai_model->get_tempatpendidikan();
			//var_dump($data['tempatpendidikan']);die;
			$data['jurusan'] = $this->pegawai_model->get_jurusan();
			$data['jenisdiklat'] = $this->pegawai_model->get_jenisdiklat();
			
			$data['skpd'] = $this->pegawai_model->get_skpd();
			//var_dump($data['skpd']);die;
			$data['jenispenghargaan'] = $this->pegawai_model->get_jenispenghargaan();
			$data['jeniscuti'] = $this->pegawai_model->get_jeniscuti();
			
			$data['data_pangkat'] = $this->pegawai_model->get_riwayat_pangkat($id_pegawai);
			$data['data_jabatan'] = $this->pegawai_model->get_riwayat_jabatan($id_pegawai);
			$data['data_pendidikan'] = $this->pegawai_model->get_riwayat_pendidikan($id_pegawai);
			$data['data_diklat'] = $this->pegawai_model->get_riwayat_diklat($id_pegawai);
			$data['data_unit_kerja'] = $this->pegawai_model->get_riwayat_unit_kerja($id_pegawai);
			$data['data_penghargaan'] = $this->pegawai_model->get_riwayat_penghargaan($id_pegawai);
			$data['data_cuti'] = $this->pegawai_model->get_riwayat_cuti($id_pegawai);
			
			$data['pendidikan_last'] = $this->pegawai_model->get_riwayat_pendidikan($id_pegawai,1);
			$data['pangkat_last'] = $this->pegawai_model->get_riwayat_pangkat($id_pegawai,1);
			$data['jabatan_last'] = $this->pegawai_model->get_riwayat_jabatan($id_pegawai,1);
			$data['unit_kerja_last'] = $this->pegawai_model->get_riwayat_unit_kerja($id_pegawai,1);

			$data['registered'] = $this->pegawai_model->cek_user($id_pegawai);
			$this->load->model('user_model');
			$this->user_model->id_pegawai = $id_pegawai;
			$data['detail_user'] = $this->user_model->get_by_pegawai();
			
			$this->load->model('skpd_model');
			$data['skpd_level1'] = $this->skpd_model->get_skpd_by_induk(0);
			
			$this->load->model('ref_pendidikan_model');
			$data['arr_level_pendidikan'] = $this->ref_pendidikan_model->arr_level;
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}

	public function edit($id_pegawai=0)
	{
		if ($this->user_id && !empty($id_pegawai) && $id_pegawai>0)
		{
			
			$data['title']		= "Master Pegawai - Admin ";
			$data['content']	= "master_pegawai/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu']		= 'master_pegawai';
			
			$this->load->model('ref_gelardepan_model');
			$data['gelardepan'] = $this->ref_gelardepan_model->get_all();
			
			$this->load->model('ref_gelarbelakang_model');
			$data['gelarbelakang'] = $this->ref_gelarbelakang_model->get_all();
			
			$this->load->model('ref_agama_model');
			$data['agama'] = $this->ref_agama_model->get_all();
			
			$this->load->model('ref_wilayah_model');
			$data['arrProvinsi'] = $this->ref_wilayah_model->get_provinsi();
			
			$this->load->model('ref_statusmenikah_model');
			$data['statusmenikah'] = $this->ref_statusmenikah_model->get_all();
			$data['jenjangpendidikan'] = $this->pegawai_model->get_jenjangpendidikan();
			$data['golongan'] = $this->pegawai_model->get_golongan();
			$data['max_size']             = 1000;
			$data['max_width']            = 500;
			$data['max_height']           = 500;
			$result = $this->pegawai_model->get($id_pegawai);
			if (!empty($_POST)){					
				if (
					$_POST['nip']!='' && 
					$_POST['nama_lengkap']!='' && 
					$_POST['id_unit_kerja']!='' && 
					$_POST['id_jabatan']!='' && 
					$_POST['eselon']!=''
				)
				{
					$nip_lama = $result[0]['nip'];
					$avaliable = $_POST['nip']==$nip_lama ? true : $this->pegawai_model->cek_nip($_POST['nip']);
					if ($avaliable){
						$config['upload_path']          = './data/user_picture/';
			            $config['allowed_types']        = 'jpg|png';
			            $config['max_size']             = $data['max_size'];
			            $config['max_width']            = $data['max_width'];
			            $config['max_height']           = $data['max_height'];

			            $this->load->library('upload', $config);
			            if ( ! $this->upload->do_upload())
		                {
		                    $tmp_name				= $_FILES['userfile']['tmp_name'];
		                    if ($tmp_name!="")
		                    {
		                    	$data['error'] 		= true;
		                    	$data['message'] 	= "Foto : ". $this->upload->display_errors();
		                    }
		                }
		                else
		                {
							if ($_POST['foto']!="user_default.png") unlink('./data/user_picture/'.$_POST['foto']);
		                	$_POST['foto'] = $this->upload->data('file_name');
		                }
						if (empty($data['error'])){
							$_POST['status'] = "aktif";
							// $_POST['kd_skpd'] = $this->kd_skpd;
							// unset($_POST['nip_old']);
							$this->pegawai_model->update($_POST,$id_pegawai);
							$data['message'] = "Update data berhasil";
							$result = $this->pegawai_model->get($id_pegawai);
							$data['kabupaten'] = $this->ref_wilayah_model->get_kabupaten($result[0]['id_kabupaten']);
							$data['kecamatan'] = $this->ref_wilayah_model->get_kecamatan($result[0]['id_kecamatan']);
							$data['desa'] = $this->ref_wilayah_model->get_desa($result[0]['id_desa']);
							$data = array_merge($data,$result[0]);
						}
						else{
							$data = array_merge($data,$_POST);
						}
					}
					else{
						$data['error'] = true;
		                $data['message']= "NIP tidak tersedia";
						$data = array_merge($data,$_POST);
					}
				}
				else{
					$data['error'] = true;
					$data['message'] = "Data belum lengkap.";
					if ($_POST['id_kabupaten']!=""){
						$kabupaten = explode("-",$_POST['id_kabupaten']);
						$_POST['id_kabupaten'] = $kabupaten[0];
						$data['kabupaten'] = $kabupaten[1];
					}
					if ($_POST['id_kecamatan']!=""){
						$kecamatan = explode("-",$_POST['id_kecamatan']);
						$_POST['id_kecamatan'] = $kecamatan[0];
						$data['kecamatan'] = $kecamatan[1];
					}
					if ($_POST['id_desa']!=""){
						$desa = explode("-",$_POST['id_desa']);
						$_POST['id_desa'] = $desa[0];
						$data['desa'] = $desa[1];
					}
					$data = array_merge($data,$_POST);
				}

			}
			else{
				$result = $this->pegawai_model->get($id_pegawai);
				
				$data['kabupaten'] = $this->ref_wilayah_model->get_kabupaten($result[0]['id_kabupaten']);
				$data['kecamatan'] = $this->ref_wilayah_model->get_kecamatan($result[0]['id_kecamatan']);
				$data['desa'] = $this->ref_wilayah_model->get_desa($result[0]['id_desa']);
				$data = array_merge($data,$result[0]);
				// print_r($result);die;
			}
			$this->load->model('ref_unit_kerja_model');
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			
			$this->load->model('ref_jabatan_model');
			$data['jabatan'] = $this->ref_jabatan_model->get_all();
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}
	public function tambah_riwayat_pangkat()
	{
		$data = array(
			'id_pegawai'	=> $_POST['id_pegawai'],
			'id_golongan'	=> $_POST['id_golongan'],
			'tmt_berlaku'	=> $_POST['tmt_berlaku'],
			'gaji_pokok'	=> $_POST['gaji_pokok'],
			'nama_pejabat'	=> $_POST['nama_pejabat'],
			'no_sk'			=> $_POST['no_sk'],
			'tgl_sk'		=> $_POST['tgl_sk'],
			'status'		=> '1'
		);
		$this->pegawai_model->tambah_riwayat_pangkat($data);
		die('success');
	}
	public function tambah_riwayat_jabatan()
	{
		$data = array(
			'id_pegawai'	=> $_POST['id_pegawai'],
			'id_jabatan'	=> $_POST['id_jabatan'],
			'id_golongan'	=> $_POST['id_golongan'],
			'tgl_mulai'		=> $_POST['tgl_mulai'],
			'tgl_akhir'		=> $_POST['tgl_akhir'],
			'gaji_pokok'	=> $_POST['gaji_pokok'],
			'nama_pejabat'	=> $_POST['nama_pejabat'],
			'no_sk'			=> $_POST['no_sk'],
			'tgl_sk'		=> $_POST['tgl_sk'],
			'id_eselon'		=> $_POST['id_eselon'],
			'status'		=> '1'
		);
		$this->pegawai_model->tambah_riwayat_jabatan($data);
		die('success');
	}
	public function tambah_riwayat_pendidikan()
	{
		$data = array(
			'id_pegawai'=>$_POST['id_pegawai'],
			'id_jenjangpendidikan'=>$_POST['id_jenjangpendidikan'],
			'id_tempatpendidikan'=>$_POST['id_tempatpendidikan'],
			'id_jurusan'=>$_POST['id_jurusan'],
			'nama_pejabat'=>$_POST['nama_pejabat'],
			'nomor_sk'=>$_POST['no_sk'],
			'tgl_sk'=>$_POST['tgl_sk'],
			'status'		=> '1'
		);
		
		$this->pegawai_model->tambah_riwayat_pendidikan($data);
		die('success');
	}
	public function tambah_riwayat_diklat()
	{
		$data = array(
			'id_pegawai'=>$_POST['id_pegawai'],
			'id_jenisdiklat'=>$_POST['id_jenisdiklat'],
			'nama_diklat'=>$_POST['nama_diklat'],
			'tempat'=>$_POST['tempat'],
			'penyelenggara'=>$_POST['penyelenggara'],
			'angkatan'=>$_POST['angkatan'],
			'tgl_mulai'=>$_POST['tgl_mulai'],
			'tgl_akhir'=>$_POST['tgl_akhir'],
			'no_sptl'=>$_POST['no_sptl'],
			'tgl_sptl'=>$_POST['tgl_sptl'],
			'status'		=> '1'
		);
		//die(json_encode($data));
		$this->pegawai_model->tambah_riwayat_diklat($data);
		die('success');
	}
	public function tambah_riwayat_unit_kerja()
	{
		$data = array(
			'id_pegawai'=>$_POST['id_pegawai'],
			'id_unit_kerja'=>$_POST['id_unit_kerja'],
			'tmt_awal'=>$_POST['tmt_awal'],
			'tmt_akhir'=>$_POST['tmt_akhir'],
			'no_sk_awal'=>$_POST['no_sk_awal'],
			'no_sk_akhir'=>$_POST['no_sk_akhir'],
			'status'		=> '1'
		);
		$this->pegawai_model->tambah_riwayat_unit_kerja($data);
		die('success');
	}
	public function tambah_riwayat_penghargaan()
	{
		$data = array(
			'id_pegawai'=>$_POST['id_pegawai'],
			'id_jenispenghargaan'=>$_POST['id_jenispenghargaan'],
			'nama_penghargaan'=>$_POST['nama_penghargaan'],
			'tahun'=>$_POST['tahun'],
			'asal_perolehan'=>$_POST['asal_perolehan'],
			'penandatangan'=>$_POST['penandatangan'],
			'no_penghargaan'=>$_POST['no_penghargaan'],
			'tgl_penghargaan'=>$_POST['tgl_penghargaan'],
			'status'		=> '1'
		);
		$this->pegawai_model->tambah_riwayat_penghargaan($data);
		die('success');
	}
	public function tambah_riwayat_cuti()
	{
		$data = array(
			'id_pegawai'=>$_POST['id_pegawai'],
			'id_jeniscuti'=>$_POST['id_jeniscuti'],
			'keterangan'=>$_POST['keterangan'],
			'pejabat_penetapan'=>$_POST['pejabat_penetapan'],
			'no_sk'=>$_POST['no_sk'],
			'tgl_sk'=>$_POST['tgl_sk'],
			'tgl_awal_cuti'=>$_POST['tgl_awal_cuti'],
			'tgl_akhir_cuti'=>$_POST['tgl_akhir_cuti'],
			'status'		=> '1'
		);
		$this->pegawai_model->tambah_riwayat_cuti($data);
		die('success');
	}
	public function verifikasi()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->set_status($_POST['id_pegawai'],"aktif");
			die("success");
		}
		else{
			die("error");
		}
	}
	public function proses()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->set_status($_POST['id_pegawai'],"proses verifikasi");
			die("success");
		}
		else{
			die("error");
		}
	}
	public function nonaktif()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->set_status($_POST['id_pegawai'],"non aktif");
			die("success");
		}
		else{
			die("error");
		}
	}
	public function aktif()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->set_status($_POST['id_pegawai'],"aktif");
			die("success");
		}
		else{
			die("error");
		}
	}
	public function ubah_status()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->ubah_status_riwayat("riwayat_".$_POST['data'],$_POST['status'],$_POST['id']);
			$fungsi = "get_riwayat_".$_POST['data'];
			$data = $this->pegawai_model->$fungsi($_POST['id_pegawai']);
			$response = $this->update_data($_POST['data'],$data);
			die($response);
		}
	}
	public function hapus_riwayat()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$this->pegawai_model->delete_riwayat("riwayat_".$_POST['data'],$_POST['id'],$_POST['berkas']);
			$fungsi = "get_riwayat_".$_POST['data'];
			$data = $this->pegawai_model->$fungsi($_POST['id_pegawai']);
			$response = $this->update_data($_POST['data'],$data);
			die($response);
		}
	}
	public function get_data_riwayat()
	{
		if (!empty($_POST['id_pegawai']) && $_POST['id_pegawai']>0)
		{
			$fungsi = "get_riwayat_".$_POST['data'];
			$data = $this->pegawai_model->$fungsi($_POST['id_pegawai']);
			$response = $this->update_data($_POST['data'],$data);
			die($response);
		}
		else{
			die("error");
		}
	}
	public function update_data($jenis,$data){
		$arrStatusRiwayat = $this->arrStatusRiwayat;
		if ($jenis=="pangkat"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
								foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
									$html_.="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->golongan</td>
                                  <td>$row->pangkat</td>
                                  <td>".date("d M Y",strtotime($row->tmt_berlaku))."</td>
                                  <td>".number_format($row->gaji_pokok)."</td>
                                  <td>$row->nama_pejabat</td>
                                  <td>$row->no_sk</td>
                                  <td>".date("d M Y",strtotime($row->tgl_sk))."</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="jabatan"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
								foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
						if ($row->tgl_akhir=="9999-12-31")
										$tgl_akhir = "-";
									else 
										$tgl_akhir = date("d M Y",strtotime($row->tgl_akhir));
									$html_.="
									
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_jabatan</td>
                                  <td>".date("d M Y",strtotime($row->tgl_mulai))."</td>
								  <td>$tgl_akhir</td>
								  <td>$row->pangkat</td>
                                  <td>".number_format($row->gaji_pokok)."</td>
                                  <td>$row->nama_pejabat</td>
                                  <td>$row->no_sk</td>
                                  <td>".date("d M Y",strtotime($row->tgl_sk))."</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="pendidikan"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
								foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
						$html_ .="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_jenjangpendidikan</td>
								  <td>$row->nama_tempatpendidikan</td>
								  <td>$row->nama_jurusan</td>
								  <td>$row->nama_pejabat</td>
								  <td>$row->nomor_sk</td>
                                  <td>".date("d M Y",strtotime($row->tgl_sk))."</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="diklat"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
							foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
									$html_.="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_jenisdiklat</td>
								  <td>$row->nama_diklat</td>
								  <td>$row->tempat</td>
								  <td>$row->penyelenggara</td>
								  <td>$row->angkatan</td>
                                  <td>".date("d M Y",strtotime($row->tgl_mulai))."</td>
								  <td>".date("d M Y",strtotime($row->tgl_akhir))."</td>
								  <td>$row->no_sptl</td>
                                  <td>".date("Y",strtotime($row->tgl_sptl))."</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_."  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="unit_kerja"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
							foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
									$html_.="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_skpd</td>
                                  <td>".date("d M Y",strtotime($row->tmt_awal))."</td>
								  <td>".date("d M Y",strtotime($row->tmt_akhir))."</td>
								  <td>$row->no_sk_awal</td>
                                  <td>$row->no_sk_akhir</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="penghargaan"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
							foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
									$html_.="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_jenispenghargaan</td>
                                  <td>$row->nama_penghargaan</td>
								  <td>$row->tahun</td>
								  <td>$row->asal_perolehan</td>
                                  <td>$row->penandatangan</td>
								  <td>$row->no_penghargaan</td>
								  <td>$row->tgl_penghargaan</td>
                                  <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		else if ($jenis=="cuti"){
			$no=1;
			$level_id = $this->level_id;
			$html_="";
							foreach($data as $row){
									if ($row->status==0)
										$tipe = "warning";
									else if ($row->status==1)
										$tipe = "success";
									else 
										$tipe = "danger";
									$html_.="
                               <tr>
                                  <td>$no</td>
                                  <td>$row->nama_jeniscuti</td>
                                  <td>$row->keterangan</td>
								  <td>$row->pejabat_penetapan</td>
								  <td>$row->no_sk</td>
								  <td>".date("d M Y",strtotime($row->tgl_sk))."</td>
                                  <td>".date("d M Y",strtotime($row->tgl_awal_cuti))."</td>
								  <td>".date("d M Y",strtotime($row->tgl_akhir_cuti))."</td>
                                 <td>
								  <span class='label label-$tipe'>".$arrStatusRiwayat[$row->status]."</span></td>";
									if ($level_id!=4){ // bukan pegawai
										$btn_aksi = "";
										if ($row->berkas!=""){
											if ($row->status==0){
												$btn_aksi="
													<button class='btn btn-success btn-xs' 
													onclick='ubah_status(\"$jenis\",1,$row->id)'>Verifikasi</button>
													<button class='btn btn-danger btn-xs' 
													onclick='ubah_status(\"$jenis\",2,$row->id)'>Tolak</button>
												";
											}
											else if ($row->status==1){
												$btn_aksi="
													<button class='btn btn-warning btn-xs' 
													onclick='ubah_status(\"$jenis\",0,$row->id)'>Batalkan verifikasi</button>
												";
											}
											$btn_aksi .= '<a href="'.base_url().'data/upload_berkas/'.$row->berkas.'" type="button" class="btn btn-default btn-xs" target=_blank>Download berkas</a>';
										}
										else{
											$btn_aksi .= '<button type="button" class="btn btn-default btn-xs" onclick="showUploadForm('.$row->id_pegawai.','.$row->id.',\''.$jenis.'\')" data-toggle="modal" data-target=".bs-example-modal-md-upload">Upload berkas</button>';
										}
										if ($level_id!=3){//bukan petugas opd
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										else if ($level_id==3 && $row->status==0){
											$btn_aksi .= "
												<button class='btn btn-danger btn-xs' 
												onclick='hapus(\"$jenis\",$row->id,\"$row->berkas\")'>Hapus</button>
											";
										}
										$html_.= '<td>'.$btn_aksi.'
										
										</td>';
									}
								$html_.="  
                                </tr>
								";$no++;}
		}
		return $html_;
	}
	
	
	public function cetak($id_pegawai=0)
	{
		if ($this->user_id && $id_pegawai>0)
		{	
			$data = array();
			$pegawai = $this->pegawai_model->get($id_pegawai);
			$pendidikan = $this->pegawai_model->get_riwayat_pendidikan($id_pegawai,1);
			$pangkat = $this->pegawai_model->get_riwayat_pangkat($id_pegawai,1);
			$jabatan = $this->pegawai_model->get_riwayat_jabatan($id_pegawai,1);
			$unit_kerja = $this->pegawai_model->get_riwayat_unit_kerja($id_pegawai,1);
			$diklat = $this->pegawai_model->get_riwayat_diklat($id_pegawai,1);
			$penghargaan = $this->pegawai_model->get_riwayat_penghargaan($id_pegawai,1);
			$cuti = $this->pegawai_model->get_riwayat_cuti($id_pegawai,1);
			$data['pegawai'] = $pegawai[0];
			$data['pendidikan']= $pendidikan;
			$data['pangkat']= $pangkat;
			$data['jabatan']= $jabatan;
			$data['unit_kerja']= $unit_kerja;
			$data['diklat']= $diklat;
			$data['penghargaan']= $penghargaan;
			$data['cuti']= $cuti;
			//var_dump($pendidikan);die;
			$this->load->view('admin/master_pegawai/cetak',$data);
		}
		else
		{
			redirect('admin');
		}
	}
	
	
	
	
	
	
	
	
	public function delete_pegawai($id){
		if ($this->user_id)
		{
		$this->pegawai_model->delete_pegawai($id);
        echo json_encode(array("status" => TRUE));

		}
		else
		{
			redirect('home');
		}

	}
	
	
	
	public function delete($id)
	{
		if ($this->user_id)
		{
			$this->skpd_model->kd_skpd = $id;
			$this->skpd_model->delete();
			redirect('manage_skpd');
		}
		else
		{
			redirect('home');
		}
	}
	
	
	function upload_berkas(){
		
		$allowedExts = array("rar", "zip","pdf","jpg","jpeg");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		if (($_FILES["file"]["type"] == "application/x-zip-compressed")
		|| ($_FILES["file"]["type"] == "application/octet-stream") || ($_FILES["file"]["type"] == "application/pdf")
		&& ($_FILES["file"]["size"] < ($this->max_file_size * 1024))
		&& in_array($extension, $allowedExts)) {
			if ($_FILES["file"]["error"] > 0) {
				echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
			} else {
				//$filename = $label.$_FILES["file"]["name"];
				$filename = $_POST['jenis_riwayat_upload']."_".$_POST['id_pegawai_upload']."_".$_POST['id_riwayat_upload'].".".$extension;
				//echo "Upload: " . $_FILES["file"]["name"] . "<br>";
				//echo "Type: " . $_FILES["file"]["type"] . "<br>";
				//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
				//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

				if (file_exists('./data/upload_berkas/' . $filename)) {
					echo $filename . " already exists. ";
				} else {
					move_uploaded_file($_FILES["file"]["tmp_name"],
					'./data/upload_berkas/' . $filename);
					echo "Upload berhasil";
					$this->pegawai_model->updateBerkasRiwayat($_POST['jenis_riwayat_upload'],$_POST['id_riwayat_upload'],$filename);
				}
			}
		} else {
			echo "Invalid file".$_FILES["file"]["type"];
		}

	}

	public function register_user(){
		$this->load->model('pegawai_model');
		$this->load->model('user_model');
        $id_pegawai = $_POST['id_pegawai'];

        $this->pegawai_model->id_pegawai = $id_pegawai;
        $detail = $this->pegawai_model->get_by_id();
        $cek_username = $this->user_model->cek_username($_POST['username']);
        $cek_user = $this->pegawai_model->cek_user($id_pegawai);

        if(cekForm($_POST)==TRUE){
        	echo json_encode(array("status" => FALSE,"message" => "Masih ada form yang kosong"));
        }elseif($_POST['password']!==$_POST['conf_password']){
        	echo json_encode(array("status" => FALSE,"message" => "Konfirmasi Password tidak sama"));
        }elseif($cek_username==FALSE){
        	echo json_encode(array("status" => FALSE,"message" => "Username sudah terdaftar"));
        }elseif($cek_user){
        	echo json_encode(array("status" => FALSE,"message" => "Pegawai ini telah didaftarkan"));
        }else{
        	$this->user_model->username = $_POST['username'];
        	$this->user_model->full_name = $detail->nama_lengkap;
        	$this->user_model->unit_kerja_id = $detail->id_unit_kerja;
        	$this->user_model->password = $_POST['password'];
        	$this->user_model->user_picture = 'user_default.png';
        	$this->user_model->user_status = 'Active';
        	$this->user_model->jabatan = '';
        	$this->user_model->kd_skpd = '';
        	$this->user_model->user_level = 2;
        	$this->user_model->id_pegawai = $id_pegawai;
        	$this->user_model->user_privileges = '';
        	$insert = $this->user_model->insert();
        	echo json_encode(array("status" => TRUE));
    	}
	}

	public function change_user(){
        $id_pegawai = $_POST['id_pegawai'];
		$this->load->model('user_model');
		$this->load->model('pegawai_model');
        $id_pegawai = $_POST['id_pegawai'];

        $this->pegawai_model->id_pegawai = $id_pegawai;
        $detail = $this->pegawai_model->get_by_id();

        $this->user_model->id_pegawai = $id_pegawai;
        $detail_user = $this->user_model->get_by_pegawai();
        $cek_username = $this->user_model->cek_username($_POST['username']);

        if(cekForm($_POST)==TRUE){
        	echo json_encode(array("status" => FALSE,"message" => "Masih ada form yang kosong"));
        }elseif($_POST['new_password']!==$_POST['cnew_password']){
        	echo json_encode(array("status" => FALSE,"message" => "Konfirmasi Password tidak sama"));
        // }elseif($cek_username==FALSE){
        	// echo json_encode(array("status" => FALSE,"message" => "Username sudah terdaftar"));
        }else{
        	$this->user_model->user_id = $detail_user->user_id;
        	$this->user_model->password = $_POST['new_password'];
        	$update = $this->user_model->update_pass();
        	echo json_encode(array("status" => TRUE));
    	}
	}

	public function delete_user($id_pegawai){
		$this->user_model->id_pegawai = $id_pegawai;
		$this->user_model->delete_from_pegawai();
        echo json_encode(array("status" => TRUE));
	}


	
}
?>