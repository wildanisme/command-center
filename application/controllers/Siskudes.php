<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siskudes extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model("pendapatan_model");
        $this->load->model("belanja_model");
        $this->load->helper('text');
        $this->load->helper('typography');
        $this->load->helper('file');
    }

    public function index()
    {

 $data["pendapatan"] = $this->pendapatan_model->getAll();
        $data["belanja"] = $this->belanja_model->getAll();
        $data["total_pendapatan"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_pendapatan')->result();
        $data["total_belanja"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_belanja')->result();

        $data["total_aslidesa"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_pendapatan where id_pendapatan between 1 and 4')->result();
        $data["detail_aslidesa"] = $this->db->query('SELECT uraian FROM siskudes_pendapatan where id_pendapatan between 1 and 4')->result();
        $data["persentase_aslidesa"] = $this->db->query('SELECT anggaran FROM siskudes_pendapatan where id_pendapatan between 1 and 4')->result();
        $data["total_transfer"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_pendapatan where id_pendapatan between 5 and 9')->result();
        $data["detail_transfer"] = $this->db->query('SELECT uraian FROM siskudes_pendapatan where id_pendapatan between 5 and 9')->result();
        $data["persentase_transfer"] = $this->db->query('SELECT anggaran FROM siskudes_pendapatan where id_pendapatan between 5 and 9')->result();
        $data["total_lain"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_pendapatan where id_pendapatan between 10 and 14')->result();
        $data["detail_lain"] = $this->db->query('SELECT uraian FROM siskudes_pendapatan where id_pendapatan between 10 and 14')->result();
        $data["persentase_lain"] = $this->db->query('SELECT anggaran FROM siskudes_pendapatan where id_pendapatan between 10 and 14')->result();

        $data["total_pegawai"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_belanja where id_belanja between 1 and 4')->result();
        $data["detail_pegawai"] = $this->db->query('SELECT uraian FROM siskudes_belanja where id_belanja between 1 and 4')->result();
        $data["persentase_pegawai"] = $this->db->query('SELECT anggaran FROM siskudes_belanja where id_belanja between 1 and 4')->result();
        $data["total_barjas"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_belanja where id_belanja between 5 and 11')->result();
        $data["detail_barjas"] = $this->db->query('SELECT uraian FROM siskudes_belanja where id_belanja between 5 and 11')->result();
        $data["persentase_barjas"] = $this->db->query('SELECT anggaran FROM siskudes_belanja where id_belanja between 5 and 11')->result();
        $data["total_modal"] = $this->db->query('SELECT sum(anggaran) as total_anggaran FROM siskudes_belanja where id_belanja between 12 and 16')->result();
        $data["detail_modal"] = $this->db->query('SELECT uraian FROM siskudes_belanja where id_belanja between 12 and 16')->result();
        $data["persentase_modal"] = $this->db->query('SELECT anggaran FROM siskudes_belanja where id_belanja between 12 and 16')->result();
        $data["belanja_limit"] = $this->db->query('SELECT * FROM siskudes_belanja limit 19')->result();
        $this->load->view('admin/siskudes/siskudes', $data);
    }
}
