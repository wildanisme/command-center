<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/integration/PTSP_service.php');
class Perizinan extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper('url');
		$this->load->library('tank_auth');
		$this->load->library('session');
		$this->load->model('tank_auth/users');

		$this->load->helper('text');
		$this->load->helper('typography');
        $this->load->helper('file');

		

		if ($this->tank_auth->is_logged_in()) {
			redirect ('welcome'); 
		}


		//echo $this->session->userdata('employee_id');exit();

	}

	public function index(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";
			$data['page'] = "perizinan";

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			$Ptsp  = new PTSP_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $data['tahun'] = $tahun;
            $param = array(
                'tahun' => $tahun
            );
            $end_point = "integration/commandcenter/init";

            $rs = json_decode($Ptsp->sendRequest($param,$end_point));
            
            if($rs->error == false){
				//die;
                //echo "<pre>";print_r($rs->data);die;
                $data['data_perizinan'] = $rs->data;                
            }

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}


	public function sumber(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/sumber" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
		}
	}
		


	public function sumber_add_perencanaan(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/sumber_add_" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}


	public function ref(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/ref" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function monitoring_skpd_api(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/monitoring_skpd" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";
			// $data['page'] = "perizinan";

			$this->user_model->user_id = $this->session->userdata('user_id');
			$this->user_model->set_user_by_user_id();

			$Ptsp  = new PTSP_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $data['tahun'] = $tahun;
            $param = array(
                'tahun' => $tahun
            );
            $end_point = "integration/commandcenter/monitoring_skpd";

            $rs = json_decode($Ptsp->sendRequest($param,$end_point));
            
            if($rs->error == false){
				//die;
                //echo "<pre>";print_r($rs->data);die;
                $rsdata = (array) $rs->data;
                $data = array_merge($data, $rsdata);
                // $data = $rs->data;                
                // echo "<pre>";print_r($data);die;
            }

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function monitoring_skpd(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/monitoring_skpd" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard_monitoring_skpd(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			// $data['query'] = $this->keuangan_model->get_keuangan_view();
			$data = array();
			
			$this->load->view('admin/perizinan/monitoring_skpd_view',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function api_pajak(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			//if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
			//if ($this->user_level=="User") redirect ('admin/dashboard'); 
			$data['title']		= app_name;
			$data['content']	= "perizinan/api_pajak" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "perizinan";

			
			$this->load->view('admin/index',$data);
		} else {
			redirect('admin/login');
			
		}
	}

	public function dashboard_api_pajak(){
		
		if ($this->user_id && $this->session->userdata("user_level")==1){
			// $data['query'] = $this->keuangan_model->get_keuangan_view();
			$data = array();
			
			$this->load->view('admin/perizinan/api_pajak_view',$data);
		} else {
			redirect('admin/login');
			
		}
	}




}
