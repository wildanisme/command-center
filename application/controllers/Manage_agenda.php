<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_agenda extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		$this->load->model('agenda_model');
		$id_agenda = $this->uri->segment(3);
		
		if (!in_array($this->session->userdata("user_level"),[1,3,4])) redirect("cityzen");
	}
	
	
	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Agenda - Admin ";
			$data['content']	= "agenda/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "agenda";

			$hal = 6;
			$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
			$mulai = ($page>1) ? ($page * $hal) - $hal : 0;
			$total = count($this->agenda_model->get_all());
			$data['pages'] = ceil($total/$hal);
			$data['current'] = $page;
			if(!empty($_POST)){
				$filter = $_POST;
				$data['filter'] = true;
				$data['filter_data'] = $_POST;
				$data['id_kategori_agenda'] = $_POST['id_kategori_agenda'];
			}
			else if(!empty($_GET)){
				$filter = $_GET;
				$data['filter'] = true;
				$data['filter_data'] = $_GET;
			}else{
				$filter = '';
				$data['filter'] = false;
			}
			$data['agenda'] = $this->agenda_model->get_for_page($mulai,$hal,$filter);

			$this->load->model('kategori_agenda_model');
			$data['kategori']		= $this->kategori_agenda_model->get_all();

			$data['total'] = $total;
			$data['belum_disetujui'] = $this->agenda_model->get_total_row("",null,['status' => 'Belum disetujui']);
			$data['disetujui'] = $this->agenda_model->get_total_row("",null,['status' => 'Disetujui']);
				

			
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah Agenda - Admin ";
			$data['content']	= "agenda/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "agenda";

			if(!empty($_POST)){
				$cek = $_POST;
				unset($cek['nama_agenda']);
				unset($cek['id_kategori_agenda']);
				if(cekForm($cek)){
					$data['message'] = 'Masih ada form yang kosong';
					$data['type'] = 'warning';
					$data = array_merge($data,$_POST);
				}else{
					$insert = $_POST;
					unset($insert['poster']);
					unset($insert['_wysihtml5_mode']);
					$insert['poster'] = "sumedang.png";

					$config['upload_path']          = './data/agenda/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('poster')){
						$tmp_name = $_FILES['poster']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$insert['poster'] = $this->upload->data('file_name');
					}

					if(!empty($insert['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($insert['id_desa']);
						$insert['id_kecamatan'] = $desa->id_kecamatan;
						
					}

					$insert['status'] = "Disetujui";
					$in = $this->agenda_model->insert($insert);
					$data['message'] = 'Agenda berhasil ditambahkan';
					$data['type'] = 'success';
				}
			}

			$this->load->model('kategori_agenda_model');
			$data['kategori']		= $this->kategori_agenda_model->get_all();
			$this->load->model("alamat_model");
			$data['desa'] = $this->alamat_model->get_desa();
			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	public function setuju($id_agenda)
	{
		if ($this->user_id && $this->agenda_model->is_authorize($id_agenda))
		{
			
			$this->db->update("agenda",["status" => "Disetujui"],["id_agenda" =>$id_agenda]);
			$detail = $this->agenda_model->get_by_id($id_agenda);
			$insert_log = array(
				'user_id'	=> $detail->id_user,
				'category'	=> 'agenda',
				'time'		=> date('Y-m-d H:i:s'),
				'activity'	=>	'Event anda untuk <b>'.$detail->nama_agenda."</b> telah disetujui",
				'description'	=> '',
			);
			$this->db->insert("logs",$insert_log);

			redirect('manage_agenda/view/'.$id_agenda);
		}
		else
		{
			redirect('home');
		}
	}
	



	public function view($id_agenda)
	{
		if ($this->user_id && $this->agenda_model->is_authorize($id_agenda)) 
		{
			$data['title']		= "Detail Agenda - Admin ";
			$data['content']	= "agenda/view" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "Agenda";
			if(!empty($_POST)){

				$insert = $_POST;

					unset($insert['_wysihtml5_mode']);

					unset($insert['poster']);
					

					
					$config['upload_path']          = './data/agenda/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;
					//$config['file_name']			= uniqid().".".$file_ext;
					$config['encrypt_name']			= true;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('poster')){
						$tmp_name = $_FILES['poster']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						
						$insert['poster'] = $this->upload->data('file_name');
						
						$agenda = $this->agenda_model->get_by_id($id_agenda);
						if($agenda && $agenda->poster!="")
						{
							if(file_exists("./data/agenda/".$agenda->poster))
								unlink("./data/agenda/".$agenda->poster);
						}
					}

					if(!empty($insert['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($insert['id_desa']);
						$insert['id_kecamatan'] = $desa->id_kecamatan;
						
					}

					
					
					$update = $this->agenda_model->update($insert,$id_agenda);
				if($update){
					//echo "<pre>";print_r($insert);die;
					$data['message'] = 'Agenda berhasil diperbarui';
					$data['type'] = 'success';

			}
			}
			
				$param = $_POST;

				unset($param['id_agenda']);
				//$this->agenda_model->update($param,$id);
				$data['message'] = 'Agenda berhasil diubah';
				$data['type'] = 'success';
			
				$data['detail'] = $this->agenda_model->get_by_id($id_agenda);
				$this->load->model("alamat_model");
				$data['desa'] = $this->alamat_model->get_desa();

				if(!empty($data['detail']->id_desa)){
					
					$data['detail_desa'] = $this->alamat_model->get_desa_by_id($data['detail']->id_desa);
				}
			
		

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


	public function hapus_agenda($id_agenda)
	{
		if ($this->user_id && $this->agenda_model->is_authorize($id_agenda))
		{
			$agenda = $this->agenda_model->get_by_id($id_agenda);
			$this->agenda_model->hapus_agenda($id_agenda);
			
			redirect('manage_agenda');
		}
		else
		{
			redirect('admin');
		}
	}

	

	

	
}
?>