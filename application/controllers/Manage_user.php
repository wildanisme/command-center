<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_user extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->user_privileges	= $this->user_model->user_privileges;
		$array_privileges = explode(';', $this->user_privileges);

		if($this->session->userdata("user_level")>1){
			redirect("home");
		}

			if (($this->user_level!="Administrator" && $this->user_level!="User") && !in_array('user', $array_privileges)) redirect ('welcome');

	}

	public function index()
	{
		if ($this->user_id)
		{
			$data['title']		= "Users - ". app_name;
			$data['content']	= "user/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			if (!empty($_GET['s'])) {
				$this->user_model->search = $_GET['s'];
				$data['search'] = $_GET['s'];
			}
			if($_POST)
			{

				$this->user_model->user_level_ = $_POST['id_level'];
				$this->user_model->full_name_ = $_POST['fullname'];

				$data = array_merge($data,$_POST);

				//echo "<pre>";print_r($data);die;
			}
			$data['per_page']	= 9;
			$data['total_rows']	= $this->user_model->get_total_row();
			$data['level']	= $this->user_model->get_user_level();

			//$data['instansi']	= $this->ref_skpd_model->get_all();




			$offset = 0;
			if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
			$data['query']		= $this->user_model->get_for_page($data['per_page'],$offset);
			$data['active_menu'] = "user";


			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}



	public function profile()
	{
		if (!$this->user_id)
		{
			redirect('admin/login');
		}
		else
		{

			$data['title']		= "Your profile - ". app_name;
			$data['content']	= "user/profile" ;

			$this->user_model->set_employee();
			$data['username']	= $this->user_model->username;
			$data['employee_id']	= $this->user_model->employee_id;
			$data['user_picture'] = $this->user_model->user_picture;
			$data['full_name']		= $this->user_model->full_name;
			$data['user_level']		= $this->user_model->level;
			$data['phone']			= $this->user_model->phone;
			$data['user_status']	= $this->user_model->user_status;
			$data['email']			= $this->user_model->email;
			$data['bio']			= $this->user_model->bio;
			$data['reg_date']		= $this->user_model->reg_date;
			$data['employee_name']		= $this->user_model->employee_name;
			$data['employee_email']		= $this->user_model->employee_email;
			$data['employee_phone']		= $this->user_model->employee_phone;
			$data['employee_address']		= $this->user_model->employee_address;
			$data['employee_designation']		= $this->user_model->employee_designation;
			$data['birthday']		= $this->user_model->birthday;
			$data['gender']		= $this->user_model->gender;
			$data['date_joining']		= $this->user_model->date_joining;
			$data['date_leaving']		= $this->user_model->date_leaving;
			$data['ktp']		= $this->user_model->ktp;
			$data['npwp']		= $this->user_model->npwp;
			$data['bpjs']		= $this->user_model->bpjs;
			$data['bpjs_kesehatan']		= $this->user_model->bpjs_kesehatan;
			$data['facebook']		= $this->user_model->facebook;
			$data['twitter']		= $this->user_model->twitter;
			$data['google']		= $this->user_model->google;
			$data['youtube']		= $this->user_model->youtube;
			$data['moto']		= $this->user_model->moto;
			$data['nama_departemen']		= $this->user_model->nama_departemen;
			$data['status_name']		= $this->user_model->status_name;
			$data['employee_identity']		= $this->user_model->employee_identity;
			$data['bank_account']		= $this->user_model->bank_account;
			$data['bank_name']		= $this->user_model->bank_name;
			$data['bank_account_holder']		= $this->user_model->bank_account_holder;
			$data['picture']		= $this->user_model->picture;
			$data['active_menu'] = "";
			$data['top_nav']	= "NO";
			$this->load->model('post_model');
			$data['total_post']		= $this->post_model->getTotalByAuthor($this->user_id);

			//activity
			$this->load->model('logs_model');
			$data['logs']		= $this->logs_model->get_some_id($this->user_model->user_id);


			//education
			$this->load->model('user_model');
			$data['education']		= $this->user_model->get_education($this->user_model->employee_id);

			//family
			$this->load->model('user_model');
			$data['family']		= $this->user_model->get_family($this->user_model->employee_id);

			//work experience
			$this->load->model('user_model');
			$data['work_ex']		= $this->user_model->get_work_ex($this->user_model->employee_id);




			$this->load->view('admin/index',$data);
		}
	}

	public function add()
	{
		if ($this->user_id && $this->user_level=="Administrator")
		{
			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes');
			$data['title']		= "Add new user - ". app_name;
			$data['content']	= "user/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "user";
			$data['level'] = $this->user_model->get_user_level();
			$this->load->model('employee_model');
			$this->load->helper('form');
			if (!empty($_POST))
			{
				if (($_POST['employee_name'] !="" &&
					$_POST['employee_identity'] !="") || ($_POST['password']!=$_POST['repassword'])
				)
				{



							$config['upload_path']          = './data/user_picture/';
				            $config['allowed_types']        = 'gif|jpg|png';
				            $config['max_size']             = 2000;
				            $config['max_width']            = 2000;
				            $config['max_height']           = 2000;

				            $this->load->library('upload', $config);
				            if ( ! $this->upload->do_upload())
			                {
			                    $this->employee_model->picture 	= "user_default.png";
			                    $tmp_name				= $_FILES['picture']['tmp_name'];
			                    if ($tmp_name!="")
			                    {
			                    	$data['message_type'] = "warning";
			                    	$data['message']			= $this->upload->display_errors();
			                    }
			                }
			                else
			                {
			                	$this->employee_model->picture = $this->upload->data('file_name');
			                }


			               	$this->employee_model->employee_name = $_POST['employee_name'];
			               	$this->employee_model->employee_identity = $_POST['employee_identity'];
			               	$this->employee_model->status_id = $_POST['status_id'];
			               	$this->employee_model->employee_departement = $_POST['employee_departement'];
			               	$this->employee_model->employee_designation = $_POST['employee_designation'];
			               	$this->employee_model->employee_phone = $_POST['employee_phone'];
			               	$this->employee_model->employee_email = $_POST['employee_email'];
			               	$this->employee_model->employee_address = $_POST['employee_address'];
			               	$this->employee_model->birthday = $_POST['birthday'];
			               	$this->employee_model->gender = $_POST['gender'];
			               	$this->employee_model->date_joining = $_POST['date_joining'];
			               	$this->employee_model->date_leaving = $_POST['date_leaving'];
			               	$this->employee_model->ktp = $_POST['ktp'];
			               	$this->employee_model->status_id = $_POST['status_id'];
			               	$this->employee_model->npwp = $_POST['npwp'];
			               	$this->employee_model->bpjs = $_POST['bpjs'];
			               	$this->employee_model->bpjs_kesehatan = $_POST['bpjs_kesehatan'];
			               	$this->employee_model->bank_name = $_POST['bank_name'];
			               	$this->employee_model->bank_account = $_POST['bank_account'];
			               	$this->employee_model->bank_account_holder = $_POST['bank_account_holder'];
			               	$this->employee_model->facebook = $_POST['facebook'];
			               	$this->employee_model->twitter = $_POST['twitter'];
			               	$this->employee_model->google = $_POST['google'];
			               	$this->employee_model->youtube = $_POST['youtube'];

			               	$this->employee_model->username = $_POST['username'];
			               	$this->employee_model->password = md5($_POST['password']);
			               	$this->employee_model->user_level = $_POST['user_level'];
			               	$this->employee_model->user_status = $_POST['user_status'];

			                $this->employee_model->insert();
			                $data['message_type'] = "success";
			                $data['message']		= "User has been added successfully.";






			                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been add employee";
		                	$this->logs_model->category = "add";
		                	$desc = $_POST['employee_name'];
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();




				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}

			//departmen
			$this->load->model('departemen_model');
			$data['departemen']		= $this->departemen_model->get_all();

			//departmen
			$this->load->model('status_employee_model');
			$data['status_employee']		= $this->status_employee_model->get_all();


			//education
			$this->load->model('user_model');
			$data['education']		= $this->user_model->get_education($this->user_model->employee_id);

			//family
			$this->load->model('user_model');
			$data['family']		= $this->user_model->get_family($this->user_model->employee_id);

			//work experience
			$this->load->model('user_model');
			$data['work_ex']		= $this->user_model->get_work_ex($this->user_model->employee_id);





			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}


	public function register()
	{
		if ($this->user_id && $this->user_level=="Administrator")
		{
			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes');
			$data['title']		= "Add new user - ". app_name;
			$data['content']	= "user/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "user";
			$data['level'] = $this->user_model->get_user_level();
			$this->load->helper('form');
			if (!empty($_POST))
			{
				if ($_POST['username'] !="" &&
					$_POST['full_name'] !="" &&
					$_POST['user_level'] !=""
				)
				{


						$avaliable = $this->user_model->check_avaliable("",$_POST['username'])	;
						if ($avaliable){
							$config['upload_path']          = './data/user_picture/';
				            $config['allowed_types']        = 'gif|jpg|png';
				            $config['max_size']             = 2000;
				            $config['max_width']            = 2000;
				            $config['max_height']           = 2000;

				            $this->load->library('upload', $config);
				            if ( ! $this->upload->do_upload())
			                {
			                    $this->user_model->user_picture 	= "user_default.png";
			                    $tmp_name				= $_FILES['userfile']['tmp_name'];
			                    if ($tmp_name!="")
			                    {
			                    	$data['message_type'] = "warning";
			                    	$data['message']			= $this->upload->display_errors();
			                    }
			                }
			                else
			                {
			                	$this->user_model->user_picture = $this->upload->data('file_name');
			                }

			                $this->user_model->username = $_POST['username'];
			                if ($_POST['password']=="")
			                	$this->user_model->password = "123456";
			                else
			                	$this->user_model->password = $_POST['password'];
			                $this->user_model->user_status = $_POST['user_status'];
			                $this->user_model->insert();
			                $data['message_type'] = "success";
			                $data['message']		= "User has been added successfully.";






			                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been add user";
		                	$this->logs_model->category = "add";
		                	$desc = $_POST['full_name'];
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();


			            }
			            else{
			            	$data['message_type'] = "danger";
							$data['message'] = "<strong>Opps..</strong> Username has been used by another user.";
			            }

				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}

			//departmen
			$this->load->model('departemen_model');
			$data['departmen']		= $this->departemen_model->get_all();

			//departmen
			$this->load->model('status_employee_model');
			$data['status_employee']		= $this->status_employee_model->get_all();


			//education
			$this->load->model('user_model');
			$data['education']		= $this->user_model->get_education($this->user_model->employee_id);

			//family
			$this->load->model('user_model');
			$data['family']		= $this->user_model->get_family($this->user_model->employee_id);

			//work experience
			$this->load->model('user_model');
			$data['work_ex']		= $this->user_model->get_work_ex($this->user_model->employee_id);





			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}


	public function edit($employee_id)
	{
		if ($this->user_id && $this->user_level=="Administrator")
		{
			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes');
			$data['title']		= "Edit user - ". app_name;
			$data['content']	= "user/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "user";
			$data['level'] = $this->user_model->get_user_level();
			$this->load->helper('form');
			$this->user_model->user_id = $employee_id;
			$this->load->model('employee_model');

			if (!empty($_POST))
			{
				if ($_POST['username'] !="" &&
					$_POST['full_name'] !="" &&
					$_POST['user_level'] !=""

				)
				{

						$avaliable = $this->user_model->check_avaliable($_POST['old_username'],$_POST['username'])	;
						if ($avaliable){
							$config['upload_path']          = './data/user_picture/';
				            $config['allowed_types']        = 'gif|jpg|png';
				            $config['max_size']             = 2000;
				            $config['max_width']            = 2000;
				            $config['max_height']           = 2000;

				            $this->load->library('upload', $config);
				            if ( ! $this->upload->do_upload())
			                {
			                    $this->user_model->user_picture 	= "";
			                    $tmp_name				= $_FILES['picture']['tmp_name'];
			                    if ($tmp_name!="")
			                    {
			                    	$data['error']			= $this->upload->display_errors();
			                    }
			                }
			                else
			                {
			                	$this->user_model->set_user_by_user_id();
			                	if ($this->user_model->user_picture !="user_default.png") unlink('./data/user_picture/'.$this->user_model->user_picture);
			                	$this->user_model->user_picture = $this->upload->data('file_name');
			                }

			                $this->employee_model->employee_name = $_POST['employee_name'];
			               	$this->employee_model->employee_identity = $_POST['employee_identity'];
			               	$this->employee_model->status_id = $_POST['status_id'];
			               	$this->employee_model->employee_departement = $_POST['employee_departement'];
			               	$this->employee_model->employee_designation = $_POST['employee_designation'];
			               	$this->employee_model->employee_phone = $_POST['employee_phone'];
			               	$this->employee_model->employee_email = $_POST['employee_email'];
			               	$this->employee_model->employee_address = $_POST['employee_address'];
			               	$this->employee_model->birthday = $_POST['birthday'];
			               	$this->employee_model->gender = $_POST['gender'];
			               	$this->employee_model->date_joining = $_POST['date_joining'];
			               	$this->employee_model->date_leaving = $_POST['date_leaving'];
			               	$this->employee_model->ktp = $_POST['ktp'];
			               	$this->employee_model->status_id = $_POST['status_id'];
			               	$this->employee_model->npwp = $_POST['npwp'];
			               	$this->employee_model->bpjs = $_POST['bpjs'];
			               	$this->employee_model->bpjs_kesehatan = $_POST['bpjs_kesehatan'];
			               	$this->employee_model->bank_name = $_POST['bank_name'];
			               	$this->employee_model->bank_account = $_POST['bank_account'];
			               	$this->employee_model->bank_account_holder = $_POST['bank_account_holder'];
			               	$this->employee_model->facebook = $_POST['facebook'];
			               	$this->employee_model->twitter = $_POST['twitter'];
			               	$this->employee_model->google = $_POST['google'];
			               	$this->employee_model->youtube = $_POST['youtube'];

			                $this->employee_model->update();
			                $data['message_type'] = "success";
			                $data['message']		= "User has been updated successfully.";


		                $this->user_model->update_privileges();


			                //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been updated user";
		                	$this->logs_model->category = "update";
		                	$desc = $_POST['full_name'];
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();




			            }
			            else{
			            	$data['message_type'] = "danger";
							$data['message'] = "<strong>Opps..</strong> Username has been used by another user.";
			            }

				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}
			$this->user_model->set_employee();
			$data['username']	= $this->user_model->username;
			$data['employee_id']	= $this->user_model->employee_id;
			$data['user_picture'] = $this->user_model->user_picture;
			$data['full_name']		= $this->user_model->full_name;
			$data['user_level']		= $this->user_model->level;
			$data['user_privileges']		= $this->user_model->user_privileges;
			$data['user_group_menu']		= $this->user_model->user_group_menu;
			$data['phone']			= $this->user_model->phone;
			$data['user_status']	= $this->user_model->user_status;
			$data['email']			= $this->user_model->email;
			$data['bio']			= $this->user_model->bio;
			$data['reg_date']		= $this->user_model->reg_date;
			$data['employee_name']		= $this->user_model->employee_name;
			$data['employee_email']		= $this->user_model->employee_email;
			$data['employee_phone']		= $this->user_model->employee_phone;
			$data['employee_address']		= $this->user_model->employee_address;
			$data['employee_designation']		= $this->user_model->employee_designation;
			$data['birthday']		= $this->user_model->birthday;
			$data['gender']		= $this->user_model->gender;
			$data['date_joining']		= $this->user_model->date_joining;
			$data['date_leaving']		= $this->user_model->date_leaving;
			$data['ktp']		= $this->user_model->ktp;
			$data['npwp']		= $this->user_model->npwp;
			$data['bpjs']		= $this->user_model->bpjs;
			$data['bpjs_kesehatan']		= $this->user_model->bpjs_kesehatan;
			$data['facebook']		= $this->user_model->facebook;
			$data['twitter']		= $this->user_model->twitter;
			$data['google']		= $this->user_model->google;
			$data['youtube']		= $this->user_model->youtube;
			$data['moto']		= $this->user_model->moto;
			$data['nama_departemen']		= $this->user_model->nama_departemen;
			$data['status_name']		= $this->user_model->status_name;
			$data['employee_identity']		= $this->user_model->employee_identity;
			$data['bank_account']		= $this->user_model->bank_account;
			$data['bank_name']		= $this->user_model->bank_name;
			$data['bank_account_holder']		= $this->user_model->bank_account_holder;
			$data['picture']		= $this->user_model->picture;
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}

	public function edit_profile()
	{
		if ($this->user_id)
		{
			$data['title']		= "Edit user - ". app_name;
			$data['content']	= "user/edit_profile" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "user";
			$data['level'] = $this->user_model->get_user_level();
			$this->load->helper('form');
			$this->user_model->user_id = $this->user_id;
			if (!empty($_POST))
			{
				if ($_POST['username'] !="" &&
					$_POST['full_name'] !=""
				)
				{
					$avaliable = $this->user_model->check_avaliable($_POST['old_username'],$_POST['username'])	;
					if ($avaliable){
						$config['upload_path']          = './data/user_picture/';
			            $config['allowed_types']        = 'gif|jpg|png';
			            $config['max_size']             = 2000;
			            $config['max_width']            = 2000;
			            $config['max_height']           = 2000;

			            $this->load->library('upload', $config);
			            if ( ! $this->upload->do_upload())
		                {
		                    $this->user_model->user_picture 	= "";
		                    $tmp_name				= $_FILES['userfile']['tmp_name'];
		                    if ($tmp_name!="")
		                    {
		                    	$data['error']			= $this->upload->display_errors();
		                    }
		                }
		                else
		                {
		                	$this->user_model->set_user_by_user_id();
		                	if ($this->user_model->user_picture !="user_default.png") unlink('./data/user_picture/'.$this->user_model->user_picture);
		                	$this->user_model->user_picture = $this->upload->data('file_name');
		                }

		                $this->user_model->username = $_POST['username'];
		                $this->user_model->password = $_POST['password'];
		                $this->user_model->full_name = $_POST['full_name'];
		                $this->user_model->user_level= "";
		                $this->user_model->user_status = "";
		                $this->user_model->email 	= $_POST['email'];
		                $this->user_model->phone 	= $_POST['phone'];
		                $this->user_model->bio 	= $_POST['bio'];
		                $this->user_model->update();
		                $data['message_type'] = "success";
		                $data['message']		= "Your profile has been updated successfully.";


		                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been updated user";
		                	$this->logs_model->category = "update";
		                	$desc = $_POST['full_name'];
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();


		            }
		            else{
		            	$data['message_type'] = "danger";
						$data['message'] = "<strong>Opps..</strong> Username has been used by another user.";
		            }
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}
			$this->user_model->set_user_by_user_id();
			$data['username']	= $this->user_model->username;
			$data['full_name']	= $this->user_model->full_name;
			$data['user_picture']	= $this->user_model->user_picture;
			$data['email']	= $this->user_model->email;
			$data['phone']	= $this->user_model->phone;
			$data['bio']	= $this->user_model->bio;
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}

	public function privileges()
	{
		if ($this->user_id)
		{

		                $this->user_model->update_privileges();


		                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been updated user";
		                	$this->logs_model->category = "update";
		                	$desc = "privileges";
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();

		            redirect('manage_user/edit/'.$this->uri->segment(3));


		}
		else
		{
			redirect('admin');
		}
	}


	public function add_education() {



	}








	public function delete()
	{
		$user_id= $this->input->post("id_user");
		if ($this->user_id && $this->user_level=="Administrator" && $user_id)
		{

			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes');
			$this->user_model->user_id = $user_id;
			$this->user_model->set_user_by_user_id();
			if ($this->user_model->user_picture !="user_default.png") unlink('./data/user_picture/'.$this->user_model->user_picture);
			$this->user_model->delete();
			redirect('manage_user');
		}
		else
		{
			//die($this->user_level);
			redirect('home');
		}
	}

	public function save_education($employee_id)
	{
		if ($employee_id && !empty($_POST)) {
        	$this->load->model('employee_model');
            $this->employee_model->save_education($employee_id);
		}
	}

	public function save_family($employee_id)
	{
		if ($employee_id && !empty($_POST)) {
        	$this->load->model('employee_model');
            $this->employee_model->save_family($employee_id);
		}
	}

	public function save_work_ex($employee_id)
	{
		if ($employee_id && !empty($_POST)) {
        	$this->load->model('employee_model');
            $this->employee_model->save_work_ex($employee_id);
		}
	}

	public function reload_table($employee_id,$table)
	{
		if ($table && $employee_id) {
			$this->load->library('table');
			switch ($table) {
				case 'education':
					$data 	= $this->user_model->get_education($employee_id);
					$template = array('table_open' => '<table id="table-education" class="table table-striped">', );
					$this->table->set_template($template);
					$this->table->set_heading(array('No.',
													'Grade',
													'Institution Name',
													'Start Year',
													'End Year',
													'Score'));
					foreach ($data as $num => $row) {
						$this->table->add_row(array(++$num,
													$row->grade,
													$row->institution_name,
													$row->start_year,
													$row->end_year,
													$row->score));
					}
					echo $this->table->generate();
					break;

				case 'family':
					$data 	= $this->user_model->get_family($employee_id);
					$template = array('table_open' => '<table id="table-family" class="table table-striped">', );
					$this->table->set_template($template);
					$this->table->set_heading(array('No.',
													'Full Name',
													'Gender',
													'Relationship',
													'Birthday',
													'Marital Status',
													'Jobs'));
					foreach ($data as $num => $row) {
						$this->table->add_row(array(++$num,
													$row->fullname,
													$row->gender,
													$row->relationship,
													$row->birthday,
													$row->marital_status,
													$row->jobs));
					}
					echo $this->table->generate();
					break;

				case 'work_ex':
					$data 	= $this->user_model->get_work_ex($employee_id);
					$template = array('table_open' => '<table id="table-work_ex" class="table table-striped">', );
					$this->table->set_template($template);
					$num=1;
					$this->table->set_heading(array('No.',
													'Company Name',
													'Position',
													'Start Date',
													'End Dater',
													'Description'));
					foreach ($data as $num => $row) {
						$this->table->add_row(array(++$num,
													$row->company_name,
													$row->position,
													$row->start_date,
													$row->end_date,
													$row->description));
					}
					echo $this->table->generate();
					break;

				default:
					# code...
					break;
			}
		}
	}

	public function save_setting($employee_id){
		$this->user_model->set_employee();
		$username = $_POST['username'];
		$password = $_POST['password'];
		$conf_password = $_POST['conf_password'];
		if(!empty($password)){
			if($password !== $conf_password){
				echo '<div class="alert alert-danger">Password Confirmation Not Match</div>';
			}else{
				$this->user_model->username = $username;
				$this->user_model->password = md5($password);
				$this->user_model->update_setting();
				echo '<div class="alert alert-success">User Setting successfully changed</div>';
			}
		}else{
			$this->user_model->username = $username;
			$this->user_model->update_setting();
			echo '<div class="alert alert-success">User Setting successfully changed</div>';
		}
	}

	public function new()
	{
		if ($this->user_id && $this->user_level=="Administrator")
		{
			if ($this->user_level!="Administrator" && $this->user_level!="Admin Web") redirect ('promkes');
			$data['title']		= "Add new user - ". app_name;
			$data['content']	= "user/new" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;

			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "user";
			$data['level'] = $this->user_model->get_user_level();


			$this->load->model("Mentor_model");
			$data['dt_mentor'] = $this->Mentor_model->get()->result();
//			echo "<pre>";print_r($data['dt_mentor']);die;

			$this->load->helper('form');
			if (!empty($_POST))
			{
				if ($_POST['username'] !="" &&
					$_POST['employee_name'] !="" &&
					$_POST['user_level'] !="" && ($_POST['password']==$_POST['repassword'])
				)
				{


						$avaliable = $this->user_model->check_avaliable("",$_POST['username'])	;
						if ($avaliable){
							$config['upload_path']          = './data/user_picture/';
				            $config['allowed_types']        = 'gif|jpg|png';
				            $config['max_size']             = 2000;
				            $config['max_width']            = 2000;
				            $config['max_height']           = 2000;

				            $this->load->library('upload', $config);
				            if ( ! $this->upload->do_upload())
			                {
			                    $this->user_model->user_picture 	= "user_default.png";
			                    $tmp_name				= $_FILES['userfile']['tmp_name'];
			                    if ($tmp_name!="")
			                    {
			                    	$data['message_type'] = "warning";
			                    	$data['message']			= $this->upload->display_errors();
			                    }
			                }
			                else
			                {
			                	$this->user_model->user_picture = $this->upload->data('file_name');
			                }

			                $this->user_model->username = $this->input->post('username');
			                if ($this->input->post('password')=="")
			                	$this->user_model->password = "123456";
			                else
			                	$this->user_model->password = $this->input->post('password');
			                $this->user_model->user_status = $this->input->post('user_status');


			                $this->user_model->full_name = $this->input->post('employee_name');
			                $this->user_model->user_level = $this->input->post('user_level');
			                $this->user_model->alamat = $this->input->post('employee_address');
			                $this->user_model->phone = $this->input->post('employee_phone');
							$this->user_model->email = $this->input->post('employee_email');
							$this->user_model->birth_date = $this->input->post('birth_date');
							$this->user_model->gender = $this->input->post('gender');
							$this->user_model->id_mentor = $this->input->post('id_mentor');
							//$this->user_model->id_kecamatan = "3211150";//paseh
							//$this->user_model->id_kabupaten = "3211";//smdg
			        //        $this->user_model->id_desa = ($_POST['user_level']==3)  ? $_POST['id_desa'] : null;

			                $this->user_model->insert();
			                $data['message_type'] = "success";
			                $data['message']		= "User has been added successfully.";

			                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been add user";
		                	$this->logs_model->category = "add";
		                	$desc = $_POST['employee_name'];
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();


			            }
			            else{
			            	$data['message_type'] = "danger";
							$data['message'] = "<strong>Opps..</strong> Username has been used by another user.";
			            }

				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data not complete. Please complete it!";
				}
			}

			//departmen


			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('admin');
		}
	}



	public function contact($user_id=null)
	{
		if (!$this->user_id)
		{
			redirect('admin/login');
		}
		else
		{

			$detail = $this->db->where("user_id",$user_id)->get("user")->row();

			if(!$user_id || !$detail)
			{
				show_404();
			}

			if(!empty($_POST))
			{
				if($this->input->post("menu")=="detail")
				{
					$this->db->where("user_id",$user_id);
					$this->db->set("email",$this->input->post("email"));
					$this->db->set("full_name",$this->input->post("fullname"));
					$this->db->update("user");
					echo "<script>alert('Pengguna Berhasil diubah')</script>";
				}
				else{
					if($this->input->post("password")=="")
					{
						echo "<script>alert('Password harus diisi')</script>";
					}
					else if($this->input->post("password") != $this->input->post("c_password"))
					{
						echo "<script>alert('Password tidak sama')</script>";
						//die;
					}
					else{
						$this->db->where("user_id",$user_id);
						$this->db->set("password",md5($this->input->post("password")));
						$this->db->update("user");
						echo "<script>alert('Password Berhasil diubah')</script>";
					}

				}
			}

			$data['title']		= "Pengguna - ". app_name;
			$data['content']	= "user/contact" ;

			$this->user_model->user_id = $this->user_id;
			$this->user_model->set_user_by_user_id();

			$data['full_name']		= $this->session->userdata("full_name");
			$data['user_level']		= $this->session->userdata("level");
			$data['user_picture'] = $this->user_model->user_picture;

			$data['user_id'] = $user_id;
			$this->user_model->user_id = $user_id;
			$this->user_model->set_user_by_user_id();

			$data['user_privileges']		= $this->user_model->user_privileges;
			$data['user_group_menu']		= $this->user_model->user_group_menu;

			$data['username']	= $this->user_model->username;



			$data['fullname']		= $this->user_model->full_name;

			$data['userlevel']		= $this->user_model->level;
			$data['phone']			= $this->user_model->phone;
			$data['user_status']	= $this->user_model->user_status;
			$data['email']			= $this->user_model->email;
			$data['bio']			= $this->user_model->bio;
			$data['reg_date']		= $this->user_model->reg_date;

			$data['picture']		= $this->user_model->user_picture;
			$data['active_menu'] = "";
			$data['top_nav']	= "NO";
			$this->load->model('post_model');
			$data['total_post']		= $this->post_model->getTotalByAuthor($this->user_id);

			//activity
			$this->load->model('logs_model');
			$data['logs']		= $this->logs_model->get_some_id($this->user_model->user_id);


			$data['level'] = $this->user_model->get_user_level();


			//echo "<pre>";print_r($data);die;
			$this->load->view('admin/index',$data);
		}
	}


	public function change_password($user_id){
		$this->user_model->user_id = $user_id;
		$username = $_POST['username'];
		$old_username = $_POST['old_username'];
		$password = $_POST['password'];
		$conf_password = $_POST['conf_password'];
		if ($this->user_model->check_avaliable($old_username, $username)==false) {
			$username = $old_username;
			echo '<div class="alert alert-warning">Username is not available, taking back to <b>'.$old_username.'</b></div>';
		}
		if(!empty($password && $password!="")){
			if($password !== $conf_password){
				echo '<div class="alert alert-danger">Password Confirmation Not Match</div>';
			}else{
				$this->user_model->username = $username;
				$this->user_model->password = md5($password);
				$this->user_model->update_setting();
				echo '<div class="alert alert-success">User Setting successfully changeds</div>';
			}
		}else{
			$this->user_model->username = $username;
			$this->user_model->update_setting();
			echo '<div class="alert alert-success">User Setting successfully changed</div>';
		}
	}

	public function update_privileges()
	{
		if ($this->user_id)
		{

		                $this->user_model->update_privileges();


		                 //logs
							$this->load->model('logs_model');
							$this->logs_model->user_id	 = $this->session->userdata('user_id');
		                	$this->logs_model->activity = "has been updated user";
		                	$this->logs_model->category = "update";
		                	$desc = "privileges";
		                	$this->logs_model->description = "with names ".$desc;
							$this->logs_model->insert();

		            redirect('manage_user/contact/'.$this->uri->segment(3));


		}
		else
		{
			redirect('admin');
		}
	}

	function x_update_profile() {
		if ($this->input->post('id') AND $this->input->post('name') AND $this->input->post('value')) {
			$this->user_model->x_update_profile();
		}
    }

	function x_update_profile_image($id) {
        if($id!=NULL){
			$config['upload_path']          = './data/user_picture/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 2000;
            $config['max_width']            = 2000;
            $config['max_height']           = 2000;

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('userfile'))
            {
                $_POST['userfile'] = "";
                echo "Error : ".$this->upload->display_errors();
            }
            else
            {
							$user = $this->db->where("user_id",$id)->get("user")->row();
							$path = $config['upload_path'].$user->user_picture;
							if($user->user_picture!="user_default.png" && file_exists($path))
							{
								unlink($path);
							}

            	$_POST['userfile'] = $this->upload->data('file_name');
				$res = $this->user_model->x_update_profile_image($id);
				if ($res) {
					echo "Berhasil mengganti foto profil.";
				} else {
					echo "Gagal mengganti foto profil.";
				}
            }

            //echo $_POST['userfile'];

        }
    }
		function x_delete_profile_image($id) {
	        if($id!=NULL){

								$user = $this->db->where("user_id",$id)->get("user")->row();
								$path = './data/user_picture/'.$user->user_picture;
								if($user->user_picture!="user_default.png" && file_exists($path))
								{
									unlink($path);
								}

								$this->db->set("user_picture","user_default.png");
								$this->db->where("user_id",$id);
								$this->db->update("user");
	         }


	 }
}
