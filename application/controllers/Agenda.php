<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->load->model('agenda_model');
		$this->load->model('kategori_agenda_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{

		$data['title'] = "Agenda - " .$this->company_profile_model->nama;
		$data['active_menu'] ="agenda";

		
		$data['per_page']	= 6;
		$offset = 0;
		if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
		
		$s= "";
		$tag=null;
		
		$filter = array();
		$html_escape = array();
		if(!empty($_POST)) $html_escape = html_escape($_POST);
		if(!empty($_GET)) $html_escape = html_escape($_GET);
		if($html_escape)
		{
			
			foreach($html_escape as $key=>$value)
			{
				$filter[$key] = $this->security->xss_clean($value);
			}
			if(!empty($filter['s']))
				$s = $filter['s'];

			if(!empty($filter['tag']))
				$tag = $filter['tag'];

			$data = array_merge($data,$filter);

			unset($filter['tag']);
			unset($filter['per_page']);
			unset($filter['s']);
			//var_dump($tag);die;
		}
		
	
		$data['agenda']		= $this->agenda_model->get_for_page($offset,$data['per_page'],$filter,$s,$tag);
		$data['total_rows']	= $this->agenda_model->get_total_row($s,$tag,$filter);
		$data['kategori_agenda'] = $this->kategori_agenda_model->get_all();
		
		$data['tags']=$this->agenda_model->getTag();

		
		//echo "<pre>";print_r($data);die;
		$this->load->view('blog/agenda',$data);

	}

	public function detail($id_agenda)
	{

		$data['title'] = "Detail Agenda - " .$this->company_profile_model->nama;
		$data['active_menu'] = "layanan";
		$data['detail'] = $this->agenda_model->get_by_id($id_agenda);

			
		$this->load->view('blog/detail_agenda',$data);
	}
	
	
}
?>