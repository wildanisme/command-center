<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->user_level = $this->session->userdata('user_level');
		$this->load->model('visitor_model');
		$this->visitor_model->cek_visitor();
		
		$this->load->model('company_profile_model');

		$this->company_profile_model->set_identity();
	}

	public function index()
	{
		$data['title'] = "Video - " .$this->company_profile_model->nama;
		$data['active_menu'] ="video";
		$this->load->model('video_model');

		if (!empty($_GET['s'])) {
			$this->video_model->search = $_GET['s'];
			$data['search'] = $_GET['s'];
		}
		if (!empty($_GET['video_model'])) {
			$this->video_model->search_c = $_GET['c'];
			$data['search_c'] = $_GET['c'];
		}
		$data['per_page']	= 6;
		$data['total_rows']	= $this->video_model->get_total_row();
		$offset = 0;
		if (!empty($_GET['per_page'])) $offset = $_GET['per_page'] ;
		$data['video'] = $this->video_model->get_for_page($data['per_page'],$offset);

		//banner
		$this->load->model('banner_model');
		$data['banner'] = $this->banner_model->get_all();

		$this->load->model('category_video_model');
		$data['category'] = $this->category_video_model->get_all();
		
		$this->load->view('blog/src/header',$data);
		$this->load->view('blog/video',$data);
		$this->load->view('blog/src/footer',$data);
	}

	public function detail($id_video)
	{
		$data['title'] = "Detail Video - " .$this->company_profile_model->nama;
		$data['active_menu'] ="video";
		$this->load->model('video_model');
		$this->video_model->id_video = $id_video;
		$data['detail'] = $this->video_model->get_by_id();
		//banner
		$this->load->model('banner_model');
		$data['banner'] = $this->banner_model->get_all();
		
		$this->load->model('category_video_model');
		$data['category'] = $this->category_video_model->get_all();
		
		$this->load->view('blog/src/header',$data);
		$this->load->view('blog/detail_video',$data);
		$this->load->view('blog/src/footer',$data);
	}
	
}
?>