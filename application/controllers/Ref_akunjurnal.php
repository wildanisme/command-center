<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_akunjurnal extends CI_Controller {
	public $user_id;
	public function __construct()
	{
			parent::__construct();
			$this->user_id = $this->session->userdata('user_id');
			$this->load->model('user_model');
			$this->user_model->user_id = $this->user_id;
			$this->user_model->set_user_by_user_id();
			$this->user_picture = $this->user_model->user_picture;
			$this->full_name    = $this->user_model->full_name;
			$this->user_level   = $this->user_model->level;
			$this->load->model('Ref_akun_jurnal_model');
			if (!$this->user_id)
			{
				redirect("admin/login");
			}

			if($this->user_level!="Administrator")
			{
				show_404();
			}
	}

	public function index()
	{
			$data['title']     = "Ref Akun Jurnal ";
					$data['content']    = "ref_akun_jurnal/index" ;
					$data['user_picture'] = $this->user_picture;
					$data['full_name']      = $this->full_name;
					$data['user_level']     = $this->user_level;
					$data['active_menu'] = "ref";

			if (!empty($_POST)) {
				//echo "<pre>";print_r($_POST);die;
				$dt = array(
					'kode_akun'  => $this->input->post("kode_akun"),
					'nama_akun'  => $this->input->post("nama_akun"),
					'jenis'  => $this->input->post("jenis"),
					'deskripsi'  => $this->input->post("deskripsi"),
					'terkunci'  => $this->input->post("terkunci"),
					'id_induk'  => $this->input->post("id_induk"),
					'status'  => $this->input->post("status"),
				);
				$id_akun = $this->input->post("id_akun");
				if($id_akun=="")
				{
					$this->db->insert("ref_akun_jurnal",$dt);
				}
				else{
					$this->db->where("id_akun",$id_akun)
					->update("ref_akun_jurnal",$dt);
				}
				$this->session->set_flashdata("success","Akun jurnal berhasil disimpan");
				redirect("ref_akunjurnal");
			}

			$data['list'] = $this->Ref_akun_jurnal_model->get()->result();

			$param_induk['where']['ref_akun_jurnal.id_induk'] = null;
			$param_induk['where']['ref_akun_jurnal.jenis'] = "pemasukan";
			$data['dt_induk'] = $this->Ref_akun_jurnal_model->get($param_induk)->result();

			$this->load->view('admin/index', $data);
	}

	public function delete()
	{
			if($this->input->is_ajax_request() )
			{
					$dt=$this->db->where("id_akun",$this->input->post("id"))->get("ref_akun_jurnal")->row();

					$data['status'] = FALSE;

					if($this->input->post("id") && $dt){
							$this->db->delete("ref_akun_jurnal",['id_akun' => $this->input->post("id")]);

							if( $this->db->affected_rows() == 1 ){
									$data['status'] = true ;
							}
					}

					echo json_encode($data);
			}
	}

	public function get_akun_induk($jenis = "pemasukan")
	{
			if($this->input->is_ajax_request() )
			{
				$param_induk['where']['ref_akun_jurnal.id_induk'] = null;
				$param_induk['where']['ref_akun_jurnal.jenis'] = $jenis ;
				$data['dt_induk'] = $this->Ref_akun_jurnal_model->get($param_induk)->result();
				echo json_encode($data);
			}
	}

}
?>
