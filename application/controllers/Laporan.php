<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->load->model('ref_rkt_model');
		$this->load->model('ref_unit_kerja_model');
		$this->load->model('renaksi_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		if ($this->user_level=="Admin Web"); 

		$this->load->model('ref_absensi_model','ref_absensi_m');
	}
	public function index()
	{
		$this->perencanaan();
	}

	public function perencanaan()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Perencanaan - Admin ";
			$data['content']	= "laporan/perencanaan" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";


			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');

			// if(!empty($_POST)){
			// 	$this->ref_absensi_m->nama_absensi = $_POST['nama_absensi'];
			// 	$this->ref_absensi_m->uraian = $_POST['uraian'];
			// }
			// $data['item'] = $this->ref_absensi_m->get_all();

			$data['tahun'] = $this->ref_rkt_model->get_tahun();
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			$data['item'] = $this->sasaran_strategis_model->getAllSasaran($params);
			// print_r($data['item']);die;


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function cetak_perencanaan()
	{
		if ($this->user_id)
		{
			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');

			include APPPATH.'third_party/PHPExcel.php';
			include APPPATH.'third_party/PHPExcel/IOFactory.php';

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load("./template/template_perencanaan.xlsx");
			$baris  = 8;
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$item = $this->sasaran_strategis_model->getAllSasaran($params);
			$no=1;

			foreach ($item as $i){
				// print_r($i);
				if($i->jenis=='SS'){
					$n = 'sasaran_strategis';
					$iku = $this->sasaran_strategis_model->get_iku($i->id_sasaran_strategis);
				}elseif($i->jenis=='SP'){
					$n = 'sasaran_program';
					$iku = $this->sasaran_program_model->get_iku($i->id_sasaran_program);
				}elseif($i->jenis=='SK'){
					$n = 'sasaran_kegiatan';
					$iku = $this->sasaran_kegiatan_model->get_iku($i->id_sasaran_kegiatan);
				}
				$nama_unit_kerja = $this->ref_unit_kerja_model->get_by_id($i->id_unit)->nama_unit_kerja;
				$kode = 'kode_'.$n;
				$styleArray = array(
                  // 'borders' => array(
                  //   'allborders' => array(
                  //     'style' => PHPExcel_Style_Border::BORDER_THIN
                  //   )
                  // )
				);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$baris, $no);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$baris, $i->$kode); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$baris, $i->$n); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$baris, $i->bobot."%"); 

				if(count($iku)>0){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, 1); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, $iku[0]->kode_indikator); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, $iku[0]->nama_indikator); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, $iku[0]->bobot."%"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, $iku[0]->target); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, $iku[0]->satuan); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, ($iku[0]->polaritas=='MAX') ? 'Maximize' : (($iku[0]->polaritas=='MIN') ? 'Minimize' : '-')); 
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, "-"); 
				} 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("L".$baris, $nama_unit_kerja); 


				$no++;
				$baris++;


				if(count($iku)>1){
					$noo=1;
					foreach($iku as $ii){
						if($noo!==1){
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$baris, "");
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, $noo); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, $ii->kode_indikator); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, $ii->nama_indikator); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, $ii->bobot."%"); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, $ii->target); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, $ii->satuan); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, ($ii->polaritas=='MAX') ? 'Maximize' : (($ii->polaritas=='MIN') ? 'Minimize' : '-')); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("L".$baris, $nama_unit_kerja); 
							$baris++;
						} 
						$noo++; 
					}
				}


			}


			if($this->user_level!=='Administrator'){

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', 'Unit Kerja '.$nama_unit_kerja);
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data');   
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$filename = 'Laporan Perencanaan SIKAP.xlsx';
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			$objWriter->save("php://output");


		}
		else
		{
			redirect('admin');
		}
	}

	public function pencapaian()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Pencapaian - Admin ";
			$data['content']	= "laporan/pencapaian" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');

			// if(!empty($_POST)){
			// 	$this->ref_absensi_m->nama_absensi = $_POST['nama_absensi'];
			// 	$this->ref_absensi_m->uraian = $_POST['uraian'];
			// }
			// $data['item'] = $this->ref_absensi_m->get_all();

			$data['tahun'] = $this->ref_rkt_model->get_tahun();
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			$data['item'] = $this->sasaran_strategis_model->getAllSasaran($params);

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function cetak_pencapaian()
	{
		if ($this->user_id)
		{
			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');
			
			include APPPATH.'third_party/PHPExcel.php';
			include APPPATH.'third_party/PHPExcel/IOFactory.php';

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load("./template/template_pencapaian.xlsx");
			$baris  = 8;
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$item = $this->sasaran_strategis_model->getAllSasaran($params);
			$no=1;

			foreach ($item as $i){
				// print_r($i);
				if($i->jenis=='SS'){
					$n = 'sasaran_strategis';
					$iku = $this->sasaran_strategis_model->get_iku($i->id_sasaran_strategis);
				}elseif($i->jenis=='SP'){
					$n = 'sasaran_program';
					$iku = $this->sasaran_program_model->get_iku($i->id_sasaran_program);
				}elseif($i->jenis=='SK'){
					$n = 'sasaran_kegiatan';
					$iku = $this->sasaran_kegiatan_model->get_iku($i->id_sasaran_kegiatan);
				}
				$nama_unit_kerja = $this->ref_unit_kerja_model->get_by_id($i->id_unit)->nama_unit_kerja;
				$kode = 'kode_'.$n;
				$styleArray = array(
                  // 'borders' => array(
                  //   'allborders' => array(
                  //     'style' => PHPExcel_Style_Border::BORDER_THIN
                  //   )
                  // )
				);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$baris, $no);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$baris, $i->$kode); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$baris, $i->$n); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$baris, $i->bobot."%"); 

				if(count($iku)>0){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, 1); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, $iku[0]->kode_indikator); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, $iku[0]->nama_indikator); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, $iku[0]->bobot."%"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, $iku[0]->target); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, $iku[0]->satuan); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, ($iku[0]->polaritas=='MAX') ? 'Maximize' : (($iku[0]->polaritas=='MIN') ? 'Minimize' : '-')); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("L".$baris, $iku[0]->realisasi); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("M".$baris, round($iku[0]->capaian,2)."%");
				}else{
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("L".$baris, "-"); 
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue("M".$baris, "-");
				} 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("N".$baris, $nama_unit_kerja); 


				$no++;
				$baris++;


				if(count($iku)>1){
					$noo=1;
					foreach($iku as $ii){
						if($noo!==1){
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$baris, "");
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$baris, ""); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, $noo); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("F".$baris, $ii->kode_indikator); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("G".$baris, $ii->nama_indikator); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("H".$baris, $ii->bobot."%"); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("I".$baris, $ii->target); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("J".$baris, $ii->satuan); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("K".$baris, ($ii->polaritas=='MAX') ? 'Maximize' : (($ii->polaritas=='MIN') ? 'Minimize' : '-')); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("L".$baris, $ii->realisasi); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("M".$baris, round($ii->capaian,2)."%"); 
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue("N".$baris, $nama_unit_kerja); 
							$baris++;
						} 
						$noo++; 
					}
				}


			}

			if($this->user_level!=='Administrator'){

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', 'Unit Kerja '.$nama_unit_kerja);
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data');   
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$filename = 'Laporan Pencapaian SIKAP.xlsx';
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			$objWriter->save("php://output");


		}
		else
		{
			redirect('admin');
		}
	}

	public function renaksi(){

		if ($this->user_id)
		{
			$data['title']		= "Laporan Rencana Aksi - Admin ";
			$data['content']	= "laporan/renaksi" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');

			// if(!empty($_POST)){
			// 	$this->ref_absensi_m->nama_absensi = $_POST['nama_absensi'];
			// 	$this->ref_absensi_m->uraian = $_POST['uraian'];
			// }
			// $data['item'] = $this->ref_absensi_m->get_all();

			$data['tahun'] = $this->ref_rkt_model->get_tahun();
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			$data['item'] = $this->renaksi_model->get_renaksi_all($params);

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function cetak_renaksi(){

		if ($this->user_id)
		{
			$this->load->model('sasaran_strategis_model');
			$this->load->model('sasaran_program_model');
			$this->load->model('sasaran_kegiatan_model');
			
			include APPPATH.'third_party/PHPExcel.php';
			include APPPATH.'third_party/PHPExcel/IOFactory.php';

			$objPHPExcel = new PHPExcel();
			$objPHPExcel = PHPExcel_IOFactory::load("./template/template_renaksi.xlsx");
			$baris  = 8;
			$params = array();
			if(!empty($_POST)){
				if(!empty($_POST['tahun_rkt'])){
					$params['tahun'] = $_POST['tahun_rkt'];
				}
				if(!empty($_POST['id_unit_kerja'])){
					$params['id_unit'] = $_POST['id_unit_kerja'];
				}
			// print_r($params);die;
			}
			if($this->user_level!=='Administrator'){
				$params['id_unit'] = $this->session->userdata('unit_kerja_id');
			}
			$item = $this->renaksi_model->get_renaksi_all($params);
			$no=1;

			foreach ($item as $i){

				$uid = $i->uid_iku;
				$kode = substr($uid,1,2);
				if($kode=="SS"){
					$r = $this->sasaran_strategis_model->gDataW(array('uid_iku'=>$uid))->row();
					$nama_u = $this->sasaran_strategis_model->get_data_by_id($r->id_sasaran)[0]->nama_unit_kerja;
                // print_r($id_unit);
				}elseif($kode=="SK"){
					$r = $this->sasaran_kegiatan_model->gDataW(array('uid_iku'=>$uid))->row();
					$nama_u = $this->sasaran_kegiatan_model->get_data_by_id($r->id_sasaran)[0]->nama_unit_kerja;
				}elseif ($kode=="SP") {
					$r = $this->sasaran_program_model->gDataW(array('uid_iku'=>$uid))->row();
					$nama_u = $this->sasaran_program_model->get_data_by_id($r->id_sasaran)[0]->nama_unit_kerja;
				}

				$re = $this->renaksi_model->getDetail(array('renaksi.id_renaksi'=>$i->id_renaksi));
				$styleArray = array(
                  // 'borders' => array(
                  //   'allborders' => array(
                  //     'style' => PHPExcel_Style_Border::BORDER_THIN
                  //   )
                  // )
				);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("A".$baris, $no);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("B".$baris, $r->kode_indikator); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("C".$baris, $r->nama_indikator); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("D".$baris, $nama_u); 
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue("E".$baris, $i->satuan); 

				$noo=0;
				$field = array('F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC');
				foreach($re as $rr){
					if(empty($rr->target)){
						$jadwal = 'N';
					}else{
						$jadwal = 'Y';
					}
					$realisasi = ($rr->realisasi==''||empty($rr->realisasi)) ? '-' : $rr->realisasi;
					$target = ($rr->target==''||empty($rr->target)) ? '-' : $rr->target;
					if($jadwal=="Y"){
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($field[$noo].$baris, $target); 
						$noo++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($field[$noo].$baris, $realisasi); 
						$noo++;
					}else{
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($field[$noo].$baris, "-"); 
						$noo++;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($field[$noo].$baris, "-"); 
						$noo++;
					}
				}

				$no++;
				$baris++;


			}
			// die;
			if($this->user_level!=='Administrator'){

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4', 'Unit Kerja '.$nama_unit_kerja);
			}
			$objPHPExcel->getActiveSheet()->setTitle('Data');   
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$filename = 'Laporan Renaksi SIKAP.xlsx';
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			$objWriter->save("php://output");


		}
		else
		{
			redirect('admin');
		}
	}


	public function pohonkerja()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Pohon Kerja - Admin ";
			$data['content']	= "laporan/pohonkerja" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			$data['tahun'] = $this->ref_rkt_model->get_tahun();

			$tahun_rkt = date('Y');
			if(!empty($_POST['tahun_rkt'])) $tahun_rkt = $_POST['tahun_rkt'];
			$data['tahun_rkt'] = $tahun_rkt;

			$this->load->model('ref_visi_misi_model');
			$data['visi'] = $this->ref_visi_misi_model->get_visi();
			$data['misi'] = $this->ref_visi_misi_model->get_all_m();
			


			//echo "<pre>";print_r($data);die;

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function cascading_iku()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Pohon Kerja - Admin ";
			$data['content']	= "laporan/cascading_iku" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			$data['tahun'] = $this->ref_rkt_model->get_tahun();

			$data['tahun_rkt'] = date('Y');

			if(!empty($_POST)){
				$data = array_merge($data,$_POST);

				$this->load->model("ref_unit_kerja_model");
				$this->load->model("indikator_model");
				$unit = $this->ref_unit_kerja_model->getUnit(array('id_unit_kerja' => $_POST['id_unit_penanggungjawab']));

				if($unit[0]->level_unit_kerja==0){
					$type = "SS";
				}
				elseif($unit[0]->level_unit_kerja==1){
					$type = "SP";
				}
				else{
					$type = "SK";
				}
				$params = array(
					'type' => $type,
					'id_unit' => $_POST['id_unit_penanggungjawab'],
				);
				$data['_type'] = $type;
				$data['indikator'] = $this->indikator_model->getIndikator($params,"uid_iku is not null");

			}

			if($this->user_level=='Administrator'){
				$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();
			}
			else{
				$data['unit_kerja'] = $this->ref_unit_kerja_model->getUnit(null,"id_unit_kerja = '".$this->session->userdata('unit_kerja_id')."' OR ket_induk like '%|".$this->session->userdata('unit_kerja_id')."|%'");
			}

			//echo "<pre>";print_r($data);die;

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function getiku()
	{
		//die($_POST['id_unit']);
		if(!empty($_POST['id_unit']))
		{
			$this->load->model("indikator_model");
			$this->load->model("ref_unit_kerja_model");
			$unit = $this->ref_unit_kerja_model->getUnit(array('id_unit_kerja' => $_POST['id_unit']));

			if(!empty($unit)){
				if($unit[0]->level_unit_kerja==0){
					$type = "SS";
				}
				elseif($unit[0]->level_unit_kerja==1){
					$type = "SP";
				}
				else{
					$type = "SK";
				}
				$params = array(
					'type' => $type,
					'id_unit' => $_POST['id_unit'],
				);
				$indikator = $this->indikator_model->getIndikator($params,"uid_iku is not null");

				$opt = "";
				foreach ($indikator as $row) {
					$opt .= "<option value='$row->id_indikator'>$row->nama_indikator</option>";
				}
				die ($opt);
			}
		}
	}

	public function unitkerja()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan unitkerja - Admin ";
			$data['content']	= "laporan/unitkerja" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";


			$data['tahun'] = $this->ref_rkt_model->get_tahun();

			$tahun_rkt = date('Y');
			if(!empty($_POST['tahun_rkt'])) $tahun_rkt = $_POST['tahun_rkt'];
			$data['tahun_rkt'] = $tahun_rkt;
			$data['GLOBALVAR'] = GLOBALVAR;
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function detail_unitkerja($id_unit_kerja=null,$tahun_rkt=null)
	{
		$unit = $this->ref_unit_kerja_model->getUnit(array('id_unit_kerja' => $id_unit_kerja));
		

		if ($this->user_id && $id_unit_kerja!=null && $tahun_rkt!=null && $unit!=null)
		{
			$data['title']		= "Laporan unitkerja - Admin ";
			$data['content']	= "laporan/detail_unitkerja" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			if(!empty($_POST['tahun_rkt'])) $tahun_rkt = $_POST['tahun_rkt'];
			$data['tahun_rkt'] = $tahun_rkt;

			$data['tahun'] = $this->ref_rkt_model->get_tahun();
			$data['unit'] = $unit[0];

			$this->ref_rkt_model->id_unit_penanggungjawab=$id_unit_kerja;
			$this->ref_rkt_model->tahun_rkt = $tahun_rkt;
			$data['rkt'] = $this->ref_rkt_model->get_all();
			$data['id_rkt'] = $data['rkt'][0]->id_rkt;
			$data['detail_unit'] = $this->ref_unit_kerja_model->detail_unit($id_unit_kerja);
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function pegawai()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Unit Kerja - Admin ";
			$data['content']	= "laporan/pegawai" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";
			$data['tahun'] = $this->ref_rkt_model->get_tahun();
			$data['unit_kerja'] = $this->ref_unit_kerja_model->get_all();

			$this->load->model('pegawai_model');
			if(!empty($_POST)){
				$search = $_POST['id_unit_kerja'];
				$data = array_merge($data,$_POST);
			}else{
				$search='';
			}
			$data['pegawai'] = $this->pegawai_model->get_all($search);


			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}

	public function detail_pegawai($id_pegawai)
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Unit Kerja - Admin ";
			$data['content']	= "laporan/detail_pegawai" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "laporan";

			$this->load->model('pegawai_model');
			$this->load->model('kegiatan_model');
			$this->pegawai_model->id_pegawai = $id_pegawai;
			$data['detail'] = $this->pegawai_model->get_by_id();


			if(!empty($_POST)){
				$data['pekerjaan'] = $this->kegiatan_model->get_pekerjaan($id_pegawai,$_POST['tahun_rkt']);
			}else{
				$data['pekerjaan'] = $this->kegiatan_model->get_pekerjaan($id_pegawai);
			}
			$this->load->model('kegiatan_model');

			$data['id_pegawai'] = $id_pegawai;

			$tahun_rkt = date('Y');
			if(!empty($_POST['tahun_rkt'])) $tahun_rkt = $_POST['tahun_rkt'];
			$data['tahun_rkt'] = $tahun_rkt;

			$data['tahun'] = $this->ref_rkt_model->get_tahun();

			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}




	
}
?>