<?php
class Devs extends CI_Controller{
    public function run(){
        $mauneh_db = $this->load->database('mauneh', TRUE); 
        $mauneh_db->where('penerima_bansos IS NULL');
        $base = $mauneh_db->get('balita_bansos')->result();
        $no=1;
        foreach($base as $b){

            $mauneh_db->where('nik',$b->nik_ayah);
            $mauneh_db->or_where('nik',$b->nik_ibu);
            $check = $mauneh_db->get('databansos')->row();
            if($check){
                echo "[$no] $b->nama_anak - $check->keterangan_data\n";
                $mauneh_db->update('balita_bansos',['jenis_bansos'=>$check->keterangan_data,'penerima_bansos'=>'Y'],['id_balita_bansos'=>$b->id_balita_bansos]);
                $no++;
            }
        }
    }
    public function run_pkh(){
        $mauneh_db = $this->load->database('mauneh', TRUE); 
        $mauneh_db->where('penerima_bansos IS NULL');
        $base = $mauneh_db->get('balita_bansos')->result();
        $no=1;
        foreach($base as $b){

            $mauneh_db->where('nik',$b->nik_anak);
            $check = $mauneh_db->get('balita_pkh')->row();
            if($check){
                echo "[$no] $b->nama_anak - PKH\n";
                $mauneh_db->update('balita_bansos',['jenis_bansos'=>'PKH','penerima_bansos'=>'Y'],['id_balita_bansos'=>$b->id_balita_bansos]);
                $no++;
            }
        }
    }
    
function curlDisduk($url, $postData = '')
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://124.158.169.179/index.php/api/' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  FALSE);
    // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    if (!empty($postData)) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($postData));
    }

    $headers = array();
    $headers[] = 'Accept: */*';
    $headers[] = 'Skey: c630643500720b255abb22e2ab2c31f6';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
        die;
    }
    curl_close($ch);
    return json_decode($result);
}

    public function run_buruh(){
        $mauneh_db = $this->load->database('mauneh', TRUE); 
        $mauneh_db->where('penerima_bansos IS NULL');
        $mauneh_db->order_by('id_balita_bansos','DESC');
        $base = $mauneh_db->get('balita_bansos')->result();
        $no=1;
        $max = 20160;
        foreach($base as $b){
            // echo $b->id_balita_bansos;die;

            $cek_jumlah =  $mauneh_db->get_where('balita_bansos',['penerima_bansos'=>'Y'])->num_rows();
            if($cek_jumlah <= $max){

            // $mauneh_db->where('nik',$b->nik_anak);
            // $check = $mauneh_db->get('balita_pkh')->row();
            if(!empty($b->nik_ayah)){
                $nik_ortu = $b->nik_ayah;
            }else{
                $nik_ortu = $b->nik_ibu;
            }
            $get = $this->curlDisduk('penduduk?nik='.$nik_ortu);
            if(isset($get->data)){
            if(in_array($get->data->kode_pekerjaan,[19,20,21,22,9,88])){
                $check = true;
            }else{
                $check = false;
            }
            if($check){
                $pekerjaan = $get->data->pekerjaan;
                $sisa = $max - $cek_jumlah - 1;
                echo "[$no] $b->nama_anak - $pekerjaan ($sisa)\n";
                $mauneh_db->update('balita_bansos',['jenis_bansos'=>$pekerjaan,'penerima_bansos'=>'Y'],['id_balita_bansos'=>$b->id_balita_bansos]);
                $no++;
            }
        }
        }else{
            echo "SUDAH TERPENUHI";
            break;
        }
        }
    }

    public function alamat(){
        
        $mauneh_db = $this->load->database('mauneh', TRUE); 
        $base = $mauneh_db->get_where('balita_bansos',['penerima_bansos'=>'Y'])->result();
        foreach($base as $b){
            $get = $mauneh_db->get_where('balita_bansos_alamat',['id_balita_bansos'=>$b->id_balita_bansos])->row();
            if($get){
                
                $mauneh_db->update('balita_bansos',[ 'alamat' => $get->alamat, 'rt' => $get->rt, 'rw' => $get->rw, 'kode_pos' => $get->kode_pos],['id_balita_bansos'=>$b->id_balita_bansos]);
                echo "$b->nama_anak \n";
            }
        }
    }
}