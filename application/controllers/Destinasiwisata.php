<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Destinasiwisata extends CI_Controller
{
  public $user_id;
  public function __construct()
  {
    parent::__construct();
    $this->load->model("isu_model");
    $this->load->library('form_validation');
    $this->user_id = $this->session->userdata('user_id');
    $this->load->model('user_model');
    $this->user_model->user_id = $this->user_id;
    $this->user_model->set_user_by_user_id();
    $this->user_picture = $this->user_model->user_picture;
    $this->full_name  = $this->user_model->full_name;
    $this->user_level = $this->user_model->level;
    
    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->load->helper('url');
    $this->load->library('tank_auth');
    $this->load->library('session');
    $this->load->model('tank_auth/users');

    $this->load->helper('text');
    $this->load->helper('typography');
    $this->load->helper('file');

    $this->bulan = array(
      1 => 'Januari',
      2 => 'Februari',
      3 => 'Maret',
      4 => 'April',
      5 => 'Mei',
      6 => 'Juni',
      7 => 'Juli',
      8 => 'Agustus',
      9 => 'September',
      10 => 'Oktober',
      11 => 'Nopember',
      12 => 'Desember',
    );


    if ($this->tank_auth->is_logged_in()) {
      redirect ('welcome'); 
    }
  }

  public function index(){

    if ($this->user_id && $this->session->userdata("user_level")==1){
      //if ($this->user_level!="Administrator" && $this->user_level!="User") redirect ('home'); 
      //if ($this->user_level=="User") redirect ('admin/dashboard'); 
      $data['title']    = app_name;
      $data['content']  = "destinasiwisata/dashboard" ;
      $data['user_picture'] = $this->user_picture;
      $data['full_name']    = $this->full_name;
      $data['user_level']   = $this->user_level;
      $data['active_menu'] = "destinasiwisata";

      
      $this->load->view('admin/index',$data);
    } else {
      redirect('admin/login');
      
    }
  }

  public function dashboard()
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $internet = 'internet';
    $tv = 'tv';
    $radio = 'radio';
    $lokal = 'lokal';
    $regional = 'regional';
    $intern = 'internasional';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');

    $data["medinternet"] = $this->db->where('media =', $internet);
    $data["medinternet"] = $this->db->get('isu_berita');
    $data["medtv"] = $this->db->where('media =', $tv);
    $data["medtv"] = $this->db->get('isu_berita');
    $data["medradio"] = $this->db->where('media =', $radio);
    $data["medradio"] = $this->db->get('isu_berita');

    $data["lokal"] = $this->db->where('asal =', $lokal);
    $data["lokal"] = $this->db->get('isu_berita');
    $data["regional"] = $this->db->where('asal =', $regional);
    $data["regional"] = $this->db->get('isu_berita');
    $data["intern"] = $this->db->where('asal =', $intern);
    $data["intern"] = $this->db->get('isu_berita');


    $data["beritadash"] = $this->db->query('SELECT * FROM isu_berita ORDER BY id DESC LIMIT 5')->result();



    $data["berita"] = $this->db->query('SELECT * FROM isu_berita ORDER BY id DESC')->result();
    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 3 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();



    $data["kec"] = $this->db->query('SELECT kecamatan,kategori ,sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg ,
      COUNT(sentimen) as total FROM isu_berita GROUP BY kecamatan ORDER BY total DESC LIMIT 5')->result();

    //data kecmatan sumedang selatan
    $data["kecsumsel"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="sumedang selatan" ')->result();
    //data kecmatan sukasari
    $data["kecsukasari"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="sukasari" ')->result();
    //data kecmatan surian
    $data["kecsurian"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="surian" ')->result();
    //data kecmatan sumedang utara
    $data["kecsumut"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="sumedang utara" ')->result();
    //data kecmatan tanjung medar
    $data["kectanjungmedar"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="tanjung medar" ')->result();
    //data kecmatan tanjung kerta
    $data["kectanjungkerta"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="tanjungkerta" ')->result();
    //data kecmatan tomo
    $data["kectomo"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="tomo" ')->result();
    //data kecmatan tanjungsari
    $data["kectanjungsari"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="tanjungsari" ')->result();
    //data kecmatan wado
    $data["kecwado"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="wado" ')->result();
    //data kecmatan ujungjaya
    $data["kecujungjaya"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="ujungjaya" ')->result();
    //data kecmatan cibugel
    $data["keccibugel"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="cibugel" ')->result();
    //data kecmatan buahdua
    $data["kecbuahdua"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="buahdua" ')->result();
    //data kecmatan darmaraja
    $data["kecdarmaraja"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
     sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
     FROM isu_berita WHERE kecamatan="darmaraja" ')->result();
    //data kecmatan cimanggung
    $data["keccimanggung"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="cimanggung" ')->result();
    //data kecmatan conggeang
    $data["kecconggeang"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="conggeang" ')->result();
    //data kecmatan cimalaka
    $data["keccimalaka"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="cimalaka" ')->result();
    //data kecmatan jati gede
    $data["kecjatigede"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="jati gede" ')->result();
    //data kecmatan cisitu
    $data["keccisitu"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="cisitu" ')->result();
    //data kecmatan ganeas
    $data["kecganeas"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="ganeas" ')->result();
    //data kecmatan cisarua
    $data["keccisarua"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="cisarua" ')->result();
    //data kecmatan jatinunggal
    $data["kecjatinunggal"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="jatinunggal" ')->result();
    //data kecmatan jatinangor
    $data["kecjatinangor"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="jatinangor" ')->result();
    //data kecmatan paseh
    $data["kecpaseh"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="paseh" ')->result();
    //data kecmatan pamulihan
    $data["kecpamulihan"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="pamulihan" ')->result();
    //data kecmatan situraja
    $data["kecsituraja"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="situraja" ')->result();
    //data kecmatan situraja
    $data["kecrancakalong"] = $this->db->query('SELECT sum(if(sentimen="positif",1,0)) as totalpos,
      sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg
      FROM isu_berita WHERE kecamatan="rancakalong" ')->result();

    $this->load->view('admin/destinasiwisata/dashboard_view', $data);
  }

  public function add($id = null)
  {

    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');
    $data["berita"] = $this->isu_model->getAll();

    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $data["kategori1"] = $this->db->query('SELECT * FROM isu_kategori ORDER BY kategori ASC')->result();
    $data["kecamatan1"] = $this->db->query('SELECT * FROM isu_kecamatan ORDER BY nama_kecamatan ASC')->result();
    $data["kec"] = $this->db->query('SELECT kecamatan,kategori ,sum(if(sentimen="positif",1,0)) as totalpos,sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg ,COUNT(sentimen) as total FROM isu_berita GROUP BY kecamatan ORDER BY total DESC LIMIT 5')->result();

    $berita = $this->isu_model;
    $validation = $this->form_validation;
    $validation->set_rules($berita->rules());

    if ($validation->run()) {
      $berita->save();
      $this->session->set_flashdata('success', 'Berhasil disimpan');
    }

    $data["berita"] = $berita->getById($id);

    $this->load->view("admin/destinasiwisata/adminnew/add", $data);
  }
  public function delete($id = null)
  {
    if (!isset($id)) show_404();

    if ($this->isu_model->delete($id)) {
      redirect(site_url('destinasiwisata'));
    }
  }

  public function update($id = null)
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');
    $data["berita"] = $this->isu_model->getAll();

    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $data["kategori1"] = $this->db->query('SELECT * FROM isu_kategori ORDER BY kategori ASC')->result();
    $data["kecamatan1"] = $this->db->query('SELECT * FROM isu_kecamatan ORDER BY nama_kecamatan ASC')->result();
    $data["kec"] = $this->db->query('SELECT kecamatan,kategori ,sum(if(sentimen="positif",1,0)) as totalpos,sum(if(sentimen="netral",1,0)) as totalnet,sum(if(sentimen="negatif",1,0)) as totalneg ,COUNT(sentimen) as total FROM isu_berita GROUP BY kecamatan ORDER BY total DESC LIMIT 5')->result();

    $berita = $this->isu_model;
    $validation = $this->form_validation;
    $validation->set_rules($berita->rules());

    if ($validation->run()) {
      $berita->update();
      $this->session->set_flashdata('success', 'Berhasil disimpan');
    }

    $data["berita"] = $berita->getById($id);

    $this->load->view("admin/destinasiwisata/adminnew/edit", $data);
  }
  public function lihatdata()
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');

    $data["berita"] = $this->db->query('SELECT * FROM isu_berita ORDER BY id DESC')->result();
    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $this->load->view("admin/destinasiwisata/adminnew/lihatdata", $data);
  }
  public function positif()
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');

    $data["berita"] = $this->db->query('SELECT * FROM isu_berita WHERE sentimen = "positif" ORDER BY id DESC')->result();
    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $this->load->view("admin/destinasiwisata/adminnew/positif", $data);
  }

  public function netral()
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');

    $data["berita"] = $this->db->query('SELECT * FROM isu_berita WHERE sentimen = "netral" ORDER BY id DESC')->result();
    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $this->load->view("admin/destinasiwisata/adminnew/netral", $data);
  }

  public function negatif()
  {
    $baik = 'positif';
    $jelek = 'negatif';
    $netral = 'netral';
    $data["positif"] = $this->db->where('sentimen =', $baik);
    $data["positif"] = $this->db->get('isu_berita');
    $data["negatif"] = $this->db->where('sentimen =', $jelek);
    $data["negatif"] = $this->db->get('isu_berita');
    $data["netral"] = $this->db->where('sentimen =', $netral);
    $data["netral"] = $this->db->get('isu_berita');

    $data["berita"] = $this->db->query('SELECT * FROM isu_berita WHERE sentimen = "negatif" ORDER BY id DESC')->result();
    $data["kategoripos"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "positif" GROUP BY kategori ORDER BY total DESC LIMIT 3')->result();
    $data["kategorinet"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "netral" GROUP BY kategori ORDER BY total DESC LIMIT 2 ')->result();
    $data["kategorineg"] = $this->db->query('SELECT kecamatan,kategori ,COUNT( * ) as total FROM isu_berita WHERE sentimen = "negatif" GROUP BY kategori ORDER BY total DESC LIMIT 1')->result();

    $this->load->view("admin/destinasiwisata/adminnew/negatif", $data);
  }

  public function addkategori()
  {
    $kategori = $this->isu_model;
    $validation = $this->form_validation;
    $validation->set_rules($kategori->rules());

    if ($validation->run()) {
      $kategori->savekategori();
      $this->session->set_flashdata('success', 'Berhasil disimpan');
    }

    $data["kategori"] = $kategori->getById($id);
  }
}
