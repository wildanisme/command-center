<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_agenda extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('kategori_agenda_model');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		$this->user_privileges	= $this->user_model->user_privileges;
		$array_privileges = explode(';', $this->user_privileges);


		if (($this->user_level!="Administrator" && $this->user_level!="Admin Web") && !in_array('blog_category', $array_privileges)) redirect ('welcome');
	}
	public function index()
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			$data['title']		= "Kategori - ". app_name;
			$data['content']	= "kategori_agenda/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			
			$data['query']		= $this->kategori_agenda_model->get_all();
			$data['active_menu'] = "kategori_agenda";
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	public function add()
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			
			$data['title']		= "Tambah Kategori - ". app_name;
			$data['content']	= "kategori_agenda/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			
			$data['active_menu'] = "Kategori_agenda";
			if (!empty($_POST))
			{
				if ($_POST['nama_kategori_agenda'] !="")
				{
					
		                $this->kategori_agenda_model->nama_kategori_agenda = $_POST['nama_kategori_agenda'];
		                $this->kategori_agenda_model->kategori_slug = $_POST['kategori_slug'];
		                $this->kategori_agenda_model->status = $_POST['status'];
		                $this->kategori_agenda_model->insert();
		                $data['message_type'] = "success";
		                $data['message']		= "Kategori telah berhasil ditambahkan.";
		                redirect('kategori_agenda');
	           		
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data belum lengkap. silahkan lengkapi dulu!";
				}
			}
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}

	public function edit($id_kategori_agenda)
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
			
			$data['title']		= "Edit Kategori - ". app_name;
			$data['content']	= "kategori_agenda/edit" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			
			
			$data['active_menu'] = "Kategori_agenda";
			$this->kategori_agenda_model->id_kategori_agenda = $id_kategori_agenda;
			if (!empty($_POST))
			{
				if ($_POST['nama_kategori_agenda'] !=""  )
				{
					
		                $this->kategori_agenda_model->nama_kategori_agenda = $_POST['nama_kategori_agenda'];
		                $this->kategori_agenda_model->kategori_slug = $_POST['kategori_slug'];
		                $this->kategori_agenda_model->status = $_POST['status'];
		                $this->kategori_agenda_model->update();
		                $data['message_type'] = "success";
		                $data['message']		= "Kategori berhasil diperbarui.";

	           		
				}
				else
				{
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Data belum lengkap. silahkan lengkapi dulu!";
				}
			}
			$this->kategori_agenda_model->set_by_id();
			$data['nama_kategori_agenda'] = $this->kategori_agenda_model->nama_kategori_agenda;
			$data['kategori_slug'] = $this->kategori_agenda_model->nama_kategori_agenda;
			$data['status'] = $this->kategori_agenda_model->status;
			$this->load->view('admin/index',$data);

		}
		else
		{
			redirect('home');
		}
	}
	public function delete($id)
	{
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
		
			$this->kategori_agenda_model->id_kategori_agenda = $id;
			$this->kategori_agenda_model->delete();
			redirect('kategori_agenda');
		}
		else
		{
			redirect('home');
		}
	}
}
?>