<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pertanian extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->helper(array('url'));
        $this->load->model('m_kelompok');
    }

    function index()
    {
        $data['j_kelompok'] = $this->m_kelompok->j_kelompok();
        $data['j_kecamatan'] = $this->m_kelompok->j_kecamatan();
        $data['j_desa'] = $this->m_kelompok->j_desa();
        $data['j_anggota'] = $this->m_kelompok->j_anggota();

        $data['kelompok_kec'] = $this->m_kelompok->kelompok_kecamatan()->result();

        $data['kelompok_jenis'] = $this->m_kelompok->kelompok_jenis()->result();

        $this->load->view('admin/pertanian/v_dashboard', $data);
    }


    function data_kelompok()
    {

        $this->load->view('admin/pertanian/v_data_kelompok');
    }



    function get_data_kelompok()
    {
        $list = $this->m_kelompok->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_kelompok;
            $row[] = $field->kecamatan;
            $row[] = $field->desa;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_kelompok->count_all(),
            "recordsFiltered" => $this->m_kelompok->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }






    /**function data_rdkk(){
		
		$this->load->view('v_data_rdkk');
	}
	
	function get_data_rdkk()
    {
        $list = $this->M_rdkk->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama;
            $row[] = $field->kecamatan;
            $row[] = $field->desa;
			$row[] = $field->alamat;
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_rdkk->count_all(),
            "recordsFiltered" => $this->M_rdkk->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    } **/
}
