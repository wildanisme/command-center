<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_organisasi extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		$this->load->model('organisasi_model');
		
		$id_organisasi = $this->uri->segment(3);
		if (!in_array($this->session->userdata("user_level"),[1,3,4])) redirect("cityzen");
		
	}
	
	
	public function index()
	{
		
		if ($this->user_id)
		{
			$data['title']		= "organisasi - Admin ";
			$data['content']	= "organisasi/index" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "organisasi";
			
			$hal = 6;
			$page = isset($_GET["page"]) ? (int)$_GET["page"] : 1;
			$mulai = ($page>1) ? ($page * $hal) - $hal : 0;
			$total = count($this->organisasi_model->get_all());
			$data['pages'] = ceil($total/$hal);
			$data['current'] = $page;
			if(!empty($_POST)){
				$filter = $_POST;
				$data['filter'] = true;
				$data['filter_data'] = $_POST;
				$data['id_kategori_organisasi'] = $_POST['id_kategori_organisasi'];
			}else{
				$filter = '';
				$data['filter'] = false;
			}
			$this->organisasi_model->user_id= null;
			$data['organisasi'] = $this->organisasi_model->get_for_page($mulai,$hal,$filter);

			$this->load->model('kategori_organisasi_model');
			$data['kategori']		= $this->kategori_organisasi_model->get_all();

			
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}
	public function add()
	{
		if ($this->user_id)
		{
			$data['title']		= "Tambah organisasi - Admin ";
			$data['content']	= "organisasi/add" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_organisasi";

			$this->load->model("alamat_model");
			$data['desa'] = $this->alamat_model->get_desa();

			if(!empty($_POST)){
				$cek = $_POST;
				unset($cek['nama_organisasi']);
				unset($cek['id_kategori_organisasi']);
				if(cekForm($cek)){
					$data['message'] = 'Masih ada form yang kosong';
					$data['type'] = 'warning';
				}else{
					$insert = $_POST;
					unset($insert['foto']);
					$insert['foto'] = "sumedang.png";

					$config['upload_path']          = './data/organisasi/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('foto')){
						die($this->upload->display_errors());
						$tmp_name = $_FILES['foto']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$insert['foto'] = $this->upload->data('file_name');
					}

					if(!empty($insert['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($insert['id_desa']);
						$insert['id_kecamatan'] = $desa->id_kecamatan;
						
					}

					$in = $this->organisasi_model->insert($insert);
					$data['message'] = 'organisasi berhasil ditambahkan';
					$data['type'] = 'success';
				}
			}

			$this->load->model('kategori_organisasi_model');
			$data['kategori']		= $this->kategori_organisasi_model->get_all();

			$this->load->view('admin/index',$data);
		}
		else
		{
			redirect('admin');
		}
	}


	



	public function view($id_organisasi)
	{
		if ($this->user_id && $this->organisasi_model->is_authorize($id_organisasi))
		{
			$data['title']		= "Detail organisasi - Admin ";
			$data['content']	= "organisasi/view" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
			$data['active_menu'] = "manage_organisasi";

			$this->load->model("alamat_model");
			$this->load->model("kategori_organisasi_model");
			$data['desa'] = $this->alamat_model->get_desa();
			$data['kategori']		= $this->kategori_organisasi_model->get_all();

			if(!empty($_POST)){

				unset($_POST['foto']);
					

					
					$config['upload_path']          = './data/organisasi/';
					$config['allowed_types']        = 'gif|jpg|png|jpeg';
					$config['max_size']             = 2000;
					$config['max_width']            = 2000;
					$config['max_height']           = 2000;
					//$config['file_name']			= uniqid().".".$file_ext;
					$config['encrypt_name']			= true;

					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('foto')){
						$tmp_name = $_FILES['foto']['tmp_name'];
						if ($tmp_name!=""){
							$data['message'] = $this->upload->display_errors();
							$data['type'] = "danger";
						}
					}else{
						$_POST['foto'] = $this->upload->data('file_name');

						$organisasi = $this->organisasi_model->get_by_id($id_organisasi);
						if($organisasi && $organisasi->foto!="")
						{
							if(file_exists("./data/organisasi/".$organisasi->foto))
								unlink("./data/organisasi/".$organisasi->foto);
						}
					}

				if(!empty($_POST['id_desa']))
					{
						$this->load->model("alamat_model");
						$desa = $this->alamat_model->get_desa_by_id($_POST['id_desa']);
						$_POST['id_kecamatan'] = $desa->id_kecamatan;
						
					}

					$update = $this->organisasi_model->update($_POST,$id_organisasi);
				if($update){
					$data['message'] = 'organisasi berhasil diperbarui';
					$data['type'] = 'success';

			}
			}
			
				$param = $_POST;

				unset($param['id_organisasi']);
				//$this->listing_model->update($param,$id);
				$data['message'] = 'organisasi berhasil diubah';
				$data['type'] = 'success';
			
			
			$data['detail'] = $this->organisasi_model->get_by_id($id_organisasi);
		
			foreach($data['detail'] as $key=>$value)
			{
				$data[$key] = $value;
			}
			
			
			$this->load->view('admin/index',$data);


		}
		else
		{
			redirect('admin');
		}
	}


	public function hapus_organisasi($id_organisasi)
	{
		if ($this->user_id && $this->organisasi_model->is_authorize($id_organisasi)) 
		{
			$this->organisasi_model->user_id= null;

			$listing = $this->organisasi_model->get_by_id($id_organisasi);
			$this->organisasi_model->hapus_organisasi($id_organisasi);
			redirect('manage_organisasi');
		}
		else
		{
			redirect('admin');
		}
	}

	

	

	
}
?>