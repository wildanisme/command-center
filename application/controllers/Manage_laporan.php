<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_laporan extends CI_Controller {
	public $user_id;

	public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->kd_puskesmas= $this->session->userdata('kd_puskesmas');
		$this->load->model('user_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;

		if ($this->user_level=="Admin Web") redirect ('admin'); 
		$this->bulan = array(
										'1' => 'Jan',
										'2' => 'Feb',
										'3' => 'Mar',
										'4' => 'Apr',
										'5' => 'Mei',
										'6' => 'Jun',
										'7' => 'Jul',
										'8' => 'Agust',
										'9' => 'Sept',
										'10' => 'Okt',
										'11' => 'Nop',
										'12' => 'Des'
									);
	}

	public function penyuluhan()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan kegiatan penyuluhan";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$data['bulan'] = $this->bulan;
			$data['active_menu'] = "laporan";
			$data['bulan'] = $this->bulan;
			$data['user_level'] = $this->user_level;
			if (!empty($_POST)){
				$this->load->model('penyuluhan_model');
				if (!empty($_POST['kd_puskesmas']) &&
					!empty($_POST['bulan']) &&
					!empty($_POST['tahun'])) 
				{
					$this->penyuluhan_model->kd_puskesmas = $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					$this->penyuluhan_model->bulan= $data['bulan_'] = $_POST['bulan'];
					$this->penyuluhan_model->tahun = $data['tahun_'] = $_POST['tahun'];
					$this->penyuluhan_model->kd_puskesmas = $this->kd_puskesmas;
					$this->puskesmas_model->kd_puskesmas = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas;
					$data['dalam_gedung'] = $this->penyuluhan_model->get_report('Dalam gedung');
					$data['luar_gedung'] = $this->penyuluhan_model->get_report('Luar gedung');
					$this->load->model('target_penyuluhan_model');
					$data['target_dalam'] =$this->target_penyuluhan_model->get_target($_POST['kd_puskesmas'],$_POST['bulan'],$_POST['tahun'],"dalam");
					$data['target_luar'] =$this->target_penyuluhan_model->get_target($_POST['kd_puskesmas'],$_POST['bulan'],$_POST['tahun'],"luar");
					$data['content']	= "laporan/penyuluhan/laporan_penyuluhan" ;
				}
				else{
					if (!empty($_POST['kd_puskesmas'])) $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					if (!empty($_POST['bulan'])) $data['bulan_'] = $_POST['bulan'];
					if (!empty($_POST['tahun'])) $data['tahun_'] = $_POST['tahun'];
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Parameter laporan belum lengkap!";
					$data['content']	= "laporan/penyuluhan/index" ;
				}

				
			}
			else{
				$data['content']	= "laporan/penyuluhan/index" ;
			}

			$this->load->view('promkes/index',$data);
			
		}
		else
		{
			redirect('admin');
		}
	}

	public function download_penyuluhan()
	{
		ob_start();
		if ($this->user_id)
		{
			$data['title']		= "Laporan kegiatan penyuluhan";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$data['bulan'] = $this->bulan;
			$data['active_menu'] = "laporan";
			$data['bulan'] = $this->bulan;
			$data['user_level'] = $this->user_level;
			if (!empty($_POST)){
				$this->load->model('penyuluhan_model');
				if (!empty($_POST['kd_puskesmas']) &&
					!empty($_POST['bulan']) &&
					!empty($_POST['tahun'])) 
				{
					$this->penyuluhan_model->kd_puskesmas = $kd_puskesmas = $_POST['kd_puskesmas'];
					$this->penyuluhan_model->bulan= $bulan_ = $_POST['bulan'];
					$this->penyuluhan_model->tahun = $tahun_ = $_POST['tahun'];
					$this->penyuluhan_model->kd_puskesmas = $this->kd_puskesmas;
					$this->puskesmas_model->kd_puskesmas = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$nama_puskesmas = $this->puskesmas_model->nama_puskesmas;
					$dalam_gedung= $this->penyuluhan_model->get_report('Dalam gedung');
					$luar_gedung= $this->penyuluhan_model->get_report('Luar gedung');
					$this->load->model('target_penyuluhan_model');
					$target_dalam =$this->target_penyuluhan_model->get_target($_POST['kd_puskesmas'],$_POST['bulan'],$_POST['tahun'],"dalam");
					$target_luar =$this->target_penyuluhan_model->get_target($_POST['kd_puskesmas'],$_POST['bulan'],$_POST['tahun'],"luar");
					
echo"
<table>
				<tr>
					<td colspan=2><b>Laporan Bulanan Kegiatan Penyuluhan</b></td>
				</tr>
				<tr>
					<td colspan=2><b>$nama_puskesmas</b></td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td width=100px>Bulan</td>
					<td>: ".$this->bulan[$bulan_]." ".$tahun_."</td>
				</tr>
				<tr>
					<td>Target penyuluhan dalam gedung</td>
					<td>: $target_dalam</td>
				</tr>
				<tr>
					<td>Target penyuluhan luar gedung</td>
					<td>: $target_luar</td>
				</tr>
			</table>					
	<table width=100% border=1 style='border-collapse:collapse;' cellpadding=5 cellspacing=5 >
		<thead>
			<tr valign=center align=center>
				<th rowspan=2 align=center>No</th>
				<th colspan=4 align=center>Penyuluhan Dalam Gedung</th>
				<th colspan=4 align=center>Penyuluhan Luar Gedung</th>
			</tr>
			<tr>
				<th align=center>Pelaksana</th>
				<th align=center>Materi</th>
				<th align=center>Jumlah<br>Penyuluhan</th>
				<th align=center>Jumlah Yang<br>diberi<br>Penyuluhan</th>
				<th align=center>Pelaksana</th>
				<th align=center>Materi</th>
				<th align=center>Jumlah<br>Penyuluhan</th>
				<th align=center>Jumlah Yang<br>diberi<br>Penyuluhan</th>
			</tr>
		</thead>
		
		<tbody>
			";
				$penyuluhan = array();
				$i=0;
				foreach ($dalam_gedung as $row) {
					$penyuluhan['pelaksana_dalam'][$i] = $row->pelaksana;
					$penyuluhan['materi_dalam'][$i] = $row->materi;
					$penyuluhan['jumlah_penyuluhan_dalam'][$i] = $row->jumlah_penyuluhan;
					$penyuluhan['jumlah_peserta_dalam'][$i] = $row->jumlah_peserta;
					$i++;
				}
				$i=0;
				foreach ($luar_gedung as $row) {
					$penyuluhan['pelaksana_luar'][$i] = $row->pelaksana;
					$penyuluhan['materi_luar'][$i] = $row->materi;
					$penyuluhan['jumlah_penyuluhan_luar'][$i] = $row->jumlah_penyuluhan;
					$penyuluhan['jumlah_peserta_luar'][$i] = $row->jumlah_peserta;
					$i++;
				}
				$num=0;
				$jumlah_dlm = count($dalam_gedung);
				$jumlah_luar = count($luar_gedung);
				$jumlah = $jumlah_dlm;
				if ($jumlah_luar>$jumlah) $jumlah=$jumlah_luar;
				for ($i=$jumlah_dlm; $i<$jumlah ; $i++) { 
					$penyuluhan['pelaksana_dalam'][$i] = "";
					$penyuluhan['materi_dalam'][$i] = "";
					$penyuluhan['jumlah_penyuluhan_dalam'][$i] = "";
					$penyuluhan['jumlah_peserta_dalam'][$i] = "";
				}
				for ($i=$jumlah_luar; $i<$jumlah ; $i++) { 
					$penyuluhan['pelaksana_luar'][$i] = "";
					$penyuluhan['materi_luar'][$i] = "";
					$penyuluhan['jumlah_penyuluhan_luar'][$i] = "";
					$penyuluhan['jumlah_peserta_luar'][$i] = "";
				}
				for($num;$num<$jumlah;$num++)
				{
					echo "
						<tr>
							<td align=center>".($num+1)."</td>
							<td>".$penyuluhan['pelaksana_dalam'][$num]."</td>
							<td>".$penyuluhan['materi_dalam'][$num]."</td>
							<td align=center>".$penyuluhan['jumlah_penyuluhan_dalam'][$num]."</td>
							<td align=center>".$penyuluhan['jumlah_peserta_dalam'][$num]."</td>
							<td>".$penyuluhan['pelaksana_luar'][$num]."</td>
							<td>".$penyuluhan['materi_luar'][$num]."</td>
							<td align=center>".$penyuluhan['jumlah_penyuluhan_luar'][$num]."</td>
							<td align=center>".$penyuluhan['jumlah_peserta_luar'][$num]."</td>
						</tr>
					";
				}
			
			echo"
		</tbody>
	</table>";

					header("Content-Type: application/xls");    
					header("Content-Disposition: attachment; filename=laporan_penyuluhan.xls");  
					header("Pragma: no-cache"); 
					header("Expires: 0");
				}
				

				
			}
			
			
		}
		else
		{
			redirect('admin');
		}
	}

	public function phbs()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan PHBS";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->user_model->kd_puskesmas;
			$this->puskesmas_model->set_by_id();
			$this->load->model('referensi_model');
			$this->referensi_model->kd_kecamatan=$this->puskesmas_model->kd_kecamatan;
			$data['kecamatan'] = $this->referensi_model->get_kecamatan();
			$this->load->model('kategori_tempat_model');
			$data['kategori'] = $this->kategori_tempat_model->get_all();
			$data['bulan'] = $this->bulan;
			$data['active_menu'] = "laporan";
			$data['bulan'] = $this->bulan;
			$data['user_level'] = $this->user_level;
			if (!empty($_POST)){
				
				if (($_POST['kd_puskesmas']!="") &&
					($_POST['kd_kecamatan']!="") &&
					($_POST['kd_kategori']!="") &&
					($_POST['bulan']!="") &&
					($_POST['tahun']!="")) 
				{
					$this->load->model('phbs_model');
					$this->phbs_model->kd_puskesmas = $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					$this->phbs_model->kd_kecamatan = $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					$this->phbs_model->kd_kategori= $data['kd_kategori'] = $_POST['kd_kategori'];
					$this->phbs_model->bulan= $data['bulan_'] = $_POST['bulan'];
					$this->phbs_model->tahun = $data['tahun_'] = $_POST['tahun'];
					$this->phbs_model->kd_puskesmas = $this->kd_puskesmas;
					$this->puskesmas_model->kd_puskesmas = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas;

					$this->referensi_model->kd_kecamatan = $_POST['kd_kecamatan'];
					$this->referensi_model->set_kecamatan();

					$this->kategori_tempat_model->kd_kategori = $_POST['kd_kategori'];
					$this->kategori_tempat_model->set_by_id();
					$data['nama_kategori'] = $this->kategori_tempat_model->kategori;
					$data['nama_kecamatan'] = $this->referensi_model->kecamatan;

					$this->load->model('indikator_model');
					$this->indikator_model->kd_kategori = $_POST['kd_kategori'];
					$data['indikator'] = $this->indikator_model->get_all();

					$this->load->model('indikator_tambahan_model');
					$this->indikator_tambahan_model->kd_kategori = $_POST['kd_kategori'];
					$data['indikator_tambahan'] = $this->indikator_tambahan_model->get_all();

					$data['desa'] = $this->referensi_model->get_desa_by_kecamatan();
					$data['content']	= "laporan/phbs/laporan_phbs" ;
				}
				else{
					if (!empty($_POST['kd_puskesmas'])) $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					if (!empty($_POST['kd_kecamatan'])) $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					if (!empty($_POST['kd_kategori'])) $data['kd_kategori'] = $_POST['kd_kategori'];
					if (!empty($_POST['bulan'])) $data['bulan_'] = $_POST['bulan'];
					if (!empty($_POST['tahun'])) $data['tahun_'] = $_POST['tahun'];
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Parameter laporan belum lengkap!";
					$data['content']	= "laporan/phbs/index" ;
				}

				
			}
			else{
				$data['content']	= "laporan/phbs/index" ;
			}

			$this->load->view('promkes/index',$data);
			
		}
		else
		{
			redirect('admin');
		}
	}

	public function download_phbs()
	{
		ob_start();
		if ($this->user_id)
		{
			$data['title']		= "Laporan PHBS";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$this->load->model('referensi_model');
			$data['kecamatan'] = $this->referensi_model->get_kecamatan();
			$this->load->model('kategori_tempat_model');
			$data['kategori'] = $this->kategori_tempat_model->get_all();
			$bulan = $this->bulan;
			if (!empty($_POST)){
				
				if (!empty($_POST['kd_puskesmas']) &&
					!empty($_POST['kd_kecamatan']) &&
					!empty($_POST['kd_kategori']) &&
					!empty($_POST['bulan']) &&
					!empty($_POST['tahun'])) 
				{
					$this->load->model('phbs_model');
					$this->phbs_model->kd_puskesmas = $kd_puskesmas = $_POST['kd_puskesmas'];
					$this->phbs_model->kd_kecamatan = $kd_kecamatan = $_POST['kd_kecamatan'];
					$this->phbs_model->kd_kategori= $kd_kategori = $_POST['kd_kategori'];
					$this->phbs_model->bulan= $bulan_ = $_POST['bulan'];
					$this->phbs_model->tahun = $tahun_ = $_POST['tahun'];
					$this->phbs_model->kd_puskesmas = $this->kd_puskesmas;
					$this->puskesmas_model->kd_puskesmas = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$nama_puskesmas = $this->puskesmas_model->nama_puskesmas;

					$this->referensi_model->kd_kecamatan = $_POST['kd_kecamatan'];
					$this->referensi_model->set_kecamatan();

					$this->kategori_tempat_model->kd_kategori = $_POST['kd_kategori'];
					$this->kategori_tempat_model->set_by_id();
					$nama_kategori = $this->kategori_tempat_model->kategori;
					$nama_kecamatan = $this->referensi_model->kecamatan;

					$this->load->model('indikator_model');
					$this->indikator_model->kd_kategori = $_POST['kd_kategori'];
					$indikator = $this->indikator_model->get_all();

					$this->load->model('indikator_tambahan_model');
					$this->indikator_tambahan_model->kd_kategori = $_POST['kd_kategori'];
					$indikator_tambahan = $this->indikator_tambahan_model->get_all();

					$desa = $this->referensi_model->get_desa_by_kecamatan();
		echo"
			<table>
				<tr>
					<td colspan=2><b>Rekapitulasi Pendataan Perilaku hidup Bersih dan sehat (PHBS) $nama_kategori tingkat kecamatan</b></td>
				</tr>
				<tr>
					<td colspan=2><b>$nama_puskesmas</b></td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td>Kabupaten</td>
					<td>: Bogor</td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td>: $nama_kecamatan</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td width=100px>Tahun</td>
					<td>: $tahun_</td>
				</tr>
				<tr>
					<td>Bulan</td>
					<td>: ".$bulan[$bulan_]."</td>
				</tr>
				
			</table>
		<table class='table table-bordered' width=100% border=1 style='border-collapse:collapse;font-size:10px' cellpadding=5 cellspacing=5 >
		<thead>
		";
		$it = count($indikator_tambahan);
		$i = count($indikator);
		$col = 0;
		echo"
			<tr valign=center class='text-center'>
				<th class='text-center' colspan=".($it+2).">Identitas</th>
				<th class='text-center' colspan=".($i).">Indikator Perilaku Hidup Bersih dan Sehat</th>
				<th class='text-center' colspan=2>Status $nama_kategori</th>
			</tr>
			<tr valign=center class='text-center'>
				<th class='text-center'>Nama Desa</th>";
				$col++;
				foreach ($indikator_tambahan as $row) {
					echo "<th class='text-center'>$row->indikator_tambahan</th>";
					$col++;
				}
		echo"		
				<th class='text-center'>Jumlah $nama_kategori yang terdata</th>";
				$col++;
				foreach ($indikator as $row) {
					echo "<th class='text-center'>$row->indikator</th>";
					$col++;
				}
		echo"
				<th class='text-center'>Sehat</th>
				<th class='text-center'>Tidak Sehat</th>
					
			</tr>
			<tr>";
			$col=$col+2;
				for ($k=1; $k <= $col ; $k++) { 
					echo "<th class='text-center'>$k</th>";
					$total[$k] = 0;
				}
		echo"
			</tr>
				
		
		</thead>
		
		<tbody>";
		
		$CI =& get_instance();
		$CI->load->model('report_model');
		foreach ($desa as $row) {
		$col=0;
		echo"
			<tr>
				<td>$row->desa</td>";
				$col++;
				foreach ($indikator_tambahan as $it) {
					$nilai = $CI->report_model->get_nilai_it($it->kd_indikator_tambahan,$row->kd_desa,$bulan_,$tahun_);
					echo "<td class='text-center'>$nilai</td>";
					$col++;
					$total[$col] = $total[$col]+$nilai;
				}
				$jml_tempat_yg_terdata = $CI->report_model->get_tempat_yg_terdata($row->kd_desa,$kd_kategori,$bulan_,$tahun_,$kd_puskesmas);
		echo"	
				<td class='text-center'>$jml_tempat_yg_terdata</td>	";
				$col++;
				$total[$col] = $total[$col]+$jml_tempat_yg_terdata;
				foreach ($indikator as $i) {
					$nilai = $CI->report_model->get_nilai_i($i->kd_indikator,$row->kd_desa,$bulan_,$tahun_, $kd_kategori,$kd_puskesmas);
					echo "<td class='text-center'>$nilai</td>";
					$col++;
					$total[$col] = $total[$col]+$nilai;
				}
				$jumlah_sehat = $CI->report_model->get_status_kesehatan("Sehat",$row->kd_desa,$kd_kategori,$bulan_,$tahun_,$kd_puskesmas);
				$col++;
				$total[$col] = $total[$col]+$jumlah_sehat;
				$jumlah_tidak_sehat = $CI->report_model->get_status_kesehatan("Tidak Sehat",$row->kd_desa,$kd_kategori,$bulan_,$tahun_,$kd_puskesmas);
				$col++;
				$total[$col] = $total[$col]+$jumlah_tidak_sehat;
		echo"		
				<td class='text-center'>$jumlah_sehat</td>
				<td class='text-center'>$jumlah_tidak_sehat</td>
			</tr>
		";
			
		}	
		echo"
		</tbody>
		<thead>
			<tr>
				<th>JUMLAH</th>
			";
				for ($k=2; $k <= $col ; $k++) { 
					echo "<th class='text-center'>$total[$k]</th>";
					$total[$k] = 0;
				}
		echo"
			</tr>
		</thead>
	</table>";

					header("Content-Type: application/xls");    
					header("Content-Disposition: attachment; filename=laporan_PHBS_".$nama_kategori.".xls");  
					header("Pragma: no-cache"); 
					header("Expires: 0");
				}	

			}
		}
		else
		{
			redirect('admin');
		}
	}

	public function statistik()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Statistik PHBS Bulanan";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->user_model->kd_puskesmas;
			$this->puskesmas_model->set_by_id();
			$this->load->model('referensi_model');
			$this->referensi_model->kd_kecamatan=$this->puskesmas_model->kd_kecamatan;
			$data['kecamatan'] = $this->referensi_model->get_kecamatan();
			$this->load->model('kategori_tempat_model');
			$data['kategori'] = $this->kategori_tempat_model->get_all();
			$data['bulan'] = $this->bulan;
			$data['active_menu'] = "laporan";
			$data['bulan'] = $this->bulan;
			$data['user_level'] = $this->user_level;
			$data['kd_kecamatan'] = $this->puskesmas_model->kd_kecamatan;
			$data['kd_puskesmas'] = $this->puskesmas_model->kd_puskesmas;
			$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas;
			$this->referensi_model->kd_kecamatan = $this->puskesmas_model->kd_kecamatan;
			$data['desa']=$this->referensi_model->get_desa_by_kecamatan();
			if (!empty($_POST)){
				
				if ($_POST['kd_kategori']!="" &&
					$_POST['tahun']!="") 
				{
					$data['tahun_'] = $_POST['tahun'];
					$this->load->model('kategori_tempat_model');
					$this->kategori_tempat_model->kd_kategori = $data['kd_kategori'] = $_POST['kd_kategori'];
					$this->kategori_tempat_model->set_by_id();
					$data['kategori'] 	= $this->kategori_tempat_model->kategori;
					$this->load->model('referensi_model');
					$this->referensi_model->kd_kecamatan = $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					$this->referensi_model->kd_desa= $data['kd_desa'] = $_POST['kd_desa'];
					$this->referensi_model->set_kecamatan();
					$this->referensi_model->set_desa();
					$data['nama_kecamatan'] 	= $this->referensi_model->kecamatan;
					$data['nama_desa'] 	= $this->referensi_model->desa;
					$this->puskesmas_model->kd_puskesmas = $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas ;
					$this->load->model('indikator_model');
					$this->indikator_model->kd_kategori = $_POST['kd_kategori'];
					$data['indikator']	= $this->indikator_model->get_all();
					$data['bulan']	= $this->bulan;
					$data['content']	= "laporan/statistik/laporan_statistik" ;
				}
				else{
					if (!empty($_POST['kd_puskesmas'])) $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					if (!empty($_POST['kd_kecamatan'])) $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					if (!empty($_POST['kd_kategori'])) $data['kd_kategori'] = $_POST['kd_kategori'];
					if (!empty($_POST['kd_desa'])) $data['kd_desa'] = $_POST['kd_desa'];
					if (!empty($_POST['tahun'])) $data['tahun_'] = $_POST['tahun'];
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Parameter laporan belum lengkap!";
					$data['content']	= "laporan/statistik/index" ;
				}

				
			}
			else{
				$data['content']	= "laporan/statistik/index" ;
			}

			$this->load->view('promkes/index',$data);
			
		}
		else
		{
			redirect('admin');
		}
	}

	public function download_statistik()
	{
		ob_start();
		if ($this->user_id)
		{
			
			if (!empty($_POST)){
				
				if ($_POST['kd_kategori']!="" &&
					$_POST['tahun']!="") 
				{
					$tahun = $_POST['tahun'];
					$this->load->model('kategori_tempat_model');
					$this->kategori_tempat_model->kd_kategori = $kd_kategori = $_POST['kd_kategori'];
					$this->kategori_tempat_model->set_by_id();
					$kategori 	= $this->kategori_tempat_model->kategori;
					$this->load->model('referensi_model');
					$this->referensi_model->kd_kecamatan = $kd_kecamatan = $_POST['kd_kecamatan'];
					$this->referensi_model->kd_desa= $kd_desa = $_POST['kd_desa'];
					$this->referensi_model->set_kecamatan();
					$this->referensi_model->set_desa();
					$nama_kecamatan 	= $this->referensi_model->kecamatan;
					$nama_desa 	= $this->referensi_model->desa;
					$this->load->model('puskesmas_model');
					$this->puskesmas_model->kd_puskesmas = $kd_puskesmas = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$nama_puskesmas = $this->puskesmas_model->nama_puskesmas ;
					$this->load->model('indikator_model');
					$this->indikator_model->kd_kategori = $_POST['kd_kategori'];
					$indikator= $this->indikator_model->get_all();
					$bulan	= $this->bulan;

					if ($nama_kecamatan == "" ) $nama_kecamatan = "Semua" ;
					if ($nama_desa == "" ) $nama_desa ="Semua";
					if ($nama_puskesmas=="") $nama_puskesmas ="Semua" ;
		echo"
			<table>
				<tr>
					<td colspan=15><b>Rekapitulasi Statistik PHBS $kategori Tahun $tahun </b></td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td colspan=2>Kabupaten</td>
					<td colspan=13>: Bogor</td>
				</tr>
				<tr>
					<td colspan=2>Kecamatan</td>
					<td colspan=13>: $nama_kecamatan</td>
				</tr>
				<tr>
					<td colspan=2>Desa</td>
					<td colspan=13>: $nama_desa</td>
				</tr>
				<tr>
					<td colspan=2>Puskesmas</td>
					<td colspan=13>: $nama_puskesmas</td>
				</tr>
				
			</table>

	<table class='table table-bordered' width=100% border=1 style='border-collapse:collapse;font-size:10px' cellpadding=5 cellspacing=5 >
		<thead>
		<tr>
			<th rowspan=2 class='text-center'>No</th>
			<th rowspan=2 class='text-center'>Indikator</th>
			<th colspan=12 class='text-center'>Bulan</th>
			<th rowspan=2 class='text-center'>Total</th>
		</tr>
		<tr>";
				for ($bln=1; $bln <=12; $bln++)
				{
					echo"
						<th class='text-center'>".$bulan[$bln]."</th>
					";
				}
	echo"
		</tr>
		</thead>
		
		<tbody>
			";
				$num=1;
				$CI =& get_instance();
				$CI->load->model('report_model');
				foreach ($indikator as $row) {
					echo"
					<tr>
						<td class='text-center'>$num</td>
						<td>$row->indikator</td>";
						$total = 0;
						for ($bln=1; $bln <=12; $bln++)
						{
							$nilai = $CI->report_model->get_statistik($tahun,$bln,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas,$row->kd_indikator);
							$total = $total + $nilai;
							echo"
								<td class='text-center'>$nilai</td>
							";
						}
					echo"
						<td class='text-center'>$total</td>
					</tr>
					";
					$num++;
				}
		
		echo"
		</tbody>
		<thead>
			<tr>
				<th colspan=2 class='text-center'>Sehat</th>
					";
						$total_sehat = 0;
						for ($bln=1; $bln <=12; $bln++)
						{
							$sehat = $CI->report_model->get_statistik_status("Sehat",$tahun,$bln,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas);
							$total_sehat = $total_sehat + $sehat; 
							echo"
								<td class='text-center'>$sehat</td>
							";
						}
						echo"<td class='text-center'>$total_sehat</td>";
		echo"
			</tr>
			<tr>
				<th colspan=2 class='text-center'>Tidak Sehat</th>
					";
						$total_tdk_sehat=0;
						for ($bln=1; $bln <=12; $bln++)
						{
							$tdk_sehat = $CI->report_model->get_statistik_status("Tidak Sehat",$tahun,$bln,$kd_kategori,$kd_kecamatan,$kd_desa,$kd_puskesmas);
							$total_tdk_sehat = $total_tdk_sehat + $tdk_sehat;
							echo"
								<td class='text-center'>$tdk_sehat</td>
							";
						}
						echo "<td class='text-center'>$total_tdk_sehat</td>";
		echo"
			</tr>
		</thead>
	</table>
		";
					header("Content-Type: application/xls");    
					header("Content-Disposition: attachment; filename=statistik_PHBS_".$kategori."_tahun_".$tahun.".xls");  
					header("Pragma: no-cache"); 
					header("Expires: 0");
				}
				

				
			}
			
			
		}
		else
		{
			redirect('admin');
		}
	}
	public function grafik_phbs()
	{
		if ($this->user_id)
		{
			$data['title']		= "Laporan Statistik PHBS Bulanan";
			
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;$data['active_menu'] = "laporan";
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->kd_puskesmas;
			$data['puskesmas'] = $this->puskesmas_model->get_all();
			$this->load->model('puskesmas_model');
			$this->puskesmas_model->kd_puskesmas = $this->user_model->kd_puskesmas;
			$this->puskesmas_model->set_by_id();
			$this->load->model('referensi_model');
			$this->referensi_model->kd_kecamatan=$this->puskesmas_model->kd_kecamatan;
			$data['kecamatan'] = $this->referensi_model->get_kecamatan();
			$this->load->model('kategori_tempat_model');
			$data['kategori'] = $this->kategori_tempat_model->get_all();
			$data['bulan'] = $this->bulan;
			$data['active_menu'] = "laporan";
			$data['bulan'] = $this->bulan;
			$data['user_level'] = $this->user_level;
			$data['kd_kecamatan'] = $this->puskesmas_model->kd_kecamatan;
			$data['kd_puskesmas'] = $this->puskesmas_model->kd_puskesmas;
			$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas;
			$this->referensi_model->kd_kecamatan = $this->puskesmas_model->kd_kecamatan;
			$data['desa']=$this->referensi_model->get_desa_by_kecamatan();
			if (!empty($_POST)){
				
				if ($_POST['tahun']!="") 
				{
					$data['tahun_'] = $_POST['tahun'];
					$this->load->model('kategori_tempat_model');
					$this->kategori_tempat_model->kd_kategori = $data['kd_kategori'] = $_POST['kd_kategori'];
					$this->kategori_tempat_model->set_by_id();
					$data['kategori'] 	= $this->kategori_tempat_model->kategori;
					$this->load->model('indikator_model');
					$this->indikator_model->kd_indikator = $data['kd_indikator'] = $_POST['kd_indikator'];
					$this->indikator_model->set_by_id();
					$data['indikator'] 	= $this->indikator_model->indikator;
					$this->load->model('referensi_model');
					$this->referensi_model->kd_kecamatan = $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					$this->referensi_model->kd_desa= $data['kd_desa'] = $_POST['kd_desa'];
					$this->referensi_model->set_kecamatan();
					$this->referensi_model->set_desa();
					$data['nama_kecamatan'] 	= $this->referensi_model->kecamatan;
					$data['nama_desa'] 	= $this->referensi_model->desa;
					$this->puskesmas_model->kd_puskesmas = $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					$this->puskesmas_model->set_by_id();
					$data['nama_puskesmas'] = $this->puskesmas_model->nama_puskesmas ;
					
					$data['bulan']	= $this->bulan;
					$data['content']	= "laporan/grafik_phbs/grafik_phbs" ;
				}
				else{
					if (!empty($_POST['kd_puskesmas'])) $data['kd_puskesmas'] = $_POST['kd_puskesmas'];
					if (!empty($_POST['kd_kecamatan'])) $data['kd_kecamatan'] = $_POST['kd_kecamatan'];
					if (!empty($_POST['kd_kategori'])) $data['kd_kategori'] = $_POST['kd_kategori'];
					if (!empty($_POST['kd_indikator'])) $data['kd_indikator'] = $_POST['kd_indikator'];
					if (!empty($_POST['kd_desa'])) $data['kd_desa'] = $_POST['kd_desa'];
					if (!empty($_POST['tahun'])) $data['tahun_'] = $_POST['tahun'];
					$data['message_type'] = "warning";
					$data['message'] = "<strong>Opps..</strong> Parameter belum lengkap!";
					$data['content']	= "laporan/statistik/index" ;
				}

				
			}
			else{
				$data['content']	= "laporan/grafik_phbs/index" ;
			}

			$this->load->view('promkes/index',$data);
			
		}
		else
		{
			redirect('admin');
		}
	}
}
?>
