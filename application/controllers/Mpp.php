<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/integration/Mpp_service.php');

class Mpp extends CI_Controller {

    public function __construct(){
		parent ::__construct();	
		$this->user_id = $this->session->userdata('user_id');
		$this->load->model('user_model');
		$this->load->model('eoffice_model');
		$this->user_model->user_id = $this->user_id;
		$this->user_model->set_user_by_user_id();
		$this->user_picture = $this->user_model->user_picture;
		$this->full_name	= $this->user_model->full_name;
		$this->user_level	= $this->user_model->level;
		
        


		//echo $this->session->userdata('employee_id');exit();

    }
    
    public function index()
	{
		
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
            $data['title']		= app_name;
			$data['content']	= "mpp/dashboard" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
            $data['active_menu'] = "mpp";
            $data['page'] = "mpp";

			$this->user_model->user_id = $this->session->userdata('user_id');
            $this->user_model->set_user_by_user_id();
            
            $Mpp  = new Mpp_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $data['tahun'] = $tahun;
            $param = array(
                'tahun' => $tahun
            );
            $end_point = "integration/commandcenter/init";

            $rs = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs->error == false){
               // echo "<pre>";print_r($rs->data);die;
                $data['data_mpp'] = $rs->data;

                
            }
            

			
			$this->load->view('admin/index',$data);
		}
		else{
			redirect('admin/login');
		}
    }
    
    public function get_layanan($rowno=1)
	{
		if($this->input->is_ajax_request())
		{
			// Row per page
		    $rowperpage = 10;// $this->config->item('num_pagination');
		    $offset = ($rowno-1) * $rowperpage;
		    
			
            $limit		= $rowperpage;
            
            $Mpp  = new Mpp_service();

            
            $param = array(
                'page' => $rowno
            );
            $end_point = "integration/commandcenter/detail_layanan";

            $rs = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs->error == false){
                $result = $rs->data;
                $total_rows = $rs->total_rows;

                $this->load->library('pagination');
		
                // Pagination Configuration
                $config['base_url'] = "#";
                $config['use_page_numbers'] = TRUE;
                $config['total_rows'] = $total_rows;
                $config['per_page'] = $rowperpage;
                $config['attributes'] = array('class' => 'btn btn-default btn-outline ');

                // Initialize
                $this->pagination->initialize($config);

                $link = $this->pagination->create_links();
                $link = str_replace("<strong>", "<button type='button' class='btn btn-primary btn-xm secondary' >", $link);
                $link = str_replace("</strong>", "</button>", $link);

                // Initialize $data Array
                $data['pagination'] = $link;
                $data['result'] 	= $result;
                $data['row'] 		= $offset;
                
            }
            

		    echo json_encode($data);
		 

		}
    }

    public function rating()
	{
		
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
            $data['title']		= app_name;
			$data['content']	= "mpp/rating" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
            $data['active_menu'] = "mpp";
            $data['page'] = "mpp";

			$this->user_model->user_id = $this->session->userdata('user_id');
            $this->user_model->set_user_by_user_id();
            
            $Mpp  = new Mpp_service();

            $param = array();
            $end_point = "integration/commandcenter/rating_layanan";

            $rs = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs->error == false){
                //echo "<pre>";print_r($rs->data);die;
                $data['data'] = $rs->data;    
            }
            

			
			$this->load->view('admin/index',$data);
		}
		else{
			redirect('admin/login');
		}
    }
    public function skpd()
	{
		
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
            $data['title']		= app_name;
			$data['content']	= "mpp/skpd" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
            $data['active_menu'] = "mpp";
            $data['page'] = "mpp_skpd";

			$this->user_model->user_id = $this->session->userdata('user_id');
            $this->user_model->set_user_by_user_id();
            
            $Mpp  = new Mpp_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $id_skpd = (!empty($this->input->get("id_skpd"))) ? $this->input->get("id_skpd") : 1;
            $data['tahun'] = $tahun;
            $data['id_skpd'] = $id_skpd;
            $param = array(
                'tahun' => $tahun,
                'id_skpd'   => $id_skpd,
            );

            $end_point = "integration/commandcenter/antrian";
            
            $rs = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs->error == false){
                //echo "<pre>";print_r($rs);die;
                $data['data'] = $rs->data;
            }
            /*
            $dt = new stdClass();
            $dt->online = 90;
            $dt->offline = 10;

            $dt->monthly = array(1,2,3,4,5,6,7,8,9,10,11,12);
            $dt->monthly_label = array(1,2,3,4,5,6,7,8,9,10,11,12);

            $data['data'] = $dt;
            */
            //echo '<pre>';print_r($data);die;
			
			$this->load->view('admin/index',$data);
		}
		else{
			redirect('admin/login');
		}
    }

    public function ikm()
	{
		
		if ($this->user_id && $this->session->userdata("user_level")==1)
		{
            $data['title']		= app_name;
			$data['content']	= "mpp/ikm" ;
			$data['user_picture'] = $this->user_picture;
			$data['full_name']		= $this->full_name;
			$data['user_level']		= $this->user_level;
            $data['active_menu'] = "mpp";
            $data['page'] = "mpp_ikm";

			$this->user_model->user_id = $this->session->userdata('user_id');
            $this->user_model->set_user_by_user_id();
            
            $Mpp  = new Mpp_service();

            $tahun = (!empty($this->input->get("tahun"))) ? $this->input->get("tahun") : date("Y");
            $data['tahun'] = $tahun;
            $param = array(
                'tahun' => $tahun
            );
            $end_point = "integration/commandcenter/survey";

            $rs = json_decode($Mpp->sendRequest($param,$end_point));
            
            if($rs->error == false){
               // echo "<pre>";print_r($rs->data);die;
                $data['data'] = $rs->data;

                
            }
            

			
			$this->load->view('admin/index',$data);
		}
		else{
			redirect('admin/login');
		}
    }
}