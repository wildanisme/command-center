<?php
    class WebserviceMPP{
        private $base_url;
        private $url_get_layanan;
        private $url_add_antrian;
        private $url_update_status_antrian;
        private $url_status_antrian;
        private $url_inquiry;

        public function __construct()
        {
            //$this->base_url                     = "http://119.235.16.185/mpp/";
            $this->base_url                     = "https://mpp.sumedangkab.go.id";
            $this->url_get_layanan              = $this->base_url."/api/v2/layanan";
            $this->url_get_layanan_detail       = $this->base_url."/api/v1/layanan/detail";
            $this->url_add_antrian              = $this->base_url."/api/v1/antrian/add";
            $this->url_update_status_antrian    = $this->base_url."/api/v1/antrian/update-status";
            $this->url_status_antrian           = $this->base_url."/api/v1/antrian/status-antrian";
            $this->url_inquiry                  = $this->base_url."/api/v1/antrian/inquiry";
            $this->url_antrian                  = $this->base_url."/api/v1/antrian";

            $this->url_instansi                 = $this->base_url."/api/v1/get-instansi";
            $this->url_instansi_detail          = $this->base_url."/api/v1/get-instansi-detail";
        }
        public function get_layanan($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_get_layanan,$app_key,$param);
            return $response; 
        }
        public function get_instansi($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_instansi,$app_key,$param);
            return $response; 
        }
        public function get_instansi_detail($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_instansi_detail,$app_key,$param);
            return $response; 
        }
        public function get_layanan_detail($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_get_layanan_detail,$app_key,$param);
            return $response; 
        }
        public function inquiry($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_inquiry,$app_key,$param);
            return $response; 
        }

        public function daftar_antrian($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_add_antrian,$app_key,$param);
            return $response; 
        }
        public function get_status($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_status_antrian,$app_key,$param);
            return $response; 
        }
        public function get_antrian($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_antrian,$app_key,$param);
            return $response; 
        }
        public function update_status($app_key=null,$param=array())
        {
            $response = $this->send_request($this->url_update_status_antrian,$app_key,$param);
            return $response; 
        }


        private function send_request($url, $app_key=null,$fields=array(),$method="POST")
        {
            $headers = array(
                'Content-Type: application/x-www-form-urlencoded',
               
            );
            if($app_key){
                
                $h_api_key = "app_key:".$app_key;
                $headers[] = $h_api_key; //"app_key: on";
            }
            
            //echo "<pre>";print_r($headers);die;
            // Open connection
            $ch = curl_init();
            
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            if($method=="POST"){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query ($fields));
                
            }
            
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
     
     
            // Execute post
            $result = curl_exec($ch);
            
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            //var_dump($result);die;
            // Close connection
            curl_close($ch);
            
            return $result;
        }
    }
?>