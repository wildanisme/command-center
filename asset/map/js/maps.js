if ($('#map_todo_listing').length > 0) {
(function(A) {
	if (!Array.prototype.forEach)
		A.forEach = A.forEach || function(action, that) {
			for (var i = 0, l = this.length; i < l; i++)
				if (i in this)
					action.call(that, this[i], i, this);
			};

		})(Array.prototype);

		var
		mapObject,
		markers = [];

		if (typeof mapstyles === 'undefined' || localStorage.getItem("dark") !== "dark-only") {
			var	 mapstyles = [
                    {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                    {
                    "color": "#444444"
                    }
                    ]
                    },
                    {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                    {
                    "color": "#f2f2f2"
                    }
                    ]
                    },
                    {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                    {
                    "visibility": "off"
                    }
                    ]
                    },
                    {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                    {
                    "saturation": -100
                    },
                    {
                    "lightness": 45
                    }
                    ]
                    },
                    {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                    {
                    "visibility": "simplified"
                    }
                    ]
                    },
                    {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                    {
                    "visibility": "off"
                    }
                    ]
                    },
                    {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                    {
                    "visibility": "off"
                    }
                    ]
                    },
                    {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                    {
                    "color": "#cfcfcf"
                    },
                    {
                    "visibility": "on"
                    }
                    ]}];
		}

			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(-6.7921022, 107.8966181),
				mapTypeId: google.maps.MapTypeId.ROADMAP,

				mapTypeControl: false,
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
					position: google.maps.ControlPosition.LEFT_CENTER
				},
				panControl: false,
				panControlOptions: {
					position: google.maps.ControlPosition.TOP_RIGHT
				},
				zoomControl: true,
				zoomControlOptions: {
					position: google.maps.ControlPosition.RIGHT_BOTTOM
				},
				scrollwheel: false,
				scaleControl: false,
				scaleControlOptions: {
					position: google.maps.ControlPosition.TOP_LEFT
				},
				streetViewControl: true,
				streetViewControlOptions: {
					position: google.maps.ControlPosition.LEFT_TOP
				},
				styles: mapstyles,
			};
			var marker;
			mapObject = new google.maps.Map(document.getElementById('map_todo_listing'), mapOptions);

			for (var key in markersData)
				markersData[key].forEach(function (item) {
                    marker = new google.maps.Marker({
						position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
						map: mapObject,
						icon: item.map_marker,
					});
					if ('undefined' === typeof markers[key])
						markers[key] = [];
					markers[key].push(marker);
					google.maps.event.addListener(marker, 'click', (function () {
				  closeInfoBox();
				  getInfoBox(item).open(mapObject, this);
				  mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
				 }));

	});

	if (typeof AreaCoordinates !== 'undefined') {
		var BogorPolygon = new google.maps.Polygon({
		    paths: AreaCoordinates,
		    strokeColor: '#6d87fb',
		    strokeOpacity: 0.8,
		    strokeWeight: 3,
		    fillColor: '#6d87fb',
		    fillOpacity: 0.035
		});
		// Draw the polygon on the desired map instance
		BogorPolygon.setMap(mapObject);
	}


	new MarkerClusterer(mapObject, markers[key], {imagePath : 'https://cc.sumedangkab.go.id/asset/map/images/icon/marker/m'});
	
		function hideAllMarkers () {
			for (var key in markers)
				markers[key].forEach(function (marker) {
					marker.setMap(null);
				});
		};
	
	

		function closeInfoBox() {
			$('div.infoBox').remove();
		};

		function getInfoBox(item) {
			if (typeof item.rate3 !== 'undefined') {
				var rating = '<div class="rating"><span class="infobox_rate">'+ item.rate3 +'</span>' +
				'<span class="btn_infobox_reviews">'+ item.review3 +'</span></div>';
			} else {
				var rating = '';
			}
			return new InfoBox({
				content:
				'<div class="marker_info" id="marker_info">' +
				'<a href="'+item.url_point+'" ' + item._blank + '><div class="thumb"><img src="' + item.map_image_url + '" alt=""/><!--<a href="#" class="save"><i class="fa fa-heart"></i>save</a>--></div>' +
				'<div></a>'+ 
					'<h3><a href="'+item.url_point+'"' + item._blank + '>'+ item.name_point +'</a></h3>' +
					'<div class="rating"><span class="infobox_rate">'+ item.rate +'</span>' +
					'<span class="btn_infobox_reviews">'+ item.review +'</span></div>' +
					'<div class="rating"><span class="infobox_rate">'+ item.rate2 +'</span>' +
					'<span class="btn_infobox_reviews">'+ item.review2 +'</span></div>' + rating +
					'<div class="location-status"><div class="todo-location"><span class="icon-phone"></span>'+ item.telephone +'</div>' +
					'<div class="location-status"><div class="todo-location"><span class="icon-location"></span>'+ item.address +'</div>' +
                    '<div class="todo-status">'+ item.status +'</div></div>' +
					'</div>' +
				'</div>',
				disableAutoPan: false,
				maxWidth: 0,
				pixelOffset: new google.maps.Size(10, 212),
				closeBoxMargin: '',
				isHidden: false,
				alignBottom: true,
				pane: 'floatPane',
				enableEventPropagation: true
			});
		};
function onHtmlClick(location_type, key){
     google.maps.event.trigger(markers[location_type][key], "click");
}
}