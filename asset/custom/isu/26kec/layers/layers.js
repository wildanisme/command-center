var wms_layers = [];

var format_sumedang26_0 = new ol.format.GeoJSON();
var features_sumedang26_0 = format_sumedang26_0.readFeatures(json_sumedang26_0, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_sumedang26_0 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_sumedang26_0.addFeatures(features_sumedang26_0);
var lyr_sumedang26_0 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_sumedang26_0, 
                style: style_sumedang26_0,
                interactive: true,
    title: 'sumedang26<br />\
    <img src="styles/legend/sumedang26_0_0.png" /> 1<br />\
    <img src="styles/legend/sumedang26_0_1.png" /> 2<br />\
    <img src="styles/legend/sumedang26_0_2.png" /> 3<br />\
    <img src="styles/legend/sumedang26_0_3.png" /> 4<br />\
    <img src="styles/legend/sumedang26_0_4.png" /> 5<br />\
    <img src="styles/legend/sumedang26_0_5.png" /> 6<br />\
    <img src="styles/legend/sumedang26_0_6.png" /> 7<br />\
    <img src="styles/legend/sumedang26_0_7.png" /> 8<br />\
    <img src="styles/legend/sumedang26_0_8.png" /> 9<br />\
    <img src="styles/legend/sumedang26_0_9.png" /> 10<br />\
    <img src="styles/legend/sumedang26_0_10.png" /> 11<br />\
    <img src="styles/legend/sumedang26_0_11.png" /> 12<br />\
    <img src="styles/legend/sumedang26_0_12.png" /> 13<br />\
    <img src="styles/legend/sumedang26_0_13.png" /> 14<br />\
    <img src="styles/legend/sumedang26_0_14.png" /> 15<br />\
    <img src="styles/legend/sumedang26_0_15.png" /> 16<br />\
    <img src="styles/legend/sumedang26_0_16.png" /> 17<br />\
    <img src="styles/legend/sumedang26_0_17.png" /> 18<br />\
    <img src="styles/legend/sumedang26_0_18.png" /> 19<br />\
    <img src="styles/legend/sumedang26_0_19.png" /> 20<br />\
    <img src="styles/legend/sumedang26_0_20.png" /> 21<br />\
    <img src="styles/legend/sumedang26_0_21.png" /> 22<br />\
    <img src="styles/legend/sumedang26_0_22.png" /> 23<br />\
    <img src="styles/legend/sumedang26_0_23.png" /> 24<br />\
    <img src="styles/legend/sumedang26_0_24.png" /> 25<br />\
    <img src="styles/legend/sumedang26_0_25.png" /> 26<br />\
    <img src="styles/legend/sumedang26_0_26.png" /> <br />'
        });

lyr_sumedang26_0.setVisible(true);
var layersList = [lyr_sumedang26_0];
lyr_sumedang26_0.set('fieldAliases', {'id': 'id', });
lyr_sumedang26_0.set('fieldImages', {'id': '', });
lyr_sumedang26_0.set('fieldLabels', {'id': 'no label', });
lyr_sumedang26_0.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});