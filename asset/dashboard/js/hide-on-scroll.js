"use strict";
$(window).scroll(function() {
    if ($(this).scrollTop()>100){
        $('.page-main-header').fadeOut();
        $('#c-pills-tab').fadeOut();
        $( ".page-body-wrapper" ).addClass( "scrolled" );
        $( ".iconsidebar-menu" ).addClass( "scrolled" );
        $( ".iconMenu-bar" ).addClass( "scrolled" );
    }
    else
    {
        $('.page-main-header').fadeIn();
        $('#c-pills-tab').fadeIn();
        $( ".page-body-wrapper" ).removeClass( "scrolled" );
        $( ".iconsidebar-menu" ).removeClass( "scrolled" );
        $( ".iconMenu-bar" ).removeClass( "scrolled" );
    }
});