"use strict";
function testAnim(x) {
    $('.modal .modal-dialog').attr('class', 'modal-dialog modal-dialog-centered modal-dialog-scrollable  ' + x + '  animated');
};
var modal_animate_custom = {
    init: function() {
        $('#cika-modal').on('show.bs.modal', function (e) {
            var anim = "bounceIn";
            testAnim(anim);
        })
        $('#cika-modal').on('hide.bs.modal', function (e) {
            var anim = "flipOutX";
            testAnim(anim);
        })
        $("a").tooltip();
    }
};
(function($) {
    "use strict";
    modal_animate_custom.init()
})(jQuery);