var colors = ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d', '#ffc717'];

// Anggaran Belanja Langsung
if (typeof labels1 !== 'undefined' && typeof series1 !== 'undefined') {
    var options1 = {
        chart: {
            width: 380,
            type: 'donut',
        },
        labels: labels1,
        series: series1,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        // dataLabels: {
        //   enabled: true,
        //   textAnchor: 'start',
        //   formatter: function(val, opt) {
        //       // return [val.toFixed(2) + "% ", opt.w.globals.series[opt.seriesIndex] + " Miliar"] 
        //       return [opt.w.globals.series[opt.seriesIndex] + " Miliar"] 
        //   },
        //   offsetX: 0,
        // },
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Miliar";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart1 = new ApexCharts(
        document.querySelector("#keuanganchart1"),
        options1
        );

    chart1.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels2 !== 'undefined' && typeof series2 !== 'undefined') {
    var options2 = {
        chart: {
            width: 380,
            type: 'donut',
        },
        labels: labels2,
        series: series2,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Milliar";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart2 = new ApexCharts(
        document.querySelector("#keuanganchart2"),
        options2
        );

    chart2.render();
}

// Anggaran Pendapatan
if (typeof labels3 !== 'undefined' && typeof series3 !== 'undefined') {
    var options3 = {
        chart: {
            width: 380,
            type: 'donut',
        },
        labels: labels3,
        series: series3,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Milliar";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart3 = new ApexCharts(
        document.querySelector("#keuanganchart3"),
        options3
        );

    chart3.render();
}

// Perbandingan Anggaran Dan Realisasi Penerimaan Pembiayaan
if (typeof labels4 !== 'undefined' && typeof series4 !== 'undefined') {
    var options4 = {
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: labels4,
        series: series4,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Milliar";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart4 = new ApexCharts(
        document.querySelector("#keuanganchart4"),
        options4
        );

    chart4.render();
}

// Perbandingan Anggaran Dan Realisasi Penerimaan Pembiayaan 2
var options5 = {

    chart: {
        height: 350,
        type: 'area',
        toolbar: {
            show: false
        },
    },
    dataLabels: {
        enabled: false
    },
    grid: {
        borderColor: '#f0f7fa',
    },
    stroke: {
        curve: 'smooth',
        width: 4
    },
    series: [{
        name: 'Anggaran',
        data: [50, 45, 55, 50, 60, 56, 58, 50, 65, 60, 50, 60, 52, 55, 52]
    },{
        name: 'Realisasi',
        data: [50, 65, 60, 50, 60, 52, 55, 52, 45, 55, 50, 60, 56, 58, 50]
    }],

    xaxis: {
        low: 0,
        offsetX: 0,
        offsetY: 0,
        show: false,
        type: 'datetime',
        labels: {
            low: 0,
            offsetX: 0,
            show: true,
        },
        axisBorder: {
            low: 0,
            offsetX: 0,
            show: false,
        },
        axisTicks: {
            show: false,
        },
        categories: ["2021-09-19T00:00:00", "2021-09-19T01:30:00", "2021-09-19T02:30:00", "2021-09-19T03:30:00", "2021-09-19T04:30:00", "2021-09-19T05:30:00", "2021-09-19T06:30:00", "2021-09-19T07:30:00", "2021-09-19T08:30:00", "2021-09-19T09:30:00", "2021-09-19T10:30:00", "2021-09-19T11:30:00" , "2021-09-19T12:30:00", "2021-09-19T13:30:00", "2021-09-19T14:30:00"],
    },
    yaxis:{
        labels: {
            show: false
        }
    },
    tooltip: {
        x: {
            format: 'dd/MM/yy HH:mm'
        },
    },
    colors:colors
}

var chart5 = new ApexCharts(
    document.querySelector("#keuanganchart5"),
    options5
    );

chart5.render();

// Perbandingan Anggaran Dan Realisasi Pengeluaran Pembiayaan
if (typeof labels6 !== 'undefined' && typeof series6 !== 'undefined') {
    var options6 = {
        chart: {
            width: 380,
            type: 'pie',
        },
        labels: labels6,
        series: series6,
        legend: {
            position: 'bottom'
        },
        colors:colors,
        yaxis: {
          labels: {
            formatter: function (value) {
              return value + " Milliar";
          }
      },
    },
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }]
    }

    var chart6 = new ApexCharts(
        document.querySelector("#keuanganchart6"),
        options6
        );

    chart6.render();
}

// Perbandingan Anggaran Dan Realisasi Pengeluaran Pembiayaan 2
var options7 = {

    chart: {
        height: 350,
        type: 'area',
        toolbar: {
            show: false
        },
    },
    dataLabels: {
        enabled: false
    },
    grid: {
        borderColor: '#f0f7fa',
    },
    stroke: {
        curve: 'smooth',
        width: 4
    },
    series: [{
        name: 'Anggaran',
        data: [50, 45, 55, 50, 60, 56, 58, 50, 65, 60, 50, 60, 52, 55, 52]
    },{
        name: 'Realisasi',
        data: [50, 65, 60, 50, 60, 52, 55, 52, 45, 55, 50, 60, 56, 58, 50]
    }],

    xaxis: {
        low: 0,
        offsetX: 0,
        offsetY: 0,
        show: false,
        type: 'datetime',
        labels: {
            low: 0,
            offsetX: 0,
            show: true,
        },
        axisBorder: {
            low: 0,
            offsetX: 0,
            show: false,
        },
        axisTicks: {
            show: false,
        },
        categories: ["2021-09-19T00:00:00", "2021-09-19T01:30:00", "2021-09-19T02:30:00", "2021-09-19T03:30:00", "2021-09-19T04:30:00", "2021-09-19T05:30:00", "2021-09-19T06:30:00", "2021-09-19T07:30:00", "2021-09-19T08:30:00", "2021-09-19T09:30:00", "2021-09-19T10:30:00", "2021-09-19T11:30:00" , "2021-09-19T12:30:00", "2021-09-19T13:30:00", "2021-09-19T14:30:00"],
    },
    yaxis:{
        labels: {
            show: false
        }
    },
    tooltip: {
        x: {
            format: 'dd/MM/yy HH:mm'
        },
    },
    colors:colors
}

var chart7 = new ApexCharts(
    document.querySelector("#keuanganchart7"),
    options7
    );

chart7.render();



