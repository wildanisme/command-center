var colors = ['#7e37d8', '#fe80b2', '#80cf00', '#06b5dd', '#fd517d', '#ffc717'];

// Anggaran Belanja Tdk Langsung
if (typeof labels_sakip_skpd !== 'undefined') {
  var options_sakip1 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Pengukuran Sakip',
      data: series_sakip_skpd
    }],
    xaxis: {
      categories: labels_sakip_skpd,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors
  }

  var chart_sakip1 = new ApexCharts(
    document.querySelector("#capaian-skpd"),
    options_sakip1
    );

  chart_sakip1.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels_sakip_kecamatan !== 'undefined') {
  var options_sakip2 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Pengukuran Sakip',
      data: series_sakip_kecamatan
    }],
    xaxis: {
      categories: labels_sakip_kecamatan,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors[4]
  }

  var chart_sakip2 = new ApexCharts(
    document.querySelector("#capaian-kecamatan"),
    options_sakip2
    );

  chart_sakip2.render();
}

// Anggaran Belanja Langsung
if (typeof series1 !== 'undefined') {

  var optionsearningchart = {
    chart: {
      height: 400,
      type: 'radialBar',
    },
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 20,
          size: "50%"
        },
        startAngle: -135,
        endAngle: 135,
        dataLabels: {
          name: {
            fontSize: '16px',
            color: '#ffffff',
            offsetY: 10
          },
          value: {
            offsetY: -40,
            fontSize: '22px',
            color: '#ffffff',
            formatter: function(val) {
              return val + "%";
            }
          }
        }
      }
    },

    fill: {
      opacity: 1
    },
    colors: ['#ffffff'],
    series: series1,
    stroke: {
      dashArray: 5,
    },
    labels: ['Capaian'],
  }

  var chartearningchart = new ApexCharts(
    document.querySelector("#sasaran_1"),
    optionsearningchart
    );
  // chartearningchart.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof series2 !== 'undefined') {
  var optionsearningchart2 = {
    chart: {
      height: 400,
      type: 'radialBar',
    },
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 20,
          size: "50%"
        },
        startAngle: -135,
        endAngle: 135,
        dataLabels: {
          name: {
            fontSize: '16px',
            color: '#ffffff',
            offsetY: 10
          },
          value: {
            offsetY: -40,
            fontSize: '22px',
            color: '#ffffff',
            formatter: function(val) {
              return val + "%";
            }
          }
        }
      }
    },

    fill: {
      opacity: 1
    },
    colors: ['#ffffff'],
    series: series2,
    stroke: {
      dashArray: 5,
    },
    labels: ['Capaian'],
  }
  chartearningchart.render();
  var chartearningchart2 = new ApexCharts(
    document.querySelector("#sasaran_2"),
    optionsearningchart2
    );
  chartearningchart2.render();
}

// Anggaran Pendapatan
if (series3 !== 'undefined') {
  var optionsearningchart3 = {
    chart: {
      height: 400,
      type: 'radialBar',
    },
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 20,
          size: "50%"
        },
        startAngle: -135,
        endAngle: 135,
        dataLabels: {
          name: {
            fontSize: '16px',
            color: '#ffffff',
            offsetY: 10
          },
          value: {
            offsetY: -40,
            fontSize: '22px',
            color: '#ffffff',
            formatter: function(val) {
              return val + "%";
            }
          }
        }
      }
    },

    fill: {
      opacity: 1
    },
    colors: ['#ffffff'],
    series: series3,
    stroke: {
      dashArray: 5,
    },
    labels: ['Capaian'],
  }
  var chartearningchart3 = new ApexCharts(
    document.querySelector("#sasaran_3"),
    optionsearningchart3
    );
  chartearningchart3.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels4 !== 'undefined') {
  var options3 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Menurunnya Jumlah Rumah Tangga Miskin',
      data: series4_c1
    }, {
      name: 'Meningkatnya pencegahan Stunting Terintegrasi',
      data: series4_c2
    }, {
      name: 'Meningkatnya kualitas pelayanan publik di Desa',
      data: series4_c3
    }],
    xaxis: {
      categories: labels4,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors
  }

  var chart3 = new ApexCharts(
    document.querySelector("#capaian-desa"),
    options3
    );

  chart3.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels4_k1 !== 'undefined') {
  var options3_1 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Menurunnya Jumlah Rumah Tangga Miskin',
      data: series4_c1_k1
    }, {
      name: 'Meningkatnya pencegahan Stunting Terintegrasi',
      data: series4_c2_k1
    }, {
      name: 'Meningkatnya kualitas pelayanan publik di Desa',
      data: series4_c3_k1
    }],
    xaxis: {
      categories: labels4_k1,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors
  }

  var chart3_1 = new ApexCharts(
    document.querySelector("#capaian-miskin"),
    options3_1
    );

  chart3_1.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels4_k2 !== 'undefined') {
  var options3_2 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Menurunnya Jumlah Rumah Tangga Miskin',
      data: series4_c1_k2
    }, {
      name: 'Meningkatnya pencegahan Stunting Terintegrasi',
      data: series4_c2_k2
    }, {
      name: 'Meningkatnya kualitas pelayanan publik di Desa',
      data: series4_c3_k2
    }],
    xaxis: {
      categories: labels4_k2,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors
  }

  var chart3_2 = new ApexCharts(
    document.querySelector("#capaian-stunting"),
    options3_2
    );

  chart3_2.render();
}

// Anggaran Belanja Tdk Langsung
if (typeof labels4_k3 !== 'undefined') {
  var options3_3 = {
    chart: {
      height: 350,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        endingShape: 'rounded',
        columnWidth: '55%',
      },
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
    },
    series: [{
      name: 'Menurunnya Jumlah Rumah Tangga Miskin',
      data: series4_c1_k3
    }, {
      name: 'Meningkatnya pencegahan Stunting Terintegrasi',
      data: series4_c2_k3
    }, {
      name: 'Meningkatnya kualitas pelayanan publik di Desa',
      data: series4_c3_k3
    }],
    xaxis: {
      categories: labels4_k3,
    },
    yaxis: {
      title: {
        text: ''
      }
    },
    fill: {
      opacity: 1

    },
    tooltip: {
      y: {
        formatter: function (val) {
          return "" + val + "%"
        }
      }
    },
    colors:colors
  }

  var chart3_3 = new ApexCharts(
    document.querySelector("#capaian-pelayanan"),
    options3_3
    );

  chart3_3.render();
}